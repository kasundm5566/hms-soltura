Soltura Customization Guide
==================================

**This guide is intended for developers who want to extend Soltura and to do customization for different clients.** 

Soltura build system is based on maven profiles. Default profile is for the Base Product. When doing customization for a new client you have create a new profile for that client. 

Profile configuration can be found and **parent/pom.xml** file. Only thing that should differ for the customization is the property files. Those changing property files should reside in **src/main/profile** folder for each of the sub modules.

When adding new profile configuration to parent/pom.xml set the location of profile files inside `properties` section.

*Eg:*

    <properties>
    			   <resource.file.location>profile/newProfile</resource.file.location>
    </properties>

Each new profile should have it's own **filter.properties** file. Create new filter.properties file inside **filter-properties/profile** for your new profile.

##Developing Client Specific Feature

When new feature request coming for a particular client deployment following steps need to be carried out when developing it. 

 * Analyze and identify separate features
 * Implement the functionality as a core feature
 * Add entry to feature-activation.properties to enable and disable it.
 * Make the value true in the feature-activation.properties in the profile of respected client. And for other clients make it false.


**Note:** At precent *feature-activation.properties* file is only avalilable in sotura web module since core functionality is same in all the deployments to this date. If new core functionality comes we have to add *feature-activation.properties* file to *kite-integration* module.





 

