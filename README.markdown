
SOLTURA Guide for Build and Deployment
===============================================================================
## System setup

### Software Versions

* JAVA : jdk 1.6
* Tomcat : 6.0.16
* Maven : 2.2.1 or higher

### Environment Variables

* MAVEN_HOME
* JAVA_HOME
* CATALINA_HOME

## Build system

### Introduction

Soltura build system is based on maven profiles. Currently there are 6 main profiles.

 * default - Base Product
 * vcity - Virtual City development
 * vcity-live - Virtual City Live configurations
 * dialog-dev - Dialog development
 * dialog-live - Dialog Live configuration
 * cloud - Soltura Cloud depoloment with Operator Charing

Each of the modules will have separate profile folder under src/main . Features/properties specific to particular profile will reside on that directory. When doing the maven build with correct profile it will copy correct resource files.

### Build Steps

Build the system by running `mvn clean install -DskipTests -P<profile_name>`

**Note**: For `<profile_name>` use one of the profiles described above. If you want to build *Base Product* use `mvn clean install -DskipTests`

### Development Version Updating

Soltura uses Maven Versions plugin to update versions in `pom` files. Follow following step when you need to update versions.

    1. Update `parent/pom.xml` and change it version to correct version.
    2. Run `mvn versions:update-child-modules` from top level

### Releasing Soltura

Soltura uses Maven Release plugin to magange software release management. Follow following steps when you need to release new version of Soltura.

    1. Commit and push all your changes.
    2. Run `mvn release:prepare -Dmaven.test.skip=true -Darguments="-Dmaven.test.skip=true" -Dresume=false`
    3. Run `mvn release:perform`

## Deploying Soltura

### DB setup

Initial script to create the mysql database required by soltura can be found at **db-scripts/mysql/create-orca-tables.sql**. Create database called `integration` and source that initial script to that database.

### DNS Configuration

Soltura requires following set of DNS entries. Edit your **/etc/hosts** file and add following to it.

    127.0.0.1    web.cur
    127.0.0.1    prov.core.sdp
    127.0.0.1    workflowdb.core.sdp
    127.0.0.1    web.pgw
    127.0.0.1    core.pgw
    127.0.0.1    web.sol
    127.0.0.1    core.sol
    127.0.0.1    db.mysql.sol
    127.0.0.1    core.sdp

### Deploging Soltura Message Flow

Zip file for the orca message flow can be found at **kite-integration/target/mchoice_orca.zip** after building the system.

Copy that file to desired deployment location and unzip it. Start the system using start-up script found at bin folder.

`sh mchoice_orca console`

### Deploying Soltura Web APP

War file for web app can be found at **soltura/target/soltura.war** after building the system. Copy that to tomcat webapps folder.

### Add terms and conditions pages

* Copy <common-registration>/docs/dialog-terms-and-conditions folder and put into apache-tomcat/webapps folder
* Add following proxy passes to the dev.sdp.hsenidmobile.com:443 virtual host of /hms/installs/apache2/conf/extra/httpd-ssl.conf file

     ProxyPass /soltura-terms-conditions/ http://127.0.0.1:4287/dialog-terms-and-conditions/
     ProxyPassReverse /soltura-terms-conditions/ http://127.0.0.1:4287/dialog-terms-and-conditions/

* Restart apache-httpd server


## Dependent Modules

In order to test the full functionality of Soltura You have to have the following applications and services up and running

 * CAS web App          - To do the User authentication
 * Registration Web App - To do the user management and user authorization handling
 * Provisioning REST API - Create applications in the provisioning for the soltura applications
 * Provisioning WEB APP -  To do the application approval and state transfers
 * SDP Message Flow - To run the SMS message flow for the soltura created applications
 * PGW Selfcare Web App - To do the Subscriber Financial Instrument configurations
 * App store Web App - To publish the soltura created applications

===





