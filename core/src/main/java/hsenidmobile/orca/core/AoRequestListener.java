/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core;

import hsenidmobile.orca.core.flow.ApplicationFinder;
import hsenidmobile.orca.core.flow.MtRequestHandler;
import hsenidmobile.orca.core.flow.impl.RequestPipelineImpl;
import hsenidmobile.orca.core.model.OrcaException;
import hsenidmobile.orca.core.model.message.AoRequest;
import hsenidmobile.orca.core.model.message.MessageContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is the start point of AO(Dev) message processing.
 *
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class AoRequestListener {
	private static final Logger logger = LoggerFactory.getLogger(AoRequestListener.class);
	private ApplicationFinder applicationFinder;
	private MtRequestHandler mtRequestHandler;

	private RequestPipelineImpl requestPipeline;

	public void init() {
		logger.info("Initializing AO Message Processing Pipeline");

		requestPipeline = new RequestPipelineImpl();
		requestPipeline.addLast("app-finder", applicationFinder);
		requestPipeline.addLast("mt-sender", mtRequestHandler);
	}

	/**
	 * Pass received AO message to the core for processing.
	 *
	 * @param message
	 *            - Received AO message
	 * @param context
	 *            - Message Context
	 * @throws OrcaException
	 */
	public void onMessage(AoRequest message, MessageContext context) throws OrcaException {
		logger.debug("AO[{}][{}] received.", message, context);

		requestPipeline.onMessage(message, context);
	}

	public void setApplicationFinder(ApplicationFinder applicationFinder) {
		this.applicationFinder = applicationFinder;
	}

	public void setMtRequestHandler(MtRequestHandler mtRequestHandler) {
		this.mtRequestHandler = mtRequestHandler;
	}

}
