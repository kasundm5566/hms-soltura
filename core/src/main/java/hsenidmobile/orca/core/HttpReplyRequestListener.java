/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hsenidmobile.orca.core;

import hsenidmobile.orca.core.flow.ApplicationFinder;
import hsenidmobile.orca.core.flow.HttpUpdateRequestDispatcher;
import hsenidmobile.orca.core.flow.impl.RequestPipelineImpl;
import hsenidmobile.orca.core.model.OrcaException;
import hsenidmobile.orca.core.model.message.MessageContext;
import hsenidmobile.orca.core.model.message.RequestShowReplyRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * $LastChangedDate: 2011-04-22 18:34:09 +0530 (Fri, 22 Apr 2011) $
 * $LastChangedBy: dulika $
 * $LastChangedRevision: 72393 $
 */
public class HttpReplyRequestListener {

    private static final Logger logger = LoggerFactory.getLogger(HttpReplyRequestListener.class);
	private ApplicationFinder applicationFinder;
	private HttpUpdateRequestDispatcher httpUpdateRequestDispatcher;

	private RequestPipelineImpl requestPipeline;

	public void init() {
		logger.info("Initializing HttpReplyRequestListener Processing Pipeline");

		requestPipeline = new RequestPipelineImpl();
		requestPipeline.addLast("app-finder", applicationFinder);
		requestPipeline.addLast("app-data-update-dispatcher", httpUpdateRequestDispatcher);
	}

	/**
	 * Pass received AO message to the core for processing.
	 *
	 * @param message
	 *            - Received AO message
	 * @param context
	 *            - Message Context
	 * @throws hsenidmobile.orca.core.model.OrcaException
	 */
	public void onMessage(RequestShowReplyRequest message, MessageContext context) throws OrcaException {
		logger.debug("RequestShowReplyRequest[{}][{}] received.", message, context);

		requestPipeline.onMessage(message, context);
	}

	public void setApplicationFinder(ApplicationFinder applicationFinder) {
		this.applicationFinder = applicationFinder;
	}

	public void setHttpUpdateRequestDispatcher(HttpUpdateRequestDispatcher httpUpdateRequestDispatcher) {
		this.httpUpdateRequestDispatcher = httpUpdateRequestDispatcher;
	}
}
