/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core;

import hsenidmobile.orca.core.flow.ApplicationFinder;
import hsenidmobile.orca.core.flow.ApplicationSubkeywordExtractor;
import hsenidmobile.orca.core.flow.MessageFlowException;
import hsenidmobile.orca.core.flow.MoRequestDispatcher;
import hsenidmobile.orca.core.flow.MtRequestHandler;
import hsenidmobile.orca.core.flow.command.CommandBuilder;
import hsenidmobile.orca.core.flow.event.MessageFlowEvent;
import hsenidmobile.orca.core.flow.exception.ExceptionHandler;
import hsenidmobile.orca.core.flow.impl.RequestPipelineImpl;
import hsenidmobile.orca.core.model.OrcaException;
import hsenidmobile.orca.core.model.message.Message;
import hsenidmobile.orca.core.model.message.MessageContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is the start point of Mo Message processing.
 *
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class MoRequestListener {

	private Logger logger = LoggerFactory.getLogger(MoRequestListener.class);
	private CommandBuilder commandBuilder;
	private ApplicationFinder applicationFinder;
	private MoRequestDispatcher moRequestDispatcher;
	private MtRequestHandler mtRequestHandler;
	private ApplicationSubkeywordExtractor appKeywordExtractor;

	private RequestPipelineImpl requestPipeline;

	private ExceptionHandler exceptionHandler;

	public void init() {
		logger.info("Initializing MO Message Processing Pipeline");

		requestPipeline = new RequestPipelineImpl();
		requestPipeline.addLast("command-builder", commandBuilder);
		requestPipeline.addLast("app-finder", applicationFinder);
		requestPipeline.addLast("keyword-extractor", appKeywordExtractor);
		requestPipeline.addLast("mo-dispatcher", moRequestDispatcher);
		// requestPipeline.addLast("mt-sender", mtRequestHandler);
	}

	/**
	 * Pass received MO message to the core for processing.
	 *
	 * @param message
	 *            - Received MO message
	 * @param context
	 *            - Message Context
	 * @throws OrcaException
	 */
	public void onMessage(Message message, MessageContext context) throws OrcaException {
			logger.debug("MO[{}][{}] received.", message, context);

		MessageFlowEvent<Message> messageFlowEvent = new MessageFlowEvent<Message>(message, context);
		try {
			requestPipeline.onMessage(messageFlowEvent);
		} catch (MessageFlowException mfe) {
			if (mfe.getMessageFlowEvent() == null) {
				mfe.setMessageFlowEvent(messageFlowEvent);
			}
			exceptionHandler.handleException(messageFlowEvent, mfe);
		} catch (Throwable th) {
			exceptionHandler.handleException(messageFlowEvent, th);
		}
	}

	public void setCommandBuilder(CommandBuilder commandBuilder) {
		this.commandBuilder = commandBuilder;
	}

	public void setApplicationFinder(ApplicationFinder applicationFinder) {
		this.applicationFinder = applicationFinder;
	}

	public void setMoRequestDispatcher(MoRequestDispatcher moRequestDispatcher) {
		this.moRequestDispatcher = moRequestDispatcher;
	}

	public void setMtRequestHandler(MtRequestHandler mtRequestHandler) {
		this.mtRequestHandler = mtRequestHandler;
	}

	public void setAppKeywordExtractor(ApplicationSubkeywordExtractor appKeywordExtractor) {
		this.appKeywordExtractor = appKeywordExtractor;
	}

	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}

}
