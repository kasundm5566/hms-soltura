/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *
 */
package hsenidmobile.orca.core.applications;

import hsenidmobile.orca.core.applications.exception.ApplicationException;
import hsenidmobile.orca.core.applications.subscription.Periodicity;
import hsenidmobile.orca.core.flow.MessageFlowErrorReasons;
import hsenidmobile.orca.core.model.ContentRelease;
import hsenidmobile.orca.core.model.OrcaException;
import hsenidmobile.orca.core.model.SubCategory;
import hsenidmobile.orca.core.repository.PersistentObject;

import java.util.Collections;
import java.util.List;
import java.util.Locale;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CollectionOfElements;
import org.hibernate.annotations.IndexColumn;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.slf4j.Logger;

/**
 * $LastChangedDate: $ $LastChangedBy: $ $LastChangedRevision: $
 */
@MappedSuperclass
public abstract class AbstractService extends PersistentObject implements Service {

	@Transient
	private Logger logger;

	@OneToMany(fetch = FetchType.EAGER)
	@LazyCollection(LazyCollectionOption.FALSE)
	@Cascade(value = { org.hibernate.annotations.CascadeType.ALL })
	@IndexColumn(name = "idx")
	private List<SubCategory> supportedSubCategories;

	@Column(name = "default_locale", nullable = false)
	private Locale defaultLocale = Locale.ENGLISH;

	@CollectionOfElements(targetElement = Locale.class)
	@Cascade(value = { org.hibernate.annotations.CascadeType.ALL })
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<Locale> supportedLocales;

	protected AbstractService(Logger logger) {
		this.logger = logger;
	}

	@Override
	public boolean isSubCategoriesSupported() {
		return this.supportedSubCategories != null && !this.supportedSubCategories.isEmpty();
	}

	@Override
	public List<SubCategory> getSupportedSubCategories() {
		if (this.supportedSubCategories == null) {
			return Collections.EMPTY_LIST;
		} else {
			return this.supportedSubCategories;
		}
	}

	@Override
	public void setSupportedSubCategories(List<SubCategory> subCategories) {
		this.supportedSubCategories = subCategories;
	}

	public Locale getDefaultLocale() {
		return defaultLocale;
	}

	public void setDefaultLocale(Locale defaultLocale) {
		this.defaultLocale = defaultLocale;
	}

	public List<Locale> getSupportedLocales() {
		return supportedLocales;
	}

	public void setSupportedLocales(List<Locale> supportedLocales) {
		this.supportedLocales = supportedLocales;
	}

	public boolean isLocaleSupported(Locale locale) {
		if (this.supportedLocales == null || this.supportedLocales.isEmpty()) {
			return false;
		} else {
			for (Locale supported : this.supportedLocales) {
				if (supported.equals(locale)) {
					return true;
				}
			}
			return false;
		}
	}

	@Override
	public SubCategory getSubcategory(String subkeyword) throws ApplicationException {
		if (isSubCategoriesSupported()) {
			for (SubCategory subCategory : supportedSubCategories) {
				if (subCategory.isMatch(subkeyword)) {
					return subCategory;
				}
			}
			throw new ApplicationException("SubKeyword not found[" + subkeyword + "]",
					MessageFlowErrorReasons.SUB_KEYWORD_NOT_FOUND);
		} else {
			return null;
		}

	}

	public void validateSubCategory(SubCategory subCategoryToMatch) throws ApplicationException {
		if (isSubCategoriesSupported()) {
			if (subCategoryToMatch != null) {
				for (SubCategory subCategory : supportedSubCategories) {
					if (subCategory.isMatch(subCategoryToMatch.getKeyword())) {
						return;
					}
				}
			}
			throw new ApplicationException("SubKeyword not found[" + subCategoryToMatch + "]",
					MessageFlowErrorReasons.SUB_KEYWORD_NOT_FOUND);
		} else {
			if (subCategoryToMatch != null) {
				throw new ApplicationException("SubKeyword not supported.",
						MessageFlowErrorReasons.SUB_KEYWORD_NOT_SUPPORTED);
			}
		}
	}

	@Override
	public void updateServiceData(List<ContentRelease> contentRelease) throws OrcaException {
		throw new UnsupportedOperationException();
	}

}
