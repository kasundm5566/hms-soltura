/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.applications;

import hsenidmobile.orca.core.applications.alert.AlertService;
import hsenidmobile.orca.core.applications.competition.Competition;
import hsenidmobile.orca.core.applications.developer.Dev;
import hsenidmobile.orca.core.applications.exception.ApplicationException;
import hsenidmobile.orca.core.applications.policy.AuthorValidationPolicy;
import hsenidmobile.orca.core.applications.policy.NMessagesPerNPeriodPolicy;
import hsenidmobile.orca.core.applications.policy.RegistrationPolicy;
import hsenidmobile.orca.core.applications.request.RequestService;
import hsenidmobile.orca.core.applications.subscription.Subscription;
import hsenidmobile.orca.core.applications.voting.VotingService;
import hsenidmobile.orca.core.flow.MessageFlowErrorReasons;
import hsenidmobile.orca.core.flow.MessageFlowException;
import hsenidmobile.orca.core.flow.event.CommandEvent;
import hsenidmobile.orca.core.model.Application;
import hsenidmobile.orca.core.model.ApplicationChargingInfo;
import hsenidmobile.orca.core.model.ApplicationRoutingInfo;
import hsenidmobile.orca.core.model.Author;
import hsenidmobile.orca.core.model.ContentRelease;
import hsenidmobile.orca.core.model.LocalizedMessage;
import hsenidmobile.orca.core.model.Msisdn;
import hsenidmobile.orca.core.model.OrcaException;
import hsenidmobile.orca.core.model.RoutingKey;
import hsenidmobile.orca.core.model.Status;
import hsenidmobile.orca.core.model.message.Message;
import hsenidmobile.orca.core.model.message.MoResponse;
import hsenidmobile.orca.core.repository.AppRepository;
import hsenidmobile.orca.core.repository.PersistentObject;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import hsenidmobile.orca.core.services.AuthorValidationService;
import org.hibernate.annotations.Any;
import org.hibernate.annotations.AnyMetaDef;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.IndexColumn;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.hibernate.annotations.MetaValue;
import org.hibernate.annotations.NamedNativeQueries;
import org.hibernate.annotations.NamedNativeQuery;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.joda.time.DateTimeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * $LastChangedDate$
 * $LastChangedBy$ $LastChangedRevision$
 */
@Entity
@Table(name = "application")
public class ApplicationImpl extends PersistentObject implements Application {

    @Transient
    private static final Logger logger = LoggerFactory.getLogger(ApplicationImpl.class);
    @Transient
    private AppRepository applicationRepository;

    @Transient
    private AuthorValidationService authorValidationService;

    @Column(name = "app_id", nullable = false, unique = true, updatable = false, length = 20)
    private String appId;

    @Column(name = "app_name", nullable = false, unique = true, updatable = false, length = 50)
    private String appName;

    @Column(name = "ncs_type", length = 10)
    @Enumerated(EnumType.STRING)
    private NcsType ncsType;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", length = 30)
    private Status status;

    @Transient
    private String displayStatus;

    @Column(name = "start_date", nullable = false)
    @Type(type = "org.joda.time.contrib.hibernate.PersistentDateTime")
    private DateTime startDate;

    @Column(name = "end_date", nullable = false)
    @Type(type = "org.joda.time.contrib.hibernate.PersistentDateTime")
    private DateTime endDate;

    private String description;

    @Transient
    private String shortDescription;

    @OneToMany(fetch = FetchType.EAGER)
    @LazyCollection(LazyCollectionOption.FALSE)
    @Cascade(value = { CascadeType.ALL, CascadeType.DELETE_ORPHAN })
    @JoinTable(name = "app_invalid_req_resp",
            joinColumns = { @JoinColumn(name = "application_id") },
            inverseJoinColumns = { @JoinColumn(name = "localized_msg_id") })
    @IndexColumn(name = "idx")
    private List<LocalizedMessage> invalidRequestErrorMessage;

    @OneToMany(fetch = FetchType.EAGER)
    @LazyCollection(LazyCollectionOption.FALSE)
    @Cascade(value = { CascadeType.ALL, CascadeType.DELETE_ORPHAN })
    @JoinTable(name = "app_subscription_resp",
            joinColumns = { @JoinColumn(name = "application_id") },
            inverseJoinColumns = { @JoinColumn(name = "localized_msg_id") })
    @IndexColumn(name = "idx")
    private List<LocalizedMessage> subscriptionResponse;

    @OneToMany(fetch = FetchType.EAGER)
    @LazyCollection(LazyCollectionOption.FALSE)
    @Cascade(value = { CascadeType.ALL, CascadeType.DELETE_ORPHAN })
    @JoinTable(name = "app_unsubscription_resp",
            joinColumns = { @JoinColumn(name = "application_id") },
            inverseJoinColumns = { @JoinColumn(name = "localized_msg_id") })
    @IndexColumn(name = "idx")
    private List<LocalizedMessage> unsubscribeResponse;

    @Column(name = "is_expirable")
    private boolean expirableApp;

    @Transient
    private ApplicationRoutingInfo applicationRoutingInfo;

    @Transient
    private ApplicationChargingInfo applicationChargingInfo;

    @Transient
    private boolean registrationRequired;

    @Transient
    private boolean chargingFromSubscriber;

    @Transient
    private boolean sateChageAllowed;

    @Transient
    private boolean requireCommonResponse;

    @Transient
    private String sdpAppId;

    @Enumerated(EnumType.STRING)
    @Column(name = "registration_policy", nullable = false, length = 7)
    private RegistrationPolicy registrationPolicy;

    @Any(metaColumn = @Column(name = "data_message_policies"))
    @AnyMetaDef(idType = "long", metaType = "string", metaValues = { @MetaValue(value = "DATAMESSAGEPOLICIES", targetEntity = NMessagesPerNPeriodPolicy.class) })
    @JoinColumn(name = "data_message_policies_id")
    @Cascade(value = org.hibernate.annotations.CascadeType.ALL)
    private ServicePolicy dataMessagePolicies;

    @Any(metaColumn = @Column(name = "service"), fetch = FetchType.EAGER)
    @AnyMetaDef(idType = "long", metaType = "string", metaValues = {
            @MetaValue(value = "VOTING", targetEntity = VotingService.class),
            @MetaValue(value = "DEV", targetEntity = Dev.class),
            @MetaValue(value = "SUBSCRIPTION", targetEntity = Subscription.class),
            @MetaValue(value = "ALERT", targetEntity = AlertService.class),
            @MetaValue(value = "REQUEST", targetEntity = RequestService.class),
            @MetaValue(value = "COMPETITION", targetEntity = Competition.class) })
    @JoinColumn(name = "service_id")
    @Cascade(value = org.hibernate.annotations.CascadeType.ALL)
    @LazyToOne(LazyToOneOption.FALSE)
    private Service service;

    @OneToMany
    @LazyCollection(value = LazyCollectionOption.FALSE)
    @Cascade(value = org.hibernate.annotations.CascadeType.ALL)
    private List<Author> authors;

    public MoResponse onDataMessage(CommandEvent<Message> commandEvent) throws MessageFlowException {

        logger.info("[DATA_MSG] Subkeyword[{}], Message [{}] appId [{}] appName [{}]",
                new Object[] { commandEvent.getSubKeyword(), commandEvent.getMessage(), getAppId(), getAppName() });

        final MoResponse response = getService().onDataMessage(commandEvent);

        logger.debug("Message received sending response [{}]", response);
        return response;
    }

    public MoResponse updateServiceData(CommandEvent<Message> commandEvent) throws OrcaException {
        logger.info("[UPDATE_MSG] Update through Sms. Message [{}]", commandEvent);

        Msisdn senderAddress = commandEvent.getMessage().getSenderAddress();
        if (authorValidationService.isValidAuthor(this, senderAddress)) {
            MoResponse response = this.getService().onServiceDataUpdateMessage(commandEvent);
            this.applicationRepository.updateService(getService());
            logger.debug("Service data saved back to DB.");
            return response;
        } else {
            throw new ApplicationException("Msisdn[" + senderAddress + "] is not allowed to update service data.",
                    MessageFlowErrorReasons.INVALID_AUTHOR);
        }
    }

    @Override
    public void updateServiceData(List<ContentRelease> contentReleaseList) throws OrcaException {
        StringBuilder logMsg = new StringBuilder(500);
        logMsg.append("[UPDATE_MSG] Update through web. AppId[");
        logMsg.append(getAppId());
        logMsg.append(", UpdateContent[");
        logMsg.append(contentReleaseList);
        logMsg.append("]");
        logger.info(logMsg.toString());

        this.getService().updateServiceData(contentReleaseList);
        this.applicationRepository.updateService(getService());
    }

    public MoResponse reportAbuse(Message message) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    /**
     * Creates the ApplicationId for an application which starts with
     * application name followed by a five numeric characters
     *
     * @param appName
     *            Application name for which the appId must be generated
     * @throws hsenidmobile.orca.core.exception.ApplicationException
     */
    public void createAppId(String appName) throws ApplicationException {
        if (appId == null) {
            if (appName != null && appName.length() > 4) {
                appName = appName.substring(0, 4).toLowerCase().replaceAll(" ", "");
            }
            NumberFormat numberFormat = new DecimalFormat("0000");
            appId = appName + "_" + numberFormat.format(new Random().nextInt(10000));
        }
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public NcsType getNcsType() {
        return ncsType;
    }

    public void setNcsType(NcsType ncsType) {
        this.ncsType = ncsType;
    }

    public Long getId() {
        return super.getId();
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public DateTime getStartDate() {
        return startDate;
    }

    public DateTime getEndDate() {
        return endDate;
    }

    public void setStartDate(DateTime startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(DateTime endDate) {
        this.endDate = endDate;
    }

    /**
     * @param keyword
     *            Check whether given keyword is a valid one for this service.
     */
    public boolean isValidKeyword(String keyword) {
        throw new UnsupportedOperationException();
    }

    public ApplicationRoutingInfo getApplicationRoutingInfo() {
        return applicationRoutingInfo;
    }

    private String getMessage(List<LocalizedMessage> localizedMsgList, Locale locale) {
        if (localizedMsgList != null) {
            for (LocalizedMessage msg : localizedMsgList) {
                if (msg.getLocale().equals(locale)) {
                    return msg.getMessage();
                }
            }
        }

        logger.warn("No message found for locale[{}]", locale);
        return "";
    }

    public String getInvalidRequestErrorMessageDefault() {
        return getMessage(invalidRequestErrorMessage, Locale.ENGLISH);
    }

    public List<LocalizedMessage> getInvalidRequestErrorMessage() {
        return invalidRequestErrorMessage;
    }

    public String getInvalidRequestErrorMessage(Locale locale) {
        return getMessage(invalidRequestErrorMessage, locale);
    }

    public void setInvalidRequestErrorMessage(List<LocalizedMessage> invalidRequestErrorMessage) {
        this.invalidRequestErrorMessage = invalidRequestErrorMessage;
    }

    public List<LocalizedMessage> getSubscriptionResponse() {
        return subscriptionResponse;
    }

    public String getSubscriptionResponse(Locale locale) {
        return getMessage(subscriptionResponse, locale);
    }

    public void setSubscriptionResponse(List<LocalizedMessage> subscriptionResponse) {
        this.subscriptionResponse = subscriptionResponse;
    }

    public List<LocalizedMessage> getUnsubscribeResponse() {
        return unsubscribeResponse;
    }

    public String getUnsubscribeResponse(Locale locale) {
        return getMessage(unsubscribeResponse, locale);
    }

    public void setUnsubscribeResponse(List<LocalizedMessage> unsubscribeResponse) {
        this.unsubscribeResponse = unsubscribeResponse;
    }

    public void setApplicationRoutingInfo(ApplicationRoutingInfo routingInfo) {
        this.applicationRoutingInfo = routingInfo;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public RegistrationPolicy getRegistrationPolicy() {
        return registrationPolicy;
    }

    public void setRegistrationPolicy(RegistrationPolicy registrationPolicy) {
        this.registrationPolicy = registrationPolicy;
    }

    public ServicePolicy getDataMessagePolicies() {
        return dataMessagePolicies;
    }

    public void setDataMessagePolicies(ServicePolicy dataMessagePolicies) {
        this.dataMessagePolicies = dataMessagePolicies;
    }

    public AppRepository getApplicationRepository() {
        return applicationRepository;
    }

    public void setApplicationRepository(AppRepository applicationRepository) {
        this.applicationRepository = applicationRepository;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public List<RoutingKey> getRoutingKeys() {
        if (applicationRoutingInfo != null && applicationRoutingInfo.getRoutingInfos() != null) {
            return applicationRoutingInfo.getRoutingKeys();
        }
        return Collections.EMPTY_LIST;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    public ApplicationChargingInfo getApplicationChargingInfo() {
        return applicationChargingInfo;
    }

    public void setApplicationChargingInfo(ApplicationChargingInfo applicationChargingInfo) {
        this.applicationChargingInfo = applicationChargingInfo;
    }

    public boolean isRegistrationRequired() {
        return registrationRequired;
    }

    public void setRegistrationRequired(boolean registrationRequired) {
        this.registrationRequired = registrationRequired;
    }

    public boolean isChargingFromSubscriber() {
        return chargingFromSubscriber;
    }

    public void setChargingFromSubscriber(boolean chargingFromSubscriber) {
        this.chargingFromSubscriber = chargingFromSubscriber;
    }

    public boolean isSateChageAllowed() {
        return sateChageAllowed;
    }

    public void setSateChageAllowed(boolean sateChageAllowed) {
        this.sateChageAllowed = sateChageAllowed;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getDisplayStatus() {
        return displayStatus;
    }

    public void setDisplayStatus(String displayStatus) {
        this.displayStatus = displayStatus;
    }

    @Override
    public boolean isExpirableApp() {
        return expirableApp;
    }

    @Override
    public void setExpirableApp(boolean expirableApp) {
        this.expirableApp = expirableApp;
    }

    public boolean isRequireCommonResponse() {
        return requireCommonResponse;
    }

    public void setRequireCommonResponse(boolean requireCommonResponse) {
        this.requireCommonResponse = requireCommonResponse;
    }

    public String getSdpAppId() {
        return sdpAppId;
    }

    public void setSdpAppId(String sdpAppId) {
        this.sdpAppId = sdpAppId;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(350);
        builder.append("ApplicationImpl [appId=");
        builder.append(appId);
        builder.append(", appName=");
        builder.append(appName);
        builder.append(", applicationRepository=");
        builder.append(applicationRepository);
        builder.append(", applicationRoutingInfo=");
        builder.append(applicationRoutingInfo);
        builder.append(", authors=");
        builder.append(authors);
        builder.append(", dataMessagePolicies=");
        builder.append(dataMessagePolicies);
        builder.append(", description=");
        builder.append(description);
        builder.append(", endDate=");
        builder.append(endDate);
        builder.append(", ncsType=");
        builder.append(ncsType);
        builder.append(", registrationPolicy=");
        builder.append(registrationPolicy);
        builder.append(", service=");
        builder.append(service);
        builder.append(", startDate=");
        builder.append(startDate);
        builder.append(", status=");
        builder.append(status);
        builder.append(", expirableApp=");
        builder.append(expirableApp);
        builder.append("]");
        return builder.toString();
    }

    public void setAuthorValidationService(AuthorValidationService authorValidationService) {
        this.authorValidationService = authorValidationService;
    }
}