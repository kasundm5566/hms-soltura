/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hsenidmobile.orca.core.applications;

import org.joda.time.DateTime;

/**
 * $LastChangedDate:$
 * $LastChangedBy:$
 * $LastChangedRevision:$
 */
public class ContentDispatchSummary {

    private String lastSentContent;
    private DateTime lastSentOnTime;

    public String getLastSentContent() {
        return lastSentContent;
    }

    public void setLastSentContent(String lastSentContent) {
        this.lastSentContent = lastSentContent;
    }

    public DateTime getLastSentOnTime() {
        return lastSentOnTime;
    }

    public void setLastSentOnTime(DateTime lastSentOnTime) {
        this.lastSentOnTime = lastSentOnTime;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("ContentDispatchSummary [lastSentContent");
        stringBuilder.append(lastSentContent);
        stringBuilder.append(", lastSentOnTime");
        stringBuilder.append(lastSentOnTime);
        stringBuilder.append("]");
        return stringBuilder.toString();
    }
}
