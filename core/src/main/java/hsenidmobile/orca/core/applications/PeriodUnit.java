/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$ 
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.applications;

import java.util.EnumSet;

public enum PeriodUnit {
	HOUR("h"), DAY("d"), WEEK("w"),MONTH("m"), CUSTOM("");

	private String code = "";

	private PeriodUnit(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	private void setCode(String code) {
		this.code = code;
	}

	/**
	 * Create PeriodUnit enum from given string code.
	 * @param code
	 * @return
	 */
	public static PeriodUnit getEnum(String code) {
		if (code == null || code.trim().length() == 0) {
			throw new IllegalArgumentException("PeriodUnit code cannot be empty or null.");
		}

		for (PeriodUnit tag : EnumSet.allOf(PeriodUnit.class)) {
			if (tag.getCode().equalsIgnoreCase(code)) {
				return tag;
			}
		}

		PeriodUnit unit = CUSTOM;
		unit.setCode(code.toLowerCase());

		return unit;

	}

}
