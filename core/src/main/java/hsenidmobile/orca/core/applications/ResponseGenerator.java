package hsenidmobile.orca.core.applications;



/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */


public enum ResponseGenerator {

    GENERATE {

        @Override
        public String generateResponse() {
            return "";
        }
    };

    public abstract String generateResponse();
}
