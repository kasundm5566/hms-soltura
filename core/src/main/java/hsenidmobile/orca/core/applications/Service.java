/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.applications;

import hsenidmobile.orca.core.applications.exception.ApplicationException;
import hsenidmobile.orca.core.flow.MessageFlowErrorReasons;
import hsenidmobile.orca.core.flow.MessageFlowException;
import hsenidmobile.orca.core.flow.event.CommandEvent;
import hsenidmobile.orca.core.model.Application;
import hsenidmobile.orca.core.model.ContentRelease;
import hsenidmobile.orca.core.model.OrcaException;
import hsenidmobile.orca.core.model.SubCategory;
import hsenidmobile.orca.core.model.message.Message;
import hsenidmobile.orca.core.model.message.MoResponse;

import java.util.List;
import java.util.Locale;

/**
 * This is the real functionality provided by the system.
 *
 * @version $LastChangedRevision$
 */
public interface Service {

	/**
	 * Data message received at the service.
	 *
	 * @param subkeyword
	 *            -
	 * @param message
	 *            -
	 * @param messageContext
	 *            -
	 * @return -
	 * @throws MessageFlowException
	 *             TODO
	 */
	MoResponse onDataMessage(CommandEvent<Message> commandEvent) throws MessageFlowException;

	void updateServiceData(List<ContentRelease> contentRelease) throws OrcaException;

	/**
	 * Service Data update request(MO message) received.
	 *
	 * @param commandEvent
	 *            -
	 * @return -
	 * @throws LanguageNotSupportedException
	 *             -
	 */
	MoResponse onServiceDataUpdateMessage(CommandEvent<Message> commandEvent) throws OrcaException;

	/**
	 * Application where this service is belongs to...
	 *
	 * @param application
	 *            -
	 */
	void setApplication(Application application);

	/**
	 * Indicate whether this service supports SubCategories
	 *
	 * @return
	 */
	boolean isSubCategoriesSupported();

	/**
	 * Returns List of SubCategories supported by this service. If service do
	 * not support SubCategories an empty list will be returned.
	 *
	 * @return
	 */
	List<SubCategory> getSupportedSubCategories();

	void setSupportedSubCategories(List<SubCategory> subCategories);

	/**
	 * Get SubCategory for a given subkeyword.
	 *
	 * @param subkeyword
	 * @return
	 * @throws ApplicationException
	 *             - If no matching SubCategory found
	 *             {@link ApplicationException} with
	 *             {@link MessageFlowErrorReasons#SUB_KEYWORD_NOT_FOUND} will be
	 *             thrown.
	 */
	SubCategory getSubcategory(String subkeyword) throws ApplicationException;

	/**
	 * Gets the default Locale of the service. If you have not set a Locale
	 * specifically using {@link #setDefaultLocale(Locale)},
	 * {@link Locale#ENGLISH} will be used.
	 *
	 * @return
	 */
	Locale getDefaultLocale();

	/**
	 * Set the default locale for the application.
	 *
	 * @param defaultLocale
	 */
	void setDefaultLocale(Locale defaultLocale);

	/**
	 * List of all supported Locales
	 *
	 * @return
	 */
	List<Locale> getSupportedLocales();

	void setSupportedLocales(List<Locale> supportedLocales);

	/**
	 * Check whether a given Locale is supported by the service.
	 *
	 * @param locale
	 * @return
	 */
	boolean isLocaleSupported(Locale locale);

	/**
	 * Whether this service require user to be subscribed before using the app
	 * @return
	 */
	boolean isSubscriptionRequired();

}
