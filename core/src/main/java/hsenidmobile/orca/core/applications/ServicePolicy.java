/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$ 
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.applications;

import hsenidmobile.orca.core.model.message.MessageContext;
import hsenidmobile.orca.core.model.message.Message;

public interface ServicePolicy {

	public void enforce(MessageContext messageContext, Message message);

	public void setNextPolicy(ServicePolicy nextPolicy);

	public ServicePolicy getNextPolicy();
}