/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.applications.alert;

import hsenidmobile.orca.core.model.ContentRelease;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
@Entity
@Table(name = "alert_content_release")
public class AlertContentRelease extends ContentRelease {

    private static final long serialVersionUID =  3021807506368691072L;

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("AlertContentRelease [");
        builder.append(super.toString()).append("]");
        return builder.toString();
    }


}
