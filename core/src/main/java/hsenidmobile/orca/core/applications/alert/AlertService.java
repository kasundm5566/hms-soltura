/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 *
 */
package hsenidmobile.orca.core.applications.alert;

import hsenidmobile.orca.core.applications.AbstractService;
import hsenidmobile.orca.core.applications.alert.traffic.AlertAppTraffic;
import hsenidmobile.orca.core.applications.exception.ApplicationException;
import hsenidmobile.orca.core.flow.MessageFlowErrorReasons;
import hsenidmobile.orca.core.flow.dispatch.aventuraapi.AventuraApiMtMessageDispatcher;
import hsenidmobile.orca.core.flow.event.CommandEvent;
import hsenidmobile.orca.core.model.*;
import hsenidmobile.orca.core.model.Content.ContentSource;
import hsenidmobile.orca.core.model.message.Message;
import hsenidmobile.orca.core.model.message.MoResponse;
import hsenidmobile.orca.core.model.message.SmsMessage;
import hsenidmobile.orca.core.repository.AlertDispatchedContentRepository;
import hsenidmobile.orca.core.repository.AppTrafficRepository;
import hsenidmobile.orca.core.repository.SdpApplicationRepository;
import hsenidmobile.orca.core.util.PropertyHolder;
import org.hibernate.annotations.Proxy;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.concurrent.*;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static hsenidmobile.orca.core.util.eventlog.EventLogger.createEventLog;

/**
 *
 * $LastChangedDate: $ $LastChangedBy: $ $LastChangedRevision: $
 *
 */
@Entity
@Table(name = "alert_service")
@Proxy(lazy = false)
public class AlertService extends AbstractService {

    private static final String AVENTURA_MAPPING_FOR_BLANK_SUBKEYWORD = "all_registered";

    @Transient
    private static final Logger logger = LoggerFactory.getLogger(AlertService.class);
    @Transient
    private Application application;

    @Transient
    private AventuraApiMtMessageDispatcher aventuraApiMtMessageDispatcher;

    @Transient
    private SdpApplicationRepository sdpApplicationRepository;
    @Transient
    private AlertDispatchedContentRepository dispatchedContentRepository;
    @Transient
    private AppTrafficRepository appTrafficRepository;

    @Column(name = "is_sms_update_support_required")
    private boolean smsUpdateRequired = false;

    @Column(name = "mapd")
    private  int mapd;

    @Column(name = "mapm")
    private int mapm;

    private static final String MESSAGE_ID_KEY = "requestId";
    private static final String SUCCESS__CODE = "S1000";
    private static final String STATUS_CODE_KEY = "statusCode";
    private static final String STATUS_DETAILS_KEY = "statusDetail";

    public AlertService() {
        super(logger);
    }

    @Override
    public MoResponse onDataMessage(CommandEvent<Message> commandEvent) throws ApplicationException {
        if (logger.isDebugEnabled()) {
            logger.debug("onDataMessage Event, subkeyword[{}], data[{}]", commandEvent.getSubKeyword(), commandEvent
                    .getMessage().getMessage());
        }
        throw new UnsupportedOperationException("This method is not Supported in Alert Service.");
    }

    @Override
    public MoResponse onServiceDataUpdateMessage(CommandEvent<Message> commandEvent) throws OrcaException {
        logger.debug("Service Data Update message received. Message[{}]", commandEvent);
        if (smsUpdateRequired) {
            ContentRelease contentRelease = createContentReleaseFromMo(commandEvent);
            AlertAppTraffic alertAppTraffic = appTrafficRepository.findAlertAppTraffic(application.getAppId());
            logger.debug("Found Alert App traffic data for the application [{}] - [{}] From the Message Flow ", new Object[]
                    {application.getAppName(), alertAppTraffic.toString()});
            alertAppTraffic.verifyThroughput(this.mapd, this.mapm);
            updateServiceData(Arrays.asList(contentRelease));
            updateAlertAppTrafficData(application.getAppName(), alertAppTraffic);
            aventuraApiMtMessageDispatcher.dispatchIndividualMessages(application.getAppId(),
                    Arrays.asList(createSmsUpdateSuccessMsg(commandEvent)));
        } else {
            throw new AlertServiceException("Service data update through Sms not supported.",
                    MessageFlowErrorReasons.SMS_UPDATE_NOT_SUPPORTED);
        }

        // TODO: Return null since returning MoResponse from method not being
        // used currently and to be changed to void in future.
        return null;
    }

    private void updateAlertAppTrafficData(String appName, AlertAppTraffic alertAppTraffic) {
        int currentDailyCount = alertAppTraffic.getCurrentAlertMsgsPerDay().getCurrentNoOfAlertMessages();
        int currentMonthlyCount = alertAppTraffic.getCurrentAlertMsgsPerMonth().getCurrentNoOfAlertMessages();
        alertAppTraffic.getCurrentAlertMsgsPerDay().setCurrentNoOfAlertMessages(++currentDailyCount);
        alertAppTraffic.getCurrentAlertMsgsPerMonth().setCurrentNoOfAlertMessages(++currentMonthlyCount);
        appTrafficRepository.updateAlertAppThroughput(alertAppTraffic);
        logger.debug("Updated Alert App traffic data for the application [{}] - [{}] From the Message Flow", new Object[] {appName,
                alertAppTraffic.toString()});
    }

    @Override
    public void updateServiceData(List<ContentRelease> contentRelease) throws OrcaException {
        if (contentRelease != null && contentRelease.size() == 1) {
            if (contentRelease.get(0) instanceof AlertContentRelease) {
                try {
                    AlertContentRelease newContent = (AlertContentRelease) contentRelease.get(0);
                    validateAndPurifyContent(newContent);
                    newContent.setAppId(this.application.getAppId());
                    newContent.setAppName(this.application.getAppName());
                    newContent.setContentSentTime(new DateTime());
                    Future<Map<String, Object>> future = sendAlertsToSubscribers(newContent);
                    Map<String, Object> response = future.get();
                    if (response.get(STATUS_CODE_KEY).equals(SUCCESS__CODE)) {
                        newContent.setBcMessageId((Long) response.get(MESSAGE_ID_KEY));
                        this.dispatchedContentRepository.addDispatchedContent(newContent);
                    } else{
                        logger.error("Error returned from sdp, "+response.get(STATUS_DETAILS_KEY));
                    }
                } catch (Exception e) {
                    logger.error("Ecxeption occured retrieving the response", e);
                }

            } else {
                throw new IllegalStateException("Supports only AlertContentRelease, given["
                        + contentRelease.getClass().getSimpleName() + "]");
            }

        } else {
            logger.warn("Content given to update is null or contains unexpected number of ContentReleases.");
        }
    }

    private void validateAndPurifyContent(AlertContentRelease newContent) throws AlertServiceException,
            ApplicationException {
        List<Content> contents = newContent.getContents();
        if (contents == null || contents.isEmpty()) {
            throw new AlertServiceException("Content given to update is empty",
                    MessageFlowErrorReasons.ALERT_CONTENT_EMPTY);
        }

        List<Content> nonEmptyContent = new ArrayList<Content>();
        for (Content content : contents) {
            validateSubCategory(content.getSubCategory());

            logger.debug("Validating for blank content[{}]", content);
            if (content.getContent() != null && content.getContent().getMessage() != null
                    && content.getContent().getMessage().trim().length() > 0) {
                nonEmptyContent.add(content);
            } else {
                logger.debug("Removing blank content[{}]", content);
            }
        }

        if (nonEmptyContent.isEmpty()) {
            throw new AlertServiceException("Content given to update is empty",
                    MessageFlowErrorReasons.ALERT_CONTENT_EMPTY);
        } else {
            newContent.setContents(nonEmptyContent);
        }
    }

    private ContentRelease createContentReleaseFromMo(CommandEvent<Message> commandEvent) throws ApplicationException {
        Content updateContent = new Content();
        updateContent.setSource(ContentSource.SMS);
        updateContent.setContent(new LocalizedMessage(commandEvent.getMessage().getMessage(), commandEvent
                .getLanguage()));
        updateContent.setSubCategory(this.getSubcategory(commandEvent.getSubKeyword()));

        ContentRelease contentRelease = new AlertContentRelease();
        contentRelease.setContents(Arrays.asList(updateContent));
        contentRelease.setCorrelationId(commandEvent.getMessage().getCorrelationId());
        return contentRelease;
    }

    private SmsMessage createSmsUpdateSuccessMsg(CommandEvent<Message> commandEvent) {
        String responseTxt = PropertyHolder.ALERT_SERVICE_DATA_UPDATE_SUCCESS_MSG;
        SmsMessage respMsg = new SmsMessage();
        respMsg.setCorrelationId(commandEvent.getMessage().getCorrelationId());
        respMsg.addReceiverAddress(commandEvent.getMessage().getSenderAddress());
        respMsg.setMessage(responseTxt);
        respMsg.setKeyword(commandEvent.getMessage().getSenderAddress().getAddress());
        respMsg.setSenderAddress(new Msisdn(this.application.getAppName(), ""));
        return respMsg;
    }

    private Future sendAlertsToSubscribers(AlertContentRelease contentRelease) throws ApplicationException {
        logger.info("Dispatching new Alert Service message(s) for AppId[{}]", application.getAppId());
        List<SmsMessage> smsMessages = new ArrayList<SmsMessage>();

        if (isSubCategoriesSupported()) {
            for (Content content : contentRelease.getContents()) {
                SmsMessage message = createSmsBroadcastMessage(content.getSubCategory().getKeyword(), content
                        .getContent().getMessage());
                message.setCorrelationId(contentRelease.getCorrelationId());
                smsMessages.add(message);
            }

        } else {

            logger.debug("Application do not support sub categories, using [{}] to create SMS.",
                    AVENTURA_MAPPING_FOR_BLANK_SUBKEYWORD);
            SmsMessage message = createSmsBroadcastMessage(AVENTURA_MAPPING_FOR_BLANK_SUBKEYWORD, contentRelease
                    .getContents().get(0).getContent().getMessage());
            message.setCorrelationId(contentRelease.getCorrelationId());
            smsMessages.add(message);
        }
        return aventuraApiMtMessageDispatcher.dispatchBroadcastMessage(application.getAppId(), smsMessages);
    }

    private SmsMessage createSmsBroadcastMessage(String keyword, String messageTxt) {
        SmsMessage message = new SmsMessage();
        message.setKeyword(keyword);
        message.setMessage(messageTxt);
        //todo put a logic to get the  sender address
        message.setSenderAddress(new Msisdn(application.getAppName(), "ANY"));
        createEventLog(application, keyword, "", Event.CONTENT_SENT, "application " + application.getAppId()
                + " sent an alert");
        logger.debug("Created alert message [{}]", message);
        return message;
    }

    @Override
    public void setApplication(Application application) {
        this.application = application;
    }

    public void setAventuraApiMtMessageDispatcher(AventuraApiMtMessageDispatcher aventuraApiMtMessageDispatcher) {
        this.aventuraApiMtMessageDispatcher = aventuraApiMtMessageDispatcher;
    }

    public void setSdpApplicationRepository(SdpApplicationRepository sdpApplicationRepository) {
        this.sdpApplicationRepository = sdpApplicationRepository;
    }

    public boolean isSmsUpdateRequired() {
        return smsUpdateRequired;
    }

    public void setSmsUpdateRequired(boolean smsUpdateRequired) {
        this.smsUpdateRequired = smsUpdateRequired;
    }

    public void setDispatchedContentRepository(AlertDispatchedContentRepository dispatchedContentRepository) {
        this.dispatchedContentRepository = dispatchedContentRepository;
    }

    public void setAppTrafficRepository(AppTrafficRepository appTrafficRepository) {
        this.appTrafficRepository = appTrafficRepository;
    }

    public int getMapd() {
        return mapd;
    }

    public void setMapd(int mapd) {
        this.mapd = mapd;
    }

    public int getMapm() {
        return mapm;
    }

    public void setMapm(int mapm) {
        this.mapm = mapm;
    }

	@Override
	public boolean isSubscriptionRequired() {
		return true;
	}
}