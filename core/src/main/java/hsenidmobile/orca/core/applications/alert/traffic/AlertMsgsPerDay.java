/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hsenidmobile.orca.core.applications.alert.traffic;

import hsenidmobile.orca.core.applications.alert.AlertServiceException;
import hsenidmobile.orca.core.flow.exception.MessageThroughputExceededException;
import static hsenidmobile.orca.core.flow.MessageFlowErrorReasons.DAYS_EXCEEDED;
import hsenidmobile.orca.core.repository.PersistentObject;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Transient;
import java.util.Calendar;


/**
 * $LastChangedDate:$
 * $LastChangedBy:$
 * $LastChangedRevision:$
 */
@Embeddable
public class AlertMsgsPerDay  {

    @Column (name = "cmpd_luts")
    private long lastUpdatedDate = 0;
    @Column (name = "cmpd")
    private int currentNoOfAlertMessages;
    @Transient
    private int maxAlertMessagesPerDay = 0;

    public AlertMsgsPerDay() {
    }

    public AlertMsgsPerDay(int maxAlertMessagesPerDay) {
        this.maxAlertMessagesPerDay = maxAlertMessagesPerDay;
        lastUpdatedDate = getCurrentDate();
    }

    public synchronized void verifyThroughputLimit() throws MessageThroughputExceededException {
        long currentDate = getCurrentDate();
        if (maxAlertMessagesPerDay <= 0) {
            return;
        }
        if (currentDate > lastUpdatedDate) {
            lastUpdatedDate = currentDate;
            currentNoOfAlertMessages = 0;
        }
        int tmp = currentNoOfAlertMessages + 1;
        if (tmp > maxAlertMessagesPerDay) {
            throw new MessageThroughputExceededException("Maximum alert messages per day limit exceeded", DAYS_EXCEEDED);
        } 
    }

    private long getCurrentDate() {
        final Calendar instance = Calendar.getInstance();
        instance.set(Calendar.MINUTE, 0);
        instance.set(Calendar.MILLISECOND, 0);
        instance.set(Calendar.HOUR, 0);
        instance.set(Calendar.SECOND, 0);
        return instance.getTime().getTime();
    }

    public void setMaxAlertMessagesPerDay(int maxAlertMessagesPerDay) {
        this.maxAlertMessagesPerDay = maxAlertMessagesPerDay;
    }

    public void setCurrentNoOfAlertMessages(int currentNoOfAlertMessages) {
        this.currentNoOfAlertMessages = currentNoOfAlertMessages;
    }

    public int getCurrentNoOfAlertMessages() {
        return currentNoOfAlertMessages;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("AlertMsgsPerDay [");
        sb.append("lastUpdatedDate=").append(lastUpdatedDate);
        sb.append(", currentNoOfAlertMessages=").append(currentNoOfAlertMessages);
        sb.append(", maxAlertMessagesPerDay=").append(maxAlertMessagesPerDay);
        sb.append("]");
        return sb.toString();
    }
}
