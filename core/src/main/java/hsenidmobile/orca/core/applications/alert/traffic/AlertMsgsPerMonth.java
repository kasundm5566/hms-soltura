/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hsenidmobile.orca.core.applications.alert.traffic;

import hsenidmobile.orca.core.flow.exception.MessageThroughputExceededException;
import org.joda.time.DateTime;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Transient;
import java.util.Calendar;

import static hsenidmobile.orca.core.flow.MessageFlowErrorReasons.MONTH_EXCEEDED;

/**
 * $LastChangedDate:$
 * $LastChangedBy:$
 * $LastChangedRevision:$
 */
@Embeddable
public class AlertMsgsPerMonth {

    @Column (name = "cmpm")
    private int currentNoOfAlertMessages;
    @Column (name = "cmpm_nssd")
    private long nextSendingStartDate = 0;
    @Transient
    private int maxAlertMessagesPerMonth = 0;
    private static final int NUMBER_OF_DAYS = 30;

    public AlertMsgsPerMonth() {
    }

    public AlertMsgsPerMonth(int maxAlertMessagesPerMonth) {
        this.maxAlertMessagesPerMonth = maxAlertMessagesPerMonth;
        setNextMessageSendingStartDate();
    }

    private void setNextMessageSendingStartDate() {
        Calendar calendar = getCurrentCalanderDate();
        calendar.add(Calendar.DATE, NUMBER_OF_DAYS);
        nextSendingStartDate = calendar.getTime().getTime();
    }

    public synchronized void verifyThroughputLimit() throws MessageThroughputExceededException {
        long currentDate = getCurrentDate();
        if (maxAlertMessagesPerMonth <= 0) {
            return;
        }
        if (currentDate > nextSendingStartDate) {
            DateTime date = new DateTime(nextSendingStartDate);
            nextSendingStartDate = date.plusDays(NUMBER_OF_DAYS).getMillis();
            currentNoOfAlertMessages = 0;
        }
        int tmp = currentNoOfAlertMessages + 1;
        if (tmp > maxAlertMessagesPerMonth) {
            throw new MessageThroughputExceededException("Maximum alert messages per month limit exceeded", MONTH_EXCEEDED);
        }
    }

    private long getCurrentDate() {
        final Calendar instance = getCurrentCalanderDate();
        return instance.getTime().getTime();
    }

    private Calendar getCurrentCalanderDate() {
        final Calendar instance = Calendar.getInstance();
        instance.set(Calendar.MINUTE, 0);
        instance.set(Calendar.MILLISECOND, 0);
        instance.set(Calendar.HOUR, 0);
        instance.set(Calendar.SECOND, 0);
        return instance;
    }

    public void setCurrentNoOfAlertMessages(int currentNoOfAlertMessages) {
        this.currentNoOfAlertMessages = currentNoOfAlertMessages;
    }

    public void setMaxAlertMessagesPerMonth(int maxAlertMessagesPerMonth) {
        this.maxAlertMessagesPerMonth = maxAlertMessagesPerMonth;
    }

    public int getCurrentNoOfAlertMessages() {
        return currentNoOfAlertMessages;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("AlertMsgsPerMonth");
        sb.append(", currentNoOfAlertMessages=").append(currentNoOfAlertMessages);
        sb.append(", nextSendingStartDate=").append(nextSendingStartDate);
        sb.append(", maxAlertMessagesPerMonth=").append(maxAlertMessagesPerMonth);
        sb.append(']');
        return sb.toString();
    }
}
