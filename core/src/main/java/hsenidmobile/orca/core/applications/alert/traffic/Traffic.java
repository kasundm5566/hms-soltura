/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hsenidmobile.orca.core.applications.alert.traffic;

import hsenidmobile.orca.core.flow.exception.MessageThroughputExceededException;
import hsenidmobile.orca.core.repository.PersistentObject;

import javax.persistence.Embedded;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

/**
 * $LastChangedDate:$
 * $LastChangedBy:$
 * $LastChangedRevision:$
 */
@MappedSuperclass
public abstract class Traffic extends PersistentObject {

    @Embedded
    private AlertMsgsPerDay currentAlertMsgsPerDay;
    @Embedded
    private AlertMsgsPerMonth currentAlertMsgsPerMonth;

    public Traffic() {
        currentAlertMsgsPerDay = new AlertMsgsPerDay(-1);
        currentAlertMsgsPerMonth = new AlertMsgsPerMonth(-1);
    }

    public void verifyThroughput(int mapd, int mapm) throws MessageThroughputExceededException {
        currentAlertMsgsPerMonth.setMaxAlertMessagesPerMonth(mapm);
        currentAlertMsgsPerMonth.verifyThroughputLimit();
        currentAlertMsgsPerDay.setMaxAlertMessagesPerDay(mapd);
        currentAlertMsgsPerDay.verifyThroughputLimit();
    }

    public AlertMsgsPerDay getCurrentAlertMsgsPerDay() {
        return currentAlertMsgsPerDay;
    }

    public AlertMsgsPerMonth getCurrentAlertMsgsPerMonth() {
        return currentAlertMsgsPerMonth;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Traffic [");
        sb.append("currentAlertMsgsPerDay=").append(currentAlertMsgsPerDay);
        sb.append(", currentAlertMsgsPerMonth=").append(currentAlertMsgsPerMonth);
        sb.append(']');
        return sb.toString();
    }
}
