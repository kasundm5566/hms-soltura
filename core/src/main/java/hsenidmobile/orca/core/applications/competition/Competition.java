/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.applications.competition;

import hsenidmobile.orca.core.applications.AbstractService;
import hsenidmobile.orca.core.applications.exception.CompetitionException;
import hsenidmobile.orca.core.applications.subscription.Periodicity;
import hsenidmobile.orca.core.flow.MessageFlowErrorReasons;
import hsenidmobile.orca.core.flow.MessageFlowException;
import hsenidmobile.orca.core.flow.event.CommandEvent;
import hsenidmobile.orca.core.model.Application;
import hsenidmobile.orca.core.model.Status;
import hsenidmobile.orca.core.model.message.Message;
import hsenidmobile.orca.core.model.message.MoResponse;

import java.util.ArrayList;
import java.util.Locale;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Entity
@Table(name = "competition")
public class Competition extends AbstractService {

	public Competition() {
		super(logger);
	}

	@Transient
	private static final Logger logger = LoggerFactory.getLogger(Competition.class);

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "id")
	@LazyCollection(LazyCollectionOption.FALSE)
	private java.util.List<Question> question = new ArrayList<Question>();

	public MoResponse onDataMessage(CommandEvent<Message> commandEvent) throws CompetitionException,
			MessageFlowException {
		Question question = getActiveQuestion();
		return question.answerReceived(commandEvent.getMessage());
	}

	public MoResponse onServiceDataUpdateMessage(CommandEvent<Message> commandEvent) {
		return null; // To change body of implemented methods use File |
						// Settings | File Templates.
	}

	@Override
	public boolean isSubscriptionRequired() {
		return true;
	}

	private Question getActiveQuestion() throws CompetitionException {

		if (question == null || question.size() == 0) {
			throw new CompetitionException("No Question Found", MessageFlowErrorReasons.NO_QUESTION_FOUND);
		}
		for (Question question : this.question) {
			if (logger.isDebugEnabled()) {
				logger.debug(new StringBuilder().append("questionId [").append(question.getQuestionId())
						.append("] questionStatus [").append(question.getStatus()).append("] questionEndDate [")
						.append(question.getEndDate()).toString());
			}
			if ((question.getStatus() == Status.ACTIVE) && question.getEndDate() > System.currentTimeMillis()) {
				return question;
			}
		}
		throw new CompetitionException("No Active Question Found", MessageFlowErrorReasons.NO_ACTIVE_QUESTION_FOUND);
	}

	public java.util.List<Question> getQuestion() {
		return question;
	}

	public void setQuestion(ArrayList<Question> question) {
		this.question = question;
	}

	//
	// @Override
	// public Message getFirstMessageOnRegistration(RegistrationInfo
	// registrationInfo) {
	// return null;
	// }

	@Override
	public void setApplication(Application application) {
		// TODO Auto-generated method stub

	}

}