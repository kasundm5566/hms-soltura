/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.applications.competition;

import hsenidmobile.orca.core.applications.exception.ApplicationException;
import hsenidmobile.orca.core.applications.exception.CompetitionException;
import hsenidmobile.orca.core.model.Answer;
import hsenidmobile.orca.core.model.Status;
import hsenidmobile.orca.core.model.message.Message;
import hsenidmobile.orca.core.model.message.MoResponse;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name = "question")
public class Question {


	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	private String questionId;
	private long startDate;
	private long endDate;

	@Enumerated(EnumType.STRING)
	private Status status;

	@OneToMany(cascade = CascadeType.ALL)
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<Answer> answers;

	private String question;

	public MoResponse answerReceived(Message message) throws CompetitionException, ApplicationException {
//		final Answer answer = getAnswerDetails(message);
//		return addNewResponse(answer, message);
		throw new UnsupportedOperationException("Not yet implemented.");
	}

//	private MoResponse addNewResponse(KeywordDetail keywordDetails, Message message) {
//		// final Response response = new Response();
//		// response.setMsisdn(msisdn);
//		// keywordDetails.getReceivedResponses().add(response);
//		return null;
//	}
//
//	private Answer getAnswerDetails(Message message) throws CompetitionException, ApplicationException {
//		if (logger.isDebugEnabled()) {
//			logger.info("Validating message [" + message.getMessage() + "]");
//		}
//		for (Answer answer : answers) {
//			if (message.getMessage().equals(answer.getKeyword())) {
//				logger.debug("Match found for answer [" + answer.getDescription(Locale.ENGLISH) + "]");
//				return answer;
//			} else if (answer.getKeywordAliases().contains(message.getMessage())) {
//				logger.debug("Aliases match found for answer [" + answer.getDescription(Locale.ENGLISH) + "]");
//				return answer;
//			}
//		}
//		if (logger.isDebugEnabled()) {
//			logger.debug("No Match Found for message [" + message.getMessage() + "]");
//		}
//		throw new ApplicationException("Invalid request",
//				MessageFlowErrorReasons.INVALID_REQUEST);
//	}

	public String getQuestionId() {
		return questionId;
	}

	public void setQuestionId(String questionId) {
		this.questionId = questionId;
	}

	public long getStartDate() {
		return startDate;
	}

	public void setStartDate(long startDate) {
		this.startDate = startDate;
	}

	public long getEndDate() {
		return endDate;
	}

	public void setEndDate(long endDate) {
		this.endDate = endDate;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public List<Answer> getAnswers() {
		return answers;
	}

	public void setAnswers(List<Answer> answers) {
		this.answers = answers;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

}