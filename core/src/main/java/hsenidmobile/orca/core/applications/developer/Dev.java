/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.applications.developer;

import hsenidmobile.orca.core.applications.AbstractService;
import hsenidmobile.orca.core.applications.subscription.Periodicity;
import hsenidmobile.orca.core.flow.MessageFlowException;
import hsenidmobile.orca.core.flow.event.CommandEvent;
import hsenidmobile.orca.core.model.Application;
import hsenidmobile.orca.core.model.Msisdn;
import hsenidmobile.orca.core.model.message.Message;
import hsenidmobile.orca.core.model.message.MessageContext;
import hsenidmobile.orca.core.model.message.MoResponse;
import hsenidmobile.orca.core.model.message.RegistrationResponse;

import java.net.URI;
import java.util.Locale;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Entity
@Table(name = "developer")
public class Dev extends AbstractService {

    @Column(name = "mo_url")
    private URI moUrl;

    @Column(name = "ws_password")
    private String wsPassword;

    @Transient
    private static final Logger logger = LoggerFactory.getLogger(Dev.class);

    public Dev() {
        super(logger);
    }

	@Override
	public boolean isSubscriptionRequired() {
		return false;
	}

    public MoResponse onDataMessage(CommandEvent<Message> commandEvent) throws MessageFlowException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public MoResponse onServiceDataUpdateMessage(CommandEvent<Message> commandEvent) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public MoResponse updateServiceData(Message message, MessageContext messageContext) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public URI getMoUrl() {
        return moUrl;
    }

    public void setMoUrl(URI moUrl) {
        this.moUrl = moUrl;
    }

	public RegistrationResponse userSubscribe(Msisdn msisdn, MessageContext messageContext, String messageDataText, Locale preferedLanguage) {
		// TODO Auto-generated method stub
		return null;
	}

	public MoResponse userUnsubscribed(Msisdn msisdn, MessageContext messageContext) {
		// TODO Auto-generated method stub
		return null;
	}

	public String getWsPassword() {
		return wsPassword;
	}

	public void setWsPassword(String wsPassword) {
		this.wsPassword = wsPassword;
	}

//	@Override
//	public Message getFirstMessageOnRegistration(RegistrationInfo registrationInfo) {
//		// TODO Auto-generated method stub
//		return null;
//	}

	@Override
	public void setApplication(Application application) {
		// TODO Auto-generated method stub

	}

}