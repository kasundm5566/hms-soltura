/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hsenidmobile.orca.core.applications.exception;

import hsenidmobile.orca.core.flow.MessageFlowErrorReasons;
import hsenidmobile.orca.core.flow.MessageFlowException;
import hsenidmobile.orca.core.flow.event.MessageEvent;


/**
 * $LastChangedDate: $ $LastChangedBy: $ $LastChangedRevision: $
 */

public class ApplicationException extends MessageFlowException {

	public ApplicationException(String message, MessageFlowErrorReasons reason, MessageEvent messageFlowEvent) {
		super(message, reason, messageFlowEvent);
	}

	public ApplicationException(String message, MessageFlowErrorReasons reason) {
		super(message, reason);
	}

}
