/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$ 
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.applications.policy;

import hsenidmobile.orca.core.applications.ServicePolicy;
import hsenidmobile.orca.core.model.message.Message;
import hsenidmobile.orca.core.model.message.MessageContext;

/**
 * Define Subscription mode used by the application. It can be;
 * NONE - No subscription required to use the service.
 * OPEN - Any user can subscribe to service and then use the service.
 * CLOSED - Only set of Msisdns defined by the CP can subscribe to the service. In this mode CP has to provide list of subscriber when creating the application.
 */
public enum RegistrationPolicy {

    NONE,OPEN,CLOSED;
    private RegistrationPolicy policyType;
	private ServicePolicy nextPolicy;

	public void enforce(MessageContext messageContext, Message message) {
		throw new UnsupportedOperationException();
	}

	public void setNextPolicy(ServicePolicy nextPolicy) {
		this.nextPolicy = nextPolicy;
	}

	public ServicePolicy getNextPolicy() {
		return this.nextPolicy;
	}

    public RegistrationPolicy getPolicyType() {
        return policyType;
    }

    public void setPolicyType(RegistrationPolicy policyType) {
        this.policyType = policyType;
    }
}