/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$ 
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.applications.policy;

import hsenidmobile.orca.core.applications.ServicePolicy;
import hsenidmobile.orca.core.model.message.Message;
import hsenidmobile.orca.core.model.message.MessageContext;
import hsenidmobile.orca.core.repository.AppRepository;
import org.hibernate.annotations.Any;
import org.hibernate.annotations.AnyMetaDef;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.MetaValue;

import javax.persistence.*;

/**
 * Check whether the received MSISDN is a Subscriber of given application.
 */
@Entity
@Table(name = "subscriber_validation_policy")
public class SubscriberValidationPolicy implements ServicePolicy {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;


    @Any(metaColumn = @Column( name = "next_policy" ), fetch= FetchType.LAZY)
    @AnyMetaDef(
            idType = "long",
            metaType = "string",
            metaValues = {
                    @MetaValue( value = "N_MESSAGES_PER_PERIOD_POLICY", targetEntity = NMessagesPerNPeriodPolicy.class ),
                    @MetaValue( value = "AUTHOR_VALIDATION_POLICY", targetEntity = AuthorValidationPolicy.class ),
                    @MetaValue( value = "SUBSCRIBER_VALIDATION_POLICY", targetEntity = SubscriberValidationPolicy.class )
            })
    @JoinColumn( name = "data_message_policies_id" )
    @Cascade(value = org.hibernate.annotations.CascadeType.ALL)
    private ServicePolicy nextPolicy;

    public long getId() {        
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Transient
    AppRepository appRepository;

	public void enforce(MessageContext messageContext, Message message) {
        messageContext.getApplication().getEndDate();

	}

	public void setNextPolicy(ServicePolicy nextPolicy) {
		this.nextPolicy = nextPolicy;
	}

	public ServicePolicy getNextPolicy() {
		return this.nextPolicy;
	}

    public void setAppRepository(AppRepository appRepository) {
        this.appRepository = appRepository;
    }
}