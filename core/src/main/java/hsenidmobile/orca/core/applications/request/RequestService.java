/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hsenidmobile.orca.core.applications.request;

import hsenidmobile.orca.core.applications.AbstractService;
import hsenidmobile.orca.core.applications.exception.ApplicationException;
import hsenidmobile.orca.core.applications.exception.RequestServiceException;
import hsenidmobile.orca.core.flow.MessageFlowErrorReasons;
import hsenidmobile.orca.core.flow.MessageFlowException;
import hsenidmobile.orca.core.flow.dispatch.aventuraapi.AventuraApiMtMessageDispatcher;
import hsenidmobile.orca.core.flow.event.CommandEvent;
import hsenidmobile.orca.core.flow.event.MessageFlowEvent;
import hsenidmobile.orca.core.model.*;
import hsenidmobile.orca.core.model.message.AventuraSmsMessage;
import hsenidmobile.orca.core.model.message.Message;
import hsenidmobile.orca.core.model.message.MoResponse;
import hsenidmobile.orca.core.model.message.RequestShowReplyRequest;
import hsenidmobile.orca.core.repository.RequestServiceDataRepository;
import hsenidmobile.orca.core.services.MoResponseValidationService;
import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;
import org.joda.time.DateTimeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import static hsenidmobile.orca.core.util.eventlog.EventLogger.createEventLog;

/**
 * $LastChangedDate$
 * $LastChangedBy$ $LastChangedRevision$
 */
@Entity
@Table(name = "request_service")
@Proxy(lazy = false)
public class RequestService extends AbstractService {

    @Transient
    private static final Logger logger = LoggerFactory.getLogger(RequestService.class);

    @Transient
    private Application application;

    @Transient
    private RequestServiceDataRepository requestServiceDataRepository;

    @Transient
    private AventuraApiMtMessageDispatcher aventuraApiMtMessageDispatcher;

    @OneToMany(fetch = FetchType.EAGER)
    @LazyCollection(LazyCollectionOption.FALSE)
    @Cascade(value = {CascadeType.ALL, CascadeType.DELETE_ORPHAN})
    private List<LocalizedMessage> responseMessages;

    @Column(name = "common_response_available", nullable = false)
    private boolean commonResponseAvailable;

    @Transient
    private MoResponseValidationService moResponseValidationService ;

    private static final String SUCCESS__CODE = "S1000";
    private static final String STATUS_CODE_KEY = "statusCode";

    public RequestService() {
        super(logger);
    }

	@Override
	public boolean isSubscriptionRequired() {
		return false;
	}

    @Override
    public MoResponse onDataMessage(CommandEvent<Message> commandEvent) throws MessageFlowException {
        logger.info("Show Request message received sending");
        RequestServiceData requestServiceData = saveRequest(commandEvent);
        if (commonResponseAvailable) {
            sendResponseMessage(commandEvent, requestServiceData);
        }
        return null;
    }

    private void sendResponseMessage(CommandEvent<Message> commandEvent, RequestServiceData requestServiceData)
            throws ApplicationException {
        String responseMessage = getResponseMessage(commandEvent.getLanguage());
        if (responseMessage != null && responseMessage.length() > 0) {
            AventuraSmsMessage message = new AventuraSmsMessage();
            message.setCorrelationId(commandEvent.getMessage().getCorrelationId());
            String senderAddress = commandEvent.getMessage().getSenderAddress().getAddress();
            message.setKeyword(senderAddress);
            message.setMessage(responseMessage);
            message.setSenderAddress(new Msisdn(commandEvent.getMessageContext().getApplication().getAppName(), "ANY"));
            createEventLog(application, null, "", Event.CONTENT_RECEIVED, "application " + application.getAppId() + ""
                    + " received a request ");
            Future future = aventuraApiMtMessageDispatcher.dispatchIndividualMessages(application.getAppId(),
                    Arrays.asList(message));
            if(future != null) {
                handleInvalidRequestResponse(requestServiceData, commandEvent, message, future);
            } else {
                logger.debug("Null Response Received for the message [{}] sent to the application [{}] " +
                        "from the MSISDN [{}]", new Object[] {message, application.getAppName(), senderAddress});
            }
        } else {
            logger.info("No response to send for App[{}]", application.getAppId());
        }
    }

    public void sendSingleReply(MessageFlowEvent<RequestShowReplyRequest> messageFlowEvent)
            throws ApplicationException {
        String responseMessage = messageFlowEvent.getMessage().getMessageContent();
        if (responseMessage != null && responseMessage.length() > 0) {
            AventuraSmsMessage message = new AventuraSmsMessage();
            String senderAddress = messageFlowEvent.getMessage().getRecepient();
            message.setKeyword(senderAddress);
            message.setMessage(responseMessage);
            message.setSenderAddress(new Msisdn(messageFlowEvent.getMessageContext().getApplication().getAppName(), "ANY"));
            createEventLog(application, null, "", Event.CONTENT_SEND, "application " + application.getAppId() + ""
                    + " sending the request show reply ");
            Future future = aventuraApiMtMessageDispatcher.dispatchIndividualMessages(application.getAppId(),
                    Arrays.asList(message));
            if (future != null){
                updateIsReplied(messageFlowEvent.getMessage().getMessageId());
            }
        } else {
            logger.info("No response to send for App[{}]", application.getAppId());
        }
    }

    private void updateIsReplied(Long messageId) {
        try {
            requestServiceDataRepository.updateIsReplied(messageId);
        } catch (RequestServiceException e) {
            logger.error("unable to update the request service data",e);
        }
    }

    private void handleInvalidRequestResponse(RequestServiceData requestServiceData, CommandEvent<Message> commandEvent,
                                              AventuraSmsMessage message, Future future) throws ApplicationException {
        try {
            Map<String, Object> futureStatusMap = (Map<String, Object>) future.get();

            String statusCode = (String) futureStatusMap.get(STATUS_CODE_KEY);
            logger.debug("Response from the Future: [{}]", futureStatusMap);
            logger.debug("moResponseValidationService " + moResponseValidationService);
            if(!moResponseValidationService.isMoCanBeChargedUsingMtCharging(statusCode)) {
                logger.debug("Handling charging fail and rollback scenarios");
                try {
                    logger.debug("Rollback Operation for Request Service ID [{}] and Request Service Message ID" +
                            " [{}] Started..",
                            new Object[] {getId(), requestServiceData.getMessageId()});
                    requestServiceDataRepository.rollBackReceivedRequestMessage(requestServiceData.getMessageId());
                    logger.debug("Rollback Operation for Request Service ID [{}] and Request Service Message ID" +
                            " [{}] Finished !!",
                            new Object[] {getId(), requestServiceData.getMessageId()});
                    handleInvalidRequestMessageReceivedResponse(commandEvent);
                } catch (ApplicationException e) {
                    throw e;
                }
                catch (Exception e) {
                    logger.warn("Error occurred while performing the rollback operation for the Request Service" +
                            " ID [{}] and Request Service Message ID [{}] ", new Object[] {getId(),
                            requestServiceData.getMessageId()}, e);
                }
            } else {
                logger.debug("MO can be chargeable using MT");
            }
        } catch (InterruptedException e) {
            logger.error("InterruptedException occurred while sending Response for the message [{}]", message);
        } catch (ExecutionException e) {
            logger.error("ExecutionException occurred while sending Request Show Response for the message [{}]", message,
                    e);
        }
    }

    private void handleInvalidRequestMessageReceivedResponse(CommandEvent<Message> commandEvent)
            throws ApplicationException {
        ApplicationException ex = new ApplicationException("Subscriber[" +
                commandEvent.getMessage().getSenderAddress()
                + "] does not have enough credits to send messages to the application [" +
                this.application.getAppId() + "]",
                MessageFlowErrorReasons.INVALID_REQUEST_MESSAGE);
        throw ex;
    }

    @Override
    public MoResponse onServiceDataUpdateMessage(CommandEvent<Message> commandEvent) throws OrcaException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setApplication(Application application) {
        this.application = application;
    }

    private RequestServiceData saveRequest(CommandEvent<Message> commandEvent) throws ApplicationException,
            RequestServiceException {

        SubCategory subCategory = getSubcategory(commandEvent.getSubKeyword());

        Message message = commandEvent.getMessage();
        RequestServiceData requestServiceData = generateRequestServiceData(message, subCategory);
        requestServiceDataRepository.add(requestServiceData);
        return requestServiceData;
    }

    private String getResponseMessage(Locale language) {
        if (commonResponseAvailable) {
            if (responseMessages != null) {
                for (LocalizedMessage resp : responseMessages) {
                    if (resp.getLocale().equals(language)) {
                        return resp.getMessage();
                    }
                }
            }
        }
        logger.warn("No  Response Message Configured for locale[{}] for application ID [{}]", language, application.getAppId());
        return null;
    }

    private RequestServiceData generateRequestServiceData(Message message, SubCategory subCategory) {
        RequestServiceData requestServiceData = new RequestServiceData();
        requestServiceData.setMsisdn(message.getSenderAddress());
        requestServiceData.setReceivedMessage(message.getMessage());
        requestServiceData.setReceivedTime(DateTimeUtils.currentTimeMillis());
        requestServiceData.setSubCategory(subCategory);
        requestServiceData.setAppId(application.getAppId());
        requestServiceData.setAppName(application.getAppName());
        requestServiceData.setMessageId(message.getCorrelationId());
        requestServiceData.setReplied(false);
        logger.debug("Adding new Request which was received from MSISDN [{}]", requestServiceData.getMsisdn());
        return requestServiceData;
    }

    public void setRequestServiceDataRepository(RequestServiceDataRepository requestServiceDataRepository) {
        this.requestServiceDataRepository = requestServiceDataRepository;
    }

    public void setAventuraApiMtMessageDispatcher(AventuraApiMtMessageDispatcher aventuraApiMtMessageDispatcher) {
        this.aventuraApiMtMessageDispatcher = aventuraApiMtMessageDispatcher;
    }

    public List<LocalizedMessage> getResponseMessages() {
        return responseMessages;
    }

    public void setResponseMessages(List<LocalizedMessage> responseMessages) {
        this.responseMessages = responseMessages;
    }

    public boolean isCommonResponseAvailable() {
        return commonResponseAvailable;
    }

    public void setCommonResponseAvailable(boolean commonResponseAvailable) {
        this.commonResponseAvailable = commonResponseAvailable;
    }

    public void setMoResponseValidationService(MoResponseValidationService moResponseValidationService) {
        this.moResponseValidationService = moResponseValidationService;
    }
}