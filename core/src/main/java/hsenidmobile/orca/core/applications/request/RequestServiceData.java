/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hsenidmobile.orca.core.applications.request;

import hsenidmobile.orca.core.model.Response;
import hsenidmobile.orca.core.model.SubCategory;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * $LastChangedDate$
 * $LastChangedBy$ $LastChangedRevision$
 */
@Entity
@Table(name = "request_service_data")
public class RequestServiceData extends Response {

	private long messageId;

	private String appId;

	private String appName;

	private boolean isRead;

    @Column(name = "is_replied")
    private boolean isReplied;

	@Transient
	private String subKeyword;

    @Transient
    private boolean commonResponseAvailable;

	@ManyToOne(fetch=FetchType.EAGER, optional=true)
	@JoinColumn(name = "sub_category")
	private SubCategory subCategory;

	public long getMessageId() {
		return messageId;
	}

	public void setMessageId(long messageId) {
		this.messageId = messageId;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

    public boolean isReplied() {
        return isReplied;
    }

    public void setReplied(boolean replied) {
        isReplied = replied;
    }

    public boolean isRead() {
		return isRead;
	}

	public SubCategory getSubCategory() {
		return subCategory;
	}

	public void setSubCategory(SubCategory subCategory) {
		this.subCategory = subCategory;
	}

	public void setRead(boolean read) {
		isRead = read;
	}

	public String getSubKeyword() {
		return subKeyword;
	}

	public void setSubKeyword(String subKeyword) {
		this.subKeyword = subKeyword;
	}

    public boolean isCommonResponseAvailable() {
        return commonResponseAvailable;
    }

    public void setCommonResponseAvailable(boolean commonResponseAvailable) {
        this.commonResponseAvailable = commonResponseAvailable;
    }
}
