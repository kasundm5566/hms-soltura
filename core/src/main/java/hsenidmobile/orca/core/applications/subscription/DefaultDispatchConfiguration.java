/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.applications.subscription;

import hsenidmobile.orca.core.applications.PeriodUnit;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.joda.time.DateTime;
import org.joda.time.DateTimeFieldType;
import org.joda.time.MutableDateTime;

/**
 * $LastChangedDate$
 * $LastChangedBy$ $LastChangedRevision$
 *
 */
@Embeddable
public class DefaultDispatchConfiguration {

	@Column(name = "default_dispatch_minute_of_hour")
	private int minuteOfHour;

	@Column(name = "default_dispatch_hour_of_day")
	private int hourOfDay;

	@Column(name = "default_dispatch_day_of_week")
	private int dayOfWeek;

	@Column(name = "default_dispatch_day_of_month")
	private int dayOfMonth;

	@Column(name = "default_dispatch_last_day_of_month")
	private boolean useLastDayOfMonth;

	private DefaultDispatchConfiguration() {

	}

	public int getMinuteOfHour() {
		return minuteOfHour;
	}

	public int getHourOfDay() {
		return hourOfDay;
	}

	public int getDayOfWeek() {
		return dayOfWeek;
	}

	public int getDayOfMonth() {
		return dayOfMonth;
	}

	public boolean isUseLastDayOfMonth() {
		return useLastDayOfMonth;
	}

	private void setDayOfMonth(int dayOfMonth) {
		this.dayOfMonth = dayOfMonth;
	}

	private void setDayOfWeek(int dayOfWeek) {
		this.dayOfWeek = dayOfWeek;
	}

	private void setHourOfDay(int hourOfDay) {
		this.hourOfDay = hourOfDay;
	}

	private void setMinuteOfHour(int minuteOfHour) {
		this.minuteOfHour = minuteOfHour;
	}

	private void setUseLastDayOfMonth(boolean useLastDayOfMonth) {
		this.useLastDayOfMonth = useLastDayOfMonth;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(200);
		builder.append("DefaultDispatchConfiguration [minuteOfHour=");
		builder.append(minuteOfHour);
		builder.append(", hourOfDay=");
		builder.append(hourOfDay);
		builder.append(", dayOfWeek=");
		builder.append(dayOfWeek);
		builder.append(", dayOfMonth=");
		builder.append(dayOfMonth);
		builder.append(", useLastDayOfMonth=");
		builder.append(useLastDayOfMonth);
		builder.append("]");
		return builder.toString();
	}

	public static DefaultDispatchConfiguration configureForHour(int minuteOfHour) {
		DefaultDispatchConfiguration configuration = new DefaultDispatchConfiguration();
		configuration.setMinuteOfHour(minuteOfHour);
		return configuration;
	}

	public static DefaultDispatchConfiguration configureForWeek(int dayOfWeek, int hourOfDay, int minuteOfHour) {
		DefaultDispatchConfiguration configuration = new DefaultDispatchConfiguration();
		configuration.setDayOfWeek(dayOfWeek);
		configuration.setHourOfDay(hourOfDay);
		configuration.setMinuteOfHour(minuteOfHour);
		return configuration;
	}

	public static DefaultDispatchConfiguration configureForLastDayOfMonth(int hourOfDay, int minuteOfHour) {
		DefaultDispatchConfiguration configuration = new DefaultDispatchConfiguration();
		configuration.setUseLastDayOfMonth(true);
		configuration.setHourOfDay(hourOfDay);
		configuration.setMinuteOfHour(minuteOfHour);
		return configuration;
	}

	/**
	 * This is special builder for UI Only. Even though 'dayOfMonth' is saved to
	 * DB it is not being used for any internal calculation.
	 *
	 * @param dayOfMonth
	 * @param hourOfDay
	 * @param minuteOfHour
	 * @return
	 */
	public static DefaultDispatchConfiguration configureForLastDayOfMonth(int dayOfMonth, int hourOfDay,
			int minuteOfHour) {
		DefaultDispatchConfiguration configuration = new DefaultDispatchConfiguration();
		configuration.setUseLastDayOfMonth(true);
		configuration.setDayOfMonth(dayOfMonth);
		configuration.setHourOfDay(hourOfDay);
		configuration.setMinuteOfHour(minuteOfHour);
		return configuration;
	}

	public static DefaultDispatchConfiguration configureForDayOfMonth(int dayOfMonth, int hourOfDay, int minuteOfHour) {
		DefaultDispatchConfiguration configuration = new DefaultDispatchConfiguration();
		configuration.setDayOfMonth(dayOfMonth);
		configuration.setHourOfDay(hourOfDay);
		configuration.setMinuteOfHour(minuteOfHour);
		configuration.setUseLastDayOfMonth(false);
		return configuration;
	}

	public static DefaultDispatchConfiguration configureForDay(int hourOfDay, int minuteOfHour) {
		DefaultDispatchConfiguration configuration = new DefaultDispatchConfiguration();
		configuration.setHourOfDay(hourOfDay);
		configuration.setMinuteOfHour(minuteOfHour);
		return configuration;
	}

	/**
	 * Check whether given time matches with the dispatch configuration.
	 *
	 * @param time
	 *            -
	 * @param periodUnit
	 *            -
	 * @return
	 */
	public boolean isMatchWithConfig(PeriodUnit periodUnit, DateTime time) {
		switch (periodUnit) {
		case HOUR:
			return time.getMinuteOfHour() == minuteOfHour;
		case DAY:
			return time.getMinuteOfHour() == minuteOfHour && time.getHourOfDay() == hourOfDay;
		case WEEK:
			return time.getMinuteOfHour() == minuteOfHour && time.getHourOfDay() == hourOfDay
					&& time.getDayOfWeek() == dayOfWeek;
		case MONTH:
			if (useLastDayOfMonth) {
				return time.dayOfMonth().getMaximumValue() == time.getDayOfMonth();
			} else {
				return time.getMinuteOfHour() == minuteOfHour && time.getHourOfDay() == hourOfDay
						&& time.getDayOfMonth() == dayOfMonth;
			}
		default:
			throw new IllegalStateException("PeriodUnit not defined.");
		}

	}

	/**
	 * Adjust given time to match with dispatch configuration
	 *
	 * @param unit
	 * @param originalDateTime
	 * @return
	 */
	public DateTime adjustToConfig(PeriodUnit periodUnit, DateTime originalDateTime) {
		MutableDateTime adjustedTime = new MutableDateTime(originalDateTime);
		switch (periodUnit) {
		case HOUR:
			adjustedTime.set(DateTimeFieldType.minuteOfHour(), minuteOfHour);
			break;
		case DAY:
			adjustedTime.set(DateTimeFieldType.minuteOfHour(), minuteOfHour);
			adjustedTime.set(DateTimeFieldType.hourOfDay(), hourOfDay);
			break;
		case WEEK:
			adjustedTime.set(DateTimeFieldType.minuteOfHour(), minuteOfHour);
			adjustedTime.set(DateTimeFieldType.hourOfDay(), hourOfDay);
			adjustedTime.set(DateTimeFieldType.dayOfWeek(), dayOfWeek);
			break;
		case MONTH:
			adjustedTime.set(DateTimeFieldType.minuteOfHour(), minuteOfHour);
			adjustedTime.set(DateTimeFieldType.hourOfDay(), hourOfDay);
			if (useLastDayOfMonth) {
				adjustedTime.set(DateTimeFieldType.dayOfMonth(), originalDateTime.dayOfMonth().getMaximumValue());
			} else {
				if (originalDateTime.dayOfMonth().getMaximumValue() < dayOfMonth) {
					adjustedTime.set(DateTimeFieldType.dayOfMonth(), originalDateTime.dayOfMonth().getMaximumValue());
				} else {
					adjustedTime.set(DateTimeFieldType.dayOfMonth(), dayOfMonth);
				}
			}
			break;
		default:
			throw new IllegalStateException("PeriodUnit not defined.");
		}
		return adjustedTime.toDateTime();
	}

}
