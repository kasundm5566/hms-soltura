/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.applications.subscription;

import hsenidmobile.orca.core.applications.PeriodUnit;
import hsenidmobile.orca.core.repository.PersistentObject;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.joda.time.DateTimeFieldType;
import org.joda.time.DurationFieldType;
import org.joda.time.MutableDateTime;
import org.joda.time.Period;
import org.joda.time.PeriodType;

/**
 * @version $LastChangedRevision$
 */

@Entity
@Table(name = "periodicity")
public class Periodicity extends PersistentObject {

	@Enumerated(EnumType.STRING)
	private PeriodUnit unit;

	@Column(name = "period_value", length = 3)
	private int periodValue;

	/**
	 * BufferTime in minutes
	 */
	@Column(name = "allowed_buffer_time", length = 11)
	private int allowedBufferTime;

	@Embedded
	private DefaultDispatchConfiguration defaultDispatchConfiguration;

	@Column(name = "last_sent_time")
	@Type(type="org.joda.time.contrib.hibernate.PersistentDateTime")
	private DateTime lastSentTime;

	@Column(name = "first_scheduled_dispatch_time", nullable = false)
	@Type(type="org.joda.time.contrib.hibernate.PersistentDateTime")
	private DateTime firstScheduledDispatchTime;

	/**
	 *
	 * @param unit
	 * @param periodValue
	 * @param allowedBufferTime
	 *            - BufferTime in minutes
	 */
	public Periodicity(PeriodUnit unit, int periodValue, int allowedBufferTime) {
		this.unit = unit;
		this.periodValue = periodValue;
		this.allowedBufferTime = allowedBufferTime;
	}

	public Periodicity() {
	}

	public PeriodUnit getUnit() {
		return unit;
	}

	public DateTime getLastSentTime() {
		return lastSentTime;
	}

	public void setLastSentTime(DateTime lastSentTime) {
		this.lastSentTime = lastSentTime;
	}

	public int getPeriodValue() {
		return periodValue;
	}

	public void setPeriodValue(int periodValue) {
		this.periodValue = periodValue;
	}

	public void setUnit(PeriodUnit unit) {
		this.unit = unit;
	}

	/**
	 * @return - BufferTime in minutes
	 */
	public int getAllowedBufferTime() {
		return allowedBufferTime;
	}

	/**
	 *
	 * @param allowedBufferTime
	 *            - BufferTime in minutes
	 */
	public void setAllowedBufferTime(int allowedBufferTime) {
		this.allowedBufferTime = allowedBufferTime;
	}

	public DefaultDispatchConfiguration getDefaultDispatchConfiguration() {
		return defaultDispatchConfiguration;
	}

	public void setDefaultDispatchConfiguration(DefaultDispatchConfiguration defaultDispatchConfiguration) {
		this.defaultDispatchConfiguration = defaultDispatchConfiguration;
	}

	public DateTime getFirstScheduledDispatchTime() {
		return firstScheduledDispatchTime;
	}

	public void setFirstScheduledDispatchTime(DateTime firstScheduledDispatchTime) {
		this.firstScheduledDispatchTime = firstScheduledDispatchTime;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(150);
		builder.append("Periodicity [unit=");
		builder.append(unit);
		builder.append(", value=");
		builder.append(periodValue);
		builder.append(", allowedBufferTime=");
		builder.append(allowedBufferTime);
		builder.append(", firstScheduledDispatchTime=");
		builder.append(firstScheduledDispatchTime);
		builder.append(", lastSentTime=");
		builder.append(lastSentTime);
		builder.append("]");
		return builder.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + (int) (allowedBufferTime ^ (allowedBufferTime >>> 32));
		result = prime * result + ((unit == null) ? 0 : unit.hashCode());
		result = prime * result + periodValue;
		return result;
	}

	/**
	 * Check whether the unit and period value are same.
	 *
	 * @param periodicity
	 * @return
	 */
	public boolean isSamePeriodicity(Periodicity periodicity) {
		return this.getUnit().equals(periodicity.getUnit()) && this.getPeriodValue() == periodicity.getPeriodValue();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Periodicity other = (Periodicity) obj;
		if (allowedBufferTime != other.allowedBufferTime)
			return false;
		if (unit == null) {
			if (other.unit != null)
				return false;
		} else if (!unit.equals(other.unit))
			return false;
		if (periodValue != other.periodValue)
			return false;
		return true;
	}

	/**
	 * Calculate the Date-Time for first content dispatchable time based on
	 * current time.
	 *
	 * @param periodUnit
	 *            -
	 * @param periodValue
	 *            -
	 * @param defaultDispatchConfiguration
	 * @return
	 */
	public static Date getFistDispatchableDate(PeriodUnit periodUnit, int periodValue,
			DefaultDispatchConfiguration defaultDispatchConfiguration) {
		if (periodValue > 1) {
			throw new UnsupportedOperationException("Periodicity with multi variance not yet supported");
		}
		MutableDateTime dispatchTime;
		switch (periodUnit) {
		case HOUR:
			dispatchTime = calculateFirstDispatchDateForHour(defaultDispatchConfiguration);
			return dispatchTime.toDate();
		case DAY:
			dispatchTime = calculateFirstDispatchDateForDay(defaultDispatchConfiguration);
			return dispatchTime.toDate();
		case WEEK:
			dispatchTime = calculateFirstDispatchDateForWeek(defaultDispatchConfiguration);
			return dispatchTime.toDate();
		case MONTH:
			dispatchTime = calculateFirstDispatchDateForMonth(defaultDispatchConfiguration);
			return dispatchTime.toDate();
		default:
			throw new IllegalStateException("PeriodUnit not defined.");
		}
	}

	private static MutableDateTime calculateFirstDispatchDateForMonth(
			DefaultDispatchConfiguration defaultDispatchConfiguration) {
		MutableDateTime now = new MutableDateTime();
		if (defaultDispatchConfiguration.isUseLastDayOfMonth()) {
			now.set(DateTimeFieldType.dayOfMonth(), now.dayOfMonth().getMaximumValue());
		} else {
			if (now.getDayOfMonth() > defaultDispatchConfiguration.getDayOfMonth()) {
				now.addMonths(1);
			}
			int maxDateForThisMonth = now.dayOfMonth().getMaximumValue();
			if (defaultDispatchConfiguration.getDayOfMonth() > maxDateForThisMonth) {
				now.set(DateTimeFieldType.dayOfMonth(), maxDateForThisMonth);
			} else {
				now.set(DateTimeFieldType.dayOfMonth(), defaultDispatchConfiguration.getDayOfMonth());
			}
		}
		now.set(DateTimeFieldType.hourOfDay(), defaultDispatchConfiguration.getHourOfDay());
		now.set(DateTimeFieldType.minuteOfHour(), defaultDispatchConfiguration.getMinuteOfHour());
		resetSeconds(now);
		return now;
	}

	private static MutableDateTime calculateFirstDispatchDateForWeek(
			DefaultDispatchConfiguration defaultDispatchConfiguration) {
		MutableDateTime now = new MutableDateTime();
		if (now.getDayOfWeek() != defaultDispatchConfiguration.getDayOfWeek()) {
			if (now.getDayOfWeek() > defaultDispatchConfiguration.getDayOfWeek()) {
				now.addDays(defaultDispatchConfiguration.getDayOfWeek() - now.getDayOfWeek());
				now.addWeeks(1);
			} else {
				now.addDays(defaultDispatchConfiguration.getDayOfWeek() - now.getDayOfWeek());
			}
		}
		now.set(DateTimeFieldType.hourOfDay(), defaultDispatchConfiguration.getHourOfDay());
		now.set(DateTimeFieldType.minuteOfHour(), defaultDispatchConfiguration.getMinuteOfHour());
		resetSeconds(now);
		return now;
	}

	private static MutableDateTime calculateFirstDispatchDateForDay(
			DefaultDispatchConfiguration defaultDispatchConfiguration) {
		MutableDateTime now = new MutableDateTime();

		if (now.getHourOfDay() >= defaultDispatchConfiguration.getHourOfDay()
				&& now.getMinuteOfHour() >= defaultDispatchConfiguration.getMinuteOfHour()) {
			now.addDays(1);
		}
		now.set(DateTimeFieldType.hourOfDay(), defaultDispatchConfiguration.getHourOfDay());
		now.set(DateTimeFieldType.minuteOfHour(), defaultDispatchConfiguration.getMinuteOfHour());
		resetSeconds(now);
		return now;
	}

	private static MutableDateTime calculateFirstDispatchDateForHour(
			DefaultDispatchConfiguration defaultDispatchConfiguration) {
		MutableDateTime now = new MutableDateTime();

		if (now.getMinuteOfHour() >= defaultDispatchConfiguration.getMinuteOfHour()) {
			now.addHours(1);
		}
		now.set(DateTimeFieldType.minuteOfHour(), defaultDispatchConfiguration.getMinuteOfHour());
		resetSeconds(now);
		return now;
	}

	private static void resetSeconds(MutableDateTime now) {
		now.set(DateTimeFieldType.secondOfMinute(), 0);
		now.set(DateTimeFieldType.millisOfSecond(), 0);
	}

	/**
	 * Calculate and get next scheduled dispatch time based on current time and
	 * fistScheduledDispatchTime.
	 *
	 * @param now
	 * @return
	 */
	public DateTime getNextScheduledDispatchTime(DateTime now) {

		if (firstScheduledDispatchTime == null) {
			throw new IllegalStateException("FirstScheduledDispatchTime not defined.");
		}

		if (periodValue > 1) {
			throw new UnsupportedOperationException("Periodicity with multi variance not yet supported");
		}

		DateTime lastSchedule = new DateTime(firstScheduledDispatchTime);
		DateTime nextSchedule = new DateTime(firstScheduledDispatchTime);
		if (lastSchedule.isAfter(now.getMillis())) {
			return lastSchedule;
		} else {
			switch (unit) {
			case HOUR:
				return calculateForNextScheduleHour(now, lastSchedule, nextSchedule);
			case DAY:
				return calculateForNextScheduleDay(now, lastSchedule, nextSchedule);
			case WEEK:
				return calculateForNextScheduleWeek(now, lastSchedule, nextSchedule);
			case MONTH:
				return calculateForNextScheduleMonth(now, lastSchedule, nextSchedule);
			default:
				throw new IllegalStateException("PeriodUnit not defined.");
			}
		}
	}

	/**
	 * Calculate the next schedule time when last schedule time is given.
	 *
	 * @param lastScheduleTime
	 * @return
	 */
	public Date getNextScheduleTimeBasedOnLastScheduleTime(Date lastScheduleTime) {
		if (periodValue > 1) {
			throw new UnsupportedOperationException("Periodicity with multi variance not yet supported");
		}

		DateTime nextSchedule = null;
		if(defaultDispatchConfiguration.isMatchWithConfig(unit, new DateTime(lastScheduleTime))){
			nextSchedule = new DateTime(lastScheduleTime);
		} else {
			nextSchedule = defaultDispatchConfiguration.adjustToConfig(unit, new DateTime(lastScheduleTime));
		}

		switch (unit) {
		case HOUR:
			return nextSchedule.plusHours(periodValue).toDate();
		case DAY:
			return nextSchedule.plusDays(periodValue).toDate();
		case WEEK:
			return nextSchedule.plusWeeks(periodValue).toDate();
		case MONTH:
			nextSchedule = nextSchedule.plusMonths(periodValue);
			if (defaultDispatchConfiguration.isUseLastDayOfMonth()) {
				return nextSchedule.dayOfMonth().withMaximumValue().toDate();
			} else {
				int maxDaysForMonth = nextSchedule.dayOfMonth().getMaximumValue();
				if (defaultDispatchConfiguration.getDayOfMonth() > nextSchedule.dayOfMonth().get()
						&& defaultDispatchConfiguration.getDayOfMonth() <= maxDaysForMonth) {
					nextSchedule = nextSchedule.plusDays(maxDaysForMonth - nextSchedule.dayOfMonth().get());
				}
				return nextSchedule.toDate();
			}
		default:
			throw new IllegalStateException("PeriodUnit not defined.");
		}
	}

	private DateTime calculateForNextScheduleMonth(DateTime now, DateTime lastSchedule, DateTime nextSchedule) {
		Period period;
		period = new Period(lastSchedule.getMillis(), now.getMillis(), PeriodType.forFields(new DurationFieldType[] {
				DurationFieldType.months(), DurationFieldType.days(), DurationFieldType.hours(),
				DurationFieldType.minutes() }));
		nextSchedule = nextSchedule.plusMonths(periodValue * period.getMonths());
		if(now.isAfter(nextSchedule)){
			nextSchedule = nextSchedule.plusMonths(periodValue * 1);
			if (defaultDispatchConfiguration.isUseLastDayOfMonth()) {
				nextSchedule = nextSchedule.dayOfMonth().withMaximumValue();
			} else {
				int maxDaysForMonth = nextSchedule.dayOfMonth().getMaximumValue();
				if (defaultDispatchConfiguration.getDayOfMonth() > nextSchedule.dayOfMonth().get()
						&& defaultDispatchConfiguration.getDayOfMonth() <= maxDaysForMonth) {
					nextSchedule = nextSchedule.plusDays(maxDaysForMonth - nextSchedule.dayOfMonth().get());
				}
			}
		}
		return nextSchedule;
	}

	private DateTime calculateForNextScheduleWeek(DateTime now, DateTime lastSchedule, DateTime nextSchedule) {
		Period period;
		period = new Period(lastSchedule.getMillis(), now.getMillis(), PeriodType.forFields(new DurationFieldType[] {
				DurationFieldType.weeks(), DurationFieldType.days(), DurationFieldType.hours(),
				DurationFieldType.minutes() }));
		nextSchedule = nextSchedule.plusWeeks(periodValue * period.getWeeks());
		if(now.isAfter(nextSchedule)){
			nextSchedule = nextSchedule.plusWeeks(periodValue * 1);
		}
		return nextSchedule;
	}

	private DateTime calculateForNextScheduleDay(DateTime now, DateTime lastSchedule, DateTime nextSchedule) {
		Period period;
		period = new Period(lastSchedule.getMillis(), now.getMillis(), PeriodType.forFields(new DurationFieldType[] {
				DurationFieldType.days(), DurationFieldType.hours(), DurationFieldType.minutes() }));
		nextSchedule = nextSchedule.plusDays(periodValue * period.getDays());
		if(now.isAfter(nextSchedule)){
			nextSchedule = nextSchedule.plusDays(periodValue * 1);
		}
		return nextSchedule;
	}

	private DateTime calculateForNextScheduleHour(DateTime now, DateTime lastSchedule, DateTime nextSchedule) {
		Period period;
		period = new Period(lastSchedule.getMillis(), now.getMillis(), PeriodType.forFields(new DurationFieldType[] {
				DurationFieldType.hours(), DurationFieldType.minutes() }));
		nextSchedule = nextSchedule.plusHours(periodValue * period.getHours());
		if(now.isAfter(nextSchedule)){
			nextSchedule = nextSchedule.plusHours(periodValue * 1);
		}
		return nextSchedule;
	}
}