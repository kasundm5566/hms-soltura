/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.applications.subscription;

import hsenidmobile.orca.core.applications.subscription.SubscriptionContentRelease.ContentReleaseStatus;
import hsenidmobile.orca.core.flow.AbstractStartUpService;
import hsenidmobile.orca.core.flow.dispatch.aventuraapi.AventuraApiMtMessageDispatcher;
import hsenidmobile.orca.core.model.Content;
import hsenidmobile.orca.core.model.Event;
import hsenidmobile.orca.core.model.MessageHistory;
import hsenidmobile.orca.core.model.Msisdn;
import hsenidmobile.orca.core.model.message.SmsMessage;
import hsenidmobile.orca.core.repository.MessageHistoryRepository;
import hsenidmobile.orca.core.util.CorrelationIdGenerationService;
import hsenidmobile.orca.core.util.PropertyHolder;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

/**
 * $LastChangedDate$
 * $LastChangedBy$ $LastChangedRevision$
 *
 */
public class ScheduledMessageDispatcher extends AbstractStartUpService {

    private static final Logger logger = LoggerFactory.getLogger(ScheduledMessageDispatcher.class);
    private static final String ALL_REGISTERED_KEYWORD = "all_registered";
    private SubscriptionServiceDataRepository subsDataRepository;
    private AventuraApiMtMessageDispatcher aventuraApiMtMessageDispatcher;
    private MessageHistoryRepository messageHistoryRepository;
    private int batchSize = 10;

    private static final String MESSAGE_ID_KEY = "requestId";
    private static final String SUCCESS__CODE = "S1000";
    private static final String STATUS_CODE_KEY = "statusCode";
    private static final String STATUS_DETAILS_KEY = "statusDetail";
    private Map<String, String> temporaryErrorCodes;

    @Override
    public void run() {
        try {
            logger.info("Started Subscription ScheduledMessageDispatcher.");

            while (!Thread.interrupted()) {

                final List<SubscriptionContentRelease> dispatchableContents = subsDataRepository
                        .getDispatchableContents(new DateTime(), batchSize);
                logger.info("Found [{}] scheduled content(s)", dispatchableContents.size());

                if (dispatchableContents.size() == 0) {
                    logger.info("Stopped Subscription ScheduledMessageDispatcher since no more scheduled contents available.");
                    return;
                } else {

                    if (dispatchableContents != null && !dispatchableContents.isEmpty()) {
                        for (SubscriptionContentRelease contentRelease : dispatchableContents) {
                            logger.info("Start dispatching content[{}]", contentRelease);
                            try {
                                final List<SmsMessage> smsMessages = createDispatchableSmsMessageList(contentRelease);
                                if (!smsMessages.isEmpty()) {
                                    try {
                                        Future<Map<String, Object>> future = aventuraApiMtMessageDispatcher.dispatchBroadcastMessage(
                                                contentRelease.getAppId(), smsMessages);

                                        Map<String, Object> response = future.get();

                                        handleResponse(contentRelease, response);

                                    } catch (Throwable th) {
                                        logger.error("Error while dispatching scheduled content " + contentRelease + ", marking as [SKIPPED]", th);
                                        contentRelease.setStatus(ContentReleaseStatus.SKIPPED);
                                    }
                                } else {
                                    logger.info("No content to be dispatched. Mark as sent");
                                    contentRelease.setStatus(ContentReleaseStatus.SENT);
                                }
                                generateEventLog(contentRelease.getAppId(), contentRelease.getAppName(), contentRelease.getStatus());
                                subsDataRepository.updateContentRelease(contentRelease);
                            } catch (Throwable th) {
                                logger.error("Error while dispatching scheduled content[" + contentRelease + "]", th);
                                logger.info("Wait 10 second before retrying");
                                try {
                                    Thread.sleep(10000);
                                } catch (InterruptedException e) {
                                    return;
                                }
                            }
                        }

                    }
                }
            }
            logger.info("Stopped Subscription ScheduledMessageDispatcher.");

        } catch (Throwable th) {
            logger.error("Exception occurred at Subscription ScheduledMessageDispatcher", th);
        }
    }

    private void handleResponse(SubscriptionContentRelease contentRelease, Map<String, Object> response) {
        if (response.get(STATUS_CODE_KEY).equals(SUCCESS__CODE)) {
            contentRelease.setBcMessageId((Long) response.get(MESSAGE_ID_KEY));
            contentRelease.setStatus(ContentReleaseStatus.SENT);
        } else {
            String retryDelayStringForErrorCode = temporaryErrorCodes.get(response.get(STATUS_CODE_KEY));
            if (retryDelayStringForErrorCode == null) {
                logger.error("No retry configured for status-code [{}], discard the content [{}]", response.get(STATUS_CODE_KEY), contentRelease);
                contentRelease.setStatus(ContentReleaseStatus.DISCARDED);
            } else {
                long retryDelay = Long.parseLong(retryDelayStringForErrorCode);
                boolean delayIsOkToHave = contentRelease.getScheduledDispatchTimeTo().getMillis() > System.currentTimeMillis() + retryDelay;
                if (delayIsOkToHave) {
                    contentRelease.setScheduledDispatchTimeFrom(new DateTime(System.currentTimeMillis() + retryDelay));
                    contentRelease.setStatus(ContentReleaseStatus.FUTURE);
                } else {
                    logger.error("Discarding the message retry since the delay of [{}] exceed the content valid time [{}]", retryDelay, contentRelease.getScheduledDispatchTimeTo());
                    contentRelease.setStatus(ContentReleaseStatus.DISCARDED);
                }
            }
        }
    }

    private List<SmsMessage> createDispatchableSmsMessageList(SubscriptionContentRelease subscriptionServiceData) {
        List<SmsMessage> smsMessageList = new ArrayList<SmsMessage>();
        for (Content content : subscriptionServiceData.getContents()) {
            content.setSentTime(new DateTime());
            SmsMessage smsMessage = createSmsMessage(content);
            smsMessage.setSenderAddress(new Msisdn(subscriptionServiceData.getAppName(),"ANY"));
            if (smsMessage != null) {
                smsMessageList.add(smsMessage);
            }
        }

        return smsMessageList;
    }

    private SmsMessage createSmsMessage(Content content) {
        String messageText = content.getContent().getMessage();
        if (messageText != null && messageText.length() > 0) {
            SmsMessage message = new SmsMessage();
            message.setCorrelationId(CorrelationIdGenerationService.getCorrelationId(PropertyHolder.SERVER_ID));
            if (content.getSubCategory() == null) {
                message.setKeyword(ALL_REGISTERED_KEYWORD);
            } else {
                message.setKeyword(content.getSubCategory().getKeyword());
            }
            //todo this has to be handled with the sender default address
            message.setMessage(messageText);
            return message;
        } else {
            logger.debug("Ignore creating sms message for [{}] since message is blank.", content);
            return null;
        }
    }

    private void generateEventLog(String appId, String appName, ContentReleaseStatus status) {
        MessageHistory messageHistory = new MessageHistory();
        messageHistory.setApplicationId(appId);
        messageHistory.setApplicationName(appName);
        messageHistory.setEvent(getEventForStatus(status));
        messageHistory.setMessage("message.history.report.channel.content.sent.scheduled.message");
        // createEventLog(subscriptionApp, null, null, Event.CONTENT_PUBLISHED,
        // "Sent messages to the subscriber-base of the application");

        messageHistoryRepository.saveEvent(messageHistory);
    }

    private Event getEventForStatus(ContentReleaseStatus status) {
        switch (status) {
            case DISCARDED:
            case SKIPPED:
                return Event.CONTENT_DELETED;
            case FUTURE:
                return Event.CONTENT_UPDATED;
            case SENT:
                return Event.CONTENT_SENT;
        }
        throw new IllegalStateException("can not find an event type for : " + status);
    }

    public void setBatchSize(int batchSize) {
        this.batchSize = batchSize;
    }

    public void setAventuraApiMtMessageDispatcher(AventuraApiMtMessageDispatcher aventuraApiMtMessageDispatcher) {
        this.aventuraApiMtMessageDispatcher = aventuraApiMtMessageDispatcher;
    }

    public void setMessageHistoryRepository(MessageHistoryRepository messageHistoryRepository) {
        this.messageHistoryRepository = messageHistoryRepository;
    }

    public void setSubsDataRepository(SubscriptionServiceDataRepository subsDataRepository) {
        this.subsDataRepository = subsDataRepository;
    }

    public void setTemporaryErrorCodes(Map<String, String> temporaryErrorCodes) {
        this.temporaryErrorCodes = temporaryErrorCodes;
    }

    public Map<String, String> getTemporaryErrorCodes() {
        return temporaryErrorCodes;
    }
}
