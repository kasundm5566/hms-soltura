/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 *
 */
package hsenidmobile.orca.core.applications.subscription;

import hsenidmobile.orca.core.applications.AbstractService;
import hsenidmobile.orca.core.applications.PeriodUnit;
import hsenidmobile.orca.core.flow.MessageFlowErrorReasons;
import hsenidmobile.orca.core.flow.MessageFlowException;
import hsenidmobile.orca.core.flow.dispatch.aventuraapi.AventuraApiMtMessageDispatcher;
import hsenidmobile.orca.core.flow.event.CommandEvent;
import hsenidmobile.orca.core.model.*;
import hsenidmobile.orca.core.model.message.Message;
import hsenidmobile.orca.core.model.message.MoResponse;
import hsenidmobile.orca.core.model.message.SmsMessage;
import hsenidmobile.orca.core.util.PropertyHolder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.Proxy;
import org.joda.time.DateTime;
import org.joda.time.DateTimeFieldType;
import org.joda.time.MutableDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * $LastChangedDate: $ $LastChangedBy: $
 *
 * @version $LastChangedRevision: $
 *
 */
@Entity
@Table(name = "subscription")
@Proxy(lazy = false)
public class Subscription extends AbstractService {

    @Transient
    private static final Logger logger = LoggerFactory.getLogger(Subscription.class);
    @Transient
    private Application application;

    @Transient
    private AventuraApiMtMessageDispatcher aventuraApiMtMessageDispatcher;

    @Transient
    private SubscriptionServiceDataRepository subscriptionServiceDataRepository;

    @Column(name = "is_sms_update_support_required")
    private boolean smsUpdateRequired = false;

    @OneToOne(fetch = FetchType.EAGER, optional = false)
    @Cascade(value = { CascadeType.ALL, CascadeType.DELETE_ORPHAN })
    @JoinColumn(name = "default_periodicity", nullable = false)
    private Periodicity defaultPeriodicity;

    @OneToMany(fetch = FetchType.EAGER)
    @LazyCollection(LazyCollectionOption.FALSE)
    @Cascade(value = { CascadeType.ALL, CascadeType.DELETE_ORPHAN })
    private List<Periodicity> supportedPeriodicities;
    private static final String DATE_PATTERN = "dd-MM-yyyy HH:mm:ss";

    public Subscription() {
        super(logger);
    }

    @Override
    public MoResponse onDataMessage(CommandEvent<Message> commandEvent) throws MessageFlowException {
        throw new UnsupportedOperationException("This method is not Supported in Subscription Service.");
    }

    @Override
    public MoResponse onServiceDataUpdateMessage(CommandEvent<Message> commandEvent) throws OrcaException {
        logger.debug("Service Data Update message received. Message[{}]", commandEvent);

        if (smsUpdateRequired) {
            logger.debug("Using default periodicity [{}]", defaultPeriodicity);

            validateSmsContent(commandEvent);

            DateTime now = new DateTime();

            List<SubscriptionContentRelease> contentBeforeUpdate = new ArrayList<SubscriptionContentRelease>();
            SubscriptionContentRelease updatableContentRelease = subscriptionServiceDataRepository
                    .getNextUpdatableContent(getAppId(), now);
            if (updatableContentRelease == null) {
                updatableContentRelease = createNewSmsUpdateContentRelease(now);
            } else {
                contentBeforeUpdate.add(updatableContentRelease);
            }

            setNewContent(updatableContentRelease, commandEvent);

            updateServiceData(Arrays.asList((ContentRelease) updatableContentRelease), contentBeforeUpdate);

            dispatchUpdateSuccessResponse(commandEvent, updatableContentRelease);
        } else {
            throw new SubscriptionServiceException("Service data update through Sms not supported.",
                    MessageFlowErrorReasons.SMS_UPDATE_NOT_SUPPORTED);
        }

        return null;
    }

    @Override
    public void updateServiceData(List<ContentRelease> updatedContents) throws OrcaException {

        List<SubscriptionContentRelease> scheduledContentList = subscriptionServiceDataRepository.getUpdatableContents(
                getAppId(), new DateTime());

        logger.debug("Current editable scheduled contents [{}]", scheduledContentList);

        if (scheduledContentList == null || scheduledContentList.isEmpty()) {
            scheduledContentList = new ArrayList<SubscriptionContentRelease>();
        }

        updateServiceData(updatedContents, scheduledContentList);

    }

    private void updateServiceData(List<ContentRelease> updatedContents,
                                   List<SubscriptionContentRelease> scheduledContentList) throws OrcaException {
        logger.debug("Current editable scheduled content size[{}]", scheduledContentList.size());
        logger.debug("Current editable scheduled contents [{}]", scheduledContentList);

        if (updatedContents != null && updatedContents.size() > 0) {
            for (ContentRelease cr : updatedContents) {
                SubscriptionContentRelease updateContent = (SubscriptionContentRelease) cr;
                logger.debug("Found content to update[{}]", updateContent);
                validateContentRelease(updateContent);

                if (updateContent.getId() == null) {
                    logger.debug("Adding new content release[{}]", updateContent);
                    scheduledContentList.add(updateContent);
                } else {
                    SubscriptionContentRelease scheduledContent = getScheduledContent(updateContent.getId(),
                            scheduledContentList);
                    if (scheduledContent != null) {
                        updateContent(scheduledContent, updateContent);
                    } else {
                        throw new SubscriptionServiceException("No Data found for content with Id["
                                + updateContent.getId() + "]", MessageFlowErrorReasons.SUBSCRIPTION_DATA_NOT_AVAILABLE);
                    }
                }
            }

            subscriptionServiceDataRepository.addOrUpdateContent(scheduledContentList);

        } else {
            logger.info("Content given to update is empty");
        }
    }

    private void updateContent(SubscriptionContentRelease scheduledContent,
                               SubscriptionContentRelease updatedContentRelease) {

        scheduledContent.setStatus(updatedContentRelease.getStatus());
        if (isSubCategoriesSupported()) {
            for (Content updateContent : updatedContentRelease.getContents()) {
                Content existingContent = scheduledContent.getContent(updateContent.getSubCategory().getKeyword());
                existingContent.getContent().setMessage(updateContent.getContent().getMessage());
            }
        } else {
            scheduledContent.getContents().get(0).getContent()
                    .setMessage(updatedContentRelease.getContents().get(0).getContent().getMessage());
        }

    }

    private SubscriptionContentRelease getScheduledContent(long id,
                                                           List<SubscriptionContentRelease> scheduledContentList) {
        if (scheduledContentList != null) {
            for (SubscriptionContentRelease contentRelease : scheduledContentList) {
                if (contentRelease.getId() == id) {
                    return contentRelease;
                }
            }
        }

        return null;
    }

    private void validateContentRelease(SubscriptionContentRelease contentRelease) throws SubscriptionServiceException {

        validatePeriodicity(contentRelease.getPeriodicity());

        if (isSubCategoriesSupported()) {
            List<SubCategory> supportedSubCategories = getSupportedSubCategories();
            logger.debug("Number of supported subcategories [{}] : ", supportedSubCategories);
            logger.debug("Number of updated contents  [{}] : ", contentRelease.getContents().size());
            List<Content> validatedContent = new ArrayList<Content>();

            for (SubCategory subCategory : supportedSubCategories) {
                Content content = contentRelease.getContent(subCategory.getKeyword());
                if (content != null) {
                    validatedContent.add(content);
                    //todo check this null checking with all the scenarios
//					throw new SubscriptionServiceException("No content found for SubCategory["
//							+ subCategory.getKeyword() + "]", MessageFlowErrorReasons.SUB_KEYWORD_NOT_FOUND);
                }
            }

            if (validatedContent.size() != contentRelease.getContents().size()) {
                throw new SubscriptionServiceException("Content found for not supported SubCategories.",
                        MessageFlowErrorReasons.SUB_KEYWORD_NOT_FOUND);
            }
        } else {
            if (contentRelease.getContents().size() != 1) {
                throw new SubscriptionServiceException(
                        "More than one content given for update in content release for App which do not support SubCategories.",
                        MessageFlowErrorReasons.SUB_KEYWORD_NOT_SUPPORTED);
            }

            if (contentRelease.getContents().get(0).getSubCategory() != null) {
                throw new SubscriptionServiceException(
                        "SubCategory provided for App which do not support SubCategories.",
                        MessageFlowErrorReasons.SUB_KEYWORD_NOT_SUPPORTED);
            }
        }

    }

    private void validatePeriodicity(Periodicity periodicity) throws SubscriptionServiceException {
        if (periodicity == null) {
            throw new IllegalArgumentException("Periodicity not provided");
        }

        for (Periodicity supportedPeriodicity : this.supportedPeriodicities) {
            if (supportedPeriodicity.isSamePeriodicity(periodicity)) {
                return;
            }
        }

        throw new SubscriptionServiceException("Periodicity[" + periodicity + "] not supported.",
                MessageFlowErrorReasons.SUBSCRIPTION_PERIODICITY_NOT_SUPPORTED);
    }

    private void setNewContent(SubscriptionContentRelease updatableContentRelease, CommandEvent<Message> commandEvent)
            throws SubscriptionServiceException {

        Content contentToUpdate;
        if (isSubCategoriesSupported()) {
            contentToUpdate = updatableContentRelease.getContent(commandEvent.getSubKeyword());
            if (contentToUpdate == null) {
                throw new SubscriptionServiceException("SubCategory[" + commandEvent.getSubKeyword() + "] not found.",
                        MessageFlowErrorReasons.SUB_KEYWORD_NOT_FOUND);
            }
        } else {
            contentToUpdate = updatableContentRelease.getContents().get(0);
        }

        contentToUpdate.getContent().setMessage(commandEvent.getMessage().getMessage());

    }

    private void validateSmsContent(CommandEvent<Message> commandEvent) throws SubscriptionServiceException {
        if (commandEvent.getMessage().getMessage() == null || commandEvent.getMessage().getMessage().trim().isEmpty()) {
            throw new SubscriptionServiceException("Empty update content.", MessageFlowErrorReasons.EMPTY_CONTENT);
        }
    }

    private void dispatchUpdateSuccessResponse(CommandEvent<Message> commandEvent,
                                               SubscriptionContentRelease updatableContentRelease) {
        String responseTxt = PropertyHolder.SUBSCRIPTION_SERVICE_DATA_UPDATE_SUCCESS_MSG;
        SmsMessage respMsg = new SmsMessage();
        respMsg.setCorrelationId(commandEvent.getMessage().getCorrelationId());
        respMsg.addReceiverAddress(commandEvent.getMessage().getSenderAddress());
        respMsg.setMessage(String.format(responseTxt,
                updatableContentRelease.getScheduledDispatchTimeFrom().toString(DATE_PATTERN)));
        respMsg.setKeyword(commandEvent.getMessage().getSenderAddress().getAddress());
        respMsg.setSenderAddress(new Msisdn(this.application.getAppName(), ""));

        aventuraApiMtMessageDispatcher.dispatchIndividualMessages(application.getAppId(), Arrays.asList(respMsg));

    }

    private SubscriptionContentRelease createNewSmsUpdateContentRelease(DateTime now)
            throws SubscriptionServiceException {
        logger.debug("Creating new content release to save new content");
        SubscriptionContentRelease newContentRelease = new SubscriptionContentRelease();
        newContentRelease.setPeriodicity(defaultPeriodicity);
        newContentRelease.setAppId(getAppId());
        newContentRelease.setAppName(this.application.getAppName());
        if (isSubCategoriesSupported()) {
            List<Content> contentList = new ArrayList<Content>();
            for (SubCategory category : getSupportedSubCategories()) {
                Content content = new Content();
                content.setSubCategory(category);
                content.setContent(new LocalizedMessage("", getDefaultLocale()));
                contentList.add(content);
            }

            newContentRelease.setContents(contentList);
        } else {
            Content content = new Content();
            content.setContent(new LocalizedMessage("", getDefaultLocale()));
            newContentRelease.setContents(Arrays.asList(content));
        }
        // Compare with last sent time as well
        DateTime scheduleTimeBasedOnCurrentTime = this.defaultPeriodicity.getNextScheduledDispatchTime(now);
        if (isLastContentSentTimeWithinScheduleTimeBuffer(this.defaultPeriodicity, scheduleTimeBasedOnCurrentTime)) {

            DateTime lastSentTime = this.defaultPeriodicity.getLastSentTime();
            logger.debug(
                    "Last content sent time[{}] is withing schedule time buffer[{}/{}, so calculating schedule time based on taking[{}] as last scheduled time.",
                    new Object[] { lastSentTime, scheduleTimeBasedOnCurrentTime,
                            this.defaultPeriodicity.getAllowedBufferTime(), scheduleTimeBasedOnCurrentTime });

            newContentRelease.setScheduledDispatchTimeFrom(new DateTime(this.defaultPeriodicity
                    .getNextScheduleTimeBasedOnLastScheduleTime(scheduleTimeBasedOnCurrentTime.toDate())));
        } else {
            newContentRelease.setScheduledDispatchTimeFrom(scheduleTimeBasedOnCurrentTime);
        }

        newContentRelease.setScheduledDispatchTimeTo(newContentRelease.getScheduledDispatchTimeFrom().plusMinutes(
                newContentRelease.getPeriodicity().getAllowedBufferTime()));

        return newContentRelease;
    }

    private boolean isLastContentSentTimeWithinScheduleTimeBuffer(Periodicity subscriptionPeriodicity,
                                                                  DateTime scheduleTimeBasedOnCurrentTime) {
        if (subscriptionPeriodicity.getLastSentTime() != null) {
            DateTime lastSentTime = new DateTime(subscriptionPeriodicity.getLastSentTime());
            logger.debug("Last content sent time[{}] scheduleTimeBasedOnCurrentTime[{}]", lastSentTime,
                    scheduleTimeBasedOnCurrentTime);
            return lastSentTime.isAfter(scheduleTimeBasedOnCurrentTime);

        } else {
            return false;
        }
    }

    private String getAppId() {
        if (this.application != null) {
            return this.application.getAppId();
        } else {
            return null;
        }
    }

    public void setSubscriptionDataRepository(SubscriptionServiceDataRepository subscriptionDataService) {
        this.subscriptionServiceDataRepository = subscriptionDataService;
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    public boolean isSmsUpdateRequired() {
        return smsUpdateRequired;
    }

    public void setSmsUpdateRequired(boolean smsUpdateRequired) {
        this.smsUpdateRequired = smsUpdateRequired;
    }

	@Override
	public boolean isSubscriptionRequired() {
		return true;
	}

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(300);
        builder.append("Subscription [appId=");
        builder.append(getAppId());
        builder.append("]");
        return builder.toString();
    }

    public void setAventuraApiMtMessageDispatcher(AventuraApiMtMessageDispatcher aventuraApiMtMessageDispatcher) {
        this.aventuraApiMtMessageDispatcher = aventuraApiMtMessageDispatcher;
    }

    public Periodicity getDefaultPeriodicity() {
        return defaultPeriodicity;
    }

    public void setDefaultPeriodicity(Periodicity defaultPeriodicity) {
        this.defaultPeriodicity = defaultPeriodicity;
    }

    public List<Periodicity> getSupportedPeriodicities() {
        return supportedPeriodicities;
    }

    public void setSupportedPeriodicities(List<Periodicity> supportedPeriodicities) {
        this.supportedPeriodicities = supportedPeriodicities;
    }

    /**
     * Check whether this subscription service is eligible to dispatch content
     * now.
     *
     * @param periodUnit
     * @param currentTime
     * @throws SubscriptionServiceException
     */
    public boolean isEligibleForDispatching(PeriodUnit periodUnit, DateTime currentTime)
            throws SubscriptionServiceException {
        Periodicity periodicity = getPeriodicity(periodUnit);

        ScheduleTimeFrame dispatchingTimeFrame = calculateDispatchingTimeFrame(periodicity, currentTime);

        if (isEqualOrAfter(currentTime, dispatchingTimeFrame.getFrom())
                && isEqualOrBefore(currentTime, dispatchingTimeFrame.getTo())) {
            return true;
        } else {
            return false;
        }

    }

    private boolean isEqualOrAfter(DateTime a, DateTime b) {
        return a.isEqual(b) || a.isAfter(b);
    }

    private boolean isEqualOrBefore(DateTime a, DateTime b) {
        return a.isEqual(b) || a.isBefore(b);
    }

    private ScheduleTimeFrame calculateDispatchingTimeFrame(Periodicity periodicity, DateTime currentTime) {
        switch (periodicity.getUnit()) {
            case HOUR:
                return calculateDispatchingTimeFrameForHour(periodicity, currentTime);
            case DAY:
                return calculateDispatchingTimeFrameForDay(periodicity, currentTime);
            case WEEK:
                return calculateDispatchingTimeFrameForWeek(periodicity, currentTime);
            case MONTH:
                return calculateDispatchingTimeFrameForMonth(periodicity, currentTime);
            default:
                throw new IllegalStateException("PeriodUnit not defined.");
        }
    }

    private ScheduleTimeFrame calculateDispatchingTimeFrameForMonth(Periodicity periodicity, DateTime currentTime) {
        MutableDateTime mutableFrom = new MutableDateTime(currentTime);
        if (periodicity.getDefaultDispatchConfiguration().isUseLastDayOfMonth()) {
            //@formatter:off
            /**
             * To Handle scenario where [
             * 		Default configuration is LAST_DAY_OF_MONTH/8/30
             * 		Buffer is 2 days
             * 		Current time is Sep/1/8/30 ]
             * So from should be calculated to Aug/31/8/30
             */
            //@formatter:on
            if (bufferTimeInDays(periodicity) >= 1) {
                if (mutableFrom.getDayOfMonth() < mutableFrom.dayOfMonth().getMaximumValue()) {
                    mutableFrom.setMonthOfYear(mutableFrom.getMonthOfYear() - 1);
                }
            }
            mutableFrom.set(DateTimeFieldType.dayOfMonth(), mutableFrom.dayOfMonth().getMaximumValue());
        } else {
            //@formatter:off
            /**
             * To Handle scenario where [
             * 		Default configuration is MONTH/31/8/30
             * 		Buffer is 2 days
             * 		Current time is Sep/1/8/30 ]
             * So from should be calculated to Aug/31/8/30
             */
            //@formatter:on
            if (bufferTimeInDays(periodicity) >= 1) {
                DateTime minusDays = currentTime.minusDays(bufferTimeInDays(periodicity));
                if (minusDays.getMonthOfYear() < currentTime.getMonthOfYear()
                        || minusDays.getYear() < currentTime.getYear()) {
                    mutableFrom.addMonths(-1);
                }
            }

            int maxDateForThisMonth = mutableFrom.dayOfMonth().getMaximumValue();
            if (periodicity.getDefaultDispatchConfiguration().getDayOfMonth() > maxDateForThisMonth) {
                mutableFrom.set(DateTimeFieldType.dayOfMonth(), maxDateForThisMonth);
            } else {
                mutableFrom.set(DateTimeFieldType.dayOfMonth(), periodicity.getDefaultDispatchConfiguration()
                        .getDayOfMonth());
            }
        }
        mutableFrom.set(DateTimeFieldType.hourOfDay(), periodicity.getDefaultDispatchConfiguration().getHourOfDay());
        mutableFrom.set(DateTimeFieldType.minuteOfHour(), periodicity.getDefaultDispatchConfiguration()
                .getMinuteOfHour());
        mutableFrom.setSecondOfMinute(0);
        mutableFrom.setMillisOfSecond(0);

        DateTime from = new DateTime(mutableFrom);
        DateTime to = from.plusMinutes(periodicity.getAllowedBufferTime());
        return new ScheduleTimeFrame(from, to);
    }

    private int bufferTimeInDays(Periodicity periodicity) {
        return periodicity.getAllowedBufferTime() / (60 * 24);
    }

    private ScheduleTimeFrame calculateDispatchingTimeFrameForWeek(Periodicity periodicity, DateTime currentTime) {
        MutableDateTime mutableFrom = new MutableDateTime(currentTime);
        if (mutableFrom.getDayOfWeek() != periodicity.getDefaultDispatchConfiguration().getDayOfWeek()) {
            if (mutableFrom.getDayOfWeek() > periodicity.getDefaultDispatchConfiguration().getDayOfWeek()) {
                mutableFrom.addDays(periodicity.getDefaultDispatchConfiguration().getDayOfWeek()
                        - mutableFrom.getDayOfWeek());
            } else {
                mutableFrom.addDays(periodicity.getDefaultDispatchConfiguration().getDayOfWeek()
                        - mutableFrom.getDayOfWeek());
            }
        }
        mutableFrom.set(DateTimeFieldType.hourOfDay(), periodicity.getDefaultDispatchConfiguration().getHourOfDay());
        mutableFrom.set(DateTimeFieldType.minuteOfHour(), periodicity.getDefaultDispatchConfiguration()
                .getMinuteOfHour());
        mutableFrom.setSecondOfMinute(0);
        mutableFrom.setMillisOfSecond(0);

        DateTime from = new DateTime(mutableFrom);
        DateTime to = from.plusMinutes(periodicity.getAllowedBufferTime());
        return new ScheduleTimeFrame(from, to);
    }

    private ScheduleTimeFrame calculateDispatchingTimeFrameForDay(Periodicity periodicity, DateTime currentTime) {
        MutableDateTime mutableFrom = new MutableDateTime(currentTime);
        mutableFrom.setHourOfDay(periodicity.getDefaultDispatchConfiguration().getHourOfDay());
        mutableFrom.setMinuteOfHour(periodicity.getDefaultDispatchConfiguration().getMinuteOfHour());
        mutableFrom.setSecondOfMinute(0);
        mutableFrom.setMillisOfSecond(0);

        DateTime from = new DateTime(mutableFrom);
        DateTime to = from.plusMinutes(periodicity.getAllowedBufferTime());
        return new ScheduleTimeFrame(from, to);
    }

    private ScheduleTimeFrame calculateDispatchingTimeFrameForHour(Periodicity periodicity, DateTime currentTime) {
        MutableDateTime mutableFrom = new MutableDateTime(currentTime);
        mutableFrom.setMinuteOfHour(periodicity.getDefaultDispatchConfiguration().getMinuteOfHour());
        mutableFrom.setSecondOfMinute(0);
        mutableFrom.setMillisOfSecond(0);

        DateTime from = new DateTime(mutableFrom);
        DateTime to = from.plusMinutes(periodicity.getAllowedBufferTime());
        return new ScheduleTimeFrame(from, to);
    }

    private Periodicity getPeriodicity(PeriodUnit periodUnit) throws SubscriptionServiceException {
        for (Periodicity periodicity : getSupportedPeriodicities()) {
            if (periodicity.getUnit() == periodUnit) {
                return periodicity;
            }
        }

        throw new SubscriptionServiceException("Periodicity [" + periodUnit + "] not supported.",
                MessageFlowErrorReasons.SUBSCRIPTION_PERIODICITY_NOT_SUPPORTED);
    }

}