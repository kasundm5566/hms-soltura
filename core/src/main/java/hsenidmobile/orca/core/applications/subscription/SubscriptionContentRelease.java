/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.applications.subscription;

import hsenidmobile.orca.core.model.ContentRelease;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * $LastChangedDate$
 * $LastChangedBy$ $LastChangedRevision$
 *
 */
@Entity
@Table(name = "subscription_content_release")
public class SubscriptionContentRelease extends ContentRelease {
	private static final Logger logger = LoggerFactory.getLogger(SubscriptionContentRelease.class);

	public enum ContentReleaseStatus {
		FUTURE, DISCARDED, SKIPPED, SENT
	}

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	private Periodicity periodicity;

	@Column(name = "scheduled_dispatch_time_from", nullable = false)
	@Type(type = "org.joda.time.contrib.hibernate.PersistentDateTime")
	private DateTime scheduledDispatchTimeFrom;

	@Column(name = "scheduled_dispatch_time_to", nullable = false)
	@Type(type = "org.joda.time.contrib.hibernate.PersistentDateTime")
	private DateTime scheduledDispatchTimeTo;

	@Enumerated(EnumType.STRING)
	private ContentReleaseStatus status = ContentReleaseStatus.FUTURE;

	public Periodicity getPeriodicity() {
		return periodicity;
	}

	public void setPeriodicity(Periodicity periodicity) {
		this.periodicity = periodicity;
	}

	public DateTime getScheduledDispatchTimeFrom() {
		return scheduledDispatchTimeFrom;
	}

	public void setScheduledDispatchTimeFrom(DateTime scheduledDispatchTime) {
		this.scheduledDispatchTimeFrom = scheduledDispatchTime;
	}

	public DateTime getScheduledDispatchTimeTo() {
		return scheduledDispatchTimeTo;
	}

	public void setScheduledDispatchTimeTo(DateTime scheduledDispatchTimeTo) {
		this.scheduledDispatchTimeTo = scheduledDispatchTimeTo;
	}

	public ContentReleaseStatus getStatus() {
		return status;
	}

	public void setStatus(ContentReleaseStatus status) {
		this.status = status;
	}

	/**
	 * <p>
	 * SubscriptionContentRelease is only sendable within (scheduledDispathTime
	 * + Periodicity.AllowedBufferTime)
	 * </p>
	 *
	 * <pre>
	 * E.g:
	 * Periodicity
	 * 	period - Hourly
	 * 	buffer - 10min
	 *
	 * scheduled on : 2010-8-20 12:00
	 * current time : 2010-8-20 12:00
	 * can send
	 *
	 * scheduled on : 2010-8-20 12:00
	 * current time : 2010-8-20 12:05
	 * can send
	 *
	 * scheduled on : 2010-8-20 12:00
	 * current time : 2010-8-20 12:10
	 * can send
	 *
	 * scheduled on : 2010-8-20 12:00
	 * current time : 2010-8-20 12:11
	 * can't send
	 *
	 * scheduled on : 2010-8-20 12:00
	 * current time : 2010-8-20 11:50
	 * can't send
	 *
	 * </pre>
	 *
	 * @param currentTime
	 * @return
	 */
	public boolean canDispathchedOn(long currentTime) {
		DateTime givenTime = new DateTime(currentTime);
		DateTime scheduled = scheduledDispatchTimeFrom;
		boolean sendable = false;

		logger.debug("Checking sendability of content group scheduled[{}], now[{}], status[{}] ", new Object[] {
				scheduled, givenTime, this.status });

		if (this.status == ContentReleaseStatus.FUTURE) {
			if (periodicity == null) {
				new IllegalArgumentException("Periodicity cannot be NULL.");
			}

			if (givenTime.isBefore(scheduledDispatchTimeFrom)) {
				sendable = false;
			} else {
				if (givenTime.isAfter(scheduled.plusMinutes(periodicity.getAllowedBufferTime()).getMillis())) {
					sendable = false;
				} else {
					sendable = true;
				}
			}
		}

		logger.debug("Content sendable[{}]", sendable);
		return sendable;

	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(250);
		builder.append("SubscriptionContentRelease [periodicity=");
		builder.append(periodicity);
		builder.append(", appId=");
		builder.append(getAppId());
		builder.append(", scheduledDispatchTimeFrom=");
		builder.append(scheduledDispatchTimeFrom);
		builder.append(", scheduledDispatchTimeTo=");
		builder.append(scheduledDispatchTimeTo);
		builder.append(", status=");
		builder.append(status);
		builder.append(", Id=");
		builder.append(getId());
		builder.append("]");
		return builder.toString();
	}

}
