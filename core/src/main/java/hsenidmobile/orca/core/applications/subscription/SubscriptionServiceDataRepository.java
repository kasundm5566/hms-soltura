/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.applications.subscription;

import java.util.List;

import hsenidmobile.orca.core.applications.ContentDispatchSummary;
import hsenidmobile.orca.core.applications.exception.ApplicationException;
import org.joda.time.DateTime;

/**
 * Service class used to manage and fetch Data related to Subscription
 * Application.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 *
 * @version $LastChangedRevision$
 */
public interface SubscriptionServiceDataRepository {

    /**
     * Get the content for given appId and scheduled time.
     *
     * @param appId
     * @param scheduledTime
     * @return
     */
    SubscriptionContentRelease getDispatchableContent(String appId, DateTime scheduledTime);

    /**
     * Get list of all dispatchable contents as per current time.
     *
     * @param currentTime
     * @param batchSize
     * @return
     */
    List<SubscriptionContentRelease> getDispatchableContents(DateTime currentTime, int batchSize);

    /**
     * Get next updatable content for the given app based on current time.
     *
     * @param appId
     * @param currentTime
     * @return
     */
    SubscriptionContentRelease getNextUpdatableContent(String appId, DateTime currentTime);

    /**
     * Get list of all updatable contents for given application.
     *
     * @param appId
     * @param currentTime
     * @return
     */
    List<SubscriptionContentRelease> getUpdatableContents(String appId, DateTime currentTime);

    void addOrUpdateContent(List<SubscriptionContentRelease> contents);

    /**
     * Add new SubscriptionContentRelease to the database.
     *
     * @param contentRelease
     */
    void addContentRelease(SubscriptionContentRelease contentRelease);

    /**
     * Update existing SubscriptionContentRelease.
     *
     * @param contentRelease
     */
    void updateContentRelease(SubscriptionContentRelease contentRelease);


    /**
     * Get last sent subscription message for a  given application
     * @param appId
     * @return
     * @throws ApplicationException
     */
    public ContentDispatchSummary getLastSentSubscriptionByApplicationId(String appId) throws ApplicationException;

    /**
     * Get last sent subscription message for a  given application and given subcategory
     * @param appId
     * @param subCategoryId
     * @return
     * @throws ApplicationException
     */
    ContentDispatchSummary getLastSentSubscriptionBySubCategoryId(String appId, long subCategoryId) throws ApplicationException;
}
