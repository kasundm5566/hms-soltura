/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.applications.voting;

import hsenidmobile.orca.core.model.Response;
import hsenidmobile.orca.core.model.SubCategory;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 *
 */
@Entity
@Table(name = "vote")
public class Vote extends Response {

    @ManyToOne
    @JoinTable(name="candidate_vote",
            joinColumns = @JoinColumn(name="vote_id"),
            inverseJoinColumns = @JoinColumn(name="candidate_id")
    )
    private SubCategory candidate;

    public SubCategory getCandidate() {
        return candidate;
    }

    public void setCandidate(SubCategory candidate) {
        this.candidate = candidate;
    }
}
