/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.applications.voting;

import java.util.Date;

/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class VoteResultSummary {
	private String candidateName;
	private String candidateCode;
	private long totalVoteCount;
	private Date lastUpdateTime;

	public String getCandidateName() {
		return candidateName;
	}

	public void setCandidateName(String candidateName) {
		this.candidateName = candidateName;
	}

	public long getTotalVoteCount() {
		return totalVoteCount;
	}

	public void setTotalVoteCount(long totalVoteCount) {
		this.totalVoteCount = totalVoteCount;
	}

	public Date getLastUpdateTime() {
		return lastUpdateTime;
	}

	public void setLastUpdateTime(Date lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}

    public String getCandidateCode() {
        return candidateCode;
    }

    public void setCandidateCode(String candidateCode) {
        this.candidateCode = candidateCode;
    }

    @Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("VoteResultSummary [candidateName=");
		builder.append(candidateName);
		builder.append(", candidateCode=");
		builder.append(candidateCode);
		builder.append(", lastUpdateTime=");
		builder.append(lastUpdateTime);
		builder.append(", totalVoteCount=");
		builder.append(totalVoteCount);
		builder.append("]");
		return builder.toString();
	}

}
