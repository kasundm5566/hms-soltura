package hsenidmobile.orca.core.applications.voting;

/**
 * Votelet will be running for long time, or for ever.
 * If we want to keep it active for some specific period of the day we can use VoteletActivePeriod.
 * 
 *
 * $LastChangedDate: 2009-10-13 10:15:52 +0530 (Tue, 13 Oct 2009) $
 * $LastChangedBy: romith $
 * $LastChangedRevision: 52686 $
 */
public interface VoteletActivePeriod {
    public boolean isActive();

    public boolean isCollide(VoteletActivePeriod activePeriod);
}
