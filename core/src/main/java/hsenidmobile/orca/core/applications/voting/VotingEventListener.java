/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.applications.voting;

import hsenidmobile.orca.core.repository.VoteRepository;

/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public interface VotingEventListener {
	void successfulVote(long voteletId, Vote vote);

	void setVoteRepository(VoteRepository voteRepository);
}
