package hsenidmobile.orca.core.applications.voting;

import java.util.Locale;

import hsenidmobile.orca.core.applications.exception.VoteletException;
import hsenidmobile.orca.core.model.SubCategory;
import hsenidmobile.orca.core.model.message.Message;
import hsenidmobile.orca.core.model.message.MessageContext;
import hsenidmobile.orca.core.model.message.MoResponse;

/**
 * $LastChangedDate: 2009-10-13 10:15:52 +0530 (Tue, 13 Oct 2009) $
 * $LastChangedBy: romith $
 * $LastChangedRevision: 52686 $
 */
public interface VotingResponseSpecification {

    public MoResponse generateResponse(VotingService votingService, Message message, SubCategory candidateDetails, MessageContext messageContext, Locale locale) throws VoteletException;

}
