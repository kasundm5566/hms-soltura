/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 *
 */
package hsenidmobile.orca.core.applications.voting;

import hsenidmobile.orca.core.applications.AbstractService;
import hsenidmobile.orca.core.applications.exception.ApplicationException;
import hsenidmobile.orca.core.applications.exception.VoteletException;
import hsenidmobile.orca.core.applications.voting.impl.NoVotingResponseSpecification;
import hsenidmobile.orca.core.applications.voting.impl.ResultSummaryVotingResponseSpecification;
import hsenidmobile.orca.core.flow.MessageFlowErrorReasons;
import hsenidmobile.orca.core.flow.MessageFlowException;
import hsenidmobile.orca.core.flow.dispatch.aventuraapi.AventuraApiMtMessageDispatcher;
import hsenidmobile.orca.core.flow.event.CommandEvent;
import hsenidmobile.orca.core.model.Application;
import hsenidmobile.orca.core.model.Event;
import hsenidmobile.orca.core.model.Msisdn;
import hsenidmobile.orca.core.model.SubCategory;
import hsenidmobile.orca.core.model.message.AventuraSmsMessage;
import hsenidmobile.orca.core.model.message.Message;
import hsenidmobile.orca.core.model.message.MessageContext;
import hsenidmobile.orca.core.model.message.MoResponse;
import hsenidmobile.orca.core.repository.SdpApplicationRepository;
import hsenidmobile.orca.core.repository.VoteRepository;
import hsenidmobile.orca.core.services.MoResponseValidationService;
import hsenidmobile.orca.core.util.PropertyHolder;
import hsenidmobile.orca.core.util.Util;
import org.hibernate.annotations.*;
import org.joda.time.DateTimeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import static hsenidmobile.orca.core.util.eventlog.EventLogger.createEventLog;

/**
 *
 * $LastChangedDate: $ $LastChangedBy: $ $LastChangedRevision: $
 *
 */
@Entity
@Table(name = "voting_service")
@Proxy(lazy = false)
public class VotingService extends AbstractService {

    @Transient
    private static final Logger logger = LoggerFactory.getLogger(VotingService.class);

    private static final String STATUS_CODE_KEY = "statusCode";

    @Transient
    private AventuraApiMtMessageDispatcher aventuraApiMtMessageDispatcher;

    @Transient
    Application application;

    @Transient
    SdpApplicationRepository sdpApplicationRepository;

    @Transient
    private VoteRepository voteRepository;

    @Transient
    private VotingEventListener votingEventListener;

    @Column(name = "is_one_vote_per_number")
    private boolean oneVotePerNumber = false;

    @Column(name = "is_vote_result_public")
    private boolean voteResultPublic = false;

    @Any(metaColumn = @Column(name = "response_spec_type"), fetch = FetchType.EAGER)
    @AnyMetaDef(idType = "long", metaType = "string", metaValues = {
            @MetaValue(value = "NO_RESPONSE", targetEntity = NoVotingResponseSpecification.class),
            @MetaValue(value = "RESULT_RESPONSE", targetEntity = ResultSummaryVotingResponseSpecification.class) })
    @JoinColumn(name = "voting_response_id")
    @Cascade(value = org.hibernate.annotations.CascadeType.ALL)
    @LazyToOne(LazyToOneOption.FALSE)
    private VotingResponseSpecification responseSpecification;

    @Transient
    private MoResponseValidationService moResponseValidationService;

    public VotingService() {
        super(logger);
    }

	@Override
	public boolean isSubscriptionRequired() {
		return false;
	}

    @Override
    public MoResponse onDataMessage(CommandEvent<Message> commandEvent) throws MessageFlowException {

        if (commandEvent.isSystemWideKeyword()) {
            return handlerVotingResultQuery(commandEvent);
        } else {
            return handleVoting(commandEvent);
        }
    }

    private MoResponse handleVoting(CommandEvent<Message> commandEvent) throws ApplicationException, VoteletException {

        if (this.oneVotePerNumber) {
            boolean hasVoted = voteRepository.hasVoted(commandEvent.getMessage().getSenderAddress(), getId());
            if (hasVoted) {
                return handleOneVotePerNumberResponse(commandEvent);
            }
        }

        MoResponse moResponse = voteReceived(commandEvent);

        if (moResponse.getDataResponse() != null) {
            sendResponseMessage(moResponse, commandEvent);
        } else {
            logger.info("No voting response to send..");
        }

        return moResponse;
    }

    private MoResponse voteReceived(final CommandEvent<Message> commandEvent) throws ApplicationException,
            VoteletException {
        String candidateCode = commandEvent.getSubKeyword();
        Message message = commandEvent.getMessage();
        MessageContext messageContext = commandEvent.getMessageContext();
        validateApplication();

        final SubCategory candidate = getSubcategory(candidateCode);
        Vote vote = generateVote(message, candidate);

        long voteId = voteRepository.add(vote);
        votingEventListener.successfulVote(getId(), vote);
        createEventLog(messageContext.getApplication(), null, "", Event.CONTENT_RECEIVED, "application "
                + messageContext.getApplication().getAppId() + " received a vote " + messageContext.getApplication());
        final MoResponse response = responseSpecification.generateResponse(this, message, candidate, messageContext,
                commandEvent.getLanguage());
        response.setVoteId(voteId);
        if (response.getDataResponse() != null && response.getDataResponse().getMessage() != null) {
            response.getDataResponse().getMessage().setCorrelationId(commandEvent.getMessage().getCorrelationId());
        }

        return response;

    }

    private void validateApplication() throws ApplicationException {
        if (!isSubCategoriesSupported()) {
            throw new IllegalStateException("Voting service without SubCategory support found.");
        }
    }

    protected Vote generateVote(Message message, SubCategory candidate) {
        Vote vote = new Vote();
        vote.setMsisdn(message.getSenderAddress());
        vote.setReceivedMessage(message.getMessage());
        vote.setReceivedTime(DateTimeUtils.currentTimeMillis());
        vote.setCandidate(candidate);
        vote.setServiceId(this.getId());
        logger.debug("Adding new vote which was received from MSISDN [{}]", vote.getMsisdn());
        return vote;
    }

    private MoResponse handlerVotingResultQuery(CommandEvent<Message> commandEvent) throws ApplicationException {
        logger.debug("System Keyword[{}] found.", commandEvent.getSubKeyword());
        MoResponse moResponse = null;
        if (commandEvent.getSubKeyword().equals(PropertyHolder.RESULT_SYSTEM_COMMAND)) {
            moResponse = getVoteResults(commandEvent.getMessage());
        } else {
            ApplicationException ex = new ApplicationException(
                    "Trying to view results of private voting app without voting.",
                    MessageFlowErrorReasons.VOTE_CANDIDATE_NOT_FOUND);
            throw ex;
        }

        if (moResponse != null && moResponse.getDataResponse() != null) {
            moResponse.getDataResponse().getMessage().setCorrelationId(commandEvent.getMessage().getCorrelationId());
            sendResponseMessage(moResponse, commandEvent);
        } else {
            logger.info("No voting response to send..");
        }

        return moResponse;
    }

    private MoResponse getVoteResults(Message message) throws ApplicationException {
        if (voteResultPublic) {
            return createVotingResultMsg(message);
        } else {
            if (voteRepository.hasVoted(message.getSenderAddress(), getId())) {
                return createVotingResultMsg(message);
            } else {
                ApplicationException ex = new ApplicationException(
                        "Trying to view results of private voting app without voting.",
                        MessageFlowErrorReasons.VOTING_RESULT_PRIVATE);
                throw ex;
            }
        }
    }

    private MoResponse createVotingResultMsg(Message message) {
        List<VoteResultSummary> votingSummary = getResultSummary();

        StringBuilder resultTxtBuilder = new StringBuilder(160);
        for (VoteResultSummary voteResultSummary : votingSummary) {
            resultTxtBuilder.append(voteResultSummary.getCandidateName()).append(" : ")
                    .append(voteResultSummary.getTotalVoteCount()).append("\n");
        }

        return Util.createResponseMessage(message, String.format(PropertyHolder.VOTING_RESULT_QUERY_RESPONSE_TEMPLATE,
                this.application.getAppName(), resultTxtBuilder.toString()));
    }

    private MoResponse handleOneVotePerNumberResponse(CommandEvent<Message> commandEvent) throws ApplicationException {
        Message message = commandEvent.getMessage();
        ApplicationException ex = new ApplicationException("Subscriber[" + message.getSenderAddress()
                + "] already voted for Application[" + this.application.getAppId() + "]",
                MessageFlowErrorReasons.ALREADY_VOTED);
        throw ex;
    }

    private void sendResponseMessage(MoResponse moResponse, CommandEvent<Message> commandEvent) throws ApplicationException {
        logger.debug("Vote received sending response [{}]", moResponse);
        AventuraSmsMessage message = new AventuraSmsMessage();
        message.setCorrelationId(moResponse.getDataResponse().getMessage().getCorrelationId());
        message.setKeyword(moResponse.getDataResponse().getMessage().getReceiverAddresses().get(0).getAddress());
        message.setMessage(moResponse.getDataResponse().getMessage().getMessage());
        message.setSenderAddress(new Msisdn(commandEvent.getMessageContext().getApplication().getAppName(),"ANY"));
        Future future = aventuraApiMtMessageDispatcher.dispatchIndividualMessages(application.getAppId(), Arrays.asList(message));
        if(future != null) {
            handleInvalidVoteResponse(moResponse, commandEvent, message, future);
        } else {
            logger.debug("Null Response Received for the message [{}]", message);
        }
    }

    private void handleInvalidVoteResponse(MoResponse moResponse, CommandEvent<Message> commandEvent,
                                           AventuraSmsMessage message, Future future) throws ApplicationException {
        try {
            Map<String, Object> futureStatusMap = (Map<String, Object>) future.get();

            String statusCode = (String) futureStatusMap.get(STATUS_CODE_KEY);
            logger.debug("Response from the Future: [{}]", futureStatusMap);
            if(!moResponseValidationService.isMoCanBeChargedUsingMtCharging(statusCode)) {
                logger.debug("Mo Charging has been failed, smsMessage=[{}]", new Object[]{message});
                try {
                    logger.debug("Rollback Operation for Vote Service ID [{}] and Vote ID [{}] Started..",
                            new Object[] {getId(), moResponse.getVoteId()});
                    voteRepository.rollBackReceivedVote(moResponse.getVoteId(), getId());
                    logger.debug("Rollback Operation for Vote Service ID [{}] and Vote ID [{}] Finished !!",
                            new Object[] {getId(), moResponse.getVoteId()});
                    handleInvalidVoteReceivedResponse(commandEvent);
                } catch (ApplicationException e) {
                    throw e;
                }
                catch (Exception e) {
                    logger.warn("Error occurred while performing the rollback operation for the Vote Service" +
                            " ID [{}] and Vote ID [{}] ", new Object[] {getId(), moResponse.getVoteId()}, e);
                }
            } else {
                logger.debug("MO can be chargeable using MT");
            }
        } catch (InterruptedException e) {
            logger.error("InterruptedException occurred while sending Voting Response for the message [{}]", message);
        } catch (ExecutionException e) {
            logger.error("ExecutionException occurred while sending Voting Response for the message [{}]", message);
        }
    }

    private void handleInvalidVoteReceivedResponse(CommandEvent<Message> commandEvent) throws ApplicationException {
        ApplicationException ex = new ApplicationException("Subscriber[" + commandEvent.getMessage().getSenderAddress()
                + "] does not have enough credits to vote the application [" + this.application.getAppId() + "]",
                MessageFlowErrorReasons.INVALID_VOTE);
        throw ex;
    }

    public MoResponse onServiceDataUpdateMessage(CommandEvent<Message> commandEvent) {
        logger.info("Message Received for Update. [{}]", commandEvent);
        throw new UnsupportedOperationException(
                "Message update service for VotingService applications is not supported");
    }

    @Override
    public void setApplication(Application application) {
        this.application = application;
    }

    public void setAventuraApiMtMessageDispatcher(AventuraApiMtMessageDispatcher aventuraApiMtMessageDispatcher) {
        this.aventuraApiMtMessageDispatcher = aventuraApiMtMessageDispatcher;
    }

    public void setSdpApplicationRepository(SdpApplicationRepository sdpApplicationRepository) {
        this.sdpApplicationRepository = sdpApplicationRepository;
    }

    public void setVoteRepository(VoteRepository voteRepository) {
        this.voteRepository = voteRepository;
    }

    public boolean isOneVotePerNumber() {
        return oneVotePerNumber;
    }

    public void setOneVotePerNumber(boolean oneVotePerNumber) {
        this.oneVotePerNumber = oneVotePerNumber;
    }

    public List<VoteResultSummary> getResultSummary() {
        return voteRepository.getVotingSummary(getId());
    }

    public void setVotingEventListener(VotingEventListener votingEventListener) {
        this.votingEventListener = votingEventListener;
    }

    public VotingResponseSpecification getResponseSpecification() {
        return responseSpecification;
    }

    public void setResponseSpecification(VotingResponseSpecification responseSpecification) {
        this.responseSpecification = responseSpecification;
    }

    public boolean isVoteResultPublic() {
        return voteResultPublic;
    }

    public void setVoteResultPublic(boolean voteResultPublic) {
        this.voteResultPublic = voteResultPublic;
    }

    public void setMoResponseValidationService(MoResponseValidationService moResponseValidationService) {
        this.moResponseValidationService = moResponseValidationService;
    }
}