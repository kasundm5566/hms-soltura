package hsenidmobile.orca.core.applications.voting.impl;

import hsenidmobile.orca.core.applications.exception.VoteletException;
import hsenidmobile.orca.core.applications.voting.VotingResponseSpecification;
import hsenidmobile.orca.core.applications.voting.VotingService;
import hsenidmobile.orca.core.model.SubCategory;
import hsenidmobile.orca.core.model.message.Message;
import hsenidmobile.orca.core.model.message.MessageContext;
import hsenidmobile.orca.core.model.message.MoResponse;
import hsenidmobile.orca.core.repository.PersistentObject;

import java.util.Locale;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import hsenidmobile.orca.core.util.Util;
import org.hibernate.annotations.Proxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * $LastChangedDate: 2009-10-13 10:15:52 +0530 (Tue, 13 Oct 2009) $
 * $LastChangedBy: romith $ $LastChangedRevision: 52686 $
 */
@Entity
@Table(name = "votelet_response_spec_no_resp")
@Proxy(lazy = false)
public class NoVotingResponseSpecification extends PersistentObject implements VotingResponseSpecification {

    @Transient
    private static final Logger logger = LoggerFactory.getLogger(NoVotingResponseSpecification.class);

    @Override
    public MoResponse generateResponse(VotingService votingService, Message message, SubCategory candidateDetails,
                                       MessageContext messageContext, Locale locale) throws VoteletException {
        logger.debug("Using NoVotingResponseSpecification");
        return new MoResponse();
    }
}
