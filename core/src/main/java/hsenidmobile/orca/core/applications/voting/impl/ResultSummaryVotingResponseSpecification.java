/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.applications.voting.impl;

import hsenidmobile.orca.core.applications.exception.VoteletException;
import hsenidmobile.orca.core.applications.voting.VoteResultSummary;
import hsenidmobile.orca.core.applications.voting.VotingResponseSpecification;
import hsenidmobile.orca.core.applications.voting.VotingService;
import hsenidmobile.orca.core.model.LocalizedMessage;
import hsenidmobile.orca.core.model.SubCategory;
import hsenidmobile.orca.core.model.message.Message;
import hsenidmobile.orca.core.model.message.MessageContext;
import hsenidmobile.orca.core.model.message.MoResponse;
import hsenidmobile.orca.core.repository.PersistentObject;
import hsenidmobile.orca.core.util.PropertyHolder;
import hsenidmobile.orca.core.util.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.Proxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static ch.lambdaj.Lambda.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 *
 * @version $LastChangedRevision$
 */
@Entity
@Table(name = "votelet_response_spec_result_summary")
@Proxy(lazy = false)
public class ResultSummaryVotingResponseSpecification extends PersistentObject implements VotingResponseSpecification {

	@Transient
	private static final Logger logger = LoggerFactory.getLogger(ResultSummaryVotingResponseSpecification.class);

	@OneToMany(cascade = CascadeType.ALL)
	@LazyCollection(LazyCollectionOption.FALSE)
	@Cascade(value = { org.hibernate.annotations.CascadeType.ALL, org.hibernate.annotations.CascadeType.DELETE_ORPHAN,
			org.hibernate.annotations.CascadeType.DELETE })
	@JoinTable(name = "voting_result_response_template")
	private List<LocalizedMessage> responseTemplate;

	@Override
	public MoResponse generateResponse(final VotingService votingService, Message message,
			SubCategory candidateDetails, MessageContext messageContext, Locale locale) throws VoteletException {
		logger.debug("Creating Result Summary Response message.");

		List<VoteResultSummary> votingSummary = votingService.getResultSummary();

		String localizedResponseTemplate = getResponseTemplate(locale, votingService);

		String responseStr = createResponseMessage(votingSummary, localizedResponseTemplate, messageContext.getApplication().getAppName());

		if (responseStr.length() > PropertyHolder.MAX_RESULT_SUMMARY_LENGTH) {
			logger.debug("Full result message is longer than[{}]chars, so generating response using top 3 results.",
					PropertyHolder.MAX_RESULT_SUMMARY_LENGTH);
			responseStr = createResponseMessage(getTop3Results(votingSummary), localizedResponseTemplate, messageContext.getApplication().getAppName());
		}

		return Util.createResponseMessage(message, responseStr);
	}

	private String createResponseMessage(List<VoteResultSummary> votingSummary, String localizedResponseTemplate, String appName) {
		StringBuilder resultTxtBuilder = createSummaryString(votingSummary);

		String responseStr = String.format(localizedResponseTemplate, appName, removeTrailingComma(resultTxtBuilder));
		return responseStr;
	}

	private List<VoteResultSummary> getTop3Results(List<VoteResultSummary> votingSummary) {
		votingSummary = sort(votingSummary, on(VoteResultSummary.class).getTotalVoteCount());
		if (votingSummary.size() > 3) {
			List<VoteResultSummary> top3 = new ArrayList<VoteResultSummary>(3);
			for (int i = 0; i < 3; i++) {
				top3.add(votingSummary.get(i));
			}

			return top3;
		} else {
			return votingSummary;
		}
	}

	private StringBuilder createSummaryString(List<VoteResultSummary> votingSummary) {
		StringBuilder resultTxtBuilder = new StringBuilder(160);

		for (VoteResultSummary voteResultSummary : votingSummary) {
			resultTxtBuilder.append(voteResultSummary.getCandidateName()).append(" : ")
					.append(voteResultSummary.getTotalVoteCount()).append(", ");
		}
		return resultTxtBuilder;
	}

	private String removeTrailingComma(StringBuilder resultTxtBuilder) {
		String resultStr = resultTxtBuilder.toString();
		if (resultStr.endsWith(", ")) {
			resultStr = resultStr.substring(0, resultStr.length() - 2);
		}
		return resultStr;
	}

	public void setResponseTemplate(List<LocalizedMessage> responseTemplate) {
		this.responseTemplate = responseTemplate;
	}

	private String getResponseTemplate(Locale locale, final VotingService votingService) {
		for (LocalizedMessage template : responseTemplate) {
			if (template.getLocale().equals(locale)) {
				return template.getMessage();
			}
		}
		logger.warn("Response template not found to requested Locale[{}] for VotingService[{}]", locale,
				votingService.getId());
		return "%s";
	}

}
