package hsenidmobile.orca.core.applications.voting.impl;

import hsenidmobile.orca.core.applications.voting.VoteletActivePeriod;
import hsenidmobile.orca.core.repository.PersistentObject;
import org.hibernate.annotations.Proxy;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * $LastChangedDate: 2009-10-13 10:15:52 +0530 (Tue, 13 Oct 2009) $
 * $LastChangedBy: romith $
 * $LastChangedRevision: 52686 $
 */
@Entity
@Table(name = "votelet_active_whole_day")
@Proxy(lazy = false)
public class VoteletActiveWholeDay extends PersistentObject implements VoteletActivePeriod {

    @Override
    public boolean isActive() {
        return true;
    }

    public boolean isCollide(VoteletActivePeriod activePeriod) {
        return true;
    }
}
