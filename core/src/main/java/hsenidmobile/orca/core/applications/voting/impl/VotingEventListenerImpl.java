/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.applications.voting.impl;

import hsenidmobile.orca.core.applications.voting.Vote;
import hsenidmobile.orca.core.applications.voting.VotingEventListener;
import hsenidmobile.orca.core.repository.VoteRepository;

/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class VotingEventListenerImpl implements VotingEventListener {

	private VoteRepository voteRepository;

	@Override
	public void successfulVote(long voteletId, Vote vote) {
		voteRepository.updateSummary(voteletId, vote);
	}

	public void setVoteRepository(VoteRepository voteRepository) {
		this.voteRepository = voteRepository;
	}



}
