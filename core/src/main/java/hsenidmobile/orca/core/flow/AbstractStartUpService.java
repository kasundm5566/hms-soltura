/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *
 */
package hsenidmobile.orca.core.flow;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * $LastChangedDat$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public abstract class AbstractStartUpService implements StartUpService {

    private int poolSize = 1;
    private long delay = 1000 * 60 * 10; //run at each 10 min as default
    private long period = 1000 * 60 * 10;
    private TimeUnit timeUnit = TimeUnit.MILLISECONDS;
    private String name;
    private ExecutorService executorService;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPoolSize() {
        return poolSize;
    }

    public long getDelay() {
        return delay;
    }

    public long getPeriod() {
        return period;
    }

    public TimeUnit getTimeUnit() {
        return timeUnit;
    }

    public void setPoolSize(int poolSize) {
        this.poolSize = poolSize;
    }

    public void setDelay(long delay) {
        this.delay = delay;
    }

    public void setPeriod(long period) {
        this.period = period;
    }

    public void setTimeUnit(TimeUnit timeUnit) {
        this.timeUnit = timeUnit;
    }

    public ExecutorService getExecutorService() {
        return executorService;
    }

    public void setExecutorService(ExecutorService executorService) {
        this.executorService = executorService;
    }
}
