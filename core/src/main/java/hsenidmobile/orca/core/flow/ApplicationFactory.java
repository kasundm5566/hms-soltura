/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.flow;

import hsenidmobile.orca.core.applications.ApplicationImpl;
import hsenidmobile.orca.core.applications.Service;
import hsenidmobile.orca.core.applications.alert.AlertService;
import hsenidmobile.orca.core.applications.request.RequestService;
import hsenidmobile.orca.core.applications.subscription.Subscription;
import hsenidmobile.orca.core.applications.subscription.SubscriptionServiceDataRepository;
import hsenidmobile.orca.core.applications.voting.VotingEventListener;
import hsenidmobile.orca.core.applications.voting.VotingService;
import hsenidmobile.orca.core.flow.dispatch.aventuraapi.AventuraApiMtMessageDispatcher;
import hsenidmobile.orca.core.model.Application;
import hsenidmobile.orca.core.repository.*;
import hsenidmobile.orca.core.services.ApplicationService;
import hsenidmobile.orca.core.services.AuthorValidationService;
import hsenidmobile.orca.core.services.MoResponseValidationService;
import hsenidmobile.orca.core.services.charging.ChargingService;

import hsenidmobile.orca.core.services.impl.AuthorValidationServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 *
 * @version $LastChangedRevision$
 */
public class ApplicationFactory {

	private static final Logger logger = LoggerFactory.getLogger(ApplicationFactory.class);
	private ApplicationService applicationService;
	private SubscriptionServiceDataRepository subscriptionDataService;
	private ChargingService chargingService;
	private AventuraApiMtMessageDispatcher aventuraApiMtMessageDispatcher;
	private AppRepository appRepository;
	private VoteRepository voteRepository;
	private VotingEventListener votingEventListener;
	private RequestServiceDataRepository requestServiceDataRepository;
	private RequestService requestService;
	private SdpApplicationRepository sdpApplicationRepository;
	private AlertDispatchedContentRepository alertDispatchedContentRepository;
    private MoResponseValidationService moResponseValidationService;
    private AppTrafficRepository appTrafficRepository;
    private AuthorValidationService authorValidationService = new AuthorValidationServiceImpl();

	/**
	 * Set all the helper service classes required by application.
	 *
	 * @param application
	 */
	public void assemble(Application application) {
		ApplicationImpl app = (ApplicationImpl) application;
		logger.debug("Application Fetched from DB[{}]", application);
		app.setApplicationRepository(appRepository);
        app.setAuthorValidationService(authorValidationService);

//		if (app.getMassageSendingChargingPolicy() != null) {
//			app.getMassageSendingChargingPolicy().setChargingService(chargingService);
//		}
//
//		if (app.getUserRegistrationChargingPolicy() != null) {
//			app.getUserRegistrationChargingPolicy().setChargingService(chargingService);
//		}

		setServiceSpecificData(app);
	}

	private void setServiceSpecificData(Application app) {

		Service service = app.getService();
		if (service == null) {
			logger.debug("Service is Null.");
			return;
		}
		service.setApplication(app);
		if (service instanceof Subscription) {
			Subscription subscription = (Subscription) service;
			subscription.setSubscriptionDataRepository(subscriptionDataService);
			subscription.setAventuraApiMtMessageDispatcher(aventuraApiMtMessageDispatcher);
		} else if (service instanceof AlertService) {
			AlertService alert = (AlertService) service;
			alert.setAventuraApiMtMessageDispatcher(aventuraApiMtMessageDispatcher);
			alert.setSdpApplicationRepository(sdpApplicationRepository);
			alert.setDispatchedContentRepository(alertDispatchedContentRepository);
            alert.setAppTrafficRepository(appTrafficRepository);
		} else if (service instanceof VotingService) {
			VotingService votingService = (VotingService) service;
			votingService.setVotingEventListener(votingEventListener);
			votingService.setAventuraApiMtMessageDispatcher(aventuraApiMtMessageDispatcher);
			votingService.setVoteRepository(voteRepository);
            votingService.setMoResponseValidationService(moResponseValidationService);
		} else if (service instanceof RequestService) {
			RequestService requestService = (RequestService) service;
			requestService.setRequestServiceDataRepository(requestServiceDataRepository);
			requestService.setAventuraApiMtMessageDispatcher(aventuraApiMtMessageDispatcher);
            requestService.setMoResponseValidationService(moResponseValidationService);
		}
	}

	public void setAventuraApiMtMessageDispatcher(AventuraApiMtMessageDispatcher aventuraApiMtMessageDispatcher) {
		this.aventuraApiMtMessageDispatcher = aventuraApiMtMessageDispatcher;
	}

	public void setApplicationService(ApplicationService applicationService) {
		this.applicationService = applicationService;
	}

	public void setSubscriptionDataService(SubscriptionServiceDataRepository subscriptionDataService) {
		this.subscriptionDataService = subscriptionDataService;
	}

	public void setChargingService(ChargingService chargingService) {
		this.chargingService = chargingService;
	}

	public void setAppRepository(AppRepository appRepository) {
		this.appRepository = appRepository;
	}

	public void setVoteRepository(VoteRepository voteRepository) {
		this.voteRepository = voteRepository;
	}

	public void setVotingEventListener(VotingEventListener votingEventListener) {
		this.votingEventListener = votingEventListener;
	}

	public void setRequestServiceDataRepository(RequestServiceDataRepository requestServiceDataRepository) {
		this.requestServiceDataRepository = requestServiceDataRepository;
	}

	public void setRequestService(RequestService requestService) {
		this.requestService = requestService;
	}

	public void setSdpApplicationRepository(SdpApplicationRepository sdpApplicationRepository) {
		this.sdpApplicationRepository = sdpApplicationRepository;
	}

	public void setAlertDispatchedContentRepository(AlertDispatchedContentRepository alertDispatchedContentRepository) {
		this.alertDispatchedContentRepository = alertDispatchedContentRepository;
	}

    public void setMoResponseValidationService(MoResponseValidationService moResponseValidationService) {
        this.moResponseValidationService = moResponseValidationService;
    }

    public void setAppTrafficRepository(AppTrafficRepository appTrafficRepository) {
        this.appTrafficRepository = appTrafficRepository;
    }

    public void setAuthorValidationService(AuthorValidationService authorValidationService) {
        this.authorValidationService = authorValidationService;
    }
}
