/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.flow;

import hsenidmobile.orca.core.applications.exception.ApplicationException;
import hsenidmobile.orca.core.flow.event.MessageEvent;
import hsenidmobile.orca.core.flow.event.MessageFlowEvent;
import hsenidmobile.orca.core.model.*;
import hsenidmobile.orca.core.model.InvalidRequest.InvalidReason;
import hsenidmobile.orca.core.model.message.AoRequest;
import hsenidmobile.orca.core.model.message.AppDataUpdateRequest;
import hsenidmobile.orca.core.model.message.Message;
import hsenidmobile.orca.core.model.message.RequestShowReplyRequest;
import hsenidmobile.orca.core.repository.AppRepository;
import hsenidmobile.orca.core.repository.SdpApplicationRepository;
import hsenidmobile.orca.core.services.InvalidRequestService;
import hsenidmobile.orca.core.util.PropertyHolder;
import hsenidmobile.orca.core.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Find the application corresponds to the the short-code, keyword(s)
 * combinations.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 *
 * @version $LastChangedRevision$
 */
public class ApplicationFinder implements MessageHandler {

    private static final int SDP_RK_STATUS_ACTIVE = 1;
    private static final Logger logger = LoggerFactory.getLogger(ApplicationFinder.class);
    private AppRepository appRepository;
    private ApplicationFactory applicationFactory;
    private SdpApplicationRepository sdpApplicationRepository;
    private InvalidRequestService invalidRequestService;
    private List<String> systemKeyWords;

    RoutingKey findApplicationRoutingKey(Msisdn shortCode, String messageText) throws ApplicationException {
        logger.debug("Finding applications for ShortCode[{}] and Message[{}].", shortCode, messageText);

        String keywordFragment = extractApplicationKeyword(messageText);

//		RoutingKey routingKey = findRoutingKey(shortCode, messageText, keywordFragment);
        RoutingKey routingKey = new RoutingKey(shortCode, keywordFragment);

        validateRoutingKey(routingKey, shortCode, messageText);

        return routingKey;

    }

    private void validateRoutingKey(RoutingKey routingKey, Msisdn shortCode, String messageText)
            throws ApplicationException {

        if (routingKey == null) {
            ApplicationException exception = new ApplicationException("No application found for ShortCode[" + shortCode
                    + "], and Message[" + messageText + "]", MessageFlowErrorReasons.ROUTING_KEY_NOT_ASSIGNED);
            throw exception;
        }
    }

//	private RoutingKey findRoutingKey(Msisdn shortCode, String messageText, String keywordFragment) {
//		logger.debug("Searching for RoutingKey in SDP Ncs[{}] ShortCode[{}] keyword[{}]", new Object[] {
//				PropertyHolder.SMSC_PD_NCS_ID, shortCode.getAddress(), keywordFragment });
//
//		try {
//			List<RoutingKey> possibleRoutingKeysStartingWithKeywordFragment = routingKeyRepository
//					.findKeywordStartWith(PropertyHolder.SMSC_PD_NCS_ID, shortCode.getAddress(), keywordFragment);
//
//			logger.debug("Number of RoutingKeys found for keywordFragment:[{}] size: [{}]", keywordFragment,
//					possibleRoutingKeysStartingWithKeywordFragment.size());
//
//			return getMatchingRoutingKey(possibleRoutingKeysStartingWithKeywordFragment, messageText);
//		} catch (RoutingKeyNotFoundException e) {
//			return null;
//		}
//	}

    private String extractApplicationKeyword(String messageText) {
        int indexOfSeparator = messageText.indexOf(PropertyHolder.MESSAGE_SPLITTER);
        String keywordFragment;
        if (indexOfSeparator > -1) {
            keywordFragment = messageText.substring(0, indexOfSeparator);
        } else {
            keywordFragment = messageText;
        }

        keywordFragment = keywordFragment.trim().toLowerCase();
        logger.debug("Message keyword fragment[{}]", keywordFragment);
        return keywordFragment;
    }

    private RoutingKey getMatchingRoutingKey(List<RoutingKey> allPossibleRoutingKeys, String messageText) {

        String formattedMsgTxt = formatMessageText(messageText);

        List<String> availableRoutingKeys = getAllPossibleKeywordStings(allPossibleRoutingKeys);

        List<String> matchingKeys = getPossibleMatchingKeywords(formattedMsgTxt, availableRoutingKeys);

        if (matchingKeys.size() == 1) {
            return getRoutingKeyForKeyword(allPossibleRoutingKeys, matchingKeys.get(0));
        } else {
            return getRoutingKeyForKeyword(allPossibleRoutingKeys, getLongestMatchingKeyword(matchingKeys));
        }
    }

    private String getLongestMatchingKeyword(List<String> matchingKeys) {
        String longestMatchingKeyword = "";
        for (String keywords : matchingKeys) {
            if (keywords.length() > longestMatchingKeyword.length()) {
                longestMatchingKeyword = keywords;
            }
        }
        return longestMatchingKeyword;
    }

    private RoutingKey getRoutingKeyForKeyword(List<RoutingKey> allPossibleRoutingKeys, String keyword) {
        for (RoutingKey routingKey : allPossibleRoutingKeys) {
            if (routingKey.getKeyword().equals(keyword)/* && routingKey.isActive()*/) {
                return routingKey;
            }
        }

        return null;
    }

    private List<String> getPossibleMatchingKeywords(String formattedMsgTxt, List<String> availableRoutingKeys) {
        List<String> matchingKeys = new ArrayList<String>();
        for (String key : availableRoutingKeys) {
            if (formattedMsgTxt.startsWith(key)) {
                matchingKeys.add(key);
            }
        }
        return matchingKeys;
    }

    private List<String> getAllPossibleKeywordStings(List<RoutingKey> allPossibleRoutingKeys) {
        List<String> availableRoutingKeys = new ArrayList<String>();

        for (RoutingKey routingKey : allPossibleRoutingKeys) {
            logger.trace("Found app with RK[{}]", routingKey.getKeyword());
            availableRoutingKeys.add(routingKey.getKeyword());
        }
        return availableRoutingKeys;
    }

    /**
     * Remove unwanted spaces, and change message to lower text.
     *
     * @param messageText
     * @return
     */
    private String formatMessageText(String messageText) {
        String messageTextLowerCase = messageText.toLowerCase();
        String[] splittedTxt = messageTextLowerCase.split(PropertyHolder.MESSAGE_SPLITTER);
        StringBuilder formattedMsgBuilder = new StringBuilder(200);
        for (String string : splittedTxt) {
            if (!Util.isEmpty(string)) {
                formattedMsgBuilder.append(string).append(PropertyHolder.MESSAGE_SPLITTER);
            }
        }

        messageTextLowerCase = formattedMsgBuilder.toString().trim();
        return messageTextLowerCase;
    }

    /**
     * Application repository used to fetch applicators from DB.
     *
     * @param appRepository
     */
    public void setAppRepository(AppRepository appRepository) {
        this.appRepository = appRepository;
    }

    /**
     * Factory used to set dependencies required by the application.
     *
     * @param applicationFactory
     */
    public void setApplicationFactory(ApplicationFactory applicationFactory) {
        this.applicationFactory = applicationFactory;
    }

    private void removeAppRkFromCommandDataText(Message message, String applicationServiceKeyword) {

        if (!Util.isEmpty(applicationServiceKeyword)) {
            String[] splittedServiceKeyword = applicationServiceKeyword.split(PropertyHolder.MESSAGE_SPLITTER);
            String textWithRk = message.getMessage();

            StringTokenizer msgTokenizer = new StringTokenizer(message.getMessage());
            List<String> partsTobeRemoved = new ArrayList<String>();
            for (int i = 0; i < splittedServiceKeyword.length; i++) {
                if (msgTokenizer.hasMoreTokens()) {
                    partsTobeRemoved.add(msgTokenizer.nextToken());
                }
            }

            for (String partToRemove : partsTobeRemoved) {
                textWithRk = textWithRk.replaceFirst(partToRemove, "");
            }
            message.setMessage(textWithRk.trim());
        }
    }

    /**
     * Handle received Mo messages.
     */
    public void handleDownStream(PipelineContext pipelineContext, MessageEvent event) throws OrcaException {
        logger.debug("Handling DownStream pipelineContext[{}] event[{}]", pipelineContext, event);
        if (isMoRequestEvent(event)) {
            Message message = (Message) event.getMessage();
            String refinedMessageText = message.getMessage();

            logger.debug("Message received for decoding message[{}]", refinedMessageText);
            if (isApplicationMessageCommand(refinedMessageText)) {
                handleApplicationCommandMessage(event, message, refinedMessageText);
            } else {
                handleSystemCommandMessage(event);
            }

        } else if (isHttpAppDataUpdateRequest(event)) {
            handleHttpUpdateCommandMessage(event);

        } else if (isHttpRequestShowReplyRequest(event)) {
            handleHttpRequestReplyCommandMessage(event);
        } else {
            logger.debug("Message passed to handle is [{}], is NOT a [{}]; ignored", event.getMessage(),
                    Message.class.getName());
        }

        pipelineContext.handleDownStream(event);
    }

    private void handleHttpUpdateCommandMessage(MessageEvent event) throws ApplicationException {
        AppDataUpdateRequest dataUpdateRequest = (AppDataUpdateRequest) event.getMessage();
        Application app = appRepository.findAppByAppId(dataUpdateRequest.getAppId());
        if (app != null) {
            applicationFactory.assemble(app);
        } else {
            handleApplicationNotFoundForHttpUpdate(event, dataUpdateRequest);
        }
        event.getMessageContext().setApplication(app);
    }

    private void handleHttpRequestReplyCommandMessage(MessageEvent event) throws ApplicationException {
        RequestShowReplyRequest requestShowReplyRequest = (RequestShowReplyRequest) event.getMessage();
        Application app = appRepository.findAppByAppId(requestShowReplyRequest.getAppId());
        if (app != null) {
            applicationFactory.assemble(app);
        } else {
            handleApplicationNotFoundForHttpRequestReply(event, requestShowReplyRequest);
        }
        event.getMessageContext().setApplication(app);
    }

    private void handleSystemCommandMessage(MessageEvent event) {
        logger.debug("Received message is not a application message command.");
        MessageFlowEvent requestEvent = (MessageFlowEvent) event;
        requestEvent.setSystemCommand(true);
    }

    private void handleApplicationCommandMessage(MessageEvent event, Message message, String refinedMessageText)
            throws ApplicationException {

        RoutingKey routingKey = findApplicationRoutingKey(message.getSenderAddress(), refinedMessageText);

        String orcaAppId = sdpApplicationRepository.getOrcaAppId(message.getAppId());
        Application app = appRepository.findAppByAppId(orcaAppId);

        if (app != null) {
            logger.debug("Is Application Status: [{}] Expirable: [{}]", app.getStatus(), app.isExpirableApp());
            applicationFactory.assemble(app);
            event.getMessageContext().setApplication(app);
            removeAppRkFromCommandDataText(message, routingKey.getKeyword());
            appRepository.updateMoSummary(app.getAppId());
        } else {
            handleApplicationNotFoundForMo(event, message, refinedMessageText);
        }
    }

    private void handleApplicationNotFoundForHttpUpdate(MessageEvent event, AppDataUpdateRequest dataUpdateRequest)
            throws ApplicationException {
        ApplicationException exception = new ApplicationException(
                "No application found for Http Update request, appId[" + dataUpdateRequest.getAppId() + "]",
                MessageFlowErrorReasons.APP_NOT_FOUND, event);
        exception.addParameter("app_id", dataUpdateRequest.getAppId());
        throw exception;
    }

    private void handleApplicationNotFoundForHttpRequestReply(MessageEvent event, RequestShowReplyRequest requestShowReplyRequest)
            throws ApplicationException {
        ApplicationException exception = new ApplicationException(
                "No application found for Http request show reply request, appId[" + requestShowReplyRequest.getAppId() + "]",
                MessageFlowErrorReasons.APP_NOT_FOUND, event);
        exception.addParameter("app_id", requestShowReplyRequest.getAppId());
        throw exception;
    }

    private void handleApplicationNotFoundForMo(MessageEvent event, Message message, String refinedMessageText)
            throws ApplicationException {
        logError(message, InvalidReason.APP_NOT_FOUND);
        ApplicationException exception = new ApplicationException(String.format(PropertyHolder.NO_APP_FOUND_ERROR_MSG,
                refinedMessageText), MessageFlowErrorReasons.APP_NOT_FOUND, event);
        exception.addParameter("app_id", refinedMessageText);
        throw exception;
    }

    private void handleApplicationNotActive(MessageEvent event, Message message, Application app)
            throws ApplicationException {
        logError(message, InvalidReason.APP_NOT_ACTIVE);
        ApplicationException exception = new ApplicationException(String.format(
                PropertyHolder.APP_NOT_ACTIVE_ERROR_MSG, app.getAppName()), MessageFlowErrorReasons.APP_NOT_ACTIVE,
                event);
        exception.addParameter("app_id", app.getAppName());
        throw exception;
    }

    private void handleRoutingKeyNotAssigned(Message message, RoutingKey routingKey, MessageEvent event)
            throws ApplicationException {
        logger.debug("Requested RK[{}|{}] is not in ASSIGNED state.", routingKey.getKeyword(), routingKey.getShortCode());
        logError(message, InvalidReason.ROUTING_KEY_NOT_ASSIGNED);
        ApplicationException exception = new ApplicationException("RK not ASSIGNED [" + routingKey.getKeyword() + "/"
                + routingKey.getKeyword() + "]", MessageFlowErrorReasons.ROUTING_KEY_NOT_ASSIGNED, event);
        exception.addParameter("shortcode", routingKey.getShortCode());
        exception.addParameter("keyword", routingKey.getKeyword());
        throw exception;
    }

    private void logError(Message message, InvalidReason invalidReason) {
        InvalidRequest invalidRequest = new InvalidRequest(message.getCorrelationId());
        invalidRequest.setReceivedTime(System.currentTimeMillis());
        invalidRequest.setSourceMsisdn(message.getSenderAddress());
        if (message.getReceiverAddresses() != null) {
            invalidRequest.setDestinationMsisdn(message.getReceiverAddresses().get(0));
        }
        invalidRequest.setReason(invalidReason);
        invalidRequest.setMessageText(message.getMessage());
        invalidRequestService.invalidRequestReceived(invalidRequest);
    }

    private boolean isApplicationMessageCommand(String messageText) {
        if (systemKeyWords == null || systemKeyWords.isEmpty()) {
            return true;
        }

        int indexOfSeparator = messageText.indexOf(PropertyHolder.MESSAGE_SPLITTER);
        String keywordFragment;
        if (indexOfSeparator > -1) {

            keywordFragment = messageText.substring(0, indexOfSeparator);

        } else {
            keywordFragment = messageText;
        }
        keywordFragment = keywordFragment.toLowerCase().trim();
        return !systemKeyWords.contains(keywordFragment);
    }

    private boolean isMoRequestEvent(MessageEvent event) {
        return event instanceof MessageFlowEvent && event.getMessage() instanceof Message;
    }

    private boolean isHttpAppDataUpdateRequest(MessageEvent event) {
        return event instanceof MessageFlowEvent && event.getMessage() instanceof AppDataUpdateRequest;
    }

    private boolean isHttpRequestShowReplyRequest(MessageEvent event) {
        return event instanceof MessageFlowEvent && event.getMessage() instanceof RequestShowReplyRequest;
    }

    /**
     * Handle received Mt Messages.
     */
    public void handleUpStream(PipelineContext pipelineContext, MessageEvent event) throws OrcaException {
        logger.debug("Handling UpStream pipelineContext[{}] event[{}]", pipelineContext, event);

        if (event.getMessage() instanceof AoRequest) {
            AoRequest aoMessage = (AoRequest) event.getMessage();
            try {
                Application app = find(aoMessage.getAppId());
                event.getMessageContext().setApplication(app);
            } catch (ApplicationException ex) {
                ex.addParameter("app_id", aoMessage.getAppId());
                throw ex;
            }
        } else {
            logger.warn("Message handle is [{}], is NOT a [{}]; ignored", event.getMessage(), AoRequest.class.getName());
        }

        pipelineContext.handleUpStream(event);
    }

    private Application find(String appId) throws ApplicationException {
        logger.debug("Finding applications with AppId[{}]", appId);

        Application application = appRepository.findAppByAppId(appId);

        if (application != null) {
            applicationFactory.assemble(application);
        } else {
            throw new ApplicationException("Application not found", MessageFlowErrorReasons.APP_NOT_FOUND);
        }
        return application;
    }

    public void setSdpApplicationRepository(SdpApplicationRepository sdpApplicationRepository) {
        this.sdpApplicationRepository = sdpApplicationRepository;
    }

    public void setSystemKeyWords(List<String> systemKeyWords) {
        this.systemKeyWords = systemKeyWords;
    }

    public void setInvalidRequestService(InvalidRequestService invalidRequestService) {
        this.invalidRequestService = invalidRequestService;
    }

}
