/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.flow;

import hsenidmobile.orca.core.applications.exception.ApplicationException;
import hsenidmobile.orca.core.flow.event.MessageEvent;
import hsenidmobile.orca.core.flow.event.MessageFlowEvent;
import hsenidmobile.orca.core.model.Application;
import hsenidmobile.orca.core.model.InvalidRequest;
import hsenidmobile.orca.core.model.InvalidRequest.InvalidReason;
import hsenidmobile.orca.core.model.OrcaException;
import hsenidmobile.orca.core.model.SubCategory;
import hsenidmobile.orca.core.model.message.Message;
import hsenidmobile.orca.core.services.InvalidRequestService;
import hsenidmobile.orca.core.util.PropertyHolder;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Extract Subkeywords given in the message text based on the application.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 *
 * @version $LastChangedRevision$
 */
public class ApplicationSubkeywordExtractor implements MessageHandler {

	private static final Logger logger = LoggerFactory.getLogger(ApplicationSubkeywordExtractor.class);
	private InvalidRequestService invalidRequestService;
	public List<String> specialSysWideKeywords;

	@Override
	public void handleDownStream(PipelineContext pipelineContext, MessageEvent event) throws OrcaException {
		if (isMoRequestEvent(event)) {
			MessageFlowEvent requestEvent = (MessageFlowEvent) event;
			if (!requestEvent.isSystemCommand()) {
				Application app = requestEvent.getMessageContext().getApplication();
				Message msg = (Message) (requestEvent.getMessage());

				setSubkeyword(app, requestEvent);
			} else {
				logger.debug("Received is system command, ignoring keyword extraction.");
			}

		} else {
			logger.debug("Message handle is [{}], is NOT a [{}]; ignored", event.getMessage(), Message.class.getName());
		}
		pipelineContext.handleDownStream(event);
	}

	private boolean isMoRequestEvent(MessageEvent event) {
		return event instanceof MessageFlowEvent && event.getMessage() instanceof Message;
	}

	/**
	 * subkey1 this the content subkey1 reg si subkey1 unreg si
	 *
	 * @param app
	 * @param messageDataTxt
	 * @return
	 * @throws ApplicationException
	 */
	protected void setSubkeyword(Application app, MessageFlowEvent requestEvent) throws ApplicationException {

        Message msg = (Message) requestEvent.getMessage();

		SubCategoryExtractionResult extractionResult = null;

		if (!app.getService().isSubCategoriesSupported()) {
			extractionResult = extractWhenSubCategoryNotSupported(msg);
		} else {
			extractionResult = extractWhenSubCategorySupported(app, msg);
		}

		requestEvent.setSubKeyword(extractionResult.getExtractedSubkeyword());
		requestEvent.setSystemWideKeyword(extractionResult.isSubkeySystemWideKey());
		msg.setMessage(extractionResult.getDataTextAfterExecration().trim());
	}

	private SubCategoryExtractionResult extractWhenSubCategorySupported(Application app, Message msg)
			throws ApplicationException {
		String messageDataTxt = msg.getMessage();
		String possibleKeyword = extractKeywordPortion(messageDataTxt);

		SubCategoryExtractionResult extractionResult;
		if (specialSysWideKeywords.contains(possibleKeyword)) {
			if (logger.isDebugEnabled()) {
				logger.debug("Provided keyword is special system wide command keyword[" + possibleKeyword
						+ "], NO keyword extraction done.");
			}
			// We do not remove the keyword part from the message text, that
			// will be done at CommandBuilder
			extractionResult = new SubCategoryExtractionResult(possibleKeyword, true, messageDataTxt);
		} else {
			List<SubCategory> allSupportedKeywords = getAllSupportedSubCategories(app);
			try {
				extractionResult = new SubCategoryExtractionResult(matchingKeyword(possibleKeyword,
						allSupportedKeywords), false, messageDataTxt.substring(possibleKeyword.length(),
						messageDataTxt.length()));

			} catch (ApplicationException e) {
				saveInvalidReqDetails(app, msg);
				e.addParameter("subkeyword", possibleKeyword);
				e.addParameter("app_id", app.getAppName());
				throw e;
			}
		}
		return extractionResult;
	}

	private SubCategoryExtractionResult extractWhenSubCategoryNotSupported(Message msg) {
		SubCategoryExtractionResult extractionResult;
		String messageDataTxt = msg.getMessage();
		String possibleKeyword = extractKeywordPortion(messageDataTxt);
		if (specialSysWideKeywords.contains(possibleKeyword)) {
			if (logger.isDebugEnabled()) {
				logger.debug("Provided keyword is special system wide keyword[" + possibleKeyword + "]");
			}
			// We do not remove the keyword part from the message text, that
			// will be done at CommandBuilder
			extractionResult = new SubCategoryExtractionResult(possibleKeyword, true, messageDataTxt);
		} else {
			extractionResult = new SubCategoryExtractionResult("", false, messageDataTxt);
		}
		return extractionResult;
	}

	private List<SubCategory> getAllSupportedSubCategories(Application app) {
		List<SubCategory> allSupportedKeywords = app.getService().getSupportedSubCategories();
		logSupportedKeywords(allSupportedKeywords);
		return allSupportedKeywords;
	}

	private void saveInvalidReqDetails(Application app, Message msg) {
		InvalidRequest invalidRequest = new InvalidRequest(msg.getCorrelationId());
		invalidRequest.setReceivedTime(System.currentTimeMillis());
		invalidRequest.setAppId(app.getAppId());
		invalidRequest.setMessageText(msg.getMessage());
		invalidRequest.setSourceMsisdn(msg.getSenderAddress());
		invalidRequest.setReason(InvalidReason.INVALID_KEYWORD);
		invalidRequestService.invalidRequestReceived(invalidRequest);
	}

	private String matchingKeyword(String possibleKeyword, List<SubCategory> keywords) throws ApplicationException {
		for (SubCategory keyword : keywords) {
			if (keyword.isMatch(possibleKeyword)) {
				return keyword.getKeyword();
			}
		}
		throw new ApplicationException("Unsupported Keyword[" + possibleKeyword + "]",
				MessageFlowErrorReasons.SUB_KEYWORD_NOT_FOUND);
	}

	private void logSupportedKeywords(List<SubCategory> allSupportedKeywords) {
		if (logger.isDebugEnabled()) {
			StringBuilder sb = new StringBuilder(200);
			sb.append("Supported sub-keywords[");
			for (SubCategory key : allSupportedKeywords) {
				sb.append(key.getKeyword()).append(", ");
			}
			sb.append("]");
			logger.debug(sb.toString());
		}
	}

	private String extractKeywordPortion(String messageDataTxt) {
		String dataTxt = messageDataTxt.trim();
		String possibleKeyword;
		int indexOfSplitter = dataTxt.indexOf(PropertyHolder.MESSAGE_SPLITTER);
		if (indexOfSplitter > -1) {
			possibleKeyword = dataTxt.substring(0, indexOfSplitter);
		} else {
			possibleKeyword = dataTxt;
		}
		possibleKeyword = possibleKeyword.trim().toLowerCase();
		if (logger.isDebugEnabled()) {
			logger.debug("Found possible keyword from received message[" + possibleKeyword + "]");
		}
		return possibleKeyword;
	}

	public void setInvalidRequestService(InvalidRequestService invalidRequestService) {
		this.invalidRequestService = invalidRequestService;
	}

	public void setSpecialSysWideKeywords(List<String> specialSysWideKeywords) {
		this.specialSysWideKeywords = specialSysWideKeywords;
	}

	@Override
	public void handleUpStream(PipelineContext pipelineContext, MessageEvent event) throws OrcaException,
			ApplicationException {
		throw new UnsupportedOperationException("UpStream handling not suppored.");

	}

	private class SubCategoryExtractionResult {
		private String extractedSubkeyword;
		private boolean subkeySystemWideKey;
		private String dataTextAfterExecration;

		public SubCategoryExtractionResult(String extractedSubkeyword, boolean subkeySystemWideKey,
				String dataTextAfterExecration) {
			this.extractedSubkeyword = extractedSubkeyword;
			this.subkeySystemWideKey = subkeySystemWideKey;
			this.dataTextAfterExecration = dataTextAfterExecration;
		}

		public String getExtractedSubkeyword() {
			return extractedSubkeyword;
		}

		public boolean isSubkeySystemWideKey() {
			return subkeySystemWideKey;
		}

		public String getDataTextAfterExecration() {
			return dataTextAfterExecration;
		}

	}

}
