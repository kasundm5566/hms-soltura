/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.flow;

import hsenidmobile.orca.core.applications.exception.ApplicationException;
import hsenidmobile.orca.core.applications.request.RequestService;
import hsenidmobile.orca.core.flow.event.MessageEvent;
import hsenidmobile.orca.core.flow.event.MessageFlowEvent;
import hsenidmobile.orca.core.model.Application;
import hsenidmobile.orca.core.model.OrcaException;
import hsenidmobile.orca.core.model.message.AppDataUpdateRequest;

import hsenidmobile.orca.core.model.message.RequestShowReplyRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 *
 * @version $LastChangedRevision$
 */
public class HttpUpdateRequestDispatcher implements MessageHandler {

	private static final Logger logger = LoggerFactory.getLogger(HttpUpdateRequestDispatcher.class);

	@Override
	public void handleDownStream(PipelineContext pipelineContext, MessageEvent event) throws OrcaException {
		logger.debug("Handling DownStream pipelineContext[{}] event[{}]", pipelineContext, event);
		if (event instanceof MessageFlowEvent && event.getMessage() instanceof AppDataUpdateRequest) {

			MessageFlowEvent<AppDataUpdateRequest> messageFlowEvent = (MessageFlowEvent<AppDataUpdateRequest>) event;
			AppDataUpdateRequest updateRequest = messageFlowEvent.getMessage();
			Application application = messageFlowEvent.getMessageContext().getApplication();
			application.updateServiceData(updateRequest.getUpdateContent());
		}

        if (event instanceof MessageFlowEvent && event.getMessage() instanceof RequestShowReplyRequest){
            MessageFlowEvent<RequestShowReplyRequest> messageFlowEvent = (MessageFlowEvent<RequestShowReplyRequest>) event;
			Application application = messageFlowEvent.getMessageContext().getApplication();
			((RequestService) application.getService()).sendSingleReply(messageFlowEvent);
        }
		pipelineContext.handleDownStream(event);
	}

	@Override
	public void handleUpStream(PipelineContext pipelineContext, MessageEvent event) throws OrcaException,
			ApplicationException {
		throw new UnsupportedOperationException("UpStream handling not supported.");

	}

}
