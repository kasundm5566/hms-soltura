/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.flow;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * $LastChangedDate$ $LastChangedBy$ $LastChangedRevision$
 *
 */
public enum MessageFlowErrorReasons {
    ROUTING_KEY_NOT_ASSIGNED,
    APP_NOT_FOUND("app_id"),
    APP_NOT_ACTIVE("app_id"),
    MESSAGE_TYPE_NOT_SUPPORTED("type"),
    SUB_KEYWORD_NOT_FOUND("app_id", "subkey"),
    VOTE_CANDIDATE_NOT_FOUND("app_id", "subkey"),
    SUB_KEYWORD_NOT_SUPPORTED,
    INVALID_REQUEST,
    LANGUAGE_NOT_SUPPORTED("app_id","lang"),
    INVALID_AUTHOR("app_id", "msisdn"),
    NO_QUESTION_FOUND("app_id"),
    NO_ACTIVE_QUESTION_FOUND("app_id"),
    SUBSCRIPTION_DATA_NOT_AVAILABLE("app_id"),
    SUBSCRIPTION_DATA_NOT_DISPATCHABLE_NOW("app_id"),
    SUBSCRIPTION_PERIODICITY_NOT_SUPPORTED("app_id", "periodicity"),
    NO_LAST_SENT_ALERT_FOUND("app_id"),
    NO_ACTIVE_VOTELET_FOUND("app_id"),
    NO_CANDIDATE_ASSIGNED("app_id"),
    VOTELET_COLLIDE("app_id"),
    ALREADY_VOTED("app_id"),
    VOTING_RESULT_PRIVATE("app_id"),
    REQUEST_SHOW_MESSAGE_ID_NOT_FOUND("app_id"),
    ALERT_CONTENT_EMPTY,
    EMPTY_CONTENT,
    SMS_UPDATE_NOT_SUPPORTED,
    INVALID_VOTE("app_id"),
    INVALID_REQUEST_MESSAGE("app_id"),
    DAYS_EXCEEDED("app_id"),
    MONTH_EXCEEDED("app_id");

    private List<String> requiredParamList = new ArrayList<String>();

    private MessageFlowErrorReasons(String... requiredParams){
        requiredParamList = Arrays.asList(requiredParams);
    }

    public List<String> getRequiredParamList() {
        return requiredParamList;
    }

}
