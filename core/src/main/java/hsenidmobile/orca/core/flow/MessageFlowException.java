/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.flow;

import hsenidmobile.orca.core.flow.event.MessageEvent;
import hsenidmobile.orca.core.model.OrcaException;

import java.util.HashMap;
import java.util.Map;

/**
 * Base class for all the exceptions generated related to MessageFlow.
 *
 * $LastChangedDate$ $LastChangedBy$ $LastChangedRevision$
 *
 */
public class MessageFlowException extends OrcaException {

	private MessageEvent messageFlowEvent;
	private MessageFlowErrorReasons reason;
	private Map<String, Object> additionalParameters = new HashMap<String, Object>();

	public MessageFlowException(String message, MessageFlowErrorReasons reason, MessageEvent messageFlowEvent) {
		super(message);
		this.reason = reason;
		this.messageFlowEvent = messageFlowEvent;
	}

	public MessageFlowException(String message, MessageFlowErrorReasons reason) {
		super(message);
		this.reason = reason;
	}

	public MessageEvent getMessageFlowEvent() {
		return messageFlowEvent;
	}

	public void setMessageFlowEvent(MessageEvent messageFlowEvent) {
		this.messageFlowEvent = messageFlowEvent;
	}

	public MessageFlowErrorReasons getReason() {
		return reason;
	}

	public void addParameter(String key, Object value) {
		additionalParameters.put(key, value);
	}

	public Object getParameter(String key) {
		return additionalParameters.get(key);
	}

	public Map<String, Object> getAdditionalParameters() {
		return additionalParameters;
	}

}
