/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.flow;

import hsenidmobile.orca.core.applications.exception.ApplicationException;
import hsenidmobile.orca.core.flow.event.MessageEvent;
import hsenidmobile.orca.core.model.OrcaException;

/**
 * This handler is
 *
 * $LastChangedDate$
 * $LastChangedBy$
 *
 * @version $LastChangedRevision$
 */
public interface MessageHandler {

	/**
	 * Processes up-stream(Orca -> SDP)(MT) message request.
	 *
	 * @param pipelineContext
	 * @param event
	 * @throws OrcaException
	 */
	void handleUpStream(PipelineContext pipelineContext, MessageEvent event) throws OrcaException, ApplicationException;

	/**
	 * Processes down-stream(SDP -> Orca)(MO) message request
	 *
	 * @param pipelineContext
	 * @param event
	 * @throws OrcaException
	 */
	void handleDownStream(PipelineContext pipelineContext, MessageEvent event) throws OrcaException;
}
