/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 *
 */
package hsenidmobile.orca.core.flow;

import hsenidmobile.orca.core.applications.exception.ApplicationException;
import hsenidmobile.orca.core.flow.command.Command;
import hsenidmobile.orca.core.flow.event.CommandEvent;
import hsenidmobile.orca.core.flow.event.MessageEvent;
import hsenidmobile.orca.core.flow.event.MessageFlowEvent;
import hsenidmobile.orca.core.flow.event.MoResponseEvent;
import hsenidmobile.orca.core.model.OrcaException;
import hsenidmobile.orca.core.model.message.Message;
import hsenidmobile.orca.core.model.message.MoResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 *
 */
public class MoRequestDispatcher implements MessageHandler {

	private static final Logger logger = LoggerFactory.getLogger(MoRequestDispatcher.class);

	public void handleDownStream(PipelineContext pipelineContext, MessageEvent event) throws OrcaException {
		logger.debug("Handling DownStream pipelineContext[{}] event[{}]", pipelineContext, event);
		if (event instanceof MessageFlowEvent && event.getMessage() instanceof Message) {
			Command command = ((MessageFlowEvent) event).getCommand();

			CommandEvent<Message> commandEvent = new CommandEvent<Message>(((MessageFlowEvent) event));

			MoResponse responseMessage = command.execute(commandEvent);

			MoResponseEvent responseEvent = new MoResponseEvent(responseMessage, event.getMessageContext());
			event = responseEvent;
		}
		pipelineContext.handleDownStream(event);
	}

	@Override
	public void handleUpStream(PipelineContext pipelineContext, MessageEvent event) throws OrcaException,
			ApplicationException {
		throw new UnsupportedOperationException("UpStream handling not supported.");
	}
}