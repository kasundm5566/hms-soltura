/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 *
 */
package hsenidmobile.orca.core.flow;

import hsenidmobile.orca.core.applications.exception.ApplicationException;
import hsenidmobile.orca.core.flow.dispatch.aventuraapi.AventuraApiMtMessageDispatcher;
import hsenidmobile.orca.core.flow.dispatch.lb.RoundRobinLb;
import hsenidmobile.orca.core.flow.event.MessageEvent;
import hsenidmobile.orca.core.model.OrcaException;
import hsenidmobile.orca.core.model.message.AoRequest;
import hsenidmobile.orca.core.model.message.MoResponse;
import hsenidmobile.orca.core.model.message.MtRequest;
import hsenidmobile.orca.core.model.message.RegistrationResponse;
import hsenidmobile.orca.core.model.message.SystemCommandResponce;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Send Mt messages to the network.
 *
 * $LastChangedDate: $
 * $LastChangedBy: $
 *
 * @version $LastChangedRevision: $
 */
public class MtRequestHandler implements MessageHandler {

	private static final Logger logger = LoggerFactory.getLogger(MtRequestHandler.class);
	private RoundRobinLb<MtRequest> rrMessageStore;
	private AventuraApiMtMessageDispatcher aventuraApiMtMessageDispatcher;

	@Override
	public void handleUpStream(PipelineContext pipelineContext, MessageEvent event) throws OrcaException {
		logger.debug("Handling UpStream Flow.");

		AoRequest aoRequest = (AoRequest) event.getMessage();
		// Do charging and create MtRequest
		MtRequest mtRequest = new MtRequest(aoRequest.getMessage(), null);
		rrMessageStore.putMessage(event.getMessageContext().getApplication().getAppId(), mtRequest);
		pipelineContext.handleUpStream(event);
	}

	@Override
	public void handleDownStream(PipelineContext pipelineContext, MessageEvent event) throws OrcaException {
		logger.debug("Handling DownStream Flow.");

		if (event.getMessage() instanceof RegistrationResponse) {
			RegistrationResponse regResponse = (RegistrationResponse) event.getMessage();

			submitMessageToLb(regResponse.getResponseForSubscriptionRequest(), event.getMessageContext()
					.getApplication().getAppId());
			submitMessageToLb(regResponse.getDataResponse(), event.getMessageContext().getApplication().getAppId());

		} else if (event.getMessage() instanceof SystemCommandResponce) {
			MoResponse moResponse = (MoResponse) event.getMessage();
			submitMessageToLb(moResponse.getDataResponse(), "SYSTEM");

		} else if (event.getMessage() instanceof MoResponse) {
			MoResponse moResponse = (MoResponse) event.getMessage();
			submitMessageToLb(moResponse.getDataResponse(), event.getMessageContext().getApplication().getAppId());

		} else {
			throw new ApplicationException("Unsupported message type[" + event.getMessage() + "], supports only["
					+ RegistrationResponse.class.getName() + "] and [" + MoResponse.class.getName() + "].",
					MessageFlowErrorReasons.INVALID_REQUEST);
		}
		pipelineContext.handleDownStream(event);
	}

	private void submitMessageToLb(MtRequest message, String appId) {
		if (message != null) {
			rrMessageStore.putMessage(appId, message);
		} else {
			logger.debug("Message given to submit to LB is NULL, so ignored.");
		}
	}

	public void setRrMessageStore(RoundRobinLb<MtRequest> rrMessageStore) {
		this.rrMessageStore = rrMessageStore;
	}

	public void setAventuraApiMtMessageDispatcher(AventuraApiMtMessageDispatcher aventuraApiMtMessageDispatcher) {
		this.aventuraApiMtMessageDispatcher = aventuraApiMtMessageDispatcher;
	}
}