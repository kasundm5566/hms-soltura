/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$ 
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.flow;

import java.util.List;

import hsenidmobile.orca.core.model.message.Message;

/**
 * Send Mt message to the network
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public interface MtRequestSender {

	void send(Message message, String appId);
	
	void send(List<Message> messages, String appId);
}
