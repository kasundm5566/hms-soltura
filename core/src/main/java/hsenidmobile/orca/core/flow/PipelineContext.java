/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.flow;

import hsenidmobile.orca.core.flow.event.MessageEvent;
import hsenidmobile.orca.core.model.OrcaException;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 *
 * @version $LastChangedRevision$
 */
public class PipelineContext {

	volatile PipelineContext next;
	volatile PipelineContext prev;
	private final String name;
	private final MessageHandler handler;

	/**
	 * Create the Context
	 *
	 * @param prev
	 * @param next
	 * @param name
	 * @param handler
	 */
	public PipelineContext(PipelineContext prev, PipelineContext next, String name, MessageHandler handler) {

		if (name == null) {
			throw new NullPointerException("name");
		}
		if (handler == null) {
			throw new NullPointerException("handler");
		}

		this.prev = prev;
		this.next = next;
		this.name = name;
		this.handler = handler;
	}

	public PipelineContext(String name, MessageHandler handler) {
		if (name == null) {
			throw new NullPointerException("name");
		}
		if (handler == null) {
			throw new NullPointerException("handler");
		}

		this.name = name;
		this.handler = handler;
	}

	/**
	 * Get the next context in the pipeline
	 *
	 * @return
	 */
	public PipelineContext getNext() {
		return next;
	}

	/**
	 * Get the previous context in the pipeline
	 *
	 * @return
	 */
	public PipelineContext getPrev() {
		return prev;
	}

	/**
	 * Name of this Context
	 *
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * Handler attached to this context
	 *
	 * @return
	 */
	public MessageHandler getHandler() {
		return handler;
	}

	/**
	 * Set next Handler
	 *
	 * @param next
	 */
	public void setNext(PipelineContext next) {
		this.next = next;
	}

	/**
	 * Set previous handler
	 *
	 * @param prev
	 */
	public void setPrev(PipelineContext prev) {
		this.prev = prev;
	}

	/**
	 * Handle given {@link MessageEvent} as a MT using the handler attached to
	 * this context.
	 *
	 * @param event
	 * @throws OrcaException
	 */
	public void handleUpStream(MessageEvent event) throws OrcaException {
		if (next != null) {
			next.getHandler().handleUpStream(next, event);
		}
	}

	/**
	 * Handle given {@link MessageEvent} as MO using the handler attached to
	 * this context.
	 *
	 * @param event
	 * @throws OrcaException
	 */
	public void handleDownStream(MessageEvent event) throws OrcaException {
		if (next != null) {
			next.getHandler().handleDownStream(next, event);
		}
	}

}
