/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.flow;

import hsenidmobile.orca.core.flow.event.MessageFlowEvent;
import hsenidmobile.orca.core.model.OrcaException;
import hsenidmobile.orca.core.model.message.AoRequest;
import hsenidmobile.orca.core.model.message.Message;
import hsenidmobile.orca.core.model.message.MessageContext;

/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public interface RequestPipeline {

	/**
	 * Add handlers to the pipeline. These handlers will be used when processing
	 * the request received.
	 *
	 * @param name
	 *            - name of the handler
	 * @param handler
	 *            - Handler
	 */
	void addLast(String name, MessageHandler handler);

	/**
	 * Push received MO message to the pipeline. Processing will be based on the
	 * DownStream handlers set to it.
	 *
	 * @param message
	 *            - Received message.
	 * @param messageContext
	 *            - Message context containing additional data related to
	 *            received message.
	 * @throws OrcaException
	 */
	void onMessage(MessageFlowEvent<Message> messageFlowEvent) throws OrcaException;

	/**
	 * Push received AO(Dev) message to the pipeline. Processing will be based
	 * on the UpStream handlers set to it.
	 *
	 * @param aoRequest
	 * @param messageContext
	 * @throws OrcaException
	 */
	void onMessage(AoRequest aoRequest, MessageContext messageContext) throws OrcaException;
}
