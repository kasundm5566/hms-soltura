/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *
 */
package hsenidmobile.orca.core.flow;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Services that need to be started when Orca is starting...
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public interface StartUpService extends Runnable {

    int getPoolSize();

    long getDelay();

    long getPeriod();

    TimeUnit getTimeUnit();

    String getName();

    public void setExecutorService(ExecutorService executorService);
}
