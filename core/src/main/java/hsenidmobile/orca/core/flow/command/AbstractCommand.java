/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$ 
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.flow.command;

import hsenidmobile.orca.core.applications.PeriodUnit;
import hsenidmobile.orca.core.applications.subscription.Periodicity;
import hsenidmobile.orca.core.model.message.Message;
import hsenidmobile.orca.core.util.PropertyHolder;

import java.util.Locale;

public abstract class AbstractCommand implements Command {

	public Periodicity extractPeriodicity(Message message) {
		String[] splittedDataTxt = message.getMessage().split(PropertyHolder.MESSAGE_SPLITTER);
		if (splittedDataTxt != null) {
			for (String code : splittedDataTxt) {
				if (PropertyHolder.SUPPORTED_PERIODICITY_CODES.contains(code.toLowerCase())) {
					message.setMessage(message.getMessage().replaceFirst(code, ""));
					return new Periodicity(PeriodUnit.getEnum(code), 1, 0);
				}
			}
		}
		return null;
	}

	public Locale extractLanguage(Message message) {
		String[] splittedDataTxt = message.getMessage().split(PropertyHolder.MESSAGE_SPLITTER);
		if (splittedDataTxt != null) {
			for (String code : splittedDataTxt) {
				if (PropertyHolder.SUPPORTED_LANGUAGE_CODES.contains(code.toLowerCase())) {
					message.setMessage(message.getMessage().replaceFirst(code, ""));
					return new Locale(code);
				}
			}
		}

		return Locale.ENGLISH;
	}

}