/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 *
 */
package hsenidmobile.orca.core.flow.command;

import hsenidmobile.orca.core.applications.exception.ApplicationException;
import hsenidmobile.orca.core.flow.MessageFlowErrorReasons;
import hsenidmobile.orca.core.flow.MessageHandler;
import hsenidmobile.orca.core.flow.PipelineContext;
import hsenidmobile.orca.core.flow.command.parser.ApplicationCommandParser;
import hsenidmobile.orca.core.flow.command.parser.CommandParser;
import hsenidmobile.orca.core.flow.command.parser.DataCommandParser;
import hsenidmobile.orca.core.flow.event.MessageEvent;
import hsenidmobile.orca.core.flow.event.MessageFlowEvent;
import hsenidmobile.orca.core.model.OrcaException;
import hsenidmobile.orca.core.model.message.Message;
import hsenidmobile.orca.core.util.PropertyHolder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Read the Mo message received by Orca and create relevant Command for that.
 *
 * $LastChangedDate: $ $LastChangedBy: $ $LastChangedRevision: $
 */
public class CommandBuilder implements MessageHandler {

	private static final Logger logger = LoggerFactory.getLogger(CommandBuilder.class);
	private CommandParser commandChain;

	public void init() {
		commandChain = createCommandParserChain();
	}

	Command createCommand(Message message) throws ApplicationException {
		logger.debug("Start Parsing Message[{}]", message);
		return parse(message);
	}

	private CommandParser createCommandParserChain() {
		logger.debug("Creating the Command Parser Chain.");
		CommandParser commandChain = new ApplicationCommandParser(PropertyHolder.RESULT_SYSTEM_COMMAND,
				ViewResultCommand.class);

		commandChain.setNextParser(new ApplicationCommandParser(PropertyHolder.UPDATE_COMMAND,
				AppDataUpdateCommand.class));
		commandChain.setNextParser(new DataCommandParser("", DataMessageCommand.class));

		return commandChain;
	}

	private Command parse(Message message) throws ApplicationException {

		String messageText = message.getMessage();
		if (messageText == null/* || messageText.trim().length() == 0 */) {
			throw new ApplicationException("Message Cannot be Null", MessageFlowErrorReasons.INVALID_REQUEST);
		}

		messageText = messageText.trim();
		message.setMessage(messageText);

		String firstKeyword = extractFirstKeyword(messageText);

		Command command = commandChain.parse(firstKeyword, message);
		logger.debug("Command Created[{}]", command);
		return command;
	}

	private String extractFirstKeyword(String messageText) {
		int indexOfSeparator = messageText.indexOf(PropertyHolder.MESSAGE_SPLITTER);
		if (indexOfSeparator > -1) {
			return messageText.substring(0, indexOfSeparator);
		} else {
			return messageText;
		}
	}

	public void handleDownStream(PipelineContext pipelineContext, MessageEvent event) throws OrcaException {
		MessageFlowEvent messageFlowEvent = null;
		if (event instanceof MessageFlowEvent && event.getMessage() instanceof Message) {
			messageFlowEvent = (MessageFlowEvent) event;
			Command command = createCommand((Message) messageFlowEvent.getMessage());
			messageFlowEvent.setCommand(command);
		}
		pipelineContext.handleDownStream(event);

	}

	@Override
	public void handleUpStream(PipelineContext pipelineContext, MessageEvent event) throws OrcaException,
			ApplicationException {
		throw new UnsupportedOperationException("UpStream handling not supported.");
	}

}