/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$ 
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.flow.command;


/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public abstract class SystemServiceCommand extends AbstractCommand {
//	private RegistrationService registrationService;
//
//	public RegistrationService getRegistrationService() {
//		return registrationService;
//	}
//
//	public void setRegistrationService(RegistrationService registrationService) {
//		this.registrationService = registrationService;
//	}

}
