/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.flow.command;

import hsenidmobile.orca.core.flow.event.CommandEvent;
import hsenidmobile.orca.core.model.OrcaException;
import hsenidmobile.orca.core.model.message.Message;
import hsenidmobile.orca.core.model.message.MoResponse;
import hsenidmobile.orca.core.util.PropertyHolder;

/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class ViewResultCommand extends AbstractCommand {

	@Override
	public MoResponse execute(CommandEvent<Message> commandEvent) throws OrcaException {
		return commandEvent.getMessageContext().getApplication().onDataMessage(commandEvent);
	}

}
