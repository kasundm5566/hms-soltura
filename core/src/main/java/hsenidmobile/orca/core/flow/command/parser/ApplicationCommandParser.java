/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.flow.command.parser;

import hsenidmobile.orca.core.flow.command.Command;
import hsenidmobile.orca.core.model.message.Message;

/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class ApplicationCommandParser<T> extends CommandParser<T> {

//	private AppCommentService appCommentService;

	public ApplicationCommandParser(String parserKeyword, Class mappingCommand) {
		super(parserKeyword, mappingCommand);
	}

	public Command parse(String firstKeyword, Message message) {
		if (firstKeyword.equalsIgnoreCase(getParserKeyword())) {
			Command command = (Command) newInstance(getMappingCommand().getName());
			message.setMessage(extractMessageData(firstKeyword, message.getMessage()));

//			if (command instanceof ReportAbuseCommand) {
//				((ReportAbuseCommand) command).setAppCommentService(appCommentService);
//			}

			return command;
		} else {
			return passToNextCommand(firstKeyword, message);
		}
	}

//	public void setAppCommentService(AppCommentService appCommentService) {
//		this.appCommentService = appCommentService;
//	}

}
