/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$ 
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.flow.command.parser;

import hsenidmobile.orca.core.flow.command.Command;
import hsenidmobile.orca.core.model.message.Message;
import hsenidmobile.orca.core.util.PropertyHolder;

/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public abstract class CommandParser<T> {
	private final String parserKeyword;
	private final Class mappingCommand;
	private CommandParser nextParser;

	public CommandParser(String parserKeyword, Class mappingCommand) {
		super();
		this.parserKeyword = parserKeyword;
		this.mappingCommand = mappingCommand;
	}

	public void setNextParser(CommandParser nextParser) {
		if (this.nextParser == null) {
			this.nextParser = nextParser;
		} else {
			this.nextParser.setNextParser(nextParser);
		}
	}

	/**
	 * Pass message to the next command in the chain to process.
	 * 
	 * @param firstKeyword
	 * @return
	 */
	public Command passToNextCommand(String firstKeyword, Message message) {
		if (this.nextParser != null) {
			return this.nextParser.parse(firstKeyword, message);
		} else {
			throw new RuntimeException("No more command parsers left keyword[" + firstKeyword + "].");
		}
	}

	public abstract Command parse(String firstKeyword, Message message);

	protected T newInstance(String name) {
		try {
			return (T) Class.forName(name).newInstance();
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	/**
	 * Removed the string given as 'firstKeyword' from the message
	 * 'completeMessageText'. e.g. firstKeyword = 'stv sss', completeMessageText
	 * = 'stv sss This is test' then output is 'This is test'
	 * 
	 * @param firstKeyword
	 * @param completeMessageText
	 * @return
	 */
	protected String extractMessageData(String firstKeyword, String completeMessageText) {
		if (completeMessageText.length() > firstKeyword.length()) {
			return completeMessageText.substring(firstKeyword.length() + 1, completeMessageText.length());
		} else {
			return "";
		}
	}

	public String getParserKeyword() {
		return parserKeyword;
	}

	public Class getMappingCommand() {
		return mappingCommand;
	}

	public CommandParser getNextParser() {
		return nextParser;
	}

}
