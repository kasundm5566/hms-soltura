/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$ 
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.flow.command.parser;

import hsenidmobile.orca.core.flow.command.Command;
import hsenidmobile.orca.core.flow.command.DataMessageCommand;
import hsenidmobile.orca.core.model.message.Message;

/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class DataCommandParser extends ApplicationCommandParser<DataMessageCommand> {

	public DataCommandParser(String parserKeyword, Class mappingCommand) {
		super(parserKeyword, mappingCommand);
	}
	
	public Command parse(String firstKeyword, Message message) {
		DataMessageCommand command =  newInstance(getMappingCommand().getName());
		return command;
	}

}
