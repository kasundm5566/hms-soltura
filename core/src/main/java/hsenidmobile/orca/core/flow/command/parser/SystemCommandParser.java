/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.flow.command.parser;

import hsenidmobile.orca.core.flow.command.Command;
import hsenidmobile.orca.core.flow.command.SystemServiceCommand;
import hsenidmobile.orca.core.model.message.Message;

/**
 * Create SystemsServiceCommands
 *
 * $LastChangedDate$
 * $LastChangedBy$
 *
 * @version $LastChangedRevision$
 */
public class SystemCommandParser extends CommandParser<SystemServiceCommand> {

	public SystemCommandParser(String parserKeyword, Class mappingCommand) {
		super(parserKeyword, mappingCommand);
	}

	public Command parse(String firstKeyword, Message message) {
		if (firstKeyword.equalsIgnoreCase(getParserKeyword())) {
			SystemServiceCommand command = newInstance(getMappingCommand().getName());

			message.setMessage("");
			return command;
		} else {
			return passToNextCommand(firstKeyword, message);
		}
	}

}
