/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$ 
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.flow.dispatch;

import hsenidmobile.orca.core.model.message.Message;
import hsenidmobile.orca.core.model.message.SmsMessage;

import java.util.List;

/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class DispatchableMessage<T> {
	private T message;
    private List<? extends Message> messageList;
	private String appId;
    private String password;

	public DispatchableMessage(String appId, T message) {
		this.appId = appId;
		this.message = message;
	}

    public DispatchableMessage(String appId, String password, List<? extends Message> messageList) {
		this.appId = appId;
		this.password = password;
		this.messageList = messageList;
	}

	public T getMessage() {
		return message;
	}

	public String getAppId() {
		return appId;
	}

    public String getPassword() {
        return password;
    }

    public List<? extends Message> getMessageList() {
        return messageList;
    }

    public void setMessageList(List<Message> messageList) {
        this.messageList = messageList;
    }

    @Override
    public String toString() {
        return new StringBuilder().append("DispatchableMessage{")
                .append("message=").append(message)
                .append(", messageList=").append(messageList)
                .append(", appId='").append(appId).append('\'')
                .append(", password='").append(password).append('\'').append('}').toString();
    }
}
