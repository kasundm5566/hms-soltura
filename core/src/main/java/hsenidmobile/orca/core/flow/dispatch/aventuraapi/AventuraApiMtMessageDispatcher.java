/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hsenidmobile.orca.core.flow.dispatch.aventuraapi;

import hsenidmobile.orca.core.model.message.AbstractMessageImpl;

import java.util.List;
import java.util.concurrent.Future;
/*
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */

public interface AventuraApiMtMessageDispatcher {

    Future dispatchIndividualMessages(String orcaAppId, List<? extends AbstractMessageImpl> messages);

    Future dispatchBroadcastMessage(String orcaAppId, List<? extends AbstractMessageImpl> messages);

    void dispatchMessage(List<AbstractMessageImpl> messages);

}
