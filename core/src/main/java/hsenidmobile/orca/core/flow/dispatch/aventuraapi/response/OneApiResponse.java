/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hsenidmobile.orca.core.flow.dispatch.aventuraapi.response;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * $LastChangedDate:$
 * $LastChangedBy:$
 * $LastChangedRevision:$
 */
@XStreamAlias("mchoice_sdp_sms_response")
public class OneApiResponse implements Response {

    protected String version;
    @XStreamAlias("status_code")
    protected String statusCode;
    @XStreamAlias("status_message")
    protected String statusMessage;
    protected String correlator;

    protected OneApiResponse(String version) {
        this.version = version;
    }

    public OneApiResponse(String version, String statusCode, String statusMessage, String correlator) {
        this.version = version;
        this.statusCode = statusCode;
        this.statusMessage = statusMessage;
        this.correlator = correlator;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public String getCorrelator() {
        return correlator;
    }

    public void setCorrelator(String correlator) {
        this.correlator = correlator;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("OneApiResponse");
        sb.append("{version='").append(version).append('\'');
        sb.append(", statusCode='").append(statusCode).append('\'');
        sb.append(", statusMessage='").append(statusMessage).append('\'');
        sb.append(", correlator='").append(correlator).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
