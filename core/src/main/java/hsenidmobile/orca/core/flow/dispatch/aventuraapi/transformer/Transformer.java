/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hsenidmobile.orca.core.flow.dispatch.aventuraapi.transformer;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.io.xml.XmlFriendlyReplacer;

/**
 * $LastChangedDate:$
 * $LastChangedBy:$
 * $LastChangedRevision:$
 */
public class Transformer {

    public static final String DOLLAR_SIGN = "$";
    public static final String UNDERSCORE_SIGN = "_";
    public static final XmlFriendlyReplacer replacer = new XmlFriendlyReplacer(DOLLAR_SIGN, UNDERSCORE_SIGN);

    public static Object toDomainObject(String xml, Class clazz) {
        XStream xStream = new XStream();
        xStream.processAnnotations(clazz);
        return xStream.fromXML(xml);
    }

    public static String toXml(Object target, Class clazz) {
        XStream xStream = new XStream(new DomDriver("UTF-8", replacer));

        xStream.processAnnotations(clazz);
        return xStream.toXML(target);
    }
}
