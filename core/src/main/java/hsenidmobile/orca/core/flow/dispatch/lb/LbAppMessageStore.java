/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.flow.dispatch.lb;

import hsenidmobile.orca.core.flow.dispatch.DispatchableMessage;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Represents single LoadBalancing entity.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 *
 * @version $LastChangedRevision$
 *
 */
public class LbAppMessageStore<T> {

	private static final Logger logger = LoggerFactory.getLogger(LbAppMessageStore.class);
	private ConcurrentLinkedQueue<DispatchableMessage<T>> messageStore = new ConcurrentLinkedQueue<DispatchableMessage<T>>();
	private int maxLoad;
	private final String appId;
	private AtomicInteger currentLoad = new AtomicInteger(0);
	private AtomicBoolean entityActive = new AtomicBoolean(false);
	private long lastActiveTime = System.currentTimeMillis();

	public LbAppMessageStore(String appId) {
		this.appId = appId;
	}

	public void setMaxLoad(int maxLoad) {
		this.maxLoad = maxLoad;
	}

	public void putMessage(T message) {
		logger.debug("Message[{}] inserted to the AppMessageStore[{}]", message, appId);
		messageStore.add(new DispatchableMessage<T>(appId, message));
		updateLastActiveTime();
	}

	private void updateLastActiveTime() {
		lastActiveTime = System.currentTimeMillis();
	}

	public boolean canProcess() {
		if (entityActive.get()) {
			if (currentLoad.incrementAndGet() > maxLoad) {
				currentLoad.set(0);
				return false;
			} else {
				if (messageStore.size() > 0) {
					return true;
				} else {
					return false;
				}
			}
		} else {
			return false;
		}
	}

	public DispatchableMessage<T> getMessage() {
		updateLastActiveTime();
		return messageStore.poll();
	}

	public void deactivate() {
		entityActive.set(false);
	}

	public void activate() {
		entityActive.set(true);
	}

	public String getAppId() {
		return appId;
	}

	public void reset() {
		currentLoad.set(0);
	}

	public int getMaxLoad() {
		return maxLoad;
	}

	public long getLastActiveTime() {
		return lastActiveTime;
	}

}