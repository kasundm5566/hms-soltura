/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$ 
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.flow.dispatch.lb;

/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class NoLbEntityAvailable extends RuntimeException {
	public NoLbEntityAvailable(String string) {
		super(string);
	}

	public NoLbEntityAvailable(String string, Throwable th) {
		super(string, th);
	}
}
