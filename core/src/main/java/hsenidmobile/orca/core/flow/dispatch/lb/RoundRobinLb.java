/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.flow.dispatch.lb;

import static ch.lambdaj.Lambda.having;
import static ch.lambdaj.Lambda.on;
import static ch.lambdaj.Lambda.selectFirst;
import static org.hamcrest.Matchers.equalTo;
import hsenidmobile.orca.core.flow.dispatch.DispatchableMessage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implements RoundRobin LB algorithm with configurable load for each entity
 *
 * $LastChangedDate$
 * $LastChangedBy$
 *
 * @version $LastChangedRevision$
 *
 */
public class RoundRobinLb<T> {

	private static final Logger logger = LoggerFactory.getLogger(RoundRobinLb.class);
	private List<LbAppMessageStore<T>> lbNodes = Collections.synchronizedList(new ArrayList<LbAppMessageStore<T>>(50));
	private AtomicInteger currentIndex = new AtomicInteger(0);

	public void putMessage(String appId, T message) {
		LbAppMessageStore<T> messageStore = getAppMessageStore(appId);

		messageStore.putMessage(message);
		logger.debug("Message[{}] inserted to the RoundRobinLb to dispatched.", message);
	}

	public void putMessages(String appId, List<T> messagesList) {
		LbAppMessageStore<T> messageStore = getAppMessageStore(appId);

		for (T message : messagesList) {
			messageStore.putMessage(message);
			logger.debug("Message[{}] inserted to the RoundRobinLb to dispatched.", message);
		}
	}

	private LbAppMessageStore<T> getAppMessageStore(String appId) {
		LbAppMessageStore<T> messageStore;
		synchronized (lbNodes) {
			messageStore = selectFirst(lbNodes, having(on(LbAppMessageStore.class).getAppId(), equalTo(appId)));
			logger.debug("AppMessageStore Found in the List for AppId[{}] is [{}]", appId, messageStore);
			if (messageStore == null) {
				messageStore = new LbAppMessageStore<T>(appId);
				messageStore.activate();
				// TODO: Have take 'MaxLoad' from some application specific
				// configuration.
				messageStore.setMaxLoad(5);
				lbNodes.add(messageStore);
				logger.debug("Created New AppMessageStore for AppId[{}]", appId);
			}
		}
		return messageStore;
	}

	public void removeLbEntity(String id) {
		synchronized (lbNodes) {
			for (int i = 0; i < lbNodes.size(); i++) {
				LbAppMessageStore<T> entity = lbNodes.get(i);
				if (entity.getAppId().equals(id)) {
					lbNodes.remove(i);
					return;
				}
			}
		}
	}

	public void clearList() {
		lbNodes.clear();
	}

	public DispatchableMessage<T> getNext() throws NoLbEntityAvailable {

		if (lbNodes.size() == 0) {
			throw new NoLbEntityAvailable("No LbNodes to be found for LB");
		}

		int startingPossition = currentIndex.get();
		LbAppMessageStore<T> lbEntity = lbNodes.get(startingPossition);
		if (lbEntity != null && lbEntity.canProcess()) {
			return lbEntity.getMessage();
		} else {

			int checkingPossition = getNextIndex(lbEntity);
			for (int i = 0; i < lbNodes.size(); i++) {
				if (lbEntity == null) {
					continue;
				}
				lbEntity = lbNodes.get(checkingPossition);

				if (lbEntity.canProcess()) {
					return lbEntity.getMessage();
				}

				checkingPossition = getNextIndex(lbEntity);
			}
			throw new NoLbEntityAvailable("No Active entity to be found for LB");

		}

	}

	private synchronized int getNextIndex(LbAppMessageStore<T> currentLbEntity) {
		currentLbEntity.reset();

		int checkingPossition = currentIndex.incrementAndGet();
		if (checkingPossition > lbNodes.size() - 1) {
			currentIndex.set(0);
			checkingPossition = 0;
		}
		return checkingPossition;
	}
}