/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.flow.event;

import hsenidmobile.orca.core.applications.subscription.Periodicity;
import hsenidmobile.orca.core.model.message.MessageContext;

import java.util.Locale;

/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class CommandEvent<T> {
	private T message;
	private MessageContext messageContext;
	private String subKeyword = "";
	private Locale language = Locale.ENGLISH;
	private Periodicity periodicity;
	private boolean isSystemWideKeyword;

	public CommandEvent(T message, MessageContext messageContext) {
		this.message = message;
		this.messageContext = messageContext;
	}

	public CommandEvent(T message, MessageContext messageContext, String subKeyword) {
		super();
		this.message = message;
		this.messageContext = messageContext;
		this.subKeyword = subKeyword;
	}

	public CommandEvent(MessageFlowEvent<T> messageFlowEvent){
		this.message = messageFlowEvent.getMessage();
		this.messageContext = messageFlowEvent.getMessageContext();
		this.subKeyword = messageFlowEvent.getSubKeyword();
		this.isSystemWideKeyword = messageFlowEvent.isSystemWideKeyword();
	}

	public T getMessage() {
		return message;
	}

	public MessageContext getMessageContext() {
		return messageContext;
	}

	public String getSubKeyword() {
		return subKeyword;
	}

	public void setSubKeyword(String subKeyword) {
		this.subKeyword = subKeyword;
	}

	public Locale getLanguage() {
		return language;
	}

	public void setLanguage(Locale language) {
		this.language = language;
	}

	public Periodicity getPeriodicity() {
		return periodicity;
	}

	public void setPeriodicity(Periodicity periodicity) {
		this.periodicity = periodicity;
	}

	public boolean isSystemWideKeyword() {
		return isSystemWideKeyword;
	}

	public void setSystemWideKeyword(boolean isSystemWideKeyword) {
		this.isSystemWideKeyword = isSystemWideKeyword;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(250);
		builder.append("CommandEvent [subKeyword=");
		builder.append(subKeyword);
		builder.append(", isSystemWideKeyword=");
		builder.append(isSystemWideKeyword);
		builder.append(", language=");
		builder.append(language);
		builder.append(", message=");
		builder.append(message);
		builder.append(", periodicity=");
		builder.append(periodicity);
		builder.append(", messageContext=");
		builder.append(messageContext);
		builder.append("]");
		return builder.toString();
	}


}
