/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hsenidmobile.orca.core.flow.event;

import hsenidmobile.orca.core.flow.command.Command;
import hsenidmobile.orca.core.model.message.MessageContext;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class MessageFlowEvent<T> implements MessageEvent {

	private T message;
	private MessageContext messageContext;
	private Command command;
	private String subKeyword;
	private boolean isSystemWideKeyword;
	private boolean isSystemCommand;

	public MessageFlowEvent(T message, MessageContext messageContext) {
		super();
		this.message = message;
		this.messageContext = messageContext;
	}

	public T getMessage() {
		return message;
	}

	public MessageContext getMessageContext() {
		return messageContext;
	}

	public Command getCommand() {
		return command;
	}

	public void setCommand(Command command) {
		this.command = command;
	}

	public String getSubKeyword() {
		return subKeyword;
	}

	public void setSubKeyword(String subKeyword) {
		this.subKeyword = subKeyword;
	}

	public boolean isSystemWideKeyword() {
		return isSystemWideKeyword;
	}

	public void setSystemWideKeyword(boolean isSystemWideKeyword) {
		this.isSystemWideKeyword = isSystemWideKeyword;
	}

	public boolean isSystemCommand() {
		return isSystemCommand;
	}

	public void setSystemCommand(boolean isSystemCommand) {
		this.isSystemCommand = isSystemCommand;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(250);
		builder.append("MessageFlowEvent [command=");
		builder.append(command);
		builder.append(", subKeyword=");
		builder.append(subKeyword);
		builder.append(", isSystemCommand=");
		builder.append(isSystemCommand);
		builder.append(", isSystemWideKeyword=");
		builder.append(isSystemWideKeyword);
		builder.append(", message=");
		builder.append(message);
		builder.append(", messageContext=");
		builder.append(messageContext);
		builder.append("]");
		return builder.toString();
	}

}
