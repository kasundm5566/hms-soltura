/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.flow.event;

import hsenidmobile.orca.core.model.message.MessageContext;
import hsenidmobile.orca.core.model.message.MoResponse;

/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class MoResponseEvent implements MessageEvent {

	private MoResponse moResponse;
	private MessageContext messageContext;

	public MoResponseEvent(MoResponse moResponse, MessageContext messageContext) {
		super();
		this.moResponse = moResponse;
		this.messageContext = messageContext;
	}

	public MoResponse getMessage() {
		return moResponse;
	}

	public MessageContext getMessageContext() {
		return messageContext;
	}

}
