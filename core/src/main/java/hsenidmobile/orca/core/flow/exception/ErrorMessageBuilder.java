/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.flow.exception;


/**
 * Used to create response message sent back to the user for error scenarios
 * happened in Orca message flow.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
public interface ErrorMessageBuilder {

	String buildResponse(Throwable messageFlowException);
}
