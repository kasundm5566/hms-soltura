/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.flow.exception;

import hsenidmobile.orca.core.flow.event.MessageEvent;

/**
 * Process the Exception Flow of the Orca Message Flow.
 *
 * $LastChangedDate$ $LastChangedBy$ $LastChangedRevision$
 *
 */
public interface ExceptionHandler {

	void handleException(MessageEvent event, Throwable throwable);
}
