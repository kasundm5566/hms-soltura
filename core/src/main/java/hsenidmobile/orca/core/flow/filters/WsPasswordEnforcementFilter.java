/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.flow.filters;

import hsenidmobile.orca.core.applications.developer.Dev;
import hsenidmobile.orca.core.flow.PipelineContext;
import hsenidmobile.orca.core.flow.event.MessageEvent;
import hsenidmobile.orca.core.model.Application;
import hsenidmobile.orca.core.model.OrcaException;
import hsenidmobile.orca.core.model.message.AoRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Enforce Password Validation for received AO request
 *
 * $LastChangedDate$
 * $LastChangedBy$
 *
 * @version $LastChangedRevision$
 */
public class WsPasswordEnforcementFilter implements AoRequestFilter {

	private static final Logger logger = LoggerFactory.getLogger(WsPasswordEnforcementFilter.class);

	/**
	 * Enforce Password Validation for received AO request
	 */
	public void handleUpStream(PipelineContext pipelineContext, MessageEvent event) throws OrcaException {
		logger.debug("Enforcing Password Validation MessageEvent[{}]", event);
		Application app = event.getMessageContext().getApplication();
		if (app.getService() instanceof Dev) {
			Dev devApp = (Dev) app.getService();
			AoRequest aoRequest = (AoRequest) event.getMessage();
			if (devApp.getWsPassword().equals(aoRequest.getPassword())) {
				pipelineContext.handleUpStream(event);
			} else {
				throw new WsPasswordNotMatchException(
						"Password in the MT request and password configured in the SLA are not matching.");
			}
		} else {
			throw new ApplicationTypeNotAllowedException("Application do not support processing this request type.");
		}
	}

	@Override
	public void handleDownStream(PipelineContext pipelineContext, MessageEvent event) throws OrcaException {
		throw new UnsupportedOperationException("DownStream handling not supported.");
	}

}
