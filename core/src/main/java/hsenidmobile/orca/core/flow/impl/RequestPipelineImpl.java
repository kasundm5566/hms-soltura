/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.flow.impl;

import hsenidmobile.orca.core.flow.MessageHandler;
import hsenidmobile.orca.core.flow.PipelineContext;
import hsenidmobile.orca.core.flow.RequestPipeline;
import hsenidmobile.orca.core.flow.event.MessageEvent;
import hsenidmobile.orca.core.flow.event.MessageFlowEvent;
import hsenidmobile.orca.core.model.OrcaException;
import hsenidmobile.orca.core.model.message.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 *
 * @version $LastChangedRevision$
 */
public class RequestPipelineImpl implements RequestPipeline {

	private static final Logger logger = LoggerFactory.getLogger(RequestPipelineImpl.class);
	private volatile PipelineContext head;
	private volatile PipelineContext tail;
	private final Map<String, PipelineContext> name2ctx = new HashMap<String, PipelineContext>(4);

	private void init(String name, MessageHandler handler) {
		PipelineContext ctx = new PipelineContext(null, null, name, handler);
		head = tail = ctx;
		name2ctx.clear();
		name2ctx.put(name, ctx);
	}

	public synchronized void addLast(String name, MessageHandler handler) {
		if (name2ctx.isEmpty()) {
			init(name, handler);
		} else {
			checkDuplicateName(name);
			PipelineContext oldTail = tail;
			PipelineContext newTail = new PipelineContext(oldTail, null, name, handler);

			oldTail.setNext(newTail);
			tail = newTail;
			name2ctx.put(name, newTail);

		}

		logger.info("Added Handler[{}][{}] to Pipeline", name, handler);
	}

	private void checkDuplicateName(String name) {
		if (name2ctx.containsKey(name)) {
			throw new IllegalArgumentException("Duplicate handler name.");
		}
	}

	public void onMessage(MessageFlowEvent<Message> messageFlowEvent) throws OrcaException {
		if (this.head == null) {
			throw new IllegalStateException("No Handler found to process received message");
		}

		handleDownStream(this.head, messageFlowEvent);
	}

	public void onMessage(AoRequest aoRequest, MessageContext messageContext) throws OrcaException {
		if (this.head == null) {
			throw new IllegalStateException("No Handler found to process received message");
		}

		handleUpStream(this.head, new MessageFlowEvent<AoRequest>(aoRequest, messageContext));
	}

	public void onMessage(AppDataUpdateRequest appDataUpdateRequest, MessageContext messageContext)
			throws OrcaException {
		if (this.head == null) {
			throw new IllegalStateException("No Handler found to process received message");
		}

		handleDownStream(this.head, new MessageFlowEvent<AppDataUpdateRequest>(appDataUpdateRequest, messageContext));
	}

    public void onMessage(RequestShowReplyRequest requestShowReplyRequest, MessageContext messageContext)
			throws OrcaException {
		if (this.head == null) {
			throw new IllegalStateException("No Handler found to process received message");
		}

		handleDownStream(this.head, new MessageFlowEvent<RequestShowReplyRequest>(requestShowReplyRequest, messageContext));
	}

	private void handleUpStream(PipelineContext ctx, MessageFlowEvent<AoRequest> event) throws OrcaException {
		ctx.getHandler().handleUpStream(ctx, event);
	}

	void handleDownStream(PipelineContext ctx, MessageEvent event) throws OrcaException {
		ctx.getHandler().handleDownStream(ctx, event);
	}

	Map<String, PipelineContext> getAvailableHandler() {
		return name2ctx;
	}

	PipelineContext getFirst() {
		return this.head;
	}
}
