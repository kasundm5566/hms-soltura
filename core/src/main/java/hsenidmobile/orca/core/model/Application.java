/*
- *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate: 2010-10-08 14:17:52 +0530 (Fri, 08 Oct 2010) $
 * $LastChangedBy: sandarenu $
 * $LastChangedRevision: 66308 $
 *
 */
package hsenidmobile.orca.core.model;

import hsenidmobile.orca.core.applications.Service;
import hsenidmobile.orca.core.applications.exception.ApplicationException;
import hsenidmobile.orca.core.flow.MessageFlowException;
import hsenidmobile.orca.core.flow.event.CommandEvent;
import hsenidmobile.orca.core.model.message.Message;
import hsenidmobile.orca.core.model.message.MoResponse;
import hsenidmobile.orca.core.repository.AppRepository;

import java.util.List;

import org.joda.time.DateTime;

/**
 *
 * $LastChangedDate: 2010-10-08 14:17:52 +0530 (Fri, 08 Oct 2010) $
 * $LastChangedBy: sandarenu $
 * $LastChangedRevision: 66308 $
 *
 */
public interface Application {

	String getAppId();

	void setAppId(String appId);

	public Status getStatus();

	public void setStatus(Status status);

	public DateTime getStartDate();

	public DateTime getEndDate();

	public void setStartDate(DateTime startDate);

	public void setEndDate(DateTime endDate);

	/**
	 * Data message received at application
	 *
	 * e.g. User sent message - stv sss subkey1 This is my message app service
	 * keyword = stv sss subkeyword = subkey1
	 *
	 * @param subkeyword
	 *            - Extracted subkeyword(subkey1)
	 * @param message
	 *            - Rest of the message after extraction of application service
	 *            keyword and sub keyword(This is my message).
	 * @param messageContext
	 * @return
	 * @throws MessageFlowException
	 *             TODO
	 */
	public MoResponse onDataMessage(CommandEvent<Message> commandEvent) throws MessageFlowException;

	/**
	 * Update the data for complete keyword set..
	 *
	 * @param contentReleaseList
	 * @throws OrcaException
	 */
	void updateServiceData(List<ContentRelease> contentReleaseList) throws OrcaException;

	public MoResponse updateServiceData(CommandEvent<Message> commandEvent) throws OrcaException;

	/**
	 * @param keyword
	 *            Check whether given keyword is a valid one for this service.
	 * @return returns if this is the correct keyword or not
	 */
	public boolean isValidKeyword(String keyword);

	public ApplicationRoutingInfo getApplicationRoutingInfo();

	public void setApplicationRoutingInfo(ApplicationRoutingInfo applicationRoutingInfo);

	public MoResponse reportAbuse(Message message);

	void createAppId(String appName) throws ApplicationException;

	/**
	 * Name of the application.
	 *
	 * @return
	 */
	String getAppName();

	Long getId();

	Service getService();

	void setService(Service service);

	/**
	 * Get the routing key used by this application.
	 *
	 * @return
	 */
	List<RoutingKey> getRoutingKeys();

	List<Author> getAuthors();

	void setAuthors(List<Author> authors);

	ApplicationChargingInfo getApplicationChargingInfo();

	void setApplicationChargingInfo(ApplicationChargingInfo applicationChargingInfo);

	boolean isRegistrationRequired();

	void setRegistrationRequired(boolean registrationRequired);

	boolean isChargingFromSubscriber();

	void setChargingFromSubscriber(boolean chargingFromSubscriber);

	boolean isSateChageAllowed();

	public String getShortDescription();

	public void setShortDescription(String shortDescription);

	public String getDescription();

	public void setDescription(String description);

	public AppRepository getApplicationRepository();

    void setExpirableApp(boolean expirableApp);

    boolean isExpirableApp();

}