/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hsenidmobile.orca.core.model;

/**
 * $LastChangedDate: $ $LastChangedBy: $ $LastChangedRevision: $
 */
public class ApplicationChargingInfo {

	public enum FromSubscriberChargingModel {
		ONLY_ONE_CHG_TYPE_ALLOWED, MULTIPLE_CHG_TYPE_ALLOWED;
	}

	/**
	 * From subscriber per message Payment Instrument based charging
	 */
	public static final String PER_MSG_PI_CHG = "PI";
	/**
	 * From subscriber per message Operator charging
	 */
	public static final String PER_MSG_OPR_CHG = "OPR";

	private FromSubscriberChargingModel fromSubscriberChargingModel = FromSubscriberChargingModel.MULTIPLE_CHG_TYPE_ALLOWED;
	private AppChargingType chargingType;
	private boolean chargingFromSubscriber;
	private boolean chargingForMessage;
	private boolean chargingForSubscription;
//	private boolean chargingFromSubscriberFinancialInstrument;
	private String selectedFinancialInstrumentType;

	private String frmSubscriberChargingValue = "CHG_SUBSCRIPTION";
	private String frmSubscriberPerMsgChargingValue = "PI";

	public String getFrmSubscriberChargingValue() {
		return frmSubscriberChargingValue;
	}

	public void setFrmSubscriberChargingValue(String frmSubscriberChargingValue) {
		this.frmSubscriberChargingValue = frmSubscriberChargingValue;
	}

	public FromSubscriberChargingModel getFromSubscriberChargingModel() {
		return fromSubscriberChargingModel;
	}

	public void setFromSubscriberChargingModel(FromSubscriberChargingModel fromSubscriberChargingModel) {
		this.fromSubscriberChargingModel = fromSubscriberChargingModel;
	}

	public boolean isChargingFromSubscriber() {
		return chargingFromSubscriber;
	}

	public void setChargingFromSubscriber(boolean chargingFromSubscriber) {
		this.chargingFromSubscriber = chargingFromSubscriber;
	}

	public boolean isChargingForMessage() {
		return chargingForMessage;
	}

	public void setChargingForMessage(boolean chargingForMessage) {
		this.chargingForMessage = chargingForMessage;
	}

	public boolean isChargingForSubscription() {
		return chargingForSubscription;
	}

	public void setChargingForSubscription(boolean chargingForSubscription) {
		this.chargingForSubscription = chargingForSubscription;
	}

	public AppChargingType getChargingType() {
		return chargingType;
	}

	public void setChargingType(AppChargingType chargingType) {
		this.chargingType = chargingType;
	}

//	public boolean isChargingFromSubscriberFinancialInstrument() {
//		return chargingFromSubscriberFinancialInstrument;
//	}
//
//	public void setChargingFromSubscriberFinancialInstrument(boolean chargingFromSubscriberFinancialInstrument) {
//		this.chargingFromSubscriberFinancialInstrument = chargingFromSubscriberFinancialInstrument;
//	}

	public String getSelectedFinancialInstrumentType() {
		return selectedFinancialInstrumentType;
	}

	public void setSelectedFinancialInstrumentType(String selectedFinancialInstrumentType) {
		this.selectedFinancialInstrumentType = selectedFinancialInstrumentType;
	}

	public enum AppChargingType {
		onetime, monthly, daily, weekly, free;
	}

	public String getFrmSubscriberPerMsgChargingValue() {
		return frmSubscriberPerMsgChargingValue;
	}

	public void setFrmSubscriberPerMsgChargingValue(String frmSubscriberPerMsgChargingValue) {
		this.frmSubscriberPerMsgChargingValue = frmSubscriberPerMsgChargingValue;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(250);
		builder.append("ApplicationChargingInfo [fromSubscriberChargingModel=");
		builder.append(fromSubscriberChargingModel);
		builder.append(", chargingType=");
		builder.append(chargingType);
		builder.append(", chargingFromSubscriber=");
		builder.append(chargingFromSubscriber);
		builder.append(", chargingForMessage=");
		builder.append(chargingForMessage);
		builder.append(", chargingForSubscription=");
		builder.append(chargingForSubscription);
//		builder.append(", chargingFromSubscriberFinancialInstrument=");
//		builder.append(chargingFromSubscriberFinancialInstrument);
		builder.append(", frmSubscriberPerMsgChargingValue=");
		builder.append(frmSubscriberPerMsgChargingValue);
		builder.append(", selectedFinancialInstrumentType=");
		builder.append(selectedFinancialInstrumentType);
		builder.append(", frmSubscriberChargingValue=");
		builder.append(frmSubscriberChargingValue);
		builder.append("]");
		return builder.toString();
	}

}
