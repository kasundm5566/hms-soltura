/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.model;

import hsenidmobile.orca.core.applications.ApplicationImpl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * $LastChangedDate: $ $LastChangedBy: $ $LastChangedRevision: $
 */
public class ApplicationRoutingInfo {

	private static final Logger logger = LoggerFactory.getLogger(ApplicationRoutingInfo.class);

	private ApplicationImpl application;

	private List<RoutingInfo> routingInfos;

	public Application getApplication() {
		return application;
	}

	public void setApplication(ApplicationImpl application) {
		this.application = application;
	}

	public void addRoutingInfo(RoutingInfo routingInfo) {
		if (this.routingInfos == null) {
			this.routingInfos = new ArrayList<RoutingInfo>();
		}

		this.routingInfos.add(routingInfo);
	}

	public List<RoutingKey> getRoutingKeys() {
		if (this.routingInfos == null || this.routingInfos.isEmpty()) {
			return Collections.EMPTY_LIST;
		} else {
			List<RoutingKey> rks = new ArrayList<RoutingKey>(this.routingInfos.size());
			for (RoutingInfo ri : this.routingInfos) {
				rks.add(ri.getRoutingKey());
			}

			return rks;
		}
	}

	/**
	 * Get a list of unique keywords used by the application.
	 *
	 * @return
	 */
	public List<String> getUsingKeywords() {
		if (this.routingInfos == null || this.routingInfos.isEmpty()) {
			return Collections.EMPTY_LIST;
		} else {
			List<String> keywordList = new ArrayList<String>(this.routingInfos.size());
			for (RoutingInfo ri : this.routingInfos) {
				String keyword = ri.getRoutingKey().getKeyword();
				if (!keywordList.contains(keyword)) {
					keywordList.add(keyword);
				}
			}

			return keywordList;
		}
	}

	/**
	 * Check whether a given keyword is being used.
	 *
	 * @param keyword
	 * @return
	 */
	public boolean usesKeyword(String keyword) {
		if (this.routingInfos != null && !this.routingInfos.isEmpty()) {
			for (RoutingInfo ri : this.routingInfos) {
				String key = ri.getRoutingKey().getKeyword();
				if (key.equals(keyword)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Get the shortcode used for a given Operator.
	 *
	 * @param operatorCode
	 * @return
	 */
	public Msisdn getShortCodeUnderOperator(String operatorCode) {
		if (this.routingInfos != null && !this.routingInfos.isEmpty()) {
			for (RoutingInfo ri : this.routingInfos) {
				if (ri.getRoutingKey().getShortCode().getOperator().equals(operatorCode)) {
					return ri.getRoutingKey().getShortCode();
				}
			}
		}
		return null;
	}

	public List<RoutingInfo> getRoutingInfos() {
		return routingInfos;
	}

	public void setRoutingInfos(List<RoutingInfo> routingInfos) {
		this.routingInfos = routingInfos;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(100);
		builder.append("ApplicationRoutingInfo [application=");
		builder.append(application);
		builder.append(", routingInfo=");
		if (routingInfos != null) {
			for (RoutingInfo ri : routingInfos) {
				builder.append(ri).append(", ");
			}
		} else {
			builder.append(routingInfos);
		}
		builder.append("]");
		return builder.toString();
	}

	/**
	 * Change the status of routing infos attached to this application to
	 * available
	 */
	public void releaseRoutingInfos() {
		for (RoutingInfo ri : this.routingInfos) {
			logger.debug("Found RI to release[{}]", ri);
			if (ri.getOwningAppId() != null && ri.getOwningAppId().equals(this.application.getAppId())) {
				logger.info("Releasing RI[{}]", ri);
				ri.setStatus(RoutingInfoStatus.AVAILABLE);
				ri.setOwningAppId(null);
			} else {
				logger.debug("RI[{}] is not currently owned by Application[{}]", ri, application.getAppId());
			}
		}
	}

}