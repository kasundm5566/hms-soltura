/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hsenidmobile.orca.core.model;

import hsenidmobile.orca.core.repository.PersistentObject;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Author who can update service data through sms
 *
 * $LastChangedDate $
 * $LastChangedBy $
 * $LastChangedRevision$
 *
 */
@Entity
@Table(name = "author")
public class Author extends PersistentObject {

	@Embedded
	private Msisdn msisdn;

	public Author(Msisdn msisdn) {
		this.msisdn = msisdn;
	}

	public Author() {
	}

	public Msisdn getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(Msisdn msisdn) {
		this.msisdn = msisdn;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(100);
		builder.append("Author [msisdn=");
		builder.append(msisdn);
		builder.append("]");
		return builder.toString();
	}

}
