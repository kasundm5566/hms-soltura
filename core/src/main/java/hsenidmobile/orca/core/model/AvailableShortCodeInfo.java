package hsenidmobile.orca.core.model;

import hsenidmobile.orca.core.applications.NcsType;
import hsenidmobile.orca.core.repository.PersistentObject;

import javax.persistence.*;

/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 */

public class AvailableShortCodeInfo extends PersistentObject {

    private Msisdn shortCode;

    private NcsType ncs;

    public AvailableShortCodeInfo() {
    }

    public AvailableShortCodeInfo(Msisdn shortCode, NcsType ncs) {
        this.shortCode = shortCode;
        this.ncs = ncs;
    }

    public Msisdn getShortCode() {
        return shortCode;
    }

    public void setShortCode(Msisdn shortCode) {
        this.shortCode = shortCode;
    }

    public NcsType getNcs() {
        return ncs;
    }

    public void setNcs(NcsType ncs) {
        this.ncs = ncs;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(50);
        builder.append("AvailableShortCodeInfo [ncs=");
        builder.append(ncs);
        builder.append(", shortCode=");
        builder.append(shortCode);
        builder.append("]");
        return builder.toString();
    }

}
