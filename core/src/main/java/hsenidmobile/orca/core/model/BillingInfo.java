/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$ 
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import hsenidmobile.orca.core.repository.PersistentObject;

@Entity
@Table(name = "billing_info")
public class BillingInfo extends PersistentObject {

    @Column(name = "billing_name")
    private String billingName;

    @Column(name = "billing_address", length = 255, nullable = true)
	private String billingAddress;

	@Column(name = "express_delivery")
	private boolean isExpressDelivery;


    public String getBillingName() {
        return billingName;
    }

    public void setBillingName(String billingName) {
        this.billingName = billingName;
    }

    public String getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(String billingAddress) {
		this.billingAddress = billingAddress;
	}

	public boolean isExpressDelivery() {
		return isExpressDelivery;
	}

	public void setExpressDelivery(boolean isExpressDelivery) {
		this.isExpressDelivery = isExpressDelivery;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(100);
		builder.append("BillingInfo [billingAddress=");
		builder.append(billingAddress);
		builder.append(", isExpressDelivery=");
		builder.append(isExpressDelivery);
		builder.append("]");
		return builder.toString();
	}

}