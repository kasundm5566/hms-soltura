/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.model;

import hsenidmobile.orca.core.repository.PersistentObject;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

/**
 * Single Content to be dispatched.
 *
 * $LastChangedDate$ $LastChangedBy$ $LastChangedRevision$
 *
 */
@Entity
@Table(name = "content")
public class Content extends PersistentObject {

    private static final long serialVersionUID =  5018988390311763922L;

    public enum ContentSource {
        WEB, SMS
    }

    @Column(name = "sdp_message_id")
    private String sdpMessageId;

    @Column(name = "sent_time")
    @Type(type="org.joda.time.contrib.hibernate.PersistentDateTime")
    private DateTime sentTime;

    @OneToOne(fetch = FetchType.EAGER, optional = false)
    @Cascade(value = { CascadeType.ALL, CascadeType.DELETE_ORPHAN })
    @JoinColumn(name = "localized_msg_id", nullable = false)
    private LocalizedMessage content;

    @ManyToOne(fetch=FetchType.EAGER, optional=true)
    @JoinColumn(name = "sub_category")
    private SubCategory subCategory;

    @Enumerated(EnumType.STRING)
    private ContentSource source;

    public String getSdpMessageId() {
        return sdpMessageId;
    }

    public void setSdpMessageId(String sdpMessageId) {
        this.sdpMessageId = sdpMessageId;
    }

    public DateTime getSentTime() {
        return sentTime;
    }

    public void setSentTime(DateTime sentTime) {
        this.sentTime = sentTime;
    }

    public LocalizedMessage getContent() {
        return content;
    }

    public void setContent(LocalizedMessage content) {
        this.content = content;
    }

    public SubCategory getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(SubCategory subCategory) {
        this.subCategory = subCategory;
    }

    public ContentSource getSource() {
        return source;
    }

    public void setSource(ContentSource source) {
        this.source = source;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(300);
        builder.append("Content [subCategory=");
        builder.append(subCategory);
        builder.append(", content=");
        builder.append(content);
        builder.append(", source=");
        builder.append(source);
        builder.append(", sdpMessageId=");
        builder.append(sdpMessageId);
        builder.append(", sentTime=");
        builder.append(sentTime);
        builder.append("]");
        return builder.toString();
    }

}
