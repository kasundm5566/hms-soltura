/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hsenidmobile.orca.core.model;

import hsenidmobile.orca.core.applications.ApplicationImpl;
import hsenidmobile.orca.core.repository.PersistentObject;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */

public class ContentProvider extends PersistentObject {

    private String cpId;
    private String spId;
    private String name;
    private String email;
    private String nic;
    private String telephone;
    private String contactPersonName;
    private String registerCode;
    private String description;
    private HashMap<String,List<RoutingKey>> ownedRoutingKeys;
    private HashMap<String,List<RoutingKey>> unassignedRoutingKeys;
    private List<CpFinancialInstrument> availableFinancialInstruments;
    private List<RoutingKey> ownedRoutingInfo;
    private List<ApplicationImpl> applications;
    private Map<String, String> additionalInfo;
    private Date createDate;
    private Date lastChargedDate;
    private Status status;
    private BillingInfo billingInfo;
    private Msisdn msisdn;
    private boolean isPersonal;
    private String userType;

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    /**
     * Creates the Content Provider for an application which starts with
     * application name followed by a namefive numeric characters
     *
     * @param contentProviderName
     *            Content Provider name for which the cpId must be generated
     * @throws ContentProviderException
     */
    public void createContentProviderId(String contentProviderName) throws ContentProviderException {
        if (cpId == null) {
            if (contentProviderName != null && contentProviderName.length() > 4) {
                contentProviderName = contentProviderName.substring(0, 4).toLowerCase().replaceAll(" ", "");
            }
            NumberFormat numberFormat = new DecimalFormat("0000");
            cpId = contentProviderName + "_" + numberFormat.format(new Random().nextInt(10000));
        } else {
            throw new ContentProviderException(ContentProviderException.CONTENT_PROVIDER_ID_ALREADY_EXISTS);
        }
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNic() {
        return nic;
    }

    public void setNic(String nic) {
        this.nic = nic;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getContactPersonName() {
        return contactPersonName;
    }

    public void setContactPersonName(String contactPersonName) {
        this.contactPersonName = contactPersonName;
    }

    public boolean isPersonal() {
        return isPersonal;
    }

    public void setPersonal(boolean personal) {
        isPersonal = personal;
    }

    public String getRegisterCode() {
        return registerCode;
    }

    public void setRegisterCode(String registerCode) {
        this.registerCode = registerCode;
    }

    private List<Application> getActiveServices() {
        throw new UnsupportedOperationException();
    }

    public void assignRoutingInfoToApplication(Application application, RoutingInfo routingInfo) {
        throw new UnsupportedOperationException();
    }

    public void reserverRoutingKey(RoutingKey routingkey) {
        throw new UnsupportedOperationException();
    }

    public HashMap<String, List<RoutingKey>> getUnassignedRoutingKeys() {
        return unassignedRoutingKeys;
    }

    public void setUnassignedRoutingKeys(HashMap<String, List<RoutingKey>> unassignedRoutingKeys) {
        this.unassignedRoutingKeys = unassignedRoutingKeys;
    }

    public String getCpId() {
        return cpId;
    }

    public void setCpId(String cpId) {
        this.cpId = cpId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BillingInfo getBillingInfo() {
        return billingInfo;
    }

    public void setBillingInfo(BillingInfo billingInfo) {
        this.billingInfo = billingInfo;
    }

    public List<ApplicationImpl> getApplications() {
        return applications;
    }

    public void setApplications(List<ApplicationImpl> applications) {
        this.applications = applications;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getLastChargedDate() {
        return lastChargedDate;
    }

    public void setLastChargedDate(Date lastChargedDate) {
        this.lastChargedDate = lastChargedDate;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Map<String, String> getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(Map<String, String> additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public List<CpFinancialInstrument> getAvailableFinancialInstruments() {
        return availableFinancialInstruments;
    }

    public void setAvailableFinancialInstruments(List<CpFinancialInstrument> availableFinancialInstruments) {
        this.availableFinancialInstruments = availableFinancialInstruments;
    }

    public HashMap<String, List<RoutingKey>> getOwnedRoutingKeys() {
        return ownedRoutingKeys;
    }

    public void setOwnedRoutingKeys(HashMap<String, List<RoutingKey>> ownedRoutingKeys) {
        this.ownedRoutingKeys = ownedRoutingKeys;
    }

    public List<RoutingKey> getOwnedRoutingInfo() {
        return ownedRoutingInfo;
    }

    public void setOwnedRoutingInfo(List<RoutingKey> ownedRoutingInfo) {
        this.ownedRoutingInfo = ownedRoutingInfo;
    }

    public String getSpId() {
        return spId;
    }

    public void setSpId(String spId) {
        this.spId = spId;
    }

    public Msisdn getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(Msisdn msisdn) {
        this.msisdn = msisdn;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(75);
        builder.append("ContentProvider [cpId=");
        builder.append(cpId);
        builder.append(", name=");
        builder.append(name);
        builder.append(", status=");
        builder.append(status);
        builder.append("]");
        return builder.toString();
    }
}
