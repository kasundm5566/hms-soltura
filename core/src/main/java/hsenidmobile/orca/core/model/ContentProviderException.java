package hsenidmobile.orca.core.model;
/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *   
 *   $LastChangedDate$
 *   $LastChangedBy$ 
 *   $LastChangedRevision$
 */

public class ContentProviderException extends OrcaException {

    public static final String CONTENT_PROVIDER_ID_ALREADY_EXISTS = "Content Provider ID Already Exists";
    public static final String CONTENT_PROVIDER_CHARGING_MSISDN_NOT_VERIFIED = "Charging MSISDN has not yet verified" +
            "hence not allowing to request new kwywords";

    public ContentProviderException(String message) {
        super(message);
    }
}
