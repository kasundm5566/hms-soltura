/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.model;

import hsenidmobile.orca.core.repository.PersistentObject;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.joda.time.DateTime;

import javax.persistence.*;
import java.util.List;

/**
 * $LastChangedDate$
 * $LastChangedBy$ $LastChangedRevision$
 *
 */
@MappedSuperclass
public class ContentRelease extends PersistentObject {

    private static final long serialVersionUID =  -7424205561634538252L;

    @OneToMany(cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    @Cascade(value = { org.hibernate.annotations.CascadeType.ALL, org.hibernate.annotations.CascadeType.DELETE_ORPHAN,
            org.hibernate.annotations.CascadeType.DELETE })
    private List<Content> contents;

    @Column(name = "app_id", nullable = false)
    private String appId;

    @Column(name = "app_name", nullable = false)
    private String appName;

    @Column(name = "message_id")
    private long BcMessageId;


    @Transient
    private long correlationId;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public List<Content> getContents() {
        return contents;
    }

    public Content getContent(String subCategory) {
        if (contents != null) {
            for (Content content : contents) {
                if (content.getSubCategory() != null && content.getSubCategory().isMatch(subCategory)) {
                    return content;
                }
            }
        }
        return null;

    }

    public void setContents(List<Content> contents) {
        this.contents = contents;
    }

    public void setContentSentTime(DateTime sentTime) {
        if (contents != null) {
            for (Content content : contents) {
                content.setSentTime(sentTime);
            }
        }
    }

    public long getCorrelationId() {
        return correlationId;
    }

    public void setCorrelationId(long correlationId) {
        this.correlationId = correlationId;
    }

    public long getBcMessageId() {
        return BcMessageId;
    }

    public void setBcMessageId(long bcMessageId) {
        BcMessageId = bcMessageId;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(300);
        builder.append("ContentRelease [correlationId=");
        builder.append(correlationId);
        builder.append(", appId=");
        builder.append(appId);
        builder.append(", appName=");
        builder.append(appName);
        builder.append(", contents=");
        builder.append(contents);
        builder.append("]");
        return builder.toString();
    }
}
