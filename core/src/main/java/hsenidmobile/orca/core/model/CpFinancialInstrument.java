/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hsenidmobile.orca.core.model;

/**
 * $LastChangedDate:$
 * $LastChangedBy:$
 * $LastChangedRevision:$
 */
public class CpFinancialInstrument {

    private String displayName;
    //todo this has to be renamed later to account id
    private String financialInstrumentId;
    private long instrumentId;

    public CpFinancialInstrument(String displayName, long payInstrumentId, String financialInstrumentId) {
        this.displayName = displayName;
        this.financialInstrumentId = financialInstrumentId;
        this.instrumentId = payInstrumentId;
    }

    public String getFinancialInstrumentId() {
        return financialInstrumentId;
    }


    public String getDisplayName() {
        return displayName;
    }

    public long getInstrumentId() {
        return instrumentId;
    }
}
