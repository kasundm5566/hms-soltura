/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$ 
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.model;

/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class DispatchStatusReport {
	public enum DispatchStatus {
		SENT_SUCCESS, SENT_FAIL, DELIVERY_SUCCESS, DELIVERY_FAIL;
	};

	private Msisdn msisdn;
	private String messageId;
	private DispatchStatus deliveryStatus;
	private String appId;
	private long sentTime;
	private long diliveredTime;
	private String chargedTransactionId;

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public long getSentTime() {
		return sentTime;
	}

	public void setSentTime(long sentTime) {
		this.sentTime = sentTime;
	}

	public long getDiliveredTime() {
		return diliveredTime;
	}

	public void setDiliveredTime(long diliveredTime) {
		this.diliveredTime = diliveredTime;
	}

	public void setMsisdn(Msisdn msisdn) {
		this.msisdn = msisdn;
	}

	public void setDeliveryStatus(DispatchStatus deliveryStatus) {
		this.deliveryStatus = deliveryStatus;
	}

	public Msisdn getMsisdn() {
		return msisdn;
	}

	public DispatchStatus getDeliveryStatus() {
		return deliveryStatus;
	}

	public String getChargedTransactionId() {
		return chargedTransactionId;
	}

	public void setChargedTransactionId(String chargedTransactionId) {
		this.chargedTransactionId = chargedTransactionId;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(200);
		builder.append("DispatchStatusReport [appId=");
		builder.append(appId);
		builder.append(", msisdn=");
		builder.append(msisdn);
		builder.append(", chargedTransactionId=");
		builder.append(chargedTransactionId);
		builder.append(", messageId=");
		builder.append(messageId);
		builder.append(", deliveryStatus=");
		builder.append(deliveryStatus);
		builder.append(", sentTime=");
		builder.append(sentTime);
		builder.append(", diliveredTime=");
		builder.append(diliveredTime);
		builder.append("]");
		return builder.toString();
	}
	
	
	
}
