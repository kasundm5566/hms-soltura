/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.model;

/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class DispatchSummary {
	private String appId;
	private String statusCode;
	private long totalCount;

	public DispatchSummary(String appId, String statusCode, long totalCount) {
		this.appId = appId;
		this.statusCode = statusCode;
		this.totalCount = totalCount;
	}

	public String getAppId() {
		return appId;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public long getTotalCount() {
		return totalCount;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(100);
		builder.append("DispatchSummary [appId=");
		builder.append(appId);
		builder.append(", statusCode=");
		builder.append(statusCode);
		builder.append(", totalCount=");
		builder.append(totalCount);
		builder.append("]");
		return builder.toString();
	}

}
