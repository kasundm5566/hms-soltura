/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hsenidmobile.orca.core.model;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public enum Event {
    CONTENT_UPDATED,
    CONTENT_PUBLISHED,
    CONTENT_SEND,
    CONTENT_SENT,
    CONTENT_DELETED,
    CONTENT_RECEIVED,
    SUCCESS_RESPONSE,
    FAILURE_RESPONSE,
    VOTES_RECEIVED,
    REQUESTS_RECEIVED
}
