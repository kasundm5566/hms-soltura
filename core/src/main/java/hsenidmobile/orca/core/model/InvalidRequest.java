/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.model;

import hsenidmobile.orca.core.repository.PersistentObject;
import hsenidmobile.orca.core.util.Util;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 *
 * @version $LastChangedRevision$
 */
public class InvalidRequest extends PersistentObject {

	public enum InvalidReason {
		APP_NOT_FOUND, APP_NOT_ACTIVE, INVALID_KEYWORD, ROUTING_KEY_NOT_ASSIGNED;
	};

	private String appId;
	private long correlationId;
	private Msisdn sourceMsisdn;
	private Msisdn destinationMsisdn;
	private String messageText;
	private long receivedTime;
	public InvalidReason reason;

	public InvalidRequest(long correlationId) {
		this.correlationId = correlationId;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getMessageText() {
		return messageText;
	}

	public void setMessageText(String messageText) {
		this.messageText = messageText;
	}

	public Msisdn getSourceMsisdn() {
		return sourceMsisdn;
	}

	public void setSourceMsisdn(Msisdn sourceMsisdn) {
		this.sourceMsisdn = sourceMsisdn;
	}

	public long getReceivedTime() {
		return receivedTime;
	}

	public void setReceivedTime(long receivedTime) {
		this.receivedTime = receivedTime;
	}

	public Msisdn getDestinationMsisdn() {
		return destinationMsisdn;
	}

	public void setDestinationMsisdn(Msisdn destinationMsisdn) {
		this.destinationMsisdn = destinationMsisdn;
	}

	public InvalidReason getReason() {
		return reason;
	}

	public void setReason(InvalidReason reason) {
		this.reason = reason;
	}

	public String getTransLog() {
		StringBuilder builder = new StringBuilder(250);
		builder.append(correlationId);
		builder.append("|");
		builder.append(receivedTime);
		builder.append("|");
		builder.append(formatString(appId));
		builder.append("|");
		builder.append(messageText);
		builder.append("|");
		builder.append(sourceMsisdn.getOperator());
		builder.append("|");
		builder.append(sourceMsisdn.getAddress());
		builder.append("|");
		builder.append(reason);
		return builder.toString();
	}

	private String formatString(String str) {
		return Util.isEmpty(str) ? "" : str;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("InvalidRequest [appId=");
		builder.append(appId);
		builder.append(", correlationId=");
		builder.append(correlationId);
		builder.append(", sourceMsisdn=");
		builder.append(sourceMsisdn);
		builder.append(", messageText=");
		builder.append(messageText);
		builder.append(", receivedTime=");
		builder.append(receivedTime);
		builder.append(", reason=");
		builder.append(reason);
		builder.append("]");
		return builder.toString();
	}

}
