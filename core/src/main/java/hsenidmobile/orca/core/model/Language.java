package hsenidmobile.orca.core.model;

import javax.persistence.Entity;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
@Deprecated //TODO: Delete this
@Entity
public enum Language {


    ENGLISH {
        @Override
        public String toString() {
            return "en";
        }
    },

    TAMIL {
        @Override
        public String toString() {
            return "ta";
        }
    },

    SINHALA  {
        @Override
        public String toString() {
            return "si";
        }
    }

}
