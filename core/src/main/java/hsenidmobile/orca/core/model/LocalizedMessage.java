/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.model;

import java.util.Locale;

import hsenidmobile.orca.core.repository.PersistentObject;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * $LastChangedDate$ $LastChangedBy$ $LastChangedRevision$
 *
 */
@Entity
@Table(name = "localized_message")
public class LocalizedMessage extends PersistentObject {

    private static final long serialVersionUID =  4658461212987249201L;

    private String message;

    private Locale locale;

    public LocalizedMessage() {
    }

    public LocalizedMessage(String message, Locale locale) {
        this.message = message;
        this.locale = locale;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(250);
        builder.append("LocalizedMessage [message=");
        builder.append(message);
        builder.append(", locale=");
        builder.append(locale);
        builder.append("]");
        return builder.toString();
    }

}
