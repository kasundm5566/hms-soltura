/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hsenidmobile.orca.core.model;

import hsenidmobile.orca.core.report.MessageHistoryRecord;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.DateFormat;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class MessageHistory {

    private final SimpleDateFormat dateFormater = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
    private int id;
    private long messageTimeStamp;
    private String messageTimeStampInDateFormat;
    private String applicationId;
    private String applicationName;
    private String keyWord;
    private Event event;
    private String message;
    private final String SPLITTER = ",";
    private MessageHistoryRecord messageHistoryRecord;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getMessageTimeStamp() {
        return messageTimeStamp;
    }

    public void setMessageTimeStamp(long messageTimeStamp) {
        this.messageTimeStamp = messageTimeStamp;
    }

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getKeyWord() {
        return keyWord;
    }

    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessageTimeStampInDateFormat() {
        return dateFormater.format(messageTimeStamp);
    }

    public void setMessageTimeStampInDateFormat(String messageTimeStampInDateFormat) {
        this.messageTimeStampInDateFormat = messageTimeStampInDateFormat;
    }

    public MessageHistoryRecord getMessageHistoryRecord() {
        return messageHistoryRecord;
    }

    public void setMessageHistoryRecord(MessageHistoryRecord messageHistoryRecord) {
        this.messageHistoryRecord = messageHistoryRecord;
    }

    public String getServiceType() {
        if(messageHistoryRecord != null) {
            return this.messageHistoryRecord.getServiceType();
        } else {
            return "";   
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(100);
        builder.append(id);
        builder.append(SPLITTER);
        builder.append(messageTimeStamp);
        builder.append(SPLITTER);
        builder.append(applicationId);
        builder.append(SPLITTER);
        builder.append(applicationName);
        builder.append(SPLITTER);
        builder.append(keyWord);
        builder.append(SPLITTER);
        builder.append(event);
        builder.append(SPLITTER);
        builder.append(message);
        return builder.toString();
    }
}
