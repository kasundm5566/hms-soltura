/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Msisdn {

	@Column(name = "address", length = 40, nullable = false)
	private String address;

	@Column(length = 10, nullable = false)
	private String operator;

	/**
	 * Create Msisdn
	 *
	 * @param address
	 * @param operator
	 */
	public Msisdn(String address, String operator) {
		super();
		this.address = address;
		this.operator = operator;
	}

	/**
	 * Create blank msisdn
	 */
	public Msisdn() {
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((operator == null) ? 0 : operator.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Msisdn other = (Msisdn) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		//TODO: removed comparison of operator
//		if (operator == null) {
//			if (other.operator != null)
//				return false;
//		} else if (!operator.equals(other.operator))
//			return false;
		return true;
	}


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Msisdn [address=");
		builder.append(address);
		builder.append(", operator=");
		builder.append(operator);
		builder.append("]");
		return builder.toString();
	}

}