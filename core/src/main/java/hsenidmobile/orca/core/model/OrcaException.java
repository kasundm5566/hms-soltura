/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.model;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 *
 * @version $LastChangedRevision$
 */
public class OrcaException extends Exception {

	public OrcaException() {
		super();
	}

	public OrcaException(String message, Throwable cause) {
		super(message, cause);
	}

	public OrcaException(String message) {
		super(message);
	}

	public OrcaException(Throwable cause) {
		super(cause);
	}

}
