/* 
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited. 
 *   All Rights Reserved. 
 * 
 *   These materials are unpublished, proprietary, confidential source code of 
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET 
 *   of hSenid Software International (Pvt) Limited. 
 * 
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual 
 *   property rights in these materials. 
 *
 */
package hsenidmobile.orca.core.model;

import hsenidmobile.orca.core.repository.PersistentObject;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
@Entity
@Table(name = "orca_sdp_app_integration")
public class OrcaSdpAppIntegration extends PersistentObject {

    @Column(name = "orca_app_id")
    private String orcaAppId;

    @Column(name = "sdp_app_id")
    private String sdpAppId;

    @Column(name = "sdp_app_password")
    private String sdpAppPassword;

    @Column(name = "sp_id")
    private String spId;

    public OrcaSdpAppIntegration(String orcaAppId, String sdpAppId, String sdpAppPassword, String spId) {
        this.orcaAppId = orcaAppId;
        this.sdpAppId = sdpAppId;
        this.sdpAppPassword = sdpAppPassword;
        this.spId = spId;
    }

    public OrcaSdpAppIntegration() {
    }

    public String getOrcaAppId() {
        return orcaAppId;
    }

    public void setOrcaAppId(String orcaAppId) {
        this.orcaAppId = orcaAppId;
    }

    public String getSdpAppId() {
        return sdpAppId;
    }

    public void setSdpAppId(String sdpAppId) {
        this.sdpAppId = sdpAppId;
    }

    public String getSdpAppPassword() {
        return sdpAppPassword;
    }

    public void setSdpAppPassword(String sdpAppPassword) {
        this.sdpAppPassword = sdpAppPassword;
    }

    public String getSpId() {
        return spId;
    }

    public void setSpId(String spId) {
        this.spId = spId;
    }

    public String toString() {
        StringBuilder builder = new StringBuilder(200);
        builder.append("SDP App Integration Data [orcaAppId=");
        builder.append(orcaAppId);
        builder.append(", sdpAppId=");
        builder.append(sdpAppId);
        builder.append(", sdpAppPassword=");
        builder.append(sdpAppPassword);
        builder.append("]");
        return builder.toString();
    }
}
