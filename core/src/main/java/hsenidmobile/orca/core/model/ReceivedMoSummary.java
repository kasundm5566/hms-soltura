/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.model;

/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class ReceivedMoSummary {
	private String appId;
	private long moCount;
	private long lastReceivedTime;

	public ReceivedMoSummary(String appId, long moCount, long lastReceivedTime) {
		this.appId = appId;
		this.moCount = moCount;
		this.lastReceivedTime = lastReceivedTime;
	}

	public String getAppId() {
		return appId;
	}

	public long getMoCount() {
		return moCount;
	}

	public long getLastReceivedTime() {
		return lastReceivedTime;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(50);
		builder.append("ReceivedMoSummary [appId=");
		builder.append(appId);
		builder.append(", moCount=");
		builder.append(moCount);
		builder.append(", lastReceivedTime=");
		builder.append(lastReceivedTime);
		builder.append("]");
		return builder.toString();
	}

}
