/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 *
 */
package hsenidmobile.orca.core.model;

import hsenidmobile.orca.core.repository.PersistentObject;

import javax.persistence.*;

/**
 * Response received from Mobile Subscriber For a keyword option in a service.
 * e.g. Response for a Competition application.
 *
 * All the child class should record responses in there own tables.
 *
 * $LastChangedDate: $ $LastChangedBy: $
 *
 * @version $LastChangedRevision: $
 */
@MappedSuperclass
public class Response extends PersistentObject {

	@Embedded
	private Msisdn msisdn;

	@Column(name = "received_message", length = 255)
	private String receivedMessage;

	@Column(name = "received_time", nullable = false)
	private long receivedTime;

	@Column(name = "service_id", nullable = false)
	private long serviceId;

	public Msisdn getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(Msisdn msisdn) {
		this.msisdn = msisdn;
	}

	public String getReceivedMessage() {
		return receivedMessage;
	}

	public void setReceivedMessage(String recivedMessage) {
		this.receivedMessage = recivedMessage;
	}

	public long getReceivedTime() {
		return receivedTime;
	}

	public void setReceivedTime(long receivedTime) {
		this.receivedTime = receivedTime;
	}

	public long getServiceId() {
		return serviceId;
	}

	public void setServiceId(long serviceId) {
		this.serviceId = serviceId;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(200);
		builder.append("Response [msisdn=");
		builder.append(msisdn);
		builder.append(", receivedMessage=");
		builder.append(receivedMessage);
		builder.append(", receivedTime=");
		builder.append(receivedTime);
		builder.append("]");
		return builder.toString();
	}
}