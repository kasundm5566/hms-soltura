/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hsenidmobile.orca.core.model;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class RevenueSummary {

    private RevenueSummaryPrimaryKey revenueSummaryPrimaryKey;
    private String applicationName;
    private long totalRevenue;
    private int totalTransCount;
    private final String SPLITTER = ",";

    public RevenueSummaryPrimaryKey getRevenueSummaryPrimaryKey() {
        return revenueSummaryPrimaryKey;
    }

    public void setRevenueSummaryPrimaryKey(RevenueSummaryPrimaryKey revenueSummaryPrimaryKey) {
        this.revenueSummaryPrimaryKey = revenueSummaryPrimaryKey;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public long getTotalRevenue() {
        return totalRevenue;
    }

    public void setTotalRevenue(long totalRevenue) {
        this.totalRevenue = totalRevenue;
    }

    public int getTotalTransCount() {
        return totalTransCount;
    }

    public void setTotalTransCount(int totalTransCount) {
        this.totalTransCount = totalTransCount;
    }

    public String printReport() {
        StringBuilder builder = new StringBuilder(100);
        builder.append(applicationName);
        builder.append(SPLITTER);
        builder.append(revenueSummaryPrimaryKey.getApplicationId());
        builder.append(SPLITTER);
        builder.append(totalTransCount);
        builder.append(SPLITTER);
        builder.append(revenueSummaryPrimaryKey.getDirection());
        builder.append(SPLITTER);
        builder.append(totalRevenue);
        return builder.toString();
    }
}
