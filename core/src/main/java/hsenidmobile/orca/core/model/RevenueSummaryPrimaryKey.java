/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hsenidmobile.orca.core.model;

import hsenidmobile.orca.core.repository.PersistentObject;


/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class RevenueSummaryPrimaryKey extends PersistentObject {

    private int dateId;
    private String providerId;
    private String applicationId;
    private String partyChargeServiceType;
    private String direction;

    public int getDateId() {
        return dateId;
    }

    public void setDateId(int dateId) {
        this.dateId = dateId;
    }

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getPartyChargeServiceType() {
        return partyChargeServiceType;
    }

    public void setPartyChargeServiceType(String partyChargeServiceType) {
        this.partyChargeServiceType = partyChargeServiceType;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    @Override
    public String toString() {
        return "RevenueSummaryPrimaryKey{" +
                "dateId=" + dateId +
                ", providerId='" + providerId + '\'' +
                ", applicationId='" + applicationId + '\'' +
                ", partyChargeServiceType='" + partyChargeServiceType + '\'' +
                ", direction='" + direction + '\'' +
                '}';
    }
}
