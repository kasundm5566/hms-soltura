/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *
 */
package hsenidmobile.orca.core.model;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class RoutingInfo {

    private String owningAppId;

    private long startDate;

    private long endDate;

    private RoutingKey routingKey;

    private long lastChargedDate;

    private RoutingInfoStatus status;

    private ContentProvider ownerCp;

    private boolean scheduledForRelease;

    public RoutingInfoStatus getStatus() {
        return status;
    }

    public void setStatus(RoutingInfoStatus status) {
        this.status = status;
    }

    public String getOwningAppId() {
        return owningAppId;
    }

    public void setOwningAppId(String owningAppId) {
        this.owningAppId = owningAppId;
    }

    public ContentProvider getOwnerCp() {
        return this.ownerCp;
    }

    public long getStartDate() {
        return startDate;
    }

    public void setStartDate(long startDate) {
        this.startDate = startDate;
    }

    public long getEndDate() {
        return endDate;
    }

    public void setEndDate(long endDate) {
        this.endDate = endDate;
    }

    public RoutingKey getRoutingKey() {
        return routingKey;
    }

    public void setRoutingKey(RoutingKey routingKey) {
        this.routingKey = routingKey;
    }

    public long getLastChargedDate() {
        return lastChargedDate;
    }

    public void setLastChargedDate(long lastChargedDate) {
        this.lastChargedDate = lastChargedDate;
    }

    public void setOwnerCp(ContentProvider ownerCp) {
        this.ownerCp = ownerCp;
    }

    public void setScheduledForRelease(boolean scheduledForRelease) {
        this.scheduledForRelease = scheduledForRelease;
    }

    public boolean getScheduledForRelease() {
        return scheduledForRelease;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(200);
        builder.append("RoutingInfo [endDate=");
        builder.append(endDate);
        builder.append(", lastChargedDate=");
        builder.append(lastChargedDate);
        builder.append(", owningAppId=");
        builder.append(owningAppId);
        builder.append(", ownerCp=");
        builder.append(ownerCp);
        builder.append(", routingKey=");
        builder.append(routingKey);
        builder.append(", startDate=");
        builder.append(startDate);
        builder.append(", status=");
        builder.append(status);
        builder.append(", scheduledForRelease=");
        builder.append(scheduledForRelease);
        builder.append("]");
        return builder.toString();
    }

}