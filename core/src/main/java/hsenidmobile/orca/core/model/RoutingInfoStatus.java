/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hsenidmobile.orca.core.model;

public enum RoutingInfoStatus {
    /**
     * Being assigned to an application.
     */
    ASSIGNED {
        public short getSdpRkStatusValue() {
            return 1;
        }},
    /**
     * Is free and can be used to assigned to an application.
     */
    AVAILABLE {
        public short getSdpRkStatusValue() {
            return 2;
        }},

    PASSIVE {
        public short getSdpRkStatusValue() {
            return -1;
        }},
    RELEASED {
        public short getSdpRkStatusValue() {
            return -3;
        }},
    /**
     * Inactive and cannot assigned to an application.
     */
    RETIRED {
        public short getSdpRkStatusValue() {
            return -2;
        }
    };

    public abstract short getSdpRkStatusValue();

    public static RoutingInfoStatus getOrcaRkStatus(short i){
        switch(i) {
            case 1: return RoutingInfoStatus.ASSIGNED;
            case 2:  return RoutingInfoStatus.AVAILABLE;
            case -1: return RoutingInfoStatus.PASSIVE;
            case -2: return RoutingInfoStatus.RETIRED;
            case -3: return RoutingInfoStatus.RELEASED;
        }
        throw new AssertionError("Unknown SDP Routing Key Status");
    }
}
