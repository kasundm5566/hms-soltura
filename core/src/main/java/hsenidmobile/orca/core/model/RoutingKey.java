/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hsenidmobile.orca.core.model;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class RoutingKey {

    private Msisdn shortCode;

    private String keyword;

    private String app_id;

    private String status;

    /**
     * Create RoutingKey with shortcode and keyword
     *
     * @param shortCode
     * @param keyword
     */
    public RoutingKey(Msisdn shortCode, String keyword) {
        super();
        this.shortCode = shortCode;
        this.keyword = keyword;
    }

    public RoutingKey() {
    }

    /**
     * ShortCode
     *
     * @return
     */
    public Msisdn getShortCode() {
        return shortCode;
    }

    /**
     * Keyword
     *
     * @return
     */
    public String getKeyword() {
        return keyword;
    }

    public void setShortCode(Msisdn shortCode) {
        this.shortCode = shortCode;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;

    }

    public String getApp_id() {
        return app_id;
    }

    public void setApp_id(String app_id) {
        this.app_id = app_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((keyword == null) ? 0 : keyword.hashCode());
        result = prime * result + ((shortCode == null) ? 0 : shortCode.hashCode());
        return result;
    }

    public boolean equals(RoutingKey other) {
        if (keyword == null) {
            if (other.keyword != null)
                return false;
        } else if (!keyword.equals(other.keyword))
            return false;
        if (shortCode == null) {
            if (other.shortCode != null)
                return false;
        } else if (!shortCode.equals(other.shortCode))
            return false;
        return true;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(50);
        builder.append("RoutingKey [shortCode=");
        builder.append(shortCode);
        builder.append(", keyword=");
        builder.append(keyword);
        builder.append(", app-id=");
        builder.append(app_id);
        builder.append(", STATUS=");
        builder.append(status);
        builder.append("]");
        return builder.toString();
    }

}