/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$ 
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.model;

/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class SdpAppLoginData {
	private String appId;
	private String password;

	public SdpAppLoginData(String appId, String password) {
		super();
		this.appId = appId;
		this.password = password;
	}

	public String getAppId() {
		return appId;
	}

	public String getPassword() {
		return password;
	}
}
