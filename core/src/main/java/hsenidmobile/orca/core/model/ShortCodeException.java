package hsenidmobile.orca.core.model;
/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *   
 *   $LastChangedDate$
 *   $LastChangedBy$ 
 *   $LastChangedRevision$
 */

public class ShortCodeException extends OrcaException{

    public static final String AGGREGATOR_DUPLICATE = "Aggregator can only have one element";
    
    public ShortCodeException() {
    }

    public ShortCodeException(String message, Throwable cause) {
        super(message, cause);
    }

    public ShortCodeException(String message) {
        super(message);
    }
}
