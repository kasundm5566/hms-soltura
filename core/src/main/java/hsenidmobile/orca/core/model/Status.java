/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.model;

import java.util.EnumSet;

public enum Status {

    ACTIVE("active-production") {
        @Override
        public boolean isActive() {
            return true;
        }
    },
    SCHEDULED_ACTIVE("scheduled-active-production"){
        @Override
        public boolean isActive() {
            return true;
        }
    },
    INACTIVE("inactive"),
    /**
     * Blocked by us due to failure to comply to T & C or because of subscriber complains.
     */
    BLOCKED("blocked"),
    TERMINATED("terminate"),
    RETIRED("retired"),
    EXPIRED("expired"),
    SUSPENDED("suspend"),
    PENDING_FOR_APPROVAL("pending-approve"),
    REJECTED("reject"),
    REQUEST_FOR_APPROVAL("requested"),
    TEST_MODE("limited-production"),
    PROV_APP_NOT_FOUND("E1304");

    private String str;

    Status(String str) {

        this.str = str;
    }

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }

    public boolean isActive() {
        return false;
    }

    /**
     * Create Status enum from given string code.
     * @param code
     * @return
     */
    public static Status getEnum(String code) {
        if (code == null || code.trim().length() == 0) {
            throw new IllegalArgumentException("Status code cannot be empty or null.");
        }

        for (Status tag : EnumSet.allOf(Status.class)) {
            if (tag.getStr().equalsIgnoreCase(code)) {
                return tag;
            }
        }
        Status status = PROV_APP_NOT_FOUND;
        status.setStr(code.toLowerCase());
        return status;
    }
}