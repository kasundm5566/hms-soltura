/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.model;

import hsenidmobile.orca.core.applications.exception.ApplicationException;
import hsenidmobile.orca.core.flow.MessageFlowErrorReasons;
import hsenidmobile.orca.core.repository.PersistentObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CollectionOfElements;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 * $LastChangedDate$ $LastChangedBy$ $LastChangedRevision$
 *
 */
@Entity
@Table(name = "sub_category")
public class SubCategory extends PersistentObject {

    private static final long serialVersionUID =  -3457235838011370937L;
    private String keyword;

    @CollectionOfElements(targetElement = String.class)
    @Cascade(value = { org.hibernate.annotations.CascadeType.ALL })
    @JoinTable(name = "keyword_aliases")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<String> aliases;

    @OneToMany(cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    @Cascade(value = { org.hibernate.annotations.CascadeType.ALL, org.hibernate.annotations.CascadeType.DELETE_ORPHAN,
            org.hibernate.annotations.CascadeType.DELETE })
    @JoinTable(name = "sub_category_description")
    private List<LocalizedMessage> description;

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public List<String> getAliases() {
        return aliases;
    }

    public void setAliases(List<String> aliases) {
        this.aliases = aliases;
    }

    public List<LocalizedMessage> getDescription() {
        return description;
    }

    public void setDescription(List<LocalizedMessage> description) {
        this.description = description;
    }

    public LocalizedMessage getDescription(Locale locale) {
        if (this.description != null) {
            for (LocalizedMessage desc : this.description) {
                if (desc.getLocale().equals(locale)) {
                    return desc;
                }
            }
        }

        return null;
    }

    public void addDescription(LocalizedMessage description) {
        if (this.description == null) {
            this.description = new ArrayList<LocalizedMessage>();
        }

        this.description.add(description);
    }

    /**
     * Check whether this is having matching keyword for given keyword. Return
     * true is keyword or any of the aliases matches with the given one.
     *
     * @param subKeyword
     *            - SubKeyword String
     * @return - True OR False
     */
    public boolean isMatch(String subKeyword) {
        if (this.keyword.equalsIgnoreCase(subKeyword)) {
            return true;
        } else if (this.aliases != null) {
            for (String alias : this.aliases) {
                if (alias.equalsIgnoreCase(subKeyword)) {
                    return true;
                }
            }
        }

        return false;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(50);
        builder.append("SubCategory [keyword=");
        builder.append(keyword);
        builder.append("]");
        return builder.toString();
    }

}
