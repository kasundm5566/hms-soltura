/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hsenidmobile.orca.core.model.message;

import hsenidmobile.orca.core.model.Msisdn;

import java.util.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */


public abstract class AbstractMessageImpl implements Message {

	private long correlationId;
    private Msisdn senderAddress;
    private List<Msisdn> receiverAddresses;
    private String message;
    private String keyword;
    private String tariff;
    private Locale language;
    private String appId;

    public Msisdn getSenderAddress() {
        return senderAddress;
    }

    public void setSenderAddress(Msisdn senderAddress) {
        this.senderAddress = senderAddress;
    }

    public List<Msisdn> getReceiverAddresses() {
        return receiverAddresses;
    }

    public void setReceiverAddresses(List<Msisdn> receiverAddresses) {
        this.receiverAddresses = receiverAddresses;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    @Override
    public void addReceiverAddress(Msisdn receiverAddress) {
        if (this.receiverAddresses == null) {
            this.receiverAddresses = new ArrayList<Msisdn>();
        }
        this.receiverAddresses.add(receiverAddress);
    }

	public String getTariff() {
		return tariff;
	}

	public void setTariff(String tariff) {
		this.tariff = tariff;
	}

	public Locale getLanguage() {
		return language;
	}

	public void setLanguage(Locale language) {
		this.language = language;
	}

	public long getCorrelationId() {
		return correlationId;
	}

	public void setCorrelationId(long correlationId) {
		this.correlationId = correlationId;
	}

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(250);
		builder.append("AbstractMessageImpl [correlationId=");
		builder.append(correlationId);
		builder.append(", keyword=");
		builder.append(keyword);
		builder.append(", language=");
		builder.append(language);
		builder.append(", message=");
		builder.append(message);
		builder.append(", receiverAddresses=");
		builder.append(receiverAddresses);
		builder.append(", senderAddress=");
		builder.append(senderAddress);
		builder.append(", tariff=");
		builder.append(tariff);
        builder.append(", appId=");
		builder.append(appId);
		builder.append("]");
		return builder.toString();
	}

}
