/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$ 
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.model.message;

/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class AoRequest {
	/**
	 * Received Messge
	 */
	private Message message;

	/**
	 * Password used for the authorization in the authentication header
	 */
	private String password;

	/**
	 * IP address of the remote host (client) who is making the request.
	 */
	private String remoteHost;

	/**
	 * Application Id
	 */
	private String appId;

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRemoteHost() {
		return remoteHost;
	}

	public void setRemoteHost(String remoteHost) {
		this.remoteHost = remoteHost;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public Message getMessage() {
		return message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}
	
}
