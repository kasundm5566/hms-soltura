/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.model.message;

import hsenidmobile.orca.core.model.ContentRelease;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 *
 * @version $LastChangedRevision$
 */
public class AppDataUpdateRequest implements Serializable {
	private String appId;
	private String correlationId;
	private List<ContentRelease> updateContent;
    private static final long serialVersionUID = 3417972486055156428L;

	public AppDataUpdateRequest(Map<String, Object> request) {
		this.appId = (String) request.get("APP_ID");
		this.correlationId = (String)request.get("CORRELATION_ID");
		this.updateContent = (List<ContentRelease>) request.get("CONTENT");
	}

	public AppDataUpdateRequest(){

	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public List<ContentRelease> getUpdateContent() {
		return updateContent;
	}

	public void setUpdateContent(List<ContentRelease> updateContent) {
		this.updateContent = updateContent;
	}

	public String getCorrelationId() {
		return correlationId;
	}

	public void setCorrelationId(String correlationId) {
		this.correlationId = correlationId;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AppDataUpdateRequest [appId=");
		builder.append(appId);
		builder.append(", correlationId=");
		builder.append(correlationId);
		builder.append(", updateContent=");
		builder.append(updateContent);
		builder.append("]");
		return builder.toString();
	}
}
