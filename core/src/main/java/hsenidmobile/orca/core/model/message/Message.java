/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.model.message;

import hsenidmobile.orca.core.model.Msisdn;

import java.util.List;
import java.util.Locale;

public interface Message {

    Msisdn getSenderAddress();
    void setSenderAddress(Msisdn senderAddress);

    List<Msisdn> getReceiverAddresses();
    void setReceiverAddresses(List<Msisdn> receiverAddress);

    /**
     * Add destination address to the list of destinations.
     * @param receiverAddress
     */
    void addReceiverAddress(Msisdn receiverAddress);

    void setMessage(String message);
    String getMessage();

    String getKeyword();
    void setKeyword(String message);

	String getTariff();
	void setTariff(String tariff);

	Locale getLanguage();
	void setLanguage(Locale language);

	long getCorrelationId();
	void setCorrelationId(long correlationId);

    String getAppId();
    void setAppId(String app_Id);
}