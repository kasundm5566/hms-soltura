/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$ 
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.model.message;

import hsenidmobile.orca.core.model.Application;

import java.util.Map;

public class MessageContext {


	private long receiveTime;
	private Map<String, Object> additionalInfo;
	private Application application;

	public long getReceiveTime() {
		return receiveTime;
	}

	public void setReceiveTime(long receiveTime) {
		this.receiveTime = receiveTime;
	}

	public Map<String, Object> getAdditionalInfo() {
		return additionalInfo;
	}

	public void setAdditionalInfo(Map<String, Object> additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	/**
	 * Set the application responsible of processing received message.
	 * @return
	 */
	public Application getApplication() {
		return application;
	}

	/**
	 * Get the application responsible of processing received message.
	 * @param application
	 */
	public void setApplication(Application application) {
		this.application = application;
	}
	
}