/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$ 
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.model.message;

/**
 * Response to be sent back to the Mobile Subscriber.
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class MoResponse {
	
	private MtRequest dataResponse;
	
	private String chargingTranactionId;
    private long voteId;

	/**
	 * Data response message to be sent back.
	 * @return
	 */
	public MtRequest getDataResponse() {
		return dataResponse;
	}

	/**
	 * Set the response message.
	 * @param dataResponse
	 */
	public void setDataResponse(MtRequest dataResponse) {
		this.dataResponse = dataResponse;
	}

	public String getChargingTranactionId() {
		return chargingTranactionId;
	}

	public void setChargingTranactionId(String chargingTranactionId) {
		this.chargingTranactionId = chargingTranactionId;
	}

    public long getVoteId() {
        return voteId;
    }

    public void setVoteId(long voteId) {
        this.voteId = voteId;
    }

    @Override
    public String toString() {
        return "MoResponse{" +
                "dataResponse=" + dataResponse +
                ", chargingTranactionId='" + chargingTranactionId + '\'' +
                '}';
    }
}
