/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$ 
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.model.message;

/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class MtRequest {
	private Message message;
	private String chargingTransactionId;

	public MtRequest(Message message, String chargingTransactionId) {
		super();
		this.message = message;
		this.chargingTransactionId = chargingTransactionId;
	}

	public Message getMessage() {
		return message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}

	public String getChargingTransactionId() {
		return chargingTransactionId;
	}

	public void setChargingTransactionId(String chargingTransactionId) {
		this.chargingTransactionId = chargingTransactionId;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MtRequest [chargingTransactionId=");
		builder.append(chargingTransactionId);
		builder.append(", message=");
		builder.append(message);
		builder.append("]");
		return builder.toString();
	}
	
}
