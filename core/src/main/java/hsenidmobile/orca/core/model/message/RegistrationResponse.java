/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$ 
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.model.message;

/**
 * Response to be sent back to the Mobile Subscriber when registration request
 * comes for a service.
 * 
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class RegistrationResponse extends MoResponse {
	/**
	 * Subscription Success/Failure response message.
	 */
	private MtRequest responseForSubscriptionRequest;

	public MtRequest getResponseForSubscriptionRequest() {
		return responseForSubscriptionRequest;
	}

	public void setResponseForSubscriptionRequest(
			MtRequest responseForSubscriptionRequest) {
		this.responseForSubscriptionRequest = responseForSubscriptionRequest;
	}

}