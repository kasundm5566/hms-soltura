package hsenidmobile.orca.core.model.message;

/*   request show reply message bind object
 *
 *   $LastChangedDate: 2011-08-01 13:48:30 +0530 (Mon, 01 Aug 2011) $
 *   $LastChangedBy: supunh $
 *   $LastChangedRevision: 75446 $
 */
public class RequestMessage {

    private String appId;
    private String message;
    private String pageNumber;
    private long messageId;
    private int messageMaxLength;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(String pageNumber) {
        this.pageNumber = pageNumber;
    }

    public long getMessageId() {
        return messageId;
    }

    public void setMessageId(Long messageId) {
        this.messageId = messageId;
    }

    public void setMessageMaxLength(int messageMaxLength) {
        this.messageMaxLength = messageMaxLength;
    }

    public int getMessageMaxLength() {
        return messageMaxLength;
    }

    @Override
    public String toString() {
        return "RequestMessage{" +
                "appId='" + appId + '\'' +
                ", message='" + message + '\'' +
                ", pageNumber='" + pageNumber + '\'' +
                ", messageId=" + messageId +
                '}';
    }
}
