/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate: 2011-07-23 13:48:39 +0530 (Sat, 23 Jul 2011) $
 *   $LastChangedBy: dulika $
 *   $LastChangedRevision: 75212 $
 */
package hsenidmobile.orca.core.model.message;

import java.io.Serializable;
import java.util.Map;

/**
 * $LastChangedDate: 2011-07-23 13:48:39 +0530 (Sat, 23 Jul 2011) $
 * $LastChangedBy: dulika $
 * $LastChangedRevision: 75212 $
 */
public class RequestShowReplyRequest implements Serializable {
    private String appId;
    private String recepient;
    private String messageContent;
    private long messageId;
    private static final long serialVersionUID = 3407972481055156428L;

    public RequestShowReplyRequest(){

    }

    public RequestShowReplyRequest(Map<String, Object> request) {
		this.appId = (String) request.get("APP_ID");
		this.recepient = (String)request.get("RECEPIENT");
		this.messageContent = (String) request.get("CONTENT");
	}

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getRecepient() {
        return recepient;
    }

    public void setRecepient(String recepient) {
        this.recepient = recepient;
    }

    public String getMessageContent() {
        return messageContent;
    }

    public void setMessageContent(String messageContent) {
        this.messageContent = messageContent;
    }

    public long getMessageId() {
        return messageId;
    }

    public void setMessageId(long messageId) {
        this.messageId = messageId;
    }

    @Override
    public String toString() {
        return "RequestShowReplyRequest{" +
                "appId='" + appId + '\'' +
                ", recepient='" + recepient + '\'' +
                ", messageContent='" + messageContent + '\'' +
                ", messageId=" + messageId +
                '}';
    }
}
