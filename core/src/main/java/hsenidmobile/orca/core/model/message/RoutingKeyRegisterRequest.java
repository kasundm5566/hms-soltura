/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hsenidmobile.orca.core.model.message;

import java.io.Serializable;

/**
 * $LastChangedDate: 2010-11-17 11:41:22 +0530 (Wed, 17 Nov 2010) $
 * $LastChangedBy: sandarenu $
 * $LastChangedRevision: 67562 $
 */
public class RoutingKeyRegisterRequest implements Serializable {

    private String shortCode;
    private String keyword;
    private String spId;
    private String ncsType;
    private String expireDate;
    private boolean performRkCreationCharging = true;
    private long correlationId;
    private String chargingCycleSize;

    public RoutingKeyRegisterRequest(String shortCode, String keyword, String spId, String ncs, String appId, String expireDate) {
        this.shortCode = shortCode;
        this.keyword = keyword;
        this.spId = spId;
        this.ncsType = ncs;
        this.expireDate = expireDate;
    }

    public RoutingKeyRegisterRequest() {
    }

    public String getShortCode() {
        return shortCode;
    }

    public void setShortCode(String shortCode) {
        this.shortCode = shortCode;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getSpId() {
        return spId;
    }

    public void setSpId(String spId) {
        this.spId = spId;
    }

    public String getNcsType() {
        return ncsType;
    }

    public void setNcsType(String ncsType) {
        this.ncsType = ncsType;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public boolean isPerformRkCreationCharging() {
		return performRkCreationCharging;
	}

	public void setPerformRkCreationCharging(boolean performRkCreationCharging) {
		this.performRkCreationCharging = performRkCreationCharging;
	}

	public long getCorrelationId() {
		return correlationId;
	}

	public void setCorrelationId(long correlationId) {
		this.correlationId = correlationId;
	}

	public String getChargingCycleSize() {
		return chargingCycleSize;
	}

	public void setChargingCycleSize(String chargingCycleSize) {
		this.chargingCycleSize = chargingCycleSize;
	}

	@Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("RoutingKeyRegisterRequest [correlationId=");
        builder.append(correlationId);
        builder.append(", shortCode=");
        builder.append(shortCode);
        builder.append(", keyword=");
        builder.append(keyword);
        builder.append(", spId=");
        builder.append(spId);
        builder.append(", ncsType=");
        builder.append(ncsType);
        builder.append(", chargingCycleSize=");
        builder.append(chargingCycleSize);
        builder.append(", performRkCreationCharging=");
        builder.append(performRkCreationCharging);
        builder.append("]");
        return builder.toString();
    }
}
