/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hsenidmobile.orca.core.report;

import java.math.BigInteger;

/**
 * $LastChangedDate:$
 * $LastChangedBy:$
 * $LastChangedRevision:$
 */
public class MessageHistoryRecord {

    private String messageContent;
    private String messageSendingStatus;
    private long totalSubscriberCount;
    private long successMsgCount;
    private long failedMsgCount;
    private BigInteger totalMsgCount;
    private String serviceType;

    public MessageHistoryRecord() {
    }

    public MessageHistoryRecord(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getMessageContent() {
        return messageContent;
    }

    public void setMessageContent(String messageContent) {
        this.messageContent = messageContent;
    }

    public String getMessageSendingStatus() {
        return messageSendingStatus;
    }

    public void setMessageSendingStatus(String messageSendingStatus) {
        this.messageSendingStatus = messageSendingStatus;
    }

    public long getTotalSubscriberCount() {
        return totalSubscriberCount;
    }

    public void setTotalSubscriberCount(long totalSubscriberCount) {
        this.totalSubscriberCount = totalSubscriberCount;
    }

    public long getSuccessMsgCount() {
        return successMsgCount;
    }

    public void setSuccessMsgCount(long successMsgCount) {
        this.successMsgCount = successMsgCount;
    }

    public long getFailedMsgCount() {
        return failedMsgCount;
    }

    public void setFailedMsgCount(long failedMsgCount) {
        this.failedMsgCount = failedMsgCount;
    }

    public BigInteger getTotalMsgCount() {
        return totalMsgCount;
    }

    public void setTotalMsgCount(BigInteger totalMsgCount) {
        this.totalMsgCount = totalMsgCount;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(75);
        sb.append("MessageHistoryRecord");
        sb.append("{serviceType=[").append(serviceType).append(']');
        sb.append("{messageContent=[").append(messageContent).append(']');
        sb.append(", messageSendingStatus=[").append(messageSendingStatus).append(']');
        sb.append(", totalSubscriberCount=[").append(totalSubscriberCount).append(']');
        sb.append(", successMsgCount=[").append(successMsgCount).append(']');
        sb.append(", failedMsgCount=[").append(failedMsgCount).append(']');
        sb.append(", totalMsgCount=[").append(totalMsgCount).append(']');
        sb.append('}');
        return sb.toString();
    }
}