/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.repository;

import hsenidmobile.orca.core.applications.ContentDispatchSummary;
import hsenidmobile.orca.core.applications.alert.AlertContentRelease;
import hsenidmobile.orca.core.applications.exception.ApplicationException;

import java.util.List;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
public interface AlertDispatchedContentRepository {

    /**
     * Save dispatched content to database.
     *
     * @param contentRelease
     */
    void addDispatchedContent(AlertContentRelease contentRelease);

    /**
     * Get all the dispatched content for a given alert application.
     *
     * @param appId
     * @return
     */
    List<AlertContentRelease> getDispatchedContent(String appId);


    /**
     * Get last sent alert message for a  given application
     * @param appId
     * @return
     */
    ContentDispatchSummary getLastSentAlertByApplicationId(String appId) throws ApplicationException;

    /**
     * Get last sent alert message for a  given application and given subcategory
     * @param appId
     * @return
     */
    ContentDispatchSummary getLastSentAlertBySubCategoryId(String appId, long subCategoryId) throws ApplicationException;



}
