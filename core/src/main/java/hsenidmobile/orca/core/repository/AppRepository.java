/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.repository;

import hsenidmobile.orca.core.applications.Service;
import hsenidmobile.orca.core.applications.ApplicationImpl;
import hsenidmobile.orca.core.applications.exception.ApplicationException;
import hsenidmobile.orca.core.model.Application;
import hsenidmobile.orca.core.model.ReceivedMoSummary;

import java.util.List;

public interface AppRepository {

    public void createApplication(Application application);

    void updateApplication(Application application);

    void updateService(Service service);

    /**
     * Get a list of Active Subscription Applications.
     * @return
     */
    List<Application> findSubscriptionApplications();

    Application findAppByAppId(String appId) throws ApplicationException;

    Application findAppById(Long id) throws ApplicationException;

    List<Application> findAppByAppNameLike(String appName) throws ApplicationException;

    boolean isAppNameExists(String appName);

    /**
     * Update mo summary table
     * @param appId
     */
    void updateMoSummary(String appId);

    /**
     * Get mo summary for given application.
     * @param appId
     * @return
     */
    ReceivedMoSummary getMoSummary(String appId);

    /**
     * Update number of abuse reports received.
     * @param appId
     */
    void updateAbuseReportSummary(String appId);

    List<ApplicationImpl> findCpAppBySpid(String spId);

    List<Application> findExpiredAppName()throws ApplicationException;

    String findAppNameByAppId(String appId);

    /**
     * Find the content providers id which related to the given application.
     * @param appId = application id. 
     * @return = The content providers id.
     */
    String findCPId(String appId);

    String findSdpAppState(String orcaAppId);

    List<ApplicationImpl> findCpMangedAppByAppName(String appName, String spId);

}