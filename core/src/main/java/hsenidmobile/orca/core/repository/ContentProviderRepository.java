/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hsenidmobile.orca.core.repository;

import hsenidmobile.orca.core.model.ContentProvider;

/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public interface ContentProviderRepository {

	void save(ContentProvider contentProvider);

	void update(ContentProvider contentProvider);

	ContentProvider findByName(String name);

	ContentProvider findByUsername(String name);

    ContentProvider findByCpId(String cpId);

    void updateSpStatus(ContentProvider contentProvider);

    boolean isCpUserAllowedToManageRequestedApp(String userName, String appId);

    ContentProvider getContentProvider();
}

