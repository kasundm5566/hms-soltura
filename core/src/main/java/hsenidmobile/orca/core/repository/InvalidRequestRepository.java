/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.repository;


/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public interface InvalidRequestRepository {

	/**
	 * Increment the invalid request summary
	 * @param appId
	 */
	void incrementInvalidRequestSummary(String appId);

	/**
	 * Get total of invalid requests received for the application.
	 * @param appId
	 * @return
	 */
	int getInvalidRequestSummary(String appId);
}
