/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.repository;

import hsenidmobile.orca.core.model.DispatchSummary;

import java.util.List;

/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public interface MessageDispatchStatRepository {

	/**
	 * Increment the summary count
	 * @param appId
	 * @param statusCode
	 */
	void updateDispatchSummary(String appId, String statusCode);

	/**
	 * Get the summary of dispatchs
	 * @param appId
	 * @return
	 */
	List<DispatchSummary> getDispatchSummary(String appId);
}
