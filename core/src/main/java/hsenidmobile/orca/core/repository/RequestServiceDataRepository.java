/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hsenidmobile.orca.core.repository;

import hsenidmobile.orca.core.applications.exception.RequestServiceException;
import hsenidmobile.orca.core.applications.request.RequestService;
import hsenidmobile.orca.core.applications.request.RequestServiceData;

import java.util.List;

/**
 * $LastChangedDate$
 * $LastChangedBy$ $LastChangedRevision$
 *
 */
public interface RequestServiceDataRepository {

    /**
     * Adds request service data to the repository
     *
     * @param requestServiceData
     */
    public void add(RequestServiceData requestServiceData) throws RequestServiceException;

    /**
     * Finds request service message for a given message Id
     *
     * @param messageId
     * @throws RequestServiceException
     */
    public RequestServiceData findRequestServiceData(long messageId) throws RequestServiceException;

    /**
     * Updates request service mesage for a given message id
     *
     * @throws RequestServiceException
     */
    public void updateRequestServiceData(RequestServiceData requestServiceData) throws RequestServiceException;

    /**
     * Returns request service data for a given appid and requestServiceId
     *
     * @param appId
     * @param selectedSubCategoryArray
     * @return
     * @throws RequestServiceException
     */
    public List<RequestServiceData> getAllRequestMessagesBySubCategory(String appId, String[] selectedSubCategoryArray)
            throws RequestServiceException;

    /**
     * Returns request service data for a given appid and requestServiceId
     *
     * @param appId
     * @param requestServiceId
     * @return
     * @throws RequestServiceException
     */
    public List<RequestServiceData> getAllRequestMessagesForService(String appId, long requestServiceId)
            throws RequestServiceException;

    /**
     * Returns request service data for a given appid and requestServiceId
     *
     * @param appId
     * @param requestServiceId
     * @return
     * @throws RequestServiceException
     */
    public List<RequestServiceData> getRequestMessages(String appId, RequestService requestServiceId, int start, int end)
            throws RequestServiceException;

    /**
     * Returns request service data for a given appid and subcategory id
     *
     * @param appId
     * @param requestsubCategoryId
     * @return
     * @throws RequestServiceException
     */
    public List<RequestServiceData> getRequestMessagesBySubCategory(String appId, long requestsubCategoryId, int start,
                                                                    int end) throws RequestServiceException;

    /**
     * Return total number of messages received for given application.
     *
     * @param appId
     * @return
     * @throws RequestServiceException
     */
    public int getTotalMessageCount(String appId) throws RequestServiceException;

    void rollBackReceivedRequestMessage(long messageId) throws RequestServiceException;

    void updateIsReplied(long messageId) throws RequestServiceException;

}
