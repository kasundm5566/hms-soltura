/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hsenidmobile.orca.core.repository;

import hsenidmobile.orca.core.model.RevenueSummary;

import java.util.Date;
import java.util.List;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public interface RevenueReportRepository {

    public List<RevenueSummary> getMatchingRevenueSummaries(List<String> appIds, Date from, Date to,
                                                            boolean filterByDuartion);

    public List<String> getAllApplicationNames();
}
