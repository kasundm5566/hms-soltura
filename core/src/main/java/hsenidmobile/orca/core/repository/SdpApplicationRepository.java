/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited. 
 *   All Rights Reserved. 
 * 
 *   These materials are unpublished, proprietary, confidential source code of 
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET 
 *   of hSenid Software International (Pvt) Limited. 
 * 
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual 
 *   property rights in these materials. 
 *
 */
package hsenidmobile.orca.core.repository;

import hsenidmobile.orca.core.model.SdpAppLoginData;
import hsenidmobile.orca.core.model.OrcaSdpAppIntegration;
import hsenidmobile.orca.core.applications.exception.ApplicationException;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */

public interface SdpApplicationRepository {

    void addOrcaSdpAppIntegrationData(OrcaSdpAppIntegration orcaSdpAppIntegration);

    SdpAppLoginData getSdpAppLoginDataForOrcaApp(String orcaAppId) throws ApplicationException;

    String getOrcaAppId(String sdpAppId) throws ApplicationException;

    String getOrcaAppName(String sdpAppId) throws ApplicationException;

    public OrcaSdpAppIntegration getIntegrationDetails(String sdpAppId) throws ApplicationException;

}
