package hsenidmobile.orca.core.repository;

import java.util.List;
import java.util.Map;

import hsenidmobile.orca.core.applications.voting.Vote;
import hsenidmobile.orca.core.applications.voting.VoteResultSummary;
import hsenidmobile.orca.core.model.Msisdn;

/**
 * $LastChangedDate: 2009-10-13 10:15:52 +0530 (Tue, 13 Oct 2009) $
 * $LastChangedBy: romith $
 * $LastChangedRevision: 52686 $
 */
public interface VoteRepository {

    public long add(Vote vote);

    /**
     * Update Voting summary table
     * @param voteletId
     * @param vote
     */
	void updateSummary(long voteletId, Vote vote);

	/**
	 * Retrieves summary of particular voting contest.
	 * @param voteletId
	 * @return
	 */
	 List<VoteResultSummary> getVotingSummary(long voteletId);

	 /**
	  * Check where given msisdn has voted for given voting contest
	  * @param msisdn
	  * @param voteletId
	  * @return
	  */
	 boolean hasVoted(Msisdn msisdn, long voteletId);

    /**
     *
     * @param voteId
     */
     void rollBackReceivedVote(long voteId, long voteServiceId);
}
