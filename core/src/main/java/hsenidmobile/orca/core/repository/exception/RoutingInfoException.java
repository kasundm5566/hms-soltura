/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hsenidmobile.orca.core.repository.exception;

import java.text.MessageFormat;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */


public class RoutingInfoException extends Exception{

    public static final String ROUTING_INFO_NOT_FOUND_FOR_ID = "Routing information not found for ID: id[{0}]";
    public static final String ROUTING_INFO_NOT_FOUND_FOR_APP_ID = "Routing information not found for appId: appId[{0}]";


    public RoutingInfoException(String message, Object... stringArgs) {
        super(MessageFormat.format(message, stringArgs));
    }
}
