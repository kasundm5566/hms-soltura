/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$ 
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.services;

import hsenidmobile.orca.core.model.Application;
import hsenidmobile.orca.core.model.Msisdn;
import hsenidmobile.orca.core.model.OrcaException;
import hsenidmobile.orca.core.model.Language;
import hsenidmobile.orca.core.model.message.Message;
import hsenidmobile.orca.core.model.message.MessageContext;
import hsenidmobile.orca.core.model.message.MoResponse;
import hsenidmobile.orca.core.model.message.RegistrationResponse;

import java.util.Locale;

/**
 * Facade which will encasulate the transaction handling and other house keeping
 * stuff when request is given to be processed to the applicaiton.
 * 
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public interface ApplicationService {

	public MoResponse onDataMessage(Application application, Message message,
			MessageContext messageContext) throws OrcaException;

	public MoResponse onApplicationDataUpdateMessage(Application application,
			Message message, MessageContext messageContext);

	public MoResponse onReportAbuse(Application application, Message message,
			MessageContext messageContext);

	public RegistrationResponse onSubscription(Application application, Msisdn sender,
			String dataText, Locale languge);

	public MoResponse onUnSubscription(Application application, Msisdn sender,
			String dataText);

}