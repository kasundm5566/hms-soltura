package hsenidmobile.orca.core.services;

import hsenidmobile.orca.core.model.Application;
import hsenidmobile.orca.core.model.Msisdn;

/**
 * Check whether the given msisdn is belong to a author.
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public interface AuthorValidationService {

    boolean isValidAuthor(Application app, Msisdn senderAddress);

}
