package hsenidmobile.orca.core.services;

import hsenidmobile.orca.core.model.InvalidRequest;

public interface InvalidRequestService {

	/**
	 * Save Invalid request to the database and increment count in summary table.
	 */
	void invalidRequestReceived(InvalidRequest invalidRequest);

	/**
	 * Get total number of invalid request received for a given application.
	 * @param appId
	 * @return
	 */
	int getInvalidRequestSummary(String appId);

}