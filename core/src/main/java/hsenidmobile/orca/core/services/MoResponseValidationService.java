package hsenidmobile.orca.core.services;

/**
 * $LastChangedDate:$
 * $LastChangedBy:$
 * $LastChangedRevision:$
 */
public interface MoResponseValidationService {

    /**
     * Return true if the Mo can be charged when teh Mt charging is applied
     * @param responseCode
     * @return
     */
    boolean isMoCanBeChargedUsingMtCharging(String responseCode);
}
