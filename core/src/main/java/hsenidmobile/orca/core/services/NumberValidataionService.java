/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$ 
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.services;


/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public interface NumberValidataionService {

	/**
	 * Find the operator of the given MSISDN.
	 * @param msisdn
	 * @return
	 */
	String findOperation(String msisdn);
}
