/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.services;

import hsenidmobile.orca.core.model.Application;
import hsenidmobile.orca.core.model.ContentProvider;
import hsenidmobile.orca.core.model.RoutingInfo;
import hsenidmobile.orca.core.repository.AppRepository;
import hsenidmobile.orca.core.repository.ContentProviderRepository;
import hsenidmobile.orca.core.services.impl.RoutingKeyException;

/**
 * Services related Routing-Key registation
 *
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public interface RoutingKeyManager {

    /**
     * Check whether a requested routing key is available for the given CP to
     * create. Since service-keyword can have space in between special set of
     * rules apply when selecting it.
     *
     * When user want to create a service-keyword with spaces in the middle, we
     * have to make sure that first portion of the keyword(portion before space)
     * is also owned by same CP. e.g. If user want to have service-keyword 'stv
     * sss' he must own the keyword 'stv' as well. If he is not the owner of
     * keyword 'stv' we should not allow him to create service-keyword 'stv sss'
     *
     * Same CP can have different application running on service-keyword as
     * follows; stv, stv sss, stv rrg, stv ds
     *
     * @param routingInfo
     *            -
     * @param cp
     *            -
     * @return
     * @throws RoutingKeyException
     *             -
     */
    boolean isRoutingKeyAvailableToCreate(RoutingInfo routingInfo, ContentProvider cp) throws RoutingKeyException;

    /**
     * Check whether a given sub-keyword can be used by the given application.
     * This testing is required since we allow spaces inside service-keyword.
     *
     * If there is a application with service-keyword 'stv sss' and another
     * application with 'stv', then that second application cannot create a new
     * keyword 'sss'.
     *
     * @param subKey
     * @param application
     * @param cp
     * @return
     */
    boolean isSubKeywordAvailableToCreate(String subKey, Application application, ContentProvider cp);

    /**
     * Assign the routing key to the given application.
     *
     * @param routingInfo
     *            -
     * @param application
     *            -
     * @param cp
     *            -
     * @throws RoutingKeyException
     *             -
     */
    Application assignRoutingKey(RoutingInfo routingInfo, Application application, ContentProvider cp)
            throws RoutingKeyException;

    /**
     *
     * @param contentProviderRepository
     *            -
     */

    /**
     *
     * @param appRepository
     *            -
     */
    void setAppRepository(AppRepository appRepository);

    /**
     * @param rkAggregatorRepository
     *            -
     */
}
