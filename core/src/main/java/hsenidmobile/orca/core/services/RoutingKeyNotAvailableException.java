/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.services;

import hsenidmobile.orca.core.model.OrcaException;

/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class RoutingKeyNotAvailableException extends OrcaException {

	/**
	 * @param message
	 * @param cause
	 */
	public RoutingKeyNotAvailableException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 */
	public RoutingKeyNotAvailableException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public RoutingKeyNotAvailableException(Throwable cause) {
		super(cause);
	}

}
