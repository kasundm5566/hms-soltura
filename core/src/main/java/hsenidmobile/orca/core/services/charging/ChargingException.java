/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.services.charging;

import hsenidmobile.orca.core.model.OrcaException;

/**
 * @version $LastChangedRevision$
 */
// TODO: Remove this class since we no longer do charging related stuff in Orca
@Deprecated
public class ChargingException extends OrcaException {

	/**
	 * @param message
	 * @param cause
	 */
	public ChargingException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 */
	public ChargingException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public ChargingException(Throwable cause) {
		super(cause);
	}

}
