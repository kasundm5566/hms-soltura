package hsenidmobile.orca.core.services.impl;

import hsenidmobile.orca.core.model.Application;
import hsenidmobile.orca.core.model.Author;
import hsenidmobile.orca.core.model.Msisdn;
import hsenidmobile.orca.core.services.AuthorValidationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class AuthorValidationServiceImpl implements AuthorValidationService {

    private static final Logger logger = LoggerFactory.getLogger(AuthorValidationServiceImpl.class);

    @Override
    public boolean isValidAuthor(Application app, Msisdn senderAddress) {
        logger.debug("Perform author validation for Msisdn[]", senderAddress);
        if (app.getAuthors() != null) {
            for (Author author : app.getAuthors()) {
                logger.debug("Comparing with configured author[]", author);
                if (senderAddress.equals(author.getMsisdn())) {
                    logger.info("Received Msisdn[{}] is a valid Author.", senderAddress);
                    return true;
                }
            }
        }

        logger.info("Received Msisdn[{}] is NOT a valid Author.", senderAddress);
        return false;
    }
}
