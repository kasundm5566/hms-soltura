/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.services.impl;

import hsenidmobile.orca.core.model.InvalidRequest;
import hsenidmobile.orca.core.repository.InvalidRequestRepository;
import hsenidmobile.orca.core.services.InvalidRequestService;
import hsenidmobile.orca.core.util.Util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 *
 * @version $LastChangedRevision$
 */
public class InvalidRequestServiceImpl implements InvalidRequestService {
	private InvalidRequestRepository invalidRequestRepository;

	private static final Logger logger = LoggerFactory.getLogger(InvalidRequestServiceImpl.class);
	private static final Logger invalidReqTransLogger = LoggerFactory.getLogger("Invalid.Request.Log");

	public void invalidRequestReceived(InvalidRequest invalidRequest) {
		logger.debug("Saving invalid request details to the database[{}]", invalidRequest);

		invalidReqTransLogger.info(invalidRequest.getTransLog());
		if (!Util.isEmpty(invalidRequest.getAppId())) {
			invalidRequestRepository.incrementInvalidRequestSummary(invalidRequest.getAppId());
		}
	}

	public void setInvalidRequestRepository(InvalidRequestRepository invalidRequestRepository) {
		this.invalidRequestRepository = invalidRequestRepository;
	}

	@Override
	public int getInvalidRequestSummary(String appId) {
		return invalidRequestRepository.getInvalidRequestSummary(appId);
	}

}
