/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.services.impl;

/**
 * Detail of Subscription done to a particular application by a Msisdn.
 *
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class MsisdnSubscriptionDetail {
	private String appName;
	private String appId;
	private String appServiceKeyword;
	private String appShortCode;
	private String appShortCodeOperator;
	private String subKeyword;

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getAppServiceKeyword() {
		return appServiceKeyword;
	}

	public void setAppServiceKeyword(String appServiceKeyword) {
		this.appServiceKeyword = appServiceKeyword;
	}

	public String getAppShortCode() {
		return appShortCode;
	}

	public void setAppShortCode(String appShortCode) {
		this.appShortCode = appShortCode;
	}

	public String getAppShortCodeOperator() {
		return appShortCodeOperator;
	}

	public void setAppShortCodeOperator(String appShortCodeOperator) {
		this.appShortCodeOperator = appShortCodeOperator;
	}

	public String getSubKeyword() {
		return subKeyword;
	}

	public void setSubKeyword(String subKeyword) {
		this.subKeyword = subKeyword;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(250);
		builder.append("MsisdnSubscriptionDetail [appId=");
		builder.append(appId);
		builder.append(", appName=");
		builder.append(appName);
		builder.append(", appServiceKeyword=");
		builder.append(appServiceKeyword);
		builder.append(", appShortCode=");
		builder.append(appShortCode);
		builder.append(", appShortCodeOperator=");
		builder.append(appShortCodeOperator);
		builder.append(", subKeyword=");
		builder.append(subKeyword);
		builder.append("]");
		return builder.toString();
	}

}
