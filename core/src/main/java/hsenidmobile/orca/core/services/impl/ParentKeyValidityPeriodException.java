/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.services.impl;

/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class ParentKeyValidityPeriodException extends RoutingKeyException {

	/**
	 * @param message
	 * @param cause
	 */
	public ParentKeyValidityPeriodException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 */
	public ParentKeyValidityPeriodException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public ParentKeyValidityPeriodException(Throwable cause) {
		super(cause);
	}

}
