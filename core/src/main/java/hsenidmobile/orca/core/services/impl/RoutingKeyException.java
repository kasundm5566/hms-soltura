/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.services.impl;

import hsenidmobile.orca.core.model.OrcaException;

/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class RoutingKeyException extends OrcaException {

	public static final String RESERVED_KEYWORD = "Cannot use Reserved Keyword";

	public RoutingKeyException(String message, Throwable cause) {
		super(message, cause);
	}

	public RoutingKeyException(String message) {
		super(message);
	}

	public RoutingKeyException(Throwable cause) {
		super(cause);
	}
}
