/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.services.impl;

import hsenidmobile.orca.core.applications.exception.ApplicationException;
import hsenidmobile.orca.core.model.*;
import hsenidmobile.orca.core.repository.AppRepository;
import hsenidmobile.orca.core.services.RoutingKeyManager;
import hsenidmobile.orca.core.util.PropertyHolder;
import hsenidmobile.orca.core.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 *
 * @version $LastChangedRevision$
 */
public class RoutingKeyManagerImpl implements RoutingKeyManager {

	private static final Logger logger = LoggerFactory.getLogger(RoutingKeyManagerImpl.class);
	private AppRepository appRepository;

	@Override
	public boolean isRoutingKeyAvailableToCreate(RoutingInfo routingInfo, ContentProvider cp)
			throws RoutingKeyException {
		logger.debug("Checking the availability of RK[{}] for CP[{}]", routingInfo, cp);
         //todo integrate this with sdp rk validation api
//		RoutingKey routingKey = routingInfo.getRoutingKey();
//		if (routingKey == null || cp == null) {
//			throw new IllegalArgumentException("Cannot have NULL Parameters.");
//		}
//
//		if (Util.isEmpty(routingKey.getKeyword())) {
//			throw new RoutingKeyException("RoutingKey cannot be Empty");
//		}
//
//		boolean hasSplitters = formatKeyword(routingKey);
//
//		validateForReservedKeywords(routingKey);

//		if (hasSplitters) {
//			String fistPortion = routingKey.getKeyword().substring(0,
//					routingKey.getKeyword().lastIndexOf(PropertyHolder.MESSAGE_SPLITTER));
//			String lastPortion = routingKey.getKeyword().substring(
//					routingKey.getKeyword().lastIndexOf(PropertyHolder.MESSAGE_SPLITTER) + 1,
//					routingKey.getKeyword().length());
//			RoutingKey rkWithFirstPortionAsKey = new RoutingKey(routingKey.getShortCode(), fistPortion);
//
//            //integrate this with sdp routing key validation API
////			if (validateRkWithParentKey(routingInfo, rkWithFirstPortionAsKey, lastPortion, cp.getOwnedRoutingInfo())) {
////				return isFullKeywordAvailable(routingKey, cp.getOwnedRoutingInfo());
////			} else {
////				throw new RoutingKeyException("Keyword [" + fistPortion + "], required to create the new routing key["
////						+ routingKey + "] is NOT owned by this CP.");
////			}
//
//		} else {
//			return sdpRoutingKeyService.isSharedShortCodeAvailableToCreate (routingInfo.getRoutingKey().getShortCode()
//					.getAddress(), routingInfo.getRoutingKey().getKeyword());
//		}
        return true;
	}

	private void validateForReservedKeywords(RoutingKey routingKey) throws RoutingKeyException {
		// TODO: NCS type is hardcoded to SMS, remove it and put it to
		// routingKey
//		if (sdpRoutingKeyService.isReservedKeyword(routingKey.getShortCode().getOperator(), routingKey.getKeyword(),
//				PropertyHolder.SMSC_PD_NCS_ID)) {
//			throw new RoutingKeyException(RoutingKeyException.RESERVED_KEYWORD);
//		}
	}

	public boolean isReservedKeyword(String keyword, String operator) {
//		if (sdpRoutingKeyService.isReservedKeyword(operator, keyword, PropertyHolder.SMSC_PD_NCS_ID)) {
//			return true;
//		} else {
//			return false;
//		}
        return false;
	}

	/**
	 * Compare the start and end dates of 'rfForFirstPortionOfRk' with
	 * 'routingInfo'
	 *
	 * @param rfForFirstPortionOfRk
	 * @param routingInfo
	 * @return
	 */
	private boolean validateValidityPeriod(RoutingInfo rfForFirstPortionOfRk, RoutingInfo routingInfo) {
		logger.debug(
				"Duration Validation: Parent Start[{}] NewKey Start[{}], ParentEnd[{}] NewKey End[{}]",
				new Object[] { rfForFirstPortionOfRk.getStartDate(), routingInfo.getStartDate(),
						rfForFirstPortionOfRk.getEndDate(), routingInfo.getEndDate() });

		return (rfForFirstPortionOfRk.getStartDate() <= routingInfo.getStartDate() && rfForFirstPortionOfRk
				.getEndDate() >= routingInfo.getEndDate());
	}

	private boolean isFullKeywordAvailable(RoutingKey routingKey, List<RoutingInfo> ownedRoutingInfo) {
		for (RoutingInfo routingInfo : ownedRoutingInfo) {
			if (routingKey.equals(routingInfo.getRoutingKey())) {
				return false;
			}
		}
		return true;
	}

	private boolean validateRkWithParentKey(RoutingInfo givenRoutingInfo, RoutingKey fistPortionRk,
			String lastPortionOfRk, List<RoutingInfo> ownedRoutingInfo) throws RoutingKeyException {
		RoutingInfo parentRoutingInfo = getRoutingInfo(fistPortionRk, ownedRoutingInfo);
		logger.debug("Found routing info[{}] with first-portion keyword[{}]", parentRoutingInfo,
				fistPortionRk.getKeyword());

		if (parentRoutingInfo != null) {

			if (validateValidityPeriod(parentRoutingInfo, givenRoutingInfo)) {

				if (parentRoutingInfo.getStatus() == RoutingInfoStatus.ASSIGNED) {
					return validateWithParentKeyAppSubKeywords(fistPortionRk, lastPortionOfRk, parentRoutingInfo);
				}
			} else {
				throw new ParentKeyValidityPeriodException(
						"Duration of parent-key do not match with the duration of given routing information");
			}

		}
		return parentRoutingInfo != null && parentRoutingInfo.getStatus() != RoutingInfoStatus.RETIRED;

	}

	private boolean validateWithParentKeyAppSubKeywords(RoutingKey fistPortionRk, String lastPortionOfRk,
			RoutingInfo routingInfo) throws RoutingKeyException {
		Application app;
		try {
			app = appRepository.findAppByAppId(routingInfo.getOwningAppId());

			if (app != null) {
				List<SubCategory> supportedSubCategories = app.getService().getSupportedSubCategories();
				if (supportedSubCategories != null) {
					for (SubCategory subCategory : supportedSubCategories) {
						if (subCategory.isMatch(lastPortionOfRk)) {
							throw new RoutingKeyException("Keyword Last Part[" + lastPortionOfRk
									+ "] is a Subkeyword of the Application[" + app.getAppName()
									+ "] which owns the Parent Key[" + fistPortionRk.getKeyword() + "].");
						}
					}
				}

				return true;
			} else {
				logger.warn("No application found for OwningRK specified in RoutingInfo[{}]", routingInfo);
				return true;
			}
		} catch (ApplicationException e) {
			logger.warn("No application found for OwningRK specified in RoutingInfo[{}]", routingInfo);
			return true;
		}
	}

	private RoutingInfo getRoutingInfo(RoutingKey fistPortionRk, List<RoutingInfo> ownedRoutingInfo) {
		if (ownedRoutingInfo != null) {
			for (RoutingInfo routingInfo : ownedRoutingInfo) {
				if (fistPortionRk.equals(routingInfo.getRoutingKey())) {
					return routingInfo;
				}
			}
		}

		return null;
	}

	private boolean formatKeyword(RoutingKey routingKey) {
		logger.debug("Formatting Given Keyword[{}]", routingKey.getKeyword());
		boolean hasSplitters = false;
		String rkTxt = routingKey.getKeyword().toLowerCase().trim();
		String[] rkTxtSplitted = rkTxt.split(PropertyHolder.MESSAGE_SPLITTER);
		logger.debug("Splitted key[{}]", Arrays.toString(rkTxtSplitted));
		if (rkTxt.indexOf(PropertyHolder.MESSAGE_SPLITTER) > -1) {
			rkTxt = "";
			for (String string : rkTxtSplitted) {
				if (!Util.isEmpty(string)) {
					rkTxt += string.trim() + PropertyHolder.MESSAGE_SPLITTER;
				}
			}
			rkTxt = rkTxt.trim();
			hasSplitters = true;
		}
		routingKey.setKeyword(rkTxt);
		logger.debug("Was the keyword formated [{}]", hasSplitters);
		return hasSplitters;
	}

	@Override
	public Application assignRoutingKey(RoutingInfo routingInfo, Application application, ContentProvider cp)
			throws RoutingKeyException {

		validateForReservedKeywords(routingInfo.getRoutingKey());

		if (isRoutingInfoAvaliableToAssignToApp(routingInfo, cp)) {
			ApplicationRoutingInfo applicationRoutingInfo = application.getApplicationRoutingInfo();
			if (applicationRoutingInfo == null) {
				applicationRoutingInfo = new ApplicationRoutingInfo();
				application.setApplicationRoutingInfo(applicationRoutingInfo);
			}

			routingInfo.setOwningAppId(application.getAppId());
			routingInfo.setStatus(RoutingInfoStatus.ASSIGNED);
			applicationRoutingInfo.addRoutingInfo(routingInfo);
			application.setApplicationRoutingInfo(applicationRoutingInfo);
			logger.info("[RK-ASSIGN-SUCCESS] RoutingInfo[{}], CP[{}]", routingInfo, cp.getCpId());
			return application;
		} else {
			throw new RoutingKeyException("Rouitng key [" + routingInfo.getRoutingKey()
					+ "] already being assigned to another application");
		}

	}

	private boolean isRoutingInfoAvaliableToAssignToApp(RoutingInfo routingInfo, ContentProvider cp)
			throws RoutingKeyException {
//		if (isRoutingKeyActive(routingInfo)) {
//
//			if (isRoutingKeyOwnedByCp(routingInfo, cp)) {
//				return sdpRoutingKeyService.isSharedShortCodeAvailableToAssign(routingInfo.getRoutingKey()
//						.getShortCode().getAddress(), routingInfo.getRoutingKey().getKeyword());
//				// Application app =
//				// appRepository.findApplication(routingInfo.getRoutingKey().getShortCode(),
//				// routingInfo
//				// .getRoutingKey().getKeyword());
//				// return routingInfoInDb == null;
//			} else {
//				throw new RoutingKeyException("Rouitng key [" + routingInfo.getRoutingKey() + "] NOT owned by this CP["
//						+ cp.getName() + "]");
//			}
//		} else {
//			throw new RoutingKeyException("Rouitng key [" + routingInfo.getRoutingKey() + "] has expired.");
//		}
        return true;
	}

	private boolean isRoutingKeyActive(RoutingInfo routingInfo) {
		long currentTime = System.currentTimeMillis();
		return (currentTime > routingInfo.getStartDate()) && (currentTime < routingInfo.getEndDate());
	}

	private boolean isRoutingKeyOwnedByCp(RoutingInfo routingInfo, ContentProvider cp) {
//		List<RoutingInfo> ownedRoutingInfo = cp.getOwnedRoutingInfo();
//		if (ownedRoutingInfo == null) {
//			return false;
//		} else {
//			for (RoutingInfo rf : ownedRoutingInfo) {
//				if (rf.getRoutingKey().equals(routingInfo.getRoutingKey())) {
//					return true;
//				}
//			}
//		}

		return true;
	}


	@Override
	public boolean isSubKeywordAvailableToCreate(String subKey, Application application, ContentProvider cp) {
//		if (Util.isEmpty(subKey)) {
//			logger.debug("Given Subkey is empty.");
//			return true;
//		} else {
//			List<RoutingKey> possibleRksWithSubKey = new ArrayList<RoutingKey>();
//			List<RoutingKey> availableRoutingKeys = application.getApplicationRoutingInfo().getRoutingKeys();
//			for (RoutingKey routingKey : availableRoutingKeys) {
//				RoutingKey rkWithSubkey = new RoutingKey(routingKey.getShortCode(), routingKey.getKeyword()
//						+ PropertyHolder.MESSAGE_SPLITTER + subKey);
//				possibleRksWithSubKey.add(rkWithSubkey);
//				logger.debug("Found Application RK[{}] and Possible RK with Subkey is[{}]", routingKey, rkWithSubkey);
//			}
//
//			logger.debug("Start Checking whether Possible RK with Subkey already there as a RK.");
//			for (RoutingKey routingKey : possibleRksWithSubKey) {
//				logger.debug("Matching [{}]", routingKey);
//				if (getRoutingInfo(routingKey, cp.getOwnedRoutingInfo()) != null) {
//					logger.info("Found RoutingKey similar to ApplicationServiceKeyword with Subkeyword[{}]",
//							routingKey.getKeyword());
//					return false;
//				}
//			}
//			return true;
//		}
        //todo integrate with sdp rk validation API
        return true;
	}

	public void setAppRepository(AppRepository appRepository) {
		this.appRepository = appRepository;
	}
}
