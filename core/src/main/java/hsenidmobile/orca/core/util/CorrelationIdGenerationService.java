/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.util;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class CorrelationIdGenerationService {
	private static int txIdIncrmentator = 0;
	private static final String DATE_FORMAT_CORRELATION_ID = "yyMMddHHmm";
	private static final String DECIMAL_FORMAT_CORRELATION_ID = "0000";

	/**
	 * Creates a long value prefixed by serverId. Value is unique within a
	 * single jvm.
	 *
	 * @param layerId
	 * @return
	 */
	public static long getCorrelationId(int serverId) {
		int localIndex;
		synchronized (CorrelationIdGenerationService.class) {
			if (txIdIncrmentator >= 9999) {
				txIdIncrmentator = 0;
			}
			localIndex = ++txIdIncrmentator;
		}
		SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT_CORRELATION_ID);
		Date date = new Date();
		StringBuilder sb = new StringBuilder(50);
		sb.append(serverId);
		sb.append(formatter.format(date));

		DecimalFormat df = new DecimalFormat(DECIMAL_FORMAT_CORRELATION_ID);
		sb.append(df.format(localIndex));

		return Long.parseLong(sb.toString());
	}

}
