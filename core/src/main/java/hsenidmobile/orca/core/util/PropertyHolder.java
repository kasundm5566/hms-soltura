/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.util;

import java.util.Collections;
import java.util.List;

/**
 * Holds system wide configuration properties.
 *
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class PropertyHolder {

	public static int SERVER_ID;
	public static String MESSAGE_SPLITTER = " ";

	public static String REGISTER_COMMAND = "reg";
	public static String UNREGISTER_COMMAND = "unreg";
	public static String UPDATE_COMMAND = "update";
	public static String REPORT_ABUSE_COMMAND = "abuse";
	public static String SHOW_ALL_REGISTRATIONS_COMMAND = "showallreg";
	public static String VERIFY_MSISDN_COMMAND = "verify";
	public static String UNREGISTER_ALL_COMMAND = "unregall";
	public static String RESULT_SYSTEM_COMMAND = "result";

	public static String ALREADY_REGISTERED_ERROR_MESSAGE = "You are already registered with %s";
	public static String NOT_REGISTERED_TO_UNSUBSCRIBE_ERROR_MESSAGE = "You are NOT registered with %s";
	public static String NO_REG_REQUIRED_ERROR_MESSAGE = "No registration required to use application '%s'";
	public static String ALERT_SERVICE_DATA_UPDATE_SUCCESS_MSG = "Service Data Updated Successfully.";
	public static String SUBSCRIPTION_SERVICE_DATA_UPDATE_SUCCESS_MSG = "Service Data Updated Successfully and scheduled to be dispatched on %s.";
	public static String NOT_REGISTERED_SUBSCRIBER_ERROR_MESSAGE = "You need to subscribe with '%s' before using it.";
	public static String ONLY_VOTERS_CAN_GET_RESULT_ERROR_MESSAGE = "Only voters are allowed to get results.";
	public static String UNSUPPORTED_KEWORD_ERROR_MESSAGE = "Unsupported keyword '%s'";
	public static String APP_NOT_ACTIVE_ERROR_MSG = "Application[%s] is not active";
	public static String NO_APP_FOUND_ERROR_MSG = "No application found for[%s]";

	public static List<String> SUPPORTED_LANGUAGE_CODES = Collections.EMPTY_LIST;
	public static List<String> SUPPORTED_PERIODICITY_CODES = Collections.EMPTY_LIST;

	public static String USER_REG_FAIL_DUE_INTERNAL_ERROR_MSG = "Error occued during registration, please try again later.";
	public static String ONE_VOTE_PER_NUMBER_ERROR_MSG = "You have already voted for this application";

	public static String MSISDN_VERIFICATION_SUCCESS_MSG = "Msisdn Verification Successfull!. Now you can start creating applications with Soltura.";
	public static String ALREADY_VERIFIED_MSISDN_ERROR_MSG = "Your MSISDN is already verifiedl!. Now you can start creating applications with Soltura.";
	public static String MSISDN_VERIFICATION_FAIL_MSG = "Msisdn verification Failed. No User registration data found.";
	public static String MSISDN_VERIFICATION_FAIL_CP_INACTIVE_MSG = "Msisdn verification Failed. Content Provider is in Inactive State";

	public static String APP_REPORT_ABUSE_ACCEPTANCE_MSG = "Thanks for reporting your concerns on Application %s. You have being UnSubscribed from the Application and actions will be taken on this.";

	public static final String SMSC_PD_NCS_ID = "smsc-pd";

	public static String VOTING_RESULT_QUERY_RESPONSE_TEMPLATE = "Results of %s - %s";
	public static int MAX_RESULT_SUMMARY_LENGTH = 120;

	public int getMAX_RESULT_SUMMARY_LENGTH() {
		return MAX_RESULT_SUMMARY_LENGTH;
	}

	public void setMAX_RESULT_SUMMARY_LENGTH(int mAX_RESULT_SUMMARY_LENGTH) {
		MAX_RESULT_SUMMARY_LENGTH = mAX_RESULT_SUMMARY_LENGTH;
	}

	public String getNOT_REGISTERED_SUBSCRIBER_ERROR_MESSAGE() {
		return NOT_REGISTERED_SUBSCRIBER_ERROR_MESSAGE;
	}

	public void setNOT_REGISTERED_SUBSCRIBER_ERROR_MESSAGE(String nOTREGISTEREDSUBSCRIBERERRORMESSAGE) {
		NOT_REGISTERED_SUBSCRIBER_ERROR_MESSAGE = nOTREGISTEREDSUBSCRIBERERRORMESSAGE;
	}

	public void setMESSAGE_SPLITTER(String mESSAGESPLITTER) {
		MESSAGE_SPLITTER = mESSAGESPLITTER;
	}

	public void setREGISTER_COMMAND(String rEGISTERCOMMAND) {
		REGISTER_COMMAND = rEGISTERCOMMAND;
	}

	public void setUNREGISTER_COMMAND(String uNREGISTERCOMMAND) {
		UNREGISTER_COMMAND = uNREGISTERCOMMAND;
	}

	public void setUPDATE_COMMAND(String uPDATECOMMAND) {
		UPDATE_COMMAND = uPDATECOMMAND;
	}

	public void setREPORT_ABUSE_COMMAND(String rEPORTABUSECOMMAND) {
		REPORT_ABUSE_COMMAND = rEPORTABUSECOMMAND;
	}

	public void setSHOW_ALL_REGISTRATIONS_COMMAND(String sHOWALLREGISTRATIONSCOMMAND) {
		SHOW_ALL_REGISTRATIONS_COMMAND = sHOWALLREGISTRATIONSCOMMAND;
	}

	public void setUNREGISTER_ALL_COMMAND(String uNREGISTERALLCOMMAND) {
		UNREGISTER_ALL_COMMAND = uNREGISTERALLCOMMAND;
	}

	public void setALREADY_REGISTERED_ERROR_MESSAGE(String aLREADYREGISTEREDERRORMESSAGE) {
		ALREADY_REGISTERED_ERROR_MESSAGE = aLREADYREGISTEREDERRORMESSAGE;
	}

	public void setNOT_REGISTERED_TO_UNSUBSCRIBE_ERROR_MESSAGE(String nOTREGISTEREDTOUNSUBSCRIBEERRORMESSAGE) {
		NOT_REGISTERED_TO_UNSUBSCRIBE_ERROR_MESSAGE = nOTREGISTEREDTOUNSUBSCRIBEERRORMESSAGE;
	}

	public void setNO_REG_REQUIRED_ERROR_MESSAGE(String nOREGREQUIREDERRORMESSAGE) {
		NO_REG_REQUIRED_ERROR_MESSAGE = nOREGREQUIREDERRORMESSAGE;
	}

	public void setSUPPORTED_LANGUAGE_CODES(List<String> sUPPORTEDLANGUAGECODES) {
		SUPPORTED_LANGUAGE_CODES = sUPPORTEDLANGUAGECODES;
	}

	public void setSUPPORTED_PERIODICITY_CODES(List<String> sUPPORTEDPERIODICITYCODES) {
		SUPPORTED_PERIODICITY_CODES = sUPPORTEDPERIODICITYCODES;
	}

	public void setUSER_REG_FAIL_DUE_INTERNAL_ERROR_MSG(String uSERREGFAILDUEINTERNALERRORMSG) {
		USER_REG_FAIL_DUE_INTERNAL_ERROR_MSG = uSERREGFAILDUEINTERNALERRORMSG;
	}

	public void setSERVER_ID(int sERVERID) {
		SERVER_ID = sERVERID;
	}

	public String getVOTING_RESULT_QUERY_RESPONSE_TEMPLATE() {
		return VOTING_RESULT_QUERY_RESPONSE_TEMPLATE;
	}

	public void setVOTING_RESULT_QUERY_RESPONSE_TEMPLATE(String vOTING_RESULT_QUERY_RESPONSE_TEMPLATE) {
		VOTING_RESULT_QUERY_RESPONSE_TEMPLATE = vOTING_RESULT_QUERY_RESPONSE_TEMPLATE;
	}

    public void setALERT_SERVICE_DATA_UPDATE_SUCCESS_MSG(String aLERT_SERVICE_DATA_UPDATE_SUCCESS_MSG) {
        ALERT_SERVICE_DATA_UPDATE_SUCCESS_MSG = aLERT_SERVICE_DATA_UPDATE_SUCCESS_MSG;
    }

    public void setSUBSCRIPTION_SERVICE_DATA_UPDATE_SUCCESS_MSG(String sUBSCRIPTION_SERVICE_DATA_UPDATE_SUCCESS_MSG) {
        SUBSCRIPTION_SERVICE_DATA_UPDATE_SUCCESS_MSG = sUBSCRIPTION_SERVICE_DATA_UPDATE_SUCCESS_MSG;
    }

    public void setNO_APP_FOUND_ERROR_MSG(String nO_APP_FOUND_ERROR_MSG) {
        NO_APP_FOUND_ERROR_MSG = nO_APP_FOUND_ERROR_MSG;
    }


}
