/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.util;

import java.net.ConnectException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
@Aspect
public abstract class SnmpLoggerAdvice {
	private static final Logger logger = LoggerFactory.getLogger("snmp");
	private static final String DB_FAIL = "SOLTURA_CORE - Database connection failed";
	private static final String CONNECTION_FAIL = "SOLTURA_CORE - SDP connection failed";

	private static final List<String> sqlErrors;
    private static final List<String> connectionErrors;

    static {
        sqlErrors = new ArrayList<String>();
        sqlErrors.add("Connections could not be acquired");
        sqlErrors.add("Could not get JDBC Connection");
        sqlErrors.add("Connection is invalid");
        sqlErrors.add("Closed Connection");
        sqlErrors.add("No more data to read from socket");
        sqlErrors.add("Broken pipe");
        sqlErrors.add("Unknown database");
        sqlErrors.add("Communications link failure");

        connectionErrors = new ArrayList<String>();
        connectionErrors.add("Connection refused");

    }

	@Pointcut
    abstract void dbFailurePointCut();

	@Pointcut
    abstract void connectionFailurePointCut();

	@AfterThrowing(pointcut = "dbFailurePointCut()", throwing = "e")
	public void logDbException(JoinPoint thisJoinPoint, Throwable e) throws Throwable {
		if (isExceptionIsDBFailure(e)) {
			logger.error(DB_FAIL);
		}
	}

	@AfterThrowing(pointcut = "connectionFailurePointCut()", throwing = "e")
	public void logConnectionException(JoinPoint thisJoinPoint, Throwable e) throws Throwable {
		if (isExceptionIsConnectionFailure(e)) {
			logger.error(CONNECTION_FAIL);
		}
	}

	private boolean isExceptionIsDBFailure(Throwable e) {
        Throwable t = e.getCause();
        boolean isSqlException = false;
        while (t != null) {
            if (t instanceof SQLException) {
                if (isDbErrorToLog(t.getMessage())) {
                	isSqlException = true;
                }
            }
            t = t.getCause();
        }
        return isSqlException;
    }

	private boolean isExceptionIsConnectionFailure(Throwable e) {
        Throwable t = e.getCause();
        boolean isSqlException = false;
        while (t != null) {
            if (t instanceof ConnectException) {
                if (isConnectionToLog(t.getMessage())) {
                	isSqlException = true;
                }
            }
            t = t.getCause();
        }
        return isSqlException;
    }

	private boolean isDbErrorToLog(String message) {
		for (String errorResponse : sqlErrors) {
			if (message.contains(errorResponse)) {
				return true;
			}
		}
		return false;
	}

	private boolean isConnectionToLog(String message) {
		for (String errorResponse : connectionErrors) {
			if (message.contains(errorResponse)) {
				return true;
			}
		}
		return false;
	}

}
