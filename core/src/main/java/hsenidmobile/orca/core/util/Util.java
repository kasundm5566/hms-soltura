/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.util;

import hsenidmobile.orca.core.model.message.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class Util {

    /**
     * Check whether given string is empty.
     * @param strToTest
     * @return
     */
    public static boolean isEmpty(String strToTest){
        if(strToTest == null || strToTest.trim().length() == 0){
            return true;
        } else {
            return false;
        }
    }

    /**
     * Create MoResponse Sms message from received message
     * @param receivedMessage
     * @param responseMsgTxt
     * @return
     */
    public static MoResponse createResponseMessage(Message receivedMessage, String responseMsgTxt) {
        SmsMessage smsResp = new SmsMessage();
        smsResp.setMessage(responseMsgTxt);
        smsResp.setSenderAddress(receivedMessage.getSenderAddress());
        smsResp.addReceiverAddress(receivedMessage.getSenderAddress());

        MoResponse moResponse = new MoResponse();
        moResponse.setDataResponse(new MtRequest(smsResp, null));
        return moResponse;
    }


    /**
     * Create MoResponse Sms message from received message
     * @param receivedMessage
     * @param responseMsgTxt
     * @return
     */
    public static SystemCommandResponce createSystemResponseMessage(Message receivedMessage, String responseMsgTxt) {
        SmsMessage smsResp = new SmsMessage();
        smsResp.setMessage(responseMsgTxt);
        smsResp.setSenderAddress(receivedMessage.getReceiverAddresses().get(0));
        smsResp.addReceiverAddress(receivedMessage.getSenderAddress());

        SystemCommandResponce moResponse = new SystemCommandResponce();
        moResponse.setDataResponse(new MtRequest(smsResp, null));
        return moResponse;
    }

    public static String[] csvToArray(String csvString) {
        return csvToArray(csvString, ",");
    }

    public static String[] csvToArray(String csvString, String delimiter) {
        String[] untrimmedResult = csvString.split(delimiter);
        List<String> result = new ArrayList<String>();
        for (String s : untrimmedResult) {
            if (s != null && s.trim().length() > 0) {
                result.add(s.trim());
            }
        }
        return result.toArray(new String[result.size()]);
    }

    public enum SdpAppState {
        DRAFT_REQUEST, REQUESTED_APPLICATION, APPROVED_APPLICATION_REQUEST, APPROVED_APP_REQUEST, PRE_ACTIVE_TB, ACTIVE_TB, PRE_ACTIVE_PRODUCTION,
        PRE_PROVISIONED_PRODUCTION, PRE_APPROVED_PRODUCTION, APPROVED_PRODUCTION,
        LIMITED_PRODUCTION, ACTIVE_PRODUCTION, RETIRED_PRODUCTION,SUSPENDED_ACTIVE_TB,
        CR_DRAFT_REQUEST_STATE, CR_REQUESTED_APP_STATE, CR_APPROVED_APP_REQ_STATE,
        CR_PREACTIVE_TESTBED,CR_ACTIVE_TESTBED, CR_PREACTIVE_PRODUCTION,SUSPENDED_ACTIVE_PRODUCTION,
        CR_PREPROVISIONED_PRODUCTION, CR_PREAPPROVED_PRODUCTION, CR_APPROVED_PRODUCTION,SUSPENDED_PRE_ACTIVE_TEST_BED,
        CR_LIMITED_PRODUCTIONSUSPENDED_PRE_ACTIVE_TEST_BED, SUSPENDED_ACTIVE_TEST_BED, SUSPENDED_PRE_ACTIVE_PRODUCTION,
        SUSPENDED_PRE_PROVISIONED_PRODUCTION, SUSPENDED_PRE_APPROVED_PRODUCTION, SUSPENDED_APPROVED_PRODUCTION,
        SUSPENDED_LIMITED_PRODUCTION, SUSPENDED_PRODUCTION, SUSPENDED_TESTBED, CR_LIMITED_PRODUCTION,
    }

}
