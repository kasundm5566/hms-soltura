/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hsenidmobile.orca.core.util.eventlog;

import static java.text.MessageFormat.format;
import hsenidmobile.orca.core.model.Application;
import hsenidmobile.orca.core.model.Event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * $LastChangedDate: $ $LastChangedBy: $ $LastChangedRevision: $
 */
public class EventLogger {

	private static final Logger logger = LoggerFactory.getLogger(EventLogger.class);

	private static final String DELIMITER = "|";

	public static void createEventLog(Application app, String subCategory, String cpId, Event event, String message) {

		logger.trace(format("applicationId: {0} " + DELIMITER + " applicationName: {1} " + DELIMITER
				+ " subCategory: {2} " + DELIMITER + " cpId: {3} " + DELIMITER + " event: {4} " + DELIMITER
				+ " message: {5}", app.getAppId(), app.getAppName(), subCategory, cpId, event, message));
	}

}
