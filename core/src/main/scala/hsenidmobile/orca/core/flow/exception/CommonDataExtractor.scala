/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.flow.exception

import hsenidmobile.orca.core.flow.event.MessageFlowEvent
import hsenidmobile.orca.core.model.message.Message

/**
 * Extract commonly used parameters from MessageEvent to be used for error response generation.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
class CommonDataExtractor {

  def extract(paraKey: String, event: MessageFlowEvent[Message]) = paraKey match {
    case "app_id" => replaceNull(getAppId(event))
    case "shortcode" => replaceNull(getShortCode(event))
    case "msisdn" => replaceNull(getMsisdn(event))
    case "keyword" => replaceNull(getServiceKeyword(event))
    case "subkeyword" => replaceNull(getSubKeyword(event))
    case "message" => replaceNull(getMessage(event))
    case _ => ""

  }

  private def getAppId(event: MessageFlowEvent[Message]) = {
    if (event != null && event.getMessageContext() != null && event.getMessageContext().getApplication() != null) {
      event.getMessageContext().getApplication().getAppName
    } else {
      ""
    }
  }

  private def replaceNull(source: String) = source match {
    case null => ""
    case _ => source
  }

  private def getShortCode(event: MessageFlowEvent[Message]) = {
    if (event != null && event.getMessage != null && event.getMessage.getReceiverAddresses != null
      && event.getMessage().getReceiverAddresses.size > 0) {
      event.getMessage.getReceiverAddresses.get(0).getAddress
    } else {
      ""
    }
  }

    private def getMsisdn(event: MessageFlowEvent[Message]) = {
    if (event != null && event.getMessage != null && event.getMessage.getSenderAddress != null) {
      event.getMessage.getSenderAddress.getAddress
    } else {
      ""
    }
  }

  private def getServiceKeyword(event: MessageFlowEvent[Message]) = {
    if (event != null && event.getMessage != null) {
      event.getMessage.getKeyword
    } else {
      ""
    }
  }

  private def getSubKeyword(event: MessageFlowEvent[Message]) = {
    if (event != null) {
      event.getSubKeyword
    } else {
      ""
    }
  }

  private def getMessage(event: MessageFlowEvent[Message]) = {
    if (event != null && event.getMessage != null) {
      event.getMessage.getMessage
    } else {
      ""
    }
  }

}