/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.flow.exception

import hsenidmobile.orca.core.flow.event.MessageEvent
import hsenidmobile.orca.core.flow.dispatch.aventuraapi.AventuraApiMtMessageDispatcher
import scala.reflect.BeanProperty
import hsenidmobile.orca.core.model.message.SmsMessage
import hsenidmobile.orca.core.flow.event.MessageFlowEvent
import hsenidmobile.orca.core.model.message.AbstractMessageImpl
import java.util.Arrays
import org.apache.log4j.Logger


/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
class DefaultExceptionHandler extends ExceptionHandler {

  val logger: Logger = Logger.getLogger(this.getClass)

  @BeanProperty
  var aventuraApiMtMessageDispatcher: AventuraApiMtMessageDispatcher = null
  @BeanProperty
  var errorMsgBuilder : ErrorMessageBuilderImpl = null

  override def handleException(event: MessageEvent, throwable: Throwable) = {
    event.getMessage() match {
      case messageEvent: SmsMessage =>
        logger.error("Exception occurred during message processing.", throwable)

        aventuraApiMtMessageDispatcher.dispatchMessage(Arrays.asList(createSmsMessage(messageEvent, throwable)))

      case other =>
        logger.error("Error has occurred for a not supported message type[" + other + "]", throwable)
    }

  }

  private def createSmsMessage(moMessage: SmsMessage, throwable: Throwable): SmsMessage = {
    val respMsg: SmsMessage = new SmsMessage
    respMsg.setCorrelationId(moMessage.getCorrelationId)
    respMsg.addReceiverAddress(moMessage.getSenderAddress)
    respMsg.setKeyword(moMessage.getSenderAddress().getAddress)
    respMsg.setSenderAddress(null)
    respMsg.setMessage(errorMsgBuilder.buildResponse(throwable))
    return respMsg
  }
}
