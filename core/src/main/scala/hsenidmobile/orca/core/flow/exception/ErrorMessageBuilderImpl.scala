/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.flow.exception

import hsenidmobile.orca.core.flow.MessageFlowException
import hsenidmobile.orca.core.model.message.{ SmsMessage => Sms }
import scala.reflect.BeanProperty
import hsenidmobile.orca.core.applications.exception.ApplicationException
import java.util.Map
import hsenidmobile.orca.core.flow.{ MessageFlowErrorReasons => Reason }
import scala.collection.JavaConversions._
import scala.util.matching.Regex
import hsenidmobile.orca.core.flow.event.MessageFlowEvent
import hsenidmobile.orca.core.model.message.Message

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
class ErrorMessageBuilderImpl extends ErrorMessageBuilder {

  @BeanProperty
  var messageMapping: Map[String, String] = null
  @BeanProperty
  var defaultErrorMessage: String = null
  @BeanProperty
  var commonDataExtractor: CommonDataExtractor = null

  val placeHolderRegx: Regex = new Regex("""\{(\w+)\}""")

  override def buildResponse(messageFlowException: Throwable): String = {
    messageFlowException match {
      case ex: MessageFlowException =>
        createResponseSms(ex)

      case other => defaultErrorMessage
    }

  }

  private def createResponseSms(exception: MessageFlowException): String = {

    if (exception == null || exception.getReason == null) {
      defaultErrorMessage
    } else {
      val errorMsg = messageMapping.get(exception.getReason.name);
      if (errorMsg == null) {
        defaultErrorMessage
      } else {
        exception.getMessageFlowEvent match {
          case null =>
            replacePlaceHolders(errorMsg, exception.getAdditionalParameters)
          case event: MessageFlowEvent[Message] =>
            replacePlaceHolders(errorMsg, exception.getAdditionalParameters, event)
          case _ =>
            defaultErrorMessage
        }
      }

    }

  }

  private def replaceParameters(formatString: String, parameters: Map[String, Object]) = {
    (formatString /: parameters) { (t, kv) =>
      t.replace("{" + kv._1 + "}", kv._2.toString)
    }
  }

  private def replacePlaceHolders(text: String, parameters: Map[String, Object],
    event: MessageFlowEvent[Message]): String = {
    val placeHolders = placeHolderRegx.findAllIn(text)
    var formattedText = text
    for { placeHolder <- placeHolders } {
      val placeHolderKey = removeBracs(placeHolder)
      var value = parameters.get(placeHolderKey)
      if (value == null) {
        value = commonDataExtractor.extract(placeHolderKey, event)
      }
      formattedText = formattedText.replace(placeHolder, value.toString)
    }

    return formattedText
  }

  private def replacePlaceHolders(text: String, parameters: Map[String, Object]): String = {
    val placeHolders = placeHolderRegx.findAllIn(text)
    var formattedText = text
    for { placeHolder <- placeHolders } {
      var value = parameters.get(removeBracs(placeHolder))
      if (value == null) {
        value = ""
      }
      formattedText = formattedText.replace(placeHolder, value.toString)
    }

    return formattedText
  }

  private def removeBracs(paramKey: String) = {
    val a: Regex = new Regex("""(\{)|(\})""")
    a.replaceAllIn(paramKey, "")
  }
}