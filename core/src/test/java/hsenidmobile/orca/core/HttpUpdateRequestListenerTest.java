/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core;

import hsenidmobile.orca.core.applications.ApplicationImpl;
import hsenidmobile.orca.core.applications.alert.AlertContentRelease;
import hsenidmobile.orca.core.applications.alert.AlertService;
import hsenidmobile.orca.core.flow.ApplicationFactory;
import hsenidmobile.orca.core.flow.ApplicationFinder;
import hsenidmobile.orca.core.flow.HttpUpdateRequestDispatcher;
import hsenidmobile.orca.core.flow.dispatch.aventuraapi.AventuraApiMtMessageDispatcher;
import hsenidmobile.orca.core.model.*;
import hsenidmobile.orca.core.model.Content.ContentSource;
import hsenidmobile.orca.core.model.message.AppDataUpdateRequest;
import hsenidmobile.orca.core.model.message.MessageContext;
import hsenidmobile.orca.core.repository.AlertDispatchedContentRepository;
import hsenidmobile.orca.core.repository.AppRepository;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static hsenidmobile.orca.core.testutil.AlertContentReleaseMatcher.alertContentWith;
import static hsenidmobile.orca.core.testutil.AventuraSmsContentMatcher.smsMessageWithContent;
import static hsenidmobile.orca.core.testutil.AventuraSmsSubKeywordMatcher.smsMessageWithSubkeyword;
import static hsenidmobile.orca.core.testutil.ListSizeMatcher.listOfSize;
import static org.hamcrest.core.AllOf.allOf;
import static org.hamcrest.core.AnyOf.anyOf;

/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class HttpUpdateRequestListenerTest {

	private HttpUpdateRequestListener updateRequestListener;
	private ApplicationFinder appFinder;
	private HttpUpdateRequestDispatcher updateRequestDispatcher;
	private AventuraApiMtMessageDispatcher messageDispatcher;
	private Mockery context;
	private AppRepository appRepository;
	private AlertDispatchedContentRepository dispatchedContentRepository;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		context = new Mockery();
		appRepository = context.mock(AppRepository.class);
		messageDispatcher = context.mock(AventuraApiMtMessageDispatcher.class);
		dispatchedContentRepository = context.mock(AlertDispatchedContentRepository.class);

		ApplicationFactory applicationFactory = new ApplicationFactory();
		applicationFactory.setAppRepository(appRepository);
		applicationFactory.setAventuraApiMtMessageDispatcher(messageDispatcher);
		applicationFactory.setAlertDispatchedContentRepository(dispatchedContentRepository);

		ApplicationFinder appFinder = new ApplicationFinder();
		appFinder.setAppRepository(appRepository);
		appFinder.setApplicationFactory(applicationFactory);
		appFinder.setSystemKeyWords(Collections.EMPTY_LIST);

		updateRequestListener = new HttpUpdateRequestListener();
		updateRequestListener.setApplicationFinder(appFinder);
		updateRequestListener.setHttpUpdateRequestDispatcher(new HttpUpdateRequestDispatcher());
		updateRequestListener.init();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for
	 * {@link hsenidmobile.orca.core.HttpUpdateRequestListener#onMessage(hsenidmobile.orca.core.model.message.AppDataUpdateRequest, hsenidmobile.orca.core.model.message.MessageContext)}
	 * .
	 *
	 * @throws OrcaException
	 */
	@Test
	public void testOnMessageUpdateAlertData() throws OrcaException {
		final ApplicationImpl application = new ApplicationImpl();
		AlertService alertService = new AlertService();
		application.setService(alertService);
		alertService.setApplication(application);
		application.setAppId("APP-ID");


		SubCategory catMr = new SubCategory();
		catMr.setKeyword("mr");
		SubCategory catSf = new SubCategory();
		catSf.setKeyword("sf");
		alertService.setSupportedSubCategories(Arrays.asList(catMr, catSf));

		String appId = application.getAppId();
		final String mrUpdateText = "Update content for mr";
		final String sfUpdateText = "Update content for sf";
		context.checking(new Expectations() {
			{
				allowing(appRepository).findAppByAppId(with(equal("APP-ID")));
				will(returnValue(application));

				allowing(messageDispatcher).dispatchBroadcastMessage(
						with(any(String.class)),
						with(allOf(
								anyOf(allOf(smsMessageWithSubkeyword("mr"), smsMessageWithContent(mrUpdateText)),
										allOf(smsMessageWithSubkeyword("sf"), smsMessageWithContent(sfUpdateText))),
								listOfSize(2))));

				allowing(appRepository).updateService(with(any(AlertService.class)));

				Map<String, String> expected = new HashMap<String, String>();
				expected.put("mr", mrUpdateText);
				expected.put("sf", sfUpdateText);
				allowing(dispatchedContentRepository).addDispatchedContent(with( alertContentWith(true, expected)));

			}
		});

		Content contentMr = new Content();
		contentMr.setSource(ContentSource.WEB);
		contentMr.setContent(new LocalizedMessage(mrUpdateText, Locale.ENGLISH));
		contentMr.setSubCategory(catMr);

		Content contentSf = new Content();
		contentSf.setSource(ContentSource.WEB);
		contentSf.setContent(new LocalizedMessage(sfUpdateText, Locale.ENGLISH));
		contentSf.setSubCategory(catSf);
		ContentRelease contentRelease = new AlertContentRelease();
		contentRelease.setContents(Arrays.asList(contentMr, contentSf));

		AppDataUpdateRequest updateRequest = new AppDataUpdateRequest();
		updateRequest.setAppId(appId);
		updateRequest.setUpdateContent(Arrays.asList(contentRelease));

		updateRequestListener.onMessage(updateRequest, new MessageContext());

	}
	// /**
	// * Test method for
	// * {@link
	// hsenidmobile.orca.core.HttpUpdateRequestListener#onMessage(hsenidmobile.orca.core.model.message.AppDataUpdateRequest,
	// hsenidmobile.orca.core.model.message.MessageContext)}
	// * .
	// *
	// * @throws OrcaException
	// * @throws IllegalAccessException
	// */
	// @Test
	// public void testOnMessageUpdateSubscriptionData() throws OrcaException,
	// IllegalAccessException, RoutingKeyNotFoundException {
	// final ApplicationImpl application =
	// MockApplicationCreator.createSubscriptionApplication(new Msisdn("9696",
	// MobileOperators.DIALOG), "stv sds");
	// String appId = application.getAppId();
	// application.setRegistrationPolicy(RegistrationPolicy.NONE);
	// context.checking(new Expectations() {
	// {
	// allowing(appRepository).findAppByAppId(with(any(String.class)));
	// will(returnValue(application));
	// }
	// });
	//
	// Subscription subscriptionService = (Subscription)
	// application.getService();
	// List<SubscriptionServiceData> subscriptionData =
	// getSubscriptionDataListThroughReflection(subscriptionService);
	//
	// long dataSizeBefore = 0;
	// boolean updateFound = false;
	// for (SubscriptionServiceData ssd : subscriptionData) {
	// if (ssd.getPeriodicity().getUnit() == PeriodUnit.HOUR) {
	// if (ssd.getData() != null) {
	// dataSizeBefore = ssd.getData().size();
	// } else {
	// dataSizeBefore = 0;
	// }
	// break;
	// }
	// }
	//
	// AppDataUpdateRequest updateRequest = new AppDataUpdateRequest();
	// updateRequest.setAppId(appId);
	// updateRequest.setPeriodicity(new Periodicity(PeriodUnit.HOUR, 1, 0));
	//
	// updateRequest.addUpdateContent(createUpdateKeywordDetail("subkey1"));
	// updateRequest.addUpdateContent(createUpdateKeywordDetail("subkey2"));
	// updateRequest.addUpdateContent(createUpdateKeywordDetail("subkey3"));
	// updateRequest.addUpdateContent(createUpdateKeywordDetail("subkey4"));
	// updateRequest.addUpdateContent(createUpdateKeywordDetail("subkey5"));
	//
	// updateRequestListener.onMessage(updateRequest, new MessageContext());
	//
	// subscriptionService = (Subscription) application.getService();
	// subscriptionData =
	// getSubscriptionDataListThroughReflection(subscriptionService);
	//
	// for (SubscriptionServiceData ssd : subscriptionData) {
	// if (ssd.getPeriodicity().getUnit() == PeriodUnit.HOUR) {
	// List<ContentGroup> sortedData = sort(ssd.getData(),
	// on(ContentGroup.class).getId());
	//
	// assertEquals("Content elements size", dataSizeBefore + 1,
	// sortedData.size());
	//
	// ContentGroup cg = sortedData.get(sortedData.size() - 1);
	// for (int i = 1; i <= 5; i++) {
	// KeywordDetail kd = cg.getContent("subkey" + i);
	// assertEquals("Updated content", "New Updated Description-" +
	// kd.getKeyword(), kd
	// .getDescription(new Locale("si")));
	// updateFound = true;
	// }
	// }
	// }
	//
	// assertTrue("Content should be updated", updateFound);
	// }
	//
	// private List<SubscriptionServiceData>
	// getSubscriptionDataListThroughReflection(Subscription
	// subscriptionService)
	// throws IllegalAccessException {
	// Field[] declaredFields = Subscription.class.getDeclaredFields();
	// for (Field field : declaredFields) {
	// if (field.getName().equals("subscriptionDataList")) {
	// field.setAccessible(true);
	// return (List<SubscriptionServiceData>) field.get(subscriptionService);
	// }
	// }
	// return null;
	// }

}
