/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core;

import static org.junit.Assert.assertTrue;
import hsenidmobile.orca.core.applications.ApplicationImpl;
import hsenidmobile.orca.core.applications.exception.ApplicationException;
import hsenidmobile.orca.core.applications.subscription.SubscriptionServiceDataRepository;
import hsenidmobile.orca.core.flow.ApplicationFactory;
import hsenidmobile.orca.core.flow.ApplicationFinder;
import hsenidmobile.orca.core.flow.ApplicationSubkeywordExtractor;
import hsenidmobile.orca.core.flow.MoRequestDispatcher;
import hsenidmobile.orca.core.flow.MtRequestHandler;
import hsenidmobile.orca.core.flow.command.CommandBuilder;
import hsenidmobile.orca.core.flow.dispatch.DispatchableMessage;
import hsenidmobile.orca.core.flow.dispatch.lb.RoundRobinLb;
import hsenidmobile.orca.core.flow.impl.RequestPipelineImpl;
import hsenidmobile.orca.core.mock.StubChargingService;
import hsenidmobile.orca.core.model.OrcaException;
import hsenidmobile.orca.core.model.message.AoRequest;
import hsenidmobile.orca.core.model.message.Message;
import hsenidmobile.orca.core.model.message.MessageContext;
import hsenidmobile.orca.core.model.message.MtRequest;
import hsenidmobile.orca.core.model.message.SmsMessage;
import hsenidmobile.orca.core.repository.AppRepository;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.Before;
import org.junit.Test;

/**
 * Test Complete Mo/Mt message Flow integration test
 *
 * $LastChangedDate$
 * $LastChangedBy$
 *
 * @version $LastChangedRevision$
 */
public class MoMessageFlowTest {

	private Mockery mockContext;
	private SubscriptionServiceDataRepository subscriptionDataRepo;
	private AppRepository appRepository;

	@Before
	public void setup() throws ApplicationException {
		mockContext = new Mockery();
		subscriptionDataRepo = mockContext.mock(SubscriptionServiceDataRepository.class);
		appRepository = mockContext.mock(AppRepository.class);

		mockContext.checking(new Expectations() {
			{
				allowing(appRepository).findAppByAppId(with("testApp"));
				will(returnValue(new ApplicationImpl()));
			}
		});
	}

	/**
	 * Test the integration of components in AO flow.
	 *
	 * @throws OrcaException
	 */
	@Test
	public void testAoFlow() throws OrcaException {
		AoRequest aoRequest = new AoRequest();
		Message message = new SmsMessage();
		message.setMessage("This is a test message from Dev Applicaiton...");
		aoRequest.setMessage(message);
		aoRequest.setAppId("testApp");
		MessageContext messageContext = new MessageContext();

		RequestPipelineImpl pipeline = new RequestPipelineImpl();
		// Message Filters e.g. Virus message filtering
		ApplicationFinder appFinder = new ApplicationFinder();
		appFinder.setAppRepository(appRepository);
		ApplicationFactory applicationFactory = new ApplicationFactory();
		appFinder.setApplicationFactory(applicationFactory);
		pipeline.addLast("app-finder", appFinder);

		// Validate Application password, IP
		MtRequestHandler mtSender = new MtRequestHandler();
		RoundRobinLb<MtRequest> rrLb = new RoundRobinLb<MtRequest>();
		mtSender.setRrMessageStore(rrLb);
		pipeline.addLast("mt-sender", mtSender);

		pipeline.onMessage(aoRequest, messageContext);

		DispatchableMessage<MtRequest> dm = rrLb.getNext();
		assertTrue("Message to send cannot be null", dm.getMessage() != null);

	}

	private RequestPipelineImpl createMoPipeline(RoundRobinLb<MtRequest> rrLb) {
		RequestPipelineImpl pipeline = new RequestPipelineImpl();

		ApplicationFinder appFinder = new ApplicationFinder();
		appFinder.setAppRepository(appRepository);
		ApplicationFactory applicationFactory = new ApplicationFactory();
		applicationFactory.setSubscriptionDataService(subscriptionDataRepo);
		applicationFactory.setChargingService(new StubChargingService());
		appFinder.setApplicationFactory(applicationFactory);
		pipeline.addLast("app-finder", appFinder);

		pipeline.addLast("subkey-extractor", new ApplicationSubkeywordExtractor());

		CommandBuilder commandBuilder = new CommandBuilder();
		pipeline.addLast("commad-builder", commandBuilder);

		pipeline.addLast("mo-dispatcher", new MoRequestDispatcher());

		MtRequestHandler mtSender = new MtRequestHandler();
		mtSender.setRrMessageStore(rrLb);
		pipeline.addLast("mt-sender", mtSender);

		return pipeline;
	}
}
