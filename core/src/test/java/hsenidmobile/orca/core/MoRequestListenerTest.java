/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core;

import hsenidmobile.orca.core.applications.ApplicationImpl;
import hsenidmobile.orca.core.applications.Service;
import hsenidmobile.orca.core.applications.subscription.SubscriptionServiceDataRepository;
import hsenidmobile.orca.core.applications.voting.Vote;
import hsenidmobile.orca.core.applications.voting.VotingEventListener;
import hsenidmobile.orca.core.flow.*;
import hsenidmobile.orca.core.flow.command.CommandBuilder;
import hsenidmobile.orca.core.flow.dispatch.aventuraapi.AventuraApiMtMessageDispatcher;
import hsenidmobile.orca.core.flow.dispatch.lb.RoundRobinLb;
import hsenidmobile.orca.core.flow.event.MessageEvent;
import hsenidmobile.orca.core.flow.exception.ExceptionHandler;
import hsenidmobile.orca.core.mock.MobileOperators;
import hsenidmobile.orca.core.mock.StubChargingService;
import hsenidmobile.orca.core.model.*;
import hsenidmobile.orca.core.model.message.MtRequest;
import hsenidmobile.orca.core.repository.AlertDispatchedContentRepository;
import hsenidmobile.orca.core.repository.AppRepository;
import hsenidmobile.orca.core.repository.SdpApplicationRepository;
import hsenidmobile.orca.core.repository.VoteRepository;
import hsenidmobile.orca.core.services.InvalidRequestService;
import hsenidmobile.orca.core.testutil.TestUtil;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JMock;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 *
 * @version $LastChangedRevision$
 */
@RunWith(JMock.class)
public class MoRequestListenerTest {
	private static final String UPDATE_SUCCESS_MSG = "Service Data Updated Successfully.";
	private Mockery context;
	private AppRepository appRepository;
	private MoRequestListener moRequestListener;
	private RoundRobinLb<MtRequest> rrLb;
	private VoteRepository votingRepository;
	private InvalidRequestService invalidRequestService;
	private SdpApplicationRepository sdpAppRepository;
	private AventuraApiMtMessageDispatcher messageDispatcher;
	private VotingEventListener votingEventListener;
	private AlertDispatchedContentRepository dispatchedContentRepository;
	private SubscriptionServiceDataRepository subscriptionDataRepo;

	/**
	 * @throws java.lang.Exception
	 *             -
	 */
	@Before
	public void setUp() throws Exception {
		context = new Mockery();
		appRepository = context.mock(AppRepository.class);
		votingRepository = context.mock(VoteRepository.class);
		invalidRequestService = context.mock(InvalidRequestService.class);
		sdpAppRepository = context.mock(SdpApplicationRepository.class);
		messageDispatcher = context.mock(AventuraApiMtMessageDispatcher.class);
		votingEventListener = context.mock(VotingEventListener.class);
		dispatchedContentRepository = context.mock(AlertDispatchedContentRepository.class);
		subscriptionDataRepo = context.mock(SubscriptionServiceDataRepository.class);

		rrLb = new RoundRobinLb<MtRequest>();

		ApplicationFactory applicationFactory = new ApplicationFactory();
		applicationFactory.setSubscriptionDataService(subscriptionDataRepo);
		applicationFactory.setChargingService(new StubChargingService());
		applicationFactory.setVotingEventListener(votingEventListener);
		applicationFactory.setAventuraApiMtMessageDispatcher(messageDispatcher);
		applicationFactory.setAppRepository(appRepository);
		applicationFactory.setAlertDispatchedContentRepository(dispatchedContentRepository);
		applicationFactory.setVoteRepository(votingRepository);

		ApplicationFinder appFinder = new ApplicationFinder();
		appFinder.setAppRepository(appRepository);
		appFinder.setApplicationFactory(applicationFactory);
		appFinder.setInvalidRequestService(invalidRequestService);
		appFinder.setSdpApplicationRepository(sdpAppRepository);

		List<String> systemKeywords = new ArrayList<String>();
		systemKeywords.add("verify");
		appFinder.setSystemKeyWords(systemKeywords);

		CommandBuilder commandBuilder = new CommandBuilder();
		commandBuilder.init();

		MoRequestDispatcher moRequestDispatcher = new MoRequestDispatcher();

		MtRequestHandler mtSender = new MtRequestHandler();
		mtSender.setRrMessageStore(rrLb);

		ApplicationSubkeywordExtractor subkeywordExtractor = new ApplicationSubkeywordExtractor();
		List<String> specialKeywords = new ArrayList<String>();
		specialKeywords.add("result");
		specialKeywords.add("abuse");
		specialKeywords.add("unreg");
		subkeywordExtractor.setSpecialSysWideKeywords(specialKeywords);
		subkeywordExtractor.setInvalidRequestService(new InvalidRequestService() {
			@Override
			public void invalidRequestReceived(InvalidRequest invalidRequest) {
			}

			@Override
			public int getInvalidRequestSummary(String appId) {
				return 0;
			}
		});

		moRequestListener = new MoRequestListener();
		moRequestListener.setApplicationFinder(appFinder);
		moRequestListener.setCommandBuilder(commandBuilder);
		moRequestListener.setMoRequestDispatcher(moRequestDispatcher);
		moRequestListener.setMtRequestHandler(mtSender);
		moRequestListener.setAppKeywordExtractor(subkeywordExtractor);
		moRequestListener.setExceptionHandler(new ExceptionHandler() {

			@Override
			public void handleException(MessageEvent event, Throwable throwable) {
				throw new RuntimeException(throwable);
			}
		});

		moRequestListener.init();

		context.checking(new Expectations() {
			{
				allowing(invalidRequestService).invalidRequestReceived(with(any(InvalidRequest.class)));

				allowing(sdpAppRepository).getOrcaAppId(with(any(String.class)));
				will(returnValue("ORCA_ID"));

				allowing(votingEventListener).successfulVote(with(any(Long.class)), with(any(Vote.class)));

				allowing(appRepository).updateService(with(any(Service.class)));
			}
		});

	}

	/**
	 * Test method for
	 * {@link hsenidmobile.orca.core.MoRequestListener#onMessage(hsenidmobile.orca.core.model.message.Message, hsenidmobile.orca.core.model.message.MessageContext)}
	 * .
	 *
	 * @throws OrcaException
	 *             -
	 *             -
	 */
	@Test
	public void testOnMessageForDataMo_ForVotingApp() throws OrcaException {
//		final List<hsenidmobile.sdp.corelogic.domain.routing.RoutingKey> rkList = createRoutingKeyList();
//
		final ApplicationImpl application = createActiveApplication("0772485587");
//		VotingService votingService = new VotingService();
//		votingService.setId(1L);
//		votingService.setSupportedSubCategories(Arrays.asList(createSubCategory("subkey1", "Candidate1")));
//		votingService.setResponseSpecification(new NoVotingResponseSpecification());
//		votingService.setOneVotePerNumber(false);
//		votingService.setApplication(application);
//		application.setService(votingService);
//
//		context.checking(new Expectations() {
//			{
//				allowing(routingInfoRepository).findKeywordStartWith(with(any(String.class)), with(any(String.class)),
//						with(any(String.class)));
//				will(returnValue(rkList));
//
//				allowing(appRepository).findAppByAppId(with(any(String.class)));
//				will(returnValue(application));
//
//				allowing(votingRepository).add(with(any(Vote.class)));
//
//				allowing(appRepository).updateMoSummary(with(any(String.class)));
//
//			}
//		});
//
//		Message message = new SmsMessage();
//		message.setMessage("stv subkey1 subkey1");
//		message.setSenderAddress(new Msisdn("0772485587", MobileOperators.DIALOG));
//		List<Msisdn> destination = new ArrayList<Msisdn>();
//		destination.add(new Msisdn("7788", MobileOperators.DIALOG));
//		message.setReceiverAddresses(destination);
//		MessageContext messageContext = new MessageContext();
//		moRequestListener.onMessage(message, messageContext);
//
	}

	private SubCategory createSubCategory(String categoryCode, String description) {
		SubCategory candidate1 = new SubCategory();
		candidate1.setKeyword(categoryCode);
		candidate1.setDescription(Arrays.asList(new LocalizedMessage(description, Locale.ENGLISH)));

		return candidate1;
	}

//	@Test
//	public void testOnMessageForServiceDataUpdate_ForAlertApp_WithSubCategory() throws OrcaException,
//			RoutingKeyNotFoundException {
//
//		final List<hsenidmobile.sdp.corelogic.domain.routing.RoutingKey> rkList = createRoutingKeyList();
//
//		final String senderMsisdn = "777654321";
//		final String updateContent = "MR Gonna speak in a rally at TownHall";
//
//		final ApplicationImpl application = createActiveApplication(senderMsisdn);
//
//		AlertService service = new AlertService();
//		service.setSupportedSubCategories(Arrays.asList(createSubCategory("mr", "Mahinda"),
//				createSubCategory("sf", "Sarath")));
//		service.setApplication(application);
//		service.setSmsUpdateRequired(true);
//
//		application.setService(service);
//
//		context.checking(new Expectations() {
//			{
//				allowing(routingInfoRepository).findKeywordStartWith(with(any(String.class)), with(any(String.class)),
//						with(any(String.class)));
//				will(returnValue(rkList));
//
//				allowing(appRepository).findAppByAppId(with(any(String.class)));
//				will(returnValue(application));
//
//				allowing(appRepository).updateMoSummary(with(any(String.class)));
//
//				allowing(messageDispatcher).dispatchIndividualMessages(with(any(String.class)),
//						with(allOf(smsMessageWithSubkeyword(senderMsisdn), smsMessageWithContent(UPDATE_SUCCESS_MSG))));
//
//				allowing(messageDispatcher).dispatchBroadcastMessage(with(any(String.class)),
//						with(allOf(smsMessageWithSubkeyword("mr"), smsMessageWithContent(updateContent))));
//
//				Map<String, String> expected = new HashMap<String, String>();
//				expected.put("mr", "MR Gonna speak in a rally at TownHall");
//				allowing(dispatchedContentRepository).addDispatchedContent(with( alertContentWith(true, expected)));
//
//
//			}
//		});
//
//		Message message = new SmsMessage();
//		message.setMessage("update stv mr " + updateContent);
//		message.setSenderAddress(new Msisdn(senderMsisdn, MobileOperators.DIALOG));
//		List<Msisdn> destination = new ArrayList<Msisdn>();
//		destination.add(new Msisdn("7788", MobileOperators.DIALOG));
//		message.setReceiverAddresses(destination);
//
//		MessageContext messageContext = new MessageContext();
//
//		moRequestListener.onMessage(message, messageContext);
//
//	}

//	@Test
//	public void testOnMessageForServiceDataUpdate_ForAlertApp_WithOutSubCategory() throws OrcaException,
//			RoutingKeyNotFoundException {
//
//		final List<hsenidmobile.sdp.corelogic.domain.routing.RoutingKey> rkList = createRoutingKeyList();
//
//		final String senderMsisdn = "777654321";
//		final String updateContent = "MR Gonna speak in a rally at TownHall";
//
//		final ApplicationImpl application = createActiveApplication(senderMsisdn);
//
//		AlertService service = new AlertService();
//		service.setApplication(application);
//		service.setSmsUpdateRequired(true);
//		application.setService(service);
//
//		context.checking(new Expectations() {
//			{
//				allowing(routingInfoRepository).findKeywordStartWith(with(any(String.class)), with(any(String.class)),
//						with(any(String.class)));
//				will(returnValue(rkList));
//
//				allowing(appRepository).findAppByAppId(with(any(String.class)));
//				will(returnValue(application));
//
//				allowing(appRepository).updateMoSummary(with(any(String.class)));
//
//				allowing(messageDispatcher).dispatchIndividualMessages(with(any(String.class)),
//						with(allOf(smsMessageWithSubkeyword(senderMsisdn), smsMessageWithContent(UPDATE_SUCCESS_MSG))));
//
//				allowing(messageDispatcher).dispatchBroadcastMessage(with(any(String.class)),
//						with(allOf(smsMessageWithSubkeyword("all_registered"), smsMessageWithContent(updateContent))));
//
//				Map<String, String> expected = new HashMap<String, String>();
//				expected.put("", "MR Gonna speak in a rally at TownHall");
//				allowing(dispatchedContentRepository).addDispatchedContent(with( alertContentWith(false, expected)));
//
//			}
//		});
//
//		Message message = new SmsMessage();
//		message.setMessage("update stv " + updateContent);
//		message.setSenderAddress(new Msisdn(senderMsisdn, MobileOperators.DIALOG));
//		List<Msisdn> destination = new ArrayList<Msisdn>();
//		destination.add(new Msisdn("7788", MobileOperators.DIALOG));
//		message.setReceiverAddresses(destination);
//
//		MessageContext messageContext = new MessageContext();
//
//		moRequestListener.onMessage(message, messageContext);
//
//	}

	private ApplicationImpl createActiveApplication(final String authorMsisdn) {
		TestUtil.setJodaSystemTime(new DateTime(2010, 9, 10, 0, 0, 0, 0));
		final ApplicationImpl application = new ApplicationImpl();
		application.setAppName("App1");
		application.setStartDate(new DateTime(2010, 9, 1, 0, 0, 0, 0));
		application.setEndDate(new DateTime(2010, 9, 30, 0, 0, 0, 0));
		application.setStatus(Status.ACTIVE);
		application.setAuthors(Arrays.asList(new Author(new Msisdn(authorMsisdn, MobileOperators.DIALOG))));
		return application;
	}

//	private List<hsenidmobile.sdp.corelogic.domain.routing.RoutingKey> createRoutingKeyList() {
//		final List<hsenidmobile.sdp.corelogic.domain.routing.RoutingKey> rkList = new ArrayList<hsenidmobile.sdp.corelogic.domain.routing.RoutingKey>();
//		SharedShortcodeRoutingKey rk1 = new SharedShortcodeRoutingKey();
//		rk1.setKeyword("stv");
//		rk1.setShortcode("7788");
//		rk1.setStatus((short) 1);
//		SharedShortcodeRoutingKey rk2 = new SharedShortcodeRoutingKey();
//		rk2.setKeyword("mtv");
//		rk2.setShortcode("7788");
//		rk2.setStatus((short) 1);
//		rkList.add(rk1);
//		rkList.add(rk2);
//		return rkList;
//	}

}
