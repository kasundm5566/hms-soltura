/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.applications;

import hsenidmobile.orca.core.applications.exception.ApplicationException;
import hsenidmobile.orca.core.applications.voting.Vote;
import hsenidmobile.orca.core.applications.voting.VotingEventListener;
import hsenidmobile.orca.core.flow.MessageFlowErrorReasons;
import hsenidmobile.orca.core.flow.MessageFlowException;
import hsenidmobile.orca.core.flow.dispatch.aventuraapi.AventuraApiMtMessageDispatcher;
import hsenidmobile.orca.core.flow.event.CommandEvent;
import hsenidmobile.orca.core.mock.MobileOperators;
import hsenidmobile.orca.core.model.Author;
import hsenidmobile.orca.core.model.Msisdn;
import hsenidmobile.orca.core.model.OrcaException;
import hsenidmobile.orca.core.model.Status;
import hsenidmobile.orca.core.model.message.Message;
import hsenidmobile.orca.core.model.message.MessageContext;
import hsenidmobile.orca.core.model.message.SmsMessage;
import hsenidmobile.orca.core.repository.AppRepository;
import hsenidmobile.orca.core.services.charging.ChargingException;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 *
 * @version $LastChangedRevision$
 */
public class ApplicationImplTest {

	private ApplicationImpl application;
	private VotingEventListener votingEventListener;
	private Mockery mockContext;
	private AventuraApiMtMessageDispatcher messageDispatcher;
	private Service service;
	private AppRepository appRepository;

	/**
	 * @throws java.lang.Exception
	 *             -
	 */
	@Before
	public void setUp() throws Exception {

		mockContext = new Mockery();
		messageDispatcher = mockContext.mock(AventuraApiMtMessageDispatcher.class);
		votingEventListener = mockContext.mock(VotingEventListener.class);
		service = mockContext.mock(Service.class);
		appRepository = mockContext.mock(AppRepository.class);

		mockContext.checking(new Expectations() {
			{
				allowing(messageDispatcher).dispatchMessage(with(any(List.class)));

				allowing(messageDispatcher).dispatchIndividualMessages(with(any(String.class)), with(any(List.class)));

				allowing(votingEventListener).successfulVote(with(any(Long.class)), with(any(Vote.class)));
			}
		});

		application = new ApplicationImpl();
		application.setStatus(Status.ACTIVE);
		application.setStartDate(new DateTime());
		application.setEndDate(new DateTime().plusDays(10));

	}

	@Test
	public void testOnDataMessage() throws ChargingException, MessageFlowException {
	}

	@Test
	public void testUpdateServiceDataByCommandEvent() throws OrcaException {
		application.setAuthors(Arrays.asList(new Author(new Msisdn("777654321", MobileOperators.DIALOG))));
		application.setService(service);
		application.setApplicationRepository(appRepository);
		mockContext.checking(new Expectations() {
			{
				allowing(service).onServiceDataUpdateMessage(with(any(CommandEvent.class)));
				will(returnValue(null));

				allowing(appRepository).updateService(with(any(Service.class)));
			}
		});

		SmsMessage message = new SmsMessage();
		message.setSenderAddress(new Msisdn("777654321", MobileOperators.DIALOG));
		message.addReceiverAddress(new Msisdn("9696", MobileOperators.DIALOG));
		message.setMessage("subkey1");

		MessageContext messageContext = new MessageContext();
		messageContext.setApplication(application);
		CommandEvent<Message> commandEvent = new CommandEvent<Message>(message, messageContext, "subkey1");

		application.updateServiceData(commandEvent);
	}

	@Test
	public void testUpdateServiceDataByCommandEventForInvalidAuthor() throws OrcaException {
		application.setAuthors(Arrays.asList(new Author(new Msisdn("12345678", MobileOperators.DIALOG))));
		application.setService(service);
		application.setApplicationRepository(appRepository);
		mockContext.checking(new Expectations() {
			{
				allowing(service).onServiceDataUpdateMessage(with(any(CommandEvent.class)));
				will(returnValue(null));

				allowing(appRepository).updateService(with(any(Service.class)));
			}
		});

		SmsMessage message = new SmsMessage();
		message.setSenderAddress(new Msisdn("777654321", MobileOperators.DIALOG));
		message.addReceiverAddress(new Msisdn("9696", MobileOperators.DIALOG));
		message.setMessage("subkey1");

		MessageContext messageContext = new MessageContext();
		messageContext.setApplication(application);
		CommandEvent<Message> commandEvent = new CommandEvent<Message>(message, messageContext, "subkey1");

		try {
			application.updateServiceData(commandEvent);
			fail("Should throw ApplicationException");
		} catch (ApplicationException ex) {
			assertEquals(MessageFlowErrorReasons.INVALID_AUTHOR, ex.getReason());
		}
	}

	/*@Test
	public void testIsActive() throws Exception {

		application = new ApplicationImpl();

		application.setStartDate(new DateTime(2010, 9, 1, 0, 0, 0, 0));
		application.setEndDate(new DateTime(2010, 9, 30, 0, 0, 0, 0));
		application.setStatus(Status.ACTIVE);

		TestUtil.setJodaSystemTime(new DateTime(2010, 9, 29, 9, 30, 15, 520));
        application.setExpirableApp(false);
		assertTrue("should be active", application.isActive());

		TestUtil.setJodaSystemTime(new DateTime(2010, 8, 29, 9, 30, 15, 520));
        application.setExpirableApp(false);
		assertFalse("should be in-active; before start", application.isActive());

        TestUtil.setJodaSystemTime(new DateTime(2010, 8, 29, 9, 30, 15, 520));
        application.setExpirableApp(true);
        assertFalse("should be in-active; before start", application.isActive());

		TestUtil.setJodaSystemTime(new DateTime(2010, 10, 1, 9, 30, 15, 520));
        application.setExpirableApp(true);
		assertFalse("should be in-active; after end ", application.isActive());


        TestUtil.setJodaSystemTime(new DateTime(2010, 10, 1, 9, 30, 15, 520));
        application.setExpirableApp(false);
        assertTrue("should be active; after end since this is not a expirable app", application.isActive());

		TestUtil.setJodaSystemTime(new DateTime(2010, 9, 29, 9, 30, 15, 520));
		application.setStatus(Status.INACTIVE);
		assertFalse("should be in-active; status:INACTIVE", application.isActive());
	}*/

}
