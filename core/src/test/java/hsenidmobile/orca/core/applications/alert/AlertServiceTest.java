/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.applications.alert;

import hsenidmobile.orca.core.applications.exception.ApplicationException;
import hsenidmobile.orca.core.flow.MessageFlowErrorReasons;
import hsenidmobile.orca.core.flow.MessageFlowException;
import hsenidmobile.orca.core.flow.dispatch.aventuraapi.AventuraApiMtMessageDispatcher;
import hsenidmobile.orca.core.flow.event.CommandEvent;
import hsenidmobile.orca.core.model.*;
import hsenidmobile.orca.core.model.Content.ContentSource;
import hsenidmobile.orca.core.model.message.Message;
import hsenidmobile.orca.core.model.message.MessageContext;
import hsenidmobile.orca.core.model.message.SmsMessage;
import hsenidmobile.orca.core.repository.AlertDispatchedContentRepository;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static hsenidmobile.orca.core.testutil.AlertContentReleaseMatcher.alertContentWith;
import static hsenidmobile.orca.core.testutil.AventuraSmsContentMatcher.smsMessageWithContent;
import static hsenidmobile.orca.core.testutil.AventuraSmsSubKeywordMatcher.smsMessageWithSubkeyword;
import static hsenidmobile.orca.core.testutil.ListSizeMatcher.listOfSize;
import static org.hamcrest.core.AllOf.allOf;
import static org.hamcrest.core.AnyOf.anyOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 *
 * @version $LastChangedRevision$
 */
public class AlertServiceTest {

	private static final String UPDATE_SUCCESS_MSG = "Service Data Updated Successfully.";
	private Mockery mockContext;
	private AventuraApiMtMessageDispatcher messageDispatcher;
	private Application application;
	private AlertDispatchedContentRepository dispatchedContentRepository;

	private AlertService alertService;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		mockContext = new Mockery();
		messageDispatcher = mockContext.mock(AventuraApiMtMessageDispatcher.class);
		application = mockContext.mock(Application.class);
		dispatchedContentRepository = mockContext.mock(AlertDispatchedContentRepository.class);

		alertService = new AlertService();
		alertService.setApplication(application);
		alertService.setAventuraApiMtMessageDispatcher(messageDispatcher);
		alertService.setDispatchedContentRepository(dispatchedContentRepository);
		alertService.setSmsUpdateRequired(true);

		mockContext.checking(new Expectations() {
			{
				allowing(application).getAppId();
				will(returnValue("APP_ID"));

				allowing(application).getAppName();
				will(returnValue("APP_Name"));
			}
		});
	}

	private SmsMessage createSmsMessage(String msg, String sender, String receiver) {
		SmsMessage message = new SmsMessage();
		message.setMessage(msg);
		message.setSenderAddress(new Msisdn(sender, "Op1"));
		message.addReceiverAddress(new Msisdn(receiver, "Op1"));
		return message;
	}

	@Test
	public void testUpdateServiceData_ForAlertAppWithoutSubkey() throws OrcaException {
		final String updateContent = "MR is going to give a speak at a rally in Maharagama";
		mockContext.checking(new Expectations() {
			{
				allowing(messageDispatcher).dispatchBroadcastMessage(with(any(String.class)),
						with(allOf(smsMessageWithSubkeyword("all_registered"), smsMessageWithContent(updateContent))));

				Map<String, String> expected = new HashMap<String, String>();
				expected.put("", updateContent);
				allowing(dispatchedContentRepository).addDispatchedContent(with( alertContentWith(false, expected)));
			}
		});

		Content content = new Content();
		content.setSource(ContentSource.WEB);
		content.setContent(new LocalizedMessage(updateContent, Locale.ENGLISH));
		ContentRelease contentRelease = new AlertContentRelease();
		contentRelease.setContents(Arrays.asList(content));

		alertService.setSupportedSubCategories(null);

		alertService.updateServiceData(Arrays.asList(contentRelease));

//		List<AlertContentRelease> dispatchedContents = alertService.getDispatchedContents();
//		assertEquals("There should be content after update", 1, dispatchedContents.size());
	}

	@Test
	public void testUpdateServiceData_ForAlertAppWithoutSubkey_WithContentWithSubCategory() throws OrcaException {
		final String updateContent = "MR is going to give a speak at a rally in Maharagama";
		mockContext.checking(new Expectations() {
			{
				allowing(messageDispatcher).dispatchBroadcastMessage(with(any(String.class)),
						with(allOf(smsMessageWithSubkeyword("all_registered"), smsMessageWithContent(updateContent))));

			}
		});

		Content content = new Content();
		content.setSource(ContentSource.WEB);
		content.setContent(new LocalizedMessage(updateContent, Locale.ENGLISH));
		SubCategory cat1 = new SubCategory();
		cat1.setKeyword("mr");
		content.setSubCategory(cat1);
		ContentRelease contentRelease = new AlertContentRelease();
		contentRelease.setContents(Arrays.asList(content));

		alertService.setSupportedSubCategories(null);

		try {
			alertService.updateServiceData(Arrays.asList(contentRelease));
			fail("Should fail with sub key not supported error");
		} catch (ApplicationException e) {
			assertEquals(MessageFlowErrorReasons.SUB_KEYWORD_NOT_SUPPORTED, e.getReason());
		}

	}

	@Test
	public void testUpdateServiceData_WithEmptyUpdateTxt() throws OrcaException {
		final String updateContent = "";
		Content content = new Content();
		content.setSource(ContentSource.WEB);
		content.setContent(new LocalizedMessage(updateContent, Locale.ENGLISH));
		ContentRelease contentRelease = new AlertContentRelease();
		contentRelease.setContents(Arrays.asList(content));

		alertService.setSupportedSubCategories(null);

		try {
			alertService.updateServiceData(Arrays.asList(contentRelease));
			fail("Should fail with empty content error.");
		} catch (AlertServiceException e) {
			assertEquals(MessageFlowErrorReasons.ALERT_CONTENT_EMPTY, e.getReason());
		}

	}

	@Test
	public void testUpdateServiceData_ForAlertAppWithSubkey() throws OrcaException {
		SubCategory cat1 = new SubCategory();
		cat1.setKeyword("mr");
		cat1.addDescription(new LocalizedMessage("Mahinda", Locale.ENGLISH));
		SubCategory cat2 = new SubCategory();
		cat2.setKeyword("sf");
		cat2.addDescription(new LocalizedMessage("Sarath", Locale.ENGLISH));
		alertService.setSupportedSubCategories(Arrays.asList(cat1, cat2));

		final String mrUpdateText = "MR is going to give a speak at a rally in Maharagama";
		final String sfUpdateText = "SF is going to give a speak at a rally in Kadawatha";
		mockContext.checking(new Expectations() {
			{

				allowing(messageDispatcher).dispatchBroadcastMessage(
						with(any(String.class)),
						with(allOf(
								anyOf(allOf(smsMessageWithSubkeyword("mr"), smsMessageWithContent(mrUpdateText)),
										allOf(smsMessageWithSubkeyword("sf"), smsMessageWithContent(sfUpdateText))),
								listOfSize(2))));

				Map<String, String> expected = new HashMap<String, String>();
				expected.put("mr", mrUpdateText);
				expected.put("sf", sfUpdateText);
				allowing(dispatchedContentRepository).addDispatchedContent(with( alertContentWith(true, expected)));

			}
		});

		Content content1 = new Content();
		content1.setSource(ContentSource.WEB);
		content1.setContent(new LocalizedMessage(mrUpdateText, Locale.ENGLISH));
		content1.setSubCategory(cat1);
		Content content2 = new Content();
		content2.setSource(ContentSource.WEB);
		content2.setContent(new LocalizedMessage(sfUpdateText, Locale.ENGLISH));
		content2.setSubCategory(cat2);

		ContentRelease contentRelease = new AlertContentRelease();
		contentRelease.setContents(Arrays.asList(content1, content2));

		alertService.updateServiceData(Arrays.asList(contentRelease));

	}

	@Test
	public void testUpdateServiceData_ForAlertAppWithSubkey_WithOneContentEmpty() throws OrcaException {
		SubCategory cat1 = new SubCategory();
		cat1.setKeyword("mr");
		cat1.addDescription(new LocalizedMessage("Mahinda", Locale.ENGLISH));
		SubCategory cat2 = new SubCategory();
		cat2.setKeyword("sf");
		cat2.addDescription(new LocalizedMessage("Sarath", Locale.ENGLISH));
		alertService.setSupportedSubCategories(Arrays.asList(cat1, cat2));

		final String mrUpdateText = "MR is going to give a speak at a rally in Maharagama";
		final String sfUpdateText = "";
		mockContext.checking(new Expectations() {
			{
				allowing(messageDispatcher).dispatchBroadcastMessage(
						with(any(String.class)),
						with(allOf(anyOf(allOf(smsMessageWithSubkeyword("mr"), smsMessageWithContent(mrUpdateText))),
								listOfSize(1))));

				Map<String, String> expected = new HashMap<String, String>();
				expected.put("mr", mrUpdateText);
				allowing(dispatchedContentRepository).addDispatchedContent(with( alertContentWith(true, expected)));


			}
		});

		Content content1 = new Content();
		content1.setSource(ContentSource.WEB);
		content1.setContent(new LocalizedMessage(mrUpdateText, Locale.ENGLISH));
		content1.setSubCategory(cat1);
		Content content2 = new Content();
		content2.setSource(ContentSource.WEB);
		content2.setContent(new LocalizedMessage(sfUpdateText, Locale.ENGLISH));
		content2.setSubCategory(cat2);

		ContentRelease contentRelease = new AlertContentRelease();
		contentRelease.setContents(Arrays.asList(content1, content2));

		alertService.updateServiceData(Arrays.asList(contentRelease));

	}

	@Test
	public void testUpdateServiceData_WithInvalidKeyword() throws OrcaException {
		SubCategory cat1 = new SubCategory();
		cat1.setKeyword("mr");
		cat1.addDescription(new LocalizedMessage("Mahinda", Locale.ENGLISH));
		SubCategory cat2 = new SubCategory();
		cat2.setKeyword("sf");
		cat2.addDescription(new LocalizedMessage("Sarath", Locale.ENGLISH));
		alertService.setSupportedSubCategories(Arrays.asList(cat1, cat2));

		final String mrUpdateText = "MR is going to give a speak at a rally in Maharagama";
		final String sfUpdateText = "SF is going to give a speak at a rally in Kadawatha";

		Content content1 = new Content();
		content1.setSource(ContentSource.WEB);
		content1.setContent(new LocalizedMessage(mrUpdateText, Locale.ENGLISH));
		content1.setSubCategory(cat1);

		SubCategory invalidCategory = new SubCategory();
		invalidCategory.setKeyword("abc");
		Content content2 = new Content();
		content2.setSource(ContentSource.WEB);
		content2.setContent(new LocalizedMessage(sfUpdateText, Locale.ENGLISH));
		content2.setSubCategory(invalidCategory);

		ContentRelease contentRelease = new AlertContentRelease();
		contentRelease.setContents(Arrays.asList(content1, content2));

		try {
			alertService.updateServiceData(Arrays.asList(contentRelease));
			fail("should fail with invalid sub key error");
		} catch (MessageFlowException e) {
			assertEquals(MessageFlowErrorReasons.SUB_KEYWORD_NOT_FOUND, e.getReason());
		}

	}

//	@Test
	public void testOnServiceDataUpdateMessage_ForAlertAppWithoutSubkey() throws OrcaException {

		final String senderMsisdn = "12345678912";
		final String updateContent = "MR is going to give a speak at a rally in Maharagama";
		mockContext.checking(new Expectations() {
			{

				allowing(messageDispatcher).dispatchIndividualMessages(with(any(String.class)),
						with(allOf(smsMessageWithSubkeyword(senderMsisdn), smsMessageWithContent(UPDATE_SUCCESS_MSG))));

				allowing(messageDispatcher).dispatchBroadcastMessage(with(any(String.class)),
						with(allOf(smsMessageWithSubkeyword("all_registered"), smsMessageWithContent(updateContent))));

				Map<String, String> expected = new HashMap<String, String>();
				expected.put("", updateContent);
				allowing(dispatchedContentRepository).addDispatchedContent(with( alertContentWith(false, expected)));


			}
		});

		alertService.setSupportedSubCategories(null);

		SmsMessage message = createSmsMessage(updateContent, senderMsisdn, "9696");

		CommandEvent<Message> commandEvent = new CommandEvent<Message>(message, new MessageContext());
		commandEvent.setSubKeyword("");
		alertService.onServiceDataUpdateMessage(commandEvent);

	}

//	@Test
	public void testOnServiceDataUpdateMessage_ForAlertAppWithSubkey() throws OrcaException {
		SubCategory cat1 = new SubCategory();
		cat1.setKeyword("mr");
		cat1.addDescription(new LocalizedMessage("Mahinda", Locale.ENGLISH));
		SubCategory cat2 = new SubCategory();
		cat2.setKeyword("sf");
		cat2.addDescription(new LocalizedMessage("Sarath", Locale.ENGLISH));
		alertService.setSupportedSubCategories(Arrays.asList(cat1, cat2));

		final String senderMsisdn = "12345678912";
		final String mrUpdateText = "MR is going to give a speak at a rally in Maharagama";
		final String sfUpdateText = "SF is going to give a speak at a rally in Kadawatha";
		mockContext.checking(new Expectations() {
			{
				allowing(messageDispatcher).dispatchIndividualMessages(with(any(String.class)),
						with(allOf(smsMessageWithSubkeyword(senderMsisdn), smsMessageWithContent(UPDATE_SUCCESS_MSG))));

				allowing(messageDispatcher).dispatchBroadcastMessage(
						with(any(String.class)),
						with(anyOf(allOf(smsMessageWithSubkeyword("mr"), smsMessageWithContent(mrUpdateText)),
								allOf(smsMessageWithSubkeyword("sf"), smsMessageWithContent(sfUpdateText)))));

				Map<String, String> expected = new HashMap<String, String>();
				expected.put("mr", mrUpdateText);
				allowing(dispatchedContentRepository).addDispatchedContent(with( alertContentWith(true, expected)));

			}
		});

		SmsMessage message = createSmsMessage(mrUpdateText, senderMsisdn, "9696");
		CommandEvent<Message> commandEvent = new CommandEvent<Message>(message, new MessageContext());
		commandEvent.setSubKeyword("mr");
		alertService.onServiceDataUpdateMessage(commandEvent);

	}

	@Test
	public void testOnServiceDataUpdateMessage_ForAlertAppWithSubkey_ForInvalidSubkeyword() throws OrcaException {
		SubCategory cat1 = new SubCategory();
		cat1.setKeyword("mr");
		cat1.addDescription(new LocalizedMessage("Mahinda", Locale.ENGLISH));
		SubCategory cat2 = new SubCategory();
		cat2.setKeyword("sf");
		cat2.addDescription(new LocalizedMessage("Sarath", Locale.ENGLISH));
		alertService.setSupportedSubCategories(Arrays.asList(cat1, cat2));
		final String senderMsisdn = "12345678912";
		final String mrUpdateText = "MR is going to give a speak at a rally in Maharagama";

		SmsMessage message = createSmsMessage(mrUpdateText, senderMsisdn, "9696");
		CommandEvent<Message> commandEvent = new CommandEvent<Message>(message, new MessageContext());
		commandEvent.setSubKeyword("mr1");
		try {
			alertService.onServiceDataUpdateMessage(commandEvent);
			fail("Should fail with incorrect sub keyword error");
		} catch (MessageFlowException e) {
			assertEquals(MessageFlowErrorReasons.SUB_KEYWORD_NOT_FOUND, e.getReason());
		}

	}

	@Test
	public void testOnServiceDataUpdateMessage_WhenAppDoNotSupportSmsUpdate() throws OrcaException {
		alertService.setSmsUpdateRequired(false);
		SubCategory cat1 = new SubCategory();
		cat1.setKeyword("mr");
		cat1.addDescription(new LocalizedMessage("Mahinda", Locale.ENGLISH));
		SubCategory cat2 = new SubCategory();
		cat2.setKeyword("sf");
		cat2.addDescription(new LocalizedMessage("Sarath", Locale.ENGLISH));
		alertService.setSupportedSubCategories(Arrays.asList(cat1, cat2));
		final String senderMsisdn = "12345678912";
		final String mrUpdateText = "MR is going to give a speak at a rally in Maharagama";

		SmsMessage message = createSmsMessage(mrUpdateText, senderMsisdn, "9696");
		CommandEvent<Message> commandEvent = new CommandEvent<Message>(message, new MessageContext());
		commandEvent.setSubKeyword("mr1");
		try {
			alertService.onServiceDataUpdateMessage(commandEvent);
			fail("Should fail with sms update not supported error");
		} catch (MessageFlowException e) {
			assertEquals(MessageFlowErrorReasons.SMS_UPDATE_NOT_SUPPORTED, e.getReason());
		}

	}

}
