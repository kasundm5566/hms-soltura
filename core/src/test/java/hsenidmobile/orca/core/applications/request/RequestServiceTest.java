/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.applications.request;

import hsenidmobile.orca.core.applications.exception.ApplicationException;
import hsenidmobile.orca.core.flow.MessageFlowErrorReasons;
import hsenidmobile.orca.core.flow.dispatch.aventuraapi.AventuraApiMtMessageDispatcher;
import hsenidmobile.orca.core.flow.event.CommandEvent;
import hsenidmobile.orca.core.model.Application;
import hsenidmobile.orca.core.model.LocalizedMessage;
import hsenidmobile.orca.core.model.Msisdn;
import hsenidmobile.orca.core.model.SubCategory;
import hsenidmobile.orca.core.model.message.Message;
import hsenidmobile.orca.core.model.message.MessageContext;
import hsenidmobile.orca.core.model.message.SmsMessage;
import hsenidmobile.orca.core.repository.RequestServiceDataRepository;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Locale;

import static hsenidmobile.orca.core.testutil.AventuraSmsContentMatcher.smsMessageWithContent;
import static hsenidmobile.orca.core.testutil.AventuraSmsSubKeywordMatcher.smsMessageWithSubkeyword;
import static org.hamcrest.core.AllOf.allOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * $LastChangedDate$ $LastChangedBy$ $LastChangedRevision$
 *
 */
public class RequestServiceTest {
	private Mockery mockContext;
	private AventuraApiMtMessageDispatcher messageDispatcher;
	private RequestServiceDataRepository requestServiceDataRepository;
	private Application application;

	private RequestService requestService;

	private String responseMsg = "Thanks for your comments.";

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		mockContext = new Mockery();
		messageDispatcher = mockContext.mock(AventuraApiMtMessageDispatcher.class);
		requestServiceDataRepository = mockContext.mock(RequestServiceDataRepository.class);
		application = mockContext.mock(Application.class);

		requestService = new RequestService();
		requestService.setApplication(application);
		requestService.setAventuraApiMtMessageDispatcher(messageDispatcher);
		requestService.setRequestServiceDataRepository(requestServiceDataRepository);
		requestService.setResponseMessages(Arrays.asList(new LocalizedMessage(responseMsg, Locale.ENGLISH)));

		mockContext.checking(new Expectations() {
			{
				allowing(application).getAppId();
				will(returnValue("APP_ID"));

				allowing(application).getAppName();
				will(returnValue("APP_Name"));

				allowing(requestServiceDataRepository).add(with(any(RequestServiceData.class)));
			}
		});
	}

//	@Test
	public void testOnDataMessage_ForAppWithOutSubKeyword() throws Exception {
		final String smsContent = "Hi DJ, I like your show...";
		final String senderMsisdn = "12345678912";
		SmsMessage message = createSmsMessage(smsContent, senderMsisdn, "9696");

		mockContext.checking(new Expectations() {
			{

				allowing(messageDispatcher).dispatchIndividualMessages(with(any(String.class)),
						with(allOf(smsMessageWithSubkeyword(senderMsisdn), smsMessageWithContent(responseMsg))));

			}
		});

		CommandEvent<Message> commandEvent = new CommandEvent<Message>(message, new MessageContext());
		commandEvent.setSubKeyword("");

		requestService.onDataMessage(commandEvent);
	}

//	@Test
	public void testOnDataMessage_ForAppWithSubKeyword() throws Exception {
		SubCategory cat1 = new SubCategory();
		cat1.setKeyword("mr");
		cat1.addDescription(new LocalizedMessage("Mahinda", Locale.ENGLISH));
		SubCategory cat2 = new SubCategory();
		cat2.setKeyword("sf");
		cat2.addDescription(new LocalizedMessage("Sarath", Locale.ENGLISH));
		requestService.setSupportedSubCategories(Arrays.asList(cat1, cat2));

		final String smsContent = "Hi DJ, I like your show...";
		final String senderMsisdn = "12345678912";
		SmsMessage message = createSmsMessage(smsContent, senderMsisdn, "9696");

		mockContext.checking(new Expectations() {
			{

				allowing(messageDispatcher).dispatchIndividualMessages(with(any(String.class)),
						with(allOf(smsMessageWithSubkeyword(senderMsisdn), smsMessageWithContent(responseMsg))));

			}
		});

		CommandEvent<Message> commandEvent = new CommandEvent<Message>(message, new MessageContext());
		commandEvent.setSubKeyword("mr");

		requestService.onDataMessage(commandEvent);
	}

	@Test
	public void testOnDataMessage_ForAppWithSubKeyword_ForInvalidSubkey() throws Exception {
		SubCategory cat1 = new SubCategory();
		cat1.setKeyword("mr");
		cat1.addDescription(new LocalizedMessage("Mahinda", Locale.ENGLISH));
		SubCategory cat2 = new SubCategory();
		cat2.setKeyword("sf");
		cat2.addDescription(new LocalizedMessage("Sarath", Locale.ENGLISH));
		requestService.setSupportedSubCategories(Arrays.asList(cat1, cat2));

		final String smsContent = "Hi DJ, I like your show...";
		final String senderMsisdn = "12345678912";
		SmsMessage message = createSmsMessage(smsContent, senderMsisdn, "9696");

		mockContext.checking(new Expectations() {
			{

				allowing(messageDispatcher).dispatchIndividualMessages(with(any(String.class)),
						with(allOf(smsMessageWithSubkeyword(senderMsisdn), smsMessageWithContent(responseMsg))));

			}
		});

		CommandEvent<Message> commandEvent = new CommandEvent<Message>(message, new MessageContext());
		commandEvent.setSubKeyword("invalid-key");
		try {
			requestService.onDataMessage(commandEvent);
			fail("should fail with subkeyword not found error.");
		} catch (ApplicationException e) {
			assertEquals(MessageFlowErrorReasons.SUB_KEYWORD_NOT_FOUND, e.getReason());
		}
	}

	private SmsMessage createSmsMessage(String msg, String sender, String receiver) {
		SmsMessage message = new SmsMessage();
		message.setMessage(msg);
		message.setSenderAddress(new Msisdn(sender, "Op1"));
		message.addReceiverAddress(new Msisdn(receiver, "Op1"));
		return message;
	}
}
