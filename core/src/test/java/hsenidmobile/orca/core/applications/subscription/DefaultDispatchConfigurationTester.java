/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.applications.subscription;

import static org.junit.Assert.*;
import hsenidmobile.orca.core.applications.PeriodUnit;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

/**
 * $LastChangedDate$
 * $LastChangedBy$ $LastChangedRevision$
 *
 */
public class DefaultDispatchConfigurationTester {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testCreateConfigurationForHour() throws Exception {
		DefaultDispatchConfiguration configuration = DefaultDispatchConfiguration.configureForHour(30);

		assertEquals(30, configuration.getMinuteOfHour());
		assertEquals(0, configuration.getHourOfDay());
		assertEquals(0, configuration.getDayOfWeek());
		assertEquals(0, configuration.getDayOfMonth());
		assertEquals(false, configuration.isUseLastDayOfMonth());
	}

	@Test
	public void testCreateConfigurationForDay() throws Exception {
		DefaultDispatchConfiguration configuration = DefaultDispatchConfiguration.configureForDay(8, 30);

		assertEquals(8, configuration.getHourOfDay());
		assertEquals(30, configuration.getMinuteOfHour());
		assertEquals(0, configuration.getDayOfWeek());
		assertEquals(0, configuration.getDayOfMonth());
		assertEquals(false, configuration.isUseLastDayOfMonth());
	}

	@Test
	public void testCreateConfigurationForWeek() throws Exception {
		DefaultDispatchConfiguration configuration = DefaultDispatchConfiguration.configureForWeek(1, 8, 30);
		assertEquals(30, configuration.getMinuteOfHour());
		assertEquals(8, configuration.getHourOfDay());
		assertEquals(1, configuration.getDayOfWeek());
		assertEquals(0, configuration.getDayOfMonth());
		assertEquals(false, configuration.isUseLastDayOfMonth());
	}

	@Test
	public void testCreateConfigurationForLastDayOfMonth() throws Exception {
		DefaultDispatchConfiguration configuration = DefaultDispatchConfiguration.configureForLastDayOfMonth(8, 30);
		assertEquals(30, configuration.getMinuteOfHour());
		assertEquals(8, configuration.getHourOfDay());
		assertEquals(0, configuration.getDayOfWeek());
		assertEquals(0, configuration.getDayOfMonth());
		assertEquals(true, configuration.isUseLastDayOfMonth());
	}

	@Test
	public void testCreateConfigurationForDayOfMonth() throws Exception {
		DefaultDispatchConfiguration configuration = DefaultDispatchConfiguration.configureForDayOfMonth(15, 8, 30);
		assertEquals(30, configuration.getMinuteOfHour());
		assertEquals(8, configuration.getHourOfDay());
		assertEquals(0, configuration.getDayOfWeek());
		assertEquals(15, configuration.getDayOfMonth());
		assertEquals(false, configuration.isUseLastDayOfMonth());
	}

	@Test
	public void isMatchWithConfig_forHour() throws Exception {
		DefaultDispatchConfiguration config = DefaultDispatchConfiguration.configureForHour(10);
		assertTrue("Should match", config.isMatchWithConfig(PeriodUnit.HOUR, new DateTime(2010, 1, 1, 10, 10, 0, 0)));
		assertFalse("Should not match",
				config.isMatchWithConfig(PeriodUnit.HOUR, new DateTime(2010, 1, 1, 10, 05, 0, 0)));
		assertFalse("Should not match",
				config.isMatchWithConfig(PeriodUnit.HOUR, new DateTime(2010, 1, 1, 10, 40, 0, 0)));
	}

	@Test
	public void isMatchWithConfig_forDay() throws Exception {
		DefaultDispatchConfiguration config = DefaultDispatchConfiguration.configureForDay(8, 30);
		assertTrue("Should match", config.isMatchWithConfig(PeriodUnit.DAY, new DateTime(2010, 1, 1, 8, 30, 0, 0)));
		assertFalse("Should not match", config.isMatchWithConfig(PeriodUnit.DAY, new DateTime(2010, 1, 1, 8, 05, 0, 0)));
		assertFalse("Should not match",
				config.isMatchWithConfig(PeriodUnit.DAY, new DateTime(2010, 1, 1, 10, 40, 0, 0)));
	}

	@Test
	public void isMatchWithConfig_forWeek() throws Exception {
		//@formatter:off
		/* 2010 -Aug/Sep
		 * 7	1	2	3	4	5	6
		 * Su	M	T	W	T	F	Sa
		 * 15	16	17	18	19	20	21
		 * 22	23	24	25	26	27	28
		 * 29	30	31	1	2	3	4
		 * 5	6	7	8	9	10	11
		*/
		//@formatter:on
		DefaultDispatchConfiguration config = DefaultDispatchConfiguration.configureForWeek(2, 4, 00);
		assertTrue("Should match", config.isMatchWithConfig(PeriodUnit.WEEK, new DateTime(2010, 8, 24, 4, 0, 0, 0)));
		assertFalse("Should not match",
				config.isMatchWithConfig(PeriodUnit.WEEK, new DateTime(2010, 8, 24, 8, 05, 0, 0)));
		assertFalse("Should not match",
				config.isMatchWithConfig(PeriodUnit.WEEK, new DateTime(2010, 8, 23, 4, 0, 0, 0)));
		assertFalse("Should not match",
				config.isMatchWithConfig(PeriodUnit.WEEK, new DateTime(2010, 1, 1, 10, 40, 0, 0)));
	}

	@Test
	public void isMatchWithConfig_forDayOfMonth() throws Exception {
		DefaultDispatchConfiguration config = DefaultDispatchConfiguration.configureForDayOfMonth(12, 8, 30);
		assertTrue("Should match", config.isMatchWithConfig(PeriodUnit.MONTH, new DateTime(2010, 1, 12, 8, 30, 0, 0)));
		assertFalse("Should not match",
				config.isMatchWithConfig(PeriodUnit.MONTH, new DateTime(2010, 1, 12, 8, 05, 0, 0)));
		assertFalse("Should not match",
				config.isMatchWithConfig(PeriodUnit.MONTH, new DateTime(2010, 1, 1, 10, 40, 0, 0)));
	}

	@Test
	public void isMatchWithConfig_forLastDayOfMonth() throws Exception {
		DefaultDispatchConfiguration config = DefaultDispatchConfiguration.configureForLastDayOfMonth(10, 0);
		assertTrue("Should match", config.isMatchWithConfig(PeriodUnit.MONTH, new DateTime(2010, 1, 31, 10, 0, 0, 0)));
		assertTrue("Should match", config.isMatchWithConfig(PeriodUnit.MONTH, new DateTime(2010, 2, 28, 10, 0, 0, 0)));
		assertTrue("Should match", config.isMatchWithConfig(PeriodUnit.MONTH, new DateTime(2012, 2, 29, 10, 0, 0, 0)));
		assertFalse("Should not match",
				config.isMatchWithConfig(PeriodUnit.MONTH, new DateTime(2012, 2, 28, 10, 0, 0, 0)));
		assertFalse("Should not match",
				config.isMatchWithConfig(PeriodUnit.MONTH, new DateTime(2010, 1, 12, 8, 05, 0, 0)));
		assertFalse("Should not match",
				config.isMatchWithConfig(PeriodUnit.MONTH, new DateTime(2010, 3, 30, 10, 0, 0, 0)));
		assertFalse("Should not match",
				config.isMatchWithConfig(PeriodUnit.MONTH, new DateTime(2010, 1, 1, 10, 40, 0, 0)));
	}

	@Test
	public void testAdjustToConfig_forHour() throws Exception {
		DefaultDispatchConfiguration config = DefaultDispatchConfiguration.configureForHour(10);
		assertEquals("Should match", new DateTime(2010, 1, 1, 10, 10, 0, 0),
				config.adjustToConfig(PeriodUnit.HOUR, new DateTime(2010, 1, 1, 10, 10, 0, 0)));
		assertEquals("Should match", new DateTime(2010, 1, 1, 10, 10, 0, 0),
				config.adjustToConfig(PeriodUnit.HOUR, new DateTime(2010, 1, 1, 10, 40, 0, 0)));
		assertEquals("Should match", new DateTime(2010, 1, 1, 8, 10, 0, 0),
				config.adjustToConfig(PeriodUnit.HOUR, new DateTime(2010, 1, 1, 8, 5, 0, 0)));

	}

	@Test
	public void testAdjustToConfig_forDay() throws Exception {
		DefaultDispatchConfiguration config = DefaultDispatchConfiguration.configureForDay(8, 30);
		assertEquals("Should match", new DateTime(2010, 1, 1, 8, 30, 0, 0),
				config.adjustToConfig(PeriodUnit.DAY, new DateTime(2010, 1, 1, 8, 30, 0, 0)));
		assertEquals("Should match", new DateTime(2010, 1, 1, 8, 30, 0, 0),
				config.adjustToConfig(PeriodUnit.DAY, new DateTime(2010, 1, 1, 10, 40, 0, 0)));
		assertEquals("Should match", new DateTime(2010, 1, 2, 8, 30, 0, 0),
				config.adjustToConfig(PeriodUnit.DAY, new DateTime(2010, 1, 2, 8, 5, 0, 0)));

	}

	@Test
	public void testAdjustToConfig_forWeek() throws Exception {
		//@formatter:off
		/* 2010 -Aug/Sep
		 * 7	1	2	3	4	5	6
		 * Su	M	T	W	T	F	Sa
		 * 15	16	17	18	19	20	21
		 * 22	23	24	25	26	27	28
		 * 29	30	31	1	2	3	4
		 * 5	6	7	8	9	10	11
		*/
		//@formatter:on
		DefaultDispatchConfiguration config = DefaultDispatchConfiguration.configureForWeek(2, 8, 30);
		assertEquals("Should match", new DateTime(2010, 8, 17, 8, 30, 0, 0),
				config.adjustToConfig(PeriodUnit.WEEK, new DateTime(2010, 8, 17, 8, 30, 0, 0)));
		assertEquals("Should match", new DateTime(2010, 8, 17, 8, 30, 0, 0),
				config.adjustToConfig(PeriodUnit.WEEK, new DateTime(2010, 8, 16, 10, 40, 0, 0)));
		assertEquals("Should match", new DateTime(2010, 8, 24, 8, 30, 0, 0),
				config.adjustToConfig(PeriodUnit.WEEK, new DateTime(2010, 8, 25, 8, 5, 0, 0)));
		assertEquals("Should match", new DateTime(2010, 8, 24, 8, 30, 0, 0),
				config.adjustToConfig(PeriodUnit.WEEK, new DateTime(2010, 8, 28, 8, 5, 0, 0)));
		assertEquals("Should match", new DateTime(2010, 8, 31, 8, 30, 0, 0),
				config.adjustToConfig(PeriodUnit.WEEK, new DateTime(2010, 8, 30, 8, 5, 0, 0)));

	}


	@Test
	public void testAdjustToConfig_forDayOfMonth() throws Exception {
		DefaultDispatchConfiguration config = DefaultDispatchConfiguration.configureForDayOfMonth(15, 8, 30);
		assertEquals("Should match", new DateTime(2010, 1, 15, 8, 30, 0, 0),
				config.adjustToConfig(PeriodUnit.MONTH, new DateTime(2010, 1, 15, 8, 30, 0, 0)));
		assertEquals("Should match", new DateTime(2010, 1, 15, 8, 30, 0, 0),
				config.adjustToConfig(PeriodUnit.MONTH, new DateTime(2010, 1, 2, 10, 40, 0, 0)));
		assertEquals("Should match", new DateTime(2010, 1, 15, 8, 30, 0, 0),
				config.adjustToConfig(PeriodUnit.MONTH, new DateTime(2010, 1, 20, 8, 5, 0, 0)));


		config = DefaultDispatchConfiguration.configureForDayOfMonth(31, 8, 30);
		assertEquals("Should match", new DateTime(2010, 1, 31, 8, 30, 0, 0),
				config.adjustToConfig(PeriodUnit.MONTH, new DateTime(2010, 1, 15, 8, 30, 0, 0)));
		assertEquals("Should match", new DateTime(2010, 2, 28, 8, 30, 0, 0),
				config.adjustToConfig(PeriodUnit.MONTH, new DateTime(2010, 2, 15, 8, 30, 0, 0)));


	}


	@Test
	public void testAdjustToConfig_forLastDayOfMonth() throws Exception {
		DefaultDispatchConfiguration config = DefaultDispatchConfiguration.configureForLastDayOfMonth(8, 30);
		assertEquals("Should match", new DateTime(2010, 1, 31, 8, 30, 0, 0),
				config.adjustToConfig(PeriodUnit.MONTH, new DateTime(2010, 1, 15, 8, 30, 0, 0)));
		assertEquals("Should match", new DateTime(2010, 2, 28, 8, 30, 0, 0),
				config.adjustToConfig(PeriodUnit.MONTH, new DateTime(2010, 2, 2, 10, 40, 0, 0)));
		assertEquals("Should match", new DateTime(2010, 4, 30, 8, 30, 0, 0),
				config.adjustToConfig(PeriodUnit.MONTH, new DateTime(2010, 4, 20, 8, 5, 0, 0)));
		assertEquals("Should match", new DateTime(2012, 2, 29, 8, 30, 0, 0),
				config.adjustToConfig(PeriodUnit.MONTH, new DateTime(2012, 2, 20, 8, 5, 0, 0)));



	}

}
