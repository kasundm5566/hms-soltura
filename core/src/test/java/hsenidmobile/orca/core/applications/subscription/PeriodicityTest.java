/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.applications.subscription;

import static org.junit.Assert.assertEquals;
import hsenidmobile.orca.core.applications.PeriodUnit;
import hsenidmobile.orca.core.testutil.TestUtil;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * $LastChangedDate$
 * $LastChangedBy$ $LastChangedRevision$
 *
 */
public class PeriodicityTest {

	/**
	 * @throws java.lang.Exception
	 */
    private static final Logger logger = LoggerFactory.getLogger(PeriodicityTest.class);
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * Test method for
	 * {@link hsenidmobile.orca.core.applications.subscription.Periodicity#getNextScheduledDispatchTime(org.joda.time.DateTime)}
	 * .
	 */
	@Test
	public void testGetNextScheduledDispatchTimeCurrentTimeAfterLastDispatchTimeForUnitHour() {
		Periodicity periodicity = new Periodicity(PeriodUnit.HOUR, 1, 10);
		periodicity.setFirstScheduledDispatchTime(new DateTime(2010, 8, 22, 8, 00, 0, 0));

		Map<DateTime, DateTime> testData = new HashMap<DateTime, DateTime>();
		// Input - Expected Output
		testData.put(new DateTime(2010, 8, 22, 8, 00, 0, 0), new DateTime(2010, 8, 22, 8, 00, 0, 0));
		testData.put(new DateTime(2010, 8, 22, 8, 5, 0, 0), new DateTime(2010, 8, 22, 9, 00, 0, 0));
		testData.put(new DateTime(2010, 8, 22, 8, 10, 0, 0), new DateTime(2010, 8, 22, 9, 00, 0, 0));
		testData.put(new DateTime(2010, 8, 22, 8, 20, 0, 0), new DateTime(2010, 8, 22, 9, 00, 0, 0));
		testData.put(new DateTime(2010, 8, 22, 8, 50, 0, 0), new DateTime(2010, 8, 22, 9, 00, 0, 0));
		testData.put(new DateTime(2010, 8, 22, 9, 5, 0, 0), new DateTime(2010, 8, 22, 10, 00, 0, 0));
		testData.put(new DateTime(2010, 8, 22, 23, 50, 0, 0), new DateTime(2010, 8, 23, 00, 00, 0, 0));

		Iterator<Entry<DateTime, DateTime>> iterator = testData.entrySet().iterator();
		DateTime nextSchedule;
		while (iterator.hasNext()) {
			Entry<DateTime, DateTime> data = iterator.next();
			nextSchedule = periodicity.getNextScheduledDispatchTime(data.getKey());
			assertEquals("For input:" + data.getKey(), data.getValue(), nextSchedule);
		}

		// TODO: Periodicity with multi out variance not yet supported
		// periodicity = new Periodicity(PeriodUnit.HOUR, 6, 10);
		// periodicity.setLastScheduledSentTime(new DateTime(2010, 8, 22, 12,
		// 00, 0, 0).getMillis());
		//
		// nextSchedule = periodicity.getNextScheduledDispatchTime(new
		// DateTime(2010, 8, 25, 9, 00, 0, 0));
		// assertEquals(new DateTime(2010, 8, 25, 12, 00, 0, 0), nextSchedule);

	}

	/**
	 * Test method for
	 * {@link hsenidmobile.orca.core.applications.subscription.Periodicity#getNextScheduledDispatchTime(org.joda.time.DateTime)}
	 * .
	 */
	@Test
	public void testGetNextScheduledDispatchTimeCurrentTimeAfterLastDispatchTimeForUnitDay() {
		Periodicity periodicity = new Periodicity(PeriodUnit.DAY, 1, 60 * 6);
		periodicity.setFirstScheduledDispatchTime(new DateTime(2010, 8, 22, 12, 00, 0, 0));

		Map<DateTime, DateTime> testData = new HashMap<DateTime, DateTime>();
		// Input - Expected Output
		testData.put(new DateTime(2010, 8, 22, 12, 00, 0, 0), new DateTime(2010, 8, 22, 12, 00, 0, 0));
		testData.put(new DateTime(2010, 8, 22, 13, 0, 0, 0), new DateTime(2010, 8, 23, 12, 00, 0, 0));
		testData.put(new DateTime(2010, 8, 22, 19, 10, 0, 0), new DateTime(2010, 8, 23, 12, 00, 0, 0));
		testData.put(new DateTime(2010, 8, 23, 10, 20, 0, 0), new DateTime(2010, 8, 23, 12, 00, 0, 0));
		testData.put(new DateTime(2010, 8, 31, 13, 20, 0, 0), new DateTime(2010, 9, 1, 12, 00, 0, 0));
		testData.put(new DateTime(2010, 8, 31, 18, 20, 0, 0), new DateTime(2010, 9, 1, 12, 00, 0, 0));

		Iterator<Entry<DateTime, DateTime>> iterator = testData.entrySet().iterator();
		DateTime nextSchedule;
		while (iterator.hasNext()) {
			Entry<DateTime, DateTime> data = iterator.next();
			nextSchedule = periodicity.getNextScheduledDispatchTime(data.getKey());
			assertEquals("For input:" + data.getKey(), data.getValue(), nextSchedule);
		}

		// periodicity = new Periodicity(PeriodUnit.DAY, 2, 60 * 6);
		// periodicity.setLastScheduledSentTime(new DateTime(2010, 8, 22, 12,
		// 00, 0, 0).getMillis());
		//
		// nextSchedule = periodicity.getNextScheduledDispatchTime(new
		// DateTime(2010, 8, 22, 13, 00, 0, 0));
		// assertEquals(new DateTime(2010, 8, 24, 12, 00, 0, 0), nextSchedule);
	}

	/**
	 * Test method for
	 * {@link hsenidmobile.orca.core.applications.subscription.Periodicity#getNextScheduledDispatchTime(org.joda.time.DateTime)}
	 * .
	 */
	@Test
	public void testGetNextScheduledDispatchTimeCurrentTimeAfterLastDispatchTimeForUnitWeek() {
		Periodicity periodicity = new Periodicity(PeriodUnit.WEEK, 1, 60 * 48);// two
																				// day
																				// buffer
		periodicity.setFirstScheduledDispatchTime(new DateTime(2010, 8, 22, 12, 00, 0, 0));

		//@formatter:off
		/* 2010 -Aug/Sep
		 * 7	1	2	3	4	5	6
		 * Su	M	T	W	T	F	Sa
		 * 15	16	17	18	19	20	21
		 * 22	23	24	25	26	27	28
		 * 29	30	31	1	2	3	4
		 * 5	6	7	8	9	10	11
		*/
		//@formatter:on

		Map<DateTime, DateTime> testData = new HashMap<DateTime, DateTime>();
		// Input - Expected Output
		testData.put(new DateTime(2010, 8, 22, 10, 00, 0, 0), new DateTime(2010, 8, 22, 12, 00, 0, 0));
		testData.put(new DateTime(2010, 8, 23, 13, 0, 0, 0), new DateTime(2010, 8, 29, 12, 00, 0, 0));
		testData.put(new DateTime(2010, 8, 26, 13, 0, 0, 0), new DateTime(2010, 8, 29, 12, 00, 0, 0));

		Iterator<Entry<DateTime, DateTime>> iterator = testData.entrySet().iterator();
		DateTime nextSchedule;
		while (iterator.hasNext()) {
			Entry<DateTime, DateTime> data = iterator.next();
			nextSchedule = periodicity.getNextScheduledDispatchTime(data.getKey());
			assertEquals("For input[" + data.getKey() + "]", data.getValue(), nextSchedule);
		}

		// periodicity = new Periodicity(PeriodUnit.WEEK, 2, 60 * 6);
		// periodicity.setLastScheduledSentTime(new DateTime(2010, 8, 22, 12,
		// 00, 0, 0).getMillis());
		//
		// nextSchedule = periodicity.getNextScheduledDispatchTime(new
		// DateTime(2010, 8, 22, 13, 00, 0, 0));
		// assertEquals(new DateTime(2010, 9, 5, 12, 00, 0, 0), nextSchedule);

	}

	/**
	 * Test method for
	 * {@link hsenidmobile.orca.core.applications.subscription.Periodicity#getNextScheduledDispatchTime(org.joda.time.DateTime)}
	 * .
	 */
	@Test
	public void testGetNextScheduledDispatchTimeCurrentTimeAfterLastDispatchTimeForUnitMonth() {
		Periodicity periodicity = new Periodicity(PeriodUnit.MONTH, 1, 60 * 48);// two
																				// day
																				// buffer
		DefaultDispatchConfiguration dispatchConfig = DefaultDispatchConfiguration.configureForDayOfMonth(22, 12, 0);
		periodicity.setDefaultDispatchConfiguration(dispatchConfig);
		periodicity.setFirstScheduledDispatchTime(new DateTime(2010, 8, 22, 12, 00, 0, 0));

		Map<DateTime, DateTime> testData = new HashMap<DateTime, DateTime>();
		// Input - Expected Output
		testData.put(new DateTime(2010, 8, 22, 12, 00, 0, 0), new DateTime(2010, 8, 22, 12, 00, 0, 0));
		testData.put(new DateTime(2010, 8, 22, 13, 00, 0, 0), new DateTime(2010, 9, 22, 12, 00, 0, 0));
		testData.put(new DateTime(2010, 8, 26, 13, 0, 0, 0), new DateTime(2010, 9, 22, 12, 00, 0, 0));

		Iterator<Entry<DateTime, DateTime>> iterator = testData.entrySet().iterator();
		DateTime nextSchedule;
		while (iterator.hasNext()) {
			Entry<DateTime, DateTime> data = iterator.next();
			nextSchedule = periodicity.getNextScheduledDispatchTime(data.getKey());
			assertEquals("For input:" + data.getKey(), data.getValue(), nextSchedule);
		}

		// periodicity = new Periodicity(PeriodUnit.MONTH, 2, 60 * 6);
		// periodicity.setLastScheduledSentTime(new DateTime(2010, 8, 22, 12,
		// 00, 0, 0).getMillis());
		//
		// nextSchedule = periodicity.getNextScheduledDispatchTime(new
		// DateTime(2010, 8, 22, 13, 00, 0, 0));
		// assertEquals(new DateTime(2010, 10, 22, 12, 00, 0, 0), nextSchedule);

	}

	/**
	 * Test method for
	 * {@link hsenidmobile.orca.core.applications.subscription.Periodicity#getNextScheduledDispatchTime(org.joda.time.DateTime)}
	 * .
	 */
	@Test
	public void testGetNextScheduledDispatchTimeCurrentTimeAfterLastDispatchTimeForUnitMonthForDayOfMonth31() {
		Periodicity periodicity = new Periodicity(PeriodUnit.MONTH, 1, 60 * 48);// two
																				// day
																				// buffer
		DefaultDispatchConfiguration dispatchConfig = DefaultDispatchConfiguration.configureForDayOfMonth(31, 12, 0);
		periodicity.setDefaultDispatchConfiguration(dispatchConfig);
		periodicity.setFirstScheduledDispatchTime(new DateTime(2010, 9, 30, 12, 00, 0, 0)); // Sep:30
																							// since
																							// Sep
																							// do
																							// not
																							// have
																							// 31
																							// days

		Map<DateTime, DateTime> testData = new HashMap<DateTime, DateTime>();
		// Input - Expected Output
		testData.put(new DateTime(2010, 9, 22, 12, 00, 0, 0), new DateTime(2010, 9, 30, 12, 00, 0, 0));
		testData.put(new DateTime(2010, 10, 22, 13, 00, 0, 0), new DateTime(2010, 10, 31, 12, 00, 0, 0));
		testData.put(new DateTime(2010, 11, 26, 13, 0, 0, 0), new DateTime(2010, 11, 30, 12, 00, 0, 0));
		testData.put(new DateTime(2010, 12, 26, 13, 0, 0, 0), new DateTime(2010, 12, 31, 12, 00, 0, 0));
		testData.put(new DateTime(2011, 1, 26, 13, 0, 0, 0), new DateTime(2011, 1, 31, 12, 00, 0, 0));
		testData.put(new DateTime(2011, 2, 26, 13, 0, 0, 0), new DateTime(2011, 2, 28, 12, 00, 0, 0));
		testData.put(new DateTime(2011, 3, 26, 13, 0, 0, 0), new DateTime(2011, 3, 31, 12, 00, 0, 0));
		testData.put(new DateTime(2012, 2, 2, 13, 0, 0, 0), new DateTime(2012, 2, 29, 12, 00, 0, 0));

		Iterator<Entry<DateTime, DateTime>> iterator = testData.entrySet().iterator();
		DateTime nextSchedule;
		while (iterator.hasNext()) {
			Entry<DateTime, DateTime> data = iterator.next();
			nextSchedule = periodicity.getNextScheduledDispatchTime(data.getKey());
			assertEquals("For input:" + data.getKey(), data.getValue(), nextSchedule);
		}

		// periodicity = new Periodicity(PeriodUnit.MONTH, 2, 60 * 6);
		// periodicity.setLastScheduledSentTime(new DateTime(2010, 8, 22, 12,
		// 00, 0, 0).getMillis());
		//
		// nextSchedule = periodicity.getNextScheduledDispatchTime(new
		// DateTime(2010, 8, 22, 13, 00, 0, 0));
		// assertEquals(new DateTime(2010, 10, 22, 12, 00, 0, 0), nextSchedule);

	}

	/**
	 * Test method for
	 * {@link hsenidmobile.orca.core.applications.subscription.Periodicity#getNextScheduledDispatchTime(org.joda.time.DateTime)}
	 * .
	 */
	@Test
	public void testGetNextScheduledDispatchTimeForLastDayOfMonthConfigurationStarting31DayMonth() {
		Periodicity periodicity = new Periodicity(PeriodUnit.MONTH, 1, 60 * 48);// two
																				// day
																				// buffer
		DefaultDispatchConfiguration dispatchConfig = DefaultDispatchConfiguration.configureForLastDayOfMonth(8, 30);
		periodicity.setDefaultDispatchConfiguration(dispatchConfig);

		periodicity.setFirstScheduledDispatchTime(new DateTime(2010, 7, 31, 8, 30, 0, 0));

		Map<DateTime, DateTime> testData = new HashMap<DateTime, DateTime>();
		// Input - Expected Output
		testData.put(new DateTime(2010, 8, 22, 12, 00, 0, 0), new DateTime(2010, 8, 31, 8, 30, 0, 0));
		testData.put(new DateTime(2010, 8, 22, 13, 00, 0, 0), new DateTime(2010, 8, 31, 8, 30, 0, 0));
		testData.put(new DateTime(2010, 8, 26, 13, 0, 0, 0), new DateTime(2010, 8, 31, 8, 30, 0, 0));
		testData.put(new DateTime(2010, 9, 8, 13, 0, 0, 0), new DateTime(2010, 9, 30, 8, 30, 0, 0));

		Iterator<Entry<DateTime, DateTime>> iterator = testData.entrySet().iterator();
		DateTime nextSchedule;
		while (iterator.hasNext()) {
			Entry<DateTime, DateTime> data = iterator.next();
			nextSchedule = periodicity.getNextScheduledDispatchTime(data.getKey());
			assertEquals("For input: " + data.getKey(), data.getValue(), nextSchedule);
		}

	}

	/**
	 * Test method for
	 * {@link hsenidmobile.orca.core.applications.subscription.Periodicity#getNextScheduledDispatchTime(org.joda.time.DateTime)}
	 * .
	 */
	@Test
	public void testGetNextScheduledDispatchTimeForLastDayOfMonthConfigurationStarting30DayMonth() {
		Periodicity periodicity = new Periodicity(PeriodUnit.MONTH, 1, 60 * 48);// two
																				// day
																				// buffer
		DefaultDispatchConfiguration dispatchConfig = DefaultDispatchConfiguration.configureForLastDayOfMonth(8, 30);
		periodicity.setDefaultDispatchConfiguration(dispatchConfig);

		periodicity.setFirstScheduledDispatchTime(new DateTime(2010, 6, 30, 8, 30, 0, 0));

		Map<DateTime, DateTime> testData = new HashMap<DateTime, DateTime>();
		// Input - Expected Output
		testData.put(new DateTime(2010, 7, 22, 12, 00, 0, 0), new DateTime(2010, 7, 31, 8, 30, 0, 0));
		testData.put(new DateTime(2010, 8, 22, 12, 00, 0, 0), new DateTime(2010, 8, 31, 8, 30, 0, 0));
		testData.put(new DateTime(2010, 8, 22, 13, 00, 0, 0), new DateTime(2010, 8, 31, 8, 30, 0, 0));
		testData.put(new DateTime(2010, 8, 26, 13, 0, 0, 0), new DateTime(2010, 8, 31, 8, 30, 0, 0));
		testData.put(new DateTime(2010, 9, 8, 13, 0, 0, 0), new DateTime(2010, 9, 30, 8, 30, 0, 0));

		Iterator<Entry<DateTime, DateTime>> iterator = testData.entrySet().iterator();
		DateTime nextSchedule;
		while (iterator.hasNext()) {
			Entry<DateTime, DateTime> data = iterator.next();
			nextSchedule = periodicity.getNextScheduledDispatchTime(data.getKey());
			assertEquals("For input: " + data.getKey(), data.getValue(), nextSchedule);
		}

	}

	/**
	 * Test method for
	 * {@link hsenidmobile.orca.core.applications.subscription.Periodicity#getNextScheduledDispatchTime(org.joda.time.DateTime)}
	 * .
	 */
	@Test
	public void testGetNextScheduleTimeBasedOnLastScheduleTimeForLastDayOfMonthConfiguration() {
		Periodicity periodicity = new Periodicity(PeriodUnit.MONTH, 1, 60 * 48);// two
																				// day
																				// buffer
		DefaultDispatchConfiguration dispatchConfig = DefaultDispatchConfiguration.configureForLastDayOfMonth(8, 30);
		periodicity.setDefaultDispatchConfiguration(dispatchConfig);

		periodicity.setFirstScheduledDispatchTime(new DateTime(2010, 8, 31, 8, 30, 0, 0));

		Map<DateTime, DateTime> testData = new HashMap<DateTime, DateTime>();
		// Input - Expected Output
		testData.put(new DateTime(2010, 8, 31, 8, 30, 0, 0), new DateTime(2010, 9, 30, 8, 30, 0, 0));
		testData.put(new DateTime(2010, 9, 30, 8, 30, 0, 0), new DateTime(2010, 10, 31, 8, 30, 0, 0));
		testData.put(new DateTime(2010, 10, 31, 8, 30, 0, 0), new DateTime(2010, 11, 30, 8, 30, 0, 0));
		testData.put(new DateTime(2010, 11, 30, 8, 30, 0, 0), new DateTime(2010, 12, 31, 8, 30, 0, 0));
		testData.put(new DateTime(2010, 12, 31, 8, 30, 0, 0), new DateTime(2011, 1, 31, 8, 30, 0, 0));
		testData.put(new DateTime(2011, 1, 31, 8, 30, 0, 0), new DateTime(2011, 2, 28, 8, 30, 0, 0));
		testData.put(new DateTime(2012, 1, 31, 8, 30, 0, 0), new DateTime(2012, 2, 29, 8, 30, 0, 0));

		Iterator<Entry<DateTime, DateTime>> iterator = testData.entrySet().iterator();
		DateTime nextSchedule;
		while (iterator.hasNext()) {
			Entry<DateTime, DateTime> data = iterator.next();
			nextSchedule = new DateTime(periodicity.getNextScheduleTimeBasedOnLastScheduleTime(data.getKey().toDate()));

            logger.info(data.getKey() + " : " + nextSchedule);
			assertEquals("For input:" + data.getKey(), data.getValue(), nextSchedule);
		}

	}

	/**
	 * Test method for
	 * {@link hsenidmobile.orca.core.applications.subscription.Periodicity#getNextScheduledDispatchTime(org.joda.time.DateTime)}
	 * .
	 */
	@Test
	public void testGetNextScheduleTimeBasedOnLastScheduleTimeForDayOfMonthConfigurationWithDate31() {
		Periodicity periodicity = new Periodicity(PeriodUnit.MONTH, 1, 60 * 48);// two
																				// day
																				// buffer
		DefaultDispatchConfiguration dispatchConfig = DefaultDispatchConfiguration.configureForDayOfMonth(31, 8, 30);
		periodicity.setDefaultDispatchConfiguration(dispatchConfig);

		periodicity.setFirstScheduledDispatchTime(new DateTime(2010, 8, 31, 8, 30, 0, 0));

		Map<DateTime, DateTime> testData = new HashMap<DateTime, DateTime>();
		// Input - Expected Output
		testData.put(new DateTime(2010, 8, 31, 8, 30, 0, 0), new DateTime(2010, 9, 30, 8, 30, 0, 0));
		testData.put(new DateTime(2010, 9, 30, 8, 30, 0, 0), new DateTime(2010, 10, 31, 8, 30, 0, 0));
		testData.put(new DateTime(2010, 10, 31, 8, 30, 0, 0), new DateTime(2010, 11, 30, 8, 30, 0, 0));
		testData.put(new DateTime(2010, 11, 30, 8, 30, 0, 0), new DateTime(2010, 12, 31, 8, 30, 0, 0));
		testData.put(new DateTime(2010, 12, 31, 8, 30, 0, 0), new DateTime(2011, 1, 31, 8, 30, 0, 0));
		testData.put(new DateTime(2011, 1, 31, 8, 30, 0, 0), new DateTime(2011, 2, 28, 8, 30, 0, 0));
		testData.put(new DateTime(2011, 2, 28, 8, 30, 0, 0), new DateTime(2011, 3, 31, 8, 30, 0, 0));
		testData.put(new DateTime(2012, 1, 31, 8, 30, 0, 0), new DateTime(2012, 2, 29, 8, 30, 0, 0));

		Iterator<Entry<DateTime, DateTime>> iterator = testData.entrySet().iterator();
		DateTime nextSchedule;
		while (iterator.hasNext()) {
			Entry<DateTime, DateTime> data = iterator.next();
			nextSchedule = new DateTime(periodicity.getNextScheduleTimeBasedOnLastScheduleTime(data.getKey().toDate()));
			logger.info(data.getKey() + " : " + nextSchedule);
			assertEquals("For input:" + data.getKey(), data.getValue(), nextSchedule);
		}

	}

	/**
	 * Test method for
	 * {@link hsenidmobile.orca.core.applications.subscription.Periodicity#getNextScheduledDispatchTime(org.joda.time.DateTime)}
	 * .
	 */
	@Test
	public void testGetNextScheduledDispatchTimeCurrentTimeBeforeLastDispatchTime() {
		Periodicity periodicity = new Periodicity(PeriodUnit.DAY, 1, 60 * 6);
		periodicity.setFirstScheduledDispatchTime(new DateTime(2010, 8, 22, 12, 00, 0, 0));
		DateTime nextSchedule = periodicity.getNextScheduledDispatchTime(new DateTime(2010, 8, 21, 13, 00, 0, 0));
		assertEquals(new DateTime(2010, 8, 22, 12, 00, 0, 0), nextSchedule);
	}

	@Test
	public void testGetFistDispatchableDateForPeriodHour() {
		DateTime now = new DateTime(2010, 8, 25, 9, 30, 15, 520);
		TestUtil.setJodaSystemTime(now);

		DefaultDispatchConfiguration dispatchConfiguration = DefaultDispatchConfiguration.configureForHour(0);
		Date fistDispatchableDate = Periodicity.getFistDispatchableDate(PeriodUnit.HOUR, 1, dispatchConfiguration);
		assertEquals(new DateTime(2010, 8, 25, 10, 0, 0, 0).toDate(), fistDispatchableDate);

		dispatchConfiguration = DefaultDispatchConfiguration.configureForHour(10);
		fistDispatchableDate = Periodicity.getFistDispatchableDate(PeriodUnit.HOUR, 1, dispatchConfiguration);
		assertEquals(new DateTime(2010, 8, 25, 10, 10, 0, 0).toDate(), fistDispatchableDate);

		dispatchConfiguration = DefaultDispatchConfiguration.configureForHour(30);
		fistDispatchableDate = Periodicity.getFistDispatchableDate(PeriodUnit.HOUR, 1, dispatchConfiguration);
		assertEquals(new DateTime(2010, 8, 25, 10, 30, 0, 0).toDate(), fistDispatchableDate);

		dispatchConfiguration = DefaultDispatchConfiguration.configureForHour(45);
		fistDispatchableDate = Periodicity.getFistDispatchableDate(PeriodUnit.HOUR, 1, dispatchConfiguration);
		assertEquals(new DateTime(2010, 8, 25, 9, 45, 0, 0).toDate(), fistDispatchableDate);
	}

	@Test
	public void testGetFistDispatchableDateForPeriodDay() {
		DateTime now = new DateTime(2010, 8, 25, 9, 30, 15, 520);
		TestUtil.setJodaSystemTime(now);

		DefaultDispatchConfiguration dispatchConfiguration = DefaultDispatchConfiguration.configureForDay(8, 30);
		Date fistDispatchableDate = Periodicity.getFistDispatchableDate(PeriodUnit.DAY, 1, dispatchConfiguration);
		assertEquals(new DateTime(2010, 8, 26, 8, 30, 0, 0).toDate(), fistDispatchableDate);

		dispatchConfiguration = DefaultDispatchConfiguration.configureForDay(9, 30);
		fistDispatchableDate = Periodicity.getFistDispatchableDate(PeriodUnit.DAY, 1, dispatchConfiguration);
		assertEquals(new DateTime(2010, 8, 26, 9, 30, 0, 0).toDate(), fistDispatchableDate);

		dispatchConfiguration = DefaultDispatchConfiguration.configureForDay(10, 30);
		fistDispatchableDate = Periodicity.getFistDispatchableDate(PeriodUnit.DAY, 1, dispatchConfiguration);
		assertEquals(new DateTime(2010, 8, 25, 10, 30, 0, 0).toDate(), fistDispatchableDate);

	}

	@Test
	public void testGetFistDispatchableDateForPeriodWeek() {

		//@formatter:off
		/* 2010 -Aug/Sep
		 * 7	1	2	3	4	5	6
		 * Su	M	T	W	T	F	Sa
		 * 15	16	17	18	19	20	21
		 * 22	23	24	25	26	27	28
		 * 29	30	31	1	2	3	4
		 * 5	6	7	8	9	10	11
		*/
		//@formatter:on

		// today is Wednesday
		DateTime now = new DateTime(2010, 8, 25, 9, 30, 15, 520);
		TestUtil.setJodaSystemTime(now);

		DefaultDispatchConfiguration dispatchConfiguration = DefaultDispatchConfiguration.configureForWeek(
				DateTimeConstants.MONDAY, 8, 30);
		Date fistDispatchableDate = Periodicity.getFistDispatchableDate(PeriodUnit.WEEK, 1, dispatchConfiguration);
		assertEquals(new DateTime(2010, 8, 30, 8, 30, 0, 0).toDate(), fistDispatchableDate);

		dispatchConfiguration = DefaultDispatchConfiguration.configureForWeek(DateTimeConstants.WEDNESDAY, 8, 30);
		fistDispatchableDate = Periodicity.getFistDispatchableDate(PeriodUnit.WEEK, 1, dispatchConfiguration);
		assertEquals(new DateTime(2010, 8, 25, 8, 30, 0, 0).toDate(), fistDispatchableDate);

		dispatchConfiguration = DefaultDispatchConfiguration.configureForWeek(DateTimeConstants.FRIDAY, 8, 30);
		fistDispatchableDate = Periodicity.getFistDispatchableDate(PeriodUnit.WEEK, 1, dispatchConfiguration);
		assertEquals(new DateTime(2010, 8, 27, 8, 30, 0, 0).toDate(), fistDispatchableDate);

		dispatchConfiguration = DefaultDispatchConfiguration.configureForWeek(DateTimeConstants.SUNDAY, 8, 30);
		fistDispatchableDate = Periodicity.getFistDispatchableDate(PeriodUnit.WEEK, 1, dispatchConfiguration);
		assertEquals(new DateTime(2010, 8, 29, 8, 30, 0, 0).toDate(), fistDispatchableDate);

	}

	@Test
	public void testGetFistDispatchableDateForPeriodMonth() {
		DateTime now = new DateTime(2010, 8, 25, 9, 30, 15, 520);
		TestUtil.setJodaSystemTime(now);

		DefaultDispatchConfiguration dispatchConfiguration = DefaultDispatchConfiguration.configureForDayOfMonth(30, 8,
				30);
		Date fistDispatchableDate = Periodicity.getFistDispatchableDate(PeriodUnit.MONTH, 1, dispatchConfiguration);
		assertEquals(new DateTime(2010, 8, 30, 8, 30, 0, 0).toDate(), fistDispatchableDate);

		dispatchConfiguration = DefaultDispatchConfiguration.configureForDayOfMonth(1, 8, 30);
		fistDispatchableDate = Periodicity.getFistDispatchableDate(PeriodUnit.MONTH, 1, dispatchConfiguration);
		assertEquals(new DateTime(2010, 9, 1, 8, 30, 0, 0).toDate(), fistDispatchableDate);

		dispatchConfiguration = DefaultDispatchConfiguration.configureForDayOfMonth(25, 12, 30);
		fistDispatchableDate = Periodicity.getFistDispatchableDate(PeriodUnit.MONTH, 1, dispatchConfiguration);
		assertEquals(new DateTime(2010, 8, 25, 12, 30, 0, 0).toDate(), fistDispatchableDate);

	}

	@Test
	public void testGetFistDispatchableDateForPeriodMonthWhenSelectedDateExceedsNumberOfDayInMonth() {
		DateTime now = new DateTime(2010, 8, 25, 9, 30, 15, 520);
		TestUtil.setJodaSystemTime(now);

		DefaultDispatchConfiguration dispatchConfiguration = DefaultDispatchConfiguration.configureForDayOfMonth(31, 8,
				30);
		Date fistDispatchableDate = Periodicity.getFistDispatchableDate(PeriodUnit.MONTH, 1, dispatchConfiguration);
		assertEquals(new DateTime(2010, 8, 31, 8, 30, 0, 0).toDate(), fistDispatchableDate);

		TestUtil.setJodaSystemTime(new DateTime(2010, 9, 25, 9, 30, 15, 520));
		dispatchConfiguration = DefaultDispatchConfiguration.configureForDayOfMonth(31, 8, 30);
		fistDispatchableDate = Periodicity.getFistDispatchableDate(PeriodUnit.MONTH, 1, dispatchConfiguration);
		assertEquals("should be 30 even though configured for 31 since month Sep do not have 31 days", new DateTime(
				2010, 9, 30, 8, 30, 0, 0).toDate(), fistDispatchableDate);

		TestUtil.setJodaSystemTime(new DateTime(2011, 2, 1, 9, 30, 15, 520));
		dispatchConfiguration = DefaultDispatchConfiguration.configureForDayOfMonth(31, 8, 30);
		fistDispatchableDate = Periodicity.getFistDispatchableDate(PeriodUnit.MONTH, 1, dispatchConfiguration);
		assertEquals("should be 28 even though configured for 31 since month Feb do not have 31 days", new DateTime(
				2011, 2, 28, 8, 30, 0, 0).toDate(), fistDispatchableDate);

	}

	@Test
	public void testGetFistDispatchableDateForPeriodMonthForLastDayOfMonthConfiguration() {
		TestUtil.setJodaSystemTime(new DateTime(2010, 8, 25, 9, 30, 15, 520));
		DefaultDispatchConfiguration dispatchConfiguration = DefaultDispatchConfiguration.configureForLastDayOfMonth(8,
				30);
		Date fistDispatchableDate = Periodicity.getFistDispatchableDate(PeriodUnit.MONTH, 1, dispatchConfiguration);
		assertEquals(new DateTime(2010, 8, 31, 8, 30, 0, 0).toDate(), fistDispatchableDate);

		TestUtil.setJodaSystemTime(new DateTime(2010, 2, 1, 9, 30, 15, 520));
		fistDispatchableDate = Periodicity.getFistDispatchableDate(PeriodUnit.MONTH, 1, dispatchConfiguration);
		assertEquals(new DateTime(2010, 2, 28, 8, 30, 0, 0).toDate(), fistDispatchableDate);

		TestUtil.setJodaSystemTime(new DateTime(2012, 2, 1, 9, 30, 15, 520));
		fistDispatchableDate = Periodicity.getFistDispatchableDate(PeriodUnit.MONTH, 1, dispatchConfiguration);
		assertEquals(new DateTime(2012, 2, 29, 8, 30, 0, 0).toDate(), fistDispatchableDate);

	}

	@Test
	public void testGetNextScheduleTimeBasedOnLastScheduleTime() {
		DateTime lastScheduled = new DateTime(2010, 8, 25, 9, 30, 0, 0);

		Periodicity periodicity = new Periodicity(PeriodUnit.HOUR, 1, 10);
		periodicity.setDefaultDispatchConfiguration(DefaultDispatchConfiguration.configureForHour(30));
		assertEquals(new DateTime(2010, 8, 25, 10, 30, 0, 0).toDate(),
				periodicity.getNextScheduleTimeBasedOnLastScheduleTime(lastScheduled.toDate()));

		periodicity = new Periodicity(PeriodUnit.DAY, 1, 10);
		periodicity.setDefaultDispatchConfiguration(DefaultDispatchConfiguration.configureForDay(9, 30));
		assertEquals(new DateTime(2010, 8, 26, 9, 30, 0, 0).toDate(),
				periodicity.getNextScheduleTimeBasedOnLastScheduleTime(lastScheduled.toDate()));

		//@formatter:off
		/* 2010 -Aug/Sep
		 * 7	1	2	3	4	5	6
		 * Su	M	T	W	T	F	Sa
		 * 15	16	17	18	19	20	21
		 * 22	23	24	25	26	27	28
		 * 29	30	31	1	2	3	4
		 * 5	6	7	8	9	10	11
		*/
		//@formatter:on

		periodicity = new Periodicity(PeriodUnit.WEEK, 1, 10);
		periodicity.setDefaultDispatchConfiguration(DefaultDispatchConfiguration.configureForWeek(3, 9, 30));
		assertEquals(new DateTime(2010, 9, 1, 9, 30, 0, 0).toDate(),
				periodicity.getNextScheduleTimeBasedOnLastScheduleTime(lastScheduled.toDate()));

		periodicity = new Periodicity(PeriodUnit.MONTH, 1, 10);
		periodicity.setDefaultDispatchConfiguration(DefaultDispatchConfiguration.configureForDayOfMonth(25, 9, 30));
		assertEquals(new DateTime(2010, 9, 25, 9, 30, 0, 0).toDate(),
				periodicity.getNextScheduleTimeBasedOnLastScheduleTime(lastScheduled.toDate()));

	}

	@Test
	public void testGetNextScheduleTimeBasedOnLastScheduleTime_WhenLastScheduledTimeDifferFromDispatchConfiguration() {
		/*
		 * ****Scenario**** You already have some scheduled content and you edit
		 * your application and change dispatch configuration. In such situation
		 * any new content should be according to new dispatch configuration.
		 */
		DateTime lastScheduled = new DateTime(2010, 8, 25, 9, 30, 0, 0);

		Periodicity periodicity = new Periodicity(PeriodUnit.HOUR, 1, 10);
		periodicity.setDefaultDispatchConfiguration(DefaultDispatchConfiguration.configureForHour(30));
		assertEquals("when dispatch config match with last scheduled",
				new DateTime(2010, 8, 25, 10, 30, 0, 0).toDate(),
				periodicity.getNextScheduleTimeBasedOnLastScheduleTime(lastScheduled.toDate()));

		periodicity.setDefaultDispatchConfiguration(DefaultDispatchConfiguration.configureForHour(45));
		assertEquals("when dispatch config differ from last scheduled",
				new DateTime(2010, 8, 25, 10, 45, 0, 0).toDate(),
				periodicity.getNextScheduleTimeBasedOnLastScheduleTime(lastScheduled.toDate()));

		periodicity.setDefaultDispatchConfiguration(DefaultDispatchConfiguration.configureForHour(15));
		assertEquals("when dispatch config differ from last scheduled",
				new DateTime(2010, 8, 25, 10, 15, 0, 0).toDate(),
				periodicity.getNextScheduleTimeBasedOnLastScheduleTime(lastScheduled.toDate()));

		// periodicity = new Periodicity(PeriodUnit.DAY, 1, 10);
		// assertEquals(new DateTime(2010, 8, 26, 9, 30, 0, 0).toDate(),
		// periodicity.getNextScheduleTimeBasedOnLastScheduleTime(lastScheduled.toDate()));
		//
//		//@formatter:off
//		/* 2010 -Aug/Sep
//		 * 7	1	2	3	4	5	6
//		 * Su	M	T	W	T	F	Sa
//		 * 15	16	17	18	19	20	21
//		 * 22	23	24	25	26	27	28
//		 * 29	30	31	1	2	3	4
//		 * 5	6	7	8	9	10	11
//		*/
//		//@formatter:on
		//
		// periodicity = new Periodicity(PeriodUnit.WEEK, 1, 10);
		// assertEquals(new DateTime(2010, 9, 1, 9, 30, 0, 0).toDate(),
		// periodicity.getNextScheduleTimeBasedOnLastScheduleTime(lastScheduled.toDate()));
		//
		// periodicity = new Periodicity(PeriodUnit.MONTH, 1, 10);
		// DefaultDispatchConfiguration dispatchConfig =
		// DefaultDispatchConfiguration.configureForDayOfMonth(25, 8, 30);
		// periodicity.setDefaultDispatchConfiguration(dispatchConfig);
		// assertEquals(new DateTime(2010, 9, 25, 9, 30, 0, 0).toDate(),
		// periodicity.getNextScheduleTimeBasedOnLastScheduleTime(lastScheduled.toDate()));

	}
}
