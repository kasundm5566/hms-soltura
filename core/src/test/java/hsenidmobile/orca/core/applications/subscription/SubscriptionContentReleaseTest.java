/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.applications.subscription;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import hsenidmobile.orca.core.applications.PeriodUnit;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

/**
 * $LastChangedDate$ $LastChangedBy$ $LastChangedRevision$
 *
 */
public class SubscriptionContentReleaseTest {
	private SubscriptionContentRelease contentRelease;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		contentRelease = new SubscriptionContentRelease();
	}

	@Test
	public void testCanDispathchedOnForPeriodUnitHour() {
		contentRelease.setScheduledDispatchTimeFrom(new DateTime(2010, 8, 20, 12, 0, 0, 0));
		contentRelease.setPeriodicity(new Periodicity(PeriodUnit.HOUR, 1, 10));

		boolean canDispathchedOn = contentRelease.canDispathchedOn(new DateTime(2010, 8, 20, 11, 30, 0, 0).getMillis());
		assertFalse("Cannot send content on 11:30", canDispathchedOn);

		canDispathchedOn = contentRelease.canDispathchedOn(new DateTime(2010, 8, 20, 12, 00, 0, 0).getMillis());
		assertTrue("Can send content on 12:00", canDispathchedOn);

		canDispathchedOn = contentRelease.canDispathchedOn(new DateTime(2010, 8, 20, 12, 05, 0, 0).getMillis());
		assertTrue("Can send content on 12:05", canDispathchedOn);

		canDispathchedOn = contentRelease.canDispathchedOn(new DateTime(2010, 8, 20, 12, 10, 0, 0).getMillis());
		assertTrue("Can send content on 12:10", canDispathchedOn);

		canDispathchedOn = contentRelease.canDispathchedOn(new DateTime(2010, 8, 20, 12, 50, 0, 0).getMillis());
		assertFalse("Cannot send content on 12:50", canDispathchedOn);

		canDispathchedOn = contentRelease.canDispathchedOn(new DateTime(2010, 8, 21, 12, 05, 0, 0).getMillis());
		assertFalse("Cannot send content on next day 12:05", canDispathchedOn);

		canDispathchedOn = contentRelease.canDispathchedOn(new DateTime(2010, 8, 19, 12, 05, 0, 0).getMillis());
		assertFalse("Cannot send content on previous day 12:05", canDispathchedOn);
	}

	@Test
	public void testCanDispathchedOnForPeriodUnitDay() {
		contentRelease.setScheduledDispatchTimeFrom(new DateTime(2010, 8, 20, 12, 0, 0, 0));
		contentRelease.setPeriodicity(new Periodicity(PeriodUnit.DAY, 1, 10));

		boolean canDispathchedOn = contentRelease.canDispathchedOn(new DateTime(2010, 8, 20, 11, 30, 0, 0).getMillis());
		assertFalse("Cannot send content same day on 11:30", canDispathchedOn);

		canDispathchedOn = contentRelease.canDispathchedOn(new DateTime(2010, 8, 20, 12, 00, 0, 0).getMillis());
		assertTrue("Can send content on same day 12:00", canDispathchedOn);

		canDispathchedOn = contentRelease.canDispathchedOn(new DateTime(2010, 8, 20, 12, 05, 0, 0).getMillis());
		assertTrue("Can send content on same day 12:05", canDispathchedOn);

		canDispathchedOn = contentRelease.canDispathchedOn(new DateTime(2010, 8, 20, 12, 10, 0, 0).getMillis());
		assertTrue("Can send content on same day 12:10", canDispathchedOn);

		canDispathchedOn = contentRelease.canDispathchedOn(new DateTime(2010, 8, 20, 12, 50, 0, 0).getMillis());
		assertFalse("Cannot send content on same day 12:50", canDispathchedOn);

		canDispathchedOn = contentRelease.canDispathchedOn(new DateTime(2010, 8, 21, 12, 05, 0, 0).getMillis());
		assertFalse("Cannot send content on next day 12:05", canDispathchedOn);

		canDispathchedOn = contentRelease.canDispathchedOn(new DateTime(2010, 8, 19, 12, 05, 0, 0).getMillis());
		assertFalse("Cannot send content on previous day 12:05", canDispathchedOn);

	}

}
