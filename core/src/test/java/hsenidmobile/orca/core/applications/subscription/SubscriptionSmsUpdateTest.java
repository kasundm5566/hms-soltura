/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.applications.subscription;

import hsenidmobile.orca.core.applications.PeriodUnit;
import hsenidmobile.orca.core.flow.MessageFlowErrorReasons;
import hsenidmobile.orca.core.flow.MessageFlowException;
import hsenidmobile.orca.core.flow.dispatch.aventuraapi.AventuraApiMtMessageDispatcher;
import hsenidmobile.orca.core.flow.event.CommandEvent;
import hsenidmobile.orca.core.model.*;
import hsenidmobile.orca.core.model.Content.ContentSource;
import hsenidmobile.orca.core.model.message.Message;
import hsenidmobile.orca.core.model.message.MessageContext;
import hsenidmobile.orca.core.model.message.SmsMessage;
import hsenidmobile.orca.core.testutil.TestUtil;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JMock;
import org.joda.time.DateTime;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import static hsenidmobile.orca.core.testutil.AventuraSmsContentMatcher.smsMessageWithContent;
import static hsenidmobile.orca.core.testutil.AventuraSmsSubKeywordMatcher.smsMessageWithSubkeyword;
import static hsenidmobile.orca.core.testutil.ListSizeMatcher.listOfSize;
import static hsenidmobile.orca.core.testutil.SubscriptionContentReleaseMatcher.subsContentWith;
import static org.hamcrest.core.AllOf.allOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Tests the scenarios related to
 * {@link Subscription#onServiceDataUpdateMessage(hsenidmobile.orca.core.flow.event.CommandEvent)}
 *
 * $LastChangedDate$
 * $LastChangedBy$ $LastChangedRevision$
 *
 */
@RunWith(JMock.class)
public class SubscriptionSmsUpdateTest {
	private String successsRespTemplate = "Service Data Updated Successfully and scheduled to be dispatched on %s.";
	private Subscription subscription;
	private Mockery mockContext;
	private AventuraApiMtMessageDispatcher messageDispatcher;
	private Application application;
	private SubscriptionServiceDataRepository subsDataRepo;

	@Before
	public void setUp() {
		mockContext = new Mockery();
		application = mockContext.mock(Application.class);
		messageDispatcher = mockContext.mock(AventuraApiMtMessageDispatcher.class);
		subsDataRepo = mockContext.mock(SubscriptionServiceDataRepository.class);

		subscription = new Subscription();
		subscription.setSmsUpdateRequired(true);
		subscription.setApplication(application);
		subscription.setAventuraApiMtMessageDispatcher(messageDispatcher);
		subscription.setSubscriptionDataRepository(subsDataRepo);

		mockContext.checking(new Expectations() {
			{

				allowing(application).getAppId();
				will(returnValue("APP_ID"));

				allowing(application).getAppName();
				will(returnValue("APP_Name"));
			}
		});

	}

	@After
	public void tearDown() {
		TestUtil.resetCurrentTime();
	}

	private SmsMessage createSmsMessage(String updateTxt) {
		SmsMessage message = new SmsMessage();
		message.setMessage(updateTxt);
		message.setSenderAddress(new Msisdn("776456123", "Op1"));
		message.addReceiverAddress(new Msisdn("9696", "Op1"));
		return message;
	}

	private Content createContent(String content, SubCategory subCategory) {
		Content con1 = new Content();
		con1.setContent(new LocalizedMessage(content, Locale.ENGLISH));
		con1.setSource(ContentSource.WEB);
		con1.setSubCategory(subCategory);

		return con1;
	}

	private SubCategory createSubCategory(String categoryCode) {
		SubCategory cat1 = new SubCategory();
		cat1.setKeyword(categoryCode);
		return cat1;
	}

	private SubscriptionContentRelease createContentRelease(long id, Periodicity periodicity, List<Content> content,
			DateTime scheduledTime) {
		SubscriptionContentRelease scheduledContent1 = new SubscriptionContentRelease();
		scheduledContent1.setId(id);
		scheduledContent1.setContents(content);
		scheduledContent1.setPeriodicity(periodicity);
		scheduledContent1.setScheduledDispatchTimeFrom(scheduledTime);
		scheduledContent1.setScheduledDispatchTimeTo(scheduledContent1.getScheduledDispatchTimeFrom().plusMinutes(
				periodicity.getAllowedBufferTime()));

		return scheduledContent1;
	}

//	@Test
	public void whenNoInitialContentAvailable_ForAppWithoutSubcategory() throws Exception {
		PeriodUnit periodUnit = PeriodUnit.DAY;
		int periodValue = 1;
		int allowedBufferTime = 60 * 6;
		final Periodicity periodicity = new Periodicity(periodUnit, periodValue, allowedBufferTime);
		periodicity.setFirstScheduledDispatchTime(new DateTime(2010, 8, 22, 12, 0, 0, 0));
		subscription.setDefaultPeriodicity(periodicity);
		subscription.setSupportedPeriodicities(Arrays.asList(periodicity));

		final String updateTxt = "MR is going to give a speak at a rally in Maharagama";

		mockContext.checking(new Expectations() {
			{
				exactly(1).of(messageDispatcher).dispatchIndividualMessages(
						with(any(String.class)),
						with(allOf(smsMessageWithSubkeyword("776456123"),
								smsMessageWithContent(String.format(successsRespTemplate, "2010-08-25 12:00:00")))));

				allowing(subsDataRepo).getNextUpdatableContent(with(any(String.class)), with(any(DateTime.class)));
				will(returnValue(null));

				SubscriptionContentRelease expected = createContentRelease(0L, periodicity,
						Arrays.asList(createContent(updateTxt, null)), new DateTime(2010, 8, 25, 12, 0, 0, 0));
				exactly(1).of(subsDataRepo).addOrUpdateContent(
						with(allOf(listOfSize(1), subsContentWith(Arrays.asList(expected)))));

			}
		});

		SmsMessage message = createSmsMessage(updateTxt);

		CommandEvent<Message> commandEvent = new CommandEvent<Message>(message, new MessageContext());
		commandEvent.setSubKeyword("");

		TestUtil.setJodaSystemTime(new DateTime(2010, 8, 25, 9, 30, 0, 0));

		subscription.onServiceDataUpdateMessage(commandEvent);
	}

//	@Test
	public void whenNoInitialContentAvailable_ForAppWithSubcategory() throws Exception {
		PeriodUnit periodUnit = PeriodUnit.DAY;
		int periodValue = 1;
		int allowedBufferTime = 60 * 6;
		final Periodicity periodicity = new Periodicity(periodUnit, periodValue, allowedBufferTime);
		periodicity.setFirstScheduledDispatchTime(new DateTime(2010, 8, 22, 12, 0, 0, 0));
		subscription.setDefaultPeriodicity(periodicity);
		subscription.setSupportedPeriodicities(Arrays.asList(periodicity));

		final SubCategory subCat1 = createSubCategory("cat1");
		final SubCategory subCat2 = createSubCategory("cat2");
		subscription.setSupportedSubCategories(Arrays.asList(subCat1, subCat2));

		final String updateTxt = "MR is going to give a speak at a rally in Maharagama";

		mockContext.checking(new Expectations() {
			{
				exactly(1).of(messageDispatcher).dispatchIndividualMessages(
						with(any(String.class)),
						with(allOf(smsMessageWithSubkeyword("776456123"),
								smsMessageWithContent(String.format(successsRespTemplate, "2010-08-25 12:00:00")))));

				allowing(subsDataRepo).getNextUpdatableContent(with(any(String.class)), with(any(DateTime.class)));
				will(returnValue(null));

				SubscriptionContentRelease expected = createContentRelease(0L, periodicity, Arrays.asList(
						createContent(updateTxt, subCat1), createContent("", subCat2)), new DateTime(2010, 8, 25, 12,
						0, 0, 0));
				exactly(1).of(subsDataRepo).addOrUpdateContent(
						with(allOf(listOfSize(1), subsContentWith(Arrays.asList(expected)))));

			}
		});

		SmsMessage message = createSmsMessage(updateTxt);

		CommandEvent<Message> commandEvent = new CommandEvent<Message>(message, new MessageContext());
		commandEvent.setSubKeyword("cat1");

		TestUtil.setJodaSystemTime(new DateTime(2010, 8, 25, 9, 30, 0, 0));

		subscription.onServiceDataUpdateMessage(commandEvent);

	}

//	@Test
	public void whenScheduledContentAvailable_ForAppWithoutSubCategory() throws OrcaException {
		final Periodicity periodicity = new Periodicity(PeriodUnit.DAY, 1, 60 * 6);
		periodicity.setFirstScheduledDispatchTime(new DateTime(2010, 8, 22, 12, 0, 0, 0));
		subscription.setDefaultPeriodicity(periodicity);
		subscription.setSupportedPeriodicities(Arrays.asList(periodicity));

		SubscriptionContentRelease release1 = createContentRelease(1L, periodicity,
				Arrays.asList(createContent("Scheduled Content1", null)), new DateTime(2010, 8, 22, 12, 0, 0, 0));

		SubscriptionContentRelease release2 = createContentRelease(2L, periodicity,
				Arrays.asList(createContent("Scheduled Content2", null)), new DateTime(2010, 8, 23, 12, 0, 0, 0));

		SubscriptionContentRelease release3 = createContentRelease(3L, periodicity,
				Arrays.asList(createContent("Scheduled Content3", null)), new DateTime(2010, 8, 24, 12, 0, 0, 0));

		final SubscriptionContentRelease release4 = createContentRelease(4L, periodicity,
				Arrays.asList(createContent("Scheduled Content4", null)), new DateTime(2010, 8, 25, 12, 0, 0, 0));

		SubscriptionContentRelease release5 = createContentRelease(5L, periodicity,
				Arrays.asList(createContent("Scheduled Content5", null)), new DateTime(2010, 8, 26, 12, 0, 0, 0));

		final List<SubscriptionContentRelease> scheduledContentList = new ArrayList<SubscriptionContentRelease>();
		scheduledContentList.add(release1);
		scheduledContentList.add(release2);
		scheduledContentList.add(release3);
		scheduledContentList.add(release4);
		scheduledContentList.add(release5);

		final String updateTxt = "MR is going to give a speak at a rally in Maharagama";

		mockContext.checking(new Expectations() {
			{
				allowing(messageDispatcher).dispatchIndividualMessages(
						with(any(String.class)),
						with(allOf(smsMessageWithSubkeyword("776456123"),
								smsMessageWithContent(String.format(successsRespTemplate, "2010-08-25 12:00:00")))));

				allowing(subsDataRepo).getNextUpdatableContent(with(any(String.class)), with(any(DateTime.class)));
				will(returnValue(release4));

				SubscriptionContentRelease expected = createContentRelease(0L, periodicity,
						Arrays.asList(createContent(updateTxt, null)), new DateTime(2010, 8, 25, 12, 0, 0, 0));
				exactly(1).of(subsDataRepo).addOrUpdateContent(
						with(allOf(listOfSize(1), subsContentWith(Arrays.asList(expected)))));

			}
		});

		//@formatter:off
		/*
		 * date - content index
		 * 22 - 0
		 * 23 - 1
		 * 24 - 2
		 * 25 - 3
		 * 26 - 4
		 */
		//@formatter:on

		SmsMessage message = createSmsMessage(updateTxt);

		CommandEvent<Message> commandEvent = new CommandEvent<Message>(message, new MessageContext());
		commandEvent.setSubKeyword("");

		TestUtil.setJodaSystemTime(new DateTime(2010, 8, 24, 9, 30, 0, 0));

		subscription.onServiceDataUpdateMessage(commandEvent);

	}

//	@Test
	public void whenScheduledContentAvailable_ForAppWithSubCategory() throws OrcaException {
		final Periodicity periodicity = new Periodicity(PeriodUnit.DAY, 1, 60 * 6);
		periodicity.setFirstScheduledDispatchTime(new DateTime(2010, 8, 22, 12, 0, 0, 0));
		subscription.setDefaultPeriodicity(periodicity);
		subscription.setSupportedPeriodicities(Arrays.asList(periodicity));

		final SubCategory cat1 = createSubCategory("cat1");
		final SubCategory cat2 = createSubCategory("cat2");
		subscription.setSupportedSubCategories(Arrays.asList(cat1, cat2));

		SubscriptionContentRelease release1 = createContentRelease(
				1L,
				periodicity,
				Arrays.asList(createContent("Scheduled Content1-cat1", cat1),
						createContent("Scheduled Content1-cat2", cat2)), new DateTime(2010, 8, 22, 12, 0, 0, 0));

		SubscriptionContentRelease release2 = createContentRelease(
				2L,
				periodicity,
				Arrays.asList(createContent("Scheduled Content2-cat1", cat1),
						createContent("Scheduled Content2-cat2", cat2)), new DateTime(2010, 8, 23, 12, 0, 0, 0));

		final SubscriptionContentRelease release3 = createContentRelease(
				3L,
				periodicity,
				Arrays.asList(createContent("Scheduled Content3-cat1", cat1),
						createContent("Scheduled Content3-cat2", cat2)), new DateTime(2010, 8, 24, 12, 0, 0, 0));

		SubscriptionContentRelease release4 = createContentRelease(
				4L,
				periodicity,
				Arrays.asList(createContent("Scheduled Content4-cat1", cat1),
						createContent("Scheduled Content4-cat2", cat2)), new DateTime(2010, 8, 25, 12, 0, 0, 0));

		SubscriptionContentRelease release5 = createContentRelease(
				5L,
				periodicity,
				Arrays.asList(createContent("Scheduled Content5-cat1", cat1),
						createContent("Scheduled Content5-cat2", cat2)), new DateTime(2010, 8, 26, 12, 0, 0, 0));

		List<SubscriptionContentRelease> scheduledContentList = new ArrayList<SubscriptionContentRelease>();
		scheduledContentList.add(release1);
		scheduledContentList.add(release2);
		scheduledContentList.add(release3);
		scheduledContentList.add(release4);
		scheduledContentList.add(release5);

		final String updateTxt = "MR is going to give a speak at a rally in Maharagama";
		mockContext.checking(new Expectations() {
			{
				allowing(messageDispatcher).dispatchIndividualMessages(
						with(any(String.class)),
						with(allOf(smsMessageWithSubkeyword("776456123"),
								smsMessageWithContent(String.format(successsRespTemplate, "2010-08-24 12:00:00")))));

				allowing(subsDataRepo).getNextUpdatableContent(with(any(String.class)), with(any(DateTime.class)));
				will(returnValue(release3));

				SubscriptionContentRelease expected = createContentRelease(0L, periodicity,
						Arrays.asList(createContent("Scheduled Content3-cat1", cat1), createContent(updateTxt, cat2)),
						new DateTime(2010, 8, 24, 12, 0, 0, 0));
				exactly(1).of(subsDataRepo).addOrUpdateContent(
						with(allOf(listOfSize(1), subsContentWith(Arrays.asList(expected)))));

			}
		});

		//@formatter:off
		/*
		 * date - content index
		 * 22 - 0
		 * 23 - 1
		 * 24 - 2
		 * 25 - 3
		 * 26 - 4
		 */
		//@formatter:on

		SmsMessage message = createSmsMessage(updateTxt);

		CommandEvent<Message> commandEvent = new CommandEvent<Message>(message, new MessageContext());
		commandEvent.setSubKeyword("cat2");

		TestUtil.setJodaSystemTime(new DateTime(2010, 8, 25, 9, 30, 0, 0));

		subscription.onServiceDataUpdateMessage(commandEvent);
	}

	@Test
	public void smsUpdateWithInvalidSubCategory() throws Exception {
		mockContext.checking(new Expectations() {
			{

				allowing(subsDataRepo).getNextUpdatableContent(with(any(String.class)), with(any(DateTime.class)));
				will(returnValue(null));

			}
		});

		PeriodUnit periodUnit = PeriodUnit.DAY;
		int periodValue = 1;
		int allowedBufferTime = 60 * 6;
		Periodicity periodicity = new Periodicity(periodUnit, periodValue, allowedBufferTime);
		periodicity.setFirstScheduledDispatchTime(new DateTime(2010, 8, 22, 12, 0, 0, 0));
		subscription.setDefaultPeriodicity(periodicity);
		subscription.setSupportedPeriodicities(Arrays.asList(periodicity));

		subscription.setSupportedSubCategories(Arrays.asList(createSubCategory("cat1"), createSubCategory("cat2")));

		String updateTxt = "MR is going to give a speak at a rally in Maharagama";
		SmsMessage message = createSmsMessage(updateTxt);

		CommandEvent<Message> commandEvent = new CommandEvent<Message>(message, new MessageContext());
		commandEvent.setSubKeyword("invalid-category");

		TestUtil.setJodaSystemTime(new DateTime(2010, 8, 25, 9, 30, 0, 0));

		try {
			subscription.onServiceDataUpdateMessage(commandEvent);
			fail("should fail with incorrect subcategory error");
		} catch (SubscriptionServiceException e) {
			assertEquals(MessageFlowErrorReasons.SUB_KEYWORD_NOT_FOUND, e.getReason());
		}

	}

//	@Test
	public void smsUpdateWithinScheduledBufferTime_ForAppWithoutSubcategory() throws Exception {
		/**
		 * Buffer time is not considered when updating content, if scheduled
		 * time is passed new content should be created from next scheduled
		 * time.
		 */

		PeriodUnit periodUnit = PeriodUnit.HOUR;
		int periodValue = 1;
		int allowedBufferTime = 10;
		final Periodicity periodicity = new Periodicity(periodUnit, periodValue, allowedBufferTime);
		periodicity.setDefaultDispatchConfiguration(DefaultDispatchConfiguration.configureForHour(10));
		periodicity.setFirstScheduledDispatchTime(new DateTime(2010, 8, 22, 12, 0, 0, 0));
		subscription.setDefaultPeriodicity(periodicity);
		subscription.setSupportedPeriodicities(Arrays.asList(periodicity));

		final String updateTxt = "MR is going to give a speak at a rally in Maharagama";

		mockContext.checking(new Expectations() {
			{
				exactly(1).of(messageDispatcher).dispatchIndividualMessages(
						with(any(String.class)),
						with(allOf(smsMessageWithSubkeyword("776456123"),
								smsMessageWithContent(String.format(successsRespTemplate, "2010-08-25 10:00:00")))));

				allowing(subsDataRepo).getNextUpdatableContent(with(any(String.class)), with(any(DateTime.class)));
				will(returnValue(null));

				SubscriptionContentRelease expected = createContentRelease(0L, periodicity,
						Arrays.asList(createContent(updateTxt, null)), new DateTime(2010, 8, 25, 10, 0, 0, 0));
				exactly(1).of(subsDataRepo).addOrUpdateContent(
						with(allOf(listOfSize(1), subsContentWith(Arrays.asList(expected)))));

			}
		});

		SmsMessage message = createSmsMessage(updateTxt);

		CommandEvent<Message> commandEvent = new CommandEvent<Message>(message, new MessageContext());
		commandEvent.setSubKeyword("");

		TestUtil.setJodaSystemTime(new DateTime(2010, 8, 25, 9, 05, 0, 0));

		subscription.onServiceDataUpdateMessage(commandEvent);

	}


	@Test
	public void smsUpdateWith_WhenAppDoNotSupportSmsUpdate() throws Exception {
		mockContext.checking(new Expectations() {
			{

				allowing(subsDataRepo).getNextUpdatableContent(with(any(String.class)), with(any(DateTime.class)));
				will(returnValue(null));

			}
		});

		PeriodUnit periodUnit = PeriodUnit.DAY;
		int periodValue = 1;
		int allowedBufferTime = 60 * 6;
		Periodicity periodicity = new Periodicity(periodUnit, periodValue, allowedBufferTime);
		periodicity.setFirstScheduledDispatchTime(new DateTime(2010, 8, 22, 12, 0, 0, 0));
		subscription.setDefaultPeriodicity(periodicity);
		subscription.setSupportedPeriodicities(Arrays.asList(periodicity));

		subscription.setSupportedSubCategories(Arrays.asList(createSubCategory("cat1"), createSubCategory("cat2")));

		String updateTxt = "MR is going to give a speak at a rally in Maharagama";
		SmsMessage message = createSmsMessage(updateTxt);

		CommandEvent<Message> commandEvent = new CommandEvent<Message>(message, new MessageContext());
		commandEvent.setSubKeyword("invalid-category");

		TestUtil.setJodaSystemTime(new DateTime(2010, 8, 25, 9, 30, 0, 0));

		subscription.setSmsUpdateRequired(false);

		try {
			subscription.onServiceDataUpdateMessage(commandEvent);
			fail("Should fail with sms update not supported error");
		} catch (MessageFlowException e) {
			assertEquals(MessageFlowErrorReasons.SMS_UPDATE_NOT_SUPPORTED, e.getReason());
		}

	}


}
