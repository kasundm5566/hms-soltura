/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hsenidmobile.orca.core.applications.subscription;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import hsenidmobile.orca.core.applications.PeriodUnit;
import hsenidmobile.orca.core.applications.subscription.SubscriptionContentRelease.ContentReleaseStatus;
import hsenidmobile.orca.core.flow.MessageFlowErrorReasons;
import hsenidmobile.orca.core.model.Application;
import hsenidmobile.orca.core.model.Content;
import hsenidmobile.orca.core.model.Content.ContentSource;
import hsenidmobile.orca.core.model.LocalizedMessage;
import hsenidmobile.orca.core.model.SubCategory;
import hsenidmobile.orca.core.model.message.SmsMessage;
import hsenidmobile.orca.core.testutil.TestUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.joda.time.DateTime;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * $LastChangedDate$
 * $LastChangedBy$ $LastChangedRevision$
 */
public class SubscriptionTest {

	private Subscription subscription;
	private Mockery mockContext;
	private Application application;

	@Before
	public void setUp() {
		mockContext = new Mockery();
		application = mockContext.mock(Application.class);

		subscription = new Subscription();
		subscription.setApplication(application);

		mockContext.checking(new Expectations() {
			{

				allowing(application).getAppId();
				will(returnValue("APP_ID"));

				allowing(application).getAppName();
				will(returnValue("APP_Name"));
			}
		});

	}

	@After
	public void tearDown() {
		TestUtil.resetCurrentTime();
	}

	private Content createContent(String content, SubCategory subCategory) {
		Content con1 = new Content();
		con1.setContent(new LocalizedMessage(content, Locale.ENGLISH));
		con1.setSource(ContentSource.WEB);
		con1.setSubCategory(subCategory);

		return con1;
	}

	private SubCategory createSubCategory(String categoryCode) {
		SubCategory cat1 = new SubCategory();
		cat1.setKeyword(categoryCode);
		return cat1;
	}

	private SubscriptionContentRelease createContentRelease(long id, Periodicity periodicity, List<Content> content,
			DateTime scheduledTime) {
		SubscriptionContentRelease scheduledContent1 = new SubscriptionContentRelease();
		scheduledContent1.setId(id);
		scheduledContent1.setContents(content);
		scheduledContent1.setPeriodicity(periodicity);
		scheduledContent1.setScheduledDispatchTimeFrom(scheduledTime);

		return scheduledContent1;
	}

	//TODO: Fix the test cases
//	@Test
//	public void testGetDispatchableContentFromSubCategoryNotSupportedApp() throws SubscriptionServiceException {
//
//		Periodicity periodicity = new Periodicity(PeriodUnit.DAY, 1, 60 * 6);
//		periodicity.setFirstScheduledDispatchTime(new DateTime(2010, 8, 22, 12, 0, 0, 0));
//
//		subscription.setDefaultPeriodicity(periodicity);
//		subscription.setSupportedPeriodicities(Arrays.asList(periodicity));
//
//		SubscriptionContentRelease release1 = createContentRelease(1L, periodicity,
//				Arrays.asList(createContent("Scheduled Content1", null)), new DateTime(2010, 8, 22, 12, 0, 0, 0));
//
//		SubscriptionContentRelease release2 = createContentRelease(2L, periodicity,
//				Arrays.asList(createContent("Scheduled Content2", null)), new DateTime(2010, 8, 23, 12, 0, 0, 0));
//
//		SubscriptionContentRelease release3 = createContentRelease(3L, periodicity,
//				Arrays.asList(createContent("Scheduled Content3", null)), new DateTime(2010, 8, 24, 12, 0, 0, 0));
//
//		SubscriptionContentRelease release4 = createContentRelease(4L, periodicity,
//				Arrays.asList(createContent("Scheduled Content4", null)), new DateTime(2010, 8, 25, 12, 0, 0, 0));
//
//		SubscriptionContentRelease release5 = createContentRelease(5L, periodicity,
//				Arrays.asList(createContent("Scheduled Content5", null)), new DateTime(2010, 8, 26, 12, 0, 0, 0));
//
//		List<SubscriptionContentRelease> scheduledContentList = new ArrayList<SubscriptionContentRelease>();
//		scheduledContentList.add(release1);
//		scheduledContentList.add(release2);
//		scheduledContentList.add(release3);
//		scheduledContentList.add(release4);
//		scheduledContentList.add(release5);
//		subscription.setScheduledContentList(scheduledContentList);
//
//		List<SmsMessage> dispatchableContent = subscription.getDispatchableContent(new DateTime(2010, 8, 22, 12, 10, 0,
//				0).getMillis(), PeriodUnit.DAY);
//		assertNotNull("Dispatchable content cannot be null", dispatchableContent);
//		assertEquals("Content size", 1, dispatchableContent.size());
//		assertEquals("Sms Content", "Scheduled Content1", dispatchableContent.get(0).getMessage());
//		assertEquals("Sms keyword", "all_registered", dispatchableContent.get(0).getKeyword());
//
//		dispatchableContent = subscription.getDispatchableContent(new DateTime(2010, 8, 26, 12, 10, 0, 0).getMillis(),
//				PeriodUnit.DAY);
//		assertNotNull("Dispatchable content cannot be null", dispatchableContent);
//		assertEquals("Content size", 1, dispatchableContent.size());
//		assertEquals("Sms Content", "Scheduled Content5", dispatchableContent.get(0).getMessage());
//		assertEquals("Sms keyword", "all_registered", dispatchableContent.get(0).getKeyword());
//	}
//
//	@Test
//	public void testGetDispatchableContentFromSubCategorySupportedApp() throws SubscriptionServiceException {
//
//		Periodicity periodicity = new Periodicity(PeriodUnit.DAY, 1, 60 * 6);
//		periodicity.setFirstScheduledDispatchTime(new DateTime(2010, 8, 22, 12, 0, 0, 0));
//		subscription.setDefaultPeriodicity(periodicity);
//		subscription.setSupportedPeriodicities(Arrays.asList(periodicity));
//
//		SubCategory cat1 = createSubCategory("cat1");
//		SubCategory cat2 = createSubCategory("cat2");
//		subscription.setSupportedSubCategories(Arrays.asList(cat1, cat2));
//
//		SubscriptionContentRelease release1 = createContentRelease(
//				1L,
//				periodicity,
//				Arrays.asList(createContent("Scheduled Content1-cat1", cat1),
//						createContent("Scheduled Content1-cat2", cat2)), new DateTime(2010, 8, 22, 12, 0, 0, 0));
//
//		SubscriptionContentRelease release2 = createContentRelease(
//				2L,
//				periodicity,
//				Arrays.asList(createContent("Scheduled Content2-cat1", cat1),
//						createContent("Scheduled Content2-cat2", cat2)), new DateTime(2010, 8, 23, 12, 0, 0, 0));
//
//		SubscriptionContentRelease release3 = createContentRelease(
//				3L,
//				periodicity,
//				Arrays.asList(createContent("Scheduled Content3-cat1", cat1),
//						createContent("Scheduled Content3-cat2", cat2)), new DateTime(2010, 8, 24, 12, 0, 0, 0));
//
//		SubscriptionContentRelease release4 = createContentRelease(
//				4L,
//				periodicity,
//				Arrays.asList(createContent("Scheduled Content4-cat1", cat1),
//						createContent("Scheduled Content4-cat2", cat2)), new DateTime(2010, 8, 25, 12, 0, 0, 0));
//
//		SubscriptionContentRelease release5 = createContentRelease(
//				5L,
//				periodicity,
//				Arrays.asList(createContent("Scheduled Content5-cat1", cat1),
//						createContent("Scheduled Content5-cat2", cat2)), new DateTime(2010, 8, 26, 12, 0, 0, 0));
//
//		List<SubscriptionContentRelease> scheduledContentList = new ArrayList<SubscriptionContentRelease>();
//		scheduledContentList.add(release1);
//		scheduledContentList.add(release2);
//		scheduledContentList.add(release3);
//		scheduledContentList.add(release4);
//		scheduledContentList.add(release5);
//		subscription.setScheduledContentList(scheduledContentList);
//
//		List<SmsMessage> dispatchableContent = subscription.getDispatchableContent(new DateTime(2010, 8, 22, 12, 10, 0,
//				0).getMillis(), PeriodUnit.DAY);
//		assertNotNull("Dispatchable content cannot be null", dispatchableContent);
//		assertEquals("Content size", 2, dispatchableContent.size());
//		assertEquals("Sms Content", "Scheduled Content1-cat1", dispatchableContent.get(0).getMessage());
//		assertEquals("Sms keyword", "cat1", dispatchableContent.get(0).getKeyword());
//		assertEquals("Sms Content", "Scheduled Content1-cat2", dispatchableContent.get(1).getMessage());
//		assertEquals("Sms keyword", "cat2", dispatchableContent.get(1).getKeyword());
//
//		dispatchableContent = subscription.getDispatchableContent(new DateTime(2010, 8, 26, 12, 10, 0, 0).getMillis(),
//				PeriodUnit.DAY);
//
//		assertNotNull("Dispatchable content cannot be null", dispatchableContent);
//		assertEquals("Content size", 2, dispatchableContent.size());
//		assertEquals("Sms Content", "Scheduled Content5-cat1", dispatchableContent.get(0).getMessage());
//		assertEquals("Sms keyword", "cat1", dispatchableContent.get(0).getKeyword());
//		assertEquals("Sms Content", "Scheduled Content5-cat2", dispatchableContent.get(1).getMessage());
//		assertEquals("Sms keyword", "cat2", dispatchableContent.get(1).getKeyword());
//	}
//
//	@Test
//	public void testGetDispatchableContentWhenContentNotAvailable() throws SubscriptionServiceException {
//
//		Periodicity periodicity = new Periodicity(PeriodUnit.DAY, 1, 60 * 6);
//		periodicity.setFirstScheduledDispatchTime(new DateTime(2010, 8, 22, 12, 0, 0, 0));
//		subscription.setDefaultPeriodicity(periodicity);
//		subscription.setSupportedPeriodicities(Arrays.asList(periodicity));
//
//		SubCategory cat1 = createSubCategory("cat1");
//		SubCategory cat2 = createSubCategory("cat2");
//		subscription.setSupportedSubCategories(Arrays.asList(cat1, cat2));
//
//		SubscriptionContentRelease release1 = createContentRelease(
//				1L,
//				periodicity,
//				Arrays.asList(createContent("Scheduled Content1-cat1", cat1),
//						createContent("Scheduled Content1-cat2", cat2)), new DateTime(2010, 8, 22, 12, 0, 0, 0));
//
//		List<SubscriptionContentRelease> scheduledContentList = new ArrayList<SubscriptionContentRelease>();
//		scheduledContentList.add(release1);
//		subscription.setScheduledContentList(scheduledContentList);
//
//		try {
//			subscription.getDispatchableContent(new DateTime(2010, 8, 27, 12, 10, 0, 0).getMillis(), PeriodUnit.DAY);
//			fail("Should fail with data not available exception.");
//		} catch (SubscriptionServiceException e) {
//			assertEquals(MessageFlowErrorReasons.SUBSCRIPTION_DATA_NOT_DISPATCHABLE_NOW, e.getReason());
//		}
//
//	}
//
//	@Test
//	public void testGetDispatchableContentWhenDispatchingBufferTimePassed() throws SubscriptionServiceException {
//
//		Periodicity periodicity = new Periodicity(PeriodUnit.DAY, 1, 60 * 6);
//		periodicity.setFirstScheduledDispatchTime(new DateTime(2010, 8, 22, 12, 0, 0, 0));
//		subscription.setDefaultPeriodicity(periodicity);
//		subscription.setSupportedPeriodicities(Arrays.asList(periodicity));
//
//		SubCategory cat1 = createSubCategory("cat1");
//		SubCategory cat2 = createSubCategory("cat2");
//		subscription.setSupportedSubCategories(Arrays.asList(cat1, cat2));
//
//		SubscriptionContentRelease release1 = createContentRelease(
//				1L,
//				periodicity,
//				Arrays.asList(createContent("Scheduled Content1-cat1", cat1),
//						createContent("Scheduled Content1-cat2", cat2)), new DateTime(2010, 8, 22, 12, 0, 0, 0));
//
//		SubscriptionContentRelease release2 = createContentRelease(
//				2L,
//				periodicity,
//				Arrays.asList(createContent("Scheduled Content2-cat1", cat1),
//						createContent("Scheduled Content2-cat2", cat2)), new DateTime(2010, 8, 23, 12, 0, 0, 0));
//
//		SubscriptionContentRelease release3 = createContentRelease(
//				3L,
//				periodicity,
//				Arrays.asList(createContent("Scheduled Content3-cat1", cat1),
//						createContent("Scheduled Content3-cat2", cat2)), new DateTime(2010, 8, 24, 12, 0, 0, 0));
//
//		SubscriptionContentRelease release4 = createContentRelease(
//				4L,
//				periodicity,
//				Arrays.asList(createContent("Scheduled Content4-cat1", cat1),
//						createContent("Scheduled Content4-cat2", cat2)), new DateTime(2010, 8, 25, 12, 0, 0, 0));
//
//		SubscriptionContentRelease release5 = createContentRelease(
//				5L,
//				periodicity,
//				Arrays.asList(createContent("Scheduled Content5-cat1", cat1),
//						createContent("Scheduled Content5-cat2", cat2)), new DateTime(2010, 8, 26, 12, 0, 0, 0));
//
//		List<SubscriptionContentRelease> scheduledContentList = new ArrayList<SubscriptionContentRelease>();
//		scheduledContentList.add(release1);
//		scheduledContentList.add(release2);
//		scheduledContentList.add(release3);
//		scheduledContentList.add(release4);
//		scheduledContentList.add(release5);
//		subscription.setScheduledContentList(scheduledContentList);
//
//		try {
//			subscription.getDispatchableContent(new DateTime(2010, 8, 25, 18, 10, 0, 0).getMillis(), PeriodUnit.DAY);
//			fail("Should fail with data not available exception.");
//		} catch (SubscriptionServiceException e) {
//			assertEquals(MessageFlowErrorReasons.SUBSCRIPTION_DATA_NOT_DISPATCHABLE_NOW, e.getReason());
//		}
//
//		assertEquals("Status should be changed to skipped", ContentReleaseStatus.SKIPPED, subscription
//				.getScheduledContentList().get(3).getStatus());
//
//	}

	@Test
	public void testIsEligibleForDispatching_ForTimeUnit_HOUR() throws Exception {

		Periodicity periodicity = new Periodicity(PeriodUnit.HOUR, 1, 10);
		periodicity.setDefaultDispatchConfiguration(DefaultDispatchConfiguration.configureForHour(0));
		periodicity.setFirstScheduledDispatchTime(new DateTime(2010, 8, 20, 12, 0, 0, 0));
		subscription.setDefaultPeriodicity(periodicity);
		subscription.setSupportedPeriodicities(Arrays.asList(periodicity));

		List<InOutPair<DateTime, Boolean>> testData = new ArrayList<InOutPair<DateTime, Boolean>>();
		testData.add(new InOutPair<DateTime, Boolean>(new DateTime(2010, 8, 22, 11, 0, 0, 0), true));
		testData.add(new InOutPair<DateTime, Boolean>(new DateTime(2010, 8, 22, 11, 40, 0, 0), false));
		testData.add(new InOutPair<DateTime, Boolean>(new DateTime(2010, 8, 22, 12, 0, 0, 0), true));
		testData.add(new InOutPair<DateTime, Boolean>(new DateTime(2010, 8, 22, 12, 5, 0, 0), true));
		testData.add(new InOutPair<DateTime, Boolean>(new DateTime(2010, 8, 22, 12, 10, 0, 0), true));
		testData.add(new InOutPair<DateTime, Boolean>(new DateTime(2010, 8, 22, 12, 40, 0, 0), false));

		for (InOutPair<DateTime, Boolean> inOutPair : testData) {
			boolean eligibility = subscription.isEligibleForDispatching(PeriodUnit.HOUR, inOutPair.getInput());
			assertEquals("For testdata[" + inOutPair + "]", inOutPair.getOutput(), eligibility);
		}
	}

	@Test
	public void testIsEligibleForDispatching_ForTimeUnit_DAY() throws Exception {

		Periodicity periodicity = new Periodicity(PeriodUnit.DAY, 1, 30);
		periodicity.setDefaultDispatchConfiguration(DefaultDispatchConfiguration.configureForDay(9, 30));
		periodicity.setFirstScheduledDispatchTime(new DateTime(2010, 8, 20, 9, 30, 0, 0));
		subscription.setDefaultPeriodicity(periodicity);
		subscription.setSupportedPeriodicities(Arrays.asList(periodicity));

		List<InOutPair<DateTime, Boolean>> testData = new ArrayList<InOutPair<DateTime, Boolean>>();
		testData.add(new InOutPair<DateTime, Boolean>(new DateTime(2010, 8, 22, 8, 0, 0, 0), false));
		testData.add(new InOutPair<DateTime, Boolean>(new DateTime(2010, 8, 22, 9, 30, 0, 0), true));
		testData.add(new InOutPair<DateTime, Boolean>(new DateTime(2010, 8, 22, 9, 45, 0, 0), true));
		testData.add(new InOutPair<DateTime, Boolean>(new DateTime(2010, 8, 22, 10, 0, 0, 0), true));
		testData.add(new InOutPair<DateTime, Boolean>(new DateTime(2010, 8, 22, 10, 5, 0, 0), false));
		testData.add(new InOutPair<DateTime, Boolean>(new DateTime(2010, 8, 23, 9, 35, 0, 0), true));

		for (InOutPair<DateTime, Boolean> inOutPair : testData) {
			boolean eligibility = subscription.isEligibleForDispatching(PeriodUnit.DAY, inOutPair.getInput());
			assertEquals("For testdata[" + inOutPair + "]", inOutPair.getOutput(), eligibility);
		}
	}

	@Test
	public void testIsEligibleForDispatching_ForTimeUnit_WEEK() throws Exception {

		Periodicity periodicity = new Periodicity(PeriodUnit.WEEK, 1, 60 * 24);
		periodicity.setDefaultDispatchConfiguration(DefaultDispatchConfiguration.configureForWeek(2, 8, 30));
		periodicity.setFirstScheduledDispatchTime(new DateTime(2010, 8, 24, 8, 30, 0, 0));
		subscription.setDefaultPeriodicity(periodicity);
		subscription.setSupportedPeriodicities(Arrays.asList(periodicity));

		//@formatter:off
		/* 2010 -Aug/Sep
		 * 7	1	2	3	4	5	6
		 * Su	M	T	W	T	F	Sa
		 * 15	16	17	18	19	20	21
		 * 22	23	24	25	26	27	28
		 * 29	30	31	1	2	3	4
		 * 5	6	7	8	9	10	11
		*/
		//@formatter:on

		List<InOutPair<DateTime, Boolean>> testData = new ArrayList<InOutPair<DateTime, Boolean>>();
		testData.add(new InOutPair<DateTime, Boolean>(new DateTime(2010, 8, 24, 8, 0, 0, 0), false));
		testData.add(new InOutPair<DateTime, Boolean>(new DateTime(2010, 8, 24, 8, 30, 0, 0), true));
		testData.add(new InOutPair<DateTime, Boolean>(new DateTime(2010, 8, 24, 12, 00, 0, 0), true));
		testData.add(new InOutPair<DateTime, Boolean>(new DateTime(2010, 8, 25, 8, 0, 0, 0), true));
		testData.add(new InOutPair<DateTime, Boolean>(new DateTime(2010, 8, 25, 8, 40, 0, 0), false));
		testData.add(new InOutPair<DateTime, Boolean>(new DateTime(2010, 8, 26, 8, 30, 0, 0), false));
		testData.add(new InOutPair<DateTime, Boolean>(new DateTime(2010, 8, 31, 8, 40, 0, 0), true));
		testData.add(new InOutPair<DateTime, Boolean>(new DateTime(2010, 9, 1, 8, 30, 0, 0), true));
		testData.add(new InOutPair<DateTime, Boolean>(new DateTime(2010, 9, 2, 8, 30, 0, 0), false));

		for (InOutPair<DateTime, Boolean> inOutPair : testData) {
			boolean eligibility = subscription.isEligibleForDispatching(PeriodUnit.WEEK, inOutPair.getInput());
			assertEquals("For testdata[" + inOutPair + "]", inOutPair.getOutput(), eligibility);
		}
	}

	@Test
	public void testIsEligibleForDispatching_ForTimeUnit_DayOf_MONTH() throws Exception {

		Periodicity periodicity = new Periodicity(PeriodUnit.MONTH, 1, 60 * 24);
		periodicity.setDefaultDispatchConfiguration(DefaultDispatchConfiguration.configureForDayOfMonth(15, 7, 00));
		periodicity.setFirstScheduledDispatchTime(new DateTime(2010, 8, 15, 7, 00, 0, 0));
		subscription.setDefaultPeriodicity(periodicity);
		subscription.setSupportedPeriodicities(Arrays.asList(periodicity));

		List<InOutPair<DateTime, Boolean>> testData = new ArrayList<InOutPair<DateTime, Boolean>>();
		testData.add(new InOutPair<DateTime, Boolean>(new DateTime(2010, 8, 14, 7, 0, 0, 0), false));
		testData.add(new InOutPair<DateTime, Boolean>(new DateTime(2010, 8, 15, 7, 00, 0, 0), true));
		testData.add(new InOutPair<DateTime, Boolean>(new DateTime(2010, 8, 15, 7, 30, 0, 0), true));
		testData.add(new InOutPair<DateTime, Boolean>(new DateTime(2010, 8, 16, 7, 0, 0, 0), true));
		testData.add(new InOutPair<DateTime, Boolean>(new DateTime(2010, 8, 17, 7, 0, 0, 0), false));
		testData.add(new InOutPair<DateTime, Boolean>(new DateTime(2010, 9, 15, 7, 00, 0, 0), true));

		for (InOutPair<DateTime, Boolean> inOutPair : testData) {
			boolean eligibility = subscription.isEligibleForDispatching(PeriodUnit.MONTH, inOutPair.getInput());
			assertEquals("For testdata[" + inOutPair + "]", inOutPair.getOutput(), eligibility);
		}

		periodicity.setDefaultDispatchConfiguration(DefaultDispatchConfiguration.configureForDayOfMonth(31, 7, 00));
		periodicity.setFirstScheduledDispatchTime(new DateTime(2010, 8, 31, 7, 00, 0, 0));
		subscription.setDefaultPeriodicity(periodicity);
		subscription.setSupportedPeriodicities(Arrays.asList(periodicity));

		testData = new ArrayList<InOutPair<DateTime, Boolean>>();
		testData.add(new InOutPair<DateTime, Boolean>(new DateTime(2010, 8, 14, 7, 0, 0, 0), false));
		testData.add(new InOutPair<DateTime, Boolean>(new DateTime(2010, 8, 31, 7, 00, 0, 0), true));
		testData.add(new InOutPair<DateTime, Boolean>(new DateTime(2010, 8, 31, 7, 30, 0, 0), true));
		// TODO:For Day_Of_Month configuration where buffer is 24 hours or more
		// not currently supported
		testData.add(new InOutPair<DateTime, Boolean>(new DateTime(2010, 9, 1, 7, 0, 0, 0), true));
		testData.add(new InOutPair<DateTime, Boolean>(new DateTime(2010, 9, 2, 7, 0, 0, 0), false));
		testData.add(new InOutPair<DateTime, Boolean>(new DateTime(2010, 9, 30, 7, 00, 0, 0), true));

		for (InOutPair<DateTime, Boolean> inOutPair : testData) {
			boolean eligibility = subscription.isEligibleForDispatching(PeriodUnit.MONTH, inOutPair.getInput());
			assertEquals("For testdata[" + inOutPair + "]", inOutPair.getOutput(), eligibility);
		}
	}

	@Test
	public void testIsEligibleForDispatching_ForTimeUnit_LastDayOf_MONTH() throws Exception {

		Periodicity periodicity = new Periodicity(PeriodUnit.MONTH, 1, 60 * 24);
		periodicity.setDefaultDispatchConfiguration(DefaultDispatchConfiguration.configureForLastDayOfMonth(7, 00));
		periodicity.setFirstScheduledDispatchTime(new DateTime(2010, 8, 31, 7, 00, 0, 0));
		subscription.setDefaultPeriodicity(periodicity);
		subscription.setSupportedPeriodicities(Arrays.asList(periodicity));

		List<InOutPair<DateTime, Boolean>> testData = new ArrayList<InOutPair<DateTime, Boolean>>();
		testData.add(new InOutPair<DateTime, Boolean>(new DateTime(2010, 8, 14, 7, 0, 0, 0), false));
		testData.add(new InOutPair<DateTime, Boolean>(new DateTime(2010, 8, 31, 7, 00, 0, 0), true));
		testData.add(new InOutPair<DateTime, Boolean>(new DateTime(2010, 8, 31, 7, 30, 0, 0), true));
		// TODO:For Last_Day_Of_Month configuration where buffer is 24 hours or
		// more not currently supported
		testData.add(new InOutPair<DateTime, Boolean>(new DateTime(2010, 9, 1, 7, 0, 0, 0), true));
		testData.add(new InOutPair<DateTime, Boolean>(new DateTime(2010, 9, 1, 7, 10, 0, 0), false));
		testData.add(new InOutPair<DateTime, Boolean>(new DateTime(2010, 9, 2, 7, 0, 0, 0), false));
		testData.add(new InOutPair<DateTime, Boolean>(new DateTime(2010, 9, 30, 7, 00, 0, 0), true));

		for (InOutPair<DateTime, Boolean> inOutPair : testData) {
			boolean eligibility = subscription.isEligibleForDispatching(PeriodUnit.MONTH, inOutPair.getInput());
			assertEquals("For testdata[" + inOutPair + "]", inOutPair.getOutput(), eligibility);
		}
	}
}

class InOutPair<In, Out> {
	private In input;
	private Out output;

	public InOutPair(In input, Out output) {
		super();
		this.input = input;
		this.output = output;
	}

	public In getInput() {
		return input;
	}

	public Out getOutput() {
		return output;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("InOutPair [input=");
		builder.append(input);
		builder.append(", output=");
		builder.append(output);
		builder.append("]");
		return builder.toString();
	}

}
