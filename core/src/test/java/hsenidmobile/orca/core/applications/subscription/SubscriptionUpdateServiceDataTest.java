/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.applications.subscription;

import hsenidmobile.orca.core.applications.PeriodUnit;
import hsenidmobile.orca.core.flow.MessageFlowErrorReasons;
import hsenidmobile.orca.core.flow.dispatch.aventuraapi.AventuraApiMtMessageDispatcher;
import hsenidmobile.orca.core.model.*;
import hsenidmobile.orca.core.model.Content.ContentSource;
import hsenidmobile.orca.core.testutil.TestUtil;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JMock;
import org.joda.time.DateTime;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import static hsenidmobile.orca.core.testutil.ListSizeMatcher.listOfSize;
import static hsenidmobile.orca.core.testutil.SubscriptionContentReleaseMatcher.subsContentWith;
import static org.hamcrest.core.AllOf.allOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Tests the scenarios related to {@link Subscription#updateServiceData(List)}
 *
 * $LastChangedDate$
 * $LastChangedBy$ $LastChangedRevision$
 *
 */
@RunWith(JMock.class)
public class SubscriptionUpdateServiceDataTest {
	private Subscription subscription;
	private Mockery mockContext;
	private AventuraApiMtMessageDispatcher messageDispatcher;
	private Application application;
	private SubscriptionServiceDataRepository subsDataRepo;

	@Before
	public void setUp() {
		mockContext = new Mockery();
		application = mockContext.mock(Application.class);
		messageDispatcher = mockContext.mock(AventuraApiMtMessageDispatcher.class);
		subsDataRepo = mockContext.mock(SubscriptionServiceDataRepository.class);

		subscription = new Subscription();
		subscription.setApplication(application);
		subscription.setAventuraApiMtMessageDispatcher(messageDispatcher);
		subscription.setSubscriptionDataRepository(subsDataRepo);

		Periodicity periodicity = new Periodicity(PeriodUnit.HOUR, 1, 10);
		subscription.setDefaultPeriodicity(periodicity);
		subscription.setSupportedPeriodicities(Arrays.asList(periodicity));

		mockContext.checking(new Expectations() {
			{
				allowing(messageDispatcher).dispatchMessage(with(any(List.class)));

				allowing(messageDispatcher).dispatchIndividualMessages(with(any(String.class)), with(any(List.class)));

				allowing(application).getAppId();
				will(returnValue("APP_ID"));

				allowing(application).getAppName();
				will(returnValue("APP_Name"));
			}
		});

	}

	@After
	public void tearDown() {
		TestUtil.resetCurrentTime();
	}

	private Content createContent(String content, SubCategory subCategory) {
		Content con1 = new Content();
		con1.setContent(new LocalizedMessage(content, Locale.ENGLISH));
		con1.setSource(ContentSource.WEB);
		con1.setSubCategory(subCategory);

		return con1;
	}

	private SubCategory createSubCategory(String categoryCode) {
		SubCategory cat1 = new SubCategory();
		cat1.setKeyword(categoryCode);
		return cat1;
	}

	private SubscriptionContentRelease createContentRelease(long id, Periodicity periodicity, List<Content> content,
			DateTime scheduledTime) {
		SubscriptionContentRelease scheduledContent1 = new SubscriptionContentRelease();
		scheduledContent1.setId(id);
		scheduledContent1.setContents(content);
		scheduledContent1.setPeriodicity(periodicity);
		scheduledContent1.setScheduledDispatchTimeFrom(scheduledTime);
		scheduledContent1.setScheduledDispatchTimeTo(scheduledContent1.getScheduledDispatchTimeFrom().plusMinutes(
				periodicity.getAllowedBufferTime()));

		return scheduledContent1;
	}

	@Test
	public void whenNoInitialContentAvailable_ForAppWithoutSubcategory() throws Exception {

		SubscriptionContentRelease contentRelease = new SubscriptionContentRelease();
		final String updateTxt = "First Subscription Content";
		contentRelease.setContents(Arrays.asList(createContent(updateTxt, null)));
		contentRelease.setScheduledDispatchTimeFrom(new DateTime(2010, 10, 4, 9, 0, 0, 0));
		contentRelease.setScheduledDispatchTimeTo(new DateTime(2010, 10, 4, 9, 10, 0, 0));
		final Periodicity periodicity = new Periodicity(PeriodUnit.HOUR, 1, 10);
		contentRelease.setPeriodicity(periodicity);

		// assertNull("There shouldn't be any before update",
		// subscription.getScheduledContentList());

		mockContext.checking(new Expectations() {
			{
				exactly(1).of(subsDataRepo).getUpdatableContents(with(any(String.class)), with(any(DateTime.class)));
				will(returnValue(null));

				SubscriptionContentRelease expected = createContentRelease(0L, periodicity,
						Arrays.asList(createContent(updateTxt, null)), new DateTime(2010, 10, 4, 9, 0, 0, 0));
				exactly(1).of(subsDataRepo).addOrUpdateContent(
						with(allOf(listOfSize(1), subsContentWith(Arrays.asList(expected)))));
			}
		});

		subscription.updateServiceData(Arrays.asList((ContentRelease) contentRelease));

		// List<SubscriptionContentRelease> scheduledContent =
		// subscription.getScheduledContentList();
		// assertNotNull("There should be a scheduled content after update",
		// scheduledContent);
		// assertEquals("Number of scheduled releases", 1,
		// scheduledContent.size());

	}

//	@Test
	public void whenNoInitialContentAvailable_ForAppWithoutSubcategory_WhenMoreThanOneContentGiven() throws Exception {

		SubscriptionContentRelease contentRelease = new SubscriptionContentRelease();
		contentRelease.setContents(Arrays.asList(createContent("First Subscription Content", null),
				createContent("Additional Content", null)));
		contentRelease.setScheduledDispatchTimeFrom(new DateTime(2010, 10, 4, 9, 0, 0, 0));
		contentRelease.setPeriodicity(new Periodicity(PeriodUnit.HOUR, 1, 10));

		// assertNull("There shouldn't be any before update",
		// subscription.getScheduledContentList());
		mockContext.checking(new Expectations() {
			{
				exactly(1).of(subsDataRepo).getUpdatableContents(with(any(String.class)), with(any(DateTime.class)));
				will(returnValue(null));

			}
		});

		try {
			subscription.updateServiceData(Arrays.asList((ContentRelease) contentRelease));
			fail("Should fail with subcategory not supported exception");
		} catch (SubscriptionServiceException e) {
			assertEquals(MessageFlowErrorReasons.SUB_KEYWORD_NOT_SUPPORTED, e.getReason());
		}

	}

	@Test
	public void updateWithNonSupportedPeriodicicy() throws Exception {

		SubscriptionContentRelease contentRelease = new SubscriptionContentRelease();
		contentRelease.setContents(Arrays.asList(createContent("First Subscription Content", null)));
		contentRelease.setScheduledDispatchTimeFrom(new DateTime(2010, 10, 4, 9, 0, 0, 0));
		Periodicity invalidPeriodicity = new Periodicity(PeriodUnit.DAY, 1, 10);
		contentRelease.setPeriodicity(invalidPeriodicity);

		// assertNull("There shouldn't be any before update",
		// subscription.getScheduledContentList());
		mockContext.checking(new Expectations() {
			{
				exactly(1).of(subsDataRepo).getUpdatableContents(with(any(String.class)), with(any(DateTime.class)));
				will(returnValue(null));

			}
		});

		try {
			subscription.updateServiceData(Arrays.asList((ContentRelease) contentRelease));
			fail("Should fail with periodicity not supported exception");
		} catch (SubscriptionServiceException e) {
			assertEquals(MessageFlowErrorReasons.SUBSCRIPTION_PERIODICITY_NOT_SUPPORTED, e.getReason());
		}

	}

	@Test
	public void whenNoInitialContentAvailable_ForAppWithSubcategory_Success() throws Exception {
		final SubCategory cat1 = createSubCategory("cat1");
		final SubCategory cat2 = createSubCategory("cat2");
		subscription.setSupportedSubCategories(Arrays.asList(cat1, cat2));

		SubscriptionContentRelease contentRelease = new SubscriptionContentRelease();
		final String updateTxtCat1 = "First Subscription Content for ca1";
		final String updateTxtCat2 = "First Subscription Content for ca2";
		contentRelease.setContents(Arrays
				.asList(createContent(updateTxtCat1, cat1), createContent(updateTxtCat2, cat2)));
		contentRelease.setScheduledDispatchTimeFrom(new DateTime(2010, 10, 4, 9, 0, 0, 0));
		contentRelease.setScheduledDispatchTimeTo(new DateTime(2010, 10, 4, 9, 10, 0, 0));
		final Periodicity periodicity = new Periodicity(PeriodUnit.HOUR, 1, 10);
		contentRelease.setPeriodicity(periodicity);

		// assertNull("There shouldn't be any before update",
		// subscription.getScheduledContentList());

		mockContext.checking(new Expectations() {
			{
				exactly(1).of(subsDataRepo).getUpdatableContents(with(any(String.class)), with(any(DateTime.class)));
				will(returnValue(null));

				SubscriptionContentRelease expected = createContentRelease(0L, periodicity,
						Arrays.asList(createContent(updateTxtCat1, cat1), createContent(updateTxtCat2, cat2)),
						new DateTime(2010, 10, 4, 9, 0, 0, 0));
				exactly(1).of(subsDataRepo).addOrUpdateContent(
						with(allOf(listOfSize(1), subsContentWith(Arrays.asList(expected)))));
			}
		});

		subscription.updateServiceData(Arrays.asList((ContentRelease) contentRelease));

		// List<SubscriptionContentRelease> scheduledContent =
		// subscription.getScheduledContentList();
		// assertNotNull("There should be a scheduled content after update",
		// scheduledContent);
		// assertEquals("Number of scheduled releases", 1,
		// scheduledContent.size());

	}

//	@Test
	public void whenNoInitialContentAvailable_ForAppWithSubcategory_WhenContentForOneSubCategoryNotGiven()
			throws Exception {
		SubCategory cat1 = createSubCategory("cat1");
		SubCategory cat2 = createSubCategory("cat2");
		subscription.setSupportedSubCategories(Arrays.asList(cat1, cat2));

		SubscriptionContentRelease contentRelease = new SubscriptionContentRelease();
		contentRelease.setContents(Arrays.asList(createContent("First Subscription Content for ca1", cat1)));
		contentRelease.setScheduledDispatchTimeFrom(new DateTime(2010, 10, 4, 9, 0, 0, 0));
		contentRelease.setPeriodicity(new Periodicity(PeriodUnit.HOUR, 1, 10));

		// assertNull("There shouldn't be any before update",
		// subscription.getScheduledContentList());
		mockContext.checking(new Expectations() {
			{
				exactly(1).of(subsDataRepo).getUpdatableContents(with(any(String.class)), with(any(DateTime.class)));
				will(returnValue(null));

			}
		});

		try {
			subscription.updateServiceData(Arrays.asList((ContentRelease) contentRelease));
			fail("Should fail with subcategory not found exception");
		} catch (SubscriptionServiceException e) {
			assertEquals(MessageFlowErrorReasons.SUB_KEYWORD_NOT_FOUND, e.getReason());
		}

	}

	@Test
	public void whenNoInitialContentAvailable_ForAppWithSubcategory_WithInvalidSubCategory() throws Exception {
		SubCategory cat1 = createSubCategory("cat1");
		SubCategory cat2 = createSubCategory("cat2");
		subscription.setSupportedSubCategories(Arrays.asList(cat1, cat2));

		SubscriptionContentRelease contentRelease = new SubscriptionContentRelease();
		contentRelease.setContents(Arrays.asList(createContent("First Subscription Content for ca2", cat2),
				createContent("Content with invalid content", createSubCategory("cat3"))));
		contentRelease.setScheduledDispatchTimeFrom(new DateTime(2010, 10, 4, 9, 0, 0, 0));
		contentRelease.setPeriodicity(new Periodicity(PeriodUnit.HOUR, 1, 10));

		// assertNull("There shouldn't be any before update",
		// subscription.getScheduledContentList());

		mockContext.checking(new Expectations() {
			{
				exactly(1).of(subsDataRepo).getUpdatableContents(with(any(String.class)), with(any(DateTime.class)));
				will(returnValue(null));

			}
		});

		try {
			subscription.updateServiceData(Arrays.asList((ContentRelease) contentRelease));
			fail("Should fail with subcategory not found exception");
		} catch (SubscriptionServiceException e) {
			assertEquals(MessageFlowErrorReasons.SUB_KEYWORD_NOT_FOUND, e.getReason());
		}

	}

	@Test
	public void whenNoInitialContentAvailable_ForAppWithSubcategory_WithAdditionalNonSupportedCategory()
			throws Exception {
		SubCategory cat1 = createSubCategory("cat1");
		SubCategory cat2 = createSubCategory("cat2");
		subscription.setSupportedSubCategories(Arrays.asList(cat1, cat2));

		SubscriptionContentRelease contentRelease = new SubscriptionContentRelease();
		contentRelease.setContents(Arrays.asList(createContent("First Subscription Content for ca1", cat1),
				createContent("First Subscription Content for ca2", cat2),
				createContent("Content with invalid content", createSubCategory("cat3"))));
		contentRelease.setScheduledDispatchTimeFrom(new DateTime(2010, 10, 4, 9, 0, 0, 0));
		contentRelease.setPeriodicity(new Periodicity(PeriodUnit.HOUR, 1, 10));

		// assertNull("There shouldn't be any before update",
		// subscription.getScheduledContentList());
		mockContext.checking(new Expectations() {
			{
				exactly(1).of(subsDataRepo).getUpdatableContents(with(any(String.class)), with(any(DateTime.class)));
				will(returnValue(null));

			}
		});

		try {
			subscription.updateServiceData(Arrays.asList((ContentRelease) contentRelease));
			fail("Should fail with subcategory not found exception");
		} catch (SubscriptionServiceException e) {
			assertEquals(MessageFlowErrorReasons.SUB_KEYWORD_NOT_FOUND, e.getReason());
		}

	}

	@Test
	public void whenScheduledContentAvailable_ForAppWithSubcategory_Success() throws Exception {
		final SubCategory cat1 = createSubCategory("cat1");
		final SubCategory cat2 = createSubCategory("cat2");
		subscription.setSupportedSubCategories(Arrays.asList(cat1, cat2));

		final SubscriptionContentRelease scheduledContent1 = new SubscriptionContentRelease();
		scheduledContent1.setContents(Arrays.asList(createContent("Scheduled Content for ca1", cat1),
				createContent("Scheduled Content for ca2", cat2)));
		scheduledContent1.setScheduledDispatchTimeFrom(new DateTime(2010, 10, 4, 8, 0, 0, 0));
		final List<SubscriptionContentRelease> scheduledContentList = new ArrayList<SubscriptionContentRelease>();
		scheduledContentList.add(scheduledContent1);

		// subscription.setScheduledContentList(scheduledContentList);
		// assertEquals("There should be initial content", 1,
		// subscription.getScheduledContentList().size());

		SubscriptionContentRelease updateContent = new SubscriptionContentRelease();
		final String updateTxtCat1 = "First Subscription Content for ca1";
		final String updateTxtCat2 = "First Subscription Content for ca2";
		updateContent
				.setContents(Arrays.asList(createContent(updateTxtCat1, cat1), createContent(updateTxtCat2, cat2)));
		updateContent.setScheduledDispatchTimeFrom(new DateTime(2010, 10, 4, 9, 0, 0, 0));
		updateContent.setScheduledDispatchTimeTo(new DateTime(2010, 10, 4, 9, 10, 0, 0));
		final Periodicity periodicity = new Periodicity(PeriodUnit.HOUR, 1, 10);
		updateContent.setPeriodicity(periodicity);

		mockContext.checking(new Expectations() {
			{
				exactly(1).of(subsDataRepo).getUpdatableContents(with(any(String.class)), with(any(DateTime.class)));
				will(returnValue(scheduledContentList));

				SubscriptionContentRelease expected = createContentRelease(0L, periodicity,
						Arrays.asList(createContent(updateTxtCat1, cat1), createContent(updateTxtCat2, cat2)),
						new DateTime(2010, 10, 4, 9, 0, 0, 0));
				exactly(1).of(subsDataRepo).addOrUpdateContent(
						with(allOf(listOfSize(2), subsContentWith(Arrays.asList(scheduledContent1, expected)))));

			}
		});

		subscription.updateServiceData(Arrays.asList((ContentRelease) updateContent));

		// List<SubscriptionContentRelease> scheduledContent =
		// subscription.getScheduledContentList();
		// assertEquals("Number of scheduled releases", 2,
		// scheduledContent.size());

	}

	@Test
	public void updateExsistingContent_ForAppWithOutSubcategory_Success() throws Exception {

		SubscriptionContentRelease scheduledContent1 = new SubscriptionContentRelease();
		scheduledContent1.setId(1L);
		scheduledContent1.setContents(Arrays.asList(createContent("Scheduled Content", null)));
		scheduledContent1.setScheduledDispatchTimeFrom(new DateTime(2010, 10, 4, 8, 0, 0, 0));
		scheduledContent1.setScheduledDispatchTimeTo(new DateTime(2010, 10, 4, 8, 10, 0, 0));
		final List<SubscriptionContentRelease> scheduledContentList = new ArrayList<SubscriptionContentRelease>();
		scheduledContentList.add(scheduledContent1);
		// subscription.setScheduledContentList(scheduledContentList);
		// assertEquals("There should be initial content", 1,
		// subscription.getScheduledContentList().size());

		SubscriptionContentRelease updateContent = new SubscriptionContentRelease();
		final String updatedTxt = "Updated Content";
		updateContent.setContents(Arrays.asList(createContent(updatedTxt, null)));
		updateContent.setId(1L);
		final Periodicity periodicity = new Periodicity(PeriodUnit.HOUR, 1, 10);
		updateContent.setPeriodicity(periodicity);
		// updateContent.setScheduledDispatchTimeFrom(new DateTime(2010, 10, 4,
		// 8, 0, 0, 0));
		// updateContent.setScheduledDispatchTimeTo(new DateTime(2010, 10, 4, 8,
		// 10, 0, 0));

		mockContext.checking(new Expectations() {
			{
				exactly(1).of(subsDataRepo).getUpdatableContents(with(any(String.class)), with(any(DateTime.class)));
				will(returnValue(scheduledContentList));

				SubscriptionContentRelease expected = createContentRelease(0L, periodicity,
						Arrays.asList(createContent(updatedTxt, null)), new DateTime(2010, 10, 4, 8, 0, 0, 0));
				exactly(1).of(subsDataRepo).addOrUpdateContent(
						with(allOf(listOfSize(1), subsContentWith(Arrays.asList(expected)))));

			}
		});

		subscription.updateServiceData(Arrays.asList((ContentRelease) updateContent));

		// List<SubscriptionContentRelease> scheduledContent =
		// subscription.getScheduledContentList();
		// assertEquals("Number of scheduled releases", 1,
		// scheduledContent.size());
		// List<Content> contents = scheduledContent.get(0).getContents();
		// assertEquals(updatedTxt, contents.get(0).getContent().getMessage());

	}

	@Test
	public void updateExsistingContent_ForAppWithSubcategory_Success() throws Exception {
		final SubCategory cat1 = createSubCategory("cat1");
		final SubCategory cat2 = createSubCategory("cat2");
		subscription.setSupportedSubCategories(Arrays.asList(cat1, cat2));

		SubscriptionContentRelease scheduledContent1 = new SubscriptionContentRelease();
		scheduledContent1.setId(1L);
		scheduledContent1.setContents(Arrays.asList(createContent("Scheduled Content for ca1", cat1),
				createContent("Scheduled Content for ca2", cat2)));
		scheduledContent1.setScheduledDispatchTimeFrom(new DateTime(2010, 10, 4, 8, 0, 0, 0));
		scheduledContent1.setScheduledDispatchTimeTo(new DateTime(2010, 10, 4, 8, 10, 0, 0));
		final List<SubscriptionContentRelease> scheduledContentList = new ArrayList<SubscriptionContentRelease>();
		scheduledContentList.add(scheduledContent1);
		// subscription.setScheduledContentList(scheduledContentList);
		// assertEquals("There should be initial content", 1,
		// subscription.getScheduledContentList().size());

		SubscriptionContentRelease updateContent = new SubscriptionContentRelease();
		final String updateTxtCat1 = "Updated Content for ca1";
		final String updateTxtCat2 = "Updated Content for ca2";
		updateContent
				.setContents(Arrays.asList(createContent(updateTxtCat1, cat1), createContent(updateTxtCat2, cat2)));
		updateContent.setId(1L);
		final Periodicity periodicity = new Periodicity(PeriodUnit.HOUR, 1, 10);
		updateContent.setPeriodicity(periodicity);

		mockContext.checking(new Expectations() {
			{
				exactly(1).of(subsDataRepo).getUpdatableContents(with(any(String.class)), with(any(DateTime.class)));
				will(returnValue(scheduledContentList));

				SubscriptionContentRelease expected = createContentRelease(0L, periodicity,
						Arrays.asList(createContent(updateTxtCat1, cat1), createContent(updateTxtCat2, cat2)),
						new DateTime(2010, 10, 4, 8, 0, 0, 0));
				exactly(1).of(subsDataRepo).addOrUpdateContent(
						with(allOf(listOfSize(1), subsContentWith(Arrays.asList(expected)))));

			}
		});

		subscription.updateServiceData(Arrays.asList((ContentRelease) updateContent));

//		List<SubscriptionContentRelease> scheduledContent = subscription.getScheduledContentList();
//		assertEquals("Number of scheduled releases", 1, scheduledContent.size());
//		List<Content> contents = scheduledContent.get(0).getContents();
//		assertEquals(updateTxtCat1, contents.get(0).getContent().getMessage());
//		assertEquals(updateTxtCat2, contents.get(1).getContent().getMessage());

	}

	@Test
	public void updateExsistingContent_ForAppWithSubcategory_WithInvalidSubCategory() throws Exception {
		SubCategory cat1 = createSubCategory("cat1");
		SubCategory cat2 = createSubCategory("cat2");
		subscription.setSupportedSubCategories(Arrays.asList(cat1, cat2));

		SubscriptionContentRelease scheduledContent1 = new SubscriptionContentRelease();
		scheduledContent1.setId(1L);
		scheduledContent1.setContents(Arrays.asList(createContent("Scheduled Content for ca1", cat1),
				createContent("Scheduled Content for ca2", cat2)));
		scheduledContent1.setScheduledDispatchTimeFrom(new DateTime(2010, 10, 4, 8, 0, 0, 0));
		final List<SubscriptionContentRelease> scheduledContentList = new ArrayList<SubscriptionContentRelease>();
		scheduledContentList.add(scheduledContent1);
//		subscription.setScheduledContentList(scheduledContentList);
//		assertEquals("There should be initial content", 1, subscription.getScheduledContentList().size());

		SubscriptionContentRelease updateContent = new SubscriptionContentRelease();
		updateContent.setContents(Arrays.asList(createContent("Updated Content for ca1", cat1),
				createContent("Invalid Content", createSubCategory("cat3"))));
		updateContent.setId(1L);
		updateContent.setPeriodicity(new Periodicity(PeriodUnit.HOUR, 1, 10));

		mockContext.checking(new Expectations() {
			{
				exactly(1).of(subsDataRepo).getUpdatableContents(with(any(String.class)), with(any(DateTime.class)));
				will(returnValue(scheduledContentList));


			}
		});


		try {
			subscription.updateServiceData(Arrays.asList((ContentRelease) updateContent));
			fail("Should fail with sub keyword not supported exception.");
		} catch (SubscriptionServiceException e) {
			assertEquals(MessageFlowErrorReasons.SUB_KEYWORD_NOT_FOUND, e.getReason());
		}

	}

}
