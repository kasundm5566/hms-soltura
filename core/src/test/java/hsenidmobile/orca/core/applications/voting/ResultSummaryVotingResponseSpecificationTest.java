package hsenidmobile.orca.core.applications.voting;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import hsenidmobile.orca.core.applications.exception.VoteletException;
import hsenidmobile.orca.core.applications.voting.impl.ResultSummaryVotingResponseSpecification;
import hsenidmobile.orca.core.mock.MobileOperators;
import hsenidmobile.orca.core.model.LocalizedMessage;
import hsenidmobile.orca.core.model.Msisdn;
import hsenidmobile.orca.core.model.message.MessageContext;
import hsenidmobile.orca.core.model.message.MoResponse;
import hsenidmobile.orca.core.model.message.SmsMessage;
import hsenidmobile.orca.core.repository.VoteRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ResultSummaryVotingResponseSpecificationTest {

	private ResultSummaryVotingResponseSpecification responseSpecification;
	private Mockery context;
	private VotingService votingService;
	private VoteRepository voteRepository;

	@Before
	public void setUp() throws Exception {
		responseSpecification = new ResultSummaryVotingResponseSpecification();
		responseSpecification.setResponseTemplate(Arrays.asList(new LocalizedMessage("Current Results.\n%s\nVote again.",
				Locale.ENGLISH)));
		context = new Mockery();
		voteRepository = context.mock(VoteRepository.class);
		votingService = new VotingService();
		votingService.setId(1L);
		votingService.setVoteRepository(voteRepository);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGenerateResponse() throws VoteletException {
		final List<VoteResultSummary> results = new ArrayList<VoteResultSummary>();
		VoteResultSummary sum1 = createResultSummay("candidate1", 1);
		results.add(sum1);

		context.checking(new Expectations() {
			{
				allowing(voteRepository).getVotingSummary(with(any(Long.class)));
				will(returnValue(results));
			}
		});

		final SmsMessage message = new SmsMessage();
		Msisdn senderAddress = new Msisdn("123456789", MobileOperators.DIALOG);
		message.setSenderAddress(senderAddress);
		Msisdn receiverAddress = new Msisdn("456789123", MobileOperators.DIALOG);
		message.addReceiverAddress(receiverAddress);

		MoResponse generateResponse = responseSpecification.generateResponse(votingService, message, null,
				new MessageContext(), Locale.ENGLISH);
		assertNotNull("Generated Resp cannot be null", generateResponse.getDataResponse());
		assertEquals("Sender Address", receiverAddress, generateResponse.getDataResponse().getMessage()
				.getSenderAddress());
		assertEquals("Receiver Address", senderAddress, generateResponse.getDataResponse().getMessage()
				.getReceiverAddresses().get(0));
		assertEquals("Msg txt", "Current Results.\ncandidate1 : 1\nVote again.", generateResponse.getDataResponse()
				.getMessage().getMessage());
	}

	@Test
	public void testGenerateResponse_WhenTemplateNotFoundForLocale() throws VoteletException {
		final List<VoteResultSummary> results = new ArrayList<VoteResultSummary>();
		VoteResultSummary sum1 = createResultSummay("candidate1", 1);
		results.add(sum1);

		context.checking(new Expectations() {
			{
				allowing(voteRepository).getVotingSummary(with(any(Long.class)));
				will(returnValue(results));
			}
		});

		final SmsMessage message = new SmsMessage();
		Msisdn senderAddress = new Msisdn("123456789", MobileOperators.DIALOG);
		message.setSenderAddress(senderAddress);
		Msisdn receiverAddress = new Msisdn("456789123", MobileOperators.DIALOG);
		message.addReceiverAddress(receiverAddress);

		MoResponse generateResponse = responseSpecification.generateResponse(votingService, message, null,
				new MessageContext(), Locale.FRENCH);
		assertNotNull("Generated Resp cannot be null", generateResponse.getDataResponse());
		assertEquals("Sender Address", receiverAddress, generateResponse.getDataResponse().getMessage()
				.getSenderAddress());
		assertEquals("Receiver Address", senderAddress, generateResponse.getDataResponse().getMessage()
				.getReceiverAddresses().get(0));
		assertEquals("Msg txt", "candidate1 : 1", generateResponse.getDataResponse()
				.getMessage().getMessage());
	}

	@Test
	public void testGenerateResponse_WhenLongListOfCandidatesAvailable() throws VoteletException {
		final List<VoteResultSummary> results = new ArrayList<VoteResultSummary>();
		for (int i = 1; i <= 12; i++) {
			results.add(createResultSummay("candidate" + i, i));
		}

		context.checking(new Expectations() {
			{
				allowing(voteRepository).getVotingSummary(with(any(Long.class)));
				will(returnValue(results));
			}
		});

		final SmsMessage message = new SmsMessage();
		Msisdn senderAddress = new Msisdn("123456789", MobileOperators.DIALOG);
		message.setSenderAddress(senderAddress);
		Msisdn receiverAddress = new Msisdn("456789123", MobileOperators.DIALOG);
		message.addReceiverAddress(receiverAddress);

		MoResponse generateResponse = responseSpecification.generateResponse(votingService, message, null,
				new MessageContext(), Locale.ENGLISH);
		assertNotNull("Generated Resp cannot be null", generateResponse.getDataResponse());
		assertEquals("Sender Address", receiverAddress, generateResponse.getDataResponse().getMessage()
				.getSenderAddress());
		assertEquals("Receiver Address", senderAddress, generateResponse.getDataResponse().getMessage()
				.getReceiverAddresses().get(0));
		assertEquals("Msg txt", "Current Results.\ncandidate1 : 1, candidate2 : 2, candidate3 : 3\nVote again.", generateResponse.getDataResponse()
				.getMessage().getMessage());
	}

	private VoteResultSummary createResultSummay(String candidateName, int voteCount) {
		VoteResultSummary sum1 = new VoteResultSummary();
		sum1.setCandidateName(candidateName);
		sum1.setLastUpdateTime(new Date());
		sum1.setTotalVoteCount(voteCount);
		return sum1;
	}

}
