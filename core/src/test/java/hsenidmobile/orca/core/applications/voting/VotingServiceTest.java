/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hsenidmobile.orca.core.applications.voting;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import hsenidmobile.orca.core.applications.ApplicationImpl;
import hsenidmobile.orca.core.applications.voting.impl.NoVotingResponseSpecification;
import hsenidmobile.orca.core.flow.MessageFlowErrorReasons;
import hsenidmobile.orca.core.flow.MessageFlowException;
import hsenidmobile.orca.core.flow.dispatch.aventuraapi.AventuraApiMtMessageDispatcher;
import hsenidmobile.orca.core.flow.event.CommandEvent;
import hsenidmobile.orca.core.model.LocalizedMessage;
import hsenidmobile.orca.core.model.Msisdn;
import hsenidmobile.orca.core.model.Status;
import hsenidmobile.orca.core.model.SubCategory;
import hsenidmobile.orca.core.model.message.Message;
import hsenidmobile.orca.core.model.message.MessageContext;
import hsenidmobile.orca.core.model.message.MoResponse;
import hsenidmobile.orca.core.repository.VoteRepository;
import hsenidmobile.orca.core.testutil.TestUtil;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.joda.time.DateTime;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * $LastChangedDate: $ $LastChangedBy: $ $LastChangedRevision: $
 */

public class VotingServiceTest {

	private Mockery context;
	private VotingService votingService;
	private Message message;
	private MessageContext messageContext;
	private VoteRepository voteRepository;
	private VotingEventListener votingEventListener;
	private AventuraApiMtMessageDispatcher messageDispatcher;

	@Before
	public void setUp() {
		context = new Mockery();
		votingService = new VotingService();
		message = context.mock(Message.class);
		messageContext = new MessageContext();
		voteRepository = context.mock(VoteRepository.class);
		votingEventListener = context.mock(VotingEventListener.class);
		messageDispatcher = context.mock(AventuraApiMtMessageDispatcher.class);

		ApplicationImpl application = new ApplicationImpl();
		application.setAppName("App1");
		application.setStartDate(new DateTime(2010, 9, 1, 0, 0, 0, 0));
		application.setEndDate(new DateTime(2010, 9, 30, 0, 0, 0, 0));
		application.setStatus(Status.ACTIVE);
		votingService.setApplication(application);

		messageContext.setApplication(application);

		votingService.setId(1L);
		votingService.setVoteRepository(voteRepository);
		votingService.setVotingEventListener(votingEventListener);
		votingService.setAventuraApiMtMessageDispatcher(messageDispatcher);

		TestUtil.setJodaSystemTime(new DateTime(2010, 9, 10, 0, 0, 0, 0));
	}

	@After
	public void tearDown() {
	}

	private SubCategory createCandidate(String candidateCode, String description) {
		SubCategory candidate1 = new SubCategory();
		candidate1.setKeyword(candidateCode);
		candidate1.setDescription(Arrays.asList(new LocalizedMessage(description, Locale.ENGLISH)));

		return candidate1;
	}

	@Test
	public void testSuccesfullVote_WhenMultipleVoting_WhenSubCategoryAvailable() throws Exception {

		votingService.setSupportedSubCategories(Arrays.asList(createCandidate("c1", "Candidate1")));
		votingService.setResponseSpecification(new NoVotingResponseSpecification());
		votingService.setOneVotePerNumber(false);

		CommandEvent commandEvent = new CommandEvent(message, messageContext, "c1");

		context.checking(new Expectations() {
			{
				allowing(message).getSenderAddress();
				will(returnValue(new Msisdn("12345678912", "Op1")));

				allowing(message).getMessage();
				will(returnValue("v1 c1"));

				allowing(voteRepository).add(with(any(Vote.class)));

				allowing(votingEventListener).successfulVote(with(any(Long.class)), with(any(Vote.class)));

				allowing(messageDispatcher).dispatchIndividualMessages(with(any(String.class)), with(any(List.class)));
			}
		});

		votingService.onDataMessage(commandEvent);
		votingService.onDataMessage(commandEvent);
		votingService.onDataMessage(commandEvent);
	}

	@Test
	public void testSuccesfullVote_WhenOneVotePerMsisdn_WhenSubCategoryAvailable_whenVotingFirstTime()
			throws MessageFlowException {
		votingService.setSupportedSubCategories(Arrays.asList(createCandidate("c1", "Candidate1")));
		votingService.setResponseSpecification(new NoVotingResponseSpecification());
		votingService.setOneVotePerNumber(true);

		CommandEvent commandEvent = new CommandEvent(message, messageContext, "c1");

		context.checking(new Expectations() {
			{
				Msisdn senderNo = new Msisdn("12345678912", "Op1");

				allowing(message).getSenderAddress();
				will(returnValue(senderNo));

				allowing(message).getReceiverAddresses();
				will(returnValue(Arrays.asList(new Msisdn("9696", "Op1"))));

				allowing(message).getMessage();
				will(returnValue("v1 c1"));

				allowing(voteRepository).hasVoted(with(senderNo), with(1L));
				will(returnValue(false));

				allowing(voteRepository).add(with(any(Vote.class)));

				allowing(votingEventListener).successfulVote(with(any(Long.class)), with(any(Vote.class)));

				allowing(messageDispatcher).dispatchIndividualMessages(with(any(String.class)), with(any(List.class)));
			}
		});

		votingService.onDataMessage(commandEvent);

	}

	@Test
	public void testSuccesfullVote_WhenOneVotePerMsisdn_WhenSubCategoryAvailable_whenVotingSecondTime() {
		votingService.setSupportedSubCategories(Arrays.asList(createCandidate("c1", "Candidate1")));
		votingService.setResponseSpecification(new NoVotingResponseSpecification());
		votingService.setOneVotePerNumber(true);

		CommandEvent commandEvent = new CommandEvent(message, messageContext, "c1");

		context.checking(new Expectations() {
			{
				Msisdn senderNo = new Msisdn("12345678912", "Op1");

				allowing(message).getSenderAddress();
				will(returnValue(senderNo));

				allowing(message).getReceiverAddresses();
				will(returnValue(Arrays.asList(new Msisdn("9696", "Op1"))));

				allowing(message).getMessage();
				will(returnValue("v1 c1"));

				allowing(voteRepository).hasVoted(with(senderNo), with(1L));
				will(returnValue(true));

				allowing(voteRepository).add(with(any(Vote.class)));

				allowing(votingEventListener).successfulVote(with(any(Long.class)), with(any(Vote.class)));

				allowing(messageDispatcher).dispatchIndividualMessages(with(any(String.class)), with(any(List.class)));
			}
		});

		try {
			votingService.onDataMessage(commandEvent);
			fail("Should throw Already voted exception");
		} catch (MessageFlowException e) {
			assertEquals(e.getReason(), MessageFlowErrorReasons.ALREADY_VOTED);
		}

	}

	@Test
	public void testSuccesfullResultQueryWhenSubCategoryAvailable_WhenResultsPublic() throws Exception {
		votingService.setSupportedSubCategories(Arrays.asList(createCandidate("c1", "Candidate1")));
		votingService.setResponseSpecification(new NoVotingResponseSpecification());
		votingService.setOneVotePerNumber(false);
		votingService.setVoteResultPublic(true);

		CommandEvent commandEvent = new CommandEvent(message, messageContext, "result");
		commandEvent.setSystemWideKeyword(true);

		context.checking(new Expectations() {
			{
				Msisdn senderNo = new Msisdn("12345678912", "Op1");

				allowing(message).getSenderAddress();
				will(returnValue(senderNo));

				allowing(message).getReceiverAddresses();
				will(returnValue(Arrays.asList(new Msisdn("9696", "Op1"))));

				allowing(message).getCorrelationId();
				will(returnValue(1L));

				allowing(message).getMessage();
				will(returnValue("v1 result"));

				allowing(voteRepository).hasVoted(with(senderNo), with(1L));
				will(returnValue(false));

				VoteResultSummary summary = new VoteResultSummary();
				summary.setCandidateCode("c1");
				summary.setTotalVoteCount(50);
				summary.setCandidateName("Candidate1");
				allowing(voteRepository).getVotingSummary(with(1L));
				will(returnValue(Arrays.asList(summary)));

				allowing(messageDispatcher).dispatchIndividualMessages(with(any(String.class)), with(any(List.class)));
			}
		});

		MoResponse response = votingService.onDataMessage(commandEvent);
	}

	@Test
	public void testSuccesfullResultQueryWhenSubCategoryAvailable_WhenResultsPrivate_WhenMsisdnNotVoted() {
		votingService.setSupportedSubCategories(Arrays.asList(createCandidate("c1", "Candidate1")));
		votingService.setResponseSpecification(new NoVotingResponseSpecification());
		votingService.setOneVotePerNumber(false);
		votingService.setVoteResultPublic(false);

		CommandEvent commandEvent = new CommandEvent(message, messageContext, "result");
		commandEvent.setSystemWideKeyword(true);

		context.checking(new Expectations() {
			{
				Msisdn senderNo = new Msisdn("12345678912", "Op1");

				allowing(message).getSenderAddress();
				will(returnValue(senderNo));

				allowing(message).getReceiverAddresses();
				will(returnValue(Arrays.asList(new Msisdn("9696", "Op1"))));

				allowing(message).getMessage();
				will(returnValue("v1 result"));

				allowing(voteRepository).hasVoted(with(senderNo), with(1L));
				will(returnValue(false)); //Not Voted

				VoteResultSummary summary = new VoteResultSummary();
				summary.setCandidateCode("c1");
				summary.setTotalVoteCount(50);
				summary.setCandidateName("Candidate1");
				allowing(voteRepository).getVotingSummary(with(1L));
				will(returnValue(Arrays.asList(summary)));

				allowing(messageDispatcher).dispatchIndividualMessages(with(any(String.class)), with(any(List.class)));
			}
		});

		try {
			votingService.onDataMessage(commandEvent);
			fail("Should fail with voting result private exception");
		} catch (MessageFlowException e) {
			assertEquals(MessageFlowErrorReasons.VOTING_RESULT_PRIVATE, e.getReason());
		}
	}

	@Test
	public void testSuccesfullResultQueryWhenSubCategoryAvailable_WhenResultsPrivate_WhenMsisdnVoted() throws MessageFlowException {
		votingService.setSupportedSubCategories(Arrays.asList(createCandidate("c1", "Candidate1")));
		votingService.setResponseSpecification(new NoVotingResponseSpecification());
		votingService.setOneVotePerNumber(false);
		votingService.setVoteResultPublic(false);

		CommandEvent commandEvent = new CommandEvent(message, messageContext, "result");
		commandEvent.setSystemWideKeyword(true);

		context.checking(new Expectations() {
			{
				Msisdn senderNo = new Msisdn("12345678912", "Op1");

				allowing(message).getSenderAddress();
				will(returnValue(senderNo));

				allowing(message).getReceiverAddresses();
				will(returnValue(Arrays.asList(new Msisdn("9696", "Op1"))));

				allowing(message).getCorrelationId();
				will(returnValue(1L));

				allowing(message).getMessage();
				will(returnValue("v1 result"));

				allowing(voteRepository).hasVoted(with(senderNo), with(1L));
				will(returnValue(true)); //Voted

				VoteResultSummary summary = new VoteResultSummary();
				summary.setCandidateCode("c1");
				summary.setTotalVoteCount(50);
				summary.setCandidateName("Candidate1");
				allowing(voteRepository).getVotingSummary(with(1L));
				will(returnValue(Arrays.asList(summary)));

				allowing(messageDispatcher).dispatchIndividualMessages(with(any(String.class)), with(any(List.class)));
			}
		});

		votingService.onDataMessage(commandEvent);
	}

	@Test(expected = IllegalStateException.class)
	public void testVote_WhenSubCategoryNotAvailable() throws Exception {

		CommandEvent commandEvent = new CommandEvent(message, messageContext, "");

		votingService.onDataMessage(commandEvent);
	}

	@Test
	public void testVote_ForWrongCandidateCode() throws Exception {

		votingService.setSupportedSubCategories(Arrays.asList(createCandidate("c1", "Candidate1")));
		votingService.setResponseSpecification(new NoVotingResponseSpecification());
		votingService.setOneVotePerNumber(false);

		CommandEvent commandEvent = new CommandEvent(message, messageContext, "abc");

		context.checking(new Expectations() {
			{
				allowing(message).getSenderAddress();
				will(returnValue(new Msisdn("12345678912", "Op1")));

				allowing(message).getMessage();
				will(returnValue("v1 abc"));

				allowing(voteRepository).add(with(any(Vote.class)));

				allowing(votingEventListener).successfulVote(with(any(Long.class)), with(any(Vote.class)));

				allowing(messageDispatcher).dispatchIndividualMessages(with(any(String.class)), with(any(List.class)));
			}
		});

		try {
			votingService.onDataMessage(commandEvent);
			fail("Should throw SubCategory not found voted exception");
		} catch (MessageFlowException e) {
			assertEquals(e.getReason(), MessageFlowErrorReasons.SUB_KEYWORD_NOT_FOUND);
		}

	}

	@Test
	public void testVote_WhenApplicationExpired() throws Exception {
		//Change current date to date after application's end date
		TestUtil.setJodaSystemTime(new DateTime(2010, 10, 1, 0, 0, 0, 0));

		votingService.setSupportedSubCategories(Arrays.asList(createCandidate("c1", "Candidate1")));
		votingService.setResponseSpecification(new NoVotingResponseSpecification());
		votingService.setOneVotePerNumber(false);

		CommandEvent commandEvent = new CommandEvent(message, messageContext, "abc");

		context.checking(new Expectations() {
			{
				allowing(message).getSenderAddress();
				will(returnValue(new Msisdn("12345678912", "Op1")));

				allowing(message).getMessage();
				will(returnValue("v1 abc"));

				allowing(voteRepository).add(with(any(Vote.class)));

				allowing(votingEventListener).successfulVote(with(any(Long.class)), with(any(Vote.class)));

				allowing(messageDispatcher).dispatchIndividualMessages(with(any(String.class)), with(any(List.class)));
			}
		});

		try {
			votingService.onDataMessage(commandEvent);
			fail("Should throw App not active exception");
		} catch (MessageFlowException e) {
			assertEquals(e.getReason(), MessageFlowErrorReasons.APP_NOT_ACTIVE);
		}

	}

}
