/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.flow;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import hsenidmobile.orca.core.applications.Service;
import hsenidmobile.orca.core.applications.exception.ApplicationException;
import hsenidmobile.orca.core.flow.event.MessageFlowEvent;
import hsenidmobile.orca.core.mock.MobileOperators;
import hsenidmobile.orca.core.model.Application;
import hsenidmobile.orca.core.model.InvalidRequest;
import hsenidmobile.orca.core.model.Msisdn;
import hsenidmobile.orca.core.model.SubCategory;
import hsenidmobile.orca.core.model.message.MessageContext;
import hsenidmobile.orca.core.model.message.SmsMessage;
import hsenidmobile.orca.core.services.InvalidRequestService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.Before;
import org.junit.Test;

/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class ApplicationSubkeywordExtractorTest {

	private Mockery mockContext;
	private ApplicationSubkeywordExtractor extractor;
	private Application application;
	private Service service;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		mockContext = new Mockery();
		application = mockContext.mock(Application.class);
		service = mockContext.mock(Service.class);

		extractor = new ApplicationSubkeywordExtractor();
		List<String> specialKeywordList = new ArrayList<String>();
		specialKeywordList.add("result");
		specialKeywordList.add("all");
		extractor.setSpecialSysWideKeywords(specialKeywordList);
		extractor.setInvalidRequestService(new InvalidRequestService() {

			@Override
			public void invalidRequestReceived(InvalidRequest invalidRequest) {
			}

			@Override
			public int getInvalidRequestSummary(String appId) {
				// TODO Auto-generated method stub
				return 0;
			}
		});

		mockContext.checking(new Expectations() {
			{
				allowing(application).getAppId();
				will(returnValue("APP1"));

				allowing(application).getAppName();
				will(returnValue("APP1"));

				allowing(application).getService();
				will(returnValue(service));
			}
		});
	}

	/**
	 * Test method for
	 * {@link hsenidmobile.orca.core.flow.ApplicationSubkeywordExtractor#setSubkeyword(hsenidmobile.orca.core.model.Application, java.lang.String)}
	 * .
	 *
	 * @throws ApplicationException
	 *
	 */
	@Test
	public void testSetSubkeyword() throws ApplicationException {
		mockContext.checking(new Expectations() {
			{
				SubCategory cat1 = new SubCategory();
				cat1.setKeyword("subkey1");
				SubCategory cat2 = new SubCategory();
				cat2.setKeyword("subkey2");

				allowing(service).isSubCategoriesSupported();
				will(returnValue(true));

				allowing(service).getSupportedSubCategories();
				will(returnValue(Arrays.asList(cat1, cat2)));
			}
		});

		SmsMessage msg = new SmsMessage();
		msg.setReceiverAddresses(Arrays.asList(new Msisdn("9696", "Op1")));
		MessageFlowEvent<SmsMessage> flowEvent = new MessageFlowEvent<SmsMessage>(msg, new MessageContext());

		msg.setMessage("subkey1");
		extractor.setSubkeyword(application, flowEvent);
		assertEquals("Extracted Subkeywords", "subkey1", flowEvent.getSubKeyword());
		assertFalse("Not a systemwide keyword", flowEvent.isSystemWideKeyword());

		msg.setMessage("subkey1    ");
		extractor.setSubkeyword(application, flowEvent);
		assertEquals("Extracted Subkeywords", "subkey1", flowEvent.getSubKeyword());
		assertFalse("Not a systemwide keyword", flowEvent.isSystemWideKeyword());

		msg.setMessage("suBkeY1");
		extractor.setSubkeyword(application, flowEvent);
		assertEquals("Extracted Subkeywords", "subkey1", flowEvent.getSubKeyword());
		assertEquals("Msg After extraction", "", msg.getMessage());
		assertFalse("Not a systemwide keyword", flowEvent.isSystemWideKeyword());

		msg.setMessage("subkey1   asfdasd");
		extractor.setSubkeyword(application, flowEvent);
		assertEquals("Extracted Subkeywords", "subkey1", flowEvent.getSubKeyword());
		assertEquals("Msg After extraction", "asfdasd", msg.getMessage());
		assertFalse("Not a systemwide keyword", flowEvent.isSystemWideKeyword());

	}

	/**
	 * Test method for
	 * {@link hsenidmobile.orca.core.flow.ApplicationSubkeywordExtractor#setSubkeyword(hsenidmobile.orca.core.model.Application, java.lang.String)}
	 * .
	 *
	 */
	@Test
	public void testSetSubkeywordForSystemWideSubkeys_WhenAppSupportsSubCategories() throws ApplicationException {

		mockContext.checking(new Expectations() {
			{
				SubCategory cat1 = new SubCategory();
				cat1.setKeyword("subkey1");
				SubCategory cat2 = new SubCategory();
				cat2.setKeyword("subkey2");

				allowing(service).isSubCategoriesSupported();
				will(returnValue(true));

				allowing(service).getSupportedSubCategories();
				will(returnValue(Arrays.asList(cat1, cat2)));
			}
		});

		SmsMessage msg = new SmsMessage();
		MessageFlowEvent<SmsMessage> flowEvent = new MessageFlowEvent<SmsMessage>(msg, new MessageContext());

		msg.setMessage("result");
		extractor.setSubkeyword(application, flowEvent);
		assertEquals("Extracted Subkeywords", "result", flowEvent.getSubKeyword());
		assertEquals("Msg After extraction", "result", msg.getMessage());
		assertTrue("Should be a systemwide keyword", flowEvent.isSystemWideKeyword());

		msg.setMessage("result    ");
		extractor.setSubkeyword(application, flowEvent);
		assertEquals("Extracted Subkeywords", "result", flowEvent.getSubKeyword());
		assertTrue("Should be a systemwide keyword", flowEvent.isSystemWideKeyword());

		msg.setMessage("ResUlt");
		extractor.setSubkeyword(application, flowEvent);
		assertEquals("Extracted Subkeywords", "result", flowEvent.getSubKeyword());
		assertEquals("Msg After extraction", "ResUlt", msg.getMessage());
		assertTrue("Should be a systemwide keyword", flowEvent.isSystemWideKeyword());

		msg.setMessage("ResUlt   asfdasd");
		extractor.setSubkeyword(application, flowEvent);
		assertEquals("Extracted Subkeywords", "result", flowEvent.getSubKeyword());
		assertEquals("Msg After extraction", "ResUlt   asfdasd", msg.getMessage());
		assertTrue("Should be a systemwide keyword", flowEvent.isSystemWideKeyword());

	}

	@Test
	public void testSetSubkeywordForSystemWideSubkeys_WhenAppDoNotSupportsSubCategories() throws ApplicationException {

		mockContext.checking(new Expectations() {
			{
				SubCategory cat1 = new SubCategory();
				cat1.setKeyword("subkey1");
				SubCategory cat2 = new SubCategory();
				cat2.setKeyword("subkey2");

				allowing(service).isSubCategoriesSupported();
				will(returnValue(false));

				allowing(service).getSupportedSubCategories();
				will(returnValue(Collections.EMPTY_LIST));
			}
		});

		SmsMessage msg = new SmsMessage();
		MessageFlowEvent<SmsMessage> flowEvent = new MessageFlowEvent<SmsMessage>(msg, new MessageContext());

		msg.setMessage("result");
		extractor.setSubkeyword(application, flowEvent);
		assertEquals("Extracted Subkeywords", "result", flowEvent.getSubKeyword());
		assertEquals("Msg After extraction", "result", msg.getMessage());
		assertTrue("Should be a systemwide keyword", flowEvent.isSystemWideKeyword());

		msg.setMessage("result    ");
		extractor.setSubkeyword(application, flowEvent);
		assertEquals("Extracted Subkeywords", "result", flowEvent.getSubKeyword());
		assertTrue("Should be a systemwide keyword", flowEvent.isSystemWideKeyword());

		msg.setMessage("ResUlt");
		extractor.setSubkeyword(application, flowEvent);
		assertEquals("Extracted Subkeywords", "result", flowEvent.getSubKeyword());
		assertEquals("Msg After extraction", "ResUlt", msg.getMessage());
		assertTrue("Should be a systemwide keyword", flowEvent.isSystemWideKeyword());

		msg.setMessage("ResUlt   asfdasd");
		extractor.setSubkeyword(application, flowEvent);
		assertEquals("Extracted Subkeywords", "result", flowEvent.getSubKeyword());
		assertEquals("Msg After extraction", "ResUlt   asfdasd", msg.getMessage());
		assertTrue("Should be a systemwide keyword", flowEvent.isSystemWideKeyword());

	}

	/**
	 * Test method for
	 * {@link hsenidmobile.orca.core.flow.ApplicationSubkeywordExtractor#setSubkeyword(hsenidmobile.orca.core.model.Application, java.lang.String)}
	 * .
	 *
	 */
	@Test
	public void testSetSubkeywordWhereAppDoNotHaveSubKey() throws ApplicationException {
		mockContext.checking(new Expectations() {
			{
				SubCategory cat1 = new SubCategory();
				cat1.setKeyword("subkey1");
				SubCategory cat2 = new SubCategory();
				cat2.setKeyword("subkey2");

				allowing(service).isSubCategoriesSupported();
				will(returnValue(false));

				allowing(service).getSupportedSubCategories();
				will(returnValue(Collections.EMPTY_LIST));
			}
		});

		SmsMessage msg = new SmsMessage();
		MessageFlowEvent<SmsMessage> flowEvent = new MessageFlowEvent<SmsMessage>(msg, new MessageContext());

		msg.setMessage("");
		extractor.setSubkeyword(application, flowEvent);
		assertEquals("Extracted Subkeywords", "", flowEvent.getSubKeyword());
		assertFalse("Not a systemwide keyword", flowEvent.isSystemWideKeyword());

		msg.setMessage("  ");
		extractor.setSubkeyword(application, flowEvent);
		assertEquals("Extracted Subkeywords", "", flowEvent.getSubKeyword());
		assertFalse("Not a systemwide keyword", flowEvent.isSystemWideKeyword());

		msg.setMessage("This is the data  ");
		extractor.setSubkeyword(application, flowEvent);
		assertEquals("Extracted Subkeywords", "", flowEvent.getSubKeyword());
		assertEquals("Msg After extraction", "This is the data", msg.getMessage());
		assertFalse("Not a systemwide keyword", flowEvent.isSystemWideKeyword());

	}

	/**
	 * Test method for
	 * {@link hsenidmobile.orca.core.flow.ApplicationSubkeywordExtractor#setSubkeyword(hsenidmobile.orca.core.model.Application, java.lang.String)}
	 * .
	 *
	 */
	@Test
	public void testSetSubkeywordWhereGivenIsAlias() throws ApplicationException {
		mockContext.checking(new Expectations() {
			{
				SubCategory cat1 = new SubCategory();
				cat1.setKeyword("subkey1");
				cat1.setAliases(Arrays.asList("subkey1ali-1", "subkey1ali-2"));

				SubCategory cat2 = new SubCategory();
				cat2.setKeyword("subkey2");
				cat2.setAliases(Arrays.asList("subkey2ali-1"));

				allowing(service).isSubCategoriesSupported();
				will(returnValue(true));

				allowing(service).getSupportedSubCategories();
				will(returnValue(Arrays.asList(cat1, cat2)));
			}
		});

		SmsMessage msg = new SmsMessage();
		MessageFlowEvent<SmsMessage> flowEvent = new MessageFlowEvent<SmsMessage>(msg, new MessageContext());

		msg.setMessage("subkey1ali-1");
		extractor.setSubkeyword(application, flowEvent);
		assertEquals("Extracted Subkeywords", "subkey1", flowEvent.getSubKeyword());
		assertFalse("Not a systemwide keyword", flowEvent.isSystemWideKeyword());

		msg.setMessage("subkey1ali-2 This is the text");
		extractor.setSubkeyword(application, flowEvent);
		assertEquals("Extracted Subkeywords", "subkey1", flowEvent.getSubKeyword());
		assertEquals("Msg After extraction", "This is the text", msg.getMessage());
		assertFalse("Not a systemwide keyword", flowEvent.isSystemWideKeyword());

	}

	/**
	 * Test method for
	 * {@link hsenidmobile.orca.core.flow.ApplicationSubkeywordExtractor#setSubkeyword(hsenidmobile.orca.core.model.Application, java.lang.String)}
	 * .
	 *
	 */
	@Test
	public void testSetSubkeywordInvalidGiven() {
		mockContext.checking(new Expectations() {
			{
				SubCategory cat1 = new SubCategory();
				cat1.setKeyword("subkey1");
				cat1.setAliases(Arrays.asList("subkey1ali-1", "subkey1ali-2"));

				SubCategory cat2 = new SubCategory();
				cat2.setKeyword("subkey2");
				cat2.setAliases(Arrays.asList("subkey2ali-1"));

				allowing(service).isSubCategoriesSupported();
				will(returnValue(true));

				allowing(service).getSupportedSubCategories();
				will(returnValue(Arrays.asList(cat1, cat2)));
			}
		});

		SmsMessage msg = new SmsMessage();
		msg.addReceiverAddress(new Msisdn("07761234566", MobileOperators.DIALOG));
		MessageFlowEvent<SmsMessage> flowEvent = new MessageFlowEvent<SmsMessage>(msg, new MessageContext());

		msg.setMessage("subasdf");
		try {
			extractor.setSubkeyword(application, flowEvent);
			fail("Should fail with incorrect subkeyword error");
		} catch (ApplicationException e) {
			assertEquals(MessageFlowErrorReasons.SUB_KEYWORD_NOT_FOUND, e.getReason());
		}
	}

}
