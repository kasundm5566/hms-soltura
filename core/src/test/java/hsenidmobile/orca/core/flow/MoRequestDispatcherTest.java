/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.flow;

import hsenidmobile.orca.core.flow.command.DataMessageCommand;
import hsenidmobile.orca.core.flow.event.CommandEvent;
import hsenidmobile.orca.core.flow.event.MessageFlowEvent;
import hsenidmobile.orca.core.mock.MobileOperators;
import hsenidmobile.orca.core.model.Application;
import hsenidmobile.orca.core.model.Msisdn;
import hsenidmobile.orca.core.model.OrcaException;
import hsenidmobile.orca.core.model.message.Message;
import hsenidmobile.orca.core.model.message.MessageContext;
import hsenidmobile.orca.core.model.message.SmsMessage;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class MoRequestDispatcherTest {

	private Mockery context;
	private Application app;

	@Before
	public void setup(){
		context = new Mockery();
		app = context.mock(Application.class);
	}

	/**
	 * Test method for {@link MessageHandler#handleDownStream(PipelineContext,hsenidmobile.orca.core.flow.event.MessageEvent)}.
	 */
	@Test
	public void testHandleDownStreamDataMessage() throws OrcaException {
		context.checking(new Expectations(){{
			allowing(app).onDataMessage(with(any(CommandEvent.class)));
		}});

		Message msg = new SmsMessage();
		msg.setMessage("hnd horos can");
		msg.setSenderAddress(new Msisdn("94776188548", MobileOperators.DIALOG));

		MessageContext context = new MessageContext();
		DataMessageCommand command = new DataMessageCommand();
		context.setApplication(app);

		MessageFlowEvent<Message> event = new MessageFlowEvent<Message>(msg, context);
		event.setCommand(command);

		MoRequestDispatcher dispatcher = new MoRequestDispatcher();
		PipelineContext pc = new PipelineContext(null, null, "a", dispatcher);
		dispatcher.handleDownStream(pc, event);

	}


	/**
	 * Test method for {@link MessageHandler#handleDownStream(PipelineContext,hsenidmobile.orca.core.flow.event.MessageEvent)}.
	 */
	@Test
	@Ignore //TODO: Fix test cases
	public void testHandleDownStreamListAllRegistrations() throws OrcaException {
		Message msg = new SmsMessage();
		msg.setMessage("showallreg");
		msg.setSenderAddress(new Msisdn("94776188548", MobileOperators.DIALOG));
		msg.addReceiverAddress(new Msisdn("9696", MobileOperators.DIALOG));

		MessageContext context = new MessageContext();

		MessageFlowEvent<Message> event = new MessageFlowEvent<Message>(msg, context);
		MoRequestDispatcher dispatcher = new MoRequestDispatcher();
		PipelineContext pc = new PipelineContext(null, null, "a", dispatcher);
		dispatcher.handleDownStream(pc, event);

	}

}
