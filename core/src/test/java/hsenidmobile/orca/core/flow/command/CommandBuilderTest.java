/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.flow.command;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import hsenidmobile.orca.core.applications.exception.ApplicationException;
import hsenidmobile.orca.core.model.message.Message;
import hsenidmobile.orca.core.model.message.SmsMessage;

import org.junit.Test;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 *
 * @version $LastChangedRevision$
 */
public class CommandBuilderTest {

	@Test
	public void testCreateUpdateDataCommand() throws ApplicationException {
		CommandBuilder cb = new CommandBuilder();
		cb.init();
		Message message = new SmsMessage();
		message.setMessage("update sub1 new text");
		Command command = cb.createCommand(message);
		assertTrue("Should be a Update Command", command instanceof AppDataUpdateCommand);
		assertEquals("Rest of the Data", "sub1 new text", message.getMessage());

		message = new SmsMessage();
		message.setMessage("UpDatE sub1 new text");
		command = cb.createCommand(message);
		assertTrue("Should be a Update Command", command instanceof AppDataUpdateCommand);
		assertEquals("Rest of the Data", "sub1 new text", message.getMessage());

		message = new SmsMessage();
		message.setMessage("UpDatE sub1 new text with	\r\n more text");
		command = cb.createCommand(message);
		assertTrue("Should be a Update Command", command instanceof AppDataUpdateCommand);
		assertEquals("Rest of the Data", "sub1 new text with	\r\n more text", message.getMessage());

	}

	@Test
	public void testCreateDataMessageCommand() throws ApplicationException {
		CommandBuilder cb = new CommandBuilder();
		cb.init();
		Message message = new SmsMessage();
		message.setMessage("stv sss");
		Command command = cb.createCommand(message);
		assertTrue("Should be a Data Command", command instanceof DataMessageCommand);
		assertEquals("Rest of the Data", "stv sss", message.getMessage());

		message = new SmsMessage();
		message.setMessage("neWs");
		command = cb.createCommand(message);
		assertTrue("Should be a Data Command", command instanceof DataMessageCommand);
		assertEquals("Rest of the Data", "neWs", message.getMessage());

	}

	@Test
	public void testCreateViewResultCommand() throws ApplicationException {
		CommandBuilder cb = new CommandBuilder();
		cb.init();
		Message message = new SmsMessage();
		message.setMessage("result rest");
		Command command = cb.createCommand(message);
		assertTrue("Should be a ViewResult Command", command instanceof ViewResultCommand);
		assertEquals("Rest of the Data", "rest", message.getMessage());

		message = new SmsMessage();
		message.setMessage("ReSult test");
		command = cb.createCommand(message);
		assertTrue("Should be a ViewResult Command", command instanceof ViewResultCommand);
		assertEquals("Rest of the Data", "test", message.getMessage());

	}
}
