/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$ 
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.flow.dispatch.lb;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import hsenidmobile.orca.core.flow.dispatch.DispatchableMessage;
import hsenidmobile.orca.core.model.message.SmsMessage;

import org.junit.Test;

/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class RoundRobinLbTest {

	@Test
	public void testAddLbEntity() {
		RoundRobinLb<Integer> roundRobinLb = new RoundRobinLb<Integer>();
		// LbAppMessageStore<Integer> entity = new
		// LbAppMessageStore<Integer>("app1");
		// entity.putMessage(1);
		// entity.activate();
		// entity.setMaxLoad(5);
		// roundRobinLb.addLbEntity(entity);
		roundRobinLb.putMessage("app1", 1);

		DispatchableMessage handler = roundRobinLb.getNext();
		assertEquals(handler.getMessage(), 1);
	}

	@Test
	public void testRemoveLbEntity() {
		RoundRobinLb<Integer> roundRobinLb = new RoundRobinLb<Integer>();
		// LbAppMessageStore<Integer> entity = new
		// LbAppMessageStore<Integer>("app1");
		// entity.putMessage(10);
		// entity.activate();
		// entity.setMaxLoad(5);
		// roundRobinLb.addLbEntity(entity);
		roundRobinLb.putMessage("app1", 10);

		// entity = new LbAppMessageStore<Integer>("app2");
		// entity.putMessage(12);
		// entity.activate();
		// entity.setMaxLoad(5);
		// roundRobinLb.addLbEntity(entity);
		roundRobinLb.putMessage("app2", 12);

		DispatchableMessage handler = roundRobinLb.getNext();
		assertEquals(handler.getMessage(), 10);

		roundRobinLb.removeLbEntity("app1");

		handler = roundRobinLb.getNext();
		assertEquals(handler.getMessage(), 12);
	}

	@Test(expected = NoLbEntityAvailable.class)
	public void testClearList() throws NoLbEntityAvailable {
		RoundRobinLb<Integer> roundRobinLb = new RoundRobinLb<Integer>();
		// LbAppMessageStore<Integer> entity = new
		// LbAppMessageStore<Integer>("app1");
		// entity.putMessage(10);
		// entity.activate();
		// entity.setMaxLoad(5);
		// roundRobinLb.addLbEntity(entity);
		roundRobinLb.putMessage("app1", 1);

		// entity = new LbAppMessageStore<Integer>("app2");
		// entity.putMessage(12);
		// entity.activate();
		// entity.setMaxLoad(5);
		// roundRobinLb.addLbEntity(entity);
		roundRobinLb.putMessage("app2", 12);

		roundRobinLb.clearList();

		roundRobinLb.getNext();
	}

	@Test
	public void testGetNext() {
		RoundRobinLb<Integer> roundRobinLb = new RoundRobinLb<Integer>();
		// LbAppMessageStore<Integer> entity = new
		// LbAppMessageStore<Integer>("app1");
		// for (int i = 0; i < 50; i++) {
		// entity.putMessage(10);
		// }
		// entity.activate();
		// entity.setMaxLoad(5);
		// roundRobinLb.addLbEntity(entity);
		for (int i = 0; i < 50; i++) {
			roundRobinLb.putMessage("app1", 10);
		}

		// entity = new LbAppMessageStore<Integer>("app2");
		// for (int i = 0; i < 50; i++) {
		// entity.putMessage(12);
		// }
		// entity.activate();
		// entity.setMaxLoad(7);
		// roundRobinLb.addLbEntity(entity);

		for (int i = 0; i < 50; i++) {
			roundRobinLb.putMessage("app2", 12);
		}

		for (int i = 0; i < 5; i++) {
			DispatchableMessage handler = roundRobinLb.getNext();
			assertEquals(handler.getMessage(), 10);
		}

		for (int i = 0; i < 5; i++) {
			DispatchableMessage handler = roundRobinLb.getNext();
			assertEquals(handler.getMessage(), 12);
		}

		for (int i = 0; i < 5; i++) {
			DispatchableMessage handler = roundRobinLb.getNext();
			assertEquals(handler.getMessage(), 10);
		}

		roundRobinLb.removeLbEntity("app2");

		for (int i = 0; i < 10; i++) {
			DispatchableMessage handler = roundRobinLb.getNext();
			assertEquals(handler.getMessage(), 10);
		}

		roundRobinLb.removeLbEntity("app1");

		try {
			DispatchableMessage handler = roundRobinLb.getNext();
			fail("Should throw an exception since all the lb entities were removed.");
		} catch (Exception e) {

		}
	}

	private SmsMessage createMessage(String message) {
		SmsMessage msg = new SmsMessage();
		msg.setMessage(message);
		return msg;
	}

}
