/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$ 
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.flow.filters;

import hsenidmobile.orca.core.applications.Service;
import hsenidmobile.orca.core.flow.PipelineContext;
import hsenidmobile.orca.core.flow.event.MessageEvent;
import hsenidmobile.orca.core.flow.event.MessageFlowEvent;
import hsenidmobile.orca.core.mock.MobileOperators;
import hsenidmobile.orca.core.mock.MockApplicationCreator;
import hsenidmobile.orca.core.model.Application;
import hsenidmobile.orca.core.model.Msisdn;
import hsenidmobile.orca.core.model.OrcaException;
import hsenidmobile.orca.core.model.message.AoRequest;
import hsenidmobile.orca.core.model.message.MessageContext;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.Before;
import org.junit.Test;

/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class WsPasswordEnforcementFilterTest {

    private Mockery context;

    @Before
    public void before() {
        context = new Mockery();
    }

    /**
	 * Test method for
	 * {@link hsenidmobile.orca.core.flow.filters.WsPasswordEnforcementFilter#handleUpStream(hsenidmobile.orca.core.flow.PipelineContext, hsenidmobile.orca.core.flow.event.MessageEvent)}
	 * .
	 * 
	 * @throws OrcaException
	 */
	@Test
	public void testHandleUpStreamCorrectPassword() throws OrcaException {
		WsPasswordEnforcementFilter passFilter = new WsPasswordEnforcementFilter();
		PipelineContext pc = new PipelineContext(null, null, "a", passFilter);

		AoRequest aoReq = new AoRequest();
		aoReq.setPassword("abc123");
		Application app = MockApplicationCreator.createDevApplication(new Msisdn("9696",
				MobileOperators.DIALOG), "stv dev");
		MessageContext mc = new MessageContext();
		mc.setApplication(app);

		MessageEvent event = new MessageFlowEvent<AoRequest>(aoReq, mc);
		passFilter.handleUpStream(pc, event);
	}

	/**
	 * Test method for
	 * {@link hsenidmobile.orca.core.flow.filters.WsPasswordEnforcementFilter#handleUpStream(hsenidmobile.orca.core.flow.PipelineContext, hsenidmobile.orca.core.flow.event.MessageEvent)}
	 * .
	 * 
	 * @throws OrcaException
	 */
	@Test(expected = WsPasswordNotMatchException.class)
	public void testHandleUpStreamWrongPassword() throws OrcaException {
		WsPasswordEnforcementFilter passFilter = new WsPasswordEnforcementFilter();
		PipelineContext pc = new PipelineContext(null, null, "a", passFilter);

		AoRequest aoReq = new AoRequest();
		aoReq.setPassword("abc123456");
		Application app = MockApplicationCreator.createDevApplication(new Msisdn("9696",
				MobileOperators.DIALOG), "stv dev");
		MessageContext mc = new MessageContext();
		mc.setApplication(app);

		MessageEvent event = new MessageFlowEvent<AoRequest>(aoReq, mc);
		passFilter.handleUpStream(pc, event);
	}

	/**
	 * Test method for
	 * {@link hsenidmobile.orca.core.flow.filters.WsPasswordEnforcementFilter#handleUpStream(hsenidmobile.orca.core.flow.PipelineContext, hsenidmobile.orca.core.flow.event.MessageEvent)}
	 * .
	 * 
	 * @throws OrcaException
	 */
	@Test(expected = ApplicationTypeNotAllowedException.class)
	public void testHandleUpStreamWrongAppType() throws OrcaException {
		final WsPasswordEnforcementFilter passFilter = new WsPasswordEnforcementFilter();
        final PipelineContext pipelineContext = new PipelineContext(null, null, "test", passFilter);
        final MessageEvent event = context.mock(MessageEvent.class);
        final MessageContext messageContext = new MessageContext();
        final Application app = context.mock(Application.class);
        messageContext.setApplication(app);

        context.checking(new Expectations() {{
            allowing(event).getMessageContext(); will(returnValue(messageContext));
            allowing(app).getService() ;will(returnValue(context.mock(Service.class)));
        }});

        passFilter.handleUpStream(pipelineContext, event);
	}

}
