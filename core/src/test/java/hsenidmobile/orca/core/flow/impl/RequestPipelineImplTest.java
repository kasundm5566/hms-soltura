/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.flow.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import hsenidmobile.orca.core.applications.ApplicationImpl;
import hsenidmobile.orca.core.flow.ApplicationFactory;
import hsenidmobile.orca.core.flow.ApplicationFinder;
import hsenidmobile.orca.core.flow.ApplicationSubkeywordExtractor;
import hsenidmobile.orca.core.flow.PipelineContext;
import hsenidmobile.orca.core.flow.command.CommandBuilder;
import hsenidmobile.orca.core.flow.event.MessageFlowEvent;
import hsenidmobile.orca.core.mock.MobileOperators;
import hsenidmobile.orca.core.mock.MockApplicationCreator;
import hsenidmobile.orca.core.model.Msisdn;
import hsenidmobile.orca.core.model.OrcaException;
import hsenidmobile.orca.core.model.RoutingInfo;
import hsenidmobile.orca.core.model.RoutingInfoStatus;
import hsenidmobile.orca.core.model.RoutingKey;
import hsenidmobile.orca.core.model.message.AoRequest;
import hsenidmobile.orca.core.model.message.Message;
import hsenidmobile.orca.core.model.message.MessageContext;
import hsenidmobile.orca.core.model.message.SmsMessage;
import hsenidmobile.orca.core.repository.AppRepository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class RequestPipelineImplTest {

	private Mockery context;
	private AppRepository appRepository;

	@Before
	public void setup() {
		context = new Mockery();
		appRepository = context.mock(AppRepository.class);

	}

	/**
	 * Test method for
	 * .
	 */
	@Test
	public void testAddLast() {
		RequestPipelineImpl pipeline = new RequestPipelineImpl();
		CommandBuilder cb = new CommandBuilder();
		cb.init();
		pipeline.addLast("commad-builder", cb);
		ApplicationFinder appFinder = new ApplicationFinder();
		appFinder.setAppRepository(appRepository);
		pipeline.addLast("app-finder", appFinder);

		assertNotNull(pipeline.getAvailableHandler());

		assertEquals("Number of handlers in the pipeline", 2, pipeline.getAvailableHandler().size());

		PipelineContext firstContext = pipeline.getFirst();
		assertTrue("First handler should be CommanBuilder", firstContext.getHandler() instanceof CommandBuilder);
		PipelineContext nextContext = firstContext.getNext();
		assertTrue("Second handler should be CommanBuilder", nextContext.getHandler() instanceof ApplicationFinder);

	}

	@Test(expected = IllegalArgumentException.class)
	public void testAddDuplicate() {
		RequestPipelineImpl pipeline = new RequestPipelineImpl();
		pipeline.addLast("commad-builder", new CommandBuilder());
		pipeline.addLast("commad-builder", new CommandBuilder());
		ApplicationFinder appFinder = new ApplicationFinder();
		appFinder.setAppRepository(appRepository);
		pipeline.addLast("app-finder", appFinder);

	}

	@Test(expected = NullPointerException.class)
	public void testAddAllNull() {
		RequestPipelineImpl pipeline = new RequestPipelineImpl();
		pipeline.addLast(null, null);

	}

	@Test(expected = NullPointerException.class)
	public void testAddNameNull() {
		RequestPipelineImpl pipeline = new RequestPipelineImpl();
		pipeline.addLast(null, new CommandBuilder());

	}

	@Test(expected = NullPointerException.class)
	public void testAddHandlerNull() {
		RequestPipelineImpl pipeline = new RequestPipelineImpl();
		pipeline.addLast("handler", null);

	}

	/**
	 * Test method for
	 * .
	 *
	 * @throws OrcaException
	 */
	@Test
	@Ignore //TODO: Fix test cases
	public void testOnMessageFoMo() throws OrcaException {
		final List<RoutingInfo> rfList = new ArrayList<RoutingInfo>();
		RoutingInfo rf1 = new RoutingInfo();
		final Msisdn shortCode = new Msisdn("9696", MobileOperators.DIALOG);
		rf1.setRoutingKey(new RoutingKey(shortCode, "stv subs"));
		rf1.setStatus(RoutingInfoStatus.ASSIGNED);
		rfList.add(rf1);
		RoutingInfo rf2 = new RoutingInfo();
		rf2.setStatus(RoutingInfoStatus.ASSIGNED);
		rf2.setRoutingKey(new RoutingKey(shortCode, "stv sss"));
		rfList.add(rf2);

		context.checking(new Expectations() {
			{
				allowing(appRepository).findAppByAppId(with(any(String.class)));
				will(returnValue(MockApplicationCreator.createAlertApplicationWithoutSubkeys(shortCode, "stv sss")));
				allowing(appRepository).updateMoSummary(with(any(String.class)));
			}
		});

		Message message = new SmsMessage();
		List<Msisdn> receiverMsisdns = new ArrayList<Msisdn>();
		receiverMsisdns.add(new Msisdn("9696", MobileOperators.DIALOG));
		message.setReceiverAddresses(receiverMsisdns);
		message.setMessage("stv sss reg");
		MessageContext messageContext = new MessageContext();

		RequestPipelineImpl pipeline = new RequestPipelineImpl();
		ApplicationFinder appFinder = new ApplicationFinder();
		appFinder.setAppRepository(appRepository);
		appFinder.setApplicationFactory(new ApplicationFactory());
		pipeline.addLast("app-finder", appFinder);
		ApplicationSubkeywordExtractor keywordExtractor = new ApplicationSubkeywordExtractor();
		keywordExtractor.setSpecialSysWideKeywords(Collections.EMPTY_LIST);
		pipeline.addLast("keyword-extractor", keywordExtractor);
		CommandBuilder cb = new CommandBuilder();
		cb.init();
		pipeline.addLast("commad-builder", cb);

		pipeline.onMessage(new MessageFlowEvent<Message>(message, messageContext));

		assertTrue("App should not null", messageContext.getApplication() != null);
		assertEquals("app routing info", "stv sss", messageContext.getApplication().getRoutingKeys().get(0)
				.getKeyword());
		assertEquals("app routing info", new Msisdn("9696", MobileOperators.DIALOG), messageContext.getApplication()
				.getRoutingKeys().get(0).getShortCode());

	}

	/**
	 * Test method for
	 * .
	 *
	 * @throws OrcaException
	 */
	@Test
	public void testOnMessageForAo() throws OrcaException {
		context.checking(new Expectations(){{
			allowing(appRepository).findAppByAppId(with("testApp")); will(returnValue(new ApplicationImpl()));
		}});

		Message message = new SmsMessage();
		message.setMessage("This is a test message from Dev Application...");
		AoRequest aoRequest = new AoRequest();
		aoRequest.setMessage(message);
		aoRequest.setAppId("testApp");
		MessageContext messageContext = new MessageContext();

		RequestPipelineImpl pipeline = new RequestPipelineImpl();
		ApplicationFinder appFinder = new ApplicationFinder();
		appFinder.setAppRepository(appRepository);
		appFinder.setApplicationFactory(new ApplicationFactory());
		pipeline.addLast("app-finder", appFinder);

		pipeline.onMessage(aoRequest, messageContext);

		// assertTrue("Command should be Registration", messageContext
		// .getCommand() instanceof SubscriberRegistrationCommand);
		assertTrue("App should be voting", messageContext.getApplication() != null);

	}

	/**
	 * Test method for
	 * .
	 *
	 * @throws OrcaException
	 */
	@Test(expected = IllegalStateException.class)
	public void testOnMessageForMoWithNoHandlers() throws OrcaException {
		Message message = new SmsMessage();
		message.setMessage("reg stv sss");
		MessageContext messageContext = new MessageContext();

		RequestPipelineImpl pipeline = new RequestPipelineImpl();
		pipeline.onMessage(new MessageFlowEvent<Message>(message, messageContext));

	}

	/**
	 * Test method for
	 * .
	 *
	 * @throws OrcaException
	 */
	@Test(expected = IllegalStateException.class)
	public void testOnMessageForAoWithNoHandlers() throws OrcaException {
		Message message = new SmsMessage();
		message.setMessage("This is a test message from Dev Application...");
		AoRequest aoRequest = new AoRequest();
		aoRequest.setMessage(message);
		aoRequest.setAppId("testApp");
		MessageContext messageContext = new MessageContext();

		RequestPipelineImpl pipeline = new RequestPipelineImpl();
		pipeline.onMessage(aoRequest, messageContext);

	}

}
