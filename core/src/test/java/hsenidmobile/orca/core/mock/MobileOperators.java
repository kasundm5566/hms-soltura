/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hsenidmobile.orca.core.mock;

/**
/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 * @author hms
 *
 */
public class MobileOperators {
	public final static String DIALOG = "DIALOG";
	public final static String MOBITEL = "MOBITEL"; 
	public final static String TIGO = "TIGO";
	public final static String AIRTEL = "AIRTEL";
	public final static String HUTCH = "HUTCH";

}
