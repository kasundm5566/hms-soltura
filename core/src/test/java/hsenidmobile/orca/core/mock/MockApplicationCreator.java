/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hsenidmobile.orca.core.mock;

import hsenidmobile.orca.core.applications.ApplicationImpl;
import hsenidmobile.orca.core.applications.PeriodUnit;
import hsenidmobile.orca.core.applications.ServicePolicy;
import hsenidmobile.orca.core.applications.alert.AlertService;
import hsenidmobile.orca.core.applications.developer.Dev;
import hsenidmobile.orca.core.applications.exception.VoteletException;
import hsenidmobile.orca.core.applications.policy.RegistrationPolicy;
import hsenidmobile.orca.core.applications.policy.SubscriberValidationPolicy;
import hsenidmobile.orca.core.applications.subscription.Subscription;
import hsenidmobile.orca.core.applications.voting.Vote;
import hsenidmobile.orca.core.applications.voting.VoteResultSummary;
import hsenidmobile.orca.core.applications.voting.VotingService;
import hsenidmobile.orca.core.model.Application;
import hsenidmobile.orca.core.model.ApplicationRoutingInfo;
import hsenidmobile.orca.core.model.Author;
import hsenidmobile.orca.core.model.LocalizedMessage;
import hsenidmobile.orca.core.model.Msisdn;
import hsenidmobile.orca.core.model.RoutingInfo;
import hsenidmobile.orca.core.model.RoutingKey;
import hsenidmobile.orca.core.model.Status;
import hsenidmobile.orca.core.model.message.Message;
import hsenidmobile.orca.core.model.message.MessageContext;
import hsenidmobile.orca.core.model.message.SmsMessage;
import hsenidmobile.orca.core.repository.AppRepository;
import hsenidmobile.orca.core.repository.VoteRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/**
 * $LastChangedDate$ $LastChangedBy$ $LastChangedRevision$
 */

public class MockApplicationCreator {

	public static final String APP_ID = "app_id_test1";
	public static final String APP_NAME = "app_name_test1";

	public static final String VOTELET_ID = "votelet_id";

	public static final String KEYWORD_CANDIDATE_1 = "keyword_can_1";
	public static final String KEYWORD_CANDIDATE_2 = "keyword_can_2";
	public static final String KEYWORD_CANDIDATE_3 = "keyword_can_3";

	public static final String ALIAS_CANDIDATE_1 = "alias_can_1";
	public static final String ALIAS_CANDIDATE_2 = "alias_can_2";
	public static final String ALIAS_CANDIDATE_3 = "alias_can_3";

	public static final String DESC_CANDIDATE_EN = "desc_can_en";
	public static final String DESC_CANDIDATE_TA = "desc_can_ta";
	public static final String DESC_CANDIDATE_SI = "desc_can_si";

	public static final String SUBSCRIPTION_SUCCESSFUL_EN = "Subscription Successful EN";
	public static final String SUBSCRIPTION_SUCCESSFUL_TA = "Subscription Successful TA";
	public static final String SUBSCRIPTION_SUCCESSFUL_SI = "Subscription Successful SI";

	public static final String UNSUBSCRIPTION_SUCCESSFUL_EN = "Unsubscription Successful EN";
	public static final String UNSUBSCRIPTION_SUCCESSFUL_TA = "Unsubscription Successful TA";
	public static final String UNSUBSCRIPTION_SUCCESSFUL_SI = "Unsubscription Successful SI";

	public static final String REQUEST_INVALID_EN = "Your Request is Invalid EN";
	public static final String REQUEST_INVALID_TA = "Your Request is Invalid TA";
	public static final String REQUEST_INVALID_SI = "Your Request is Invalid SI";

	public static final String KEYWORD = "9696";
	public static final String WS_PASSWORD = "abc123";

	private static final Logger logger = LoggerFactory.getLogger(MockApplicationCreator.class);

	private static AppRepository appRepository;

	public static ApplicationImpl createVotingApplication(Msisdn shortCodeNumber, String keyword) {
		try {
			final ApplicationImpl votingApp = new ApplicationImpl();
			votingApp.setAppId(APP_ID);
			votingApp.setAppName(APP_NAME);
			votingApp.setSubscriptionResponse(getSubscriptiongRespounse());
			votingApp.setUnsubscribeResponse(getUnsubscriptiongRespounse());
			votingApp.setInvalidRequestErrorMessage(getInvalidRequestError());
			votingApp.setDataMessagePolicies(getServicePolicy());
			votingApp.setStartDate(new DateTime());
			votingApp.setEndDate(new DateTime().plusDays(10));
			votingApp.setApplicationRoutingInfo(getApplicationRoutingInfo(shortCodeNumber, keyword));
			votingApp.setService(createVotingService());
			votingApp.setApplicationRepository(appRepository);

//			ChargeBeforeSendPolicy massageSendingChargingPolicy = new ChargeBeforeSendPolicy();
//			massageSendingChargingPolicy.setAmount(5);
//			massageSendingChargingPolicy.setChargingService(new StubChargingService());
//			votingApp.setMassageSendingChargingPolicy(massageSendingChargingPolicy);
//
//			PeriodicSubscriptionChargingPolicy userRegistrationChargingPolicy = new PeriodicSubscriptionChargingPolicy();
//			userRegistrationChargingPolicy.setAmount(50);
//			userRegistrationChargingPolicy.setPeriodUnit(PeriodUnit.MONTH);
//			userRegistrationChargingPolicy.setPeriodValue(1);
//			votingApp.setUserRegistrationChargingPolicy(userRegistrationChargingPolicy);
			return votingApp;
		} catch (VoteletException e) {
			logger.error(e.getMessage(), e);
			return null;
		}
	}

	public static ApplicationImpl createAlertApplication(Msisdn shortCodeNumber, String keyword) {
		final ApplicationImpl alertApp = createAlertApplicationWithOutRouting();
		alertApp.setApplicationRoutingInfo(getApplicationRoutingInfo(shortCodeNumber, keyword));
		return alertApp;
	}

	public static ApplicationImpl createAlertApplicationWithoutSubkey(Msisdn shortCodeNumber, String keyword) {
		final ApplicationImpl alertApp = createAlertApplicationWithOutRouting();
		alertApp.setApplicationRoutingInfo(getApplicationRoutingInfo(shortCodeNumber, keyword));
//		((AlertService)alertApp.getService()).setLatestContent(createAlertDataWithoutSubkey());
		return alertApp;
	}

	public static Application createAlertApplicationWithoutSubkeys(Msisdn shortCodeNumber, String keyword) {
		final ApplicationImpl alertApp = new ApplicationImpl();
		alertApp.setAppId(APP_ID);
		alertApp.setAppName(APP_NAME);
		alertApp.setSubscriptionResponse(getSubscriptiongRespounse());
		alertApp.setUnsubscribeResponse(getUnsubscriptiongRespounse());
		alertApp.setInvalidRequestErrorMessage(getInvalidRequestError());
		alertApp.setDataMessagePolicies(getServicePolicy());
		alertApp.setStartDate(new DateTime());
		alertApp.setEndDate(new DateTime().plusDays(10));
		alertApp.setApplicationRoutingInfo(getApplicationRoutingInfo(shortCodeNumber, keyword));

		AlertService service = new AlertService();
		service.setApplication(alertApp);

//		List<KeywordDetail> data = new ArrayList<KeywordDetail>();
//		KeywordDetail kd = new KeywordDetail();
//		kd.setKeyword("");
//		kd.addDescription(Locale.ENGLISH, "hSenid Breaking news");
//		kd.setKeywordAliases(Collections.EMPTY_LIST);
//		data.add(kd);
//		service.setLatestContent(data);

		//service.setAlertServiceExecutorService(new AlertMessageDispatcher());
		alertApp.setService(service);

		alertApp.setApplicationRepository(appRepository);
		alertApp.setStatus(Status.ACTIVE);
		alertApp.setRegistrationPolicy(RegistrationPolicy.OPEN);

//		ChargeBeforeSendPolicy massageSendingChargingPolicy = new ChargeBeforeSendPolicy();
//		massageSendingChargingPolicy.setAmount(5);
//		massageSendingChargingPolicy.setChargingService(new StubChargingService());
//		alertApp.setMassageSendingChargingPolicy(massageSendingChargingPolicy);
//
//		PeriodicSubscriptionChargingPolicy userRegistrationChargingPolicy = new PeriodicSubscriptionChargingPolicy();
//		userRegistrationChargingPolicy.setAmount(50);
//		userRegistrationChargingPolicy.setPeriodUnit(PeriodUnit.MONTH);
//		userRegistrationChargingPolicy.setPeriodValue(1);
//		alertApp.setUserRegistrationChargingPolicy(userRegistrationChargingPolicy);
		return alertApp;
	}

	public static ApplicationImpl createAlertApplicationWithOutRouting() {
		final ApplicationImpl alertApp = new ApplicationImpl();
		alertApp.setAppId(APP_ID);
		alertApp.setAppName(APP_NAME);
		alertApp.setSubscriptionResponse(getSubscriptiongRespounse());
		alertApp.setUnsubscribeResponse(getUnsubscriptiongRespounse());
		alertApp.setInvalidRequestErrorMessage(getInvalidRequestError());
		alertApp.setDataMessagePolicies(getServicePolicy());
		alertApp.setStartDate(new DateTime());
		alertApp.setEndDate(new DateTime().plusDays(10));

		AlertService service = new AlertService();
		service.setApplication(alertApp);
//		service.setLatestContent(createAlertData());
		//service.setAlertServiceExecutorService(new AlertMessageDispatcher());
		alertApp.setService(service);

		alertApp.setApplicationRepository(appRepository);
		alertApp.setStatus(Status.ACTIVE);
		alertApp.setRegistrationPolicy(RegistrationPolicy.OPEN);

		Author author = new Author();
		author.setMsisdn(new Msisdn("776123456", MobileOperators.DIALOG));
		List<Author> authors = new ArrayList<Author>(1);
		authors.add(author);
		alertApp.setAuthors(authors);

//		ChargeBeforeSendPolicy massageSendingChargingPolicy = new ChargeBeforeSendPolicy();
//		massageSendingChargingPolicy.setAmount(5);
//		massageSendingChargingPolicy.setChargingService(new StubChargingService());
//		alertApp.setMassageSendingChargingPolicy(massageSendingChargingPolicy);
//
//		PeriodicSubscriptionChargingPolicy userRegistrationChargingPolicy = new PeriodicSubscriptionChargingPolicy();
//		userRegistrationChargingPolicy.setAmount(50);
//		userRegistrationChargingPolicy.setPeriodUnit(PeriodUnit.MONTH);
//		userRegistrationChargingPolicy.setPeriodValue(1);
//		alertApp.setUserRegistrationChargingPolicy(userRegistrationChargingPolicy);
//		userRegistrationChargingPolicy.setChargingService(new StubChargingService());
		return alertApp;
	}

//	private static List<KeywordDetail> createAlertData() {
//		List<KeywordDetail> data = new ArrayList<KeywordDetail>();
//
//		KeywordDetail vote1 = new KeywordDetail();
//		vote1.setKeyword("mr");
//		vote1.addDescription(Locale.ENGLISH, "Mahinda Rajapakshe Gosips");
//		vote1.setKeywordAliases(Collections.EMPTY_LIST);
//		data.add(vote1);
//
//		KeywordDetail vote2 = new KeywordDetail();
//		vote2.setKeyword("sf");
//		vote2.addDescription(Locale.ENGLISH, "Sarath Fonseka Gosips");
//		vote2.setKeywordAliases(Collections.EMPTY_LIST);
//		data.add(vote2);
//
//		return data;
//	}
//
//	private static List<KeywordDetail> createAlertDataWithoutSubkey() {
//		List<KeywordDetail> data = new ArrayList<KeywordDetail>();
//
//		KeywordDetail vote1 = new KeywordDetail();
//		vote1.setKeyword("");
//		vote1.addDescription(Locale.ENGLISH, "Test alert data....");
//		vote1.setKeywordAliases(Collections.EMPTY_LIST);
//		data.add(vote1);
//
//		return data;
//	}

	private static VotingService createVotingService() throws VoteletException {
		VotingService votingService = new VotingService();
		votingService.setId(new Long(1));
//		votingService.addVotelet(getVotelet());


		return votingService;
	}

	private static VotingService createVotingServiceWithExpired() throws VoteletException {
		VotingService votingService = new VotingService();
//		votingService.addVotelet(getVotelet());
		return votingService;
	}

	private static VotingService createVotingServiceWithoutVotelet() throws VoteletException {
		VotingService votingService = new VotingService();
//		votingService.addVotelet(getVotelet());
		return votingService;
	}

	public static ApplicationImpl createDevApplication(Msisdn shortCodeNumber, String keyword) {
		final ApplicationImpl devApp = new ApplicationImpl();
		devApp.setAppId(APP_ID);
		devApp.setAppName(APP_NAME);
		devApp.setInvalidRequestErrorMessage(getInvalidRequestError());
		devApp.setDataMessagePolicies(getServicePolicy());
		devApp.setStartDate(new DateTime());
		devApp.setEndDate(new DateTime().plusDays(10));
		devApp.setApplicationRoutingInfo(getApplicationRoutingInfo(shortCodeNumber, keyword));
		final Dev service = new Dev();
		service.setWsPassword(WS_PASSWORD);
		devApp.setService(service);
		devApp.setApplicationRepository(appRepository);
		return devApp;
	}

	public static ApplicationImpl createAppWithoutService(Msisdn shortCodeNumber, String keyword) {
		final ApplicationImpl app = new ApplicationImpl();
		app.setAppId(APP_ID);
		app.setAppName(APP_NAME);
		app.setInvalidRequestErrorMessage(getInvalidRequestError());
		app.setDataMessagePolicies(getServicePolicy());
		app.setStartDate(new DateTime());
		app.setEndDate(new DateTime().plusDays(10));
		app.setApplicationRoutingInfo(getApplicationRoutingInfo(shortCodeNumber, keyword));
		app.setApplicationRepository(appRepository);
//		ChargeBeforeSendPolicy massageSendingChargingPolicy = new ChargeBeforeSendPolicy();
//		massageSendingChargingPolicy.setAmount(5);
//		massageSendingChargingPolicy.setChargingService(new StubChargingService());
//		app.setMassageSendingChargingPolicy(massageSendingChargingPolicy);
		return app;
	}

	public static ApplicationImpl createSubscriptionApplication(Msisdn shortCodeNumber, String keyword) {
		final ApplicationImpl subcriptionApp = new ApplicationImpl();
		subcriptionApp.setAppId(APP_ID);
		subcriptionApp.setAppName(APP_NAME);
		subcriptionApp.setSubscriptionResponse(getSubscriptiongRespounse());
		subcriptionApp.setUnsubscribeResponse(getUnsubscriptiongRespounse());
		subcriptionApp.setInvalidRequestErrorMessage(getInvalidRequestError());
		subcriptionApp.setDataMessagePolicies(getServicePolicy());
		subcriptionApp.setStartDate(new DateTime());
		subcriptionApp.setEndDate(new DateTime().plusDays(10));
		subcriptionApp.setApplicationRoutingInfo(getApplicationRoutingInfo(shortCodeNumber, keyword));
		final Subscription service = new Subscription();
//		service.setSubscriptionServiceDataList(createSubscriptionData());
//		service.setSubscriptionDataRepository(new StubSubscriptionDataService());
		service.setApplication(subcriptionApp);
		subcriptionApp.setService(service);
		subcriptionApp.setApplicationRepository(appRepository);
		subcriptionApp.setStatus(Status.ACTIVE);
		subcriptionApp.setRegistrationPolicy(RegistrationPolicy.OPEN);

//		ChargeBeforeSendPolicy massageSendingChargingPolicy = new ChargeBeforeSendPolicy();
//		massageSendingChargingPolicy.setAmount(5);
//		massageSendingChargingPolicy.setChargingService(new StubChargingService());
//		subcriptionApp.setMassageSendingChargingPolicy(massageSendingChargingPolicy);
//
//		PeriodicSubscriptionChargingPolicy userRegistrationChargingPolicy = new PeriodicSubscriptionChargingPolicy();
//		userRegistrationChargingPolicy.setAmount(50);
//		userRegistrationChargingPolicy.setPeriodUnit(PeriodUnit.MONTH);
//		userRegistrationChargingPolicy.setPeriodValue(1);
//		userRegistrationChargingPolicy.setChargingService(new StubChargingService());
//		subcriptionApp.setUserRegistrationChargingPolicy(userRegistrationChargingPolicy);

		return subcriptionApp;
	}

//	private static List<SubscriptionServiceData> createSubscriptionData() {
//		SubscriptionServiceData serviceData = new SubscriptionServiceData();
//		serviceData.setPeriodicity(new Periodicity(PeriodUnit.HOUR, 1, 0));
//
//		Calendar now = new GregorianCalendar();
//
//		Calendar cal = new GregorianCalendar(0, 0, 0, now.get(Calendar.HOUR_OF_DAY), 0, 0);
//
//		serviceData.setExpectedDispatchTime(cal.getTime());
//
//		List<ContentGroup> content = new ArrayList<ContentGroup>();
//		ContentGroup cg = new ContentGroup();
//		cg.setId(1L);
//		cg.setContents(getKeywordDetails(5));
//		content.add(cg);
//
//		serviceData.setData(content);
//
//		List<SubscriptionServiceData> sdList = new ArrayList<SubscriptionServiceData>(1);
//		sdList.add(serviceData);
//		return sdList;
//	}

	private static ApplicationRoutingInfo getApplicationRoutingInfo(Msisdn shortCodeNumber, String keyword) {
		ApplicationRoutingInfo routing1 = new ApplicationRoutingInfo();
		final RoutingInfo routingInfo = new RoutingInfo();
		routingInfo.setRoutingKey(new RoutingKey(shortCodeNumber, keyword));
		routing1.addRoutingInfo(routingInfo);
		return routing1;
	}

	public static ApplicationImpl createVotingApplication() {
		try {
			final ApplicationImpl votingApp = new ApplicationImpl();
			votingApp.setAppId(APP_ID);
			votingApp.setAppName(APP_NAME);
			votingApp.setApplicationRoutingInfo(getApplicationRoutingInfo(new Msisdn("9696", MobileOperators.DIALOG),
					"vote"));
			votingApp.setSubscriptionResponse(getSubscriptiongRespounse());
			votingApp.setUnsubscribeResponse(getUnsubscriptiongRespounse());
			votingApp.setInvalidRequestErrorMessage(getInvalidRequestError());
			votingApp.setService(createVotingService());
			votingApp.setDataMessagePolicies(getServicePolicy());
			votingApp.setStartDate(new DateTime());
			votingApp.setEndDate(new DateTime().plusDays(10));
			votingApp.setApplicationRepository(appRepository);

			votingApp.setRegistrationPolicy(RegistrationPolicy.OPEN);

//			ChargeBeforeSendPolicy massageSendingChargingPolicy = new ChargeBeforeSendPolicy();
//			massageSendingChargingPolicy.setAmount(5);
//			massageSendingChargingPolicy.setChargingService(new StubChargingService());
//			massageSendingChargingPolicy.setChargingService(new StubChargingService());
//			votingApp.setMassageSendingChargingPolicy(massageSendingChargingPolicy);
//
//			PeriodicSubscriptionChargingPolicy userRegistrationChargingPolicy = new PeriodicSubscriptionChargingPolicy();
//			userRegistrationChargingPolicy.setAmount(50);
//			userRegistrationChargingPolicy.setPeriodUnit(PeriodUnit.MONTH);
//			userRegistrationChargingPolicy.setPeriodValue(1);
//			userRegistrationChargingPolicy.setChargingService(new StubChargingService());
//			votingApp.setUserRegistrationChargingPolicy(userRegistrationChargingPolicy);
			return votingApp;
		} catch (VoteletException e) {
			logger.error(e.getMessage(), e);
			return null;
		}
	}

	public static ApplicationImpl createVotingApplicationExpiredVotlet() {
		try {
			final ApplicationImpl votingApp = new ApplicationImpl();
			votingApp.setAppId(APP_ID);
			votingApp.setAppName(APP_NAME);
			votingApp.setSubscriptionResponse(getSubscriptiongRespounse());
			votingApp.setUnsubscribeResponse(getUnsubscriptiongRespounse());
			votingApp.setInvalidRequestErrorMessage(getInvalidRequestError());
			votingApp.setService(createVotingServiceWithExpired());
			votingApp.setDataMessagePolicies(getServicePolicy());
			votingApp.setStartDate(new DateTime());
			votingApp.setEndDate(new DateTime().plusDays(10));
			votingApp.setApplicationRepository(appRepository);
			return votingApp;
		} catch (VoteletException e) {
			logger.error(e.getMessage(), e);
			return null;
		}
	}

	public static ApplicationImpl createVotingApplicationWithoutVotelet() {
		try {
			final ApplicationImpl votingApp = new ApplicationImpl();
			votingApp.setAppId(APP_ID);
			votingApp.setAppName(APP_NAME);
			votingApp.setSubscriptionResponse(getSubscriptiongRespounse());
			votingApp.setUnsubscribeResponse(getUnsubscriptiongRespounse());
			votingApp.setInvalidRequestErrorMessage(getInvalidRequestError());
			votingApp.setService(createVotingServiceWithoutVotelet());
			votingApp.setDataMessagePolicies(getServicePolicy());
			votingApp.setStartDate(new DateTime());
			votingApp.setEndDate(new DateTime().plusDays(10));
			votingApp.setApplicationRepository(appRepository);
			return votingApp;
		} catch (VoteletException e) {
			logger.error(e.getMessage(), e);
			return null;
		}
	}

	private static List<LocalizedMessage> getSubscriptiongRespounse() {
		return Arrays.asList(new LocalizedMessage(SUBSCRIPTION_SUCCESSFUL_EN, Locale.ENGLISH));

//		Map<Locale, String> subscription = new HashMap<Locale, String>();
//		subscription.put(Locale.ENGLISH, SUBSCRIPTION_SUCCESSFUL_EN);
//		subscription.put(new Locale("ta"), SUBSCRIPTION_SUCCESSFUL_TA);
//		subscription.put(new Locale("si"), SUBSCRIPTION_SUCCESSFUL_SI);
//		return subscription;
	}

	private static  List<LocalizedMessage> getUnsubscriptiongRespounse() {
		return Arrays.asList(new LocalizedMessage(UNSUBSCRIPTION_SUCCESSFUL_EN, Locale.ENGLISH));
//		Map<Locale, String> subscription = new HashMap<Locale, String>();
//		subscription.put(Locale.ENGLISH, UNSUBSCRIPTION_SUCCESSFUL_EN);
//		subscription.put(new Locale("ta"), UNSUBSCRIPTION_SUCCESSFUL_TA);
//		subscription.put(new Locale("si"), UNSUBSCRIPTION_SUCCESSFUL_SI);
//		return subscription;
	}

	private static ServicePolicy getServicePolicy() {
		return new SubscriberValidationPolicy();
	}

	private static VoteRepository getVoteRepository() {
		return new VoteRepository() {
			@Override
			public long add(Vote response) {
                return response.getId();
			}
			@Override
			public void updateSummary(long voteletId, Vote vote) {

			}
			@Override
			public List<VoteResultSummary> getVotingSummary(long voteletId) {
				return null;
			}
			@Override
			public boolean hasVoted(Msisdn msisdn, long voteletId) {
				return false;
			}

            @Override
            public void rollBackReceivedVote(long voteId, long voteServiceId) {
            }

        };
	}

//	public static List<KeywordDetail> getKeywordDetails() {
//		List<KeywordDetail> voteesList = new ArrayList<KeywordDetail>();
//		final KeywordDetail keyworDetails = new KeywordDetail();
//		keyworDetails.setKeyword(KEYWORD_CANDIDATE_1);
//		keyworDetails.setDescription(getKeywordDesc(KEYWORD_CANDIDATE_1));
//		keyworDetails.setKeywordAliases(getKeywordAliases(KEYWORD_CANDIDATE_1));
//		voteesList.add(keyworDetails);
//		return voteesList;
//	}
//
//	public static List<KeywordDetail> getKeywordDetails(int keywordCount) {
//		List<KeywordDetail> voteesList = new ArrayList<KeywordDetail>();
//		for (int j = 1; j <= keywordCount; j++) {
//			final KeywordDetail keyworDetails = new KeywordDetail();
//			String keyword = "subkey" + j;
//			keyworDetails.setKeyword(keyword);
//			keyworDetails.setDescription(getKeywordDesc(keyword));
//			keyworDetails.setKeywordAliases(getKeywordAliases(keyword));
//			voteesList.add(keyworDetails);
//		}
//		return voteesList;
//	}

	private static Map<Locale, String> getKeywordDesc(String str) {
		Map<Locale, String> map = new HashMap<Locale, String>();
		map.put(Locale.ENGLISH, DESC_CANDIDATE_EN + str);
		map.put(new Locale("ta"), DESC_CANDIDATE_TA + str);
		map.put(new Locale("si"), DESC_CANDIDATE_SI + str);
		return map;
	}

	private static List<String> getKeywordAliases(String key) {
		List<String> list = new ArrayList<String>();
		// list.add(ALIAS_CANDIDATE_1);
		// list.add(ALIAS_CANDIDATE_2);
		// list.add(ALIAS_CANDIDATE_3);
		list.add(key + "ali-1");
		list.add(key + "ali-2");
		list.add(key + "ali-3");
		return list;
	}

	private static Status getStatus() {
		return Status.ACTIVE;
	}

	private static List<LocalizedMessage> getInvalidRequestError() {
		return Arrays.asList(new LocalizedMessage(REQUEST_INVALID_EN, Locale.ENGLISH));
//		Map<Locale, String> invalidRequest = new HashMap<Locale, String>();
//		invalidRequest.put(Locale.ENGLISH, REQUEST_INVALID_EN);
//		invalidRequest.put(new Locale("ta"), REQUEST_INVALID_TA);
//		invalidRequest.put(new Locale("si"), REQUEST_INVALID_SI);
//		return invalidRequest;
	}

	public static MessageContext getMessageContext() {
		MessageContext messageContext = new MessageContext();
		Msisdn msisdn = new Msisdn(KEYWORD, MobileOperators.DIALOG);
		messageContext.setApplication(new ApplicationImpl());
		messageContext.setReceiveTime(System.currentTimeMillis());
		return messageContext;
	}

	public static Message getMessage() {
		Message message = new SmsMessage();
		message.setMessage(KEYWORD_CANDIDATE_1);
		message.setSenderAddress(getSenderAddress());
		message.setReceiverAddresses(getReciverAddress());
		return message;
	}

	private static List<Msisdn> getReciverAddress() {
		List<Msisdn> listMsisdn = new ArrayList<Msisdn>();
		listMsisdn.add(new Msisdn(KEYWORD, MobileOperators.DIALOG));
		return listMsisdn;
	}

	private static Msisdn getSenderAddress() {
		return new Msisdn("0777123456", MobileOperators.DIALOG);
	}

}
