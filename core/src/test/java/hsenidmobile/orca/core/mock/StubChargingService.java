/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$ 
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.mock;

import hsenidmobile.orca.core.model.Msisdn;
import hsenidmobile.orca.core.services.charging.ChargingException;
import hsenidmobile.orca.core.services.charging.ChargingService;

import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class StubChargingService implements ChargingService {

	private Random rand = new Random(System.currentTimeMillis());
	private Map<Msisdn, Double> accountBalance = new ConcurrentHashMap<Msisdn, Double>();
	private Map<String, ChargingTransaction> chargingTransactions = new ConcurrentHashMap<String, ChargingTransaction>();
	
	public static final float initialBalance = 500;

	@Override
	public void cancel(String transactionId) throws ChargingException {
		ChargingTransaction trans = chargingTransactions.remove(transactionId);
		if(trans == null){
			throw new ChargingException("Invalid TransactionId"); 
		}
		
		Double balance = accountBalance.get(trans.getMsisdn());
		if(balance == null){
			balance = new Double(0);
		} 
		accountBalance.put(trans.getMsisdn(), balance.floatValue() + trans.getAmount());
	}

	@Override
	public void commit(String transactionId) {
		chargingTransactions.remove(transactionId);
	}

	@Override
	public String reserveDebit(Msisdn msisdn, String amount) throws ChargingException {
		Double balance = accountBalance.get(msisdn);
		if (balance == null) {
			balance = new Double(initialBalance);
			accountBalance.put(msisdn, balance);
		}

		double amountF = Double.parseDouble(amount);
		if (balance > amountF) {
			accountBalance.put(msisdn, balance - amountF);
		} else {
			throw new ChargingException("No sufficient balance");
		}
		String transId = String.valueOf(rand.nextLong());
		
		chargingTransactions.put(transId, new ChargingTransaction(amountF, msisdn));
		
		return transId;
	}
	
	public double getBalance(Msisdn msisdn){
		return accountBalance.get(msisdn);
	}

}

class ChargingTransaction {
	private double amount;
	private Msisdn msisdn;
	public ChargingTransaction(double amount, Msisdn msisdn) {
		super();
		this.amount = amount;
		this.msisdn = msisdn;
	}
	public double getAmount() {
		return amount;
	}
	public Msisdn getMsisdn() {
		return msisdn;
	}
	
	
}
