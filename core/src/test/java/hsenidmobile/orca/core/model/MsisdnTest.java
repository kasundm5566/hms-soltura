/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hsenidmobile.orca.core.model;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 * @author hms
 *
 */
public class MsisdnTest {

	/**
	 * Test method for {@link hsenidmobile.orca.core.model.Msisdn#equals(hsenidmobile.orca.core.model.Msisdn)}.
	 */
	@Test
	public void testEqualsMsisdn() {
		Msisdn msisdn1 = new Msisdn("776123456", "DIALOG");
		Msisdn msisdn2 = new Msisdn("776123456", "DIALOG");
		
		assertTrue(msisdn1.equals(msisdn2));
	}

}
