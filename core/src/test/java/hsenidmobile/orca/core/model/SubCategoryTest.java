/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.model;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.junit.Test;


/**
 * $LastChangedDate$ $LastChangedBy$ $LastChangedRevision$
 *
 */
public class SubCategoryTest {

	/**
	 * Test method for
	 * {@link hsenidmobile.orca.core.model.SubCategory#isMatch(java.lang.String)}
	 * .
	 */
	@Test
	public void testIsMatch() {
		SubCategory category = new SubCategory();
		category.setKeyword("keyword");

		assertTrue("should match", category.isMatch("keyword"));
		assertTrue("should match", category.isMatch("Keyword"));
	}

	@Test
	public void testIsMatchNotFound() {
		SubCategory category = new SubCategory();
		category.setKeyword("keyword");

		assertFalse("should be false", category.isMatch("key"));
		assertFalse("should be false", category.isMatch(null));
	}


	@Test
	public void testIsMatchWithAlias() {
		SubCategory category = new SubCategory();
		category.setKeyword("keyword");
		category.setAliases(Arrays.asList("kayword", "kaiword"));

		assertTrue("should match", category.isMatch("keyword"));
		assertTrue("should match for alias", category.isMatch("kayword"));
		assertTrue("should match for alias", category.isMatch("kaiword"));
	}

}
