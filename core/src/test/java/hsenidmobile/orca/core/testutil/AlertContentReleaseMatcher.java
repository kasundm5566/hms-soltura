/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.testutil;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import hsenidmobile.orca.core.applications.alert.AlertContentRelease;
import hsenidmobile.orca.core.model.Content;

import java.util.Map;
import java.util.Set;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.TypeSafeMatcher;

/**
 * $LastChangedDate$ $LastChangedBy$ $LastChangedRevision$
 *
 */
public class AlertContentReleaseMatcher extends TypeSafeMatcher<AlertContentRelease> {

	private boolean subCategorySupported;
	private Map<String, String> contents;

	@Factory
	public static AlertContentReleaseMatcher alertContentWith(boolean subCategorySupported, Map<String, String> contents){
		return new AlertContentReleaseMatcher(subCategorySupported, contents);
	}

	public AlertContentReleaseMatcher(boolean subCategorySupported, Map<String, String> contents) {
		super();
		this.subCategorySupported = subCategorySupported;
		this.contents = contents;
	}

	@Override
	public void describeTo(Description description) {
		description.appendText("ContentRelease should be ").appendValue(contents);

	}

	@Override
	public boolean matchesSafely(AlertContentRelease item) {
		if (subCategorySupported) {
			Set<String> subcategories = contents.keySet();
			for (String subCategory : subcategories) {
				Content content = item.getContent(subCategory);
				assertNotNull("No content found for subcategory[" + subCategory + "]", content);
				assertEquals("Message for the SubCategory[" + subCategory + "]", contents.get(subCategory), content
						.getContent().getMessage());
			}
			return true;
		} else {
			assertEquals("There should be one content for SubCategory not supported ContentRelease", 1, item
					.getContents().size());
			assertEquals("Message for SubCategory not supported", contents.get(""), item.getContents().get(0).getContent().getMessage());
			return true;
		}
	}

}
