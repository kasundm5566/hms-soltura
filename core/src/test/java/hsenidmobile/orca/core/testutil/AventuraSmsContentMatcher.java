/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.testutil;

import hsenidmobile.orca.core.model.message.SmsMessage;

import java.util.List;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

/**
 * $LastChangedDate$ $LastChangedBy$ $LastChangedRevision$
 *
 */
public class AventuraSmsContentMatcher extends TypeSafeMatcher<List<SmsMessage>> {
	/**
	 * Evaluate True if given content matches with the content of one of the sms
	 * messages passed.
	 */
	@Factory
	public static Matcher<List<SmsMessage>> smsMessageWithContent(String content) {
		return new AventuraSmsContentMatcher(content);
	}

	private String content;

	public AventuraSmsContentMatcher(String subkeyword) {
		super();
		this.content = subkeyword;
	}

	@Override
	public void describeTo(Description description) {
		description.appendText("content of sms message should be ").appendValue(content);
	}

	@Override
	public boolean matchesSafely(List<SmsMessage> item) {
		if (item.size() > 0) {
			boolean matchFound = false;
			for (SmsMessage smsMessage : item) {
				if (smsMessage.getMessage().equals(content)) {
					matchFound = true;
				}
			}

			return matchFound;
		} else {
			return false;
		}
	}
}
