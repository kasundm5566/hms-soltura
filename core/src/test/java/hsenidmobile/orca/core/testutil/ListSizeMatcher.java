/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.testutil;

import java.util.List;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

/**
 * $LastChangedDate$ $LastChangedBy$ $LastChangedRevision$
 *
 */
public class ListSizeMatcher extends TypeSafeMatcher<List> {
	/**
	 * Evaluate True if given size matches with the size of list passes.
	 */
	@Factory
	public static Matcher<List> listOfSize(int size) {
		return new ListSizeMatcher(size);
	}

	private int expectedSize;

	public ListSizeMatcher(int expectedSize) {
		super();
		this.expectedSize = expectedSize;
	}

	@Override
	public void describeTo(Description description) {
		description.appendText("number of items in the list should be ").appendValue(expectedSize);
	}

	@Override
	public boolean matchesSafely(List item) {
		if (item.size() == expectedSize) {
			return true;
		} else {
			return false;
		}
	}
}
