/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.testutil;

import java.util.List;

import hsenidmobile.orca.core.applications.subscription.SubscriptionContentRelease;
import hsenidmobile.orca.core.model.Content;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.TypeSafeMatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.Assert.*;

/**
 * $LastChangedDate$ $LastChangedBy$ $LastChangedRevision$
 *
 */
public class SubscriptionContentReleaseMatcher extends TypeSafeMatcher<List<SubscriptionContentRelease>> {
     private static final Logger logger = LoggerFactory.getLogger(SubscriptionContentReleaseMatcher.class);
	private List<SubscriptionContentRelease> expectedContents;

	@Factory
	public static SubscriptionContentReleaseMatcher subsContentWith(List<SubscriptionContentRelease> expectedContents) {
		return new SubscriptionContentReleaseMatcher(expectedContents);
	}

	public SubscriptionContentReleaseMatcher(List<SubscriptionContentRelease> expectedContents) {
		super();
		this.expectedContents = expectedContents;
	}

	@Override
	public void describeTo(Description description) {
		description.appendText("Expected content list size should be ").appendValue(expectedContents.size());
		description.appendText("Expected content should be ").appendValue(expectedContents);

	}

	@Override
	public boolean matchesSafely(List<SubscriptionContentRelease> item) {
		if (expectedContents.size() != item.size()) {
			return false;
		}

		for (int i = 0; i < expectedContents.size(); i++) {
            logger.info("Compairing [" + expectedContents.get(i) + "] with[" + item.get(i) + "]");
			checkEquals(expectedContents.get(i), item.get(i));
		}
		return true;
	}

	private void checkEquals(SubscriptionContentRelease expected, SubscriptionContentRelease given) {
		assertEquals("Scheduled From", expected.getScheduledDispatchTimeFrom(), given.getScheduledDispatchTimeFrom());
		assertEquals("Scheduled To", expected.getScheduledDispatchTimeTo(), given.getScheduledDispatchTimeTo());
		assertEquals("Status", expected.getStatus(), given.getStatus());

		List<Content> expectedContents = expected.getContents();
		List<Content> givenContents = given.getContents();
		assertEquals("Contents list size", expectedContents.size(), givenContents.size());

		for (int i = 0; i < expectedContents.size(); i++) {
			checkEquals(expectedContents.get(i), givenContents.get(i));
		}

	}

	private void checkEquals(Content expected, Content given) {
		assertEquals("Content text", expected.getContent().getMessage(), given.getContent().getMessage());

		if (expected.getSubCategory() == null) {
			assertNull("Subcategory", given.getSubCategory());
		} else {
			assertNotNull("Subcategory", given.getSubCategory());
			assertEquals("SubCategory", expected.getSubCategory().getKeyword(), given.getSubCategory().getKeyword());
		}

	}

}
