/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.testutil;

import org.joda.time.DateTime;
import org.joda.time.DateTimeUtils;
import org.junit.Ignore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
@Ignore //This is not a test class
public class TestUtil {

	/**
	 * Change Joda current time to our specific value for testing purposes NOTE:
	 *
	 * In order for this to work Class should use
	 * DateTimeUtils.currentTimeMillis() to get current time
	 *
	 * @param now
	 */
    private static final Logger logger = LoggerFactory.getLogger(TestUtil.class);
	public static void setJodaSystemTime(DateTime now) {
		DateTimeUtils.setCurrentMillisFixed(now.getMillis());

        logger.debug("Actual SysTime[" + new DateTime(System.currentTimeMillis()) + "]" + ", Modified SysTime["
				+ new DateTime(DateTimeUtils.currentTimeMillis()) + "]");
	}

	public static void resetCurrentTime() {
		DateTimeUtils.setCurrentMillisFixed(System.currentTimeMillis());
	}

}
