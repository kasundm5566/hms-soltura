/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.flow.exception

import org.scalatest.Spec
import org.scalatest.matchers.ShouldMatchers
import scala.collection.mutable.Stack
import org.scalatest.junit.JUnitRunner
import org.junit.runner.RunWith
import hsenidmobile.orca.core.model.message.SmsMessage
import hsenidmobile.orca.core.flow.event.MessageFlowEvent
import hsenidmobile.orca.core.model.message.MessageContext
import hsenidmobile.orca.core.model.message.Message
import scala.util.matching.Regex
import scala.util.matching.Regex.MatchIterator

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
@RunWith(classOf[JUnitRunner])
class CommonDataExtractorTest extends Spec with ShouldMatchers {

  describe("CommonDataExtractor") {
    val extractor: CommonDataExtractor = new CommonDataExtractor

    it("should return blank when message event is null") {
      extractor.extract("app_Id", null) should equal("")
    }

    it("should return blank when message content is null") {
      val event: MessageFlowEvent[Message] = new MessageFlowEvent(new SmsMessage, null)
      extractor.extract("message", event) should equal("")
    }

    it("test") {
      val event: MessageFlowEvent[Message] = new MessageFlowEvent(new SmsMessage, null)
      extractor.extract("message", event) should equal("")

      val placeHolderRegx: Regex = new Regex("""\$\{(\w+)\}""")
      var text = "No application found for ${shortcode}/${keyword}."
      val placeHolders = placeHolderRegx.findAllIn(text)
      for { placeHolder <- placeHolders } {
        text = text.replace(placeHolder, extractor.extract(placeHolder, event))
        println(text)
      }

      placeHolders match {
        case _ => println
      }

    }

  }

  def sumOdd(in: List[Int]): Int = in match {
    case Nil => 0
    case x :: rest if x % 2 == 1 => x + sumOdd(rest)
    case _ :: rest => sumOdd(rest)
  }

}