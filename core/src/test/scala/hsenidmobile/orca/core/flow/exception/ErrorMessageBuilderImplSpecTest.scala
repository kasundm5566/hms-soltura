/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hsenidmobile.orca.core.flow.exception

import org.scalatest.Spec
import org.scalatest.matchers.ShouldMatchers
import scala.collection.mutable.Stack
import org.scalatest.junit.JUnitRunner
import org.junit.runner.RunWith
import hsenidmobile.orca.core.applications.exception.ApplicationException
import hsenidmobile.orca.core.model.message.SmsMessage
import hsenidmobile.orca.core.flow.MessageFlowErrorReasons
import java.util.HashMap
import java.util.Map
import hsenidmobile.orca.core.flow.event.MessageFlowEvent
import hsenidmobile.orca.core.model.message.Message

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
@RunWith(classOf[JUnitRunner])
class ErrorMessageBuilderImplSpecTest extends Spec with ShouldMatchers {
  describe("For APP_NOT_FOUND error") {
    val builder: ErrorMessageBuilderImpl = new ErrorMessageBuilderImpl
    builder.setCommonDataExtractor(new CommonDataExtractor)
    val messageMapping: Map[String, String] = new HashMap
    messageMapping.put("APP_NOT_FOUND", "Application {app_id} not found")
    builder.setMessageMapping(messageMapping)
    builder.setDefaultErrorMessage("Internal error occurred");

    it("When app_id is given as exception parameter") {
      val ex: ApplicationException = new ApplicationException("", MessageFlowErrorReasons.APP_NOT_FOUND)
      ex.addParameter("app_id", "My Application");
      val msg = builder.buildResponse(ex)
      assert(msg === "Application My Application not found")
    }

    it("When reason not given") {
      val ex: ApplicationException = new ApplicationException("", null)
      val msg = builder.buildResponse(ex)
      assert(msg === "Internal error occurred")
    }

    it("When message flow event is provided but it do not have application data") {
      val event: MessageFlowEvent[Message] = new MessageFlowEvent(new SmsMessage, null)
      val ex: ApplicationException = new ApplicationException("", MessageFlowErrorReasons.APP_NOT_FOUND, event)
      val msg = builder.buildResponse(ex)

      msg should equal("Application  not found")
    }

    it("When message flow event is provided and it has application data") {
      import hsenidmobile.orca.core.applications.ApplicationImpl
      import hsenidmobile.orca.core.model.message.MessageContext

      val app: ApplicationImpl = new ApplicationImpl
      app.setAppName("My Application")
      val context: MessageContext = new MessageContext
      context.setApplication(app)

      val event: MessageFlowEvent[Message] = new MessageFlowEvent(new SmsMessage, context)
      val ex: ApplicationException = new ApplicationException("", MessageFlowErrorReasons.APP_NOT_FOUND, event)
      val msg = builder.buildResponse(ex)

      msg should equal("Application My Application not found")
    }
  }

}