exec sp_msforeachtable @command1="Alter table ? nocheck constraint all", @command2="drop table ? "
exec sp_msforeachtable @command1="Alter table ? nocheck constraint all", @command2="drop table ? "
exec sp_msforeachtable @command1="Alter table ? nocheck constraint all", @command2="drop table ? "
exec sp_msforeachtable @command1="Alter table ? nocheck constraint all", @command2="drop table ? "

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[voting_result_summary]') AND type in (N'U'))
DROP TABLE [dbo].[voting_result_summary]
CREATE TABLE voting_result_summary ( votelet_id BIGINT NOT NULL, candidate_id BIGINT NOT NULL, total_vote_count BIGINT NOT NULL, last_update_time DATETIME, CONSTRAINT PK_voting_result_summary PRIMARY KEY (candidate_id) );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[invalid_request_summary]') AND type in (N'U'))
DROP TABLE [dbo].[invalid_request_summary]
CREATE TABLE invalid_request_summary ( app_id NVARCHAR(60) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL, total_requests BIGINT NOT NULL, last_update_time DATETIME, CONSTRAINT PK_invalid_request_summary PRIMARY KEY (app_id) );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[message_dispatching_summary]') AND type in (N'U'))
DROP TABLE [dbo].[message_dispatching_summary]
CREATE TABLE message_dispatching_summary ( app_id NVARCHAR(60) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL, status_code NVARCHAR(60) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL, total_count BIGINT NOT NULL, last_update_time DATETIME, CONSTRAINT PK_message_dispatching_summary PRIMARY KEY (app_id, status_code) );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[mo_summary]') AND type in (N'U'))
DROP TABLE [dbo].[mo_summary]
CREATE TABLE mo_summary ( app_id NVARCHAR(60) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL, total_count BIGINT NOT NULL, last_update_time DATETIME, CONSTRAINT PK_mo_summary PRIMARY KEY (app_id) );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[abuse_report]') AND type in (N'U'))
DROP TABLE [dbo].[abuse_report]
    create table abuse_report (
        id numeric(19,0) identity not null,
        created_time datetime null,
        last_modified_time datetime null,
        version numeric(19,0) null,
        app_id varchar(255) null,
        content varchar(160) null,
        address varchar(15) not null,
        operator varchar(10) not null,
        received_time numeric(19,0) null,
        primary key (id)
    );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[alert_service]') AND type in (N'U'))
DROP TABLE [dbo].[alert_service]
    create table alert_service (
        id numeric(19,0) identity not null,
        created_time datetime null,
        last_modified_time datetime null,
        version numeric(19,0) null,
        primary key (id)
    );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[alert_service_keyword_detail]') AND type in (N'U'))
DROP TABLE [dbo].[alert_service_keyword_detail]
    create table alert_service_keyword_detail (
        alert_service_id numeric(19,0) not null,
        latestContent_id numeric(19,0) not null,
        unique (latestContent_id)
    );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[application]') AND type in (N'U'))
DROP TABLE [dbo].[application]
    create table application (
        id numeric(19,0) identity not null,
        created_time datetime null,
        last_modified_time datetime null,
        version numeric(19,0) null,
        app_id varchar(20) not null unique,
        app_name varchar(50) not null unique,
        data_message_policies varchar(255) null,
        data_message_policies_id numeric(19,0) null,
        description varchar(255) null,
        end_date numeric(19,0) not null,
        massage_sending_charging_policy varchar(255) null,
        massage_sending_charging_policy_id numeric(19,0) not null,
        ncs_type varchar(10) null,
        registration_policy varchar(7) not null,
        service varchar(255) null,
        service_id numeric(19,0) null,
        start_date numeric(19,0) not null,
        status varchar(10) null,
        user_registration_charging_policy varchar(255) null,
        user_registration_charging_policy_id numeric(19,0) not null,
        application_routing_info numeric(19,0) null,
        primary key (id)
    );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[application_author]') AND type in (N'U'))
DROP TABLE [dbo].[application_author]
    create table application_author (
        application_id numeric(19,0) not null,
        authors_id numeric(19,0) not null,
        unique (authors_id)
    );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[application_routing_info]') AND type in (N'U'))
DROP TABLE [dbo].[application_routing_info]
    create table application_routing_info (
        id numeric(19,0) identity not null,
        created_time datetime null,
        last_modified_time datetime null,
        version numeric(19,0) null,
        primary key (id)
    );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[application_routing_info_routing_info]') AND type in (N'U'))
DROP TABLE [dbo].[application_routing_info_routing_info]
    create table application_routing_info_routing_info (
        application_routing_info_id numeric(19,0) not null,
        routingInfos_id numeric(19,0) not null,
        unique (routingInfos_id)
    );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[author]') AND type in (N'U'))
DROP TABLE [dbo].[author]
    create table author (
        id numeric(19,0) identity not null,
        created_time datetime null,
        last_modified_time datetime null,
        version numeric(19,0) null,
        address varchar(15) not null,
        operator varchar(10) not null,
        primary key (id)
    );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[author_validation_policy]') AND type in (N'U'))
DROP TABLE [dbo].[author_validation_policy]
    create table author_validation_policy (
        id numeric(19,0) identity not null,
        next_policy varchar(255) null,
        next_policy_id numeric(19,0) null,
        primary key (id)
    );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[available_short_code]') AND type in (N'U'))
DROP TABLE [dbo].[available_short_code]
    create table available_short_code (
        id numeric(19,0) identity not null,
        created_time datetime null,
        last_modified_time datetime null,
        version numeric(19,0) null,
        ncs varchar(5) not null,
        address varchar(15) not null,
        operator varchar(10) not null,
        primary key (id)
    );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[billing_info]') AND type in (N'U'))
DROP TABLE [dbo].[billing_info]
    create table billing_info (
        id numeric(19,0) identity not null,
        created_time datetime null,
        last_modified_time datetime null,
        version numeric(19,0) null,
        billing_address varchar(255) not null,
        billing_name varchar(255) null,
        express_delivery tinyint null,
        primary key (id)
    );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[candidate_vote]') AND type in (N'U'))
DROP TABLE [dbo].[candidate_vote]
    create table candidate_vote (
        candidate_id numeric(19,0) null,
        vote_id numeric(19,0) not null,
        primary key (vote_id)
    );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[charge_after_deliver_policy]') AND type in (N'U'))
DROP TABLE [dbo].[charge_after_deliver_policy]
    create table charge_after_deliver_policy (
        id numeric(19,0) identity not null,
        created_time datetime null,
        last_modified_time datetime null,
        version numeric(19,0) null,
        amount float not null,
        primary key (id)
    );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[charge_before_send_policy]') AND type in (N'U'))
DROP TABLE [dbo].[charge_before_send_policy]
    create table charge_before_send_policy (
        id numeric(19,0) identity not null,
        created_time datetime null,
        last_modified_time datetime null,
        version numeric(19,0) null,
        amount float not null,
        primary key (id)
    );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[competition]') AND type in (N'U'))
DROP TABLE [dbo].[competition]
    create table competition (
        id numeric(19,0) identity not null,
        created_time datetime null,
        last_modified_time datetime null,
        version numeric(19,0) null,
        primary key (id)
    );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[content_group]') AND type in (N'U'))
DROP TABLE [dbo].[content_group]
    create table content_group (
        id numeric(19,0) identity not null,
        created_time datetime null,
        last_modified_time datetime null,
        version numeric(19,0) null,
        contentGroupId numeric(19,0) not null,
        primary key (id)
    );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[content_group_keyword_detail]') AND type in (N'U'))
DROP TABLE [dbo].[content_group_keyword_detail]
    create table content_group_keyword_detail (
        content_group_id numeric(19,0) not null,
        contents_id numeric(19,0) not null,
        unique (contents_id)
    );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[content_provider]') AND type in (N'U'))
DROP TABLE [dbo].[content_provider]
    create table content_provider (
        id numeric(19,0) identity not null,
        created_time datetime null,
        last_modified_time datetime null,
        version numeric(19,0) null,
        contact_person varchar(255) null,
        cp_id varchar(20) not null unique,
        createDate datetime null,
        description varchar(255) null,
        email varchar(35) null,
        isPersonal tinyint not null,
        lastChargedDate datetime null,
        name varchar(50) not null unique,
        nic varchar(255) null,
        registration_code varchar(255) null,
        status varchar(255) null,
        telephone varchar(255) null,
        billingInfo_id numeric(19,0) null,
        primary key (id)
    );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[content_provider_application]') AND type in (N'U'))
DROP TABLE [dbo].[content_provider_application]
    create table content_provider_application (
        content_provider_id numeric(19,0) not null,
        applications_id numeric(19,0) not null,
        unique (applications_id)
    );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[content_provider_cp_charging_msisdn]') AND type in (N'U'))
DROP TABLE [dbo].[content_provider_cp_charging_msisdn]
    create table content_provider_cp_charging_msisdn (
        content_provider_id numeric(19,0) not null,
        msisdns_id numeric(19,0) not null,
        unique (msisdns_id)
    );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[content_provider_routing_info]') AND type in (N'U'))
DROP TABLE [dbo].[content_provider_routing_info]
    create table content_provider_routing_info (
        content_provider_id numeric(19,0) not null,
        ownedRoutingInfo_id numeric(19,0) not null,
        unique (ownedRoutingInfo_id)
    );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[cp_charging_msisdn]') AND type in (N'U'))
DROP TABLE [dbo].[cp_charging_msisdn]
    create table cp_charging_msisdn (
        id numeric(19,0) identity not null,
        created_time datetime null,
        last_modified_time datetime null,
        version numeric(19,0) null,
        address varchar(15) not null,
        operator varchar(10) not null,
        msisdn_verified tinyint null,
        verification_code varchar(255) null,
        primary key (id)
    );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[cp_user]') AND type in (N'U'))
DROP TABLE [dbo].[cp_user]
    create table cp_user (
        id numeric(19,0) identity not null,
        enabled tinyint not null,
        name varchar(35) not null,
        password varchar(35) not null,
        role varchar(35) not null,
        content_provider_id numeric(19,0) null,
        primary key (id)
    );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[developer]') AND type in (N'U'))
DROP TABLE [dbo].[developer]
    create table developer (
        id numeric(19,0) identity not null,
        created_time datetime null,
        last_modified_time datetime null,
        version numeric(19,0) null,
        mo_url varbinary(255) null,
        ws_password varchar(255) null,
        primary key (id)
    );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[duration_between_message]') AND type in (N'U'))
DROP TABLE [dbo].[duration_between_message]
    create table duration_between_message (
        periodicity_id numeric(19,0) null,
        id numeric(19,0) not null,
        primary key (id)
    );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[free_charging_policy]') AND type in (N'U'))
DROP TABLE [dbo].[free_charging_policy]
    create table free_charging_policy (
        id numeric(19,0) identity not null,
        created_time datetime null,
        last_modified_time datetime null,
        version numeric(19,0) null,
        primary key (id)
    );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[invalid_request_error_message]') AND type in (N'U'))
DROP TABLE [dbo].[invalid_request_error_message]
    create table invalid_request_error_message (
        application_id numeric(19,0) not null,
        element varchar(255) null,
        mapkey varchar(255) not null,
        primary key (application_id, mapkey)
    );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[keyword_aliases]') AND type in (N'U'))
DROP TABLE [dbo].[keyword_aliases]
    create table keyword_aliases (
        keyword_detail_id numeric(19,0) not null,
        element varchar(255) null
    );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[keyword_detail]') AND type in (N'U'))
DROP TABLE [dbo].[keyword_detail]
    create table keyword_detail (
        DTYPE varchar(31) not null,
        id numeric(19,0) identity not null,
        created_time datetime null,
        last_modified_time datetime null,
        version numeric(19,0) null,
        keyword varchar(20) not null,
        isCorrect tinyint null,
        primary key (id)
    );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[keyword_detail_description]') AND type in (N'U'))
DROP TABLE [dbo].[keyword_detail_description]
    create table keyword_detail_description (
        keyword_detail_id numeric(19,0) not null,
        description varchar(255) null,
        mapkey varchar(255) not null,
        primary key (keyword_detail_id, mapkey)
    );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[n_messages_n_period_policy]') AND type in (N'U'))
DROP TABLE [dbo].[n_messages_n_period_policy]
    create table n_messages_n_period_policy (
        id numeric(19,0) identity not null,
        next_policy varchar(255) null,
        next_policy_id numeric(19,0) null,
        number_of_allowed_response int null,
        restriction_valid_period int null,
        restriction_valid_period_unit varchar(7) not null,
        primary key (id)
    );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[one_time_registration_charging_policy]') AND type in (N'U'))
DROP TABLE [dbo].[one_time_registration_charging_policy]
    create table one_time_registration_charging_policy (
        id numeric(19,0) identity not null,
        created_time datetime null,
        last_modified_time datetime null,
        version numeric(19,0) null,
        amount float not null,
        primary key (id)
    );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[periodic_subscription_charging_policy]') AND type in (N'U'))
DROP TABLE [dbo].[periodic_subscription_charging_policy]
    create table periodic_subscription_charging_policy (
        id numeric(19,0) identity not null,
        created_time datetime null,
        last_modified_time datetime null,
        version numeric(19,0) null,
        amount float not null,
        period_unit varchar(255) not null,
        period_value int not null,
        primary key (id)
    );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[periodicity]') AND type in (N'U'))
DROP TABLE [dbo].[periodicity]
    create table periodicity (
        id numeric(19,0) identity not null,
        created_time datetime null,
        last_modified_time datetime null,
        version numeric(19,0) null,
        allowed_buffer_time numeric(19,0) null,
        last_scheduled_sent_time numeric(19,0) null,
        last_sent_time numeric(19,0) null,
        period_value int null,
        unit varchar(255) null,
        primary key (id)
    );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[question]') AND type in (N'U'))
DROP TABLE [dbo].[question]
    create table question (
        id numeric(19,0) identity not null,
        endDate numeric(19,0) not null,
        question varchar(255) null,
        questionId varchar(255) null,
        startDate numeric(19,0) not null,
        status varchar(255) null,
        primary key (id)
    );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[question_keyword_detail]') AND type in (N'U'))
DROP TABLE [dbo].[question_keyword_detail]
    create table question_keyword_detail (
        question_id numeric(19,0) not null,
        answers_id numeric(19,0) not null,
        unique (answers_id)
    );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[registration_info]') AND type in (N'U'))
DROP TABLE [dbo].[registration_info]
    create table registration_info (
        id numeric(19,0) identity not null,
        created_time datetime null,
        last_modified_time datetime null,
        version numeric(19,0) null,
        app_id varchar(15) null,
        last_sent_content_id varchar(255) null,
        last_subscription_renewed_date numeric(19,0) null,
        message_last_dispatched_time numeric(19,0) null,
        preferredLanguage varchar(255) null,
        status varchar(255) null,
        sub_keywords varchar(255) null,
        subscription_date numeric(19,0) null,
        subscriber_id numeric(19,0) null,
        primary key (id)
    );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[reserved_keyword]') AND type in (N'U'))
DROP TABLE [dbo].[reserved_keyword]
    create table reserved_keyword (
        id numeric(19,0) identity not null,
        created_time datetime null,
        last_modified_time datetime null,
        version numeric(19,0) null,
        keyword varchar(255) null,
        ncs varchar(5) not null,
        operator varchar(255) null,
        primary key (id)
    );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[routing_info]') AND type in (N'U'))
DROP TABLE [dbo].[routing_info]
    create table routing_info (
        id numeric(19,0) identity not null,
        created_time datetime null,
        last_modified_time datetime null,
        version numeric(19,0) null,
        end_date numeric(19,0) not null,
        last_charged_date numeric(19,0) not null,
        owningAppId varchar(255) null,
        start_date numeric(19,0) not null,
        status varchar(255) null,
        routing_key numeric(19,0) null,
        primary key (id)
    );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[routing_key]') AND type in (N'U'))
DROP TABLE [dbo].[routing_key]
    create table routing_key (
        id numeric(19,0) identity not null,
        created_time datetime null,
        last_modified_time datetime null,
        version numeric(19,0) null,
        keyword varchar(100) not null,
        address varchar(15) not null,
        operator varchar(10) not null,
        primary key (id)
    );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[subscriber]') AND type in (N'U'))
DROP TABLE [dbo].[subscriber]
    create table subscriber (
        id numeric(19,0) identity not null,
        language varchar(10) null,
        address varchar(15) not null,
        operator varchar(10) not null,
        name varchar(20) null,
        primary key (id)
    );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[subscriber_comment]') AND type in (N'U'))
DROP TABLE [dbo].[subscriber_comment]
    create table subscriber_comment (
        id numeric(19,0) identity not null,
        created_time datetime null,
        last_modified_time datetime null,
        version numeric(19,0) null,
        comment varchar(255) not null,
        date numeric(19,0) not null,
        address varchar(15) not null,
        operator varchar(10) not null,
        primary key (id)
    );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[subscriber_validation_policy]') AND type in (N'U'))
DROP TABLE [dbo].[subscriber_validation_policy]
    create table subscriber_validation_policy (
        id numeric(19,0) identity not null,
        next_policy varchar(255) null,
        data_message_policies_id numeric(19,0) null,
        primary key (id)
    );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[subscription]') AND type in (N'U'))
DROP TABLE [dbo].[subscription]
    create table subscription (
        id numeric(19,0) identity not null,
        created_time datetime null,
        last_modified_time datetime null,
        version numeric(19,0) null,
        data_last_updated_on numeric(19,0) null,
        message_last_dispatched_time numeric(19,0) null,
        default_periodicity_id numeric(19,0) null,
        primary key (id)
    );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[subscription_response]') AND type in (N'U'))
DROP TABLE [dbo].[subscription_response]
    create table subscription_response (
        application_id numeric(19,0) not null,
        element varchar(255) null,
        mapkey varchar(255) not null,
        primary key (application_id, mapkey)
    );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[subscription_service_data]') AND type in (N'U'))
DROP TABLE [dbo].[subscription_service_data]
    create table subscription_service_data (
        id numeric(19,0) identity not null,
        created_time datetime null,
        last_modified_time datetime null,
        version numeric(19,0) null,
        expectedDispatchTime datetime null,
        last_dispatched_content_id numeric(19,0) null,
        periodicity_id numeric(19,0) null,
        primary key (id)
    );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[subscription_service_data_content_group]') AND type in (N'U'))
DROP TABLE [dbo].[subscription_service_data_content_group]
    create table subscription_service_data_content_group (
        subscription_service_data_id numeric(19,0) not null,
        data_id numeric(19,0) not null,
        unique (data_id)
    );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[subscription_subscription_service_data]') AND type in (N'U'))
DROP TABLE [dbo].[subscription_subscription_service_data]
    create table subscription_subscription_service_data (
        subscription_id numeric(19,0) not null,
        subscriptionDataList_id numeric(19,0) not null,
        unique (subscriptionDataList_id)
    );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[unsubscribe_response]') AND type in (N'U'))
DROP TABLE [dbo].[unsubscribe_response]
    create table unsubscribe_response (
        application_id numeric(19,0) not null,
        element varchar(255) null,
        mapkey varchar(255) not null,
        primary key (application_id, mapkey)
    );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[vote]') AND type in (N'U'))
DROP TABLE [dbo].[vote]
    create table vote (
        id numeric(19,0) identity not null,
        created_time datetime null,
        last_modified_time datetime null,
        version numeric(19,0) null,
        address varchar(15) not null,
        operator varchar(10) not null,
        received_message varchar(255) null,
        received_time numeric(19,0) not null,
        primary key (id)
    );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[votelet]') AND type in (N'U'))
DROP TABLE [dbo].[votelet]
    create table votelet (
        id numeric(19,0) identity not null,
        created_time datetime null,
        last_modified_time datetime null,
        version numeric(19,0) null,
        active_period_type varchar(255) null,
        active_period_id numeric(19,0) null,
        end_date numeric(19,0) not null,
        is_vote_result_public tinyint null,
        response_spec_type varchar(255) null,
        voting_response_id numeric(19,0) null,
        start_date numeric(19,0) not null,
        status varchar(10) null,
        primary key (id)
    );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[votelet_active_whole_day]') AND type in (N'U'))
DROP TABLE [dbo].[votelet_active_whole_day]
    create table votelet_active_whole_day (
        id numeric(19,0) identity not null,
        created_time datetime null,
        last_modified_time datetime null,
        version numeric(19,0) null,
        primary key (id)
    );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[votelet_keyword_detail]') AND type in (N'U'))
DROP TABLE [dbo].[votelet_keyword_detail]
    create table votelet_keyword_detail (
        votelet_id numeric(19,0) not null,
        candidates_id numeric(19,0) not null,
        unique (candidates_id)
    );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[votelet_response_spec_no_resp]') AND type in (N'U'))
DROP TABLE [dbo].[votelet_response_spec_no_resp]
    create table votelet_response_spec_no_resp (
        id numeric(19,0) identity not null,
        created_time datetime null,
        last_modified_time datetime null,
        version numeric(19,0) null,
        primary key (id)
    );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[votelet_response_spec_result_summary]') AND type in (N'U'))
DROP TABLE [dbo].[votelet_response_spec_result_summary]
    create table votelet_response_spec_result_summary (
        id numeric(19,0) identity not null,
        created_time datetime null,
        last_modified_time datetime null,
        version numeric(19,0) null,
        result_header_txt varchar(255) null,
        result_tailer_txt varchar(255) null,
        primary key (id)
    );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[voting_service]') AND type in (N'U'))
DROP TABLE [dbo].[voting_service]
    create table voting_service (
        id numeric(19,0) identity not null,
        created_time datetime null,
        last_modified_time datetime null,
        version numeric(19,0) null,
        primary key (id)
    );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[voting_service_votelet]') AND type in (N'U'))
DROP TABLE [dbo].[voting_service_votelet]
    create table voting_service_votelet (
        voting_service_id numeric(19,0) not null,
        votelet_id numeric(19,0) not null,
        unique (votelet_id)
    );

    alter table alert_service_keyword_detail
        add constraint FK731584744B57F23F
        foreign key (alert_service_id)
        references alert_service;

    alter table alert_service_keyword_detail
        add constraint FK731584744C44CD1E
        foreign key (latestContent_id)
        references keyword_detail;

    alter table application
        add constraint FK5CA405505C3B999E
        foreign key (application_routing_info)
        references application_routing_info;

    alter table application_author
        add constraint FKCB77F5AB2869EE
        foreign key (application_id)
        references application;

    alter table application_author
        add constraint FKCB77F5A6AF5B961
        foreign key (authors_id)
        references author;

    alter table application_routing_info_routing_info
        add constraint FKC73C9710B0A0BA8B
        foreign key (routingInfos_id)
        references routing_info;

    alter table application_routing_info_routing_info
        add constraint FKC73C9710B80B0D6C
        foreign key (application_routing_info_id)
        references application_routing_info;

    alter table candidate_vote
        add constraint FK536070262F9C8015
        foreign key (vote_id)
        references vote;

    alter table candidate_vote
        add constraint FK53607026E03B450D
        foreign key (candidate_id)
        references keyword_detail;

    alter table content_group_keyword_detail
        add constraint FKB773952D165709CC
        foreign key (content_group_id)
        references content_group;

    alter table content_group_keyword_detail
        add constraint FKB773952D91100416
        foreign key (contents_id)
        references keyword_detail;

    alter table content_provider
        add constraint FKD162FAF787841C56
        foreign key (billingInfo_id)
        references billing_info;

    alter table content_provider_application
        add constraint FK1AB7A408D97553B
        foreign key (applications_id)
        references application;

    alter table content_provider_application
        add constraint FK1AB7A408CEC1AA09
        foreign key (content_provider_id)
        references content_provider;

    alter table content_provider_cp_charging_msisdn
        add constraint FKC9C335DECEC1AA09
        foreign key (content_provider_id)
        references content_provider;

    alter table content_provider_cp_charging_msisdn
        add constraint FKC9C335DE8D24591D
        foreign key (msisdns_id)
        references cp_charging_msisdn;

    alter table content_provider_routing_info
        add constraint FKE0B080CFCEC1AA09
        foreign key (content_provider_id)
        references content_provider;

    alter table content_provider_routing_info
        add constraint FKE0B080CFF43C525B
        foreign key (ownedRoutingInfo_id)
        references routing_info;

    alter table cp_user
        add constraint FK3999545D414D37C1
        foreign key (id)
        references content_provider;

    alter table cp_user
        add constraint FK3999545DCEC1AA09
        foreign key (content_provider_id)
        references content_provider;

    alter table duration_between_message
        add constraint FKB3E8862590515886
        foreign key (id)
        references registration_info;

    alter table duration_between_message
        add constraint FKB3E886257CE497D5
        foreign key (periodicity_id)
        references periodicity;

    alter table invalid_request_error_message
        add constraint FK6CDEDED8B2869EE
        foreign key (application_id)
        references application;

    alter table keyword_aliases
        add constraint FK935E1AA8775B0A9
        foreign key (keyword_detail_id)
        references keyword_detail;

    alter table keyword_detail_description
        add constraint FKD9D72F24775B0A9
        foreign key (keyword_detail_id)
        references keyword_detail;

    alter table question
        add constraint FKBA823BE6A31FE91F
        foreign key (id)
        references competition;

    alter table question_keyword_detail
        add constraint FK83A658E0FD3FA447
        foreign key (answers_id)
        references keyword_detail;

    alter table question_keyword_detail
        add constraint FK83A658E0AB683D5
        foreign key (question_id)
        references question;

    alter table registration_info
        add constraint FKAFC9C074490621E
        foreign key (subscriber_id)
        references subscriber;

    alter table routing_info
        add constraint FKDC504887B0854C83
        foreign key (routing_key)
        references routing_key;

    alter table subscription
        add constraint FK1456591DA65FC593
        foreign key (default_periodicity_id)
        references periodicity;

    alter table subscription_response
        add constraint FK922BDD23B2869EE
        foreign key (application_id)
        references application;

    alter table subscription_service_data
        add constraint FK70B052D67CE497D5
        foreign key (periodicity_id)
        references periodicity;

    alter table subscription_service_data_content_group
        add constraint FKB3C1059041EF07BB
        foreign key (data_id)
        references content_group;

    alter table subscription_service_data_content_group
        add constraint FKB3C105908FDFB041
        foreign key (subscription_service_data_id)
        references subscription_service_data;

    alter table subscription_subscription_service_data
        add constraint FK822E2A34C3C4DD52
        foreign key (subscriptionDataList_id)
        references subscription_service_data;

    alter table subscription_subscription_service_data
        add constraint FK822E2A347C6BE8FF
        foreign key (subscription_id)
        references subscription;

    alter table unsubscribe_response
        add constraint FK275CB5EFB2869EE
        foreign key (application_id)
        references application;

    alter table votelet_keyword_detail
        add constraint FK13654E75711ED020
        foreign key (candidates_id)
        references keyword_detail;

    alter table votelet_keyword_detail
        add constraint FK13654E75AB1E079F
        foreign key (votelet_id)
        references votelet;

    alter table voting_service_votelet
        add constraint FK6081DC4F99E8C450
        foreign key (voting_service_id)
        references voting_service;

    alter table voting_service_votelet
        add constraint FK6081DC4FAB1E079F
        foreign key (votelet_id)
        references votelet;

