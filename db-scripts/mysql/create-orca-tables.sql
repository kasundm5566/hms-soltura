alter table alert_content_release_content drop foreign key FK8F67BE98104B2DF5;
alter table alert_content_release_content drop foreign key FK8F67BE9881AB4CC8;
alter table alert_service_sub_category drop foreign key FK6A9D6C6A4B57F23F;
alter table alert_service_sub_category drop foreign key FK6A9D6C6AE44BEB06;
alter table alert_service_supportedLocales drop foreign key FKC2F0C6984B57F23F;
alter table app_invalid_req_resp drop foreign key FKF4E8E277B2869EE;
alter table app_invalid_req_resp drop foreign key FKF4E8E277D65F0A4D;
alter table app_subscription_resp drop foreign key FK51166314B2869EE;
alter table app_subscription_resp drop foreign key FK51166314D65F0A4D;
alter table app_unsubscription_resp drop foreign key FK2C7A691BB2869EE;
alter table app_unsubscription_resp drop foreign key FK2C7A691BD65F0A4D;
alter table application_author drop foreign key FKCB77F5AB2869EE;
alter table application_author drop foreign key FKCB77F5A6AF5B961;
alter table candidate_vote drop foreign key FK536070262F9C8015;
alter table candidate_vote drop foreign key FK536070268A45EBD1;
alter table competition_sub_category drop foreign key FK438F1E9DAE8CEF9F;
alter table competition_sub_category drop foreign key FK438F1E9DE44BEB06;
alter table competition_supportedLocales drop foreign key FK8A88274BAE8CEF9F;
alter table content drop foreign key FK38B73479D65F0A4D;
alter table content drop foreign key FK38B734796016DA97;
alter table developer_sub_category drop foreign key FK9D37327228612E95;
alter table developer_sub_category drop foreign key FK9D373272E44BEB06;
alter table developer_supportedLocales drop foreign key FKE7A848A028612E95;
alter table keyword_aliases drop foreign key FK935E1AA86EEA0AF7;
alter table question drop foreign key FKBA823BE6A31FE91F;
alter table question_answer drop foreign key FK561DF237FD3FA447;
alter table question_answer drop foreign key FK561DF237AB683D5;
alter table request_service_data drop foreign key FK38FC22A46016DA97;
alter table request_service_localized_message drop foreign key FK1867B289DA91587D;
alter table request_service_localized_message drop foreign key FK1867B289F4774286;
alter table request_service_sub_category drop foreign key FK5227D117F4774286;
alter table request_service_sub_category drop foreign key FK5227D117E44BEB06;
alter table request_service_supportedLocales drop foreign key FK602F0CC5F4774286;
alter table sub_category_description drop foreign key FK2C3DD6DAE84A2EAE;
alter table sub_category_description drop foreign key FK2C3DD6DA6EEA0AF7;
alter table subscription drop foreign key FK1456591D3014263;
alter table subscription_content_release drop foreign key FKE9104F1F7CE497D5;
alter table subscription_content_release_content drop foreign key FKB0E19B594E425DAB;
alter table subscription_content_release_content drop foreign key FKB0E19B59104B2DF5;
alter table subscription_periodicity drop foreign key FK77A82C11F174BE5;
alter table subscription_periodicity drop foreign key FK77A82C117C6BE8FF;
alter table subscription_sub_category drop foreign key FK6919BFFF7C6BE8FF;
alter table subscription_sub_category drop foreign key FK6919BFFFE44BEB06;
alter table subscription_supportedLocales drop foreign key FKBD3F47AD7C6BE8FF;
alter table voting_result_response_template drop foreign key FK2F6D672E40A80387;
alter table voting_result_response_template drop foreign key FK2F6D672E7808968F;
alter table voting_service_sub_category drop foreign key FK3D5757F99E8C450;
alter table voting_service_sub_category drop foreign key FK3D5757FE44BEB06;
alter table voting_service_supportedLocales drop foreign key FK80243D2D99E8C450;
drop table if exists alert_app_traffic;
drop table if exists alert_content_release;
drop table if exists alert_content_release_content;
drop table if exists alert_service;
drop table if exists alert_service_sub_category;
drop table if exists alert_service_supportedLocales;
drop table if exists answer;
drop table if exists app_invalid_req_resp;
drop table if exists app_subscription_resp;
drop table if exists app_unsubscription_resp;
drop table if exists application;
drop table if exists application_author;
drop table if exists application_revenue_summary;
drop table if exists author;
drop table if exists author_validation_policy;
drop table if exists billing_info;
drop table if exists candidate_vote;
drop table if exists competition;
drop table if exists competition_sub_category;
drop table if exists competition_supportedLocales;
drop table if exists content;
drop table if exists developer;
drop table if exists developer_sub_category;
drop table if exists developer_supportedLocales;
drop table if exists keyword_aliases;
drop table if exists localized_message;
drop table if exists message_history;
drop table if exists n_messages_n_period_policy;
drop table if exists orca_sdp_app_integration;
drop table if exists periodicity;
drop table if exists question;
drop table if exists question_answer;
drop table if exists request_service;
drop table if exists request_service_data;
drop table if exists request_service_localized_message;
drop table if exists request_service_sub_category;
drop table if exists request_service_supportedLocales;
drop table if exists sub_category;
drop table if exists sub_category_description;
drop table if exists subscriber_validation_policy;
drop table if exists subscription;
drop table if exists subscription_content_release;
drop table if exists subscription_content_release_content;
drop table if exists subscription_periodicity;
drop table if exists subscription_sub_category;
drop table if exists subscription_supportedLocales;
drop table if exists vote;
drop table if exists votelet_active_whole_day;
drop table if exists votelet_response_spec_no_resp;
drop table if exists votelet_response_spec_result_summary;
drop table if exists voting_result_response_template;
drop table if exists voting_service;
drop table if exists voting_service_sub_category;
drop table if exists voting_service_supportedLocales;
create table alert_app_traffic (id bigint not null auto_increment, created_time datetime, last_modified_time datetime, version bigint, cmpd integer, cmpd_luts bigint, cmpm integer, cmpm_nssd bigint, app_id varchar(255), primary key (id)) ;
create table alert_content_release (id bigint not null auto_increment, created_time datetime, last_modified_time datetime, version bigint, message_id bigint, app_id varchar(255) not null, app_name varchar(255) not null, primary key (id)) ;
create table alert_content_release_content (alert_content_release_id bigint not null, contents_id bigint not null, unique (contents_id)) ;
create table alert_service (id bigint not null auto_increment, created_time datetime, last_modified_time datetime, version bigint, default_locale varchar(255) not null, mapd integer, mapm integer, is_sms_update_support_required bit, primary key (id)) ;
create table alert_service_sub_category (alert_service_id bigint not null, supportedSubCategories_id bigint not null, idx integer not null, primary key (alert_service_id, idx), unique (supportedSubCategories_id)) ;
create table alert_service_supportedLocales (alert_service_id bigint not null, element varchar(255)) ;
create table answer (id bigint not null auto_increment, created_time datetime, last_modified_time datetime, version bigint, answer tinyblob, isCorrect bit not null, primary key (id)) ;
create table app_invalid_req_resp (application_id bigint not null, localized_msg_id bigint not null, idx integer not null, primary key (application_id, idx), unique (localized_msg_id)) ;
create table app_subscription_resp (application_id bigint not null, localized_msg_id bigint not null, idx integer not null, primary key (application_id, idx), unique (localized_msg_id)) ;
create table app_unsubscription_resp (application_id bigint not null, localized_msg_id bigint not null, idx integer not null, primary key (application_id, idx), unique (localized_msg_id)) ;
create table application (id bigint not null auto_increment, created_time datetime, last_modified_time datetime, version bigint, app_id varchar(20) not null unique, app_name varchar(50) not null unique, data_message_policies varchar(255), data_message_policies_id bigint, description varchar(255), end_date datetime not null, ncs_type varchar(10), registration_policy varchar(7) not null, service varchar(255), service_id bigint, start_date datetime not null, status varchar(30), is_expirable BIT DEFAULT FALSE, primary key (id)) ;
create table application_author (application_id bigint not null, authors_id bigint not null, unique (authors_id)) ;
create table application_revenue_summary (sp_id varchar(255) not null, app_id varchar(255) not null, party_charge_service_type varchar(255) not null, direction varchar(255) not null, date_id integer not null, app_name VARCHAR(65), total_revenue BIGINT(11), total_trans_count INTEGER, primary key (sp_id, app_id, party_charge_service_type, direction, date_id)) ;
create table author (id bigint not null auto_increment, created_time datetime, last_modified_time datetime, version bigint, address varchar(40) not null, operator varchar(10) not null, primary key (id)) ;
create table author_validation_policy (id bigint not null auto_increment, next_policy varchar(255), next_policy_id bigint, primary key (id)) ;
create table billing_info (id bigint not null auto_increment, created_time datetime, last_modified_time datetime, version bigint, billing_address varchar(255), billing_name varchar(255), express_delivery bit, primary key (id)) ;
create table candidate_vote (candidate_id bigint, vote_id bigint not null, primary key (vote_id)) ;
create table competition (id bigint not null auto_increment, created_time datetime, last_modified_time datetime, version bigint, default_locale varchar(255) not null, primary key (id)) ;
create table competition_sub_category (competition_id bigint not null, supportedSubCategories_id bigint not null, idx integer not null, primary key (competition_id, idx), unique (supportedSubCategories_id)) ;
create table competition_supportedLocales (competition_id bigint not null, element varchar(255)) ;
create table content (id bigint not null auto_increment, created_time datetime, last_modified_time datetime, version bigint, sdp_message_id varchar(255), sent_time datetime, source varchar(255), localized_msg_id bigint not null, sub_category bigint, primary key (id), unique (localized_msg_id)) ;
create table developer (id bigint not null auto_increment, created_time datetime, last_modified_time datetime, version bigint, default_locale varchar(255) not null, mo_url tinyblob, ws_password varchar(255), primary key (id)) ;
create table developer_sub_category (developer_id bigint not null, supportedSubCategories_id bigint not null, idx integer not null, primary key (developer_id, idx), unique (supportedSubCategories_id)) ;
create table developer_supportedLocales (developer_id bigint not null, element varchar(255)) ;
create table keyword_aliases (sub_category_id bigint not null, element varchar(255)) ;
create table localized_message (id bigint not null auto_increment, created_time datetime, last_modified_time datetime, version bigint, locale varchar(255), message varchar(255), primary key (id)) ;
create table message_history (id integer not null auto_increment, message_timestamp BIGINT(14) not null, application_id VARCHAR(20) not null, application_name VARCHAR(50) not null, keyword VARCHAR(50), message VARCHAR(100), event VARCHAR(255) not null, primary key (id)) ;
create table n_messages_n_period_policy (id bigint not null auto_increment, next_policy varchar(255), next_policy_id bigint, number_of_allowed_response integer, restriction_valid_period integer, restriction_valid_period_unit varchar(7) not null, primary key (id)) ;
create table orca_sdp_app_integration (id bigint not null auto_increment, created_time datetime, last_modified_time datetime, version bigint, orca_app_id varchar(255), sdp_app_id varchar(255), sdp_app_password varchar(255), sp_id varchar(255), primary key (id)) ;
create table periodicity (id bigint not null auto_increment, created_time datetime, last_modified_time datetime, version bigint, allowed_buffer_time integer, default_dispatch_day_of_month integer, default_dispatch_day_of_week integer, default_dispatch_hour_of_day integer, default_dispatch_minute_of_hour integer, default_dispatch_last_day_of_month bit, first_scheduled_dispatch_time datetime not null, last_sent_time datetime, period_value integer, unit varchar(255), primary key (id)) ;
create table question (id bigint not null auto_increment, endDate bigint not null, question varchar(255), questionId varchar(255), startDate bigint not null, status varchar(255), primary key (id)) ;
create table question_answer (question_id bigint not null, answers_id bigint not null, unique (answers_id)) ;
create table request_service (id bigint not null auto_increment, created_time datetime, last_modified_time datetime, version bigint, default_locale varchar(255) not null, common_response_available bit not null, primary key (id)) ;
create table request_service_data (id bigint not null auto_increment, created_time datetime, last_modified_time datetime, version bigint, address varchar(40) not null, operator varchar(10) not null, received_message varchar(255), received_time bigint not null, service_id bigint not null, appId varchar(255), appName varchar(255), isRead bit not null, is_replied bit, messageId bigint not null, sub_category bigint, primary key (id)) ;
create table request_service_localized_message (request_service_id bigint not null, responseMessages_id bigint not null, unique (responseMessages_id)) ;
create table request_service_sub_category (request_service_id bigint not null, supportedSubCategories_id bigint not null, idx integer not null, primary key (request_service_id, idx), unique (supportedSubCategories_id)) ;
create table request_service_supportedLocales (request_service_id bigint not null, element varchar(255)) ;
create table sub_category (id bigint not null auto_increment, created_time datetime, last_modified_time datetime, version bigint, keyword varchar(255), primary key (id)) ;
create table sub_category_description (sub_category_id bigint not null, description_id bigint not null, unique (description_id)) ;
create table subscriber_validation_policy (id bigint not null auto_increment, next_policy varchar(255), data_message_policies_id bigint, primary key (id)) ;
create table subscription (id bigint not null auto_increment, created_time datetime, last_modified_time datetime, version bigint, default_locale varchar(255) not null, is_sms_update_support_required bit, default_periodicity bigint not null, primary key (id), unique (default_periodicity)) ;
create table subscription_content_release (id bigint not null auto_increment, created_time datetime, last_modified_time datetime, version bigint, message_id bigint, app_id varchar(255) not null, app_name varchar(255) not null, scheduled_dispatch_time_from datetime not null, scheduled_dispatch_time_to datetime not null, status varchar(255), periodicity_id bigint not null, primary key (id)) ;
create table subscription_content_release_content (subscription_content_release_id bigint not null, contents_id bigint not null, unique (contents_id)) ;
create table subscription_periodicity (subscription_id bigint not null, supportedPeriodicities_id bigint not null, unique (supportedPeriodicities_id)) ;
create table subscription_sub_category (subscription_id bigint not null, supportedSubCategories_id bigint not null, idx integer not null, primary key (subscription_id, idx), unique (supportedSubCategories_id)) ;
create table subscription_supportedLocales (subscription_id bigint not null, element varchar(255)) ;
create table vote (id bigint not null auto_increment, created_time datetime, last_modified_time datetime, version bigint, address varchar(40) not null, operator varchar(10) not null, received_message varchar(255), received_time bigint not null, service_id bigint not null, primary key (id)) ;
create table votelet_active_whole_day (id bigint not null auto_increment, created_time datetime, last_modified_time datetime, version bigint, primary key (id)) ;
create table votelet_response_spec_no_resp (id bigint not null auto_increment, created_time datetime, last_modified_time datetime, version bigint, primary key (id)) ;
create table votelet_response_spec_result_summary (id bigint not null auto_increment, created_time datetime, last_modified_time datetime, version bigint, primary key (id)) ;
create table voting_result_response_template (votelet_response_spec_result_summary_id bigint not null, responseTemplate_id bigint not null, unique (responseTemplate_id)) ;
create table voting_service (id bigint not null auto_increment, created_time datetime, last_modified_time datetime, version bigint, default_locale varchar(255) not null, is_one_vote_per_number bit, response_spec_type varchar(255), voting_response_id bigint, is_vote_result_public bit, primary key (id)) ;
create table voting_service_sub_category (voting_service_id bigint not null, supportedSubCategories_id bigint not null, idx integer not null, primary key (voting_service_id, idx), unique (supportedSubCategories_id)) ;
create table voting_service_supportedLocales (voting_service_id bigint not null, element varchar(255)) ;
alter table alert_content_release_content add index FK8F67BE98104B2DF5 (contents_id), add constraint FK8F67BE98104B2DF5 foreign key (contents_id) references content (id);
alter table alert_content_release_content add index FK8F67BE9881AB4CC8 (alert_content_release_id), add constraint FK8F67BE9881AB4CC8 foreign key (alert_content_release_id) references alert_content_release (id);
alter table alert_service_sub_category add index FK6A9D6C6A4B57F23F (alert_service_id), add constraint FK6A9D6C6A4B57F23F foreign key (alert_service_id) references alert_service (id);
alter table alert_service_sub_category add index FK6A9D6C6AE44BEB06 (supportedSubCategories_id), add constraint FK6A9D6C6AE44BEB06 foreign key (supportedSubCategories_id) references sub_category (id);
alter table alert_service_supportedLocales add index FKC2F0C6984B57F23F (alert_service_id), add constraint FKC2F0C6984B57F23F foreign key (alert_service_id) references alert_service (id);
alter table app_invalid_req_resp add index FKF4E8E277B2869EE (application_id), add constraint FKF4E8E277B2869EE foreign key (application_id) references application (id);
alter table app_invalid_req_resp add index FKF4E8E277D65F0A4D (localized_msg_id), add constraint FKF4E8E277D65F0A4D foreign key (localized_msg_id) references localized_message (id);
alter table app_subscription_resp add index FK51166314B2869EE (application_id), add constraint FK51166314B2869EE foreign key (application_id) references application (id);
alter table app_subscription_resp add index FK51166314D65F0A4D (localized_msg_id), add constraint FK51166314D65F0A4D foreign key (localized_msg_id) references localized_message (id);
alter table app_unsubscription_resp add index FK2C7A691BB2869EE (application_id), add constraint FK2C7A691BB2869EE foreign key (application_id) references application (id);
alter table app_unsubscription_resp add index FK2C7A691BD65F0A4D (localized_msg_id), add constraint FK2C7A691BD65F0A4D foreign key (localized_msg_id) references localized_message (id);
alter table application_author add index FKCB77F5AB2869EE (application_id), add constraint FKCB77F5AB2869EE foreign key (application_id) references application (id);
alter table application_author add index FKCB77F5A6AF5B961 (authors_id), add constraint FKCB77F5A6AF5B961 foreign key (authors_id) references author (id);
alter table candidate_vote add index FK536070262F9C8015 (vote_id), add constraint FK536070262F9C8015 foreign key (vote_id) references vote (id);
alter table candidate_vote add index FK536070268A45EBD1 (candidate_id), add constraint FK536070268A45EBD1 foreign key (candidate_id) references sub_category (id);
alter table competition_sub_category add index FK438F1E9DAE8CEF9F (competition_id), add constraint FK438F1E9DAE8CEF9F foreign key (competition_id) references competition (id);
alter table competition_sub_category add index FK438F1E9DE44BEB06 (supportedSubCategories_id), add constraint FK438F1E9DE44BEB06 foreign key (supportedSubCategories_id) references sub_category (id);
alter table competition_supportedLocales add index FK8A88274BAE8CEF9F (competition_id), add constraint FK8A88274BAE8CEF9F foreign key (competition_id) references competition (id);
alter table content add index FK38B73479D65F0A4D (localized_msg_id), add constraint FK38B73479D65F0A4D foreign key (localized_msg_id) references localized_message (id);
alter table content add index FK38B734796016DA97 (sub_category), add constraint FK38B734796016DA97 foreign key (sub_category) references sub_category (id);
alter table developer_sub_category add index FK9D37327228612E95 (developer_id), add constraint FK9D37327228612E95 foreign key (developer_id) references developer (id);
alter table developer_sub_category add index FK9D373272E44BEB06 (supportedSubCategories_id), add constraint FK9D373272E44BEB06 foreign key (supportedSubCategories_id) references sub_category (id);
alter table developer_supportedLocales add index FKE7A848A028612E95 (developer_id), add constraint FKE7A848A028612E95 foreign key (developer_id) references developer (id);
alter table keyword_aliases add index FK935E1AA86EEA0AF7 (sub_category_id), add constraint FK935E1AA86EEA0AF7 foreign key (sub_category_id) references sub_category (id);
alter table question add index FKBA823BE6A31FE91F (id), add constraint FKBA823BE6A31FE91F foreign key (id) references competition (id);
alter table question_answer add index FK561DF237FD3FA447 (answers_id), add constraint FK561DF237FD3FA447 foreign key (answers_id) references answer (id);
alter table question_answer add index FK561DF237AB683D5 (question_id), add constraint FK561DF237AB683D5 foreign key (question_id) references question (id);
alter table request_service_data add index FK38FC22A46016DA97 (sub_category), add constraint FK38FC22A46016DA97 foreign key (sub_category) references sub_category (id);
alter table request_service_localized_message add index FK1867B289DA91587D (responseMessages_id), add constraint FK1867B289DA91587D foreign key (responseMessages_id) references localized_message (id);
alter table request_service_localized_message add index FK1867B289F4774286 (request_service_id), add constraint FK1867B289F4774286 foreign key (request_service_id) references request_service (id);
alter table request_service_sub_category add index FK5227D117F4774286 (request_service_id), add constraint FK5227D117F4774286 foreign key (request_service_id) references request_service (id);
alter table request_service_sub_category add index FK5227D117E44BEB06 (supportedSubCategories_id), add constraint FK5227D117E44BEB06 foreign key (supportedSubCategories_id) references sub_category (id);
alter table request_service_supportedLocales add index FK602F0CC5F4774286 (request_service_id), add constraint FK602F0CC5F4774286 foreign key (request_service_id) references request_service (id);
alter table sub_category_description add index FK2C3DD6DAE84A2EAE (description_id), add constraint FK2C3DD6DAE84A2EAE foreign key (description_id) references localized_message (id);
alter table sub_category_description add index FK2C3DD6DA6EEA0AF7 (sub_category_id), add constraint FK2C3DD6DA6EEA0AF7 foreign key (sub_category_id) references sub_category (id);
alter table subscription add index FK1456591D3014263 (default_periodicity), add constraint FK1456591D3014263 foreign key (default_periodicity) references periodicity (id);
alter table subscription_content_release add index FKE9104F1F7CE497D5 (periodicity_id), add constraint FKE9104F1F7CE497D5 foreign key (periodicity_id) references periodicity (id);
alter table subscription_content_release_content add index FKB0E19B594E425DAB (subscription_content_release_id), add constraint FKB0E19B594E425DAB foreign key (subscription_content_release_id) references subscription_content_release (id);
alter table subscription_content_release_content add index FKB0E19B59104B2DF5 (contents_id), add constraint FKB0E19B59104B2DF5 foreign key (contents_id) references content (id);
alter table subscription_periodicity add index FK77A82C11F174BE5 (supportedPeriodicities_id), add constraint FK77A82C11F174BE5 foreign key (supportedPeriodicities_id) references periodicity (id);
alter table subscription_periodicity add index FK77A82C117C6BE8FF (subscription_id), add constraint FK77A82C117C6BE8FF foreign key (subscription_id) references subscription (id);
alter table subscription_sub_category add index FK6919BFFF7C6BE8FF (subscription_id), add constraint FK6919BFFF7C6BE8FF foreign key (subscription_id) references subscription (id);
alter table subscription_sub_category add index FK6919BFFFE44BEB06 (supportedSubCategories_id), add constraint FK6919BFFFE44BEB06 foreign key (supportedSubCategories_id) references sub_category (id);
alter table subscription_supportedLocales add index FKBD3F47AD7C6BE8FF (subscription_id), add constraint FKBD3F47AD7C6BE8FF foreign key (subscription_id) references subscription (id);
alter table voting_result_response_template add index FK2F6D672E40A80387 (votelet_response_spec_result_summary_id), add constraint FK2F6D672E40A80387 foreign key (votelet_response_spec_result_summary_id) references votelet_response_spec_result_summary (id);
alter table voting_result_response_template add index FK2F6D672E7808968F (responseTemplate_id), add constraint FK2F6D672E7808968F foreign key (responseTemplate_id) references localized_message (id);
alter table voting_service_sub_category add index FK3D5757F99E8C450 (voting_service_id), add constraint FK3D5757F99E8C450 foreign key (voting_service_id) references voting_service (id);
alter table voting_service_sub_category add index FK3D5757FE44BEB06 (supportedSubCategories_id), add constraint FK3D5757FE44BEB06 foreign key (supportedSubCategories_id) references sub_category (id);
alter table voting_service_supportedLocales add index FK80243D2D99E8C450 (voting_service_id), add constraint FK80243D2D99E8C450 foreign key (voting_service_id) references voting_service (id);



CREATE TABLE voting_result_summary (
  `votelet_id` bigint(20) NOT NULL,
  `candidate_id` bigint(20) NOT NULL,
  `total_vote_count` bigint(20) NOT NULL,
  `last_update_time` datetime NOT NULL,
  PRIMARY KEY (`candidate_id`)
) ENGINE=InnoDB;

CREATE TABLE invalid_request_summary (
  `app_id` varchar(20) NOT NULL ,
  `total_requests` bigint(20) NOT NULL,
  `last_update_time` datetime NOT NULL,
    PRIMARY KEY (`app_id`)
 ) ENGINE=InnoDB;

 CREATE TABLE message_dispatching_summary (
  `app_id` varchar(20) NOT NULL ,
  `status_code` varchar(20) NOT NULL,
  `total_count` bigint(20) NOT NULL,
  `last_update_time` datetime NOT NULL,
    PRIMARY KEY (`app_id`, `status_code`)
 ) ENGINE=InnoDB;

  CREATE TABLE mo_summary (
  `app_id` varchar(20) NOT NULL ,
  `total_count` bigint(20) NOT NULL,
  `last_update_time` datetime NOT NULL,
    PRIMARY KEY (`app_id`)
 ) ENGINE=InnoDB;