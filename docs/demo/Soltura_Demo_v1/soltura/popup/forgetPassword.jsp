<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!--
(C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
All Rights Reserved.

These materials are unpublished, proprietary, confidential source code of
hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
of hSenid Software International (Pvt) Limited.

hSenid Software International (Pvt) Limited retains all title to and intellectual
property rights in these materials.
-->

<script type="text/javascript">

    function submitForm() {
        document.forms[1].submit();

    }
</script>
<script type="text/javascript" src="javascripts/passwordFieldValidator.js"></script>
<div id="popup-terms" style="display:none;">
    <div id="body" style="width:auto;height:auto;">
        <div class="wrap">
            <div id="dashboard-widgets-wrap">
                <div id='dashboard-widgets' class='metabox-holder' style="width:420px">
                    <div id="dashboard_quick_press" class="postbox ">
                        <h3 class='hndle'><span><fmt:message key="forgot.password"/></span></h3>

                        <div class="inside" style="margin:5px; max-height:430px;overflow:auto;">


                            <div class="clear">&nbsp;</div>
                            <div style="text-align:center;">

                                <form:form commandName="signin" action="forgetPasswordPage.html" onsubmit="isEmptyForm()">

                                    <div class="input_row3">
                                        <label for="UserName"><fmt:message key='username'/></label>
                                    </div>
                                    <div class="input_row">
                                        <div class="error-req"  id="errorUsername"><form:errors path="username"/>
                                        </div>
                                        <div class="input-text-wrap">
                                            <form:input path="username" id="UserName"/>
                                        </div>
                                    </div>


                                    <div class="input_row3">
                                        <label for="Email"><fmt:message key='email'/></label>
                                     </div>
                                    <div class="input_row">
                                        <div class="error-req"  id="errorEmail"><form:errors path="email"/></div>
                                        <div class="input-text-wrap">
                                            <form:input path="email" id="Email"/>
                                        </div>
                                    </div>

                                    <div class="input_row5">
                                         <div id="emailhint" style="font-size:10px;float:left;"><fmt:message key='email.sent'/></div>
                                    </div>

                                    <div class="input_row4">
                                        <jsp:element name="input">
                                            <jsp:attribute name="type">submit</jsp:attribute>
                                            <jsp:attribute name="name">ok</jsp:attribute>
                                            <jsp:attribute name="id">ok</jsp:attribute>
                                            <jsp:attribute name="value"><fmt:message key='submit'/></jsp:attribute>
                                            <jsp:attribute name="class">button</jsp:attribute>
                                            <jsp:attribute name="style=">width:50px</jsp:attribute>
                                        </jsp:element>
                                        <%--<input type="submit" name="ok" id="ok" value="Submit" class="button" style="width:50px"/>--%>
                                    </div>
                                </form:form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clear"><fmt:message key='blank'/></div>
            </div>
        </div>
        <!-- wrap -->
        <div class="clear"></div>
    </div>
</div>
