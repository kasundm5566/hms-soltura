
<%--(C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
All Rights Reserved.

These materials are unpublished, proprietary, confidential source code of
hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
of hSenid Software International (Pvt) Limited.

hSenid Software International (Pvt) Limited retains all title to and intellectual
property rights in these materials.--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>



<script type="text/javascript">
    function submitForm() {
        document.forms[1].submit();
    }

</script>

<div id="popup-terms" style="display:none;">
    <div id="body" style="width:auto;height:auto;">
        <div class="wrap">
            <div id="dashboard-widgets-wrap">
                <div id='dashboard-widgets' class='metabox-holder' style="width:550px">
                    <div id="dashboard_quick_press" class="postbox ">
                        <h3 class='hndle'><span><fmt:message key="verify.popup.title"/> </span></h3>

                        <div class="inside" style="margin:20px; max-height:400px;overflow:auto;">
                        <div>
                           <br/>
                        <p><fmt:message key="verify.popup.heading"/><br><br>
                        </p>
                            <p>
                                 <p> <fmt:message key="verify.page.main.heading"/><fmt:message key="blank"/>
                                    <%--<c:out value="${registration.cp.msisdns[0].verificationCode}"/></p>--%>
                                <p> <%--<fmt:message key="verify.page.activate.message.part1"/><fmt:message key="blank"/><c:out value="${registration.cp.msisdns[0].verificationCode}"/>--%>
                                    <fmt:message key="verify.page.activate.message.part2"/><c:out value="${registration.shortCode}"/>
                                    <fmt:message key="verify.page.activate.message.part3"/>
                                </p>
                        </div>
                            <div class="clear">&nbsp;</div>
                            <div style="text-align:center;">

                                <form action="#" name="validateVerification">

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clear"><fmt:message key='blank'/></div>
            </div>
        </div>
        <!-- wrap -->
        <div class="clear"></div>
    </div>
</div>

<script type="text/javascript">
//    document.getElementById("popup-terms").style.display = "none";
</script>