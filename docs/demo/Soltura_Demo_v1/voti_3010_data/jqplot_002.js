(function(b) {
    b.jqplot.BarRenderer = function() {
        b.jqplot.LineRenderer.call(this)
    };
    b.jqplot.BarRenderer.prototype = new b.jqplot.LineRenderer();
    b.jqplot.BarRenderer.prototype.constructor = b.jqplot.BarRenderer;
    b.jqplot.BarRenderer.prototype.init = function(d) {
        this.barPadding = 8;
        this.barMargin = 10;
        this.barDirection = "vertical";
        this.barWidth = null;
        this.shadowOffset = 2;
        this.shadowDepth = 5;
        this.shadowAlph = 0.08;
        b.extend(true, this, d);
        this.fill = true;
        if (this.barDirection == "vertical") {
            this._primaryAxis = "_xaxis";
            this._stackAxis = "x"
        } else {
            this._primaryAxis = "_yaxis";
            this._stackAxis = "y"
        }
        var e = {lineJoin:"miter",lineCap:"round",fill:true,isarc:false,strokeStyle:this.color,fillStyle:this.color,closePath:this.fill};
        this.renderer.shapeRenderer.init(e);
        var c = {lineJoin:"miter",lineCap:"round",fill:true,isarc:false,angle:this.shadowAngle,offset:this.shadowOffset,alpha:this.shadowAlpha,depth:this.shadowDepth,closePath:this.fill};
        this.renderer.shadowRenderer.init(c)
    };
    function a(e, d, c) {
        if (this.rendererOptions.barDirection == "horizontal") {
            this._stackAxis = "x"
        }
    }

    b.jqplot.preSeriesInitHooks.push(a);
    b.jqplot.BarRenderer.prototype.calcSeriesNumbers = function() {
        var g = 0;
        var h = 0;
        var f = this[this._primaryAxis];
        var e,d,j;
        for (var c = 0; c < f._series.length; c++) {
            d = f._series[c];
            if (d === this) {
                j = c
            }
            if (d.renderer.constructor == b.jqplot.BarRenderer) {
                g += d.data.length;
                h += 1
            }
        }
        return[g,h,j]
    };
    b.jqplot.BarRenderer.prototype.setBarWidth = function() {
        var f;
        var c = 0;
        var d = 0;
        var h = this[this._primaryAxis];
        var m,g,k;
        var l = this.renderer.calcSeriesNumbers.call(this);
        c = l[0];
        d = l[1];
        var j = h.numberTicks;
        var e = (j - 1) / 2;
        if (h.name == "xaxis" || h.name == "x2axis") {
            if (this._stack) {
                this.barWidth = (h._offsets.max - h._offsets.min) / c * d - this.barMargin
            } else {
                this.barWidth = ((h._offsets.max - h._offsets.min) / e - this.barPadding * (d - 1) - this.barMargin * 2) / d
            }
        } else {
            if (this._stack) {
                this.barWidth = (h._offsets.min - h._offsets.max) / c * d - this.barMargin
            } else {
                this.barWidth = ((h._offsets.min - h._offsets.max) / e - this.barPadding * (d - 1) - this.barMargin * 2) / d
            }
        }
        return[c,d]
    };
    b.jqplot.BarRenderer.prototype.draw = function(p, u, e) {
        var r;
        var m = (e != undefined) ? e : {};
        var h = (m.shadow != undefined) ? m.shadow : this.shadow;
        var w = (m.showLine != undefined) ? m.showLine : this.showLine;
        var q = (m.fill != undefined) ? m.fill : this.fill;
        var d = this.xaxis;
        var s = this.yaxis;
        var l = this._xaxis.series_u2p;
        var t = this._yaxis.series_u2p;
        var o,n,k,j,g;
        if (this.barWidth == null) {
            this.renderer.setBarWidth.call(this)
        }
        var v = this.renderer.calcSeriesNumbers.call(this);
        k = v[0];
        j = v[1];
        g = v[2];
        if (this._stack) {
            this._barNudge = 0
        } else {
            this._barNudge = (-Math.abs(j / 2 - 0.5) + g) * (this.barWidth + this.barPadding)
        }
        if (w) {
            if (this.barDirection == "vertical") {
                for (var r = 0; r < u.length; r++) {
                    points = [];
                    var f = u[r][0] + this._barNudge;
                    var c;
                    if (this._stack && this._prevGridData.length) {
                        c = this._prevGridData[r][1]
                    } else {
                        c = p.canvas.height
                    }
                    points.push([f - this.barWidth / 2,c]);
                    points.push([f - this.barWidth / 2,u[r][1]]);
                    points.push([f + this.barWidth / 2,u[r][1]]);
                    points.push([f + this.barWidth / 2,c]);
                    if (h && !this._stack) {
                        this.renderer.shadowRenderer.draw(p, points, m)
                    }
                    this.renderer.shapeRenderer.draw(p, points, m)
                }
            } else {
                if (this.barDirection == "horizontal") {
                    for (var r = 0; r < u.length; r++) {
                        points = [];
                        var f = u[r][1] - this._barNudge;
                        var x;
                        if (this._stack && this._prevGridData.length) {
                            x = this._prevGridData[r][0]
                        } else {
                            x = 0
                        }
                        points.push([x,f + this.barWidth / 2]);
                        points.push([u[r][0],f + this.barWidth / 2]);
                        points.push([u[r][0],f - this.barWidth / 2]);
                        points.push([x,f - this.barWidth / 2]);
                        if (h && !this._stack) {
                            this.renderer.shadowRenderer.draw(p, points, m)
                        }
                        this.renderer.shapeRenderer.draw(p, points, m)
                    }
                }
            }
        }
    };
    b.jqplot.BarRenderer.prototype.drawShadow = function(p, u, e) {
        var r;
        var m = (e != undefined) ? e : {};
        var h = (m.shadow != undefined) ? m.shadow : this.shadow;
        var w = (m.showLine != undefined) ? m.showLine : this.showLine;
        var q = (m.fill != undefined) ? m.fill : this.fill;
        var d = this.xaxis;
        var s = this.yaxis;
        var l = this._xaxis.series_u2p;
        var t = this._yaxis.series_u2p;
        var o,n,k,j,g;
        if (this._stack && this.shadow) {
            if (this.barWidth == null) {
                this.renderer.setBarWidth.call(this)
            }
            var v = this.renderer.calcSeriesNumbers.call(this);
            k = v[0];
            j = v[1];
            g = v[2];
            if (this._stack) {
                this._barNudge = 0
            } else {
                this._barNudge = (-Math.abs(j / 2 - 0.5) + g) * (this.barWidth + this.barPadding)
            }
            if (w) {
                if (this.barDirection == "vertical") {
                    for (var r = 0; r < u.length; r++) {
                        points = [];
                        var f = u[r][0] + this._barNudge;
                        var c;
                        if (this._stack && this._prevGridData.length) {
                            c = this._prevGridData[r][1]
                        } else {
                            c = p.canvas.height
                        }
                        points.push([f - this.barWidth / 2,c]);
                        points.push([f - this.barWidth / 2,u[r][1]]);
                        points.push([f + this.barWidth / 2,u[r][1]]);
                        points.push([f + this.barWidth / 2,c]);
                        this.renderer.shadowRenderer.draw(p, points, m)
                    }
                } else {
                    if (this.barDirection == "horizontal") {
                        for (var r = 0; r < u.length; r++) {
                            points = [];
                            var f = u[r][1] - this._barNudge;
                            var x;
                            if (this._stack && this._prevGridData.length) {
                                x = this._prevGridData[r][0]
                            } else {
                                x = 0
                            }
                            points.push([x,f + this.barWidth / 2]);
                            points.push([u[r][0],f + this.barWidth / 2]);
                            points.push([u[r][0],f - this.barWidth / 2]);
                            points.push([x,f - this.barWidth / 2]);
                            this.renderer.shadowRenderer.draw(p, points, m)
                        }
                    }
                }
            }
        }
    }
})(jQuery);