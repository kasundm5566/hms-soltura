/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hsenidmobile.orca.integration.aventura;

import hsenidmobile.orca.core.applications.exception.ApplicationException;
import hsenidmobile.orca.core.flow.dispatch.aventuraapi.AventuraApiMtMessageDispatcher;
import hsenidmobile.orca.core.model.SdpAppLoginData;
import hsenidmobile.orca.core.model.message.AbstractMessageImpl;
import hsenidmobile.orca.core.repository.SdpApplicationRepository;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.Map;
import java.util.concurrent.*;


/*
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
public class AventuraApiMessageDispatcherImpl implements AventuraApiMtMessageDispatcher {

    private static final Logger logger = Logger.getLogger(AventuraApiMessageDispatcherImpl.class);
    private AventuraSmsSender aventuraSmsSender;
    private ExecutorService executorService;
    private int executorThreadCount = 60;
    private String defaultAppId;
    private String defaultPassword;

    private SdpApplicationRepository sdpApplicationRepository;

    public void init() {
        executorService = Executors.newFixedThreadPool(executorThreadCount);
    }

    @Override
    public Future dispatchIndividualMessages(String orcaAppId, List<? extends AbstractMessageImpl> messages) {
        return dispatchMessage(orcaAppId, messages, false);
    }

    @Override
    public Future dispatchBroadcastMessage(String orcaAppId, List<? extends AbstractMessageImpl> messages) {
        return dispatchMessage(orcaAppId, messages, true);
    }

    private Future dispatchMessage(String orcaAppId, List<? extends AbstractMessageImpl> messages, boolean isBroadcast) {
        Future<Map<String, Object>> future = null;
        if (messages != null && messages.size() > 0) {
            final SdpAppLoginData sdpApp;
            try {
                sdpApp = sdpApplicationRepository.getSdpAppLoginDataForOrcaApp(orcaAppId);
                logger.info("SDP app found sdpAppID[" + sdpApp.getAppId() + "] for orcaAppId[" + orcaAppId + "]");
            } catch (ApplicationException e) {
                logger.error("Error in Retriving SDP application for appId[" + orcaAppId + "]", e);
                throw new RuntimeException("Error in Retriving SDP application for appId[" + orcaAppId + "]", e);
            }
            //todo here the password has been hardcoded due to the urgency of teh release and this has to be taken from either
            // the SDP or need to store in orca DB
            future = executorService.submit(createTask(sdpApp.getAppId(), sdpApp.getPassword(), (List<AbstractMessageImpl>) messages,
                    isBroadcast));

        } else {
            logger.info("Message size is 0. Ignoring SMS MT request...");
        }
        return future;
    }

    public void dispatchMessage(List<AbstractMessageImpl> messages) {
        if (logger.isInfoEnabled()) {
            logger.info("Sending message using default appId[" + defaultAppId + "] password[" + defaultPassword + "]");
        }
        executorService.submit(createTask(defaultAppId, defaultPassword, messages, false));
    }

    private SmsMtMessageDispatcher createTask(String sdpAppId, String password, List<AbstractMessageImpl> messages,
                                              boolean isBroadcast) {
        final SmsMtMessageDispatcher smsMtMessageDispatcher = new SmsMtMessageDispatcher(aventuraSmsSender, sdpAppId,
                password, messages, isBroadcast);
        if (logger.isDebugEnabled()) {
            logger.debug("Creating new task to executor. task [" + smsMtMessageDispatcher + "]");
        }
        return smsMtMessageDispatcher;
    }

    public void setAventuraSmsSender(AventuraSmsSender aventuraSmsSender) {
        this.aventuraSmsSender = aventuraSmsSender;
    }

    public void setExecutorThreadCount(int executorThreadCount) {
        this.executorThreadCount = executorThreadCount;
    }

    public void setSdpApplicationRepository(SdpApplicationRepository sdpApplicationRepository) {
        this.sdpApplicationRepository = sdpApplicationRepository;
    }

    public void setDefaultAppId(String defaultAppId) {
        this.defaultAppId = defaultAppId;
    }

    public void setDefaultPassword(String defaultPassword) {
        this.defaultPassword = defaultPassword;
    }
}

class SmsMtMessageDispatcher implements Callable<Map<String, Object>> {
    private final static Logger logger = Logger.getLogger(SmsMtMessageDispatcher.class);
    private AventuraSmsSender aventuraSmsSender;
    private String appId;
    private String password;
    private List<AbstractMessageImpl> smsMessageList;
    private boolean isBroadcast;

    SmsMtMessageDispatcher(AventuraSmsSender aventuraSmsSender, String appId, String password,
                           List<AbstractMessageImpl> smsMessageList, boolean isBroadcast) {
        this.aventuraSmsSender = aventuraSmsSender;
        this.appId = appId;
        this.password = password;
        this.smsMessageList = smsMessageList;
        this.isBroadcast = isBroadcast;
    }

    @Override
    public Map<String, Object> call() {
        if (logger.isDebugEnabled()) {
            logger.debug("Executing SmsMtMessageDispatcher.");
        }
        if (smsMessageList.size() != 1){
            throw new IllegalStateException("multiple messages cannot be send ..");
        }
        if (isBroadcast) {
            logger.debug("Sending broadcast messages....................");
            for (AbstractMessageImpl smsMessage : smsMessageList) {
                return aventuraSmsSender.sendBroadcastMessage(smsMessage, appId, password);
            }
        } else {
            logger.debug("Sending individual messages....................");
            for (AbstractMessageImpl smsMessage : smsMessageList) {
                return aventuraSmsSender.sendSingleMessage(smsMessage, appId, password);
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return new StringBuilder().append("SmsMtMessageDispatcher{").append("aventuraSmsSender=")
                .append(aventuraSmsSender).append(", appId='").append(appId).append('\'').append(", password='")
                .append(password).append('\'').append(", smsMessageList=").append(smsMessageList).append('}')
                .toString();
    }
}
