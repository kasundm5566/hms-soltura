/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hsenidmobile.orca.integration.aventura;

import hms.common.rest.util.JsonBodyProvider;
import hms.commons.SnmpLogUtil;
import hsenidmobile.nettyclient.MessageSender;
import hsenidmobile.orca.core.model.Msisdn;
import hsenidmobile.orca.core.model.message.AbstractMessageImpl;
import org.apache.cxf.jaxrs.client.ClientWebApplicationException;
import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.log4j.Logger;
import org.apache.log4j.NDC;
import org.jboss.netty.handler.codec.http.HttpHeaders;
import org.jboss.netty.handler.codec.http.HttpRequest;

import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.net.URI;
import java.util.*;

import static hsenidmobile.orca.integration.util.kiteApiKeys.*;

//import hsenidmobile.sdp.rest.servletbase.MchoiceAventuraMessagingException;

/*
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */

public class AventuraSmsSender {

    private static final Logger logger = Logger.getLogger(AventuraSmsSender.class);
    private URI sdpCoreEndPoint;
    private String dispatchErrorDueExceptionErrorCode;
    private MessageSender messageSender;
    private int httpResponseWaitTime = 1000;
    private String sdpConnectionFailSnmpTrapMessage;
    private String sdpConnectionSuccessSnmpTrapMessage;

    /**
     * @param message
     * @param appId
     * @param password
     * @return
     *
     */
    public Map sendSingleMessage(AbstractMessageImpl message, String appId, String password) {
        try {
            String correlationId = String.valueOf(message.getCorrelationId());
            NDC.push(correlationId);
            if (logger.isDebugEnabled()) {
                logger.debug("Creating single message for telephone no[" + message.getKeyword() + "]");

            }
            String senderAddress = null;
            if(message.getSenderAddress() != null) {
                senderAddress = message.getSenderAddress().getAddress();
            }
            final Map<String, Object> httpParameters = createMessageParameters(message.getMessage(),
                    message.getKeyword(),
                    senderAddress,
                    appId,
                    password);
            return sendMessage(httpParameters);
        }
        finally {
            NDC.remove();
        }
    }

    /**
     * @param message
     * @param appId
     * @param password
     * @return
     * @throws      *
     */
    public Map sendBroadcastMessage(AbstractMessageImpl message, String appId, String password) {
        try {
            String correlationId = String.valueOf(message.getCorrelationId());
            NDC.push(correlationId);
            if (logger.isDebugEnabled()) {
                logger.debug("Creating broadcast message for broadcast list[" + message.getKeyword() + "]");
            }
            final Map httpParameters = createBcMessageParameters(message.getMessage(),
                    message.getSenderAddress().getAddress(),
                    appId, password);

            return sendMessage(httpParameters);
        } finally {
            NDC.remove();
        }
    }

    private Map  createMessageParameters(final String message, final String destinationAddressList,
                                         final String sourceAddress, final  String appId, final String password) {
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put(APPLICATION_ID, appId);
        parameters.put(APP_PASSWORD, password);
        parameters.put(MESSAGE_KEY, message);
        List<Object> addressList = new ArrayList<Object>();
        addressList.add(destinationAddressList);
        parameters.put(ADDRESS_KEY, addressList);
//        parameters.put(SOURCE_ADDRESS_KEY, sourceAddress);
        return parameters;
    }

    private Map createBcMessageParameters(String message, String senderAddress, String appId, String password) {
        Map params = new Hashtable<String, Object>();
        params.put(APPLICATION_ID, appId);
        params.put(APP_PASSWORD, password);
        params.put(MESSAGE_KEY, message);
//        params.put(SOURCE_ADDRESS_KEY, senderAddress);
        List<Object> address = new ArrayList<Object>();
        address.add(BC_ALL);
        params.put(ADDRESS_KEY,address);
        return params;
    }

    private Map sendMessage(Map requestMessage) {
        Map<String, Object> responseMap = new HashMap<String, Object>();
        String appId = null;
        try {
            Map response = (Map) makeRestCall(requestMessage, Map.class);
            if (response != null && response.get(MESSAGE_ID_KEY) != null){
                SnmpLogUtil.clearTrap("sol-sdp-sms-connection", sdpConnectionSuccessSnmpTrapMessage);
                Long messageId = Long.parseLong((String) response.get(MESSAGE_ID_KEY));
                if (((String) response.get(STATUS_CODE_KEY)).equals(SUCCESS__CODE) && messageId != null) {
                    appId = (String) requestMessage.get(APPLICATION_ID);
                    logger.info("Message Successfully processed for appId[" + appId + "]");
                    responseMap.put(MESSAGE_ID_KEY,messageId);
                    responseMap.put(STATUS_CODE_KEY, SUCCESS__CODE);
                    return responseMap;
                } else {
                    logger.error("Message Possessing Error appId[" + appId + "]");
                    responseMap.put(STATUS_CODE_KEY, "fail");
                    responseMap.put(STATUS_DETAILS_KEY, (String) response.get(STATUS_DETAILS_KEY));
                    return responseMap;
                }
            } else {
                logger.error("Error while sending message to SDP. AppId[" + appId + "]");
                SnmpLogUtil.trap("sol-sdp-sms-connection", sdpConnectionFailSnmpTrapMessage);
                responseMap.put(STATUS_CODE_KEY, "fail");
                responseMap.put(STATUS_DETAILS_KEY, RESPONCE_NULL_ERROR_MESSAGE);
                return responseMap;
            }
        } catch (ClientWebApplicationException e) {
            logger.error("Error while sending message", e);
            SnmpLogUtil.trap("sol-sdp-sms-connection", sdpConnectionFailSnmpTrapMessage);
            responseMap.put(STATUS_DETAILS_KEY, EXCEPTION_OCCURED_ERROR_MESSAGE);
            return responseMap;
        } catch (Exception e) {
            logger.error("Error while sending message to SDP. AppId[" + appId + "]", e);
            responseMap.put(STATUS_DETAILS_KEY, EXCEPTION_OCCURED_ERROR_MESSAGE);
            return responseMap;
        }
    }

    public Object makeRestCall(Map<String,? extends Object> parameters, Class aClass) {
        LinkedList<Object> providers = new LinkedList<Object>();
        providers.add(new JsonBodyProvider());
        WebClient regClient = WebClient.create(sdpCoreEndPoint.toString(), providers);
        regClient.header("Content-Type", MediaType.APPLICATION_JSON);
        regClient.accept(MediaType.APPLICATION_JSON);
        return regClient.invoke("POST", parameters, aClass);
    }

    /**
     * @param request
     * @param appId
     * @param password
     */
    private void addAuthentication(HttpRequest request, String appId, String password) {
        // request for authentication.
        final String usernamePassword = appId + ":" + password;
        final String basicAuthenticating = new sun.misc.BASE64Encoder().encode(usernamePassword.getBytes());
        request.setHeader(HttpHeaders.Names.AUTHORIZATION, "Basic " + basicAuthenticating);
    }

    private static String createPostBody(Map<String, String> postParameters) {
        StringBuffer params = new StringBuffer(200);
        for (Map.Entry<String, String> entry : postParameters.entrySet()) {
            params.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
        }

        String paramsStr = params.toString();
        if (paramsStr.endsWith("&")) {
            return paramsStr.substring(0, paramsStr.length() - 1);
        } else {
            return paramsStr;
        }
    }

    public void setDispatchErrorDueExceptionErrorCode(String dispatchErrorDueExceptionErrorCode) {
        this.dispatchErrorDueExceptionErrorCode = dispatchErrorDueExceptionErrorCode;
    }

    public void setSendFailureStatusCode(String sendFailureStatusCode) {
        sendFailureStatusCode = sendFailureStatusCode;
    }

    public void setSuccessStatusCode(String successStatusCode) {
        successStatusCode = successStatusCode;
    }

    public void setSdpCoreEndPoint(URI sdpCoreEndPoint) {
        this.sdpCoreEndPoint = sdpCoreEndPoint;
    }

    public void setMessageSender(MessageSender messageSender) {
        this.messageSender = messageSender;
    }

    public void setHttpResponseWaitTime(int httpResponseWaitTime) {
        this.httpResponseWaitTime = httpResponseWaitTime;
    }

    public void setSdpConnectionFailSnmpTrapMessage(String sdpConnectionFailSnmpTrapMessage) {
        this.sdpConnectionFailSnmpTrapMessage = sdpConnectionFailSnmpTrapMessage;
    }

    public void setSdpConnectionSuccessSnmpTrapMessage(String sdpConnectionSuccessSnmpTrapMessage) {
        this.sdpConnectionSuccessSnmpTrapMessage = sdpConnectionSuccessSnmpTrapMessage;
    }
}
