/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hsenidmobile.orca.integration.aventura.server;

import hsenidmobile.orca.core.flow.dispatch.aventuraapi.AventuraApiMtMessageDispatcher;
import hsenidmobile.orca.core.model.OrcaException;
import hsenidmobile.orca.core.model.message.AbstractMessageImpl;
import hsenidmobile.orca.core.model.message.Message;
import hsenidmobile.orca.core.model.message.MessageContext;
import hsenidmobile.orca.core.model.message.SmsMessage;
import hsenidmobile.orca.integration.handlers.IntegrationMoRequestHandler;
import hsenidmobile.orca.integration.transformers.Transformer;
import org.apache.log4j.Logger;
import org.apache.log4j.NDC;
import org.jboss.netty.channel.*;
import org.jboss.netty.handler.codec.http.*;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
/*
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */

public class AventuraSmsMoMessageReceivingServerHandler extends SimpleChannelHandler {


    private Transformer<HttpRequest, SmsMessage> aventuraSmsMoMessageTransformer;
    private IntegrationMoRequestHandler<SmsMessage> smsMoRequestHandler;
    private AventuraApiMtMessageDispatcher aventuraApiMessageDispatcher;
    private ExecutorService executorService = Executors.newCachedThreadPool();

    private static final Logger logger = Logger.getLogger(AventuraSmsMoMessageReceivingServerHandler.class);

    @Override
    public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) throws Exception {
        try {
            final MessageContext context = new MessageContext();
            context.setReceiveTime(System.currentTimeMillis());
            final HttpRequest request;
            request = (HttpRequest) e.getMessage();
            try {
                if (executorService != null) {
                    executorService.execute(new Runnable() {
                        public void run() {
                            doHandle(context, request);
                        }
                    });
                } else {
                    doHandle(context, request);
                }
                writeResponse(request, e, HttpResponseStatus.OK);
            } catch (Throwable th) {
                logger.error("Error while processing received SmsMo.", th);
                writeResponse(request, e, HttpResponseStatus.INTERNAL_SERVER_ERROR);
            }
        } finally {
            NDC.remove();
        }
    }

    private void doHandle(MessageContext context, HttpRequest request) {
        SmsMessage message = aventuraSmsMoMessageTransformer.transform(request);
        NDC.push(String.valueOf(message.getCorrelationId()));
        try {
            logger.info("[T1] Sms Mo message received[" + message + "]");
            smsMoRequestHandler.processRequest(message, context);
        } catch (OrcaException ex) {
            logger.error("Error while processing received SmsMo.", ex);
            aventuraApiMessageDispatcher.dispatchMessage(createResponseMessage(message, ex.getMessage()));
        }
    }

    /**
     * Create MoResponse Sms message from received message
     *
     * @param receivedMessage
     * @param responseMsgTxt
     * @return
     */
    private List<AbstractMessageImpl> createResponseMessage(Message receivedMessage, String responseMsgTxt) {
        SmsMessage smsResp = new SmsMessage();
        smsResp.setCorrelationId(receivedMessage.getCorrelationId());
        smsResp.setMessage(responseMsgTxt);
        smsResp.setSenderAddress(receivedMessage.getReceiverAddresses().get(0));
        smsResp.addReceiverAddress(receivedMessage.getSenderAddress());
        final ArrayList<AbstractMessageImpl> arrayList = new ArrayList<AbstractMessageImpl>();
        arrayList.add(smsResp);
        return arrayList;
    }

    private void writeResponse(HttpRequest request, MessageEvent e, HttpResponseStatus httpResponseStatus) {

        // Decide whether to close the connection or not.
        boolean close = HttpHeaders.Values.CLOSE.equalsIgnoreCase(request.getHeader(HttpHeaders.Names.CONNECTION))
                || request.getProtocolVersion().equals(HttpVersion.HTTP_1_1)
                && !HttpHeaders.Values.KEEP_ALIVE.equalsIgnoreCase(request.getHeader(HttpHeaders.Names.CONNECTION));

        // Build the response object.
        HttpResponse response = new DefaultHttpResponse(HttpVersion.HTTP_1_1, httpResponseStatus);
        response.setHeader(HttpHeaders.Names.CONTENT_TYPE, "text/plain; charset=UTF-8");
        response.setHeader(HttpHeaders.Names.CONTENT_LENGTH, "0");

        // Write the response.
        ChannelFuture future = e.getChannel().write(response);

        // Close the connection after the write operation is done if necessary.
        if (close) {
            future.addListener(ChannelFutureListener.CLOSE);
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e) throws Exception {
        logger.error("Exception: ", e.getCause());
    }

    public Transformer<HttpRequest, SmsMessage> getAventuraSmsMoMessageTransformer() {
        return aventuraSmsMoMessageTransformer;
    }

    public void setAventuraSmsMoMessageTransformer(Transformer<HttpRequest, SmsMessage> aventuraSmsMoMessageTransformer) {
        this.aventuraSmsMoMessageTransformer = aventuraSmsMoMessageTransformer;
    }

    public IntegrationMoRequestHandler<SmsMessage> getSmsMoRequestHandler() {
        return smsMoRequestHandler;
    }

    public void setSmsMoRequestHandler(IntegrationMoRequestHandler<SmsMessage> smsMoRequestHandler) {
        this.smsMoRequestHandler = smsMoRequestHandler;
    }

    public AventuraApiMtMessageDispatcher getAventuraApiMessageDispatcher() {
        return aventuraApiMessageDispatcher;
    }

    public void setAventuraApiMessageDispatcher(AventuraApiMtMessageDispatcher aventuraApiMessageDispatcher) {
        this.aventuraApiMessageDispatcher = aventuraApiMessageDispatcher;
    }

    public void setExecutorService(ExecutorService executorService) {
        this.executorService = executorService;
    }
}
