/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hsenidmobile.orca.integration.aventura.server;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import hsenidmobile.orca.core.model.Msisdn;
import hsenidmobile.orca.core.model.message.SmsMessage;
import hsenidmobile.orca.integration.transformers.Transformer;

import org.apache.cxf.jaxrs.provider.json.JSONProvider;
import org.apache.log4j.Logger;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.handler.codec.http.HttpRequest;
import org.jboss.netty.handler.codec.http.QueryStringDecoder;

import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

import static hsenidmobile.orca.integration.util.kiteApiKeys.*;
/*
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */

public class AventuraSmsMoMessageTransformer implements Transformer<HttpRequest, SmsMessage> {
    private static final Logger logger = Logger.getLogger(AventuraSmsMoMessageTransformer.class);

    /**
     * Transform given message of one type(Source) to some other
     * type(Transformed).
     *
     * @param sourceMessage
     * @return
     */
/*    public SmsMessage transform(HttpRequest sourceMessage) {
        try {
            return processRequest(sourceMessage);
        } catch (Exception e) {
            logger.error("Error Transforming Aventura API request to SmsMessage ", e);
            throw new RuntimeException("Error Transforming Aventura API request to SmsMessage ", e);
        }
    }

    public SmsMessage processRequest(HttpRequest request) throws Exception {
        final Map<String, List<String>> valueList = createRequestParameters(buildRequestString(request));
        List<Msisdn> receiverAddresses  = new ArrayList<Msisdn>();

        final SmsMessage message = new SmsMessage();
        message.setMessage(valueList.get(MESSAGE_KEY).get(0));
        message.addReceiverAddress(new Msisdn(request.getHeader(RECIPIENT_ADDRESS), null));
        message.setSenderAddress(new Msisdn(valueList.get(SENDER_KEY).get(0), "ANY"));
        receiverAddresses.add(new Msisdn(valueList.get(RECEVER_KEY).get(0), "ANY" ));
        message.setReceiverAddresses(receiverAddresses);
        String correlationId = valueList.get(MESSAGE_KEY).get(0);
        message.setCorrelationId(Long.parseLong(correlationId));
        message.setAppId(valueList.get(APPLICATION_ID).get(0));
        return message;
    }*/

    public SmsMessage transform(HttpRequest sourceMessage) {
        try {
            return processRequest(sourceMessage);
        } catch (Exception e) {
            logger.error("Error Transforming Aventura API request to SmsMessage ", e);
            throw new RuntimeException("Error Transforming Aventura API request to SmsMessage ", e);
        }
    }

    public SmsMessage processRequest(HttpRequest request) throws Exception {

        String jsonString = buildRequestString(request);
        JsonObject jsonObject = (new JsonParser().parse(jsonString)).getAsJsonObject();
        final SmsMessage message = new SmsMessage();
        message.setMessage(jsonObject.get(MESSAGE_KEY).getAsString());
        message.setSenderAddress(new Msisdn(jsonObject.get(SOURCE_ADDRESS_KEY).getAsString(), "ANY"));
        message.setCorrelationId(jsonObject.get(MESSAGE_ID_KEY).getAsLong());
        message.setAppId(jsonObject.get(APPLICATION_ID).getAsString());
        return message;
    }

    public Map<String, List<String>> createRequestParameters(String requestedString) throws Exception {
        if (requestedString.length() > 0) {
            QueryStringDecoder queryStringDecoder = new QueryStringDecoder("/?" + requestedString);
            return queryStringDecoder.getParameters();
        } else {
            throw new StringIndexOutOfBoundsException("Sms mt request body is empty.");
        }
    }

    public String buildRequestString(HttpRequest httpRequest) throws UnsupportedEncodingException {
        ChannelBuffer content = httpRequest.getContent();
        final byte[] requestBytes = new byte[content.capacity()];
        content.readBytes(requestBytes);
        return new String(requestBytes, "UTF-8");
    }

    public Object readJsonResponse(InputStream is, Class aClass) {
       JSONProvider jsonProvider = new JSONProvider();
       try {
           return jsonProvider.readFrom(aClass, null, null, MediaType.APPLICATION_JSON_TYPE, null, is);
       } catch (IOException e) {
           e.printStackTrace();
           return null;
       }
    }
}
