/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.integration.boot;

import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.tanukisoftware.wrapper.WrapperListener;
import org.tanukisoftware.wrapper.WrapperManager;

/**
 * Start the Orca System.
 *
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class OrcaSystemStarter implements WrapperListener {

    private Logger logger = Logger.getLogger(OrcaSystemStarter.class);
    private Logger snmpLogger = Logger.getLogger("snmp_logger");
    private AbstractApplicationContext applicationContext;

    /**
     * @param args
     */
    public static void main(String[] args) throws IOException {
        WrapperManager.start(new OrcaSystemStarter(), args);
    }

    @Override
    public void controlEvent(int event) {
        if ((event == WrapperManager.WRAPPER_CTRL_LOGOFF_EVENT)
                && (WrapperManager.isLaunchedAsService() || WrapperManager.isIgnoreUserLogoffs())) {
            // Ignore
        } else {
            WrapperManager.stop(0);
            // Will not get here.
        }
    }

    @Override
    public Integer start(String[] arg0) {
        applicationContext = new ClassPathXmlApplicationContext("orca-spring-context.xml");
        applicationContext.registerShutdownHook();
        applicationContext.start();
        final Properties snmpTrapProperties = (Properties) applicationContext.getBean("snmpTrapProperties");
        snmpLogger.info(snmpTrapProperties.getProperty("snmp.trap.system.startup"));
        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
            public void run() {
                snmpLogger.info(snmpTrapProperties.getProperty("snmp.trap.system.shutdown"));
            }
        }));
        logger.info("============== SYSTEM STARTED ===========");
        return null;
    }

    @Override
    public int stop(int exitCode) {
        applicationContext.stop();
        logger.info("============== SYSTEM STOPPED ===========");
        return exitCode;
    }

}
