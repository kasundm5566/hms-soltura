/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$ 
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.integration.handlers;

import hsenidmobile.orca.core.MoRequestListener;
import hsenidmobile.orca.core.model.OrcaException;
import hsenidmobile.orca.core.model.message.MessageContext;
import hsenidmobile.orca.core.model.message.SmsMessage;
import org.apache.log4j.Logger;

/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class IntegrationSmsMoRequestHandler implements IntegrationMoRequestHandler<SmsMessage> {

	private Logger logger = Logger.getLogger(IntegrationSmsMoRequestHandler.class);
	private MoRequestListener moRequestListener;

	public boolean processRequest(SmsMessage message, MessageContext context) throws OrcaException {
		if (logger.isDebugEnabled()) {
			logger.debug("Sending SmsMo[" + message + "] to the Core.");
		}

        moRequestListener.onMessage(message, context);
        return true;
	}

	public void setMoRequestListener(MoRequestListener moRequestListener) {
		this.moRequestListener = moRequestListener;
	}
	
}
