/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.integration.http.client;

import java.net.URI;

import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.channel.Channels;
import org.jboss.netty.handler.codec.http.HttpRequestEncoder;
import org.jboss.netty.handler.codec.http.HttpResponseDecoder;

import hsenidmobile.nettyclient.channelpool.ChannelEventListener;
import hsenidmobile.nettyclient.channelpool.PooledChannelHandler;

/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class HttpClientPipelineFactory implements ChannelPipelineFactory {
	private ChannelEventListener channelEventListener;
	private URI attachedHost;


	public HttpClientPipelineFactory(ChannelEventListener channelEventListener, URI attachedHost) {
		super();
		this.channelEventListener = channelEventListener;
		this.attachedHost = attachedHost;

	}


	@Override
	public ChannelPipeline getPipeline() throws Exception {
		ChannelPipeline channelPipeline = Channels.pipeline();
		channelPipeline.addLast("decoder", new HttpResponseDecoder());
		channelPipeline.addLast("encoder", new HttpRequestEncoder());
		channelPipeline.addLast("handler", new PooledChannelHandler(channelEventListener, attachedHost, new HttpResponseProcessor()));
		return channelPipeline;
	}

}

