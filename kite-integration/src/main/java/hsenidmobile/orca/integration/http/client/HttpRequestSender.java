/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.integration.http.client;

import hsenidmobile.nettyclient.MessageResponseFuture;
import hsenidmobile.nettyclient.MessageSender;
import hsenidmobile.orca.core.model.message.SmsMessage;
import hsenidmobile.orca.integration.services.MtRequestSender;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.handler.codec.http.DefaultHttpRequest;
import org.jboss.netty.handler.codec.http.HttpHeaders;
import org.jboss.netty.handler.codec.http.HttpMethod;
import org.jboss.netty.handler.codec.http.HttpRequest;
import org.jboss.netty.handler.codec.http.HttpVersion;

/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class HttpRequestSender implements MtRequestSender<SmsMessage>{

	private static final Logger logger = Logger.getLogger(HttpRequestSender.class);
	private String dispatchErrorDueExceptionErrorCode;
	private MessageSender messageSender;
	private URI destination;
	private String sendFailureStatusCode = "SMS-MT-7001";
	private String successStatusCode = "SMS-MT-2000";

	private static ChannelBuffer setPostParam(SmsMessage smsMessage) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("MSG", smsMessage.getMessage());
		params.put("RSV_ADDR", smsMessage.getReceiverAddresses().get(0).getAddress());
		params.put("SND_ADDR", smsMessage.getSenderAddress().getAddress());

		ChannelBuffer buffer = ChannelBuffers.copiedBuffer(createPostBody(params).getBytes());
		return buffer;
	}

	private static String createPostBody(Map<String, String> postParameters) {
		Set<Entry<String, String>> entrySet = postParameters.entrySet();
		StringBuffer params = new StringBuffer(200);
		for (Entry<String, String> entry : entrySet) {
			params.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
		}

		String paramsStr = params.toString();
		if (paramsStr.endsWith("&")) {
			return paramsStr.substring(0, paramsStr.length() - 1);
		} else {
			return paramsStr;
		}
	}

	public void setMessageSender(MessageSender messageSender) {
		this.messageSender = messageSender;
	}

	public void setDestination(String destination) {
		try {
			this.destination = new URI(destination);
		} catch (URISyntaxException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	public String sendMt(SmsMessage message, String orcaAppId) {
		HttpRequest request = new DefaultHttpRequest(HttpVersion.HTTP_1_1, HttpMethod.POST, destination.toASCIIString());
		request.setHeader(HttpHeaders.Names.HOST, destination.getHost());
		request.setHeader(HttpHeaders.Names.CONNECTION, HttpHeaders.Values.KEEP_ALIVE);

		// request.
		request.setHeader(HttpHeaders.Names.CONTENT_TYPE, "application/x-www-form-urlencoded");
		request.setContent(setPostParam(message));
		request.setHeader(HttpHeaders.Names.CONTENT_LENGTH, String.valueOf(request.getContent().capacity()));

		try {
			MessageResponseFuture responseFuture = messageSender.sendMessage(destination, request);
			responseFuture.waitTillFinish(50);
			if(responseFuture.isSuccess()) {
				return successStatusCode;
			} else {
				return sendFailureStatusCode;
			}
		} catch (Throwable e) {
			logger.error("Error while sending message to SDP. AppId[" + orcaAppId + "], Message[" + message + "]", e);
			return dispatchErrorDueExceptionErrorCode;
		}

	}

	public void setDispatchErrorDueExceptionErrorCode(String dispatchErrorDueExceptionErrorCode) {
		this.dispatchErrorDueExceptionErrorCode = dispatchErrorDueExceptionErrorCode;
	}

	public void setSendFailureStatusCode(String sendFailureStatusCode) {
		this.sendFailureStatusCode = sendFailureStatusCode;
	}

	public void setSuccessStatusCode(String successStatusCode) {
		this.successStatusCode = successStatusCode;
	}

}
