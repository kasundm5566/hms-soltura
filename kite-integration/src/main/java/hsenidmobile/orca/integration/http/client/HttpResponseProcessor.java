/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.integration.http.client;

import java.util.Set;

import hsenidmobile.orca.core.flow.dispatch.aventuraapi.response.OneApiResponse;
import hsenidmobile.orca.core.flow.dispatch.aventuraapi.transformer.Transformer;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.handler.codec.http.HttpResponse;

import hsenidmobile.nettyclient.channelpool.ResponseProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class HttpResponseProcessor implements ResponseProcessor {
    private static final Logger logger = LoggerFactory.getLogger(HttpResponseProcessor.class);
    @Override
    public Object processReceivedMessage(ChannelHandlerContext ctx, MessageEvent e) {
        HttpResponse response = (HttpResponse) e.getMessage();
        Set<String> headerNames = response.getHeaderNames();
        for (String string : headerNames) {
            logger.debug(string+":");
        }
        OneApiResponse oneApiResponse = null;
        ChannelBuffer content = response.getContent();
        if (content.readable()) {
            oneApiResponse = (OneApiResponse)Transformer.toDomainObject(content.toString("UTF-8"), OneApiResponse.class);
        }
        String correlationId = response.getHeader("CorelationId");
        if(correlationId == null || correlationId.trim().length() ==0){
            oneApiResponse.setCorrelator(String.valueOf(new Long(0)));
        } else {
            oneApiResponse.setCorrelator(String.valueOf(correlationId));
        }
        return oneApiResponse;
    }
}
