/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.integration.http.server;

import hsenidmobile.orca.core.model.message.MessageContext;
import hsenidmobile.orca.core.util.CorrelationIdGenerationService;
import hsenidmobile.orca.core.util.PropertyHolder;
import hsenidmobile.orca.integration.http.server.handler.RequestHandler;
import hsenidmobile.orca.integration.http.server.transformers.HttpRequestTransformer;

import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.log4j.NDC;
import org.jboss.netty.channel.ChannelFuture;
import org.jboss.netty.channel.ChannelFutureListener;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ChannelPipelineCoverage;
import org.jboss.netty.channel.ExceptionEvent;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelHandler;
import org.jboss.netty.handler.codec.http.DefaultHttpResponse;
import org.jboss.netty.handler.codec.http.HttpHeaders;
import org.jboss.netty.handler.codec.http.HttpRequest;
import org.jboss.netty.handler.codec.http.HttpResponse;
import org.jboss.netty.handler.codec.http.HttpResponseStatus;
import org.jboss.netty.handler.codec.http.HttpVersion;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * @version $LastChangedRevision$
 */
@ChannelPipelineCoverage(value = "one")
public class HttpMessageReceivingServerHandler extends SimpleChannelHandler {

	public enum OperationMode {
		GET, POST
	}

	private static final Logger logger = Logger.getLogger(HttpMessageReceivingServerHandler.class);
	private OperationMode operationMode = OperationMode.GET;
	private Map<String, RequestTransformerHandlerPair> httpRequestTransformerHandlerPairMap;

	@Override
	public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) throws Exception {
		if (logger.isDebugEnabled()) {
			logger.debug("Http Request Received....");
		}
		String localAddress = ctx.getChannel().getLocalAddress().toString();
		try {
			long correlationId = CorrelationIdGenerationService.getCorrelationId(PropertyHolder.SERVER_ID);
			NDC.push(String.valueOf(correlationId));

			MessageContext context = new MessageContext();
			context.setReceiveTime(System.currentTimeMillis());

			HttpRequest request = (HttpRequest) e.getMessage();
			RequestTransformerHandlerPair transformerHandlerPair = httpRequestTransformerHandlerPairMap.get(request
					.getUri());
			if (transformerHandlerPair == null) {
				logger.warn("No RequestTransformerHandlerPair found to decode requests on [" + request.getUri() + "]");
				writeResponse(request, e, HttpResponseStatus.NOT_FOUND);
			} else {

				Object transformed = transformRequest(request, transformerHandlerPair, correlationId);

				RequestHandler requestHandler = transformerHandlerPair.getRequestHandler();
				requestHandler.processRequest(transformed, context);

				writeResponse(request, e, HttpResponseStatus.OK);
			}

		} finally {
			NDC.remove();
		}
	}

	private Object transformRequest(HttpRequest request, RequestTransformerHandlerPair transformerHandlerPair, long correlationId) {
		HttpRequestTransformer transformer = transformerHandlerPair.getHttpRequestTransformer();
		Object transformed;
		if (this.operationMode == OperationMode.GET) {
			transformed = transformer.transformGet(request, correlationId);
		} else {
			transformed = transformer.transformPost(request, correlationId);
		}
		return transformed;
	}

	private void writeResponse(HttpRequest request, MessageEvent e, HttpResponseStatus httpResponseStatus) {

		// Decide whether to close the connection or not.
		boolean close = HttpHeaders.Values.CLOSE.equalsIgnoreCase(request.getHeader(HttpHeaders.Names.CONNECTION))
				|| request.getProtocolVersion().equals(HttpVersion.HTTP_1_0)
				&& !HttpHeaders.Values.KEEP_ALIVE.equalsIgnoreCase(request.getHeader(HttpHeaders.Names.CONNECTION));

		// Build the response object.
		HttpResponse response = new DefaultHttpResponse(HttpVersion.HTTP_1_1, httpResponseStatus);
		response.setHeader(HttpHeaders.Names.CONTENT_TYPE, "text/plain; charset=UTF-8");
		response.setHeader(HttpHeaders.Names.CONTENT_LENGTH, "0");

		// Write the response.
		ChannelFuture future = e.getChannel().write(response);

		// Close the connection after the write operation is done if necessary.
		if (close) {
			future.addListener(ChannelFutureListener.CLOSE);
		}
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e) throws Exception {
		logger.error("Exception: ", e.getCause());
	}

	public void setOperationMode(OperationMode operationMode) {
		this.operationMode = operationMode;
	}

	public void setHttpRequestTransformerHandlerPairMap(
			Map<String, RequestTransformerHandlerPair> httpRequestTransformerHandlerPairMap) {
		this.httpRequestTransformerHandlerPairMap = httpRequestTransformerHandlerPairMap;
	}

}
