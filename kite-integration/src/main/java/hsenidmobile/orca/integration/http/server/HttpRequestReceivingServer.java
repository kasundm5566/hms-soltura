/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.integration.http.server;

/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */

import java.net.InetSocketAddress;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;
import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.channel.ChannelFactory;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextStartedEvent;

/**
 * @author sandarenu
 *
 */
public class HttpRequestReceivingServer implements ApplicationListener {


	private static final Logger logger = Logger.getLogger(HttpRequestReceivingServer.class);

	private int responseReceivingPort;
	private ChannelPipelineFactory messageReceivingServerPipelineFactory;

	private void startServer() {
		if (logger.isInfoEnabled()) {
			logger.info("Starting ResponseReceivingServer on port [" + responseReceivingPort + "]");
		}
		ChannelFactory channelFactory = new NioServerSocketChannelFactory(Executors.newCachedThreadPool(),
				Executors.newCachedThreadPool());

		ServerBootstrap serverBootstrap = new ServerBootstrap(channelFactory);

		serverBootstrap.setPipelineFactory(messageReceivingServerPipelineFactory);

		serverBootstrap.setOption("child.tcpNoDelay", true);
		serverBootstrap.setOption("child.keepAlive", true);

		serverBootstrap.bind(new InetSocketAddress(responseReceivingPort));
		if (logger.isInfoEnabled()) {
			logger.info("Started ResponseReceivingServer on port [" + responseReceivingPort + "]");
		}
	}

	public void setResponseReceivingPort(int responseReceivingPort) {
		this.responseReceivingPort = responseReceivingPort;
	}

	public void setMessageReceivingServerPipelineFactory(
			ChannelPipelineFactory messageReceivingServerPipelineFactory) {
		this.messageReceivingServerPipelineFactory = messageReceivingServerPipelineFactory;
	}


	@Override
	public void onApplicationEvent(ApplicationEvent springEvent) {
		if(springEvent instanceof ContextStartedEvent){
			if (logger.isDebugEnabled()) {
				logger.debug("Loading_Spring_Context_Finised event received.");
			}
			startServer();
		}
	}

}