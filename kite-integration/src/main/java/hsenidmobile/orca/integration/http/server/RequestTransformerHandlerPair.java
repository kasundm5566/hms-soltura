/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.integration.http.server;

import hsenidmobile.orca.integration.http.server.handler.RequestHandler;
import hsenidmobile.orca.integration.http.server.transformers.HttpRequestTransformer;

/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class RequestTransformerHandlerPair<T> {
	private HttpRequestTransformer<T> httpRequestTransformer;
	private RequestHandler<T> requestHandler;

	public HttpRequestTransformer<T> getHttpRequestTransformer() {
		return httpRequestTransformer;
	}

	public void setHttpRequestTransformer(HttpRequestTransformer<T> httpRequestTransformer) {
		this.httpRequestTransformer = httpRequestTransformer;
	}

	public RequestHandler<T> getRequestHandler() {
		return requestHandler;
	}

	public void setRequestHandler(RequestHandler<T> requestHandler) {
		this.requestHandler = requestHandler;
	}

}
