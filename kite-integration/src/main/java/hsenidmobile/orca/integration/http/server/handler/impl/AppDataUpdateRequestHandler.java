/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.integration.http.server.handler.impl;

import org.apache.log4j.Logger;

import hsenidmobile.orca.core.HttpUpdateRequestListener;
import hsenidmobile.orca.core.model.OrcaException;
import hsenidmobile.orca.core.model.message.AppDataUpdateRequest;
import hsenidmobile.orca.core.model.message.MessageContext;
import hsenidmobile.orca.integration.http.server.handler.RequestHandler;

/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class AppDataUpdateRequestHandler implements RequestHandler<AppDataUpdateRequest> {

	private static final Logger logger = Logger.getLogger(AppDataUpdateRequestHandler.class);
	private HttpUpdateRequestListener httpUpdateRequestListener;

	@Override
	public boolean processRequest(AppDataUpdateRequest message, MessageContext context) throws OrcaException
    {
		if (logger.isDebugEnabled()) {
			logger.debug("Sending AppDataUpdateRequest to Core[" + message + "]");
		}
        httpUpdateRequestListener.onMessage(message, context);
        return true;
	}

	public void setHttpUpdateRequestListener(HttpUpdateRequestListener httpUpdateRequestListener) {
		this.httpUpdateRequestListener = httpUpdateRequestListener;
	}

}
