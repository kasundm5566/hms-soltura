package hsenidmobile.orca.integration.http.server.handler.impl;

import hsenidmobile.orca.core.flow.dispatch.lb.RoundRobinLb;
import hsenidmobile.orca.core.model.OrcaException;
import hsenidmobile.orca.core.model.message.MessageContext;
import hsenidmobile.orca.core.model.message.MtRequest;
import hsenidmobile.orca.core.model.message.SmsMessage;
import hsenidmobile.orca.integration.handlers.IntegrationMoRequestHandler;
import hsenidmobile.orca.integration.http.server.handler.RequestHandler;

import org.apache.log4j.Logger;

public class GsmModemRequestHander implements RequestHandler<SmsMessage> {

	private static final Logger logger = Logger.getLogger(GsmModemRequestHander.class);
	private RoundRobinLb<MtRequest> messageCache;
	private IntegrationMoRequestHandler<SmsMessage> moRequestHandler;

	@Override
	public boolean processRequest(SmsMessage message, MessageContext context) throws OrcaException {
		try {
			logger.info("[T1] Sms Mo message received[" + message + "]");
			moRequestHandler.processRequest(message, context);
			return true;
		} catch (Exception e1) {
			logger.error("Exception while processign received message", e1);
			messageCache.putMessage("", createErrorResponse(message, e1.getMessage()));
			return false;
		}
	}

	private MtRequest createErrorResponse(SmsMessage receivedMessage, String responseMsgTxt){
		SmsMessage smsResp = new SmsMessage();
		smsResp.setMessage(responseMsgTxt);
		smsResp.setSenderAddress(receivedMessage.getReceiverAddresses().get(0));
		smsResp.addReceiverAddress(receivedMessage.getSenderAddress());
		return new MtRequest(smsResp, null);
	}

	public void setMessageCache(RoundRobinLb<MtRequest> messageCache) {
		this.messageCache = messageCache;
	}

	public void setMoRequestHandler(IntegrationMoRequestHandler<SmsMessage> moRequestHandler) {
		this.moRequestHandler = moRequestHandler;
	}

}
