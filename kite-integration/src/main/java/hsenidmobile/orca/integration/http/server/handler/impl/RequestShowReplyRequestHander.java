/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hsenidmobile.orca.integration.http.server.handler.impl;

import hsenidmobile.orca.core.HttpReplyRequestListener;
import hsenidmobile.orca.core.model.OrcaException;
import hsenidmobile.orca.core.model.message.MessageContext;
import hsenidmobile.orca.core.model.message.RequestShowReplyRequest;
import hsenidmobile.orca.integration.http.server.handler.RequestHandler;
import org.apache.log4j.Logger;

/**
 * $LastChangedDate: 2010-01-25 14:40:28 +0530 (Mon, 25 Jan 2010) $
 * $LastChangedBy: sandarenu $
 * $LastChangedRevision: 56714 $
 */
public class RequestShowReplyRequestHander implements RequestHandler<RequestShowReplyRequest> {

    private static final Logger logger = Logger.getLogger(AppDataUpdateRequestHandler.class);
	private HttpReplyRequestListener httpReplyRequestListener;

	@Override
	public boolean processRequest(RequestShowReplyRequest message, MessageContext context) throws OrcaException
    {
		if (logger.isDebugEnabled()) {
			logger.debug("Sending RequestShowReplyRequest to Core[" + message + "]");
		}
        httpReplyRequestListener.onMessage(message, context);
        return true;
	}

	public void setHttpUpdateRequestListener(HttpReplyRequestListener httpUpdateRequestListener) {
		this.httpReplyRequestListener = httpUpdateRequestListener;
	}
}
