/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.integration.http.server.transformers;

import hsenidmobile.orca.core.model.ContentRelease;
import hsenidmobile.orca.core.model.message.AppDataUpdateRequest;

import java.io.IOException;
import java.io.ObjectInputStream;

import org.apache.log4j.Logger;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBufferInputStream;
import org.jboss.netty.handler.codec.http.HttpHeaders;
import org.jboss.netty.handler.codec.http.HttpRequest;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * @version $LastChangedRevision$
 */
public class AppDataUpdateHttpRequestTransformer implements HttpRequestTransformer<AppDataUpdateRequest> {

	private static final Logger logger = Logger.getLogger(AppDataUpdateHttpRequestTransformer.class);

	@Override
	public AppDataUpdateRequest transformGet(HttpRequest httpRequest, long correlationId) {
		throw new UnsupportedOperationException("Receiving update requests through GET request NOT supported.");
	}

	@Override
	public AppDataUpdateRequest transformPost(HttpRequest httpRequest, long correlationId) {
		ChannelBuffer content = httpRequest.getContent();

		if (content.capacity() > 0) {
			try {
				AppDataUpdateRequest updateReq = readSubmitObject(content,
						httpRequest.getHeader(HttpHeaders.Names.CONTENT_TYPE));
				logger.info("Generated Orca CorrelationId[" + correlationId + "] for UpdateRequest CorrelationId["
						+ updateReq.getCorrelationId() + "]");
				if (updateReq.getUpdateContent() != null) {
					for (ContentRelease contentRelease : updateReq.getUpdateContent()) {
						contentRelease.setCorrelationId(correlationId);
					}
				}
				return updateReq;
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
		return null;
	}

	private AppDataUpdateRequest readSubmitObject(ChannelBuffer content, String contentType) throws IOException,
			ClassNotFoundException {
		if (logger.isDebugEnabled()) {
			logger.debug("Received Content type[" + contentType + "]");
		}
		if (contentType != null) {
			if (contentType.contains("application/octet-stream")) {
				Object object = createMsgFromSerializedData(content);
				if (object instanceof AppDataUpdateRequest) {
					AppDataUpdateRequest updateRequest = (AppDataUpdateRequest) object;
					if (logger.isDebugEnabled()) {
						logger.debug("Update request received [" + updateRequest + "]");
					}
					return updateRequest;
				}
			}
		}

		throw new RuntimeException("Not supported Content-Type[" + contentType
				+ "]. Supports application/octet-stream and text/plain");
	}

	private Object createMsgFromSerializedData(ChannelBuffer content) throws IOException, ClassNotFoundException {
		if (logger.isDebugEnabled()) {
			logger.debug("Decoding received message from Serialized object.");
		}
		ObjectInputStream objectInputStream = null;
		try {
			ChannelBufferInputStream bufferInputStream = new ChannelBufferInputStream(content);
			objectInputStream = new ObjectInputStream(bufferInputStream);
			return objectInputStream.readObject();
		} finally {
			try {
				if (objectInputStream != null) {
					objectInputStream.close();
				}
			} catch (Exception ex) {
			}
		}
	}

}
