/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.integration.http.server.transformers;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.handler.codec.http.HttpHeaders;
import org.jboss.netty.handler.codec.http.HttpRequest;
import org.jboss.netty.handler.codec.http.QueryStringDecoder;

import hsenidmobile.orca.core.model.Msisdn;
import hsenidmobile.orca.core.model.message.SmsMessage;
import hsenidmobile.orca.core.services.NumberValidataionService;

/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class GsmModemHttpRequestTransformer implements HttpRequestTransformer<SmsMessage> {

	private static final Logger logger = Logger.getLogger(GsmModemHttpRequestTransformer.class);
	private NumberValidataionService numberValidataionService;

	@Override
	public SmsMessage transformGet(HttpRequest httpRequest, long correlationId) {
		QueryStringDecoder queryStringDecoder = new QueryStringDecoder(httpRequest.getUri());
		Map<String, List<String>> params = queryStringDecoder.getParameters();
		if (!params.isEmpty()) {
			String messageTxt = params.get("MSG").get(0);
			String senderAddr = params.get("SND_ADDR").get(0);
			String destinationAddr = params.get("RSV_ADDR").get(0);

			SmsMessage smsMessage = new SmsMessage();
			smsMessage.setMessage(messageTxt);
			smsMessage.setCorrelationId(correlationId);
			smsMessage.setSenderAddress(new Msisdn(senderAddr, numberValidataionService.findOperation(senderAddr)));
			smsMessage.addReceiverAddress(new Msisdn(destinationAddr, numberValidataionService
					.findOperation(destinationAddr)));

			return smsMessage;
		}
		return null;
	}

	@Override
	public SmsMessage transformPost(HttpRequest httpRequest, long correlationId) {
		ChannelBuffer content = httpRequest.getContent();

		if (content.capacity() > 0) {
			try {
				SmsMessage sms = readSubmitObject(content, httpRequest.getHeader(HttpHeaders.Names.CONTENT_TYPE));
				if(sms != null){
					sms.setCorrelationId( correlationId);
				}
				return sms;
			} catch (Exception e) {
				logger.error("Error while creating Sms Message form received Http POST request", e);
				return null;
			}

		} else {
			if (logger.isTraceEnabled()) {
				logger.trace("Message from SBL Content size [0], nothing to do.");
			}
			return null;
		}
	}

	private SmsMessage readSubmitObject(ChannelBuffer content, String contentType) throws IOException,
			ClassNotFoundException {
		if (logger.isDebugEnabled()) {
			logger.debug("Received Content type[" + contentType + "]");
		}
		if (contentType != null) {
			if (contentType.contains("application/x-www-form-urlencoded")) {
				Map<String, String> postParm = createPostParamMap(content.toString("UTF-8"));
				String messageTxt = postParm.get("MSG");
				String senderAddr = postParm.get("SND_ADDR");
				String destinationAddr = postParm.get("RSV_ADDR");

				SmsMessage smsMessage = new SmsMessage();
				smsMessage.setMessage(messageTxt);
				smsMessage.setSenderAddress(new Msisdn(senderAddr, numberValidataionService.findOperation(senderAddr)));
				smsMessage.addReceiverAddress(new Msisdn(destinationAddr, numberValidataionService
						.findOperation(destinationAddr)));

				return smsMessage;
			}
		}

		throw new RuntimeException("Not supported Content-Type[" + contentType
				+ "]. Supports application/octet-stream and text/plain");
	}

	private Map<String, String> createPostParamMap(String postParam) {
		if (logger.isDebugEnabled()) {
			logger.debug("Decoding recived message from POST parameters.");
		}
		Map<String, String> postParamMap = new HashMap<String, String>();

		String[] paramsList = postParam.split("&");
		for (String aParam : paramsList) {
			String[] nameValue = aParam.split("=");
			if (nameValue.length >= 1) {
				String key = nameValue[0];
				String value = "";
				if (nameValue.length > 1) {
					value = nameValue[1];
				}
				postParamMap.put(key, value);
			}
		}
		return postParamMap;
	}

	public void setNumberValidataionService(NumberValidataionService numberValidataionService) {
		this.numberValidataionService = numberValidataionService;
	}

}
