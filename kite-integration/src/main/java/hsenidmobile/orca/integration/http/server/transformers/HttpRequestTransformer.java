/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.integration.http.server.transformers;

import org.jboss.netty.handler.codec.http.HttpRequest;

/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public interface HttpRequestTransformer <Transformed> {

	/**
	 * Transform given HTTP GET request to some other
	 * type(Transformed).
	 *
	 * @param httpRequest -
	 * @return
	 */
	Transformed transformGet(HttpRequest httpRequest, long correlationId);

	/**
	 * Transform given HTTP POST request to some other
	 * type(Transformed).
	 *
	 * @param httpRequest -
	 * @return
	 */
	Transformed transformPost(HttpRequest httpRequest, long correlationId);
}
