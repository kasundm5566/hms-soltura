/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hsenidmobile.orca.integration.http.server.transformers;

import hsenidmobile.orca.core.model.message.AppDataUpdateRequest;
import hsenidmobile.orca.core.model.message.RequestShowReplyRequest;
import org.apache.log4j.Logger;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBufferInputStream;
import org.jboss.netty.handler.codec.http.HttpHeaders;
import org.jboss.netty.handler.codec.http.HttpRequest;

import java.io.IOException;
import java.io.ObjectInputStream;

/**
 * $LastChangedDate: 2010-01-25 14:40:28 +0530 (Mon, 25 Jan 2010) $
 * $LastChangedBy: sandarenu $
 * $LastChangedRevision: 56714 $
 */
public class RequestShowHttpRequestTransformer implements HttpRequestTransformer<RequestShowReplyRequest>{

    private static final Logger logger = Logger.getLogger(RequestShowHttpRequestTransformer.class);

	@Override
	public RequestShowReplyRequest transformGet(HttpRequest httpRequest, long correlationId) {
		throw new UnsupportedOperationException("Receiving reply requests through GET request NOT supported.");
	}

    @Override
	public RequestShowReplyRequest transformPost(HttpRequest httpRequest, long correlationId) {
		ChannelBuffer content = httpRequest.getContent();

		if (content.capacity() > 0) {
			try {
				RequestShowReplyRequest updateReq = readSubmitObject(content,
						httpRequest.getHeader(HttpHeaders.Names.CONTENT_TYPE));

				return updateReq;
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
		return null;
	}

    private RequestShowReplyRequest readSubmitObject(ChannelBuffer content, String contentType) throws IOException,
			ClassNotFoundException {
		if (logger.isDebugEnabled()) {
			logger.debug("Received Content type[" + contentType + "]");
		}
		if (contentType != null) {
			if (contentType.contains("application/octet-stream")) {
				Object object = createMsgFromSerializedData(content);
				if (object instanceof RequestShowReplyRequest) {
					RequestShowReplyRequest updateRequest = (RequestShowReplyRequest) object;
					if (logger.isDebugEnabled()) {
						logger.debug("Update request received [" + updateRequest + "]");
					}
					return updateRequest;
				}
			}
		}

		throw new RuntimeException("Not supported Content-Type[" + contentType
				+ "]. Supports application/octet-stream and text/plain");
	}

    private Object createMsgFromSerializedData(ChannelBuffer content) throws IOException, ClassNotFoundException {
		if (logger.isDebugEnabled()) {
			logger.debug("Decoding received message from Serialized object.");
		}
		ObjectInputStream objectInputStream = null;
		try {
			ChannelBufferInputStream bufferInputStream = new ChannelBufferInputStream(content);
			objectInputStream = new ObjectInputStream(bufferInputStream);
			return objectInputStream.readObject();
		} finally {
			try {
				if (objectInputStream != null) {
					objectInputStream.close();
				}
			} catch (Exception ex) {
			}
		}
	}
}
