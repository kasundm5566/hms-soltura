/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$ 
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.integration.services;

import org.apache.log4j.Logger;

import hsenidmobile.orca.core.model.Msisdn;
import hsenidmobile.orca.core.services.charging.ChargingException;
import hsenidmobile.orca.core.services.charging.ChargingService;

/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class ChargingServiceImpl implements ChargingService {

	private static final Logger logger = Logger.getLogger(ChargingServiceImpl.class);

	@Override
	public void cancel(String transactionId) {
		// TODO Auto-generated method stub

	}

	@Override
	public void commit(String transactionId) {
		// TODO Auto-generated method stub
		logger.info("Commiting Transaction[" + transactionId + "]");
	}

	@Override
	public String reserveDebit(Msisdn msisdn, String amount) throws ChargingException {
		// TODO Auto-generated method stub
		logger.info("ReserveDebit from Msisdn[" + msisdn + "] Amount[" + amount + "]");
		return null;
	}

}
