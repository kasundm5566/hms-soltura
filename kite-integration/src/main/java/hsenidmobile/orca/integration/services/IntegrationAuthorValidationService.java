package hsenidmobile.orca.integration.services;

import hsenidmobile.orca.core.applications.ApplicationImpl;
import hsenidmobile.orca.core.applications.exception.ApplicationException;
import hsenidmobile.orca.core.model.Application;
import hsenidmobile.orca.core.model.Msisdn;
import hsenidmobile.orca.core.model.OrcaSdpAppIntegration;
import hsenidmobile.orca.core.repository.SdpApplicationRepository;
import hsenidmobile.orca.core.services.AuthorValidationService;
import hsenidmobile.orca.rest.common.util.RestApiKeys;
import hsenidmobile.orca.rest.registration.transport.CommonRegistrationRequestSender;
import hsenidmobile.orca.rest.sdp.sp.transport.SpManageRequestSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class IntegrationAuthorValidationService implements AuthorValidationService {

    private static final Logger logger = LoggerFactory.getLogger(IntegrationAuthorValidationService.class);

    @Autowired
    private SdpApplicationRepository sdpApplicationRepository;
    @Autowired
    private SpManageRequestSender spManageRequestSender;
    @Autowired
    private CommonRegistrationRequestSender commonRegistrationRequestSender;

    public boolean isValidAuthor(Application app, Msisdn senderAddress) {
        try {
            logger.debug("start validating author [{}] for app [{}]", senderAddress, app.getAppId());

            OrcaSdpAppIntegration integrationDetails = sdpApplicationRepository.getIntegrationDetails(app.getAppId());

            Map<String,Object> sp = spManageRequestSender.findSp(integrationDetails.getSpId());

            if (sp == null) {
                logger.error("Couldn't find sp for app [{}] with sp-id [{}]", app.getAppId(), integrationDetails.getSpId());
                return false;
            }
            logger.debug("Sp for sp-id is ", sp);

            String corpUserId = (String) sp.get(RestApiKeys.CP_ID);
            String msisdn = commonRegistrationRequestSender.getMsisdnByUserId(corpUserId);

            if (msisdn == null) {
                logger.error("msisdn [{}] for user not found.", corpUserId);
                return false;
            }

            logger.debug("msisdn for corp-user-id [{}] is [{}]", sp.get(RestApiKeys.CP_ID), msisdn);

            return senderAddress.getAddress().contains(msisdn);

        } catch (ApplicationException e) {
            logger.error("Error while validating author", e);
            //todo throw the exception rather sending boolean, so the client of this application can handle it properly
            return false;
        }
    }

    public void setSdpApplicationRepository(SdpApplicationRepository sdpApplicationRepository) {
        this.sdpApplicationRepository = sdpApplicationRepository;
    }

    public void setSpManageRequestSender(SpManageRequestSender spManageRequestSender) {
        this.spManageRequestSender = spManageRequestSender;
    }

    public void setCommonRegistrationRequestSender(CommonRegistrationRequestSender commonRegistrationRequestSender) {
        this.commonRegistrationRequestSender = commonRegistrationRequestSender;
    }
}
