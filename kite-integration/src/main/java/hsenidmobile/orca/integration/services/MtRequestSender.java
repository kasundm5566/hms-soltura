/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.integration.services;

import hsenidmobile.orca.core.applications.exception.ApplicationException;


/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public interface MtRequestSender<T> {
	/**
	 * Send Mt message out.
	 * @param message
	 * @param orcaAppId
	 * @return Status-Code
	 */
	String sendMt(T message, String orcaAppId) throws ApplicationException;

	/**
	 * Error code to be used if internal exception happen when sending message.
	 * @param dispatchErrorDueExceptionErrorCode
	 */
	void setDispatchErrorDueExceptionErrorCode(String dispatchErrorDueExceptionErrorCode);
}
