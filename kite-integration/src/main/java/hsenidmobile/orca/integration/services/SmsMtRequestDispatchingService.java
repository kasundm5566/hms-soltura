/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.integration.services;

import hsenidmobile.orca.core.flow.StartUpService;
import hsenidmobile.orca.core.flow.dispatch.DispatchableMessage;
import hsenidmobile.orca.core.flow.dispatch.lb.NoLbEntityAvailable;
import hsenidmobile.orca.core.flow.dispatch.lb.RoundRobinLb;
import hsenidmobile.orca.core.model.message.MtRequest;
import hsenidmobile.orca.core.model.message.SmsMessage;
import hsenidmobile.orca.core.repository.MessageDispatchStatRepository;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

import hsenidmobile.orca.integration.aventura.AventuraApiMessageDispatcherImpl;
import org.apache.log4j.Logger;
import org.apache.log4j.NDC;

/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class SmsMtRequestDispatchingService implements Runnable {

	private static final Logger logger = Logger.getLogger(SmsMtRequestDispatchingService.class);
	private ExecutorService dispatchingThreadPool;    
	private RoundRobinLb<MtRequest> roundRobinLb;
	private String dispatchErrorDueExceptionErrorCode;
	private AtomicBoolean continueProcessing = new AtomicBoolean(false);
	private MtRequestSender smsMtService;
	private MessageDispatchStatRepository dispatchStatRepository;
	private DataAppender smsAppender;

	private int dispatchingThreadCont = 5;
	private final Object waitLock = new Object();

	public boolean isRunning() {
		return continueProcessing.get();
	}

	public void start() {
		logger.info("=== Starting SmsMtRequestDispatchingService ===");
		if (this.continueProcessing.get()) {
			logger.warn("SmsMtRequestDispatchingService is already running.");
		} else {
			continueProcessing.set(true);
			dispatchingThreadPool = Executors.newFixedThreadPool(dispatchingThreadCont);
			Thread runningThread = new Thread(this);
			runningThread.start();
			logger.info("=== Started SmsMtRequestDispatchingService ===");
		}
	}

	public void stop() {
		logger.info("=== Stopping SubscriptionServiceSchedular ===");
		continueProcessing.set(false);
	}

	public void run() {
		logger.info("=== Started MtRequestDispatchingService ===");

		while (continueProcessing.get()) {
			try {
				DispatchableMessage<MtRequest> dm = roundRobinLb.getNext();
				if (dm != null) {
					if (logger.isDebugEnabled()) {
						logger.debug("Message Taken from Cache[" + dm + "]");
					}
					SmsMessage nextMessageToDispatch = (SmsMessage) (dm.getMessage().getMessage());
					if (nextMessageToDispatch != null) {
						if (nextMessageToDispatch.getMessage() == null
								|| nextMessageToDispatch.getMessage().length() == 0) {
							logger.warn("Given message is empty, Ignore message given to send[" + nextMessageToDispatch
									+ "]");
						} else {
							if (smsAppender != null) {
								smsAppender.append(nextMessageToDispatch);
							}
							dispatchingThreadPool.submit(new SmsMtSender(smsMtService, nextMessageToDispatch, dm
									.getAppId(), dispatchErrorDueExceptionErrorCode, dispatchStatRepository));
						}
					}
				} else {
					performWait(100);
				}
			} catch (NoLbEntityAvailable e) {
				// ignore for the moment
				performWait(1000);
			} catch (Throwable th) {
				logger.error("Exception while sending SmsMessage", th);
			}
		}

		logger.info("=== Stopped MtRequestDispatchingService ===");
	}

	private void performWait(int waitTime) {
		synchronized (waitLock) {
			try {
				waitLock.wait(waitTime);
			} catch (InterruptedException e1) {
				Thread.currentThread().interrupt();
			}
		}
	}

	public void setRoundRobinLb(RoundRobinLb<MtRequest> roundRobinLb) {
		this.roundRobinLb = roundRobinLb;
	}

	public void setSmsMtService(MtRequestSender smsMtService) {
		this.smsMtService = smsMtService;
	}

	public void setDispatchingThreadCont(int dispatchingThreadCont) {
		this.dispatchingThreadCont = dispatchingThreadCont;
	}

	public void setDispatchErrorDueExceptionErrorCode(String dispatchErrorDueExceptionErrorCode) {
		this.dispatchErrorDueExceptionErrorCode = dispatchErrorDueExceptionErrorCode;
	}

	public void setDispatchStatRepository(MessageDispatchStatRepository dispatchStatRepository) {
		this.dispatchStatRepository = dispatchStatRepository;
	}

	public void setSmsAppender(DataAppender smsAppender) {
		this.smsAppender = smsAppender;
	}

}

class SmsMtSender implements Runnable {

	private static final Logger logger = Logger.getLogger(SmsMtRequestDispatchingService.class);
	private final MtRequestSender smsMtService;
	private final SmsMessage message;
	private final String orcaAppId;
	private final String internalErrorCode;
	private final MessageDispatchStatRepository dispatchStatRepository;

	public SmsMtSender(MtRequestSender smsMtService, SmsMessage message, String orcaAppId, String internalErrorCode,
			MessageDispatchStatRepository dispatchStatRepository) {
		this.smsMtService = smsMtService;
		this.message = message;
		this.orcaAppId = orcaAppId;
		this.internalErrorCode = internalErrorCode;
		this.dispatchStatRepository = dispatchStatRepository;
	}

	public void run() {
		try {
			NDC.push(String.valueOf(message.getCorrelationId()));

			if (logger.isDebugEnabled()) {
				logger.debug("Dispatching SMS message[" + message + "] AppId[" + orcaAppId + "]");
			}
			String statusCode = internalErrorCode;
			try {
				statusCode = smsMtService.sendMt(message, orcaAppId);
			} catch (Throwable th) {
				logger.error("Exception while sending SmsMessage[" + message + "] AppId[" + orcaAppId + "]", th);
			}

			dispatchStatRepository.updateDispatchSummary(orcaAppId, statusCode);

		} finally {
			NDC.remove();
		}

	}
}
