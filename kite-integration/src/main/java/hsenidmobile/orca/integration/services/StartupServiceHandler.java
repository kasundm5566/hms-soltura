/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$ 
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.integration.services;

import hsenidmobile.orca.core.flow.StartUpService;

import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextStartedEvent;
import org.springframework.context.event.ContextStoppedEvent;

/**
 * Start any configured startup services when Orca is starting up.
 *
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class StartupServiceHandler implements ApplicationListener {

    private static final Logger logger = Logger.getLogger(StartupServiceHandler.class);
    private List<StartUpService> startupServicesList;
    private Map<String, ScheduledExecutorService> executorServices;

    public void onApplicationEvent(ApplicationEvent springEvent) {

        if (logger.isDebugEnabled()) {
            logger.debug("Spring Event Received...["+springEvent+"]");
        }

        if (springEvent instanceof ContextStartedEvent ) { // || springEvent instanceof ContextRefreshedEvent
            if (executorServices != null) {
                logger.info("start up services has started.");
            } else {
                executorServices = new HashMap<String, ScheduledExecutorService>();
                startServices();
            }
        } else if (springEvent instanceof ContextStoppedEvent ) { // || springEvent instanceof ContextClosedEvent) {
            stopServices();
            executorServices = null;
        }

    }

    private void stopServices() {
        if (executorServices == null || executorServices.isEmpty()) {
            logger.info("No start up service found to terminate.");
            return;
        }
        Set<Map.Entry<String,ScheduledExecutorService>> entries = executorServices.entrySet();
        for (Map.Entry<String, ScheduledExecutorService> entry : entries) {
            logger.info("Shutting down start up service [" + entry.getKey() + "]");
            entry.getValue().shutdown();
        }
    }

    private void startServices() {

        if (startupServicesList == null) {
            logger.info("No startup services configured to start up");
            return;
        }

        logger.info("Starting [" + startupServicesList.size() + "] system start up services..");

        for (StartUpService startUpService : startupServicesList) {

            logger.info("Starting start up service [" + startUpService.getName()
                    + "] delay[" + startUpService.getDelay() + "] period [" + startUpService.getPeriod() + "]");

            ScheduledExecutorService executor = Executors.newScheduledThreadPool(startUpService.getPoolSize());

            executorServices.put(getServiceName(startUpService.getName()), executor);
            startUpService.setExecutorService(executor);

            executor.scheduleAtFixedRate(startUpService, startUpService.getDelay(), startUpService.getPeriod(),
                    startUpService.getTimeUnit());
        }

        logger.info("Started all [" + startupServicesList.size() + "]  start up services.");
    }

    private String getServiceName(String serviceName) {
        return "" + serviceName + "/" + System.currentTimeMillis() + "";
    }

    public void setStartupServicesList(List<StartUpService> startupServicesList) {
        this.startupServicesList = startupServicesList;
    }

}
