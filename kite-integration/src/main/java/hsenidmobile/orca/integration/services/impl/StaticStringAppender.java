/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.integration.services.impl;

import org.apache.log4j.Logger;

import hsenidmobile.orca.core.model.message.SmsMessage;
import hsenidmobile.orca.integration.services.DataAppender;

/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class StaticStringAppender implements DataAppender<SmsMessage> {

	private static final Logger logger = Logger.getLogger(StaticStringAppender.class);
	private String appendTxt;

	@Override
	public void append(SmsMessage smsMessage) {
		if (logger.isDebugEnabled()) {
			logger.debug("Appending txt[" + appendTxt + "] to given sms[" + smsMessage + "]");
		}
		if (smsMessage != null && smsMessage.getMessage() != null) {
			smsMessage.setMessage(smsMessage.getMessage() + appendTxt);
		}

	}

	public void setAppendTxt(String appendTxt) {
		this.appendTxt = appendTxt;
	}

}
