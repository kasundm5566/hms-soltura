/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$ 
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.integration.transformers;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import hsenidmobile.orca.core.model.Msisdn;
import hsenidmobile.orca.core.model.message.SmsMessage;
import hsenidmobile.orca.core.services.NumberValidataionService;
//import hsenidmobile.sdp.ws.sms.wsdl.deliver.DeliverSmsRequestType;

/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class SmsDeliverRequestToSmsMoRequestTransformer  {

	private static final Logger logger = Logger.getLogger(SmsDeliverRequestToSmsMoRequestTransformer.class);
	private NumberValidataionService numberValidataionService;


	public void setNumberValidataionService(NumberValidataionService numberValidataionService) {
		this.numberValidataionService = numberValidataionService;
	}

}
