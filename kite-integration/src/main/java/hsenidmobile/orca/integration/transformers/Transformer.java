/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$ 
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.integration.transformers;

import java.io.InputStream;

/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public interface Transformer<Source, Transformed> {
	/**
	 * Transform given message of one type(Source) to some other
	 * type(Transformed).
	 * 
	 * @param sourceMessage
	 * @return
	 */
	Transformed transform(Source sourceMessage);

    Object readJsonResponse(InputStream is, Class aClass);

}
