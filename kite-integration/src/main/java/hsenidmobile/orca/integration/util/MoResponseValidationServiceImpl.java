/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hsenidmobile.orca.integration.util;

import hsenidmobile.orca.core.services.MoResponseValidationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class MoResponseValidationServiceImpl implements MoResponseValidationService {

    private static final Logger logger = LoggerFactory.getLogger(MoResponseValidationServiceImpl.class);
    private List<String> chargingErrorCodes;
    private List<String> chargingSuccessCodes;

    public boolean isMoCanBeChargedUsingMtCharging(String responseCode) {
        logger.debug("Received response code for verification [{}]", responseCode);
        boolean moChargeSuccess = true;
        if(responseCode != null && !chargingSuccessCodes.contains(responseCode.trim())) {
            moChargeSuccess = false;
        }
        if (logger.isDebugEnabled()) {
            logger.debug("moChargeSuccess [ " + moChargeSuccess + " ]");
        }
        return moChargeSuccess;
    }

    public void setChargingErrorCodes(List<String> chargingErrorCodes) {
        this.chargingErrorCodes = chargingErrorCodes;
    }

    public void setChargingSuccessCodes(List<String> chargingSuccessCodes) {
        this.chargingSuccessCodes = chargingSuccessCodes;
    }
}
