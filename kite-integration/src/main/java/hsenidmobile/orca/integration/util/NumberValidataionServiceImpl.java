/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.integration.util;

import hsenidmobile.orca.core.services.NumberValidataionService;

import java.util.Map;

import org.apache.log4j.Logger;

/**
 * From SDP we don't receive the Operator of a given MSISDN. Here we are using
 * the Operator code to find the Operator for a given MSISDN. This will not work
 * with 'Number Portability'.
 *
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class NumberValidataionServiceImpl implements NumberValidataionService {

	private static final Logger logger = Logger.getLogger(NumberValidataionServiceImpl.class);
	private Map<String, String> operatorCodes;
	private String Operator;

	/**
	 * +94777123456, 0094777123456, 0777123456
	 */
	public String findOperation(String msisdn) {
		// TODO: Implement the logic
		return Operator;
	}

	public void setOperatorCodes(Map<String, String> operatorCodes) {
		this.operatorCodes = operatorCodes;
	}

	public void setOperator(String operator) {
		Operator = operator;
	}

}
