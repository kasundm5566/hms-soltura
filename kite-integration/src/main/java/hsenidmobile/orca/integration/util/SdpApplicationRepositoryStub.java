package hsenidmobile.orca.integration.util;

import hsenidmobile.orca.core.model.SdpAppLoginData;
import hsenidmobile.orca.orm.sdp.SdpApplicationRepositoryImpl;
/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *   
 *   $LastChangedDate$
 *   $LastChangedBy$ 
 *   $LastChangedRevision$
 */
//TODO REMOVE THIS CLASS

public class SdpApplicationRepositoryStub extends SdpApplicationRepositoryImpl {

    private String appId;
    private String password;


    @Override
    public SdpAppLoginData getSdpAppLoginDataForOrcaApp(String orcaAppId) {
//		return new SdpAppLoginData("TB_Test0404", "098f6bcd4621d373cade4e832627b4f6");
		return new SdpAppLoginData(appId, password);
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
