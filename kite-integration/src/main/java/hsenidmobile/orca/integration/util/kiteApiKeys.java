/**
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hsenidmobile.orca.integration.util;

/*
* $LastChangedDate: 2011-07-25 15:38:58 +0530 (Mon, 25 Jul 2011) $
* $LastChangedBy: supunh $
* $LastChangedRevision: 75237 $
*/
public interface kiteApiKeys {

    public static final String DEFAULT_VERSION = "1.0";
    public static final String ADDRESS_KEY = "destinationAddresses";
    public static final String SOURCE_ADDRESS_KEY = "sourceAddress";
    public static final String MESSAGE_KEY = "message";
    public static final String MESSAGE_ID_KEY = "requestId";
    public static final String VERSION_KEY = "version";
    public static final String ADDRESS_LIST_PREFIX = "list:";
    public static final String CORRELATION_ID_HEADER = "correlation-id";
    public static final String APPLICATION_ID = "applicationId";
    public static final String APP_PASSWORD = "password";
    public static final String BC_ENCODING = "encoding";
    public static final String BC_UDHI = "udhi";
    public static final String BC_ALL = "tel:all";
    public static final String BC_START_DATE = "start-date";
    public static final String BC_END_DATE = "end-date";
    public static final String STATUS_CODE_KEY = "statusCode";
    public static final String STATUS_DETAILS_KEY = "statusDetail";
    public static final String SUCCESS_STATUS_CODE = "success-status-code";
    public static final String SUCCESS__CODE = "S1000";
    public static final String EXCEPTION_OCCURED_ERROR_MESSAGE= "Exception Occured while sending the message";
    public static final String RESPONCE_NULL_ERROR_MESSAGE = "Connectivity problem occured please check the sdp connection";
    public static final boolean UDHI = false;
    public static final String sendFailureStatusCode = "SMS-MT-7001";
    public static final String successStatusCode = "SMS-MT-2000";

}



