/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$ 
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.integration.transformers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import hsenidmobile.orca.core.model.message.SmsMessage;
import hsenidmobile.orca.integration.util.NumberValidataionServiceImpl;
//import hsenidmobile.sdp.ws.sms.wsdl.common.SegmentType;
//import hsenidmobile.sdp.ws.sms.wsdl.deliver.DeliverSmsRequestType;

import org.junit.Test;

/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class SmsDeliverRequestToSmsMoRequestTransformerTest {

	/**
	 * Test method for
	 * {@link hsenidmobile.orca.integration.transformers.SmsDeliverRequestToSmsMoRequestTransformer#transform(hsenidmobile.sdp.ws.sms.wsdl.deliver.DeliverSmsRequestType)}
	 * .
	 */
	@Test
	public void testTransform() {
//		DeliverSmsRequestType deliverReq = new DeliverSmsRequestType();
//		deliverReq.setDestination("0771456789");
//		deliverReq.setSourceAddress("071456789");
//		SegmentType segment = new SegmentType();
//		segment.setMessage("Test Message");
//		deliverReq.setSegment(segment);
//
//
//		SmsDeliverRequestToSmsMoRequestTransformer trasformer = new SmsDeliverRequestToSmsMoRequestTransformer();
//		trasformer.setNumberValidataionService(new NumberValidataionServiceImpl());
//		SmsMessage transformed =  trasformer.transform(deliverReq);
//
//		assertNotNull(transformed);
//		assertEquals("Source NO", "071456789", transformed.getSenderAddress().getAddress());
//		assertEquals("Destination No", "0771456789", transformed.getReceiverAddresses().get(0).getAddress());
//		assertEquals("Message Test", "Test Message", transformed.getMessage());
	}
}
