/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hsenidmobile.orca.orm.repository;

import hsenidmobile.orca.core.applications.ContentDispatchSummary;
import hsenidmobile.orca.core.applications.alert.AlertContentRelease;
import hsenidmobile.orca.core.applications.exception.ApplicationException;
import hsenidmobile.orca.core.flow.MessageFlowErrorReasons;
import hsenidmobile.orca.core.repository.AlertDispatchedContentRepository;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.joda.time.DateTime;
import org.springframework.transaction.annotation.Transactional;

/**
 * $LastChangedDate$ $LastChangedBy$ $LastChangedRevision$
 *
 */
public class AlertDispatchedContentRepositoryImpl implements AlertDispatchedContentRepository {

    private static final Logger logger = Logger.getLogger(AlertDispatchedContentRepositoryImpl.class);
    private SessionFactory sessionFactory;
    private static final int START = 0;
    private static final int END = 1;
    private static final String  GET_LAST_SENT_ALERT_BY_APP_ID = "select lm.message, con.sent_time from alert_content_release acr " +
            "INNER JOIN alert_content_release_content acrc on acr.id=acrc.alert_content_release_id " +
            "INNER JOIN content con on con.id= acrc.contents_id " +
            "INNER JOIN localized_message lm on lm.id=con.localized_msg_id " +
            "where acr.app_id=? order by con.sent_time DESC limit " + START + ", " + END + " ";

    private static final String  GET_LAST_SENT_ALERT_BY_SUB_CATEGORY = "select lm.message, con.sent_time from alert_content_release acr " +
            "INNER JOIN alert_content_release_content acrc on acr.id=acrc.alert_content_release_id " +
            "INNER JOIN content con on con.id= acrc.contents_id " +
            "INNER JOIN localized_message lm on lm.id=con.localized_msg_id " +
            "INNER JOIN sub_category sub on sub.id=con.sub_category " +
            "where acr.app_id=? and con.sub_category=? order by con.sent_time DESC limit " + START + "," + END + " ";

    @Override
    @Transactional
    public void addDispatchedContent(AlertContentRelease contentRelease) {
        logger.info("Add AlertContentRelease to repository [" + contentRelease + "]");
        sessionFactory.getCurrentSession().save(contentRelease);
    }

    @Override
    @Transactional(readOnly = true)
    public List<AlertContentRelease> getDispatchedContent(String appId) {
        final Query query = sessionFactory.getCurrentSession().createQuery("from AlertContentRelease where appId = ?")
                .setString(START, appId);
        List<AlertContentRelease> dispatchedContent = query.list();
        return dispatchedContent;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    @Transactional(readOnly = true)
    public ContentDispatchSummary getLastSentAlertByApplicationId(String appId) throws ApplicationException {
        Query query = sessionFactory.getCurrentSession().createSQLQuery(GET_LAST_SENT_ALERT_BY_APP_ID).
                setString(0, appId);
        List summaryDataList = query.list();
        ContentDispatchSummary contentDispatchSummary = null;
        if (summaryDataList != null && !summaryDataList.isEmpty()) {
            contentDispatchSummary = processReceivedContentDispatchSummary(summaryDataList);
            return contentDispatchSummary;
        } else {
            throw new ApplicationException("Last Sent Alert Content not found for the appId[" + appId + "]",
                    MessageFlowErrorReasons.NO_LAST_SENT_ALERT_FOUND);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public ContentDispatchSummary getLastSentAlertBySubCategoryId(String appId, long subCategoryId)
            throws ApplicationException {
        Query query = sessionFactory.getCurrentSession().createSQLQuery(GET_LAST_SENT_ALERT_BY_SUB_CATEGORY).
                setString(0, appId).setLong(1, subCategoryId);
        List summaryDataList = query.list();
        ContentDispatchSummary contentDispatchSummary = null;
        if (summaryDataList != null && !summaryDataList.isEmpty()) {
            contentDispatchSummary = processReceivedContentDispatchSummary(summaryDataList);
            return contentDispatchSummary;
        } else {
            throw new ApplicationException("Last Sent Alert Content not found for the appId[" + appId + "]" +
                    "and sub category id [" + subCategoryId+"]",
                    MessageFlowErrorReasons.NO_LAST_SENT_ALERT_FOUND);
        }
    }

    private ContentDispatchSummary processReceivedContentDispatchSummary(List summaryDataList) {
        ContentDispatchSummary contentDispatchSummary = new ContentDispatchSummary();
        Object[] summaryRow = (Object[]) summaryDataList.get(0);
        contentDispatchSummary.setLastSentContent((String)summaryRow[0]);
        contentDispatchSummary.setLastSentOnTime(new DateTime(summaryRow[1]));
        return contentDispatchSummary;
    }
}
