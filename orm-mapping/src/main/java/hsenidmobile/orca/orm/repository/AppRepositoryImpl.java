/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.orm.repository;

import hsenidmobile.orca.core.applications.ApplicationImpl;
import hsenidmobile.orca.core.applications.Service;
import hsenidmobile.orca.core.applications.alert.AlertService;
import hsenidmobile.orca.core.applications.exception.ApplicationException;
import hsenidmobile.orca.core.flow.MessageFlowErrorReasons;
import hsenidmobile.orca.core.model.Application;
import hsenidmobile.orca.core.model.ReceivedMoSummary;
import hsenidmobile.orca.core.repository.AppRepository;
import java.math.BigInteger;
import java.sql.Date;
import java.util.List;

import hsenidmobile.orca.core.repository.AppTrafficRepository;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class AppRepositoryImpl implements AppRepository {

    private static final Logger logger = Logger.getLogger(AppRepositoryImpl.class);
    private SessionFactory sessionFactory;

    private static final String FIND_APP_ID = "from ApplicationImpl where appId = ?";
    private static final String FIND_APP_NAME_LIKE = "from ApplicationImpl where appName like ?";
    private static final String FIND_APP_BY_NAME = "from ApplicationImpl where appName = ?";
    private static final String FIND_BY_ID = "from ApplicationImpl where id = ?";
    private static final String FIND_EXPIRED_APP_NAME = "from ApplicationImpl where endDate <= ? and status = 'ACTIVE'";

    private static final String FIND_CP_MANAGED_APP_BY_SPID = "SELECT * FROM application INNER JOIN orca_sdp_app_integration osdp "
            + "ON application.app_id=osdp.orca_app_id WHERE osdp.sp_id=?";

    private static final String FIND_CP_MANAGED_APP_BY_APP_NAME = "SELECT * FROM application INNER JOIN orca_sdp_app_integration osdp "
            + "ON application.app_id=osdp.orca_app_id WHERE osdp.sp_id=? and application.app_name like ?";

    private static final String UPDATE_MO_SUMMARY = "UPDATE mo_summary "
            + "SET total_count = (total_count + 1), last_update_time = ? WHERE app_id = ?";

    private static final String INSERT_MO_SUMMARY = "INSERT INTO mo_summary "
            + "(app_id, total_count, last_update_time) VALUES (?, 1, ?) ";

    private static final String GET_MO_SUMMARY = "SELECT app_id, total_count, last_update_time "
            + "FROM mo_summary WHERE app_id= ? ";

    @Transactional(readOnly = true)
    public List<ApplicationImpl> findCpMangedAppByAppName(String appName, String spId) {
        SQLQuery query = (SQLQuery) sessionFactory.getCurrentSession().createSQLQuery(FIND_CP_MANAGED_APP_BY_APP_NAME)
                .setString(0, spId).setString(1, "%" + appName + "%");
        query.addEntity(ApplicationImpl.class);
        List<ApplicationImpl> appList = query.list();
        return appList;
    }

    /**
     * @param appId
     * @return returns
     */
    @Transactional
    public Application findAppByAppId(String appId) throws ApplicationException {

        checkNullParam(appId);
        if (logger.isDebugEnabled()) {
            logger.debug("finding application : appId [" + appId + "]");
        }

        final Query query = sessionFactory.getCurrentSession().createQuery(FIND_APP_ID).setString(0, appId);
        List<Application> apps = query.list();
        if (apps.size() > 1) {
            throw new IllegalStateException("Multiple application found for appId[" + appId + "]");
        } else if (apps.size() == 0) {
            throw new ApplicationException("Application not found for appId[" + appId + "]",
                    MessageFlowErrorReasons.APP_NOT_FOUND);
        } else {
            return apps.get(0);
        }

    }

    @Transactional
    public List<Application> findAppByAppNameLike(String appName) throws ApplicationException {

        checkNullParam(appName);
        if (logger.isDebugEnabled()) {
            logger.debug("finding application by : appName [" + appName + "]");
        }

        final Query query = sessionFactory.getCurrentSession().createQuery(FIND_APP_NAME_LIKE).setString(0, appName);
        List<Application> apps = query.list();

        if (apps.size() > 0) {
            if (logger.isDebugEnabled()) {
                logger.debug("Applications found : count [" + apps.size() + "]");
            }
            return apps;
        } else {
            throw new ApplicationException("Application not found for appName[" + appName + "]",
                    MessageFlowErrorReasons.APP_NOT_FOUND);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public boolean isAppNameExists(String appName) {

        checkNullParam(appName);
        if (logger.isDebugEnabled()) {
            logger.debug("finding application by : appName [" + appName + "]");
        }
        final Query query = sessionFactory.getCurrentSession().createQuery(FIND_APP_BY_NAME).setString(0, appName);
        List<Application> apps = query.list();
        if (apps.size() != 0) {
            if (logger.isDebugEnabled()) {
                logger.debug("Applications name found");
            }
            return true;
        } else {
            if (logger.isDebugEnabled()) {
                logger.debug("Application not found : appName [" + appName + "]");
            }
            return false;
        }

    }

    /**
     * Creates new Application.
     *
     * @param application
     */

    @Transactional
    public void createApplication(Application application) {
        sessionFactory.getCurrentSession().save(application);
    }

    @Transactional
    public void updateApplication(Application application) {
        sessionFactory.getCurrentSession().update(application);
    }

    @Override
    @Transactional
    public List<Application> findSubscriptionApplications() {
        if (logger.isDebugEnabled()) {
            logger.debug("Finding Subscription applications.");
        }
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(ApplicationImpl.class);
        criteria.add(Restrictions.sqlRestriction("service = 'SUBSCRIPTION'"));
        criteria.add(Restrictions.sqlRestriction("status = 'ACTIVE'"));
        return criteria.list();
    }

    @Transactional
    public Application findAppById(Long id) throws ApplicationException {

        checkNullParam(id);
        final Query query = sessionFactory.getCurrentSession().createQuery(FIND_BY_ID).setLong(0, id);
        List<Application> apps = query.list();
        if (apps.size() > 1) {
            throw new IllegalStateException("Multiple applications found with id[" + id + "]");
        } else if (apps.size() == 0) {
            throw new ApplicationException("Application not found for id[" + id + "]",
                    MessageFlowErrorReasons.APP_NOT_FOUND);
        } else {
            return apps.get(0);
        }
    }

    private void checkNullParam(Object param) {
        if (param == null) {
            throw new NullPointerException("Argument cannot be null");
        }
    }

    private void checkNullParam(Object... param) {
        for (Object s : param) {
            if (s == null) {
                throw new NullPointerException("Arguments cannot be null");
            }
        }
    }

    @Override
    @Transactional
    public void updateService(Service service) {
        if (logger.isDebugEnabled()) {
            logger.debug("Updating service[" + service + "] to the db.");
        }
        sessionFactory.getCurrentSession().update(service);
    }

    @Transactional
    public void updateMoSummary(String appId) {
        if (logger.isDebugEnabled()) {
            logger.debug("Updating mo summary for application[" + appId + "]");
        }
        Query updateQuery = sessionFactory.getCurrentSession().createSQLQuery(UPDATE_MO_SUMMARY);
        updateQuery.setDate(0, new Date(System.currentTimeMillis()));
        updateQuery.setString(1, appId);
        int updatedRowCount = updateQuery.executeUpdate();
        if (logger.isDebugEnabled()) {
            logger.debug("Updated Row Count[" + updatedRowCount + "]");
        }
        if (updatedRowCount == 0) {
            if (logger.isDebugEnabled()) {
                logger.debug("Inserting since updated count is 0");
            }
            Query insertQuery = sessionFactory.getCurrentSession().createSQLQuery(INSERT_MO_SUMMARY);
            insertQuery.setString(0, appId);
            insertQuery.setDate(1, new Date(System.currentTimeMillis()));
            insertQuery.executeUpdate();
        }
    }

    @Transactional(readOnly = true)
    public ReceivedMoSummary getMoSummary(String appId) {
        if (logger.isDebugEnabled()) {
            logger.debug("Getting mo summary for application[" + appId + "]");
        }

        Query query = sessionFactory.getCurrentSession().createSQLQuery(GET_MO_SUMMARY).setString(0, appId);
        List summaryDataList = query.list();
        ReceivedMoSummary moSummary;

        if (summaryDataList == null || summaryDataList.size() == 0) {
            moSummary = new ReceivedMoSummary(appId, 0, 0);
        } else {

            Object[] summaryRow = (Object[]) summaryDataList.get(0);
            long count = summaryRow[1] == null ? 0 : ((java.math.BigInteger) summaryRow[1]).longValue();
            long updatedDate = summaryRow[2] == null ? 0 : ((java.sql.Timestamp) summaryRow[2]).getTime();

            moSummary = new ReceivedMoSummary(appId, count, updatedDate);

        }
        if (logger.isDebugEnabled()) {
            logger.debug("Mo summary [" + moSummary + "]");
        }
        return moSummary;
    }

    @Transactional(readOnly = true)
    public List<ApplicationImpl> findCpAppBySpid(String spId) {
        logger.debug("getting apps  for the sp-id [ "+spId+" ]");
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(FIND_CP_MANAGED_APP_BY_SPID);
        query.setString(0, spId);
        query.addEntity(ApplicationImpl.class);
        List<ApplicationImpl> appList = query.list();
        return appList;
    }

    @Transactional(readOnly = true)
    public List<Application> findExpiredAppName() throws ApplicationException {
        if (logger.isDebugEnabled()) {
            logger.debug("Application expiration operation started....");
        }
        if (logger.isDebugEnabled()) {
            logger.debug("Current Date [ " +  new DateTime().toDate() + " ]");
        }
        final Query query = sessionFactory.getCurrentSession().createQuery(FIND_EXPIRED_APP_NAME)
                .setTimestamp(0, new DateTime().toDate());
        List<Application> apps =(List<Application>)query.list();
        if (logger.isDebugEnabled()) {
            logger.debug("Number of applications to be expired [ " + apps.size() + " ]");
        }
        if (apps.size() > 0) {
            return apps;
        } else {
            throw new ApplicationException("No Expired application found for [" + new DateTime().toDate() + "]",
                    MessageFlowErrorReasons.APP_NOT_FOUND);
        }
    }

    private final String FIND_APP_NAME_BY_ID = "select app.appName from ApplicationImpl app where app.appId = ? ";

    @Transactional(readOnly = true)
    public String findAppNameByAppId(String appId) {
        if (logger.isDebugEnabled()) {
            logger.debug("Getting application name for appId[" + appId + "]");
        }
        final Query query = sessionFactory.getCurrentSession().createQuery(FIND_APP_NAME_BY_ID).setString(0, appId);
        List result = query.list();
        return result.size() > 0 ? result.get(0).toString() : "";
    }

//    private static final String FIND_CP_ID = "select cp_id from content_provider where content_provider.id = " +
//            "(select cpa.content_provider_id from application a left join content_provider_application cpa " +
//            "on a.id = cpa.applications_id where a.app_id = '?')";
//

    /**
     * Here i used two sqls for single work. It was due to hibernate issue. Hibernate gives syntax exception when using
     *  single sql.
     */
    private static final String FIND_TABLE_CP_ID = "select cpa.content_provider_id from application a left join content_provider_application cpa on a.id = cpa.applications_id where a.app_id = ?";
    private static final String FIND_CP_ID = "select cp_id from content_provider where content_provider.id = ?";
    private static final String FIND_SDP_APP_STATE_BY_ORCA_APP_ID = "select a.current_state from app a, orca_sdp_app_integration app_integration where a.app_id=app_integration.sdp_app_id and app_integration.orca_app_id = ?";

    @Transactional(readOnly = true)
    public String findCPId(String appId) {
        if (logger.isDebugEnabled()) {
            logger.debug("Getting content provider id using application id [" + appId + "]");
        }
        final Query table_cp_id_query = sessionFactory.getCurrentSession().createSQLQuery(FIND_TABLE_CP_ID);
        table_cp_id_query.setString(0, appId);
        List result1 = table_cp_id_query.list();


        final Query cp_id_query = sessionFactory.getCurrentSession().createSQLQuery(FIND_CP_ID);
        cp_id_query.setString(0, result1.get(0).toString());
        List result2 = cp_id_query.list();

        return result2.size() > 0 ? result2.get(0).toString() : "";
    }

    @Transactional(readOnly = true)
    public String findSdpAppState(String orcaAppId) {
        if (logger.isDebugEnabled()) {
            logger.debug("Getting SDP Application state info for Orca Application appId [ " + orcaAppId + "]");
        }
        final Query retreiveSdpAppStateQuery = sessionFactory.getCurrentSession().createSQLQuery(
                FIND_SDP_APP_STATE_BY_ORCA_APP_ID);
        retreiveSdpAppStateQuery.setString(0, orcaAppId);
        List result = retreiveSdpAppStateQuery.list();
        return result.size() > 0 ? result.get(0).toString() : "";
    }

    @Override
    public void updateAbuseReportSummary(String appId) {
        // TODO Auto-generated method stub

    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

}
