/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hsenidmobile.orca.orm.repository;

import hsenidmobile.orca.core.applications.alert.traffic.AlertAppTraffic;
import hsenidmobile.orca.core.repository.AppTrafficRepository;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * $LastChangedDate:$
 * $LastChangedBy:$
 * $LastChangedRevision:$
 */
public class AppTrafficRepositoryImpl implements AppTrafficRepository {

    private static final Logger logger = LoggerFactory.getLogger(AppTrafficRepositoryImpl.class);
    private SessionFactory sessionFactory;

    @Transactional
    public void createAlertAppTraffic(String appId) {
        logger.debug("Adding Trafiic data for the app id [{}] ", appId);
        if (!isAppEntryAvailable(appId)) {
            AlertAppTraffic appTrafficMo = new AlertAppTraffic(appId);
            try {
                sessionFactory.getCurrentSession().save(appTrafficMo);
            } catch (org.hibernate.NonUniqueObjectException e) {
                logger.error("Error while adding traffic data ", e);
                //do nothing
            } catch (org.hibernate.exception.ConstraintViolationException e) {
                logger.error("Error while adding traffic data ", e);
                //do nothing
            }
        }

    }

    @Transactional
    public void updateAlertAppThroughput(AlertAppTraffic alertAppTraffic) {
      sessionFactory.getCurrentSession().update(alertAppTraffic);
    }

    @Transactional
    public AlertAppTraffic findAlertAppTraffic(String appId) {
        final List<AlertAppTraffic> list = sessionFactory.getCurrentSession().createQuery(findAppHql).
                setString(0, appId).
                list();
        if(list != null && !list.isEmpty()) {
            return list.get(0);
        } else {
            throw new IllegalStateException("Alert Application found with no traffic data for app id [ " + appId + " ]");
        }
    }


    private static final String findAppHql =
            "select a from AlertAppTraffic a where a.appId = ? ";

    private boolean isAppEntryAvailable(String appId) {
        final List list = sessionFactory.getCurrentSession().createQuery(findAppHql).
                setString(0, appId).
                list();
        return list != null && !list.isEmpty();
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
