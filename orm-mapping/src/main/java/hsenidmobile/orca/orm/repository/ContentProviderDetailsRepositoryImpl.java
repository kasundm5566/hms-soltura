package hsenidmobile.orca.orm.repository;

import hsenidmobile.orca.core.model.ContentProvider;
import hsenidmobile.orca.core.repository.ContentProviderRepository;

import java.io.Serializable;

/**
 * $LastChangedDate: 2011-07-04 10:28:25 +0530 (Mon, 04 Jul 2011) $
 * $LastChangedBy: supunh $
 * $LastChangedRevision: 74666 $
 */
public class ContentProviderDetailsRepositoryImpl implements ContentProviderRepository, Serializable {

    private ContentProvider contentProvider = new ContentProvider();

    public void save(ContentProvider contentProvider) {
        throw new IllegalStateException("Saving Content Provider not allowed");
    }

    public void update(ContentProvider contentProvider) {
        throw new IllegalStateException("Updating Content Provider not allowed");
    }

    public ContentProvider findByName(String name) {
        return null;
    }

    public ContentProvider findByUsername(String name) {
        if (name.equals(this.contentProvider.getName()))
             return this.contentProvider;
        else
            throw new IllegalStateException("User invalid");
    }

    public ContentProvider findByCpId(String cpId) {
        return null;
    }

    public void updateSpStatus(ContentProvider contentProvider) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean isCpUserAllowedToManageRequestedApp(String userName, String appId) {
        return true;
    }

    public ContentProvider getContentProvider() {
        return contentProvider;
    }
}
