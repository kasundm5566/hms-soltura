/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.orm.repository;

import hsenidmobile.orca.core.model.InvalidRequest;
import hsenidmobile.orca.core.repository.InvalidRequestRepository;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class InvalidRequestRepositoryImpl implements InvalidRequestRepository {

	private static final Logger logger = Logger.getLogger(InvalidRequestRepositoryImpl.class);

	private static final String GET_INVALID_REQUEST_SUMMARY = "SELECT total_requests, last_update_time "
			+ "FROM invalid_request_summary WHERE app_id = ? ";

	private static final String INSERT_INVALID_REQUEST_SUMMARY = "INSERT INTO invalid_request_summary "
			+ "(app_id, total_requests, last_update_time) VALUES (?,1, ?) ";

	private static final String UPDATE_INVALID_REQUEST_SUMMARY = "UPDATE invalid_request_summary "
			+ "SET total_requests = (total_requests + 1), last_update_time = ? WHERE app_id = ?";

	private SessionFactory sessionFactory;

	@Override
	@Transactional
	public int getInvalidRequestSummary(String appId) {
		if (logger.isDebugEnabled()) {
			logger.debug("Getting invalid request summary for AppId[" + appId + "]");
		}

		Query query = sessionFactory.getCurrentSession().createSQLQuery(GET_INVALID_REQUEST_SUMMARY)
				.setString(0, appId);
		List summaryDataList = query.list();
		if (summaryDataList != null && summaryDataList.size() == 1) {
			Object[] summaryRow = (Object[]) summaryDataList.get(0);
			return (summaryRow[0] == null ? 0 : ((java.math.BigInteger) summaryRow[0]).intValue());
		} else {
			return 0;
		}

	}

	@Override
	@Transactional
	public void incrementInvalidRequestSummary(String appId) {
		if (logger.isDebugEnabled()) {
			logger.debug("Incrementing Invalid Request Summary for application[" + appId + "]");
		}
		Query updateQuery = sessionFactory.getCurrentSession().createSQLQuery(UPDATE_INVALID_REQUEST_SUMMARY);
		updateQuery.setDate(0, new Date());
		updateQuery.setString(1, appId);
		int updateRowCount = updateQuery.executeUpdate();
		if (logger.isDebugEnabled()) {
			logger.debug("Updated row count[" + updateRowCount + "]");
		}
		if (updateRowCount == 0) {
			if (logger.isDebugEnabled()) {
				logger.debug("Inserting since updated rows = 0");
			}
			Query insertQuery = sessionFactory.getCurrentSession().createSQLQuery(INSERT_INVALID_REQUEST_SUMMARY);
			insertQuery.setString(0, appId);
			insertQuery.setDate(1, new Date());
			insertQuery.executeUpdate();
		}

	}

	@Transactional
	public void delete(InvalidRequest invalidRequest) {
		if (logger.isDebugEnabled()) {
			logger.debug("Deleting [" + invalidRequest + "]");
		}
		sessionFactory.getCurrentSession().delete(invalidRequest);
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

}
