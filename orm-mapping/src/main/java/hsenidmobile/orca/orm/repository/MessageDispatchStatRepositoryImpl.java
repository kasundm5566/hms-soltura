/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.orm.repository;

import hsenidmobile.orca.core.model.DispatchSummary;
import hsenidmobile.orca.core.repository.MessageDispatchStatRepository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class MessageDispatchStatRepositoryImpl implements MessageDispatchStatRepository {

	private static final Logger logger = Logger.getLogger(MessageDispatchStatRepositoryImpl.class);

	private static final String GET_DISPATCH_SUMMARY = "SELECT status_code,total_count, last_update_time "
			+ "FROM message_dispatching_summary WHERE app_id = ? ";

	private static final String INSERT_DISPATCH_SUMMARY = "INSERT INTO message_dispatching_summary "
			+ "(app_id, status_code,total_count, last_update_time) VALUES (?,?,1, ?) ";

	private static final String UPDATE_DISPATCH_SUMMARY = "UPDATE message_dispatching_summary SET total_count = (total_count + 1), last_update_time = ?"
			+ " WHERE  app_id = ? AND status_code = ?";

	private SessionFactory sessionFactory;

	@Transactional
	public void updateDispatchSummary(String appId, String statusCode) {
		if (logger.isDebugEnabled()) {
			logger.debug("Update MessageDispatchSummary appId[" + appId + "] statusCode[" + statusCode + "]");
		}

		SQLQuery updateQuery = sessionFactory.getCurrentSession().createSQLQuery(UPDATE_DISPATCH_SUMMARY);
		updateQuery.setDate(0, new Date());
		updateQuery.setString(1, appId);
		updateQuery.setString(2, statusCode);
		int updatedRows = updateQuery.executeUpdate();
		if (logger.isDebugEnabled()) {
			logger.debug("Updated row count[" + updatedRows + "]");
		}
		if (updatedRows == 0) {
			if (logger.isDebugEnabled()) {
				logger.debug("Inserting since no row was updated.");
			}
			SQLQuery insertQuery = sessionFactory.getCurrentSession().createSQLQuery(INSERT_DISPATCH_SUMMARY);
			insertQuery.setString(0, appId);
			insertQuery.setString(1, statusCode);
			insertQuery.setDate(2, new Date());
			insertQuery.executeUpdate();
		}
	}

	@Transactional(readOnly = true)
	public List<DispatchSummary> getDispatchSummary(String appId) {
		if (logger.isDebugEnabled()) {
			logger.debug("Finding dispatch summary for application[" + appId + "]");
			;
		}
		Query query = sessionFactory.getCurrentSession().createSQLQuery(GET_DISPATCH_SUMMARY).setString(0, appId);
		List summaryDataList = query.list();
		if (summaryDataList == null || summaryDataList.size() == 0) {
			if (logger.isDebugEnabled()) {
				logger.debug("No Dispatch Summary data found for application[" + appId + "]");
			}
			return Collections.EMPTY_LIST;
		}

		List<DispatchSummary> dispatchSummaries = new ArrayList<DispatchSummary>(summaryDataList.size());

		for (Object object : summaryDataList) {
			Object[] summaryRow = (Object[]) object;
			String statusCode = (String) summaryRow[0];
			long count = summaryRow[1] == null ? 0 : ((java.math.BigInteger) summaryRow[1]).longValue();
			Date updatedDate = summaryRow[2] == null ? null : new Date(((java.sql.Timestamp) summaryRow[2]).getTime());

			DispatchSummary ds = new DispatchSummary(appId, statusCode, count);
			if (logger.isDebugEnabled()) {
				logger.debug("Found dispatch summary data[" + ds + "]");
			}
			dispatchSummaries.add(ds);
		}

		return dispatchSummaries;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
}
