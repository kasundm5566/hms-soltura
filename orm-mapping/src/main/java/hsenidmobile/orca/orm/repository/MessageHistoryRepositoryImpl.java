/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hsenidmobile.orca.orm.repository;

import hsenidmobile.orca.core.applications.alert.AlertContentRelease;
import hsenidmobile.orca.core.applications.exception.ApplicationException;
import hsenidmobile.orca.core.applications.subscription.SubscriptionContentRelease;
import hsenidmobile.orca.core.applications.voting.VotingService;
import hsenidmobile.orca.core.model.Application;
import hsenidmobile.orca.core.model.Content;
import hsenidmobile.orca.core.model.MessageHistory;
import hsenidmobile.orca.core.model.ServiceTypes;
import hsenidmobile.orca.core.report.MessageHistoryRecord;
import hsenidmobile.orca.core.repository.AppRepository;
import hsenidmobile.orca.core.repository.MessageHistoryRepository;
import hsenidmobile.orca.core.repository.SdpApplicationRepository;
import hsenidmobile.orca.rest.sdp.broadcast.transport.BroadcastStatusDetailRequestSender;
import org.apache.cxf.jaxrs.client.ClientWebApplicationException;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.*;

import static hsenidmobile.orca.core.model.Event.*;
import static hsenidmobile.orca.core.model.ServiceTypes.*;
import static hsenidmobile.orca.rest.common.util.RestApiKeys.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class MessageHistoryRepositoryImpl implements MessageHistoryRepository {

    private static final Logger logger = Logger.getLogger(MessageHistoryRepositoryImpl.class);
    final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    private SessionFactory sessionFactory;
    @Autowired
    private SdpApplicationRepository sdpApplicationRepository;
    @Autowired
    private AppRepository appRepository;
    @Autowired
    protected BroadcastStatusDetailRequestSender broadcastStatusDetailRequestSender;

    private static final String GET_MATCHING_MESSAGE_HISTORY_BY_APPIDS_BY_DURATION = "from MessageHistory " +
            "where applicationId like ? and messageTimeStamp >= ? and messageTimeStamp <= ?";

    private static final String GET_MATCHING_MESSAGE_HISTORY_BY_APPIDS = "from MessageHistory where applicationId like ?";

    private static final String GET_MATCHING_VOTING_RESULT_HISTORY_COUNT_BY_APP_ID = "SELECT count(*) FROM vote v " +
            "INNER JOIN candidate_vote cv  on v.id=cv.vote_id " +
            "INNER JOIN application app on v.service_id=app.service_id " +
            "INNER JOIN sub_category sc on sc.id = cv.candidate_id " +
            "WHERE app.app_id = ? and app.service= \"VOTING\"" ;

    private static final String GET_MATCHING_VOTING_RESULT_HISTORY_BY_DURATION =
            GET_MATCHING_VOTING_RESULT_HISTORY_COUNT_BY_APP_ID +  " and v.received_time >= ? and v.received_time <= ? ";

    private static final String GET_MATCHING_REQUEST_MESSAGE_COUNT_BY_APP_ID = "SELECT count(*) " +
            "FROM request_service_data rsd WHERE rsd.appId = ? ";

    private static final String GET_MATCHING_REQUEST_MESSAGES_BY_DURATION =
            GET_MATCHING_REQUEST_MESSAGE_COUNT_BY_APP_ID + " and rsd.received_time >= ? and rsd.received_time <= ?";

    private static final String GET_SERVICE_TYPE_BY_APP_ID="SELECT app.service, app.app_name FROM application app " +
            "where app.app_id= ?";

    private static final String GET_VOTING_RESULT = "SELECT MAX(vrs.last_update_time) "
            + "FROM voting_result_summary vrs WHERE vrs.votelet_id = ?";

    private static final String GET_MATCHING_REQUEST_MESSAGES_BY_APP_ID = "SELECT MAX(last_modified_time) " +
            "FROM request_service_data rsd WHERE rsd.appId = ? ";

    private static final String GET_ALERT_CONTENT_RELEASED_BY_APP_ID = "FROM AlertContentRelease acr WHERE acr.appId = ? ";

    private static final String GET_SUBSCRIPTION_CONTENT_RELEASED_BY_APP_ID = "FROM SubscriptionContentRelease WHERE appId = ? ";

    private static final String GET_ALERT_MESSAGEIDS_BY_APP_ID_DURATION_ON = "FROM AlertContentRelease acr " +
            "WHERE acr.appId = ? and acr.lastModifiedTime >= ? and acr.lastModifiedTime <= ?";

    private static final String GET_SUBSCRIPTION_MESSAGEIDS_BY_APP_ID_DURATION_ON = "FROM SubscriptionContentRelease scr " +
            "WHERE scr.appId = ? and scr.lastModifiedTime >= ? and scr.lastModifiedTime <= ?";

    private static final String GET_MATCHING_CONTENT_BY_ALERT_ID = "SELECT * FROM content cn " +
            "INNER JOIN alert_content_release_content a on cn.id=a.contents_id WHERE a.alert_content_release_id = ? ";

    private static final String GET_MATCHING_CONTENT_BY_SUBSCRIPTION_ID = "SELECT * FROM content cn " +
            "INNER JOIN subscription_content_release_content s on s.contents_id=cn.id WHERE s.subscription_content_release_id = ? ";

    @Transactional
    public void saveEvent(MessageHistory messageHistory) {
        messageHistory.setMessageTimeStamp((new Date()).getTime());

        try {
            sessionFactory.getCurrentSession().save(messageHistory);
        } catch (HibernateException e) {
            logger.warn("Error while saving MessageHistory", e);
        }
    }

    /**
     * This will generates the Message History Report for a given set of application IDs
     * @param appIds
     * @param from
     * @param to
     * @param filterByDuartion
     * @return
     * @throws ApplicationException
     */
    @Transactional
    public List<MessageHistory> getMatchingMessageHistoryForCp(List<String> appIds, long from, long to,
                                                               boolean filterByDuartion) throws ApplicationException {
        Query query;
        String serviceType = null;
        String appName = null;
        List<MessageHistory> messageHistoryList = new ArrayList<MessageHistory>();

        if (logger.isDebugEnabled()) {
            logger.debug("Searching for matching Message History records in database with appids = " + appIds);
        }
        for (String appId : appIds) {
            logger.info("Generating Message History Report for [ " + appId + " ] Started");
            if (logger.isDebugEnabled()) {
                logger.debug("Finding the appName and the Service Type for the appID [ " + appId + " ]" );
            }
            try {
                query = sessionFactory.getCurrentSession().createSQLQuery(GET_SERVICE_TYPE_BY_APP_ID).
                        setString(0, appId);
                List result = query.list();
                for (Object o : result) {
                    Object[] summaryRow = (Object[]) o;
                    serviceType = (String) summaryRow[0];
                    appName = (String) summaryRow[1];
                }
                if (logger.isDebugEnabled()) {
                    logger.debug("Application Details for the appID [ " + appId + " ] Found - App NAme [ " + appName
                            +" ] Servic Type [ " + serviceType + " ]" );
                }
            } catch (Exception e) {
                logger.error("Error occured while searching the aplication details for the given appId [ "
                        + appId + " ]");
            }
            handleMessageHistoryReportGenerationByServiceType(appIds, from, to, filterByDuartion, serviceType,
                    appName, messageHistoryList, appId);
        }
        return messageHistoryList;
    }

    private void handleMessageHistoryReportGenerationByServiceType(List<String> appIds, long from, long to,
                                                                   boolean filterByDuartion,
                                                                   String serviceType, String appName,
                                                                   List<MessageHistory> messageHistoryList,
                                                                   String appId) throws ApplicationException {
        switch(ServiceTypes.valueOf(serviceType)) {
            case VOTING:
                if(filterByDuartion) {
                    generateVotingMessageHistoryReportByDuration(messageHistoryList, from, to, appId, appName);
                } else {
                    generateVotingMessageHistoryReportByAppId(messageHistoryList, appId, appName);
                }
                break;
            case REQUEST:
                if(filterByDuartion) {
                    generateRequestShowMessageHistoryReportByDuration(messageHistoryList, from, to, appId, appName);
                } else {
                    generateRequestShowMessageHistoryReportByAppId(messageHistoryList, appId, appName);
                }
                break;
            case ALERT:
                getMoSupportedMessageHistoryContent(appIds, from, to, filterByDuartion, messageHistoryList, appId, ALERT);
                break;
            case SUBSCRIPTION:
                getMoSupportedMessageHistoryContent(appIds, from, to, filterByDuartion, messageHistoryList, appId, SUBSCRIPTION);
                break;
            default:
                throw new IllegalStateException("Service Type Not Defined.");
        }
    }

    private void getMoSupportedMessageHistoryContent(List<String> appIds, long from, long to, boolean filterByDuartion,
                                                     List<MessageHistory> messageHistoryList, String appId, ServiceTypes serviceType) {
        if(filterByDuartion) {
            generateSubscriptionMessageHistoryReportByDuration(from, to, messageHistoryList, appId, serviceType);
        } else {
            generateSubscriptionMessageHistoryReportByAppId(messageHistoryList, appId, serviceType);
        }

        List contentReleasedList = getMessageIdList(filterByDuartion, from, to, appId, serviceType);

        try {
            updateBroadcastMessageStatusDetails(appIds, messageHistoryList, contentReleasedList, appId);
        } catch (Exception e) {
            logger.error("Error while updating broadcast message statistics", e);
        }
    }

    private List<Object> getMessageIdList(boolean filterByDuartion, long from, long to, String appId, ServiceTypes serviceType) {
        if(filterByDuartion){
            java.sql.Date fromDate = new java.sql.Date(from);
            java.sql.Date toDate = new java.sql.Date(to);

            switch(ServiceTypes.valueOf(serviceType.name())) {
                case ALERT:
                    Query query1 = sessionFactory.getCurrentSession().createQuery(GET_ALERT_MESSAGEIDS_BY_APP_ID_DURATION_ON).
                            setString(0, appId);
                    query1.setDate(1, fromDate);
                    query1.setDate(2, toDate);
                    List<Object> result1 = query1.list();
                    return result1;
                case SUBSCRIPTION:
                    Query query2 = sessionFactory.getCurrentSession().createQuery(GET_SUBSCRIPTION_MESSAGEIDS_BY_APP_ID_DURATION_ON).
                            setString(0, appId);
                    query2.setDate(1, fromDate);
                    query2.setDate(2, toDate);
                    List<Object> result2 = query2.list();
                    return result2;
                default:
                    throw new IllegalStateException("Service Type Not Defined.");
            }

        } else {
            switch(ServiceTypes.valueOf(serviceType.name())) {
                case ALERT:
                    Query query1 = sessionFactory.getCurrentSession().createQuery(GET_ALERT_CONTENT_RELEASED_BY_APP_ID).
                            setString(0, appId);
                    List<Object> result1 = query1.list();
                    return result1;
                case SUBSCRIPTION:
                    Query query2 = sessionFactory.getCurrentSession().createQuery(GET_SUBSCRIPTION_CONTENT_RELEASED_BY_APP_ID).
                            setString(0, appId);
                    List<Object> result2 = query2.list();
                    return result2;
                default:
                    throw new IllegalStateException("Service Type Not Defined.");
            }
        }
    }

    private void generateSubscriptionMessageHistoryReportByAppId(List<MessageHistory> messageHistoryList,
                                                                 String appId, ServiceTypes serviceType) {
        Query query;
        query = sessionFactory.getCurrentSession().createQuery(GET_MATCHING_MESSAGE_HISTORY_BY_APPIDS)
                .setString(0, "%" + appId + "%");
        updateServiceTypeInfo(query.list(), serviceType, messageHistoryList);
    }

    private void generateSubscriptionMessageHistoryReportByDuration(long from, long to,
                                                                    List<MessageHistory> messageHistoryList,
                                                                    String appId, ServiceTypes serviceType) {
        Query query;
        query = sessionFactory.getCurrentSession().createQuery(
                GET_MATCHING_MESSAGE_HISTORY_BY_APPIDS_BY_DURATION);

        query.setString(0, "%" + appId + "%");
        query.setLong(1, (from - 1));
        query.setLong(2, to + 86399000l);
        messageHistoryList.addAll(query.list());
        updateServiceTypeInfo(messageHistoryList, serviceType, messageHistoryList);
    }

    private void updateServiceTypeInfo(List<MessageHistory> searchResult, ServiceTypes serviceType, List<MessageHistory>
            messageHistoryList) {
        for (MessageHistory messageHistory : searchResult) {
            messageHistory.setMessageHistoryRecord(new MessageHistoryRecord(serviceType.name()));
        }
        messageHistoryList.addAll(searchResult);
    }

    private void generateRequestShowMessageHistoryReportByAppId(List<MessageHistory> messageHistoryList,
                                                                String appId, String appName) {
        Query query;
        query = sessionFactory.getCurrentSession().createSQLQuery(GET_MATCHING_REQUEST_MESSAGE_COUNT_BY_APP_ID)
                .setString(0, appId);
        List results = query.list();
        if (results != null && validateList(results)) {
            generateRequestShowHistoryReportEntry(appId, appName, (BigInteger)results.get(0), messageHistoryList);
        }
    }

    private void generateRequestShowMessageHistoryReportByDuration(List<MessageHistory> messageHistoryList,
                                                                   long from, long to, String appId, String appName) {
        Query query;
        query = sessionFactory.getCurrentSession().createSQLQuery(GET_MATCHING_REQUEST_MESSAGES_BY_DURATION)
                .setString(0, appId);
        query.setLong(1, (from -1));
        query.setLong(2, to + 86399000l);
        List results = query.list();
        if (results != null && validateList(results)) {
            generateRequestShowHistoryReportEntry(appId, appName, (BigInteger)results.get(0), messageHistoryList);
        }
    }

    private void generateVotingMessageHistoryReportByAppId(List<MessageHistory> messageHistoryList,
                                                           String appId, String appName) throws ApplicationException {
        Query query;
        query = sessionFactory.getCurrentSession().createSQLQuery(GET_MATCHING_VOTING_RESULT_HISTORY_COUNT_BY_APP_ID)
                .setString(0, appId);
        List results = query.list();
        if (results != null && validateList(results)) {
            generateVotingHistoryReportEntry(appId, appName, (BigInteger)results.get(0), messageHistoryList);
        }
    }

    private void generateVotingMessageHistoryReportByDuration(List<MessageHistory> messageHistoryList, long from,
                                                              long to, String appId, String appName)
            throws ApplicationException {
        Query query;
        query = sessionFactory.getCurrentSession().createSQLQuery(GET_MATCHING_VOTING_RESULT_HISTORY_BY_DURATION)
                .setString(0, appId);
        query.setLong(1, from -1);
        query.setLong(2, to + 86399000l);
        List results = query.list();
        if (results != null && validateList(results)) {
            generateVotingHistoryReportEntry(appId, appName, (BigInteger)results.get(0), messageHistoryList);
        }
    }

    //return true only if list have
    private boolean validateList(List results) {
        if (! results.isEmpty()) {
            if (results.size() == 1) {
                return ((BigInteger) results.get(0)).intValue() != 0;
            }
        }
        return true;
    }

    private void generateVotingHistoryReportEntry(String appId, String appName, BigInteger totalMsgCount,
                                                  List<MessageHistory> messageHistoryList) throws ApplicationException {
        MessageHistory messageHistory = new MessageHistory();
        messageHistory.setApplicationName(appName);
        messageHistory.setMessageHistoryRecord(createVotingResultHistoryMessage(totalMsgCount));
        messageHistory.setMessageTimeStamp(getVotingLastUpdatedTime(appId).getTime());
        messageHistory.setEvent(VOTES_RECEIVED);
        messageHistoryList.add(messageHistory);
    }

    private Date getVotingLastUpdatedTime(String appId) throws ApplicationException {
        Application app = appRepository.findAppByAppId(appId);
        VotingService votingService = (VotingService) app.getService();
        Long voteletId = votingService.getId();
        Query query = sessionFactory.getCurrentSession().createSQLQuery(GET_VOTING_RESULT).setLong(0, voteletId);
        List summaryDataList = query.list();
        Object lastUpdateTime = summaryDataList.toArray()[0];
        Date updatedDate = (lastUpdateTime == null) ? null : new Date(((java.sql.Timestamp) lastUpdateTime).getTime());
        return updatedDate;
    }

    private void generateRequestShowHistoryReportEntry(String appId, String appName, BigInteger totalMsgCount,
                                                       List<MessageHistory> messageHistoryList) {
        MessageHistory messageHistory = new MessageHistory();
        messageHistory.setApplicationName(appName);
        messageHistory.setMessageHistoryRecord(createRequestShowHistoryMessage(totalMsgCount));
        messageHistory.setMessageTimeStamp(getRequestLastUpdatedTime(appId).getTime());
        messageHistory.setEvent(REQUESTS_RECEIVED);
        messageHistoryList.add(messageHistory);
    }

    private Date getRequestLastUpdatedTime(String appId) {
        Query query = sessionFactory.getCurrentSession().createSQLQuery(GET_MATCHING_REQUEST_MESSAGES_BY_APP_ID).
                setString(0, appId);
        List summaryDataList = query.list();
        Object lastModifiedTime = summaryDataList.toArray()[0];
        Date updatedDate = (lastModifiedTime == null) ? null : new Date(((java.sql.Timestamp) lastModifiedTime).getTime());
        return updatedDate;
    }

    //todo integrate with braodcast api to get statistics
    @Transactional
    public void updateBroadcastMessageStatusDetails(final List<String> appIds, List<MessageHistory> messageHistoryList,
                                                    final List<Object> contentReleasedList, String appId)
            throws ApplicationException {

        try {
            final String sdpAppId = sdpApplicationRepository.getSdpAppLoginDataForOrcaApp(appId).getAppId();
            final String appName = appRepository.findAppNameByAppId(appId);
            if (messageHistoryList.size() > 0) {
                for (Object contentReleased : contentReleasedList) {
                    Map broadcastStatusResponse;
                    MessageHistory history = null;
                    if (contentReleased.getClass().equals(AlertContentRelease.class)){
                        AlertContentRelease alertContent = (AlertContentRelease) contentReleased;
                        if (logger.isDebugEnabled()) {
                            logger.debug("Alert broadcast messageId found[" +alertContent.getBcMessageId() + "]");
                        }
                        Content messageContent = getAlertMessageContent(alertContent.getId());
                        broadcastStatusResponse = getBroadcastDetails(alertContent.getBcMessageId(), sdpAppId);
                        history = createHistoryEntry(appId, appName, alertContent.getLastModifiedTime().getTime(),
                                messageContent.getContent().getMessage(),broadcastStatusResponse);
                    } else if (contentReleased.getClass().equals(SubscriptionContentRelease.class)){
                        SubscriptionContentRelease subscriptionContent = (SubscriptionContentRelease) contentReleased;
                        if (logger.isDebugEnabled()) {
                            logger.debug("Subscription broadcast messageId found[" +subscriptionContent.getBcMessageId() + "]");
                        }
                        Content messageContent = getSubscriptionMessageContent(subscriptionContent.getId());
                        broadcastStatusResponse = getBroadcastDetails(subscriptionContent.getBcMessageId(), sdpAppId);
                        history = createHistoryEntry(appId, appName, subscriptionContent.getLastModifiedTime().getTime(),
                                messageContent.getContent().getMessage(),broadcastStatusResponse);
                    }
                    messageHistoryList.add(history);
                }
            }
        } catch (ClientWebApplicationException e){
            throw e;
        } catch (Exception e) {
            logger.error("Error while getting BcRequest data for appId [ " + appId + " ]", e);
        }
    }

    private MessageHistory createHistoryEntry(String appId, String appName, long time, String message,Map broadcastStatusResponse) {
        final MessageHistory history = new MessageHistory();
        history.setApplicationId(appId);
        history.setApplicationName(appName);
        history.setEvent(CONTENT_SEND);
        history.setMessageTimeStamp(time);
        history.setMessageHistoryRecord(createBcStatusMessage(message, time, broadcastStatusResponse));
        return history;
    }

    private Map getBroadcastDetails(long bcMessageId, String sdpAppId) {
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put(APPLICATION_ID, sdpAppId);
        parameters.put(MESSAGE_ID,bcMessageId);
        //todo passward hardcoded need to check
        parameters.put(PASSWORD,"test");
        try {
            Map bcRequestStatistics = broadcastStatusDetailRequestSender.getBroadCastDetails(parameters);
            if (logger.isDebugEnabled()) {
                logger.debug("BcRequestDetails[" + bcRequestStatistics + "]");
            }
            return bcRequestStatistics;
        } catch (Exception e) {
            logger.warn("No statistics data found for broadcast messageID["
                    + bcMessageId+ "][" + e.getMessage() + "]");
            throw new RuntimeException("EXception in retriieving broadcast details");
        }
    }

    private Content getAlertMessageContent(Long id) {
        SQLQuery query;
        query = sessionFactory.getCurrentSession().createSQLQuery(GET_MATCHING_CONTENT_BY_ALERT_ID);
        query.setLong(0, id);
        query.addEntity(Content.class);
        List<Content> result = query.list();
        if (result != null){
            return result.get(0);
        }
        return null;
    }
    private Content getSubscriptionMessageContent(Long id) {
        SQLQuery query;
        query = sessionFactory.getCurrentSession().createSQLQuery(GET_MATCHING_CONTENT_BY_SUBSCRIPTION_ID);
        query.setLong(0, id);
        query.addEntity(Content.class);
        List<Content> result = query.list();
        if (result != null){
            return result.get(0);
        }
        return null;
    }

    private MessageHistoryRecord createBcStatusMessage(String message, long time,Map bcRequestStatistics) {

        MessageHistoryRecord messageHistoryRecord = new MessageHistoryRecord();
        messageHistoryRecord.setMessageContent(message);
        messageHistoryRecord.setMessageSendingStatus((String) bcRequestStatistics.get(BROADCAST_STATUS));
        messageHistoryRecord.setSuccessMsgCount(Long.parseLong((String) bcRequestStatistics.get(TOTAL_ERRORS)));
        messageHistoryRecord.setFailedMsgCount(Long.parseLong((String) bcRequestStatistics.get(TOTAL_SENT)));
        messageHistoryRecord.setServiceType(BROADCAST.name());
        return messageHistoryRecord;
    }

    private MessageHistoryRecord createVotingResultHistoryMessage(BigInteger totalMsgCount) {
        MessageHistoryRecord messageHistoryRecord = new MessageHistoryRecord();
        messageHistoryRecord.setTotalMsgCount(totalMsgCount);
        messageHistoryRecord.setServiceType(VOTING.name());
        return messageHistoryRecord;
    }

    private MessageHistoryRecord createRequestShowHistoryMessage(BigInteger totalMsgCount) {
        MessageHistoryRecord messageHistoryRecord = new MessageHistoryRecord();
        messageHistoryRecord.setTotalMsgCount(totalMsgCount);
        messageHistoryRecord.setServiceType(REQUEST.name());
        return messageHistoryRecord;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

}


