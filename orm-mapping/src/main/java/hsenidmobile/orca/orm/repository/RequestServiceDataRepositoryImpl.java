/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hsenidmobile.orca.orm.repository;

import hsenidmobile.orca.core.applications.request.RequestService;
import hsenidmobile.orca.core.model.Application;
import hsenidmobile.orca.core.repository.RequestServiceDataRepository;
import hsenidmobile.orca.core.applications.request.RequestServiceData;
import hsenidmobile.orca.core.applications.exception.RequestServiceException;
import hsenidmobile.orca.core.flow.MessageFlowErrorReasons;
import hsenidmobile.orca.core.model.Msisdn;

import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

/**
 * $LastChangedDate$
 * $LastChangedBy$ $LastChangedRevision$
 */
public class RequestServiceDataRepositoryImpl implements RequestServiceDataRepository {

    private static final Logger logger = Logger.getLogger(RequestServiceDataRepositoryImpl.class);

    private static final String DYNAMIC_CONDITION_VALUES = "dynamicClause";
    private final String REPEATED_STATEMENT = "rsd.sub_category = ";
    private final String QUERY_CONNECTOR = "OR";
    private final String QUERY_SEPARATOR = " ";

    private SessionFactory sessionFactory;

    private static final String FIND_All_REQUEST_SERVICE_DATA_FOR_SERVICE = "SELECT rsd.address, rsd.operator,"
            + " rsd.received_message, rsd.received_time, rsd.messageId, rsd.isRead, sc.keyword from request_service_data rsd "
            + "INNER JOIN sub_category sc on rsd.sub_category = sc.id "
            + "WHERE rsd.appid= ? ORDER BY rsd.received_time desc";

    private static final String FIND_REQUEST_SERVICE_DATA_FOR_SERVICE = FIND_All_REQUEST_SERVICE_DATA_FOR_SERVICE
            + " limit ?, ?";

    private static final String FIND_ALL_REQUEST_SERVICE_DATA_WITH_SUB_CATEGORY = "SELECT rsd.address, rsd.operator,"
            + " rsd.received_message, rsd.received_time, rsd.messageId, rsd.isRead, sc.keyword from request_service_data rsd "
            + "INNER JOIN sub_category sc on rsd.sub_category = sc.id WHERE " + DYNAMIC_CONDITION_VALUES
            + " AND rsd.appid= ? ORDER BY rsd.received_time desc";

    private static final String FIND_REQUEST_SERVICE_DATA_FOR_SUB_CATEGORY = FIND_ALL_REQUEST_SERVICE_DATA_WITH_SUB_CATEGORY
            + " limit ?, ?";

    private static final String FIND_BY_MESSAGE_ID = "from RequestServiceData where messageId = ?";

    private static final String GET_ALL_MESSAGE_COUNT_FOR_SERVICE = "SELECT COUNT(*)"
            + "FROM request_service_data rsd WHERE rsd.appId = ? ";

    private static final String FIND_APPLICATION_BY_APP_ID = "from ApplicationImpl where appId = ?";

    private static final String FIND_RESPONSE_MESSAGE_BY_REQUEST_ID = "SELECT rslm.responseMessages_id FROM request_service_localized_message rslm "
            + " WHERE rslm.request_service_id = ? ";

    private static final String FIND_RESPONSE_MESSAGE_BY_ID = "SELECT message FROM LocalizedMessage "
            + " WHERE id = ? ";

    @Override
    @Transactional
    public void add(RequestServiceData requestServiceData) throws RequestServiceException {
        if (logger.isDebugEnabled()) {
            logger.debug("Added Request Message to repository [" + requestServiceData + "]");
        }
        sessionFactory.getCurrentSession().save(requestServiceData);
    }

    /**
     * Returns an ordered list of request messages for given subcategories.
     *
     * @param appId
     * @param selectedSubCategoryArray
     * @return
     * @throws RequestServiceException
     */
    @Override
    @Transactional(readOnly = true)
    public List<RequestServiceData> getAllRequestMessagesBySubCategory(String appId, String[] selectedSubCategoryArray)
            throws RequestServiceException {
        if (logger.isDebugEnabled()) {
            logger.debug("Retrieving the received request messages for sub_category_id [" + selectedSubCategoryArray
                    + "] " + "for app_id [" + appId + "]");
        }
        StringBuilder sb = getUpdatedQueryWithSelectedCategories(selectedSubCategoryArray);
        String formattedQuery = FIND_ALL_REQUEST_SERVICE_DATA_WITH_SUB_CATEGORY.replace(DYNAMIC_CONDITION_VALUES,
                sb.toString());
        Query query = sessionFactory.getCurrentSession().createSQLQuery(formattedQuery);
        query.setString(0, appId);
        if (logger.isDebugEnabled()) {
            logger.debug("Formatted Query [ " + query.toString() + " ]");
        }
        List requestMessageList = query.list();
        List<RequestServiceData> requestServiceDataResult = processRequestServiceDataInfo(requestMessageList);
        return requestServiceDataResult;
    }

    /**
     * This will format the query with the selected subcategory ids prior to the
     * query execution.
     *
     * @param selectedSubCategoryArray
     * @return
     */
    private StringBuilder getUpdatedQueryWithSelectedCategories(String[] selectedSubCategoryArray) {
        StringBuilder sb = new StringBuilder();
        int arrayLength = selectedSubCategoryArray.length;
        int counter = 1;
        for (String s : selectedSubCategoryArray) {
            if (counter == arrayLength) {
                sb.append(REPEATED_STATEMENT).append(s);
            } else {
                sb.append(REPEATED_STATEMENT).append(s).append(QUERY_SEPARATOR).append(QUERY_CONNECTOR)
                        .append(QUERY_SEPARATOR);
            }
            counter++;
        }
        return sb;
    }

    @Override
    @Transactional(readOnly = true)
    public List<RequestServiceData> getAllRequestMessagesForService(String appId, long requestServiceId)
            throws RequestServiceException {
        if (logger.isDebugEnabled()) {
            logger.debug("Retreving the received request messages for service_id [" + requestServiceId + "]"
                    + " for app_id [" + appId + "]");
        }
        Query query = sessionFactory.getCurrentSession().createSQLQuery(FIND_All_REQUEST_SERVICE_DATA_FOR_SERVICE);
        query.setLong(0, requestServiceId);
        query.setString(1, appId);
        List requestMessageList = query.list();
        List<RequestServiceData> requestServiceDataResult = processRequestServiceDataInfo(requestMessageList);
        return requestServiceDataResult;
    }

    @Transactional(readOnly = true)
    public List<RequestServiceData> getRequestMessages(String appId, long requestServiceId, int start, int end)
            throws RequestServiceException {
        final Application application = (Application) sessionFactory.getCurrentSession()
                .createQuery(FIND_APPLICATION_BY_APP_ID).setString(0, appId).list().get(0);
        return getRequestMessages(appId, ((RequestService) application.getService()), start, end);
    }

    @Override
    @Transactional(readOnly = true)
    public List<RequestServiceData> getRequestMessages(String appId, RequestService requestService, int start, int end)
            throws RequestServiceException {
        if (requestService.isSubCategoriesSupported()) {
            if (logger.isDebugEnabled()) {
                logger.debug("Getting request messages for the application which contains sub categories");
            }
            return fetchDataForAllCategories(appId, start, end);

        } else {
            if (logger.isDebugEnabled()) {
                logger.debug("Getting request messages for the application which doesn't contain sub categories");
            }
            Criteria criteria = sessionFactory.getCurrentSession().createCriteria(RequestServiceData.class)
                    .add(Restrictions.eq("appId", appId)).addOrder(Order.desc("receivedTime")).setFirstResult(start)
                    .setMaxResults(end - start);
            List list = criteria.list();
            if (logger.isDebugEnabled()) {
                logger.debug(" Returned record size [ " + list.size() + " ]");
            }

            for (Object rsd : list) {
                boolean commonRespAvailable = checkCommonResponseAvailability("" + appId);
                logger.debug("COMMON RESPONSE AVAILABILITY : [" + commonRespAvailable + "]");
                ((RequestServiceData)rsd).setCommonResponseAvailable(commonRespAvailable);
            }


            return list;
        }
    }

    private List<RequestServiceData> fetchDataForAllCategories(final String appId, final int start, final int end) {
        if (logger.isDebugEnabled()) {
            logger.debug("Retrieving the received request messages for App[" + appId + "]");
        }
        Query query = sessionFactory.getCurrentSession().createSQLQuery(FIND_REQUEST_SERVICE_DATA_FOR_SERVICE);
        query.setString(0, appId);
        query.setInteger(1, start);
        query.setInteger(2, end);
        List requestMessageList = query.list();
        List<RequestServiceData> requestServiceDataResult = processRequestServiceDataInfo(requestMessageList);
        return requestServiceDataResult;
    }

    @Override
    @Transactional(readOnly = true)
    public List<RequestServiceData> getRequestMessagesBySubCategory(String appId, long requestsubCategoryId, int start,
                                                                    int end) throws RequestServiceException {
        if (logger.isDebugEnabled()) {
            logger.debug("Retreving the received request messages for sub_category_id [" + requestsubCategoryId + "]"
                    + " for app_id [" + appId + "]");
        }
        Query query = sessionFactory.getCurrentSession().createSQLQuery(FIND_REQUEST_SERVICE_DATA_FOR_SUB_CATEGORY);
        query.setLong(0, requestsubCategoryId);
        query.setString(1, appId);
        query.setInteger(2, start);
        query.setInteger(3, end);
        List requestMessageList = query.list();
        List<RequestServiceData> requestServiceDataResult = processRequestServiceDataInfo(requestMessageList);
        return requestServiceDataResult;
    }

    @Override
    @Transactional(readOnly = true)
    public RequestServiceData findRequestServiceData(long messageId) throws RequestServiceException {
        logger.debug("finding request service message for : messageId [" + messageId + "]");
        final Query query = sessionFactory.getCurrentSession().createQuery(FIND_BY_MESSAGE_ID).setLong(0, messageId);
        List<RequestServiceData> requestServiceDataList = query.list();
        if (requestServiceDataList.size() > 1) {
            throw new IllegalStateException("Multiple Request Service Message found for messageId[" + messageId + "]");
        } else if (requestServiceDataList.size() == 0) {
            throw new RequestServiceException("Message not found[" + messageId + "]",
                    MessageFlowErrorReasons.REQUEST_SHOW_MESSAGE_ID_NOT_FOUND);
        } else {
            return requestServiceDataList.get(0);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public int getTotalMessageCount(String appId) throws RequestServiceException {
        if (logger.isDebugEnabled()) {
            logger.debug("finding total message count for app_id [ " + appId + " ]");
        }
        final Query query = sessionFactory.getCurrentSession().createSQLQuery(GET_ALL_MESSAGE_COUNT_FOR_SERVICE);
        query.setString(0, appId);
        int count = ((BigInteger) query.uniqueResult()).intValue();
        return count;

    }

    @Override
    @Transactional
    public void rollBackReceivedRequestMessage(long messageId) throws RequestServiceException {
        sessionFactory.getCurrentSession().delete(findRequestServiceData(messageId));
    }

    @Override
    @Transactional
    public void updateIsReplied(long messageId) throws RequestServiceException{
        if (logger.isDebugEnabled()) {
            logger.debug("finding request service message for : messageId [" + messageId + "]");
        }
        final Query query = sessionFactory.getCurrentSession().createQuery(FIND_BY_MESSAGE_ID).setLong(0, messageId);
        List<RequestServiceData> requestServiceDataList = query.list();
        if (requestServiceDataList.size() > 1) {
            throw new IllegalStateException("Multiple Request Service Message found for messageId[" + messageId + "]");
        } else if (requestServiceDataList.size() == 0) {
            throw new RequestServiceException("Message not found[" + messageId + "]",
                    MessageFlowErrorReasons.REQUEST_SHOW_MESSAGE_ID_NOT_FOUND);
        } else {
            RequestServiceData updatedData = requestServiceDataList.get(0);
            updatedData.setReplied(true);
            sessionFactory.getCurrentSession().update(updatedData);
            logger.debug("request service data ["+updatedData.toString()+"] successfully saved");
        }
    }

    @Override
    @Transactional
    public void updateRequestServiceData(RequestServiceData requestServiceData) throws RequestServiceException {
        sessionFactory.getCurrentSession().update(requestServiceData);
    }

    public boolean checkCommonResponseAvailability(String applicationId){
        final Application application = (Application) sessionFactory.getCurrentSession()
                .createQuery(FIND_APPLICATION_BY_APP_ID).setString(0, applicationId).list().get(0);
        RequestService requestService = (RequestService) application.getService();

        String responseMessage = requestService.getResponseMessages().iterator().next().getMessage();
        logger.debug("COMMON RESPONSE MESSAGE : [" + responseMessage + "]");
        return (responseMessage != null);
    }

    private List<RequestServiceData> processRequestServiceDataInfo(List requestMessageList) {
        List<RequestServiceData> requestServiceDataResult = new ArrayList<RequestServiceData>();
        for (Object object : requestMessageList) {
            Object[] requestMsgRow = (Object[]) object;
            String address = (String) requestMsgRow[0];
            String operator = (String) requestMsgRow[1];
            String message = (String) requestMsgRow[2];
            long receivedTime = requestMsgRow[3] == null ? null : ((java.math.BigInteger) requestMsgRow[3]).longValue();
            long messageId = requestMsgRow[4] == null ? 0 : ((java.math.BigInteger) requestMsgRow[4]).longValue();
            boolean isRead = (Boolean) requestMsgRow[5];
            String keyword = (String) requestMsgRow[6];
            RequestServiceData rsd = new RequestServiceData();
            rsd.setMsisdn(new Msisdn(address, operator));
            rsd.setReceivedMessage(message);
            rsd.setReceivedTime(receivedTime);
            rsd.setMessageId(messageId);
            rsd.setRead(isRead);
            rsd.setSubKeyword(keyword);
            if (logger.isDebugEnabled()) {
                logger.debug("Request Service Data [" + rsd + "]");
            }
            requestServiceDataResult.add(rsd);
        }
        return requestServiceDataResult;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

}
