/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hsenidmobile.orca.orm.repository;

import hsenidmobile.orca.core.model.RevenueSummary;
import hsenidmobile.orca.core.repository.RevenueReportRepository;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class RevenueReportRepositoryImpl implements RevenueReportRepository {

    private static final Logger logger = Logger.getLogger(RevenueReportRepositoryImpl.class);

    final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

    private static final String GET_MATCHING_REVENUE_SUMMARIES_BY_DURATION =
            "from RevenueSummary where revenueSummaryPrimaryKey.applicationId like ? and " +
                    "revenueSummaryPrimaryKey.dateId >= ? and " +
                    "revenueSummaryPrimaryKey.dateId <= ?";

    private static final String GET_MATCHING_REVENUE_SUMMARIES =
            "from RevenueSummary where revenueSummaryPrimaryKey.applicationId like ?";

    private static final String SELECT_ALL_APPLICATION_NAMES = "select distinct applicationName from RevenueSummary";

    private static final String GET_DATEID_FROM_FULL_DATE = "select date_id from date_info where full_date = ?";

    private SessionFactory sessionFactory;

    @Transactional(readOnly = true)
    public List<String> getAllApplicationNames() {
        List<String> appNames = new ArrayList<String>();

        if (logger.isDebugEnabled()) {
            logger.debug("Searching all available application names in the database");
        }

        Query query = sessionFactory.getCurrentSession().createQuery(SELECT_ALL_APPLICATION_NAMES);
        appNames.addAll(query.list());

        return appNames;
    }

    @Transactional(readOnly = true)
    public List<RevenueSummary> getMatchingRevenueSummaries(List<String> appIds, Date from, Date to,
                                                            boolean filterByDuartion) {
        Query query;
        List<RevenueSummary> revenueSummaryList = new ArrayList<RevenueSummary>();

        if (logger.isDebugEnabled()) {
            logger.debug("Searching for matching Revenue Summaries in database with application ids = " + appIds);
        }

        for (String appId : appIds) {
            if (filterByDuartion) {
                int fromDateId = getDateIdFromFullDate(from);
                int toDateId = getDateIdFromFullDate(to);

                query = sessionFactory.getCurrentSession().createQuery(GET_MATCHING_REVENUE_SUMMARIES_BY_DURATION);
                query.setString(0, "%" + appId + "%");
                query.setInteger(1, fromDateId);
                query.setInteger(2, toDateId);
                revenueSummaryList.addAll(query.list());
            } else {
                query = sessionFactory.getCurrentSession().createQuery(GET_MATCHING_REVENUE_SUMMARIES).
                        setString(0, "%" + appId + "%");
                revenueSummaryList.addAll(query.list());
            }
        }

        return revenueSummaryList;
    }

    private int getDateIdFromFullDate(Date date) {
        Query query = sessionFactory.getCurrentSession().createSQLQuery(GET_DATEID_FROM_FULL_DATE);
        query.setDate(0, date);

        if (query.list().isEmpty()) {
            return 0;
        } else {
            return Integer.parseInt(query.list().get(0).toString());
        }
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
