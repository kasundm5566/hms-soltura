/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.orm.repository;

import java.util.List;

import hsenidmobile.orca.core.applications.ContentDispatchSummary;
import hsenidmobile.orca.core.applications.exception.ApplicationException;
import hsenidmobile.orca.core.applications.subscription.Subscription;
import hsenidmobile.orca.core.applications.subscription.SubscriptionContentRelease;
import hsenidmobile.orca.core.applications.subscription.SubscriptionServiceDataRepository;

import hsenidmobile.orca.core.flow.MessageFlowErrorReasons;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 *
 * @version $LastChangedRevision$
 */
public class SubscriptionServiceDataRepositoryImpl implements SubscriptionServiceDataRepository {

    private static final Logger logger = LoggerFactory.getLogger(Subscription.class);
    private SessionFactory sessionFactory;
    private static final int START = 0;
    private static final int END = 1;
    private static final String  GET_LAST_SENT_SUBSCRIPTION_BY_APP_ID = "select lm.message, con.sent_time from subscription_content_release scr " +
            "INNER JOIN subscription_content_release_content scrc on scr.id=scrc.subscription_content_release_id " +
            "INNER JOIN content con on con.id= scrc.contents_id " +
            "INNER JOIN localized_message lm on lm.id=con.localized_msg_id " +
            "where scr.app_id=? and scr.status='SENT' order by con.sent_time DESC limit " + START + ", " + END + " ";

    private static final String  GET_LAST_SENT_SUBSCRIPTION_BY_SUB_CATEGORY = "select lm.message, con.sent_time from subscription_content_release scr " +
            "INNER JOIN subscription_content_release_content scrc on scr.id=scrc.subscription_content_release_id " +
            "INNER JOIN content con on con.id= scrc.contents_id " +
            "INNER JOIN localized_message lm on lm.id=con.localized_msg_id " +
            "INNER JOIN sub_category sub on sub.id=con.sub_category " +
            "where scr.app_id=? and scr.status='SENT' and con.sub_category=? order by con.sent_time DESC limit " + START + "," + END + " ";

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void addContentRelease(SubscriptionContentRelease arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    @Transactional
    public void addOrUpdateContent(List<SubscriptionContentRelease> contents) {
        if (contents != null) {
            for (SubscriptionContentRelease subscriptionContentRelease : contents) {
                sessionFactory.getCurrentSession().saveOrUpdate(subscriptionContentRelease);
            }
        }

    }

    @Override
    public SubscriptionContentRelease getDispatchableContent(String arg0, DateTime arg1) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    @Transactional(readOnly = true)
    public List<SubscriptionContentRelease> getDispatchableContents(DateTime currentTime, int batchSize) {
        final Query query = sessionFactory
                .getCurrentSession()
                .createQuery(
                        "from SubscriptionContentRelease WHERE status = 'FUTURE' AND scheduledDispatchTimeFrom <= ?  AND scheduledDispatchTimeTo >= ?")
                .setTimestamp(0, currentTime.toDate()).setTimestamp(1, currentTime.toDate());
        ;
        query.setMaxResults(batchSize);

        return (List<SubscriptionContentRelease>) query.list();
    }

    @Override
    @Transactional(readOnly = true)
    public SubscriptionContentRelease getNextUpdatableContent(String appId, DateTime currentTime) {
        final Query query = sessionFactory
                .getCurrentSession()
                .createQuery(
                        "from SubscriptionContentRelease WHERE appId = ? AND status = 'FUTURE' AND scheduledDispatchTimeFrom > ? ")
                .setString(0, appId).setTimestamp(1, currentTime.toDate());
        query.setMaxResults(1);

        final List contents = query.list();
        if (contents.size() == 1) {
            return (SubscriptionContentRelease) contents.get(0);
        } else {
            return null;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<SubscriptionContentRelease> getUpdatableContents(String appId, DateTime currentTime) {
        logger.debug("Getting updatable contents for app[{}] at[{}]", appId, currentTime);
        final Query query = sessionFactory
                .getCurrentSession()
                .createQuery(
                        "from SubscriptionContentRelease WHERE appId = ? AND status = 'FUTURE' AND scheduledDispatchTimeFrom > ? ")
                .setString(0, appId).setTimestamp(1, currentTime.toDate());

        return (List<SubscriptionContentRelease>) query.list();
    }

    @Override
    @Transactional
    public void updateContentRelease(SubscriptionContentRelease contentRelease) {
        sessionFactory.getCurrentSession().update(contentRelease);
    }

    @Override
    @Transactional
    public ContentDispatchSummary getLastSentSubscriptionByApplicationId(String appId) throws ApplicationException {
        Query query = sessionFactory.getCurrentSession().createSQLQuery(GET_LAST_SENT_SUBSCRIPTION_BY_APP_ID).
                setString(0, appId);
        List summaryDataList = query.list();
        ContentDispatchSummary contentDispatchSummary = null;
        if (summaryDataList != null && !summaryDataList.isEmpty()) {
            contentDispatchSummary = processReceivedContentDispatchSummary(summaryDataList);
            return contentDispatchSummary;
        } else {
            throw new ApplicationException("Last Sent Alert Content not found for the appId[" + appId + "]",
                    MessageFlowErrorReasons.NO_LAST_SENT_ALERT_FOUND);
        }
    }

    @Override
    @Transactional
    public ContentDispatchSummary getLastSentSubscriptionBySubCategoryId(String appId, long subCategoryId) throws ApplicationException {
        Query query = sessionFactory.getCurrentSession().createSQLQuery(GET_LAST_SENT_SUBSCRIPTION_BY_SUB_CATEGORY).
                setString(0, appId).setLong(1, subCategoryId);
        List summaryDataList = query.list();
        ContentDispatchSummary contentDispatchSummary = null;
        if (summaryDataList != null && !summaryDataList.isEmpty()) {
            contentDispatchSummary = processReceivedContentDispatchSummary(summaryDataList);
            return contentDispatchSummary;
        } else {
            throw new ApplicationException("Last Sent Alert Content not found for the appId[" + appId + "]" +
                    "and sub category id [" + subCategoryId+"]",
                    MessageFlowErrorReasons.NO_LAST_SENT_ALERT_FOUND);
        }
    }

    private ContentDispatchSummary processReceivedContentDispatchSummary(List summaryDataList) {
        ContentDispatchSummary contentDispatchSummary = new ContentDispatchSummary();
        Object[] summaryRow = (Object[]) summaryDataList.get(0);
        contentDispatchSummary.setLastSentContent((String)summaryRow[0]);
        contentDispatchSummary.setLastSentOnTime(new DateTime(summaryRow[1]));
        return contentDispatchSummary;
    }
}
