/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate: $
 *   $LastChangedBy: $
 *   $LastChangedRevision: $
 *
 */
package hsenidmobile.orca.orm.repository;

import hsenidmobile.orca.core.applications.voting.Vote;
import hsenidmobile.orca.core.applications.voting.VoteResultSummary;
import hsenidmobile.orca.core.model.Msisdn;
import hsenidmobile.orca.core.model.Response;
import hsenidmobile.orca.core.repository.VoteRepository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 *
 */
public class VoteRepositoryImpl implements VoteRepository {

    private static final Logger logger = Logger.getLogger(VoteRepositoryImpl.class);

    private static final String GET_VOTING_RESULT = "SELECT lm.message, sc.keyword, vrs.total_vote_count, vrs.last_update_time "
            + "FROM voting_result_summary vrs "
            + "INNER JOIN sub_category sc on vrs.candidate_id=sc.id "
            + "INNER JOIN sub_category_description scd on scd.sub_category_id = sc.id "
            + "INNER JOIN localized_message lm on lm.id = scd.description_id WHERE vrs.votelet_id = ?";

    private static final String FIND_VOTE = "SELECT v.address, v.operator "
            + "FROM vote v INNER JOIN candidate_vote cv  on v.id=cv.vote_id "
            + "INNER JOIN sub_category sc on sc.id = cv.candidate_id "
            + "WHERE v.service_id = ? AND v.address = ? AND v.operator = ?";

    private static final String UPDATE_VOTE_SUMMARY = "UPDATE voting_result_summary "
            + "SET total_vote_count = (total_vote_count + 1), last_update_time = ? WHERE votelet_id = ? AND candidate_id = ?";

    private static final String INSERT_VOTE_SUMMARY = "INSERT INTO voting_result_summary "
            + "(votelet_id, candidate_id,total_vote_count, last_update_time) VALUES (?,?,1, ?) ";

    private static final String FIND_VOTE_BY_ID = "from Vote where id=?";

    private static final String UPDATE_VOTE_SUMMARY_AFTER_ROLLBACK = "update voting_result_summary set total_vote_count " +
            " = total_vote_count-1 where votelet_id = ? and candidate_id = ?";

    private SessionFactory sessionFactory;

    public VoteRepositoryImpl() {
    }

    public VoteRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Transactional
    public long add(Vote vote) {
        logger.info("Add vote to repository [" + vote + "]");
        sessionFactory.getCurrentSession().save(vote);
        return vote.getId();
    }

    @Override
    @Transactional
    public void updateSummary(long voteletId, Vote vote) {
        SQLQuery updateQuery = sessionFactory.getCurrentSession().createSQLQuery(UPDATE_VOTE_SUMMARY);
        updateQuery.setTimestamp(0, new Date());
        updateQuery.setLong(1, voteletId);
        updateQuery.setLong(2, vote.getCandidate().getId());
        int updatedRowCount = updateQuery.executeUpdate();
        if (logger.isDebugEnabled()) {
            logger.debug("Updated row count [" + updatedRowCount + "]");
        }
        if (updatedRowCount == 0) {
            if (logger.isDebugEnabled()) {
                logger.debug("Inserting since no update happened.");
            }
            SQLQuery insertQuery = sessionFactory.getCurrentSession().createSQLQuery(INSERT_VOTE_SUMMARY);
            insertQuery.setLong(0, voteletId);
            insertQuery.setLong(1, vote.getCandidate().getId());
            insertQuery.setTimestamp(2, new Date());
            insertQuery.executeUpdate();
        }
    }

    @Transactional
    public List<VoteResultSummary> getVotingSummary(long voteletId) {
        if (logger.isDebugEnabled()) {
            logger.debug("Getting voting summary for votelet[" + voteletId + "]");
        }

        Query query = sessionFactory.getCurrentSession().createSQLQuery(GET_VOTING_RESULT).setLong(0, voteletId);
        List summaryDataList = query.list();

        List<VoteResultSummary> voteResults = new ArrayList<VoteResultSummary>();
        for (Object object : summaryDataList) {
            Object[] summaryRow = (Object[]) object;
            String candidate = (String) summaryRow[0];
            String candidateCode = (String) summaryRow[1];
            long voteCount = summaryRow[2] == null ? 0 : ((java.math.BigInteger) summaryRow[2]).longValue();
            Date updatedDate = summaryRow[3] == null ? null : new Date(((java.sql.Timestamp) summaryRow[3]).getTime());
            VoteResultSummary vrs = new VoteResultSummary();
            vrs.setCandidateName(candidate);
            vrs.setCandidateCode(candidateCode);
            vrs.setTotalVoteCount(voteCount);
            vrs.setLastUpdateTime(updatedDate);

            if (logger.isDebugEnabled()) {
                logger.debug("Voting summary data[" + vrs + "]");
            }
            voteResults.add(vrs);
        }

        return voteResults;

    }

    @Override
    @Transactional
    public boolean hasVoted(Msisdn msisdn, long voteletId) {
        if (logger.isDebugEnabled()) {
            logger.debug("Checking whether msisdn[" + msisdn + "] has voted in votelet[" + voteletId + "]");
        }
        Query query = sessionFactory.getCurrentSession().createSQLQuery(FIND_VOTE);
        query.setLong(0, voteletId);
        query.setString(1, msisdn.getAddress());
        query.setString(2, msisdn.getOperator());
        List ballotList = query.list();
        return (ballotList != null && ballotList.size() > 0);
    }

    @Override
    @Transactional
    public void rollBackReceivedVote(long voteId, long voteServiceId) {
        if(logger.isDebugEnabled()) {
            logger.debug("Rollback transaction for Vote ID [ " +  voteId + " ] started due to the failed response");
        }
        Query query = sessionFactory.getCurrentSession().createQuery(FIND_VOTE_BY_ID);
        query.setLong(0, voteId);
        List<Vote> results = query.list();
        if(results != null && !results.isEmpty()) {
            Vote vote = results.get(0);
            sessionFactory.getCurrentSession().delete(vote);
            if(logger.isDebugEnabled()) {
                logger.debug("Vote Record for Vote ID [ " + voteId + " ] removed from the data base.");
            }
            updateSummaryAfterRollBack(voteServiceId, vote.getCandidate().getId());
        }
    }

    @Transactional
    public void updateSummaryAfterRollBack(long voteServiceId, long candidateId) {
        if(logger.isDebugEnabled()) {
            logger.debug("Updating vote result summary after rollbacking the transaction for vote service ID  [ " +
                    voteServiceId + " ] and candidate ID [ " + candidateId + " ]  started.");
        }
        SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(UPDATE_VOTE_SUMMARY_AFTER_ROLLBACK);
        query.setLong(0, voteServiceId);
        query.setLong(1, candidateId);
        query.executeUpdate();
        if(logger.isDebugEnabled()) {
            logger.debug("Vote Summary Record for Vote Service ID [ " + voteServiceId + " ] and Candidate ID [ " +
                    candidateId + " ] removed from the data base.");
        }
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

}
