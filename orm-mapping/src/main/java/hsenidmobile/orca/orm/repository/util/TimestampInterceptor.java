/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.orm.repository.util;

import hsenidmobile.orca.core.repository.PersistentObject;
import org.hibernate.EmptyInterceptor;
import org.hibernate.type.Type;

import java.io.Serializable;
import java.util.Date;

/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class TimestampInterceptor extends EmptyInterceptor {

	@Override
	public boolean onSave(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types) {
		boolean updated = false;
		if (entity instanceof PersistentObject) {
			final Date date = new Date();
			for (int i = 0; i < propertyNames.length; i++) {
				if ("createdTime".equals(propertyNames[i]) || "lastModifiedTime".equals(propertyNames[i])) {
					state[i] = date;
					updated = true;
				}
			}
		}
		return updated;
	}

	/**
	 * Called by hibernate right before updating an entity in the database
	 */
	@Override
	public boolean onFlushDirty(Object entity, Serializable id, Object[] currentState, Object[] previousState,
			String[] propertyNames, Type[] types) {
		if (entity instanceof PersistentObject) {
			final Date date = new Date();
			for (int i = 0; i < propertyNames.length; i++) {
				if ("lastModifiedTime".equals(propertyNames[i])) {
					currentState[i] = date;
					return true;
				}
			}
		}
		return false;
	}
}
