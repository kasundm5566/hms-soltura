/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.orm.sdp;

import hsenidmobile.orca.core.applications.exception.ApplicationException;
import hsenidmobile.orca.core.flow.MessageFlowErrorReasons;
import hsenidmobile.orca.core.model.OrcaSdpAppIntegration;
import hsenidmobile.orca.core.model.SdpAppLoginData;
import hsenidmobile.orca.core.repository.SdpApplicationRepository;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class SdpApplicationRepositoryImpl implements SdpApplicationRepository {

    private static final Logger logger = Logger.getLogger(SdpApplicationRepositoryImpl.class);

    private SessionFactory sessionFactory;

    private static final String FIND_SDP_APP_LOGIN_DATA = "from OrcaSdpAppIntegration where orcaAppId = ?";
    private static final String FIND_ORCA_APP_LOGIN_DATA = "from OrcaSdpAppIntegration where sdpAppId = ?";
    private static final String FIND_ORCA_APP_NAME_BY_APP_ID= "select app.app_name from application app inner join " +
            "orca_sdp_app_integration os on os.orca_app_id = app.app_id where os.orca_app_id= ? ";

    @Transactional
    @Override
    public void addOrcaSdpAppIntegrationData(OrcaSdpAppIntegration orcaSdpAppIntegration) {
        if (logger.isDebugEnabled()) {
            logger.info("Added Orca SDp Application Inetgration Information to repository [" + orcaSdpAppIntegration
                    + "]");
        }
        sessionFactory.getCurrentSession().save(orcaSdpAppIntegration);
    }

    @Transactional(readOnly = true)
    @Override
    public SdpAppLoginData getSdpAppLoginDataForOrcaApp(String orcaAppId) throws ApplicationException {
        if (logger.isDebugEnabled()) {
            logger.debug("finding sdp application  : orcaAppId [" + orcaAppId + "]");
        }

        final Query query = sessionFactory.getCurrentSession().createQuery(FIND_SDP_APP_LOGIN_DATA)
                .setString(0, orcaAppId);
        List<OrcaSdpAppIntegration> orcaSdpAppIntegrationList = query.list();
        if (orcaSdpAppIntegrationList.size() > 1) {
            throw new IllegalStateException("Multiple sdp application found for orcaAppId[" + orcaAppId + "]");
        } else if (orcaSdpAppIntegrationList.isEmpty()) {
            throw new ApplicationException("Application not found for appId[" + orcaAppId + "]",
                    MessageFlowErrorReasons.APP_NOT_FOUND);
        } else {
            return new SdpAppLoginData(orcaSdpAppIntegrationList.get(0).getSdpAppId(), orcaSdpAppIntegrationList.get(0)
                    .getSdpAppPassword());
        }
    }

    @Transactional(readOnly = true)
    @Override
    public String getOrcaAppId(String sdpAppId) throws ApplicationException {
        if (logger.isDebugEnabled()) {
            logger.debug("finding orca application  : sdpAppId [" + sdpAppId + "]");
        }

        final Query query = sessionFactory.getCurrentSession().createQuery(FIND_ORCA_APP_LOGIN_DATA)
                .setString(0, sdpAppId);
        List<OrcaSdpAppIntegration> orcaSdpAppIntegrationList = query.list();
        if (orcaSdpAppIntegrationList.size() > 1) {
            throw new IllegalStateException("Multiple sdp application found for sdpAppId[" + sdpAppId + "]");
        } else if (orcaSdpAppIntegrationList.size() == 0) {
            throw new ApplicationException("Application not found for appId[" + sdpAppId + "]",
                    MessageFlowErrorReasons.APP_NOT_FOUND);
        } else {
            return orcaSdpAppIntegrationList.get(0).getOrcaAppId();
        }
    }

    @Transactional(readOnly = true)
    @Override
    public OrcaSdpAppIntegration getIntegrationDetails(String orcaAppId) throws ApplicationException {
        if (logger.isDebugEnabled()) {
            logger.debug("finding orca application  : orcaAppId [" + orcaAppId + "]");
        }

        final Query query = sessionFactory.getCurrentSession().createQuery(FIND_SDP_APP_LOGIN_DATA)
                .setString(0, orcaAppId);
        List<OrcaSdpAppIntegration> orcaSdpAppIntegrationList = query.list();
        if (orcaSdpAppIntegrationList.size() > 1) {
            throw new IllegalStateException("Multiple sdp application found for sdpAppId[" + orcaAppId + "]");
        } else if (orcaSdpAppIntegrationList.size() == 0) {
            throw new ApplicationException("Application not found for appId[" + orcaAppId + "]",
                    MessageFlowErrorReasons.APP_NOT_FOUND);
        } else {
            return orcaSdpAppIntegrationList.get(0);
        }
    }

    @Transactional(readOnly = true)
    @Override
    public String getOrcaAppName(String orcaAppId) throws ApplicationException {
        if (logger.isDebugEnabled()) {
            logger.debug("finding orca application name  : orcaAppId [" + orcaAppId + "]");
        }

        final Query query = sessionFactory.getCurrentSession().createSQLQuery(FIND_ORCA_APP_NAME_BY_APP_ID)
                .setString(0, orcaAppId);
        List<String> appNameList = query.list();
        if (appNameList.size() > 1) {
            throw new IllegalStateException("Multiple sdp application found for orcaAppId[" + orcaAppId + "]");
        } else if (appNameList.size() == 0) {
            throw new ApplicationException("Application not found for appId[" + orcaAppId + "]",
                    MessageFlowErrorReasons.APP_NOT_FOUND);
        } else {
            return appNameList.get(0);
        }
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
