/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hsenidmobile.orca.orm.mock;

import hsenidmobile.orca.core.applications.ApplicationImpl;
import hsenidmobile.orca.core.applications.PeriodUnit;
import hsenidmobile.orca.core.applications.ServicePolicy;
import hsenidmobile.orca.core.applications.policy.RegistrationPolicy;
import hsenidmobile.orca.core.applications.policy.SubscriberValidationPolicy;
import hsenidmobile.orca.core.applications.subscription.Periodicity;
import hsenidmobile.orca.core.applications.voting.VotingService;
import hsenidmobile.orca.core.model.ApplicationRoutingInfo;
import hsenidmobile.orca.core.model.ContentProvider;
import hsenidmobile.orca.core.model.LocalizedMessage;
import hsenidmobile.orca.core.model.Msisdn;
import hsenidmobile.orca.core.model.RoutingInfo;
import hsenidmobile.orca.core.model.RoutingInfoStatus;
import hsenidmobile.orca.core.model.RoutingKey;
import hsenidmobile.orca.core.model.Status;
import hsenidmobile.orca.core.model.message.Message;
import hsenidmobile.orca.core.model.message.SmsMessage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.joda.time.DateTime;

/**
 * $LastChangedDate: $ $LastChangedBy: $ $LastChangedRevision: $
 */
public class MockApplicationCreator {

	public static final int RANDOM_VALUE = ((int) (Math.random() * 100));

	public static final String APP_ID_VOTING = "app_id_vote_" + RANDOM_VALUE;
	public static final String APP_ID_SUB = "app_id_sub_" + RANDOM_VALUE;
	public static final String APP_ID_SUB_NO_SUB = "app_id_sub_n" + RANDOM_VALUE;
	public static final String APP_ID_ALERT = "app_id_alt_" + RANDOM_VALUE;
	public static final String APP_ID_REQUEST = "app_id_req_" + RANDOM_VALUE;

	public static final String APP_NAME = "app_name_" + RANDOM_VALUE;

	public static final String VOTELET_ID = "votelet_id" + RANDOM_VALUE;

	public static final String KEYWORD_CANDIDATE_1 = "keyword_can_1";
	public static final String KEYWORD_CANDIDATE_2 = "keyword_can_2";
	public static final String KEYWORD_CANDIDATE_3 = "keyword_can_3";

	public static final String ALIAS_CANDIDATE_1 = "alias_can_1";
	public static final String ALIAS_CANDIDATE_2 = "alias_can_2";
	public static final String ALIAS_CANDIDATE_3 = "alias_can_3";

	public static final String DESC_CANDIDATE_EN = "desc_can_en";
	public static final String DESC_CANDIDATE_TA = "desc_can_ta";
	public static final String DESC_CANDIDATE_SI = "desc_can_si";

	public static final String SUBSCRIPTION_SUCCESSFUL_EN = "Subscription Successful EN";
	public static final String SUBSCRIPTION_SUCCESSFUL_TA = "Subscription Successful TA";
	public static final String SUBSCRIPTION_SUCCESSFUL_SI = "Subscription Successful SI";

	public static final String UNSUBSCRIPTION_SUCCESSFUL_EN = "Unsubscription Successful EN";
	public static final String UNSUBSCRIPTION_SUCCESSFUL_TA = "Unsubscription Successful TA";
	public static final String UNSUBSCRIPTION_SUCCESSFUL_SI = "Unsubscription Successful SI";

	public static final String REQUEST_INVALID_EN = "Your Request is Invalid EN";
	public static final String REQUEST_INVALID_TA = "Your Request is Invalid TA";
	public static final String REQUEST_INVALID_SI = "Your Request is Invalid SI";

	public static final String KEYWORD_VOTE = "9696" + RANDOM_VALUE;
	public static final String KEYWORD_SUB = "8888" + RANDOM_VALUE;
	public static final String KEYWORD_SUB_NO_SUB = "7777" + RANDOM_VALUE;
	public static final String KEYWORD_REQUEST = "9999" + RANDOM_VALUE;

	public static final String WS_PASSWORD = "abc123";

	public static final String VOTING_CANDIDATE_1_KEYWORD = "sf";
	public static final String[] VOTING_CANDIDATE_1_KEYWORD_ALIAS = { "SARATH", "HANSAYA", "WINNER" };
	public static final String VOTING_CANDIDATE_2_KEYWORD = "mr";
	public static final String[] VOTING_CANDIDATE_2_KEYWORD_ALIAS = { "MAHINDA", "BULATH", "LOSSER" };

	public static final String REQUEST_SUB_1_KEYWORD = "reqsub1";

	public static ApplicationImpl createVotingApplication(RoutingKey rk, int randomVal) {

		final ApplicationImpl votingApp = new ApplicationImpl();
		votingApp.setAppId(APP_ID_VOTING + randomVal);
		votingApp.setAppName(APP_NAME + randomVal);
		votingApp.setSubscriptionResponse(getSubscriptiongRespounse());
		votingApp.setUnsubscribeResponse(getUnsubscriptiongRespounse());
		votingApp.setInvalidRequestErrorMessage(getInvalidRequestError());
		votingApp.setApplicationRoutingInfo(getApplicationRoutingInfo(votingApp.getAppId(), rk));
		votingApp.setStatus(Status.ACTIVE);
		votingApp.setRegistrationPolicy(RegistrationPolicy.OPEN);
		votingApp.setService(createVotingService());
		votingApp.setDataMessagePolicies(getServicePolicy());
		votingApp.setStartDate(new DateTime());
		votingApp.setEndDate(new DateTime().plusDays(10));

		return votingApp;
	}

	private static VotingService createVotingService() {
		VotingService voting = new VotingService();
		// try {
		// voting.addVotelets(getVotletList());
		// } catch (VoteletException e) {
		// throw new RuntimeException("Error while added votelets", e);
		// }
		return voting;
	}

	private static ApplicationRoutingInfo getApplicationRoutingInfo(String appId, RoutingKey rk) {

		ApplicationRoutingInfo routing1 = new ApplicationRoutingInfo();
		final RoutingInfo routingInfo = new RoutingInfo();
		routingInfo.setStartDate(System.currentTimeMillis());
		routingInfo.setEndDate(System.currentTimeMillis() + 500000);
		routingInfo.setLastChargedDate(System.currentTimeMillis());
		routingInfo.setOwnerCp(getContentProvider());
		routingInfo.setRoutingKey(rk);
		routingInfo.setOwningAppId(appId);
		routingInfo.setStatus(RoutingInfoStatus.ASSIGNED);
		routing1.addRoutingInfo(routingInfo);
		return routing1;
	}

	private static ContentProvider getContentProvider() {
		final ContentProvider provider = new ContentProvider();
		return provider;
	}

	// private static Map<Locale, String> getSubscriptiongRespounse() {
	// Map<Locale, String> subscription = new HashMap<Locale, String>();
	// subscription.put(Locale.ENGLISH, SUBSCRIPTION_SUCCESSFUL_EN);
	// subscription.put(new Locale("ta"), SUBSCRIPTION_SUCCESSFUL_TA);
	// subscription.put(new Locale("si"), SUBSCRIPTION_SUCCESSFUL_SI);
	// return subscription;
	// }

	private static List<LocalizedMessage> getSubscriptiongRespounse() {
		return Arrays.asList(new LocalizedMessage(SUBSCRIPTION_SUCCESSFUL_EN, Locale.ENGLISH));
	}

	// private static Map<Locale, String> getUnsubscriptiongRespounse() {
	// Map<Locale, String> subscription = new HashMap<Locale, String>();
	// subscription.put(Locale.ENGLISH, UNSUBSCRIPTION_SUCCESSFUL_EN);
	// subscription.put(new Locale("ta"), UNSUBSCRIPTION_SUCCESSFUL_TA);
	// subscription.put(new Locale("si"), UNSUBSCRIPTION_SUCCESSFUL_SI);
	// return subscription;
	// }

	private static List<LocalizedMessage> getUnsubscriptiongRespounse() {
		return Arrays.asList(new LocalizedMessage(UNSUBSCRIPTION_SUCCESSFUL_EN, Locale.ENGLISH));
	}

	private static ServicePolicy getServicePolicy() {
		return new SubscriberValidationPolicy();
	}

	public static Periodicity getPeriodicity() {
		Periodicity periodicity = new Periodicity();
		periodicity.setUnit(PeriodUnit.HOUR);
		periodicity.setPeriodValue(2);
		return periodicity;
	}

	// private static Map<Locale, String> getInvalidRequestError() {
	// Map<Locale, String> invalidRequest = new HashMap<Locale, String>();
	// invalidRequest.put(Locale.ENGLISH, REQUEST_INVALID_EN);
	// invalidRequest.put(new Locale("ta"), REQUEST_INVALID_TA);
	// invalidRequest.put(new Locale("si"), REQUEST_INVALID_SI);
	// return invalidRequest;
	// }
	private static List<LocalizedMessage> getInvalidRequestError() {
		return Arrays.asList(new LocalizedMessage(REQUEST_INVALID_EN, Locale.ENGLISH));
	}

	public static Message getMessage() {
		Message message = new SmsMessage();
		message.setMessage(KEYWORD_CANDIDATE_1);
		message.setSenderAddress(getSenderAddress());
		message.setReceiverAddresses(getReciverAddress());
		return message;
	}

	private static List<Msisdn> getReciverAddress() {
		List<Msisdn> listMsisdn = new ArrayList<Msisdn>();
		listMsisdn.add(new Msisdn(KEYWORD_VOTE, "DIALOG"));
		return listMsisdn;
	}

	private static Msisdn getSenderAddress() {
		return new Msisdn("0777123456", "DIALOG");
	}

}