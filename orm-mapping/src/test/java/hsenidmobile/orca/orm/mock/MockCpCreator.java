/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.orm.mock;

import hsenidmobile.orca.core.applications.ApplicationImpl;
import hsenidmobile.orca.core.model.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class MockCpCreator {

	private final static long MILLY_PER_DAY = 1000 * 60 * 60 * 24;
	public static final int RANDOM_VALUE = ((int) (Math.random() * 100));

	public static ContentProvider createPlainCp(int randomVal) {
		ContentProvider cp = new ContentProvider();
		cp.setCpId("hyso" + randomVal);
		cp.setName("Hypo Solutions" + randomVal);
		cp.setDescription("CPs of the Next Generation");
		cp.setCreateDate(new Date());
		cp.setStatus(Status.ACTIVE);
		return cp;
	}

	public static ContentProvider createCpWithAllDeps(int randomVal) {
		ContentProvider cp = new ContentProvider();
		cp.setCpId("hyso" + randomVal);
		cp.setName("Hypo Solutions" + randomVal);
		cp.setDescription("CPs of the Next Generation");
		cp.setCreateDate(new Date());
		cp.setStatus(Status.ACTIVE);

		BillingInfo billing = new BillingInfo();
		billing.setBillingName("Test");
		billing.setBillingAddress("No.2/a, Nawam Mv, Colombo 2, Sri Lanka.");
		billing.setExpressDelivery(false);
		cp.setBillingInfo(billing);


//		List<RoutingInfo> ownedRoutingInfo = new ArrayList<RoutingInfo>();
//		RoutingInfo info1 = new RoutingInfo();
//		info1.setStartDate(System.currentTimeMillis() - (MILLY_PER_DAY * 20));
//		info1.setEndDate(System.currentTimeMillis() + (MILLY_PER_DAY * 10));
//		info1.setOwnerCp(cp);
		RoutingKey rk1 = new RoutingKey(new Msisdn("9696", "DIALOG"), "elec" + randomVal);
//		info1.setRoutingKey(rk1);
//		ownedRoutingInfo.add(info1);
//
//		RoutingInfo info2 = new RoutingInfo();
//		info2.setStartDate(System.currentTimeMillis() - (MILLY_PER_DAY * 20));
//		info2.setEndDate(System.currentTimeMillis() + (MILLY_PER_DAY * 10));
//		info2.setOwnerCp(cp);
//		RoutingKey rk2 = new RoutingKey(new Msisdn("9696", "DIALOG"), "elec" + randomVal + " result");
//		info2.setRoutingKey(rk2);
//		ownedRoutingInfo.add(info2);
//
//		cp.setOwnedRoutingInfo(ownedRoutingInfo);

		List<ApplicationImpl> apps = new ArrayList<ApplicationImpl>();
		apps.add(MockApplicationCreator.createVotingApplication(rk1, randomVal));
		cp.setApplications(apps);
		return cp;
	}
}
