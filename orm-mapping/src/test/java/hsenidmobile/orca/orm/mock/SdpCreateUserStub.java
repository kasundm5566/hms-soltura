package hsenidmobile.orca.orm.mock;

import hsenidmobile.commonadmin.model.Group;
import hsenidmobile.commonadmin.model.User;
import hsenidmobile.orca.core.model.*;
import hsenidmobile.orca.orm.repository.ContextLoader;
import org.springframework.context.ApplicationContext;

import java.text.DecimalFormat;
import java.util.*;

/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *   
 *   $LastChangedDate$
 *   $LastChangedBy$ 
 *   $LastChangedRevision$
 */

public class SdpCreateUserStub {

    public final int RANDOM_VALUE = ((int) (Math.random() * 1000));

    private ApplicationContext context;
    public final String CP_NAME = "cp1" + RANDOM_VALUE;
    public final String CONTACT_PERSON_NAME = "contact1" + RANDOM_VALUE;
    public final String NIC = "848215471v" + RANDOM_VALUE;
    public final String TELEPHONE = "94778526535" + RANDOM_VALUE;
    public final String OPERATOR = "operator" + RANDOM_VALUE;
    public final String USER_NAME = "user" + RANDOM_VALUE;
    public final String COMPANY_NAME = "soltura1" + RANDOM_VALUE;

    public SdpCreateUserStub() {
        context = ContextLoader.getInstance().getContext();
    }

    public ContentProvider createContentProvider() throws ContentProviderException {
        ContentProvider contentProvider = new ContentProvider();
        contentProvider.setName(CP_NAME);
        contentProvider.setContactPersonName(CONTACT_PERSON_NAME);
        contentProvider.setCreateDate(new Date());
        contentProvider.setEmail("cp@hsenidmobile.com");
        contentProvider.setNic(NIC);
        contentProvider.setTelephone(TELEPHONE);
        contentProvider.createContentProviderId(CP_NAME);
        contentProvider.setDescription("description");
        contentProvider.setStatus(Status.ACTIVE);
        return contentProvider;
    }

    private String generateVerificationCode() {
        DecimalFormat numberFormat = new DecimalFormat("000");
        final String currentTime = Double.toString(System.currentTimeMillis());
        return currentTime.substring(currentTime.length() - 3, currentTime.length()) +
                numberFormat.format(new Random().nextInt(100));
    }

    public User createUser(Group spGroup, Group marketingUserGroup) {
        User user;
        user = new User();
        user.setUsername(USER_NAME);
        user.setPassword("098f6bcd4621d373cade4e832627b4f6");
        user.setEnabled(true);
        Set<Group> groups = new HashSet<Group>();
        groups.add(spGroup);
        groups.add(marketingUserGroup);
        user.setGroups(groups);
        user.setCreatedBy(USER_NAME);
        user.setLogingAttemptCount(1);
        return user;
    }
}
