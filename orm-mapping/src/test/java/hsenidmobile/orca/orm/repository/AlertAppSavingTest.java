/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hsenidmobile.orca.orm.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import hsenidmobile.orca.core.applications.ApplicationImpl;
import hsenidmobile.orca.core.applications.Service;
import hsenidmobile.orca.core.applications.alert.AlertContentRelease;
import hsenidmobile.orca.core.applications.alert.AlertService;
import hsenidmobile.orca.core.flow.dispatch.aventuraapi.AventuraApiMtMessageDispatcher;
import hsenidmobile.orca.core.model.Application;
import hsenidmobile.orca.core.model.Content;
import hsenidmobile.orca.core.model.ContentRelease;
import hsenidmobile.orca.core.model.SubCategory;
import hsenidmobile.orca.orm.test.util.ApplicationUtil;
import hsenidmobile.orca.orm.test.util.ServiceUtil;

import java.util.Arrays;
import java.util.List;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.context.ApplicationContext;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
public class AlertAppSavingTest {
	private AppRepositoryImpl appRepository;
	private static ApplicationContext context;
	private Mockery mockContext;
	private AventuraApiMtMessageDispatcher messageDispatcher;

	@BeforeClass
	public static void loadContext() {
		context = ContextLoader.getInstance().getContext();
	}

	@Before
	public void setUp() throws Exception {
		appRepository = (AppRepositoryImpl) context.getBean("appRepository");
		mockContext = new Mockery();
		messageDispatcher = mockContext.mock(AventuraApiMtMessageDispatcher.class);
		mockContext.checking(new Expectations() {
			{
				allowing(messageDispatcher).dispatchBroadcastMessage(with(any(String.class)), with(any(List.class)));
			}
		});

	}

	private AlertContentRelease createContentRelease(List<Content> content) {
		AlertContentRelease release = new AlertContentRelease();
		release.setContents(content);
		return release;
	}

	@Test
	public void testCreateApplication_WithoutSubCategories() throws Exception {
		ApplicationImpl app = ApplicationUtil.createApplication("ALERT-APP-ID1", "AlertApp1");
		app.setService(ServiceUtil.createAlertService(null));
		appRepository.createApplication(app);
		assertTrue("If inserted Id should be greater that 0", app.getId() > 0);

		Application savedApp = appRepository.findAppByAppId("ALERT-APP-ID1");

		assertEquals(app, savedApp);

	}

	@Test
	public void testCreateApplication_WithSubCategories() throws Exception {
		ApplicationImpl app = ApplicationUtil.createApplication("ALERT-APP-ID2", "AlertApp2");
		app.setService(ServiceUtil.createAlertService(Arrays.asList(ApplicationUtil.createSubCategory("cat1"))));
		appRepository.createApplication(app);
		assertTrue("If inserted Id should be greater that 0", app.getId() > 0);

		Application savedApp = appRepository.findAppByAppId("ALERT-APP-ID2");

		assertEquals(app, savedApp);

	}

	@Test
	@Ignore
	public void testDispatchedContentSaving() throws Exception {
		ApplicationImpl app = ApplicationUtil.createApplication("ALERT-APP-ID3", "AlertApp3");
		app.setService(ServiceUtil.createAlertService(Arrays.asList(ApplicationUtil.createSubCategory("cat1"))));
		appRepository.createApplication(app);
		assertTrue("If inserted Id should be greater that 0", app.getId() > 0);

		Application savedApp = appRepository.findAppByAppId("ALERT-APP-ID3");

		assertEquals(app, savedApp);

		AlertService alert = (AlertService) savedApp.getService();
		alert.setApplication(savedApp);
		alert.setAventuraApiMtMessageDispatcher(messageDispatcher);
		SubCategory subCat1 = alert.getSubcategory("cat1");

		ContentRelease release1 = createContentRelease(Arrays.asList(ApplicationUtil.createContent(
				"Content release 1 - cat1", subCat1)));
		alert.updateServiceData(Arrays.asList(release1));
		appRepository.updateService(alert);
//		savedApp = appRepository.findAppByAppId("ALERT-APP-ID3");
//		assertEquals("One dispatched content should have saved", 1, ((AlertService)savedApp.getService()).getDispatchedContents().size());
//
//		ContentRelease release2 = createContentRelease(Arrays.asList(ApplicationUtil.createContent(
//				"Content release 2 - cat1", subCat1)));
//		alert.updateServiceData(Arrays.asList(release2));
//		appRepository.updateService(alert);
//		savedApp = appRepository.findAppByAppId("ALERT-APP-ID3");
//		assertEquals("Two dispatched contents should have saved", 2, ((AlertService)savedApp.getService()).getDispatchedContents().size());


	}
}
