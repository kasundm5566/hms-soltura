/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hsenidmobile.orca.orm.repository;

import static org.junit.Assert.*;
import hsenidmobile.orca.core.applications.ApplicationImpl;
import hsenidmobile.orca.core.applications.alert.AlertContentRelease;
import hsenidmobile.orca.core.applications.exception.ApplicationException;
import hsenidmobile.orca.core.flow.dispatch.aventuraapi.AventuraApiMtMessageDispatcher;
import hsenidmobile.orca.core.model.Content;
import hsenidmobile.orca.core.model.ContentRelease;
import hsenidmobile.orca.core.model.SubCategory;
import hsenidmobile.orca.orm.test.util.ApplicationUtil;
import hsenidmobile.orca.orm.test.util.ServiceUtil;

import java.util.Arrays;
import java.util.List;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
public class AlertDispatchedContentRepositoryImplTest {
	private AppRepositoryImpl appRepository;
	private static ApplicationContext context;
	private AlertDispatchedContentRepositoryImpl dispatchedContentRepository;

	@BeforeClass
	public static void loadContext() {
		context = ContextLoader.getInstance().getContext();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		appRepository = (AppRepositoryImpl) context.getBean("appRepository");
		dispatchedContentRepository = (AlertDispatchedContentRepositoryImpl)context.getBean("alertDispatchedContentRepository");


	}

	private AlertContentRelease createContentRelease(List<Content> content) {
		AlertContentRelease release = new AlertContentRelease();
		release.setContents(content);
		return release;
	}

	/**
	 * Test method for {@link hsenidmobile.orca.orm.repository.AlertDispatchedContentRepositoryImpl#addDispatchedContent(hsenidmobile.orca.core.applications.alert.AlertContentRelease)}.
	 * @throws ApplicationException
	 */
	@Test
	public void testAddDispatchedContent() throws ApplicationException {
		ApplicationImpl alertApplication = ApplicationUtil.createApplication("ALERT-APP-ID30", "AlertApp30");
		alertApplication.setService(ServiceUtil.createAlertService(Arrays.asList(ApplicationUtil.createSubCategory("cat1"))));
		appRepository.createApplication(alertApplication);
		SubCategory subCat1 = alertApplication.getService().getSubcategory("cat1");
		AlertContentRelease release1 = createContentRelease(Arrays.asList(ApplicationUtil.createContent(
				"Content release 1 - cat1", subCat1)));
		release1.setAppId("ALERT-APP-ID30");
		release1.setAppName("AlertApp30");

		dispatchedContentRepository.addDispatchedContent(release1);
		assertTrue("If inserted Id should be greater that 0", release1.getId() > 0);
	}

	/**
	 * Test method for {@link hsenidmobile.orca.orm.repository.AlertDispatchedContentRepositoryImpl#getDispatchedContent(java.lang.String)}.
	 * @throws ApplicationException
	 */
	@Test
	public void testGetDispatchedContent() throws ApplicationException {
		ApplicationImpl alertApplication = ApplicationUtil.createApplication("ALERT-APP-ID31", "AlertApp31");
		alertApplication.setService(ServiceUtil.createAlertService(Arrays.asList(ApplicationUtil.createSubCategory("cat1"))));
		appRepository.createApplication(alertApplication);
		SubCategory subCat1 = alertApplication.getService().getSubcategory("cat1");
		AlertContentRelease release1 = createContentRelease(Arrays.asList(ApplicationUtil.createContent(
				"Content release 1 - cat1", subCat1)));
		release1.setAppId("ALERT-APP-ID31");
		release1.setAppName("AlertApp31");

		dispatchedContentRepository.addDispatchedContent(release1);
		assertTrue("If inserted Id should be greater that 0", release1.getId() > 0);

		List<AlertContentRelease> dispatchedContent = dispatchedContentRepository.getDispatchedContent("ALERT-APP-ID31");
		assertTrue("There should be some dispatched contents", dispatchedContent.size() > 0);
	}

}
