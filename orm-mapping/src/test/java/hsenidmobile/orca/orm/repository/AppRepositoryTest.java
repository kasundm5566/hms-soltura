/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hsenidmobile.orca.orm.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import hsenidmobile.orca.core.applications.ApplicationImpl;
import hsenidmobile.orca.core.applications.exception.ApplicationException;
import hsenidmobile.orca.core.applications.subscription.Subscription;
import hsenidmobile.orca.core.applications.voting.VotingService;
import hsenidmobile.orca.core.model.Application;
import hsenidmobile.orca.core.model.ReceivedMoSummary;
import hsenidmobile.orca.orm.test.util.ApplicationUtil;
import hsenidmobile.orca.orm.test.util.ServiceUtil;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.joda.time.DateTime;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;

/**
 * $LastChangedDate$
 * $LastChangedBy$ $LastChangedRevision$
 */
// @RunWith(SpringJUnit4ClassRunner.class)
// @ContextConfiguration(locations = {"classpath:spring-test-context.xml"})
// @TestExecutionListeners({DependencyInjectionTestExecutionListener.class})
public class AppRepositoryTest {

	private static final String APP_NAME = "VotingApp find";

	private static final String APP_ID = "VOTING-APP-FOR-FIND";

	private static AppRepositoryImpl appRepository;

	private static Application savedApp;
	private static ApplicationContext context;

	@BeforeClass
	public static void loadContext() {
		context = ContextLoader.getInstance().getContext();
		appRepository = (AppRepositoryImpl) context.getBean("appRepository");

		savedApp = ApplicationUtil.createApplication(APP_ID, APP_NAME);
		savedApp.setService(new VotingService());
		appRepository.createApplication(savedApp);
		assertTrue("If inserted Id should be greater that 0", savedApp.getId() > 0);
	}

	private void delete(Object obj) {
		SessionFactory sf = (SessionFactory) context.getBean("hibernateSessionFactory");
		final Session session = sf.openSession();
		Transaction transaction = session.beginTransaction();
		try {
			session.delete(obj);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			throw e;
		} finally {
			session.close();
		}
	}

	@Test
	public void testFindAppByAppId() throws Exception {

		final Application fetchedApp = appRepository.findAppByAppId(APP_ID);
		assertEquals(savedApp, fetchedApp);

	}

	@Test
	public void testFindAppId() throws Exception {
		final Application fetchedApp = appRepository.findAppById(savedApp.getId());
		assertEquals(savedApp, fetchedApp);
	}

	@Test
	public void testFindAppName() throws Exception {
		final List<Application> applications = appRepository.findAppByAppNameLike(APP_NAME);
		assertTrue("App should be selected", applications.size() >= 0);
		assertEquals(savedApp, applications.get(0));
	}

	@Test(expected = ApplicationException.class)
	public void testFindAppByIdNotFound() throws ApplicationException {
		appRepository.findAppById((long) 1234567);

	}

	@Test(expected = ApplicationException.class)
	public void testFindAppByAppIdNotFound() throws ApplicationException {
		appRepository.findAppByAppId("1234567");
	}

	@Test
	public void testFindSubscriptionApplications() throws ApplicationException {
		ApplicationImpl app = ApplicationUtil.createApplication("SUBS-APP-ID10", "SubsApp10");
		app.setService(ServiceUtil.createSubsctiptionService(null));
		appRepository.createApplication(app);
		assertTrue("If inserted Id should be greater that 0", app.getId() > 0);

		List<Application> applications = appRepository.findSubscriptionApplications();
		assertTrue("subscription application should be there", applications.size() > 0);
		for (Application application : applications) {
			assertNotNull(application);
			assertNotNull(application.getService());
			assertTrue(application.getService() instanceof Subscription);
		}
	}

	@Test
	public void testUpdateMoSummary() {
		appRepository.updateMoSummary("app_id_1");
		ReceivedMoSummary moSummary = appRepository.getMoSummary("app_id_1");
		assertNotNull(moSummary);
		assertEquals("appId", "app_id_1", moSummary.getAppId());
		assertEquals("Total count", 1, moSummary.getMoCount());

		appRepository.updateMoSummary("app_id_1");
		moSummary = appRepository.getMoSummary("app_id_1");
		assertNotNull(moSummary);
		assertEquals("appId", "app_id_1", moSummary.getAppId());
		assertEquals("Total count", 2, moSummary.getMoCount());

		appRepository.updateMoSummary("app_id_2");
		moSummary = appRepository.getMoSummary("app_id_1");
		assertNotNull(moSummary);
		assertEquals("appId", "app_id_1", moSummary.getAppId());
		assertEquals("Total count", 2, moSummary.getMoCount());
		moSummary = appRepository.getMoSummary("app_id_2");
		assertNotNull(moSummary);
		assertEquals("appId", "app_id_2", moSummary.getAppId());
		assertEquals("Total count", 1, moSummary.getMoCount());
	}

	@Test
	public void testFindExpiredAppNameSucess() throws Exception {
		ApplicationImpl app = ApplicationUtil.createApplication("SUBS-APP-EXPIRED", "SubsAppExpired");
		app.setStartDate(new DateTime().minusDays(30));
		app.setEndDate(new DateTime().minusDays(10));
		app.setService(ServiceUtil.createSubsctiptionService(null));
		appRepository.createApplication(app);
		assertTrue("If inserted Id should be greater that 0", app.getId() > 0);

		final List<Application> applications = appRepository.findExpiredAppName();
		assertTrue("App should be selected", applications.size() >= 0);
	}

}
