/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.orm.repository;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class ContextLoader {
	private static ContextLoader contextLoaderInstance;
	private ApplicationContext context;

	private ContextLoader() {

	}

	public synchronized static ContextLoader getInstance() {
		if (contextLoaderInstance == null) {
			contextLoaderInstance = new ContextLoader();
			contextLoaderInstance.initAppRepo();
		}

		return contextLoaderInstance;
	}

	private void initAppRepo() {
		context = new ClassPathXmlApplicationContext("spring-test-context.xml");
	}

	public ApplicationContext getContext() {
		return context;
	}

}
