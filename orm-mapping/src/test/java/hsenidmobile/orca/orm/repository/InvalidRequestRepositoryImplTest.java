/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.orm.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import hsenidmobile.orca.core.model.InvalidRequest;
import hsenidmobile.orca.core.model.Msisdn;

import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class InvalidRequestRepositoryImplTest {

	private InvalidRequest invalidRequest;
	private InvalidRequestRepositoryImpl invalidRequestRepository;
	private static ApplicationContext context;

	@BeforeClass
	public static void loadContext() {
		context = ContextLoader.getInstance().getContext();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {

		invalidRequestRepository = (InvalidRequestRepositoryImpl) context.getBean("invalidRequestRepository");

		invalidRequest = new InvalidRequest(1L);
		invalidRequest.setAppId("app_id_1");
		invalidRequest.setMessageText("Sample invalid request");
		invalidRequest.setSourceMsisdn(new Msisdn("773687965", "DIALOG"));
		invalidRequest.setReceivedTime(System.currentTimeMillis());

	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		SessionFactory sf = (SessionFactory) context.getBean("hibernateSessionFactory");
		final Session session = sf.openSession();
		Transaction transaction = session.beginTransaction();
		try {
			SQLQuery query = session.createSQLQuery("DELETE FROM invalid_request_summary");
			query.executeUpdate();
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			throw e;
		} finally {
			session.close();

		}

	}

	/**
	 * Test method for
	 * {@link hsenidmobile.orca.orm.repository.InvalidRequestRepositoryImpl#getInvalidRequestSummary(java.lang.String)}
	 * .
	 */
	@Test
	public void testGetInvalidRequestSummary() {
		invalidRequestRepository.incrementInvalidRequestSummary("app_id_1");

		int count = invalidRequestRepository.getInvalidRequestSummary("app_id_1");
		assertEquals("summary count", 1, count);

		invalidRequestRepository.incrementInvalidRequestSummary("app_id_1");

		count = invalidRequestRepository.getInvalidRequestSummary("app_id_1");
		assertEquals("summary count", 2, count);
	}


}
