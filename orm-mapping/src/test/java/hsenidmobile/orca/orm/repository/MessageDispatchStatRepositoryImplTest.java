/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.orm.repository;

import static org.junit.Assert.assertEquals;
import hsenidmobile.orca.core.model.DispatchSummary;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author sandarenu
 * @version $LastChangedRevision$
 */
public class MessageDispatchStatRepositoryImplTest {

	private MessageDispatchStatRepositoryImpl dispatchStatRepositoryImpl;
	private static ApplicationContext context;

	@BeforeClass
	public static void loadContext() {
		context = ContextLoader.getInstance().getContext();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {

		this.dispatchStatRepositoryImpl = (MessageDispatchStatRepositoryImpl) context
				.getBean("dispatchStatRepositoryImpl");
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		SessionFactory sf = (SessionFactory) context.getBean("hibernateSessionFactory");
		final Session session = sf.openSession();
		Transaction transaction = session.beginTransaction();
		try {
			SQLQuery query = session.createSQLQuery("DELETE FROM message_dispatching_summary");
			query.executeUpdate();
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			throw e;
		} finally {
			session.close();

		}
		// this.hibernateManager.update("DELETE FROM message_dispatching_summary",
		// null);
	}

	/**
	 * Test method for
	 * {@link hsenidmobile.orca.orm.repository.MessageDispatchStatRepositoryImpl#updateDispatchSummary(java.lang.String, java.lang.String)}
	 * .
	 */
	@Test
	public void testUpdateDispatchSummary() {
		this.dispatchStatRepositoryImpl.updateDispatchSummary("app_id", "SMS-MT-2000");

		List<DispatchSummary> dispatchSummary = this.dispatchStatRepositoryImpl.getDispatchSummary("app_id");
		assertEquals("Summary size", 1, dispatchSummary.size());
		assertEquals("Count size", 1, dispatchSummary.get(0).getTotalCount());
		assertEquals("AppId", "app_id", dispatchSummary.get(0).getAppId());
		assertEquals("Status Code", "SMS-MT-2000", dispatchSummary.get(0).getStatusCode());

		this.dispatchStatRepositoryImpl.updateDispatchSummary("app_id", "SMS-MT-2000");

		dispatchSummary = this.dispatchStatRepositoryImpl.getDispatchSummary("app_id");
		assertEquals("Summary size", 1, dispatchSummary.size());
		assertEquals("Count size", 2, dispatchSummary.get(0).getTotalCount());
		assertEquals("AppId", "app_id", dispatchSummary.get(0).getAppId());
		assertEquals("Status Code", "SMS-MT-2000", dispatchSummary.get(0).getStatusCode());

	}

}
