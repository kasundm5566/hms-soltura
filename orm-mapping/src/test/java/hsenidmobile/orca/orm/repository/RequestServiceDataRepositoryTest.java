/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hsenidmobile.orca.orm.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import hsenidmobile.orca.core.applications.ApplicationImpl;
import hsenidmobile.orca.core.applications.request.RequestService;
import hsenidmobile.orca.core.applications.request.RequestServiceData;
import hsenidmobile.orca.core.model.Application;
import hsenidmobile.orca.core.model.Msisdn;
import hsenidmobile.orca.orm.test.util.ApplicationUtil;
import hsenidmobile.orca.orm.test.util.ServiceUtil;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;

/**
 * $LastChangedDate$
 * $LastChangedBy$ $LastChangedRevision$
 */
public class RequestServiceDataRepositoryTest {

	RequestServiceDataRepositoryImpl requestServiceDataRepository;
	private AppRepositoryImpl appRepository;

	private static ApplicationContext context;
	private RequestServiceData requestServiceData;

	@BeforeClass
	public static void loadContext() {
		context = ContextLoader.getInstance().getContext();
	}

	@Before
	public void setUp() throws Exception {
		requestServiceDataRepository = (RequestServiceDataRepositoryImpl) context
				.getBean("requestServiceDataRepository");
		appRepository = (AppRepositoryImpl) context.getBean("appRepository");
	}

	@After
	public void tearDown() throws Exception {
		if (requestServiceData != null && requestServiceData.getId() != null) {
			delete(requestServiceData);
		}
	}

	@Test
	public void testAddRequest() throws Exception {
		requestServiceData = new RequestServiceData();
		requestServiceData.setAppId("TB_APP1");
		requestServiceData.setAppName("testAppName");
		requestServiceData.setMessageId(1234567);
		requestServiceData.setMsisdn(new Msisdn("9477123456789", "operator1"));
		requestServiceData.setReceivedMessage("Test Request Message");
		requestServiceData.setReceivedTime(new Date().getTime());
		requestServiceDataRepository.add(requestServiceData);
		assertTrue("Id should be greater than 0 if saved", requestServiceData.getId() > 0);
	}

	@Test
	public void testGetTotalMessageCount() throws Exception {
		ApplicationImpl application = ApplicationUtil.createApplication("REQ-APP-ID9", "ReqsApp9");
		application.setService(ServiceUtil.createRequestShowService(null));
		appRepository.createApplication(application);

		long count = requestServiceDataRepository.getTotalMessageCount("REQ-APP-ID9");
		assertEquals("Count before adding any requests", 0, count);

		RequestServiceData requestServiceData1 = new RequestServiceData();
		requestServiceData1.setAppId("REQ-APP-ID9");
		requestServiceData1.setMessageId(1234578454);
		requestServiceData1.setReceivedMessage("test message");
		requestServiceData1.setReceivedTime(124645);
		requestServiceData1.setMsisdn(new Msisdn("0773687965", "ET"));

		requestServiceDataRepository.add(requestServiceData1);

		count = requestServiceDataRepository.getTotalMessageCount("REQ-APP-ID9");
		assertEquals("Count after adding a request", 1, count);


	}

	@Test
	public void testGetRequestMessages_ForAppWithOutSubCategory() throws Exception {

		ApplicationImpl application = ApplicationUtil.createApplication("REQ-APP-ID10", "ReqsApp10");
		application.setService(ServiceUtil.createRequestShowService(null));
		appRepository.createApplication(application);

		Application fetchedApp = appRepository.findAppByAppId("REQ-APP-ID10");

		RequestServiceData requestServiceData1 = new RequestServiceData();
		requestServiceData1.setAppId("REQ-APP-ID10");
		requestServiceData1.setMessageId(1234578454);
		requestServiceData1.setReceivedMessage("test message");
		requestServiceData1.setReceivedTime(124645);
		requestServiceData1.setMsisdn(new Msisdn("0773687965", "ET"));

		requestServiceDataRepository.add(requestServiceData1);

		List<RequestServiceData> requestServiceDataList = requestServiceDataRepository.getRequestMessages(
				"REQ-APP-ID10", (RequestService) fetchedApp.getService(), 0, 10);
		assertEquals("Checking the message count", 1, requestServiceDataList.size());
		assertEquals("Checking the message ", "test message", requestServiceDataList.get(0).getReceivedMessage());
	}

	@Test
	public void testGetRequestMessagesBySubCategory_ForAppWithSubCategory() throws Exception {
		ApplicationImpl application = ApplicationUtil.createApplication("REQ-APP-ID11", "ReqsApp11");
		application.setService(ServiceUtil.createRequestShowService(Arrays.asList(
				ApplicationUtil.createSubCategory("cat1"), ApplicationUtil.createSubCategory("cat2"))));
		appRepository.createApplication(application);

		Application fetchedApp = appRepository.findAppByAppId("REQ-APP-ID11");

		RequestServiceData requestServiceData1 = new RequestServiceData();
		requestServiceData1.setAppId("REQ-APP-ID11");
		requestServiceData1.setMessageId(1234578454);
		requestServiceData1.setReceivedMessage("test message with subkeyword cat1");
		requestServiceData1.setSubCategory(fetchedApp.getService().getSubcategory("cat1"));
		requestServiceData1.setReceivedTime(124645);
		requestServiceData1.setMsisdn(new Msisdn("0773687965", "ET"));

		RequestServiceData requestServiceData2 = new RequestServiceData();
		requestServiceData2.setAppId("REQ-APP-ID11");
		requestServiceData2.setMessageId(1234578455);
		requestServiceData2.setReceivedMessage("test message with subkeyword cat2");
		requestServiceData2.setSubCategory(fetchedApp.getService().getSubcategory("cat2"));
		requestServiceData2.setReceivedTime(124645);
		requestServiceData2.setMsisdn(new Msisdn("0773687965", "ET"));

		requestServiceDataRepository.add(requestServiceData1);
		requestServiceDataRepository.add(requestServiceData2);

		List<RequestServiceData> requestServiceDataList = requestServiceDataRepository
				.getAllRequestMessagesBySubCategory(
						"REQ-APP-ID11",
						new String[] { String.valueOf(fetchedApp.getService().getSubcategory("cat1").getId()),
								String.valueOf(fetchedApp.getService().getSubcategory("cat2").getId()) });
		assertEquals("Checking the message count", 2, requestServiceDataList.size());
		assertEquals("Checking the message ", "test message with subkeyword cat1", requestServiceDataList.get(0)
				.getReceivedMessage());
		assertEquals("Checking the message ", "test message with subkeyword cat2", requestServiceDataList.get(1)
				.getReceivedMessage());
	}

	@Test
	public void testFindRequestServiceData() throws Exception {
		RequestServiceData requestServiceData = new RequestServiceData();
		requestServiceData.setAppId("TB_APP1");
		requestServiceData.setAppName("testAppName");
		requestServiceData.setMessageId(1234568);
		requestServiceData.setMsisdn(new Msisdn("9477123456789", "operator1"));
		requestServiceData.setReceivedMessage("Test Request Message");
		requestServiceData.setReceivedTime(new Date().getTime());
		requestServiceDataRepository.add(requestServiceData);
		RequestServiceData fetchedData = requestServiceDataRepository.findRequestServiceData(1234568);
		assertNotNull(fetchedData);
		assertEquals("Checking the application name", "testAppName", fetchedData.getAppName());
		delete(fetchedData);
	}

	@Test
	public void testUpdateRequestServiceData() throws Exception {
		RequestServiceData requestServiceData = new RequestServiceData();
		requestServiceData.setAppId("TB_APP1");
		requestServiceData.setAppName("testAppName");
		requestServiceData.setMessageId(1234569);
		requestServiceData.setMsisdn(new Msisdn("9477123456789", "operator1"));
		requestServiceData.setReceivedMessage("Test Request Message");
		requestServiceData.setReceivedTime(new Date().getTime());
		requestServiceDataRepository.add(requestServiceData);
		RequestServiceData fetchedData = requestServiceDataRepository.findRequestServiceData(1234569);
		fetchedData.setRead(true);
		requestServiceDataRepository.updateRequestServiceData(fetchedData);
		RequestServiceData readAfterUpdate = requestServiceDataRepository.findRequestServiceData(1234569);
		assertEquals("Checking the application read status", true, readAfterUpdate.isRead());
		delete(readAfterUpdate);
	}

	private void delete(Object obj) {
		SessionFactory sf = (SessionFactory) context.getBean("hibernateSessionFactory");
		final Session session = sf.openSession();
		Transaction transaction = session.beginTransaction();
		try {
			session.delete(obj);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			throw e;
		} finally {
			session.close();

		}
	}
}
