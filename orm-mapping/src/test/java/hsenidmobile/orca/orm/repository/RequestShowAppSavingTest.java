/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hsenidmobile.orca.orm.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import hsenidmobile.orca.core.applications.ApplicationImpl;
import hsenidmobile.orca.core.applications.request.RequestService;
import hsenidmobile.orca.core.model.Application;
import hsenidmobile.orca.core.model.LocalizedMessage;
import hsenidmobile.orca.core.model.SubCategory;
import hsenidmobile.orca.orm.test.util.ApplicationUtil;
import hsenidmobile.orca.orm.test.util.ServiceUtil;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;

/**
 * $LastChangedDate$ $LastChangedBy$ $LastChangedRevision$
 *
 */
public class RequestShowAppSavingTest {
	private AppRepositoryImpl appRepository;
	private static ApplicationContext context;

	@BeforeClass
	public static void loadContext() {
		context = ContextLoader.getInstance().getContext();
	}

	@Before
	public void setUp() throws Exception {
		appRepository = (AppRepositoryImpl) context.getBean("appRepository");
	}

	@Test
	public void testCreateApplication_WithoutSubCategories() throws Exception {
		ApplicationImpl app = ApplicationUtil.createApplication("REQ-APP-ID1", "ReqApp1");
		app.setService(ServiceUtil.createRequestShowService(null));
		appRepository.createApplication(app);
		assertTrue("If inserted Id should be greater that 0", app.getId() > 0);

		Application savedApp = appRepository.findAppByAppId("REQ-APP-ID1");

		assertEquals(app, savedApp);

	}

	@Test
	public void testCreateApplication_WithSubCategories() throws Exception {
		ApplicationImpl app = ApplicationUtil.createApplication("REQ-APP-ID2", "ReqsApp2");
		app.setService(ServiceUtil.createRequestShowService(Arrays.asList(ApplicationUtil.createSubCategory("cat1"))));
		appRepository.createApplication(app);
		assertTrue("If inserted Id should be greater that 0", app.getId() > 0);

		Application savedApp = appRepository.findAppByAppId("REQ-APP-ID2");

		assertEquals(app, savedApp);

	}
}
