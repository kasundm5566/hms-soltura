/* 
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited. 
 *   All Rights Reserved. 
 * 
 *   These materials are unpublished, proprietary, confidential source code of 
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET 
 *   of hSenid Software International (Pvt) Limited. 
 * 
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual 
 *   property rights in these materials. 
 *
 */
package hsenidmobile.orca.orm.repository;

import hsenidmobile.orca.core.model.OrcaSdpAppIntegration;
import hsenidmobile.orca.core.model.SdpAppLoginData;
import hsenidmobile.orca.orm.sdp.SdpApplicationRepositoryImpl;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;

import static org.junit.Assert.*;
/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class SdpApplicationRepositoryTest {

    // @Autowired
    private SdpApplicationRepositoryImpl sdpApplicationRepository;

    private OrcaSdpAppIntegration orcaSdpAppIntegration;
    private static ApplicationContext context;

    @BeforeClass
    public static void loadContext() {
        context = ContextLoader.getInstance().getContext();
    }

    @Before
    public void setUp() throws Exception {
        orcaSdpAppIntegration = new OrcaSdpAppIntegration("orca_app_id", "sdp_app_id", "sdp_app_password", "spId" );
        sdpApplicationRepository = (SdpApplicationRepositoryImpl) context.getBean("sdpApplicationRepository");
    }

    public void tearDown() {
        if (orcaSdpAppIntegration != null && orcaSdpAppIntegration.getId() != null) {
            delete(orcaSdpAppIntegration);
        }
    }

    private void delete(Object obj) {
        SessionFactory sf = (SessionFactory) context.getBean("hibernateSessionFactory");
        final Session session = sf.openSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.delete(obj);
            transaction.commit();
        } catch (HibernateException e) {
            transaction.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @Test
    public void testAddOrcaSdpAppIntegrationData() throws Exception {
        sdpApplicationRepository.addOrcaSdpAppIntegrationData(orcaSdpAppIntegration);
        assertTrue("If inserted Id should be greater that 0", orcaSdpAppIntegration.getId() > 0);
        delete(orcaSdpAppIntegration);
    }

    @Test
    public void testGetSdpAppLoginDataForOrcaApp() throws Exception {
        sdpApplicationRepository.addOrcaSdpAppIntegrationData(orcaSdpAppIntegration);
        SdpAppLoginData sdpAppLoginData = sdpApplicationRepository.getSdpAppLoginDataForOrcaApp("orca_app_id");
        assertNotNull("If read sdpAppLoginData should not be null ", sdpAppLoginData);
        assertEquals("If read sdp_app_id should equal to ", sdpAppLoginData.getAppId(), "sdp_app_id");
        delete(orcaSdpAppIntegration);
    }

}
