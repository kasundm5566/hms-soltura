/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hsenidmobile.orca.orm.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import hsenidmobile.orca.core.applications.ApplicationImpl;
import hsenidmobile.orca.core.applications.PeriodUnit;
import hsenidmobile.orca.core.applications.subscription.Periodicity;
import hsenidmobile.orca.core.applications.subscription.Subscription;
import hsenidmobile.orca.core.applications.subscription.SubscriptionContentRelease;
import hsenidmobile.orca.core.model.Application;
import hsenidmobile.orca.core.model.Content;
import hsenidmobile.orca.core.model.Content.ContentSource;
import hsenidmobile.orca.core.model.ContentRelease;
import hsenidmobile.orca.core.model.LocalizedMessage;
import hsenidmobile.orca.core.model.SubCategory;
import hsenidmobile.orca.orm.test.util.ApplicationUtil;
import hsenidmobile.orca.orm.test.util.ServiceUtil;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;

/**
 * $LastChangedDate$
 * $LastChangedBy$ $LastChangedRevision$
 *
 */

public class SubscriptionAppSavingTest {
	private AppRepositoryImpl appRepository;
	private static ApplicationContext context;

	@BeforeClass
	public static void loadContext() {
		context = ContextLoader.getInstance().getContext();
	}

	@Before
	public void setUp() throws Exception {
		appRepository = (AppRepositoryImpl) context.getBean("appRepository");
	}

	@Test
	public void testCreateApplication_WithoutSubCategories() throws Exception {
		ApplicationImpl app = ApplicationUtil.createApplication("SUBS-APP-ID1", "SubsApp1");
		app.setService(ServiceUtil.createSubsctiptionService(null));
		appRepository.createApplication(app);
		assertTrue("If inserted Id should be greater that 0", app.getId() > 0);

		Application savedApp = appRepository.findAppByAppId("SUBS-APP-ID1");

		assertEquals(app, savedApp);

	}

	@Test
	public void testCreateApplication_WithSubCategories() throws Exception {
		ApplicationImpl app = ApplicationUtil.createApplication("SUBS-APP-ID2", "SubsApp2");
		app.setService(ServiceUtil.createSubsctiptionService(Arrays.asList(ApplicationUtil.createSubCategory("cat1"))));
		appRepository.createApplication(app);
		assertTrue("If inserted Id should be greater that 0", app.getId() > 0);

		Application savedApp = appRepository.findAppByAppId("SUBS-APP-ID2");

		assertEquals(app, savedApp);

	}

}
