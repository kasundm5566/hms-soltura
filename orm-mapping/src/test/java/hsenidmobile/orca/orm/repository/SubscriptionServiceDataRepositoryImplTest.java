/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hsenidmobile.orca.orm.repository;

import static org.junit.Assert.*;
import hsenidmobile.orca.core.applications.ApplicationImpl;
import hsenidmobile.orca.core.applications.exception.ApplicationException;
import hsenidmobile.orca.core.applications.subscription.Periodicity;
import hsenidmobile.orca.core.applications.subscription.Subscription;
import hsenidmobile.orca.core.applications.subscription.SubscriptionContentRelease;
import hsenidmobile.orca.core.applications.subscription.SubscriptionContentRelease.ContentReleaseStatus;
import hsenidmobile.orca.core.model.Application;
import hsenidmobile.orca.core.model.Content;
import hsenidmobile.orca.core.model.LocalizedMessage;
import hsenidmobile.orca.core.model.SubCategory;
import hsenidmobile.orca.core.model.Content.ContentSource;
import hsenidmobile.orca.orm.test.util.ApplicationUtil;
import hsenidmobile.orca.orm.test.util.ServiceUtil;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;

/**
 * $LastChangedDate$ $LastChangedBy$ $LastChangedRevision$
 *
 */
public class SubscriptionServiceDataRepositoryImplTest {

	private SubscriptionServiceDataRepositoryImpl subsRepo;
	private AppRepositoryImpl appRepository;
	private static ApplicationContext context;

	@BeforeClass
	public static void loadContext() {
		context = ContextLoader.getInstance().getContext();

	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		subsRepo = (SubscriptionServiceDataRepositoryImpl) context.getBean("subscriptionServiceDataRepository");
		appRepository = (AppRepositoryImpl) context.getBean("appRepository");
	}

	private SubscriptionContentRelease createContentRelease(Long id, String appId, Periodicity periodicity,
			List<Content> content, DateTime scheduledTime) {
		SubscriptionContentRelease scheduledContent1 = new SubscriptionContentRelease();
		scheduledContent1.setId(id);
		scheduledContent1.setAppId(appId);
		scheduledContent1.setAppName(appId);
		scheduledContent1.setContents(content);
		scheduledContent1.setPeriodicity(periodicity);
		scheduledContent1.setScheduledDispatchTimeFrom(scheduledTime);
		scheduledContent1.setScheduledDispatchTimeTo(scheduledContent1.getScheduledDispatchTimeFrom().plusMinutes(
				periodicity.getAllowedBufferTime()));

		return scheduledContent1;
	}

	private Content createContent(String content, SubCategory subCategory) {
		Content con1 = new Content();
		con1.setContent(new LocalizedMessage(content, Locale.ENGLISH));
		con1.setSource(ContentSource.WEB);
		con1.setSubCategory(subCategory);

		return con1;
	}

	// /**
	// * Test method for
	// * {@link
	// hsenidmobile.orca.orm.repository.SubscriptionServiceDataRepositoryImpl#addContentRelease(hsenidmobile.orca.core.applications.subscription.SubscriptionContentRelease)}
	// * .
	// */
	// @Test
	// public void testAddContentRelease() {
	// fail("Not yet implemented");
	// }

	/**
	 * Test method for
	 * {@link hsenidmobile.orca.orm.repository.SubscriptionServiceDataRepositoryImpl#addOrUpdateContent(java.util.List)}
	 * .
	 *
	 * @throws ApplicationException
	 */
	@Test
	public void testAddOrUpdateContent() throws ApplicationException {
		ApplicationImpl app = ApplicationUtil.createApplication("SUBS-REPO-APP-ID1", "SubsRepoApp1");
		app.setService(ServiceUtil.createSubsctiptionService(Arrays.asList(ApplicationUtil.createSubCategory("cat1"))));
		appRepository.createApplication(app);

		Periodicity periodicity = ((Subscription) app.getService()).getDefaultPeriodicity();
		final SubscriptionContentRelease release1 = createContentRelease(null, "SUBS-REPO-APP-ID1", periodicity,
				Arrays.asList(createContent("Content1", app.getService().getSubcategory("cat1"))), new DateTime(2010,
						10, 13, 8, 30, 0, 0));
		final SubscriptionContentRelease release2 = createContentRelease(null, "SUBS-REPO-APP-ID1", periodicity,
				Arrays.asList(createContent("Content2", app.getService().getSubcategory("cat1"))), new DateTime(2010,
						10, 14, 8, 30, 0, 0));

		subsRepo.addOrUpdateContent(Arrays.asList(release1, release2));

		assertTrue("If saved id should be there", release1.getId() > 0);
		assertTrue("If saved id should be there", release2.getId() > 0);

	}

	// /**
	// * Test method for
	// * {@link
	// hsenidmobile.orca.orm.repository.SubscriptionServiceDataRepositoryImpl#getDispatchableContent(java.lang.String,
	// org.joda.time.DateTime)}
	// * .
	// */
	// @Test
	// public void testGetDispatchableContent() {
	// fail("Not yet implemented");
	// }
	//

	/**
	 * Test method for
	 * {@link hsenidmobile.orca.orm.repository.SubscriptionServiceDataRepositoryImpl#getUpdatableContents(java.lang.String, org.joda.time.DateTime)}
	 * .
	 *
	 * @throws ApplicationException
	 */
	@Test
	public void testGetUpdatableContents() throws ApplicationException {
		ApplicationImpl app = ApplicationUtil.createApplication("SUBS-REPO-APP-ID2", "SubsRepoApp2");
		app.setService(ServiceUtil.createSubsctiptionService(Arrays.asList(ApplicationUtil.createSubCategory("cat1"))));
		appRepository.createApplication(app);

		Periodicity periodicity = ((Subscription) app.getService()).getDefaultPeriodicity();
		final SubscriptionContentRelease release1 = createContentRelease(null, "SUBS-REPO-APP-ID2", periodicity,
				Arrays.asList(createContent("Content1", app.getService().getSubcategory("cat1"))), new DateTime(2010,
						10, 12, 8, 30, 0, 0));
		final SubscriptionContentRelease release2 = createContentRelease(null, "SUBS-REPO-APP-ID2", periodicity,
				Arrays.asList(createContent("Content2", app.getService().getSubcategory("cat1"))), new DateTime(2010,
						10, 13, 8, 30, 0, 0));
		release1.setStatus(ContentReleaseStatus.SENT);
		final SubscriptionContentRelease release3 = createContentRelease(null, "SUBS-REPO-APP-ID2", periodicity,
				Arrays.asList(createContent("Content3", app.getService().getSubcategory("cat1"))), new DateTime(2010,
						10, 14, 8, 30, 0, 0));

		subsRepo.addOrUpdateContent(Arrays.asList(release1, release2, release3));

		List<SubscriptionContentRelease> updatableContents = subsRepo.getUpdatableContents("SUBS-REPO-APP-ID2",
				new DateTime(2010, 10, 13, 8, 30, 0, 0));
		assertNotNull("updatableContents cannot be null", updatableContents);
		assertEquals(1, updatableContents.size());
		assertEquals("Content of updatable release", "Content3", updatableContents.get(0).getContent("cat1")
				.getContent().getMessage());

		updatableContents = subsRepo.getUpdatableContents("SUBS-REPO-APP-ID2", new DateTime(2010, 10, 13, 7, 30, 0, 0));
		assertNotNull("updatableContents cannot be null", updatableContents);
		assertEquals(2, updatableContents.size());
		assertEquals("Content of updatable release", "Content2", updatableContents.get(0).getContent("cat1")
				.getContent().getMessage());
		assertEquals("Content of updatable release", "Content3", updatableContents.get(1).getContent("cat1")
				.getContent().getMessage());
	}

	/**
	 * Test method for
	 * {@link hsenidmobile.orca.orm.repository.SubscriptionServiceDataRepositoryImpl#getNextUpdatableContent(java.lang.String, org.joda.time.DateTime)}
	 * .
	 *
	 * @throws ApplicationException
	 */
	@Test
	public void testGetNextUpdatableContent() throws ApplicationException {
		ApplicationImpl app = ApplicationUtil.createApplication("SUBS-REPO-APP-ID3", "SubsRepoApp3");
		app.setService(ServiceUtil.createSubsctiptionService(Arrays.asList(ApplicationUtil.createSubCategory("cat1"))));
		appRepository.createApplication(app);

		Periodicity periodicity = ((Subscription) app.getService()).getDefaultPeriodicity();
		final SubscriptionContentRelease release1 = createContentRelease(null, "SUBS-REPO-APP-ID3", periodicity,
				Arrays.asList(createContent("Content1", app.getService().getSubcategory("cat1"))), new DateTime(2010,
						10, 12, 8, 30, 0, 0));
		final SubscriptionContentRelease release2 = createContentRelease(null, "SUBS-REPO-APP-ID3", periodicity,
				Arrays.asList(createContent("Content2", app.getService().getSubcategory("cat1"))), new DateTime(2010,
						10, 13, 8, 30, 0, 0));
		final SubscriptionContentRelease release3 = createContentRelease(null, "SUBS-REPO-APP-ID3", periodicity,
				Arrays.asList(createContent("Content3", app.getService().getSubcategory("cat1"))), new DateTime(2010,
						10, 14, 8, 30, 0, 0));

		subsRepo.addOrUpdateContent(Arrays.asList(release1, release2, release3));

		final SubscriptionContentRelease updatableContent = subsRepo.getNextUpdatableContent("SUBS-REPO-APP-ID3",
				new DateTime(2010, 10, 13, 7, 30, 0, 0));
		assertNotNull("updatableContent cannot be null", updatableContent);
		assertEquals("Content of updatable release", "Content2", updatableContent.getContent("cat1").getContent()
				.getMessage());

	}

	/**
	 * Test method for
	 * {@link hsenidmobile.orca.orm.repository.SubscriptionServiceDataRepositoryImpl#getDispatchableContents(org.joda.time.DateTime)}
	 * .
	 * @throws ApplicationException
	 */
	@Test
	public void testGetDispatchableContents() throws ApplicationException {
		ApplicationImpl app = ApplicationUtil.createApplication("SUBS-REPO-APP-ID4", "SubsRepoApp4");
		app.setService(ServiceUtil.createSubsctiptionService(Arrays.asList(ApplicationUtil.createSubCategory("cat1"))));
		appRepository.createApplication(app);
		Periodicity periodicity = ((Subscription) app.getService()).getDefaultPeriodicity();

		SubscriptionContentRelease release1 = createContentRelease(null, "SUBS-REPO-APP-ID4", periodicity,
				Arrays.asList(createContent("Content1", app.getService().getSubcategory("cat1"))), new DateTime(2010,
						10, 16, 8, 30, 0, 0));

		SubscriptionContentRelease release2 = createContentRelease(null, "SUBS-REPO-APP-ID4", periodicity,
				Arrays.asList(createContent("Content2", app.getService().getSubcategory("cat1"))), new DateTime(2010,
						10, 17, 8, 30, 0, 0));

		SubscriptionContentRelease release3 = createContentRelease(null, "SUBS-REPO-APP-ID4", periodicity,
				Arrays.asList(createContent("Content3", app.getService().getSubcategory("cat1"))), new DateTime(2010,
						10, 18, 8, 30, 0, 0));

		subsRepo.addOrUpdateContent(Arrays.asList(release1, release2, release3));

		List<SubscriptionContentRelease> dispatchableContents = subsRepo.getDispatchableContents(new DateTime(2010,10, 16, 8, 30, 0, 0), 10);
		assertEquals("dispatchable size", 1, dispatchableContents.size());
		assertEquals("dispatchable release", "Content1", dispatchableContents.get(0).getContent("cat1").getContent()
				.getMessage());

		dispatchableContents = subsRepo.getDispatchableContents(new DateTime(2010,10, 17, 7, 30, 0, 0), 10);
		assertEquals("dispatchable size", 0, dispatchableContents.size());

		dispatchableContents = subsRepo.getDispatchableContents(new DateTime(2010,10, 18, 9, 30, 0, 0), 10);
		assertEquals("dispatchable size", 1, dispatchableContents.size());
		assertEquals("dispatchable release", "Content3", dispatchableContents.get(0).getContent("cat1").getContent()
				.getMessage());


	}

	//
	// /**
	// * Test method for
	// * {@link
	// hsenidmobile.orca.orm.repository.SubscriptionServiceDataRepositoryImpl#updateContentRelease(hsenidmobile.orca.core.applications.subscription.SubscriptionContentRelease)}
	// * .
	// */
	// @Test
	// public void testUpdateContentRelease() {
	// fail("Not yet implemented");
	// }

}
