/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate: $
 *   $LastChangedBy: $
 *   $LastChangedRevision: $
 *
 */
package hsenidmobile.orca.orm.repository;

import hsenidmobile.orca.core.applications.exception.ApplicationException;
import hsenidmobile.orca.core.applications.voting.Vote;
import hsenidmobile.orca.core.applications.voting.VoteResultSummary;
import hsenidmobile.orca.core.applications.voting.VotingService;
import hsenidmobile.orca.core.model.Application;
import hsenidmobile.orca.core.model.Msisdn;
import hsenidmobile.orca.core.model.SubCategory;
import hsenidmobile.orca.core.repository.AppRepository;
import hsenidmobile.orca.core.repository.VoteRepository;
import hsenidmobile.orca.orm.test.util.ApplicationUtil;
import hsenidmobile.orca.orm.test.util.ServiceUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


/**
 * $LastChangedDate: $ $LastChangedBy: $ $LastChangedRevision: $
 *
 */
public class VoteRepositoryTest {
	private static final Logger logger = LoggerFactory.getLogger(VoteRepositoryTest.class);
	private VoteRepository voteRepository;
	private AppRepository appRepository;
	private static ApplicationContext context;

	@BeforeClass
	public static void loadContext() {
		context = ContextLoader.getInstance().getContext();
	}

	@Before
	public void setUp() throws Exception {
		voteRepository = (VoteRepositoryImpl) context.getBean("voteRepository");
		appRepository = (AppRepositoryImpl) context.getBean("appRepository");

	}

	private void delete(Object obj) {
		SessionFactory sf = (SessionFactory) context.getBean("hibernateSessionFactory");
		final Session session = sf.openSession();
		Transaction transaction = session.beginTransaction();
		try {
			session.delete(obj);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			throw e;
		} finally {
			session.close();

		}
	}

	private Object read(final Class clazz, final Serializable serializable) {
		SessionFactory sf = (SessionFactory) context.getBean("hibernateSessionFactory");
		final Session session = sf.openSession();
		Transaction transaction = session.beginTransaction();
		try {
			Object obj = session.load(clazz, serializable);
			transaction.commit();
			return obj;
		} catch (HibernateException e) {
			transaction.rollback();
			throw e;
		} finally {
			session.close();

		}
	}

	@Test
	public void testUpdateSummary_WithSubCategory() throws ApplicationException {
		Application voteApplication = ApplicationUtil.createApplication("VOTING-APP-ID10", "VotingApp10");
		SubCategory candidate1 = ApplicationUtil.createSubCategory("can1");
		SubCategory candidate2 = ApplicationUtil.createSubCategory("can2");
		voteApplication.setService(ServiceUtil.createVotingService(Arrays.asList(candidate1, candidate2)));

		appRepository.createApplication(voteApplication);

		VotingService service = (VotingService) voteApplication.getService();
		Vote vote = new Vote();
		vote.setCandidate(service.getSubcategory("can1"));

		voteRepository.updateSummary(((VotingService) voteApplication.getService()).getId(), vote);
	}

	@Test
	public void testGetVotingSummary() throws ApplicationException {
		Application voteApplication = ApplicationUtil.createApplication("VOTING-APP-ID11", "VotingApp11");
		SubCategory candidate1 = ApplicationUtil.createSubCategory("can1");
		SubCategory candidate2 = ApplicationUtil.createSubCategory("can2");
		voteApplication.setService(ServiceUtil.createVotingService(Arrays.asList(candidate1, candidate2)));
		appRepository.createApplication(voteApplication);

		VotingService service = (VotingService) voteApplication.getService();
		Long voteletId = ((VotingService) voteApplication.getService()).getId();
		Vote vote = new Vote();
		vote.setServiceId(voteletId);
		vote.setCandidate(service.getSubcategory("can1"));

		voteRepository.updateSummary(voteletId, vote);

		List<VoteResultSummary> summary = ((VoteRepositoryImpl) voteRepository).getVotingSummary(voteletId);
		for (VoteResultSummary voteResultSummary : summary) {
			logger.debug(voteResultSummary.toString());
		}
		assertEquals("Summary List size", 1, summary.size());

	}

	@Test
	public void testHasVoted() throws ApplicationException {
		Application voteApplication = ApplicationUtil.createApplication("VOTING-APP-ID12", "VotingApp12");
		SubCategory candidate1 = ApplicationUtil.createSubCategory("can1");
		SubCategory candidate2 = ApplicationUtil.createSubCategory("can2");
		voteApplication.setService(ServiceUtil.createVotingService(Arrays.asList(candidate1, candidate2)));
		appRepository.createApplication(voteApplication);

		VotingService service = (VotingService) voteApplication.getService();
		Long voteletId = ((VotingService) voteApplication.getService()).getId();

		Vote vote = new Vote();
		Msisdn msisdn = new Msisdn("0773687965", "DIALOG");
		vote.setMsisdn(msisdn);
		vote.setServiceId(voteletId);
		vote.setCandidate(service.getSubcategory("can1"));

		voteRepository.add(vote);

		assertTrue("There should be a vote", voteRepository.hasVoted(msisdn, voteletId));

	}
}
