/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hsenidmobile.orca.orm.test.util;

import hsenidmobile.orca.core.applications.ApplicationImpl;
import hsenidmobile.orca.core.applications.policy.RegistrationPolicy;
import hsenidmobile.orca.core.model.ApplicationRoutingInfo;
import hsenidmobile.orca.core.model.Author;
import hsenidmobile.orca.core.model.Content;
import hsenidmobile.orca.core.model.Content.ContentSource;
import hsenidmobile.orca.core.model.ContentProvider;
import hsenidmobile.orca.core.model.LocalizedMessage;
import hsenidmobile.orca.core.model.Msisdn;
import hsenidmobile.orca.core.model.RoutingInfo;
import hsenidmobile.orca.core.model.RoutingInfoStatus;
import hsenidmobile.orca.core.model.RoutingKey;
import hsenidmobile.orca.core.model.Status;
import hsenidmobile.orca.core.model.SubCategory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.joda.time.DateTime;

/**
 * $LastChangedDate$
 * $LastChangedBy$ $LastChangedRevision$
 *
 */
public class ApplicationUtil {

	public static ApplicationImpl createApplication(String appId, String appName) {
		final ApplicationImpl application = new ApplicationImpl();
		application.setAppId(appId);
		application.setAppName(appName);
		application.setSubscriptionResponse(createLocalizedMsg("Subscription success"));
		application.setUnsubscribeResponse(createLocalizedMsg("Unsubscription success"));
		application.setInvalidRequestErrorMessage(createLocalizedMsg("Invalid request"));
		application.setStartDate(new DateTime());
		application.setEndDate(new DateTime().plusDays(30));
		application.setApplicationRoutingInfo(getApplicationRoutingInfo(application.getAppId(), new Msisdn(
				"0777123456", "DIALOG"), "mysubs"));
		ArrayList<Author> authors = new ArrayList<Author>();
		Author author1 = new Author();
		author1.setMsisdn(new Msisdn("771123456", "DIALOG"));
		authors.add(author1);
		application.setAuthors(authors);
		application.setRegistrationPolicy(RegistrationPolicy.OPEN);
		application.setStatus(Status.ACTIVE);

		return application;
	}

	public static SubCategory createSubCategory(String categoryCode) {
		SubCategory cat1 = new SubCategory();
		cat1.setKeyword(categoryCode);
		cat1.setDescription(Arrays.asList(new LocalizedMessage(categoryCode + "-decription", Locale.ENGLISH)));
		return cat1;
	}

	public static Content createContent(String content, SubCategory subCategory) {
		Content con1 = new Content();
		con1.setContent(new LocalizedMessage(content, Locale.ENGLISH));
		con1.setSource(ContentSource.WEB);
		con1.setSubCategory(subCategory);

		return con1;
	}

	public static List<LocalizedMessage> createLocalizedMsg(String msg) {
		return Arrays.asList(new LocalizedMessage(msg, Locale.ENGLISH));
	}

	public static ApplicationRoutingInfo getApplicationRoutingInfo(String appId, Msisdn shortCodeNumber, String keyword) {

		ApplicationRoutingInfo routing1 = new ApplicationRoutingInfo();
		final RoutingInfo routingInfo = new RoutingInfo();
		routingInfo.setStartDate(System.currentTimeMillis());
		routingInfo.setEndDate(System.currentTimeMillis() + 500000);
		routingInfo.setLastChargedDate(System.currentTimeMillis());
		routingInfo.setOwnerCp(getContentProvider());
		routingInfo.setRoutingKey(new RoutingKey(shortCodeNumber, keyword));
		routingInfo.setOwningAppId(appId);
		routingInfo.setStatus(RoutingInfoStatus.ASSIGNED);
		routing1.addRoutingInfo(routingInfo);
		return routing1;
	}

	public static ContentProvider getContentProvider() {
		final ContentProvider provider = new ContentProvider();
		return provider;
	}
}
