/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hsenidmobile.orca.orm.test.util;

import hsenidmobile.orca.core.applications.PeriodUnit;
import hsenidmobile.orca.core.applications.alert.AlertService;
import hsenidmobile.orca.core.applications.request.RequestService;
import hsenidmobile.orca.core.applications.subscription.Periodicity;
import hsenidmobile.orca.core.applications.subscription.Subscription;
import hsenidmobile.orca.core.applications.voting.VotingService;
import hsenidmobile.orca.core.model.LocalizedMessage;
import hsenidmobile.orca.core.model.SubCategory;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.joda.time.DateTime;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
public class ServiceUtil {

	public static RequestService createRequestShowService(List<SubCategory> subCategories) {
		RequestService requestService = new RequestService();
		requestService.setSupportedSubCategories(subCategories);
		requestService.setResponseMessages(Arrays.asList(new LocalizedMessage("Thank you for your request",
				Locale.ENGLISH)));

		return requestService;
	}

	public static VotingService createVotingService(List<SubCategory> subCategories) {
		VotingService voting = new VotingService();
		voting.setSupportedSubCategories(subCategories);
		return voting;
	}

	public static Subscription createSubsctiptionService(List<SubCategory> subCategories) {
		Subscription subscription = new Subscription();
		Periodicity periodicity = new Periodicity(PeriodUnit.DAY, 1, 60 * 6);
		periodicity.setFirstScheduledDispatchTime(new DateTime(2010, 8, 22, 8, 30, 0, 0));
		subscription.setDefaultPeriodicity(periodicity);
		subscription.setSupportedPeriodicities(Arrays.asList(periodicity));
		subscription.setSupportedSubCategories(subCategories);

		return subscription;
	}

	public static AlertService createAlertService(List<SubCategory> subCategories) {
		AlertService alert = new AlertService();
		alert.setSupportedSubCategories(subCategories);

		return alert;
	}
}
