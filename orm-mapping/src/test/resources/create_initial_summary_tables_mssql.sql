USE soltura;
exec sp_msforeachtable @command1="Alter table ? nocheck constraint all", @command2="drop table ? "
exec sp_msforeachtable @command1="Alter table ? nocheck constraint all", @command2="drop table ? "
exec sp_msforeachtable @command1="Alter table ? nocheck constraint all", @command2="drop table ? "

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[voting_result_summary]') AND type in (N'U'))
DROP TABLE [dbo].[voting_result_summary]
CREATE TABLE voting_result_summary ( votelet_id BIGINT NOT NULL, candidate_id BIGINT NOT NULL, total_vote_count BIGINT NOT NULL, last_update_time DATETIME, CONSTRAINT PK_voting_result_summary PRIMARY KEY (candidate_id) );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[invalid_request_summary]') AND type in (N'U'))
DROP TABLE [dbo].[invalid_request_summary]
CREATE TABLE invalid_request_summary ( app_id NVARCHAR(60) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL, total_requests BIGINT NOT NULL, last_update_time DATETIME, CONSTRAINT PK_invalid_request_summary PRIMARY KEY (app_id) );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[message_dispatching_summary]') AND type in (N'U'))
DROP TABLE [dbo].[message_dispatching_summary]
CREATE TABLE message_dispatching_summary ( app_id NVARCHAR(60) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL, status_code NVARCHAR(60) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL, total_count BIGINT NOT NULL, last_update_time DATETIME, CONSTRAINT PK_message_dispatching_summary PRIMARY KEY (app_id, status_code) );

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[mo_summary]') AND type in (N'U'))
DROP TABLE [dbo].[mo_summary]
CREATE TABLE mo_summary ( app_id NVARCHAR(60) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL, total_count BIGINT NOT NULL, last_update_time DATETIME, CONSTRAINT PK_mo_summary PRIMARY KEY (app_id) );
