--DROP DATABASE IF EXISTS ${test.db.name};
--CREATE DATABASE ${test.db.name};
--USE ${test.db.name};
CREATE TABLE  `voting_result_summary` (
  `votelet_id` bigint(20) NOT NULL,
  `candidate_id` bigint(20) NOT NULL,
  `total_vote_count` bigint(20) NOT NULL,
  `last_update_time` datetime NOT NULL,
  PRIMARY KEY (`candidate_id`)
) ENGINE=InnoDB;

CREATE TABLE  `invalid_request_summary` (
  `app_id` varchar(20) NOT NULL ,
  `total_requests` bigint(20) NOT NULL,
  `last_update_time` datetime NOT NULL,
    PRIMARY KEY (`app_id`)
 ) ENGINE=InnoDB;

 CREATE TABLE `message_dispatching_summary` (
  `app_id` varchar(20) NOT NULL ,
  `status_code` varchar(20) NOT NULL,
  `total_count` bigint(20) NOT NULL,
  `last_update_time` datetime NOT NULL,
    PRIMARY KEY (`app_id`, `status_code`)
 ) ENGINE=InnoDB;

  CREATE TABLE `mo_summary` (
  `app_id` varchar(20) NOT NULL ,
  `total_count` bigint(20) NOT NULL,
  `last_update_time` datetime NOT NULL,
    PRIMARY KEY (`app_id`)
 ) ENGINE=InnoDB;