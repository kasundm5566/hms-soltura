alter table alert_service_keyword_detail drop foreign key FK731584744B57F23F;
alter table alert_service_keyword_detail drop foreign key FK731584744C44CD1E;
alter table application_author drop foreign key FKCB77F5AB2869EE;
alter table application_author drop foreign key FKCB77F5A6AF5B961;
alter table candidate_vote drop foreign key FK536070262F9C8015;
alter table candidate_vote drop foreign key FK53607026E03B450D;
alter table content_group_keyword_detail drop foreign key FKB773952D165709CC;
alter table content_group_keyword_detail drop foreign key FKB773952D91100416;
alter table content_provider drop foreign key FKD162FAF787841C56;
alter table content_provider_application drop foreign key FK1AB7A408D97553B;
alter table content_provider_application drop foreign key FK1AB7A408CEC1AA09;
alter table content_provider_cp_charging_msisdn drop foreign key FKC9C335DECEC1AA09;
alter table content_provider_cp_charging_msisdn drop foreign key FKC9C335DE8D24591D;
alter table cp_user drop foreign key FK3999545D414D37C1;
alter table cp_user drop foreign key FK3999545DCEC1AA09;
alter table invalid_request_error_message drop foreign key FK6CDEDED8B2869EE;
alter table keyword_aliases drop foreign key FK935E1AA8775B0A9;
alter table keyword_detail_description drop foreign key FKD9D72F24775B0A9;
alter table question drop foreign key FKBA823BE6A31FE91F;
alter table question_keyword_detail drop foreign key FK83A658E0FD3FA447;
alter table question_keyword_detail drop foreign key FK83A658E0AB683D5;
alter table request_service_keyword_detail drop foreign key FKA19971E1F4774286;
alter table request_service_keyword_detail drop foreign key FKA19971E142305B95;
alter table request_show drop foreign key FK4DADE2AD99C2D006;
alter table request_show drop foreign key FK4DADE2ADE3F7AD22;
alter table subscription drop foreign key FK1456591DA65FC593;
alter table subscription_response drop foreign key FK922BDD23B2869EE;
alter table subscription_service_data drop foreign key FK70B052D67CE497D5;
alter table subscription_service_data_content_group drop foreign key FKB3C1059041EF07BB;
alter table subscription_service_data_content_group drop foreign key FKB3C105908FDFB041;
alter table subscription_subscription_service_data drop foreign key FK822E2A34C3C4DD52;
alter table subscription_subscription_service_data drop foreign key FK822E2A347C6BE8FF;
alter table unsubscribe_response drop foreign key FK275CB5EFB2869EE;
alter table votelet_keyword_detail drop foreign key FK13654E75711ED020;
alter table votelet_keyword_detail drop foreign key FK13654E75AB1E079F;
alter table voting_service_votelet drop foreign key FK6081DC4F99E8C450;
alter table voting_service_votelet drop foreign key FK6081DC4FAB1E079F;
drop table if exists alert_service;
drop table if exists alert_service_keyword_detail;
drop table if exists application;
drop table if exists application_author;
drop table if exists application_revenue_summary;
drop table if exists author;
drop table if exists author_validation_policy;
drop table if exists billing_info;
drop table if exists candidate_vote;
drop table if exists charge_after_deliver_policy;
drop table if exists charge_before_send_policy;
drop table if exists competition;
drop table if exists content_group;
drop table if exists content_group_keyword_detail;
drop table if exists content_provider;
drop table if exists content_provider_application;
drop table if exists content_provider_cp_charging_msisdn;
drop table if exists cp_charging_msisdn;
drop table if exists cp_user;
drop table if exists developer;
drop table if exists free_charging_policy;
drop table if exists invalid_request_error_message;
drop table if exists keyword_aliases;
drop table if exists keyword_detail;
drop table if exists keyword_detail_description;
drop table if exists message_history;
drop table if exists n_messages_n_period_policy;
drop table if exists one_time_registration_charging_policy;
drop table if exists orca_sdp_app_integration;
drop table if exists periodic_subscription_charging_policy;
drop table if exists periodicity;
drop table if exists question;
drop table if exists question_keyword_detail;
drop table if exists request_service;
drop table if exists request_service_data;
drop table if exists request_service_keyword_detail;
drop table if exists request_show;
drop table if exists subscriber_validation_policy;
drop table if exists subscription;
drop table if exists subscription_response;
drop table if exists subscription_service_data;
drop table if exists subscription_service_data_content_group;
drop table if exists subscription_subscription_service_data;
drop table if exists unsubscribe_response;
drop table if exists vote;
drop table if exists votelet;
drop table if exists votelet_active_whole_day;
drop table if exists votelet_keyword_detail;
drop table if exists votelet_response_spec_no_resp;
drop table if exists votelet_response_spec_result_summary;
drop table if exists voting_service;
drop table if exists voting_service_votelet;
create table alert_service (id bigint not null auto_increment, created_time datetime, last_modified_time datetime, version bigint, primary key (id));
create table alert_service_keyword_detail (alert_service_id bigint not null, latestContent_id bigint not null, unique (latestContent_id));
create table application (id bigint not null auto_increment, created_time datetime, last_modified_time datetime, version bigint, app_id varchar(20) not null unique, app_name varchar(50) not null unique, data_message_policies varchar(255), data_message_policies_id bigint, description varchar(255), end_date bigint not null, massage_sending_charging_policy varchar(255), massage_sending_charging_policy_id bigint not null, ncs_type varchar(10), registration_policy varchar(7) not null, service varchar(255), service_id bigint, start_date bigint not null, status varchar(10), sub_category_required bit not null, user_registration_charging_policy varchar(255), user_registration_charging_policy_id bigint not null, primary key (id));
create table application_author (application_id bigint not null, authors_id bigint not null, unique (authors_id));
create table application_revenue_summary (sp_id varchar(255) not null, app_id varchar(255) not null, party_charge_service_type varchar(255) not null, direction varchar(255) not null, date_id integer not null, app_name VARCHAR(65), total_revenue BIGINT(11), total_trans_count INTEGER, primary key (sp_id, app_id, party_charge_service_type, direction, date_id));
create table author (id bigint not null auto_increment, created_time datetime, last_modified_time datetime, version bigint, address varchar(15) not null, operator varchar(10) not null, primary key (id));
create table author_validation_policy (id bigint not null auto_increment, next_policy varchar(255), next_policy_id bigint, primary key (id));
create table billing_info (id bigint not null auto_increment, created_time datetime, last_modified_time datetime, version bigint, billing_address varchar(255), billing_name varchar(255), express_delivery bit, primary key (id));
create table candidate_vote (candidate_id bigint, vote_id bigint not null, primary key (vote_id));
create table charge_after_deliver_policy (id bigint not null auto_increment, created_time datetime, last_modified_time datetime, version bigint, amount float not null, primary key (id));
create table charge_before_send_policy (id bigint not null auto_increment, created_time datetime, last_modified_time datetime, version bigint, amount float not null, primary key (id));
create table competition (id bigint not null auto_increment, created_time datetime, last_modified_time datetime, version bigint, primary key (id));
create table content_group (id bigint not null auto_increment, created_time datetime, last_modified_time datetime, version bigint, contentGroupId bigint not null, primary key (id));
create table content_group_keyword_detail (content_group_id bigint not null, contents_id bigint not null, unique (contents_id));
create table content_provider (id bigint not null auto_increment, created_time datetime, last_modified_time datetime, version bigint, contact_person varchar(255), cp_id varchar(20) not null unique, createDate datetime, description varchar(255), email varchar(35), isPersonal bit not null, lastChargedDate datetime, name varchar(50) not null unique, nic varchar(255), registration_code varchar(255), status varchar(255), telephone varchar(255), billingInfo_id bigint, primary key (id));
create table content_provider_application (content_provider_id bigint not null, applications_id bigint not null, unique (applications_id));
create table content_provider_cp_charging_msisdn (content_provider_id bigint not null, msisdns_id bigint not null, unique (msisdns_id));
create table cp_charging_msisdn (id bigint not null auto_increment, created_time datetime, last_modified_time datetime, version bigint, cp_sent_verification_code varchar(255), address varchar(15) not null, operator varchar(10) not null, msisdn_verified bit, verification_code varchar(255), primary key (id));
create table cp_user (id bigint not null auto_increment, enabled bit not null, name varchar(35) not null, password varchar(35) not null, role varchar(35) not null, content_provider_id bigint, primary key (id));
create table developer (id bigint not null auto_increment, created_time datetime, last_modified_time datetime, version bigint, mo_url tinyblob, ws_password varchar(255), primary key (id));
create table free_charging_policy (id bigint not null auto_increment, created_time datetime, last_modified_time datetime, version bigint, primary key (id));
create table invalid_request_error_message (application_id bigint not null, element varchar(255), mapkey varchar(255), primary key (application_id, mapkey));
create table keyword_aliases (keyword_detail_id bigint not null, element varchar(255));
create table keyword_detail (DTYPE varchar(31) not null, id bigint not null auto_increment, created_time datetime, last_modified_time datetime, version bigint, keyword varchar(20) not null, isCorrect bit, primary key (id));
create table keyword_detail_description (keyword_detail_id bigint not null, description varchar(255), mapkey varchar(255), primary key (keyword_detail_id, mapkey));
create table message_history (id integer not null, message_timestamp BIGINT(14) not null, application_id VARCHAR(20) not null, application_name VARCHAR(50) not null, keyword VARCHAR(50), message VARCHAR(100), event varchar(255) not null, primary key (id));
create table n_messages_n_period_policy (id bigint not null auto_increment, next_policy varchar(255), next_policy_id bigint, number_of_allowed_response integer, restriction_valid_period integer, restriction_valid_period_unit varchar(7) not null, primary key (id));
create table one_time_registration_charging_policy (id bigint not null auto_increment, created_time datetime, last_modified_time datetime, version bigint, amount float not null, primary key (id));
create table orca_sdp_app_integration (id bigint not null auto_increment, created_time datetime, last_modified_time datetime, version bigint, orca_app_id varchar(255), sdp_app_id varchar(255), sdp_app_password varchar(255), primary key (id));
create table periodic_subscription_charging_policy (id bigint not null auto_increment, created_time datetime, last_modified_time datetime, version bigint, amount float not null, period_unit varchar(255) not null, period_value integer not null, primary key (id));
create table periodicity (id bigint not null auto_increment, created_time datetime, last_modified_time datetime, version bigint, allowed_buffer_time bigint, last_scheduled_sent_time bigint, last_sent_time bigint, period_value integer, unit varchar(255), primary key (id));
create table question (id bigint not null auto_increment, endDate bigint not null, question varchar(255), questionId varchar(255), startDate bigint not null, status varchar(255), primary key (id));
create table question_keyword_detail (question_id bigint not null, answers_id bigint not null, unique (answers_id));
create table request_service (id bigint not null auto_increment, created_time datetime, last_modified_time datetime, version bigint, primary key (id));
create table request_service_data (id bigint not null auto_increment, created_time datetime, last_modified_time datetime, version bigint, address varchar(15) not null, operator varchar(10) not null, received_message varchar(255), received_time bigint not null, appId varchar(255), appName varchar(255), isRead bit not null, messageId bigint not null, primary key (id));
create table request_service_keyword_detail (request_service_id bigint not null, requestSubKeywords_id bigint not null, unique (requestSubKeywords_id));
create table request_show (request_subcategory_id bigint, request_id bigint not null, primary key (request_id));
create table subscriber_validation_policy (id bigint not null auto_increment, next_policy varchar(255), data_message_policies_id bigint, primary key (id));
create table subscription (id bigint not null auto_increment, created_time datetime, last_modified_time datetime, version bigint, data_last_updated_on bigint, message_last_dispatched_time bigint, default_periodicity_id bigint, primary key (id));
create table subscription_response (application_id bigint not null, element varchar(255), mapkey varchar(255), primary key (application_id, mapkey));
create table subscription_service_data (id bigint not null auto_increment, created_time datetime, last_modified_time datetime, version bigint, expected_dispatch_time varchar(255), last_dispatched_content_id bigint, periodicity_id bigint, primary key (id));
create table subscription_service_data_content_group (subscription_service_data_id bigint not null, data_id bigint not null, unique (data_id));
create table subscription_subscription_service_data (subscription_id bigint not null, subscriptionDataList_id bigint not null, unique (subscriptionDataList_id));
create table unsubscribe_response (application_id bigint not null, element varchar(255), mapkey varchar(255), primary key (application_id, mapkey));
create table vote (id bigint not null auto_increment, created_time datetime, last_modified_time datetime, version bigint, address varchar(15) not null, operator varchar(10) not null, received_message varchar(255), received_time bigint not null, primary key (id));
create table votelet (id bigint not null auto_increment, created_time datetime, last_modified_time datetime, version bigint, active_period_type varchar(255), active_period_id bigint, end_date bigint not null, is_vote_result_public bit, response_spec_type varchar(255), voting_response_id bigint, start_date bigint not null, status varchar(10), primary key (id));
create table votelet_active_whole_day (id bigint not null auto_increment, created_time datetime, last_modified_time datetime, version bigint, primary key (id));
create table votelet_keyword_detail (votelet_id bigint not null, candidates_id bigint not null, unique (candidates_id));
create table votelet_response_spec_no_resp (id bigint not null auto_increment, created_time datetime, last_modified_time datetime, version bigint, primary key (id));
create table votelet_response_spec_result_summary (id bigint not null auto_increment, created_time datetime, last_modified_time datetime, version bigint, result_header_txt varchar(255), result_tailer_txt varchar(255), primary key (id));
create table voting_service (id bigint not null auto_increment, created_time datetime, last_modified_time datetime, version bigint, primary key (id));
create table voting_service_votelet (voting_service_id bigint not null, votelet_id bigint not null, unique (votelet_id));
alter table alert_service_keyword_detail add index FK731584744B57F23F (alert_service_id), add constraint FK731584744B57F23F foreign key (alert_service_id) references alert_service (id);
alter table alert_service_keyword_detail add index FK731584744C44CD1E (latestContent_id), add constraint FK731584744C44CD1E foreign key (latestContent_id) references keyword_detail (id);
alter table application_author add index FKCB77F5AB2869EE (application_id), add constraint FKCB77F5AB2869EE foreign key (application_id) references application (id);
alter table application_author add index FKCB77F5A6AF5B961 (authors_id), add constraint FKCB77F5A6AF5B961 foreign key (authors_id) references author (id);
alter table candidate_vote add index FK536070262F9C8015 (vote_id), add constraint FK536070262F9C8015 foreign key (vote_id) references vote (id);
alter table candidate_vote add index FK53607026E03B450D (candidate_id), add constraint FK53607026E03B450D foreign key (candidate_id) references keyword_detail (id);
alter table content_group_keyword_detail add index FKB773952D165709CC (content_group_id), add constraint FKB773952D165709CC foreign key (content_group_id) references content_group (id);
alter table content_group_keyword_detail add index FKB773952D91100416 (contents_id), add constraint FKB773952D91100416 foreign key (contents_id) references keyword_detail (id);
alter table content_provider add index FKD162FAF787841C56 (billingInfo_id), add constraint FKD162FAF787841C56 foreign key (billingInfo_id) references billing_info (id);
alter table content_provider_application add index FK1AB7A408D97553B (applications_id), add constraint FK1AB7A408D97553B foreign key (applications_id) references application (id);
alter table content_provider_application add index FK1AB7A408CEC1AA09 (content_provider_id), add constraint FK1AB7A408CEC1AA09 foreign key (content_provider_id) references content_provider (id);
alter table content_provider_cp_charging_msisdn add index FKC9C335DECEC1AA09 (content_provider_id), add constraint FKC9C335DECEC1AA09 foreign key (content_provider_id) references content_provider (id);
alter table content_provider_cp_charging_msisdn add index FKC9C335DE8D24591D (msisdns_id), add constraint FKC9C335DE8D24591D foreign key (msisdns_id) references cp_charging_msisdn (id);
alter table cp_user add index FK3999545D414D37C1 (id), add constraint FK3999545D414D37C1 foreign key (id) references content_provider (id);
alter table cp_user add index FK3999545DCEC1AA09 (content_provider_id), add constraint FK3999545DCEC1AA09 foreign key (content_provider_id) references content_provider (id);
alter table invalid_request_error_message add index FK6CDEDED8B2869EE (application_id), add constraint FK6CDEDED8B2869EE foreign key (application_id) references application (id);
alter table keyword_aliases add index FK935E1AA8775B0A9 (keyword_detail_id), add constraint FK935E1AA8775B0A9 foreign key (keyword_detail_id) references keyword_detail (id);
alter table keyword_detail_description add index FKD9D72F24775B0A9 (keyword_detail_id), add constraint FKD9D72F24775B0A9 foreign key (keyword_detail_id) references keyword_detail (id);
alter table question add index FKBA823BE6A31FE91F (id), add constraint FKBA823BE6A31FE91F foreign key (id) references competition (id);
alter table question_keyword_detail add index FK83A658E0FD3FA447 (answers_id), add constraint FK83A658E0FD3FA447 foreign key (answers_id) references keyword_detail (id);
alter table question_keyword_detail add index FK83A658E0AB683D5 (question_id), add constraint FK83A658E0AB683D5 foreign key (question_id) references question (id);
alter table request_service_keyword_detail add index FKA19971E1F4774286 (request_service_id), add constraint FKA19971E1F4774286 foreign key (request_service_id) references request_service (id);
alter table request_service_keyword_detail add index FKA19971E142305B95 (requestSubKeywords_id), add constraint FKA19971E142305B95 foreign key (requestSubKeywords_id) references keyword_detail (id);
alter table request_show add index FK4DADE2AD99C2D006 (request_id), add constraint FK4DADE2AD99C2D006 foreign key (request_id) references request_service_data (id);
alter table request_show add index FK4DADE2ADE3F7AD22 (request_subcategory_id), add constraint FK4DADE2ADE3F7AD22 foreign key (request_subcategory_id) references keyword_detail (id);
alter table subscription add index FK1456591DA65FC593 (default_periodicity_id), add constraint FK1456591DA65FC593 foreign key (default_periodicity_id) references periodicity (id);
alter table subscription_response add index FK922BDD23B2869EE (application_id), add constraint FK922BDD23B2869EE foreign key (application_id) references application (id);
alter table subscription_service_data add index FK70B052D67CE497D5 (periodicity_id), add constraint FK70B052D67CE497D5 foreign key (periodicity_id) references periodicity (id);
alter table subscription_service_data_content_group add index FKB3C1059041EF07BB (data_id), add constraint FKB3C1059041EF07BB foreign key (data_id) references content_group (id);
alter table subscription_service_data_content_group add index FKB3C105908FDFB041 (subscription_service_data_id), add constraint FKB3C105908FDFB041 foreign key (subscription_service_data_id) references subscription_service_data (id);
alter table subscription_subscription_service_data add index FK822E2A34C3C4DD52 (subscriptionDataList_id), add constraint FK822E2A34C3C4DD52 foreign key (subscriptionDataList_id) references subscription_service_data (id);
alter table subscription_subscription_service_data add index FK822E2A347C6BE8FF (subscription_id), add constraint FK822E2A347C6BE8FF foreign key (subscription_id) references subscription (id);
alter table unsubscribe_response add index FK275CB5EFB2869EE (application_id), add constraint FK275CB5EFB2869EE foreign key (application_id) references application (id);
alter table votelet_keyword_detail add index FK13654E75711ED020 (candidates_id), add constraint FK13654E75711ED020 foreign key (candidates_id) references keyword_detail (id);
alter table votelet_keyword_detail add index FK13654E75AB1E079F (votelet_id), add constraint FK13654E75AB1E079F foreign key (votelet_id) references votelet (id);
alter table voting_service_votelet add index FK6081DC4F99E8C450 (voting_service_id), add constraint FK6081DC4F99E8C450 foreign key (voting_service_id) references voting_service (id);
alter table voting_service_votelet add index FK6081DC4FAB1E079F (votelet_id), add constraint FK6081DC4FAB1E079F foreign key (votelet_id) references votelet (id);


CREATE TABLE  `orca_db`.`voting_result_summary` (
  `votelet_id` bigint(20) NOT NULL,
  `candidate_id` bigint(20) NOT NULL,
  `total_vote_count` bigint(20) NOT NULL,
  `last_update_time` datetime NOT NULL,
  PRIMARY KEY (`candidate_id`)
) ENGINE=InnoDB;

CREATE TABLE  `orca_db`.`invalid_request_summary` (
  `app_id` varchar(20) NOT NULL ,
  `total_requests` bigint(20) NOT NULL,
  `last_update_time` datetime NOT NULL,
    PRIMARY KEY (`app_id`)
 ) ENGINE=InnoDB;

 CREATE TABLE `orca_db`.`message_dispatching_summary` (
  `app_id` varchar(20) NOT NULL ,
  `status_code` varchar(20) NOT NULL,
  `total_count` bigint(20) NOT NULL,
  `last_update_time` datetime NOT NULL,
    PRIMARY KEY (`app_id`, `status_code`)
 ) ENGINE=InnoDB;

  CREATE TABLE `orca_db`.`mo_summary` (
  `app_id` varchar(20) NOT NULL ,
  `total_count` bigint(20) NOT NULL,
  `last_update_time` datetime NOT NULL,
    PRIMARY KEY (`app_id`)
 ) ENGINE=InnoDB;
