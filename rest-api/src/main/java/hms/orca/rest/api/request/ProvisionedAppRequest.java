/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.orca.rest.api.request;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */

@XmlRootElement(name = "provisionedapp")
@XmlAccessorType(XmlAccessType.NONE)
public class ProvisionedAppRequest{

    public ProvisionedAppRequest() {
    }

    @XmlAttribute(name = "appname")
    private String appName;

    @XmlAttribute(name = "appexpiarable")
    private boolean appExpirable;

    @XmlAttribute(name = "startdate")
    private String startDate;

    @XmlAttribute(name = "enddate")
    private String endDate;

    @XmlAttribute(name = "description")
    private String description;

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public boolean isAppExpirable() {
        return appExpirable;
    }

    public void setAppExpirable(boolean appExpirable) {
        this.appExpirable = appExpirable;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ProvisionedAppRequest");
        sb.append("{appName='").append(appName).append('\'');
        sb.append('}');
        sb.append("{description='").append(description).append('\'');
        sb.append('}');
        sb.append("{startDate='").append(startDate).append('\'');
        sb.append('}');
        sb.append("{endDate='").append(appName).append('\'');
        sb.append('}');
        sb.append("{appExpirable='").append(appExpirable).append('\'');
        sb.append('}');

        return sb.toString();

    }
}