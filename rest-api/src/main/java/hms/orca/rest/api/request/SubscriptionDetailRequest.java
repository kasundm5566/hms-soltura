/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.orca.rest.api.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */

@XmlRootElement(name = "subscriber")
@XmlAccessorType(XmlAccessType.NONE)
public class SubscriptionDetailRequest {

    @XmlAttribute(name = "appId")
    private String appId;

    public SubscriptionDetailRequest(String appId) {
        this.appId = appId;
    }

    public SubscriptionDetailRequest() {
    }

    public String getAppId() {
        return appId;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("SubscriptionDetailRequest");
        sb.append("{appId='").append(appId).append('\'');
        sb.append('}');
        return sb.toString();
    }
}