/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.orca.rest.api.response;

import hms.orca.rest.api.util.common.MapStringStringAdapter;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.Map;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
@XmlRootElement( name = "response")
@XmlAccessorType(XmlAccessType.NONE)
public class BasicResponse {

    @XmlAttribute(name = "statusMessage-code")
    private String statusCode;

    @XmlAttribute(name = "statusMessage-message")
    private String statusMessage;

    @XmlElement(name = "response-parameters")
    @XmlJavaTypeAdapter(MapStringStringAdapter.class)
    private Map<String, String> responseParams;

    public BasicResponse() {
    }

    public BasicResponse(String statusCode, String statusMessage) {
        this.statusCode = statusCode;
        this.statusMessage = statusMessage;
    }

    public BasicResponse(String statusCode, String statusMessage, Map<String, String> responseParams) {
        this.statusCode = statusCode;
        this.statusMessage = statusMessage;
        this.responseParams = responseParams;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public Map<String, String> getResponseParams() {
        return responseParams;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("BasicResponse");
        sb.append("{statusCode='").append(statusCode).append('\'');
        sb.append(", statusMessage='").append(statusMessage).append('\'');
        sb.append(", responseParams=").append(responseParams);
        sb.append('}');
        return sb.toString();
    }
}
