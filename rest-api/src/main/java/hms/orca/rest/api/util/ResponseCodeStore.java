/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.orca.rest.api.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * $LastChangedDate:$
 * $LastChangedBy:$
 * $LastChangedRevision:$
 */
public class ResponseCodeStore {

    private static final Logger logger = LoggerFactory.getLogger(ResponseCodeStore.class);

    public static final Map<String, String> commonResponseCodeMap = new
            ConcurrentHashMap<String, String>();

    public static void insertResponseCode(String msisdn, String code) {
        commonResponseCodeMap.put(msisdn, code);
    }

    public static String getResponseCode(String msisdn) {
        return commonResponseCodeMap.get(msisdn);
    }
}
