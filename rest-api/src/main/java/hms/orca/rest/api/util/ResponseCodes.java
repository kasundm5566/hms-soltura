package hms.orca.rest.api.util;

/**
 * $LastChangedDate:$
 * $LastChangedBy:$
 * $LastChangedRevision:$
 */
public interface ResponseCodes {

    //Response code for Common Registration Login
    public static final String CR_SUCCESS_CODE = "CR_SUCC_1000";
    public static final String CR_SUCCESS_MESSAGE = "SUCCESS";
    public static final String CR_ERROR_CODE = "CR_ERR_1000";
    public static final String CR_ERROR_MESSAGE = "ERROR";

    //Response code for Provisioning Login
    public static final String PROV_SP_SUCCESS_CODE = "PROV_SUCC_1000";
    public static final String PROV_SP_SUCCESS_MESSAGE = "SUCCESS";
    public static final String PROV_SP_ERROR_CODE = "PROV_ERR_1000";
    public static final String PROV_SP_ERROR_MESSAGE = "ERROR";

    //Response code for Payment Gateway
    public static final String PG_SUCCESS_CODE = "PG_SUCC_1000";
    public static final String PG_SUCCESS_MESSAGE = "SUCCESS";
    public static final String PG_ERROR_CODE = "PG_ERR_1000";
    public static final String PG_ERROR_MESSAGE = "ERROR";

    //Response code for Provisioning App Creation
    public static final String SDP_SUCCESS_CODE = "S1000";
    public static final String SDP_SUCCESS_MESSAGE = "SUCCESS";
    public static final String PROV_APP_ERROR_CODE = "PROV_APP_ERR_1000";
    public static final String PROV_APP_ERROR_MESSAGE = "ERROR";

}
