/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.orca.rest.api.util.common;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * $LastChangedDate:$
 * $LastChangedBy:$
 * $LastChangedRevision:$
 */
public class MapStringStringAdapter extends MapAdapter<MapStringStringElement, String, String> {

    @Override
    public MapStringStringElement newMapElement(final String key, final String value) {
        final MapStringStringElement result = new MapStringStringElement();
        result.setKey(key);
        result.setValue(value);
        return result;
    }

    @Override
    public MapStringStringElement[] newMapElement(int size) {
        return new MapStringStringElement[size];
    }
}

class MapStringStringElement extends MapElement<String, String> {
    @XmlAttribute
    @Override
    public String getKey() {
        return key;
    }

    @XmlElement
    @Override
    public String getValue() {
        return value;
    }

    @Override
    public void setKey(final String key) {
        this.key = key;
    }

    @Override
    public void setValue(final String value) {
        this.value = value;
    }
}
