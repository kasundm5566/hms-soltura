/**
 *
 */
package hsenidmobile.orca.rest;

/**
 * @author sandarenu
 *
 */
public class RestRequestException extends Exception {

	public RestRequestException(String message, Throwable cause) {
		super(message, cause);
	}

	public RestRequestException(String message) {
		super(message);
	}

}
