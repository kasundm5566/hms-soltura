/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hsenidmobile.orca.rest.common;

import hms.orca.rest.api.response.BasicResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

import static hms.orca.rest.api.util.ResponseCodes.*;
import static hsenidmobile.orca.rest.common.ResponseStatus.*;
import static hsenidmobile.orca.rest.common.util.RestApiKeys.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class ResponseCodeMapper {

    private static final Logger logger = LoggerFactory.getLogger(ResponseCodeMapper.class);

    public Response getCommonUserRegistrationLoginResponse(String responseCode) {
        Response response = new Response();
        if (responseCode.trim().equals(CR_SUCCESS_CODE)) {
            response.setResponseStatus(SUCCESS);
        } else {
            response.setResponseStatus(INVALID_CREDINTIALS);
        }
        return response;
    }

    public Response getProvSpLoginResponse(String responseCode) {
        Response response = new Response();
        if (responseCode.trim().equals(PROV_SP_SUCCESS_CODE)) {
            response.setResponseStatus(SUCCESS);
        } else {
            response.setResponseStatus(SP_NOT_FOUND);
        }
        return response;
    }

    public Response getProvAppCreateResponse(Map<String, Object> receivedResponse) {
        String statusCode = (String) receivedResponse.get(STATUS_CODE);
        Response response = new Response();
        if (statusCode.trim().equals(SDP_SUCCESS_CODE)) {
            response.setResponseStatus(SUCCESS);
            response.getResponseParameters().put(APP_ID, (String)receivedResponse.get(APP_ID));
            response.getResponseParameters().put(PASSWORD, (String)receivedResponse.get(PASSWORD));
        } else {
            String statusDescription = (String) receivedResponse.get(ERROR_DESCRIPTION);
            response.setResponseStatus(PROV_APP_CREATION_FAILED);
        }
        return response;
    }

    public Response getPgResponse(BasicResponse basicResponse) {
        Response response = new Response();
        String responseCode = basicResponse.getStatusCode();
        if(responseCode.trim().equals(PG_SUCCESS_CODE)) {
            response.setResponseStatus(SUCCESS);
            response.setResponseParameters(basicResponse.getResponseParams());
        } else {
            response.setResponseStatus(PG_FINANCIAL_INSTRUMENTS_NOT_FOUND);
        }
        return response;
    }

    public Response getRkAvailabilityResponse(Boolean basicResponse) {
        Response response = new Response();
        Boolean flag = basicResponse.booleanValue();
        if (flag == Boolean.TRUE) {
            response.setResponseStatus(RK_AVAILABLE);
        } else {
            response.setResponseStatus(RK_NOT_AVAILABLE);
        }
        return response;
    }

    public Response getSubscriberResponse(Map<String, Object> basicResponse) {
        String responseCode = (String) basicResponse.get("statusCode");
        logger.debug("Received code [{}] ", responseCode);
        Response response = new Response();
        if (responseCode.trim().equals(SDP_SUCCESS_CODE)) {
            response.setResponseStatus(SUCCESS);
            HashMap resultMap = new HashMap();
            resultMap.put(BASE_SIZE, basicResponse.get(BASE_SIZE));
            response.setResponseParameters(resultMap);
        }
        return response;
    }

    public Response getsdpRoutingkeyResponse(BasicResponse basicResponse) {
        Response response = new Response();
        String responseCode = basicResponse.getStatusCode();

        response.setResponseStatus(SUCCESS);
        response.setResponseParameters(basicResponse.getResponseParams());

        return response;
    }

}



