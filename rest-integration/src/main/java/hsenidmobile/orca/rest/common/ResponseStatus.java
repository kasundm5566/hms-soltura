package hsenidmobile.orca.rest.common;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public enum ResponseStatus {

    SUCCESS {
        @Override
        public boolean isSuccess() {
            return true;
        }},

    INVALID_CREDINTIALS {
        @Override
        public boolean isSuccess() {
            return false;
        }},

    USER_NOT_ACTIVE {
        @Override
        public boolean isSuccess() {
            return false;
        }
    },

    USER_NOT_ALLOWED {
        @Override
        public boolean isSuccess() {
            return false;
        }
    },

    SP_NOT_FOUND {
        @Override
        public boolean isSuccess() {
            return false;
        }
    },

    PG_FINANCIAL_INSTRUMENTS_NOT_FOUND {
        @Override
        public boolean isSuccess() {
            return false;
        }
    },

    RK_AVAILABLE {
        @Override
        public boolean isSuccess() {
            return true;
        }
    },

    RK_NOT_AVAILABLE {
        @Override
        public boolean isSuccess() {
            return false;
        }
    },

    PROV_APP_CREATION_FAILED {
        @Override
        public boolean isSuccess() {
            return false;
        }
    };

    public abstract boolean isSuccess();
}
