/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hsenidmobile.orca.rest.common.transformer;

import hms.common.rest.util.JsonBodyProvider;
import hms.orca.rest.api.response.BasicResponse;
import hms.pgw.api.response.PayInstrumentResponse;
import hsenidmobile.orca.rest.RestRequestException;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.apache.cxf.jaxrs.provider.json.JSONProvider;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;

/**
 * This is used to transform the response input stream to a Json response or XML
 * response $LastChangedDate$ $LastChangedBy$ $LastChangedRevision$
 */
public class Transformer {

	public static Map<String, Object> readJsonResponse(javax.ws.rs.core.Response response) throws RestRequestException {
		InputStream is = (InputStream) response.getEntity();
		Class aClass = Map.class;
		JsonBodyProvider jsonProvider = new JsonBodyProvider();
		try {
			return (Map<String, Object>) jsonProvider.readFrom(aClass, null, null, MediaType.APPLICATION_JSON_TYPE,
					null, is);

		} catch (Exception e) {
			throw new RestRequestException("Error while decoding JSON response message.", e);
		}
	}

	/**
	 * Creates Response from a XML annotated object
	 *
	 * @param is
	 * @return
	 */
	public static PayInstrumentResponse decodePayInsQueryResp(Response response) {
		InputStream resStream = (InputStream) response.getEntity();
		JAXBContext ctx;
		PayInstrumentResponse res = null;
		try {
			ctx = JAXBContext.newInstance(PayInstrumentResponse.class);
			res = (PayInstrumentResponse) ctx.createUnmarshaller().unmarshal(
					new InputStreamReader(resStream));
			return res;
		} catch (JAXBException e) {
			e.printStackTrace();
			return null;
		}
	}
}
