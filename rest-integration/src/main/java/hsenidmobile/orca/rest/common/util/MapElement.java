/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hsenidmobile.orca.rest.common.util;

import javax.xml.bind.annotation.XmlTransient;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
@XmlTransient
abstract class MapElement<K, V> {

    protected K key;
    protected V value;

    public abstract K getKey();

    public abstract V getValue();

    public abstract void setKey(K key);

    public abstract void setValue(V value);

}
