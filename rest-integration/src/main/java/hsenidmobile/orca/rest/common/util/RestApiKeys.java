/**
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hsenidmobile.orca.rest.common.util;

/*
* $LastChangedDate$
* $LastChangedBy$
* $LastChangedRevision$
*/
public interface RestApiKeys {

    public static final String EXPIRE = "expire";
    public static final String ALLOWED_PI = "allowed-payment-instruments";
    public static final String APP_ID = "app-id";
    public static final String APPLICATION_ID = "applicationId";
    public static final String PASSWORD = "password";
    public static final String ALLOWED_HOST_ADDRESS = "allowed-hosts";
    public static final String AVAILABLE = "available";
    public static final String BROADCAST_ADDRESS = "broadcast-address";
    public static final String CORRALATION_ID = "correlation-id";
    public static final String MESSAGE_ID = "requestId";
    public static final String NAME = "name";
    public static final String OPERATOR = "operator";
    public static final String KEYWORD = "keyword";
    public static final String DESCRIPTION = "description";
    public static final String SHORTCODE = "shortcode";
    public static final String NUMBER_MASKING = "mask-number";
    public static final String GOVERN_ENABLED = "govern";
    public static final String TAX_ENABLED = "apply-tax";
    public static final String ADVERTISE_ENABLED = "advertise";
    public static final String REVENUE_SHARE = "revenue-share";
    public static final String CHARGING_TYPE = "type";
    public static final String CHARGING_METHOD = "method";
    public static final String CHARGING_SLA = "charging";
    public static final String CHARGING_AMOUNT = "amount";
    public static final String CHARGE_PARTY = "party";
    public static final String SUBSCRIPTION_CHARGING_TYPE = "charging-type";
    public static final String SUBSCRIPTION_SUCCESS_REG_RESPONSE = "subscription-response-message";
    public static final String SUBSCRIPTION_SUCCESS_UNREG_RESPONSE = "unsubscription-response-message";
    public static final String SUBSCRIPTION_CONFIRMATION_REQUIRED = "confirmation-required";
    public static final String SUBSCRIPTION_FREQUENCY = "frequency";
    public static final String ALLOW_HTTP_REQUESTS = "allow-http-subscription-requests";
    public static final String SP_ID = "sp-id";
    public static final String CP_ID = "coop-user-id";
    public static final String NCS = "ncs";
    public static final String NCS_TYPE = "ncs-type";
    public static final String NCS_SLAS = "ncs-slas";
    public static final String START_DATE = "app-request-date";
    public static final String END_DATE = "end-date";
    public static final String MO_CONNECTION_URL = "connection-url";
    public static final String ROUTING_KEYS = "routing-keys";
    public static final String TPS = "tps";
    public static final String TPD = "tpd";
    public static final String STATUS = "status";
    public static final String SUCCESS_CODE = "success";
    public static final String ALLOWED = "allowed";
    public static final String STATUS_CODE = "status-code";
    public static final String ERROR_DESCRIPTION = "status-description";
    public static final String KEYWORD_NOT_AVAILABLE_CODE = "not-available";
    public static final String MAX_NO_OF_BC_MSG_PER_DAY = "max-no-of-bc-msgs-per-day";
    public static final String MASSAGE = "message";
    public static final String DEFAULT_SENDER_ADDRESS = "default-sender-address";
    public static final String ALISING = "aliasing";
    public static final String ROUTING_KEYS_CATEGORIES = "category";
    public static final String SUBSCRIPTION_REQUIRED = "subscription-required";

    //sdp error codes
    public static final String SP_LOGIN_ERROR_CODE_E1812 = "E1812";
    public static final String SP_LOGIN_ERROR_CODE_E1811 = "E1811";
    public static final String SP_LOGIN_ERROR_CODE_E1302 = "E1302";
    public static final String SP_LOGIN_ERROR_CODE_E1601 = "E1601";
    public static final String SP_LOGIN_ERROR_CODE_E1819 = "E1819";
    public static final String SP_LOGIN_ERROR_CODE_PREFIX = "sp.login.error.code_";
    public static final String BASE_SIZE = "baseSize";
    public static final String BLACK_LIST = "black-list";
    public static final String WHITE_LIST = "white-list";
    public String SUBSCRIPTION_NCS_TYPE = "subscription";
    public String SP_CHARGE_PARTY = "sp";
    public String SUBSCRIBER_CHARGE_PARTY = "subscriber";
    public String MO = "mo";
    public String MT = "mt";
    public String FLAT_CHARGING_TYPE = "flat";
    public String FREE_CHARGING_TYPE = "free";
    public String DEFAULT_PAYMENT_INSTRUMENT = "default-payment-instrument";
    public String OPERATOR_CHARGING = "operator-charging";
    public String PAYMENT_INSTRUMENT = "payment-instrument";
    public String PAYMENT_INSTRUMENT_NAME = "payment-instrument-name";
    public String PAYMENT_INSTRUMENT_ID = "payment-instrument-id";
    public String PAYMENT_ACCOUNT = "payment-account";
    public String CREATED_BY = "created-by";
    public String CREATED_USER_TYPE = "user-type";
    public static final String TOTAL_ERRORS = "total-errors";
    public static final String TOTAL_SENT = "total-sent";
    public static final String BROADCAST_STATUS = "broadcast-status";

    public static final String LAST_LOGIN_TIME = "last-login-time";
    public static final String MSISDN_VERIFIED = "msisdn-verified";
    public static final String FIRST_NAME = "first-name";
    public static final String LAST_NAME = "last-name";
    public static final String DISPLAY_NAME = "display-name";
    public static final String CONTACT_PERSON_NAME = "contact-person-name";
}