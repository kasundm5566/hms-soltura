package hsenidmobile.orca.rest.pg.transport;


import hms.pgw.api.response.PayInstrumentResponse;

/**
 * REST - API integration interface for Payment Gateway module
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public interface PaymentGatewayRequestSender {

    /**
     * Returns configured financial instrument
     * @param userId
     * @return
     */
    PayInstrumentResponse getAvailableFinancialInstruments(String userId);
}
