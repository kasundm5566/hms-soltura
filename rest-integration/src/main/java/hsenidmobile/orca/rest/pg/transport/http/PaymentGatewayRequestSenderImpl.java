/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hsenidmobile.orca.rest.pg.transport.http;

import hms.commons.SnmpLogUtil;
import hms.pgw.api.request.PayInstrumentQueryMessage;
import hms.pgw.api.response.PayInstrumentResponse;
import hsenidmobile.orca.rest.common.ResponseCodeMapper;
import hsenidmobile.orca.rest.common.transformer.Transformer;
import hsenidmobile.orca.rest.pg.transport.PaymentGatewayRequestSender;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.jaxrs.client.ClientWebApplicationException;
import org.apache.cxf.jaxrs.client.WebClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.core.MediaType;

/**
 * REST - API integration interface implementation for Payment Gateway module
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class PaymentGatewayRequestSenderImpl implements PaymentGatewayRequestSender {

    private String url;
    @Autowired
    private ResponseCodeMapper responseCodeMapper;
    private static final Logger logger  = LoggerFactory.getLogger(PaymentGatewayRequestSenderImpl.class);
    private String pgwConnectionDownTrap;
    private String pgwConnectionUpTrap;
    private static final String PGW_CONNECTION_ID = "soltura-pgw-connection";

    @Override
    public PayInstrumentResponse getAvailableFinancialInstruments(String userId) {
        try {
            WebClient webClient = WebClient.create(url);
            webClient.header("Content-Type", MediaType.APPLICATION_XML);
            webClient.accept(MediaType.APPLICATION_XML);

            logger.debug("Sending Get Available Financial Instruments Request for the User [{}] ",userId);
            PayInstrumentQueryMessage request = new PayInstrumentQueryMessage();
            request.setUserId(userId);
            logger.debug("Sending Get Available Financial Instruments Request To Payment Gateway [ {} ] for the" +
                    " User [{}] ", request, userId);
            javax.ws.rs.core.Response response = webClient.post(request);
            PayInstrumentResponse receivedResponse = Transformer.decodePayInsQueryResp(response);

            if (receivedResponse != null) {
                SnmpLogUtil.clearTrap(PGW_CONNECTION_ID, pgwConnectionUpTrap);
                return receivedResponse;
            } else {
                SnmpLogUtil.trap(PGW_CONNECTION_ID, pgwConnectionDownTrap);
                return null;
            }
        } catch (ClientWebApplicationException e) {
            SnmpLogUtil.trap(PGW_CONNECTION_ID, pgwConnectionDownTrap);
            throw e;
        }
    }

    public void setUrl(String url) {
        this.url = url;
    }


    public void setPgwConnectionDownTrap(String pgwConnectionDownTrap) {
        this.pgwConnectionDownTrap = pgwConnectionDownTrap;
    }

    public void setPgwConnectionUpTrap(String pgwConnectionUpTrap) {
        this.pgwConnectionUpTrap = pgwConnectionUpTrap;
    }
}
