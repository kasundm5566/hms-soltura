/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hsenidmobile.orca.rest.registration.transport;

import hms.common.registration.api.response.BasicUserResponseMessage;

/**
 * REST - API integration interface for Common Registration module 
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public interface CommonRegistrationRequestSender {

    public BasicUserResponseMessage getBasicUserResponseMessage(String userName);

    public String getMsisdnByUserId(String userId);

}
