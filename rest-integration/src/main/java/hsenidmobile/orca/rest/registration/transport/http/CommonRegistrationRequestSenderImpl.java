/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hsenidmobile.orca.rest.registration.transport.http;

import static hms.common.registration.api.common.RequestType.USER_BASIC_DETAILS;
import static hsenidmobile.orca.rest.util.RestUtils.makeRestCall;
import hms.common.registration.api.request.UserDetailByUsernameRequestMessage;
import hms.common.registration.api.response.BasicUserResponseMessage;
import hms.common.rest.util.JsonBodyProvider;
import hms.commons.SnmpLogUtil;
import hsenidmobile.orca.rest.common.ResponseCodeMapper;
import hsenidmobile.orca.rest.common.transformer.Transformer;
import hsenidmobile.orca.rest.registration.transport.CommonRegistrationRequestSender;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.core.MediaType;

import org.apache.cxf.jaxrs.client.ClientWebApplicationException;
import org.apache.cxf.jaxrs.client.WebClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import scala.actors.threadpool.Arrays;

/**
 * REST - API integration interface implementation for Common Registration
 * module $LastChangedDate$ $LastChangedBy$ $LastChangedRevision$
 */
public class CommonRegistrationRequestSenderImpl implements CommonRegistrationRequestSender {

	private static final Logger logger = LoggerFactory.getLogger(CommonRegistrationRequestSenderImpl.class);

	private String userByUserNameUrl;
	private String userByUserIdUrl;

	@Autowired
	private ResponseCodeMapper responseCodeMapper;
	private String curConnectionDownTrap;
	private String curConnectionUpTrap;
	private static final String CUR_CONNECTION_ID = "soltura-cur-connection";

	public BasicUserResponseMessage getBasicUserResponseMessage(String userName) {
		try {
			WebClient webClient = WebClient.create(userByUserNameUrl,
					Arrays.asList(new Object[] { new JsonBodyProvider() }));
			UserDetailByUsernameRequestMessage request = new UserDetailByUsernameRequestMessage();
			request.setRequestedTimeStamp(new Date());
			request.setUserName(userName);
			request.setRequestType(USER_BASIC_DETAILS);
			webClient.header("Content-Type", MediaType.APPLICATION_JSON);
			webClient.accept(MediaType.APPLICATION_JSON);
			logger.debug("Sending request to get user-basic-details user[{}], url[{}] ", userName,
					webClient.getCurrentURI());
			javax.ws.rs.core.Response response = webClient.post(request.convertToMap());
			Map<String, Object> respMap;
			try {
				respMap = Transformer.readJsonResponse(response);
				BasicUserResponseMessage receivedResponse = BasicUserResponseMessage.convertFromMap(respMap);
				SnmpLogUtil.clearTrap(CUR_CONNECTION_ID, curConnectionUpTrap);
				return receivedResponse;
			} catch (Exception e) {
				logger.error("Error sending request to get user-basic-details for user[{}]", userName, e);
				SnmpLogUtil.trap(CUR_CONNECTION_ID, curConnectionDownTrap);
				return null;
			}

		} catch (ClientWebApplicationException e) {
			SnmpLogUtil.trap(CUR_CONNECTION_ID, curConnectionDownTrap);
			throw e;
		}
	}

	public String getMsisdnByUserId(String userId) {

		try {
			Map<String, Object> request = new HashMap<String, Object>();
			request.put("user-id", userId);
			logger.debug("Sending find user by user id request userId[{}], url[{}] ", userId, userByUserIdUrl);
			Map<String, String> response = (Map<String, String>) makeRestCall(userByUserIdUrl, request, Map.class);
			if (response != null) {
				SnmpLogUtil.clearTrap(CUR_CONNECTION_ID, curConnectionUpTrap);
				return response.get("msisdn");
			} else {
				SnmpLogUtil.trap(CUR_CONNECTION_ID, curConnectionDownTrap);
				return null;
			}
		} catch (ClientWebApplicationException e) {
			SnmpLogUtil.trap(CUR_CONNECTION_ID, curConnectionDownTrap);
			throw e;
		}
	}

	public void setUserByUserNameUrl(String userByUserNameUrl) {
		this.userByUserNameUrl = userByUserNameUrl;
	}

	public void setUserByUserIdUrl(String userByUserIdUrl) {
		this.userByUserIdUrl = userByUserIdUrl;
	}

	public void setCurConnectionDownTrap(String curConnectionDownTrap) {
		this.curConnectionDownTrap = curConnectionDownTrap;
	}

	public void setCurConnectionUpTrap(String curConnectionUpTrap) {
		this.curConnectionUpTrap = curConnectionUpTrap;
	}
}
