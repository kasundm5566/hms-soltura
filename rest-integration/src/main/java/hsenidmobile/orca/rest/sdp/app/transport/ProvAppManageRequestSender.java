package hsenidmobile.orca.rest.sdp.app.transport;

import hsenidmobile.orca.rest.common.Response;

import java.util.Map;

/**
 * REST - API integration interface for Provisioning module
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public interface ProvAppManageRequestSender {

    /**
     * Sends request to create provisioned application
     *
     * @param provisionedAppRequest
     * @param appId
     * @return
     */
    Response createSdpProvisionedApp(Map<String, Object> provisionedAppRequest, String appId);

    /**
     * Sending request to check the appName available in sdp
     * @param requestMessage
     * @return
     */
    Map<String, Object> validateAppName(Map<String, Object> requestMessage);

    /**
     * Returns status of the peovisioning application
     * @param appStateRequest
     * @return
     */
    Map<String, Object> getSdpAppStatus(Map<String, Object> appStateRequest);

    /**
     * Sending request to retrieve all available applications
     * @param requestMessage
     * @return
     */
    Map<String, Object> getApp(Map requestMessage);

}
