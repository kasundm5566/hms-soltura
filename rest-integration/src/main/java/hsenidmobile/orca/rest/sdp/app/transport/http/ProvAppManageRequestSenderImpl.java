/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hsenidmobile.orca.rest.sdp.app.transport.http;

import hms.commons.SnmpLogUtil;
import hsenidmobile.orca.rest.common.Response;
import hsenidmobile.orca.rest.common.ResponseCodeMapper;
import hsenidmobile.orca.rest.sdp.app.transport.ProvAppManageRequestSender;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.jaxrs.client.ClientWebApplicationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.ServletConfig;
import java.util.Map;

import static hsenidmobile.orca.rest.common.util.RestApiKeys.SP_ID;
import static hsenidmobile.orca.rest.util.RestUtils.makeRestCall;

/**
 * REST - API integration interface implementation for Provisioning
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class ProvAppManageRequestSenderImpl implements ProvAppManageRequestSender {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProvAppManageRequestSenderImpl.class);
    private String createProAppUrl;
    private String validateAppnameUrl;
    private String sdpAppStatusUrl;
    private String getApplicationsUrl;
    private WebApplicationContext webApplicationContext;
    private ServletConfig servletConfig;

    @Autowired
    private ResponseCodeMapper responseCodeMapper;
    private static final String PROV_CONNECTION_ID = "soltura-prov-connection";
    private String provConnectionDownTrap;
    private String provConnectionUpTrap;

    @Override
    public Response createSdpProvisionedApp(Map<String, Object> provisionedAppRequest, String appId) {
        LOGGER.debug("Sending Provisioned App Creattion Request for User ID [{}] ", provisionedAppRequest.get(SP_ID));
        Map<String, Object> receivedResponse = doSend(provisionedAppRequest, createProAppUrl);
        if (receivedResponse != null) {
            LOGGER.debug("Received Response [ {} ]", receivedResponse);
            if (receivedResponse != null) {
                return responseCodeMapper.getProvAppCreateResponse(receivedResponse);
            }
        }
        return null;
    }

    @Override
    public Map<String, Object> validateAppName(Map<String, Object> requestMessage) {
        LOGGER.debug("sending request to check the availablity of the app name [ {} ] ", requestMessage);

        Map<String, Object> receivedResponse = doSend(requestMessage, validateAppnameUrl);

        if (receivedResponse != null) {
            LOGGER.debug("Received Response [ {} ]", receivedResponse);
            return receivedResponse;
        }
        return null;
    }

    @Override
    public Map<String, Object> getSdpAppStatus(Map<String, Object> appStateRequest) {
        LOGGER.debug("Finding App Status [{}]", appStateRequest);
        Map<String, Object> receivedResponse = doSend(appStateRequest, sdpAppStatusUrl);
        if (receivedResponse != null) {
            LOGGER.debug("Received Response [ {} ]", receivedResponse);
            return receivedResponse;
        }
        return null;
    }

    private Map<String, Object> doSend(Map<String, Object> request, String url) {
        try {
            Map<String, Object> result = (Map<String, Object>) makeRestCall(url, request, Map.class);
            if (result != null) {
                SnmpLogUtil.clearTrap(PROV_CONNECTION_ID, provConnectionUpTrap);
            } else {
                SnmpLogUtil.trap(PROV_CONNECTION_ID, provConnectionDownTrap);
            }
            return result;
        } catch (ClientWebApplicationException e) {
            SnmpLogUtil.trap(PROV_CONNECTION_ID, provConnectionDownTrap);
            throw e;
        }
    }

    @Override
    public Map<String, Object> getApp(Map requestMessage) {
        LOGGER.debug("Getting Available Apps [ {} ] ", requestMessage);
        Map<String, Object> receivedResponse = doSend(requestMessage, getApplicationsUrl);
        if (receivedResponse != null) {
            LOGGER.debug("Received Response [ {} ]", receivedResponse);
            return receivedResponse;
        }
        return null;
    }


    public void setCreateProAppUrl(String createProAppUrl) {
        this.createProAppUrl = createProAppUrl;
    }

    public void setValidateAppnameUrl(String appNameAvailableUrl) {
        this.validateAppnameUrl = appNameAvailableUrl;
    }

    public void setSdpAppStatusUrl(String sdpAppStatusUrl) {
        this.sdpAppStatusUrl = sdpAppStatusUrl;
    }

    public void setGetApplicationsUrl(String getApplicationsUrl) {
        this.getApplicationsUrl = getApplicationsUrl;
    }

    public void setProvConnectionDownTrap(String provConnectionDownTrap) {
        this.provConnectionDownTrap = provConnectionDownTrap;
    }

    public void setProvConnectionUpTrap(String provConnectionUpTrap) {
        this.provConnectionUpTrap = provConnectionUpTrap;
    }
}

