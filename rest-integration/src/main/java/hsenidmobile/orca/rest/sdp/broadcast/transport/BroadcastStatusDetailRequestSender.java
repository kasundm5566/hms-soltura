/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hsenidmobile.orca.rest.sdp.broadcast.transport;

import java.util.Map;

/**
 * REST - API integration interface for get the broad cast status details
 * $LastChangedDate: 2011-07-21 15:53:59 +0530 (Thu, 21 Jul 2011) $
 * $LastChangedBy: dulika $
 * $LastChangedRevision: 75147 $
 */
public interface BroadcastStatusDetailRequestSender {

    Map<String, Object> getBroadCastDetails(Map requestMessage);
}
