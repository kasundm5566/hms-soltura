/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hsenidmobile.orca.rest.sdp.broadcast.transport.http;

import hms.commons.SnmpLogUtil;
import hsenidmobile.orca.rest.sdp.broadcast.transport.BroadcastStatusDetailRequestSender;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.jaxrs.client.ClientWebApplicationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

import static hsenidmobile.orca.rest.common.util.RestApiKeys.APPLICATION_ID;
import static hsenidmobile.orca.rest.common.util.RestApiKeys.MESSAGE_ID;
import static hsenidmobile.orca.rest.util.RestUtils.makeRestCall;

/**
 * $LastChangedDate: 2011-07-21 15:53:59 +0530 (Thu, 21 Jul 2011) $
 * $LastChangedBy: dulika $
 * $LastChangedRevision: 75147 $
 */
public class BroadcastStatusDetailRequestSenderImpl implements BroadcastStatusDetailRequestSender{
    private static final Logger LOGGER = LoggerFactory.getLogger(BroadcastStatusDetailRequestSenderImpl.class);
    private String broadcastStatusDetailsUrl;
    private String sdpConnectionDownTrap;
    private String sdpConnectionUpTrap;

    @Override
    public Map<String, Object> getBroadCastDetails(Map requestMessage) {
        try {
            LOGGER.debug("sending request to get broadcast details for the appID [{}] and the messageID [{}] ", requestMessage.get(APPLICATION_ID), requestMessage.get(MESSAGE_ID));

            Map<String, Object> receivedResponse = (Map<String, Object>) makeRestCall(broadcastStatusDetailsUrl,
                    requestMessage, Map.class);
            if (receivedResponse != null) {
                LOGGER.debug("Received Response [ {} ]", receivedResponse);
                SnmpLogUtil.clearTrap("soltura-sdp-connection", sdpConnectionUpTrap);
                return receivedResponse;
            } else {
                SnmpLogUtil.trap("soltura-sdp-connection", sdpConnectionDownTrap);
                return null;
            }
        } catch (ClientWebApplicationException e) {
            SnmpLogUtil.trap("soltura-sdp-connection", sdpConnectionDownTrap);
            throw e;
        }
    }

    public void setBroadcastStatusDetailsUrl(String broadcastStatusDetailsUrl) {
        this.broadcastStatusDetailsUrl = broadcastStatusDetailsUrl;
    }

    public void setSdpConnectionDownTrap(String sdpConnectionDownTrap) {
        this.sdpConnectionDownTrap = sdpConnectionDownTrap;
    }

    public void setSdpConnectionUpTrap(String sdpConnectionUpTrap) {
        this.sdpConnectionUpTrap = sdpConnectionUpTrap;
    }
}
