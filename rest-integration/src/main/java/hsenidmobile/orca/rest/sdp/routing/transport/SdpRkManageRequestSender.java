/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hsenidmobile.orca.rest.sdp.routing.transport;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * REST - API integration interface for SDP routing key module
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public interface SdpRkManageRequestSender {

    /**
     *Sending Request to create routing key in SDP
     * @param requestMessage
     * @return
     */
    Map<String, Object> registerRks(Map requestMessage);

    /**
     * Sending request to find all available routing keys
     * @param requestMessage
     * @return
     */
    Map<String, Map<String, List<String>>> findAvailableRks(Map requestMessage);

    /**
     * Sending request to validate routing keys in SDP
     * @param requestMessage
     * @return
     */
    Map<String, Object> validateAvailableRks(Map requestMessage);

    /**
     * Sending request to retrieve all available short codes
     * @param requestMessage
     * @return
     */
    Map<String, Set<String>> retrieveShortcodes(Map requestMessage);

    /**
     * Sending request to retrieve all available short codes for settings
     * @param requestMessage
     * @return
     */
    List<Map<String, Object>> findAll(Map requestMessage);
}


