/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hsenidmobile.orca.rest.sdp.routing.transport.http;

import hms.commons.SnmpLogUtil;
import hsenidmobile.orca.rest.common.ResponseCodeMapper;
import hsenidmobile.orca.rest.sdp.routing.transport.SdpRkManageRequestSender;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.jaxrs.client.ClientWebApplicationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;
import java.util.Set;

import static hsenidmobile.orca.rest.common.util.RestApiKeys.SP_ID;
import static hsenidmobile.orca.rest.util.RestUtils.makeRestCall;

/**
 * REST - API integration interface implementation for SDP routing key module
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */

/**
 * SdpRkManageRequestSenderImpl Class
 */
public class SdpRkManageRequestSenderImpl implements SdpRkManageRequestSender {
    private static final Logger LOGGER = LoggerFactory.getLogger(SdpRkManageRequestSenderImpl.class);
    private String regRoutingKeyUrl;
    private String findRoutingKeyUrl;
    private String valRoutingKeyUrl;
    private String retrieveShortCodesUrl;
    private String shortCodesDetailsUrl;
    private static final String PROV_CONNECTION_ID = "soltura-prov-connection";
    private String provConnectionDownTrap;
    private String provConnectionUpTrap;

    @Autowired
    private ResponseCodeMapper responseCodeMapper;

    /**
     * Registering new Routing key
     *
     * @param requestMessage
     * @return
     */
    @Override
    public Map<String, Object> registerRks(Map requestMessage) {
        LOGGER.debug("Registering new Routing key for User ID [{}] ", requestMessage.get(SP_ID));

        Map<String, Object> receivedResponse = doSend(requestMessage, regRoutingKeyUrl);
        if (receivedResponse != null) {
            LOGGER.debug("Received Response [ {} ]", receivedResponse);
            return receivedResponse;
        }
        return null;
    }

    /**
     * Searching Available Routing keys
     *
     * @param requestMessage
     * @return
     */
    @Override
    public Map<String, Map<String, List<String>>> findAvailableRks(Map requestMessage) {
        LOGGER.debug("Finding available Routing keys for the User ID [{}] ",  requestMessage.get(SP_ID));

        Map<String, Map<String, List<String>>> receivedResponse = (Map<String, Map<String, List<String>>>)
                doSend(requestMessage, findRoutingKeyUrl);
        if (receivedResponse != null) {
            LOGGER.debug("Received Response [ {} ]", receivedResponse);
            return receivedResponse;
        }
        return null;
    }

    /**
     * For validation of an Available routing key
     *
     * @param requestMessage
     * @return
     */
    @Override
    public Map<String, Object> validateAvailableRks(Map requestMessage) {
        LOGGER.debug("Validating Routing key [ {} ] ", requestMessage);

        Map<String, Object> receivedResponse = doSend(requestMessage, valRoutingKeyUrl);
        if (receivedResponse != null) {
            LOGGER.debug("Received Response [ {} ]", receivedResponse);
            return receivedResponse;
        }
        return null;
    }

    @Override
    public Map<String, Set<String>> retrieveShortcodes(Map requestMessage) {
        LOGGER.debug("Retrieving Short Codes  [ {} ] ", requestMessage);

        Map<String, Set<String>> receivedResponse = (Map<String, Set<String>>)
                doSend(requestMessage, retrieveShortCodesUrl);
        if (receivedResponse != null) {
            LOGGER.debug("Received Response [ {} ]", receivedResponse);
            return receivedResponse;
        }
        return null;
    }

    @Override
    public List<Map<String, Object>> findAll(Map requestMessage) {
        LOGGER.debug("Retrieving Short Codes for settings [ {} ] ", requestMessage);

        List<Map<String, Object>> receivedResponse = (List<Map<String, Object>>)
                doSend(requestMessage, shortCodesDetailsUrl, List.class);
        if (receivedResponse != null) {
            LOGGER.debug("Received Response [ {} ]", receivedResponse);
            return receivedResponse;
        }
        return null;
    }

    private Map doSend(Map<String, Object> request, String url) {
        return doSend(request, url, Map.class);
    }

    private <T> T doSend(Map<String, Object> request, String url, Class<T> t) {
        try {
            T result = (T) makeRestCall(url, request, t);
            if (result != null) {
                SnmpLogUtil.clearTrap(PROV_CONNECTION_ID, provConnectionUpTrap);
            } else {
                SnmpLogUtil.trap(PROV_CONNECTION_ID, provConnectionDownTrap);
            }
            return result;
        } catch (ClientWebApplicationException e) {
            SnmpLogUtil.trap(PROV_CONNECTION_ID, provConnectionDownTrap);
            throw e;
        }
    }


    public void setValRoutingKeyUrl(String valRoutingKeyUrl) {
        this.valRoutingKeyUrl = valRoutingKeyUrl;
    }

    public void setFindRoutingKeyUrl(String findRoutingKeyUrl) {
        this.findRoutingKeyUrl = findRoutingKeyUrl;
    }

    public void setRegRoutingKeyUrl(String regRoutingKeyUrl) {
        this.regRoutingKeyUrl = regRoutingKeyUrl;
    }

    public void setRetrieveShortCodesUrl(String retrieveShortCodesUrl) {
        this.retrieveShortCodesUrl = retrieveShortCodesUrl;
    }

    public void setShortCodesDetailsUrl(String shortCodesDetailsUrl) {
        this.shortCodesDetailsUrl = shortCodesDetailsUrl;
    }

    public void setProvConnectionDownTrap(String provConnectionDownTrap) {
        this.provConnectionDownTrap = provConnectionDownTrap;
    }

    public void setProvConnectionUpTrap(String provConnectionUpTrap) {
        this.provConnectionUpTrap = provConnectionUpTrap;
    }
}