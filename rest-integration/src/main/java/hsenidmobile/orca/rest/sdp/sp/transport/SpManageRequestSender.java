/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hsenidmobile.orca.rest.sdp.sp.transport;

import java.util.Map;

/**
 * $LastChangedDate: 2011-06-22 13:32:40 +0530 (Wed, 22 Jun 2011) $
 * $LastChangedBy: supunh $
 * $LastChangedRevision: 74308 $
 */
public interface SpManageRequestSender {

    Map<String, Object> authenticateSpbyCpid(Map requestMessage);

    public Map<String, Object> findSp(final String spId);

}
