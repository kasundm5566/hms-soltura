/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hsenidmobile.orca.rest.sdp.sp.transport.http;

import hms.commons.SnmpLogUtil;
import hsenidmobile.orca.rest.sdp.sp.transport.SpManageRequestSender;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.jaxrs.client.ClientWebApplicationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

import static hsenidmobile.orca.rest.util.RestUtils.makeRestCall;

/**
 * $LastChangedDate: 2011-06-22 13:32:40 +0530 (Wed, 22 Jun 2011) $
 * $LastChangedBy: supunh $
 * $LastChangedRevision: 74308 $
 */
public class SpManageRequestSenderImpl implements SpManageRequestSender {
    private static final Logger LOGGER = LoggerFactory.getLogger(SpManageRequestSenderImpl.class);

    private String spAuthenticationEndPoint;
    private String spFindUrl;
    private static final String PROV_CONNECTION_ID = "soltura-prov-connection";
    private String provConnectionDownTrap;
    private String provConnectionUpTrap;

    @Override
    public Map<String, Object> authenticateSpbyCpid(Map requestMessage) {

        LOGGER.debug("Retrieving SP Details  [ {} ] ", requestMessage);

        Map<String, Object> receivedResponse = doSend(requestMessage, spAuthenticationEndPoint);

        if (receivedResponse != null) {
            LOGGER.debug("Received Response [ {} ]", receivedResponse);
            return receivedResponse;
        }
        return null;
    }

    @Override
    public Map<String, Object> findSp(final String spId) {

        LOGGER.debug("Find SP Details  [ {} ] with [{}]", spId, spFindUrl);

        HashMap<String, Object> request = new HashMap<String, Object>();
        request.put("sp-id", spId);

        Map<String, Object> receivedResponse = doSend(request, spFindUrl);

        if (receivedResponse != null) {
            LOGGER.debug("Received Response [ {} ]", receivedResponse);
            return receivedResponse;
        }
        return null;
    }

    private Map<String, Object> doSend(Map<String, Object> request, String url) {
        try {
            Map<String, Object> result = (Map<String, Object>) makeRestCall(url, request, Map.class);
            if (result != null) {
                SnmpLogUtil.clearTrap(PROV_CONNECTION_ID, provConnectionUpTrap);
            } else {
                SnmpLogUtil.trap(PROV_CONNECTION_ID, provConnectionDownTrap);
            }
            return result;
        } catch (ClientWebApplicationException e) {
            SnmpLogUtil.trap(PROV_CONNECTION_ID, provConnectionDownTrap);
            throw e;
        }
    }


    public void setSpAuthenticationEndPoint(String spAuthenticationEndPoint) {
        this.spAuthenticationEndPoint = spAuthenticationEndPoint;
    }

    public void setSpFindUrl(String spFindUrl) {
        this.spFindUrl = spFindUrl;
    }

    public void setProvConnectionDownTrap(String provConnectionDownTrap) {
        this.provConnectionDownTrap = provConnectionDownTrap;
    }

    public void setProvConnectionUpTrap(String provConnectionUpTrap) {
        this.provConnectionUpTrap = provConnectionUpTrap;
    }
}
