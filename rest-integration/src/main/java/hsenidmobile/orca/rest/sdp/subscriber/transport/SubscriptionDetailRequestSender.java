/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hsenidmobile.orca.rest.sdp.subscriber.transport;

import hsenidmobile.orca.rest.common.Response;

/**
 * REST - API integration interface for Subscription module
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */

public interface SubscriptionDetailRequestSender {

    Response getSubscriberCount(String appId, String password);

}
