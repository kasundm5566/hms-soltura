/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hsenidmobile.orca.rest.sdp.subscriber.transport.http;

import hms.common.rest.util.Message;
import hms.commons.SnmpLogUtil;
import hsenidmobile.orca.rest.common.Response;
import hsenidmobile.orca.rest.common.ResponseCodeMapper;
import hsenidmobile.orca.rest.common.transformer.Transformer;
import hsenidmobile.orca.rest.sdp.subscriber.transport.SubscriptionDetailRequestSender;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.jaxrs.client.ClientWebApplicationException;
import org.apache.cxf.jaxrs.client.WebClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.core.MediaType;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import static hsenidmobile.orca.rest.common.util.RestApiKeys.APPLICATION_ID;
import static hsenidmobile.orca.rest.common.util.RestApiKeys.PASSWORD;
import static hsenidmobile.orca.rest.util.RestUtils.makeRestCall;

/**
 * REST - API integration interface implementation for Subscription module
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class SubscriptionDetailRequestSenderImpl implements SubscriptionDetailRequestSender {

    private String url;
    private static final Logger LOGGER = LoggerFactory.getLogger(SubscriptionDetailRequestSenderImpl.class);
    private static final String PROV_CONNECTION_ID = "soltura-prov-connection";
    private String provConnectionDownTrap;
    private String provConnectionUpTrap;

    @Autowired
    private ResponseCodeMapper responseCodeMapper;

    @Override
    public Response getSubscriberCount(String appId, String password)  {
        LOGGER.debug("Retrieving Number of registered user for application ID [{}] ", appId);
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put(APPLICATION_ID, appId);
        parameters.put(PASSWORD, password);
        Map<String, Object> receivedResponse = doSend(parameters, url);
        if (receivedResponse != null) {
            return responseCodeMapper.getSubscriberResponse(receivedResponse);
        }
        return null;
    }

    private Map<String, Object> doSend(Map<String, Object> request, String url) {
        try {
            Map<String, Object> result = (Map<String, Object>) makeRestCall(url, request, Map.class);
            if (result != null) {
                SnmpLogUtil.clearTrap(PROV_CONNECTION_ID, provConnectionUpTrap);
            } else {
                SnmpLogUtil.trap(PROV_CONNECTION_ID, provConnectionDownTrap);
            }
            return result;
        } catch (ClientWebApplicationException e) {
            SnmpLogUtil.trap(PROV_CONNECTION_ID, provConnectionDownTrap);
            throw e;
        }
    }


    public void setUrl(String url) {
        this.url = url;
    }

    public void setProvConnectionDownTrap(String provConnectionDownTrap) {
        this.provConnectionDownTrap = provConnectionDownTrap;
    }

    public void setProvConnectionUpTrap(String provConnectionUpTrap) {
        this.provConnectionUpTrap = provConnectionUpTrap;
    }
}

