/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hsenidmobile.orca.rest.util;

import hms.common.rest.util.JsonBodyProvider;
import org.apache.cxf.jaxrs.client.WebClient;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import javax.ws.rs.core.MediaType;
import java.util.LinkedList;
import java.util.Map;

/**
 * Utility class for rest-integration module
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class RestUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(RestUtils.class);

    /**
     * Sending request to the given URL                 
     * @param restUrl
     * @param parameters
     * @param aClass
     * @return
     */
    public static Object makeRestCall(String restUrl, Map<String, Object> parameters, Class aClass) {
        LinkedList<Object> providers = new LinkedList<Object>();
        providers.add(new JsonBodyProvider());
        LOGGER.debug("Sending Request [{}] ", parameters);
        WebClient regClient = WebClient.create( restUrl, providers);
        regClient.header("Content-Type", MediaType.APPLICATION_JSON);
        regClient.accept(MediaType.APPLICATION_JSON);
        return regClient.invoke("POST", parameters, aClass);
    }
}