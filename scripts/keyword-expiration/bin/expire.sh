#!/bin/bash

#Import configurations
source ../conf/system.conf

#Get the expire date
day=$(date --date="-$KEYWORD_EXPIRE_DAYS day" +"%_d")
month=$(($(date --date="-$KEYWORD_EXPIRE_DAYS day" +"%_m") - 1))
year=$(date --date="-$KEYWORD_EXPIRE_DAYS day" +"%_Y")

#Set the expire date
date=$year,$month,$day

#Set the last log date
logdate=$(date +"%F %T")

#Create necessary directory if not exists
mkdir -p $LOG_PATH

#Flag for first keyword
checkflag=true

#Keep first keyword
firskey=""

#Process all the expired records in the directory
echo "$logdate - Keywords expiration started" >> $EXPIRATION_LOG

echo "$logdate - Keywords Removing Process started" >> $EXPIRATION_LOG

while : 
do
	logdate=$(date +"%F %T")

	#Make mongo findOne query
	query="db.routing_keys.findOne({\"keyword\":{\$ne:\"\"},\"app-id\":\"\",\"exclusive\":false,\"last-updated-time\":{\$lt:new Date($date)}},{\"_id\":1})"

	#Get ID of one record in the database
	recordid=$(echo $query|mongo $MONGO_HOST:$MONGO_PORT/$MONGO_DATABASE --quiet|sed -e 's/bye/''/g')

	#Check whether there are expired keywords
	if [ "1" != $(echo $recordid|grep -c "null") ];then
		keywordquery="db.routing_keys.find($recordid,{\"_id\":0,\"keyword\":1})"
		infoquery="db.routing_keys.find($recordid)"

		#Get the record info
		info=$(echo $infoquery|mongo $MONGO_HOST:$MONGO_PORT/$MONGO_DATABASE --quiet|sed -e 's/bye/''/g')
		keyword=$(echo $keywordquery|mongo $MONGO_HOST:$MONGO_PORT/$MONGO_DATABASE --quiet|sed -e 's/bye/''/g')

		#break the loop if the current keyword already been processed
		if [ "$keyword" == "$firskey" ];then
			break
		fi

		#Set first keyword
		if $checkflag ;then
			firskey=$keyword
		fi
		
		echo "$logdate - Processing - $keyword" >> $EXPIRATION_LOG
		echo "$logdate - Full Information On - $keyword - $info" >> $EXPIRATION_LOG

		#Remove the record from the database

		#Make mongo remove query
		query="db.routing_keys.remove($recordid)"

		echo "$logdate - Removing Entry - $keyword" >> $EXPIRATION_LOG

		#Remove keyword
		result=$(echo $query|mongo $MONGO_HOST:$MONGO_PORT/$MONGO_DATABASE --quiet|sed -e 's/bye/''/g')
		echo "$logdate - Processed - $keyword" >> $EXPIRATION_LOG

		#If the first key is checked
		checkflag=false
	else
		#If there are no keywords
		if $checkflag ;then
			echo "$logdate - There are no keywords to expire" >> $EXPIRATION_LOG
		fi

		break
	fi
done

echo "$logdate - Keywords Removing Process ended" >> $EXPIRATION_LOG

checkflag=true

firskey=""

echo "$logdate - Removing SP ID in Keywords Process started" >> $EXPIRATION_LOG

while : 
do
	logdate=$(date +"%F %T")

	#Make mongo findOne query
	query="db.routing_keys.findOne({\"keyword\":{\$ne:\"\"},\"sp-id\":{\$ne:\"\"},\"app-id\":\"\",\"exclusive\":true,\"last-updated-time\":{\$lt:new Date($date)}},{\"_id\":1})"

	#Get ID of one record in the database
	recordid=$(echo $query|mongo $MONGO_HOST:$MONGO_PORT/$MONGO_DATABASE --quiet|sed -e 's/bye/''/g')

	#Check whether there are expired keywords
	if [ "1" != $(echo $recordid|grep -c "null") ];then
		keywordquery="db.routing_keys.find($recordid,{\"_id\":0,\"keyword\":1})"
		infoquery="db.routing_keys.find($recordid)"

		#Get the record info
		info=$(echo $infoquery|mongo $MONGO_HOST:$MONGO_PORT/$MONGO_DATABASE --quiet|sed -e 's/bye/''/g')
		keyword=$(echo $keywordquery|mongo $MONGO_HOST:$MONGO_PORT/$MONGO_DATABASE --quiet|sed -e 's/bye/''/g')
		
		#break the loop if the current keyword already been processed
		if [ "$keyword" == "$firskey" ];then
			break
		fi

		#Set first keyword
		if $checkflag ;then
			firskey=$keyword
		fi
		
		echo "$logdate - Processing - $keyword" >> $EXPIRATION_LOG
		echo "$logdate - Full Information On - $keyword - $info" >> $EXPIRATION_LOG

		#Update the record from the database

		#Make mongo update query
		query="db.routing_keys.update($recordid,{\$set:{\"sp-id\":\"\"}})"

		echo "$logdate - Removing SP ID - $keyword" >> $EXPIRATION_LOG

		#Update keyword
		result=$(echo $query|mongo $MONGO_HOST:$MONGO_PORT/$MONGO_DATABASE --quiet|sed -e 's/bye/''/g')
		echo "$logdate - Processed - $keyword" >> $EXPIRATION_LOG

		#If the first key is checked
		checkflag=false
	else
		#If there are no keywords
		if $checkflag ;then
			echo "$logdate - There are no keywords to expire" >> $EXPIRATION_LOG
		fi

		break
	fi
done

echo "$logdate - Removing SP ID in Keywords Process ended" >> $EXPIRATION_LOG

echo "$logdate - Keywords expiration ended" >> $EXPIRATION_LOG
echo "" >> $EXPIRATION_LOG
