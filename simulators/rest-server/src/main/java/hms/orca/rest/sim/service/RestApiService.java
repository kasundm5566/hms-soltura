
/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.orca.rest.sim.service;

import hms.common.rest.util.Message;
import hms.orca.rest.api.request.*;
import hms.orca.rest.api.response.BasicResponse;
import hms.pgw.api.request.PayInstrumentQueryMessage;
import hms.pgw.api.response.PayInstrumentResponse;
import javax.ws.rs.core.Response;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */

public interface RestApiService {

    BasicResponse login(LoginRequest loginRequest);

    BasicResponse spLogin(SpLoginRequest spLoginRequest);

    PayInstrumentResponse getFinancialInstruments(PayInstrumentQueryMessage payInstrumentQueryMessage);

    BasicResponse addErrorCode(String errorcode, String userame);

    BasicResponse createProvisionedApp(Message provisionedAppRequest);

    BasicResponse getSubscriberCount(SubscriptionDetailRequest subscriptionDetailRequest);

    BasicResponse getAvailableShortCodes(SdpRoutingkeyRequest sdpRoutingkeyRequest);

    Message validateAvailableRks(Message message);

    Message registerRks(Message message);

    Message findAvailableRks(Message request);

    Message retrieveShortcodes(Message message);

    Response aventuraSMSsenderListner(Message message);

    Message retrieveStatusCode(Message message);

}




