/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.orca.rest.sim.service.impl;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */

import hms.common.rest.util.Message;
import hms.orca.rest.api.request.*;
import hms.orca.rest.api.response.BasicResponse;
import hms.orca.rest.sim.service.RestApiService;
import hms.orca.rest.sim.util.ResponseCodeStore;
import hms.pgw.api.common.PayInstrumentInfo;
import hms.pgw.api.request.PayInstrumentQueryMessage;
import hms.pgw.api.response.PayInstrumentResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.Map;

import static hms.orca.rest.api.util.ResponseCodes.*;

@Path("/restapiservice/")
@Produces("application/json")
public class RestApiServiceImpl implements RestApiService {

    private static final Logger logger = LoggerFactory.getLogger(RestApiServiceImpl.class);
    private static Map<String, String> availableFinanacialInstruments = new HashMap();
    private static Map<String, String> availableRoutingKeyMap =  new HashMap();
    private static Map<String, Object> responseMap = new HashMap<String, Object>();

    static {
        availableFinanacialInstruments.put("BankA", "BankID");
        availableFinanacialInstruments.put("Mobile-WalletA", "WalletID");

        availableRoutingKeyMap.put("Airtel", "5000 - key1");
        availableRoutingKeyMap.put("Yu", "5050 - key1");
        availableRoutingKeyMap.put("Safaricom", "5555 - key1");
    }

    public RestApiServiceImpl() {
    }

    @Path("register")
    @Produces({"application/json"})
    @Consumes({"application/json"})
    @POST
    @Override
    public BasicResponse login(LoginRequest loginRequest) {
        logger.debug("Common Registration Request Received [{}] ", loginRequest);
        String configuredResponse = ResponseCodeStore.getResponseCode(loginRequest.getUserName().trim());
        if(configuredResponse != null) {
            return new BasicResponse(configuredResponse, CR_ERROR_MESSAGE);
        }
        return new BasicResponse(CR_SUCCESS_CODE, CR_SUCCESS_MESSAGE);
    }

    @Path("provisioning/splogin")
    @Produces({"application/json"})
    @Consumes({"application/json"})
    @POST
    @Override
    public BasicResponse spLogin(SpLoginRequest spLoginRequest) {
        logger.debug("Provisioning SP Login Request Received [{}] ", spLoginRequest);
        String configuredResponse = ResponseCodeStore.getResponseCode(spLoginRequest.getUserName());
        if(configuredResponse != null) {
            return new BasicResponse(configuredResponse, PROV_SP_ERROR_MESSAGE);
        }
        return new BasicResponse(PROV_SP_SUCCESS_CODE, PROV_SP_SUCCESS_MESSAGE);
    }

    @Path("pg/instruments")
    @Produces({"application/json"})
    @Consumes({"application/json"})
    @POST
    @Override
    public PayInstrumentResponse getFinancialInstruments(PayInstrumentQueryMessage payInstrumentQueryMessage) {
        logger.debug("Payment Gateway Request Received [{}] ", payInstrumentQueryMessage);
        PayInstrumentResponse payInstrumentResponse = new PayInstrumentResponse();
        payInstrumentResponse.setUserId(payInstrumentQueryMessage.getUserId());
        PayInstrumentInfo payInstrumentInfo1 = new PayInstrumentInfo();
        PayInstrumentInfo payInstrumentInfo2 = new PayInstrumentInfo();
        payInstrumentInfo1.setDefault(true);
        payInstrumentInfo1.setPayInstrumentName("Bank");
        payInstrumentInfo1.setPayInsAccountId("11111");
        payInstrumentInfo2.setDefault(false);
        payInstrumentInfo2.setPayInstrumentName("Mobile");
        payInstrumentInfo2.setPayInsAccountId("22222");
        payInstrumentResponse.setPayInstrumentInfos(new PayInstrumentInfo[] {
                payInstrumentInfo1, payInstrumentInfo2
        });
        return payInstrumentResponse;
    }

    @Path("/provisioning/createapp")
    @Produces({"application/json"})
    @Consumes({"application/json"})
    @POST
    @Override
    public BasicResponse createProvisionedApp(Message provisionedAppRequest) {
        logger.debug("Provisioning SDP app creation Login Request Received [{}] ", provisionedAppRequest);
        return new BasicResponse(SDP_SUCCESS_CODE, SDP_SUCCESS_MESSAGE);
    }

    @GET
    @Path("/config/{errorcode}/{username}")
    @Override
    public BasicResponse addErrorCode(@PathParam("errorcode") String errorcode,
                                      @PathParam("username") String userName) {
        logger.debug("Error Code Config Request Received [{}] - [{}]  ", userName, errorcode);
        ResponseCodeStore.insertResponseCode(userName, errorcode);
        return new BasicResponse();
    }

    @Path("sdp/subscriber")
    @Produces({"application/json"})
    @Consumes({"application/json"})
    @POST
    @Override
    public BasicResponse getSubscriberCount(SubscriptionDetailRequest subscriptionDetailRequest) {
        logger.debug("Subscription count Detail Request Received [{}] ", subscriptionDetailRequest);

//        String configuredResponse = ResponseCodeStore.getResponseCode(subscriberCountRequest.getAppId());
//        if(configuredResponse != null) {
//            return new BasicResponse(configuredResponse, PG_ERROR_MESSAGE);
//        }

        return new BasicResponse(PG_SUCCESS_CODE, "100");
    }


    @Path("/sdp/availableShortCodes")
    @Produces({"application/json"})
    @Consumes({"application/json"})
    @POST
    @Override
    public BasicResponse getAvailableShortCodes(SdpRoutingkeyRequest sdpRoutingkeyRequest) {
        logger.debug("sdp ShortCodes Request Received [{}] ", sdpRoutingkeyRequest);
        return new BasicResponse(PG_SUCCESS_CODE, PG_SUCCESS_MESSAGE, availableRoutingKeyMap);
    }


    @Path("/sdp/validate")
    @Produces({"application/json"})
    @Consumes({"application/json"})
    @POST
    @Override
    public Message validateAvailableRks(Message message) {
        logger.debug("Keyword validation Message Received [{}] ", message);

        Map<String, Object> responseMap = new HashMap<String, Object>();
        responseMap.put("status", "not-available");

        return new Message(responseMap);
    }


    @Path("/sdp/register")
    @Produces({"application/json"})
    @Consumes({"application/json"})
    @POST
    @Override
    public Message registerRks(Message message) {
        logger.debug("Keyword Registration Message Received [{}] ", message);

        Map<String, Object> regRkMap = new HashMap<String, Object>();
        regRkMap.put("status", "success");

        return new Message(regRkMap);
    }

    @Path("/sdp/find")
    @Produces({"application/json"})
    @Consumes({"application/json"})
    @POST
    @Override
    public Message findAvailableRks(Message request) {
        logger.debug("Routing Key Retrieval Message Received [{}] ", request);

//        {operator1:{9696:key1,1234:key2,3456:yek3}, operator2:{123:sdd,1234:ert,345:sdfsd}}

        responseMap.put("airtel", "{9696:key1,1234:key2}");
        responseMap.put("safaricom", "{1234:sdd,12346:ert,3455:sdfsd}");

        return new Message(responseMap);
    }

    @Path("/sdp/retrieveScs")
    @Produces({"application/json"})
    @Consumes({"application/json"})
    @POST
    @Override
    public Message retrieveShortcodes(Message message) {
        logger.debug("Short Code Retrieval Message Received [{}] ", message);

//        {operator1:(9696,1234,3456), operator2:(123,1234,345)}

        Map<String, Object> responseMap2 = new HashMap<String, Object>();
        responseMap2.put("operator1", "[9696,1234,3456]");
        responseMap2.put("operator2", "[123,1234,345]");

        return new Message(responseMap2);
        
    }

    @Path("/aventura")
    @Produces({"application/json"})
    @Consumes({"application/json"})
    @POST
    @Override
    public Response aventuraSMSsenderListner(Message message) {
        return Response.ok().build();
    }

    @Path("/sdp/status")
    @Produces({"application/json"})
    @Consumes({"application/json"})
    @POST
    @Override
    public Message retrieveStatusCode(Message message) {
        logger.debug("Status Code Retrieval Message Received [{}]" , message);

        String appId = message.getParameters().get("app-id");
        Map<String, Object> responseMap2 = new HashMap<String, Object>();
        if ("vote_5209".equals(appId)) {
            responseMap2.put("status", "PENDING");
        } else if ("vvvv_7270".equals(appId)) {
            responseMap2.put("status", "INACTIVE");
        }  else if ("vote_5938".equals(appId)) {
            responseMap2.put("status", "Requested");
        } else {
            responseMap2.put("status", "ACTIVE");
        }

        Message response = new Message(responseMap2);

        logger.debug("Status Response [{}]", response);

        return response;

    }
}



