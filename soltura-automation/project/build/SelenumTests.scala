import sbt._
import reaktor.scct.ScctProject

class selenium_test(info: ProjectInfo) extends DefaultProject(info) with ScctProject {

  val newReleaseToolsRepository = "Scala Tools Repository" at
   "http://nexus.scala-tools.org/content/repositories/snapshots/"

  override def libraryDependencies = Set(
    "org.seleniumhq.selenium.client-drivers" % "selenium-java-testng-helper" % "1.0.1",
    "org.scalatest" % "scalatest" % "1.2-for-scala-2.8.0.final-SNAPSHOT" % "test",
    "mysql" % "mysql-connector-java" % "5.1.6"
	) ++ super.libraryDependencies

}