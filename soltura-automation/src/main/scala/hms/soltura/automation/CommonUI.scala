package hms.soltura.automation

import com.thoughtworks.selenium._
import org.testng.Assert._
import java.util.ResourceBundle
import java.util.Locale
import hms.soltura.automation.DataProvider._
import hms.soltura.automation.CommonUiParams._
import hms.soltura.automation.login.LoginComponent._
import hms.soltura.automation.home.HomeComponent._



/**
 * $LastChangedDate:$
 * $LastChangedBy:$
 * $LastChangedRevision:$
 */
class CommonUI extends SeleneseTestCase {

  lazy val ss: Selenium = seleniumRef
  import ss._

  def seleniumRef: Selenium = selenium

  val loc: Locale = new Locale(locale)
  val rbundle: ResourceBundle = ResourceBundle.getBundle("messages",loc)

  def login = {
    `type`(usernameText, "shamilacorp ")
    `type`(pwdText, "shamila12!@")
    clickAndWait(loginButton)
  }

  def logout = {
    click("link=Logout")
    clickAndWait("popup_ok")
    assertTrue(isTextPresent("Logout successful"))
  }

  def clickAndWait(url: String) = {
    click(url)
    waitForPageToLoad(timeout)
  }

  def gotoHome = {
    clickAndWait(homeLinkLocation)
    assertTrue(isTextPresent(rbundle.getString("create.application")))
    assertTrue(isTextPresent(rbundle.getString("my.applications")))
  }

  def gotoCreateApp = {
    clickAndWait(createAppLinkLocation)
    assertTrue(isTextPresent(rbundle.getString("create.new.application")))
  }

  def gotoMyApp = {
    clickAndWait(myAppLinkLocation)
  }

  def gotoSetting = {
    clickAndWait(mySettingLinkLocation)
    //    assertTrue(isTextPresent("My Profile"))
    assertTrue(isTextPresent("Keywords"))
  }

  def gotoReport = {
    clickAndWait(reportLinkLocation)
    // assertTrue(isTextPresent("Revenue Summary"))
    assertTrue(isTextPresent(rbundle.getString("message.history.report")))
  }

}