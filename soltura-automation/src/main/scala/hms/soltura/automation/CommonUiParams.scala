package hms.soltura.automation

/**
 * $LastChangedDate:$
 * $LastChangedBy:$
 * $LastChangedRevision:$
 */
object CommonUiParams {

  val APP_NAME_VOTE : String = "APPVote"
  val APP_NAME_ALERT : String = "APPAlert"
  val APP_NAME_REQ : String = "APPReq"
  val APP_NAME_CHA : String = "APPCha";

  val rnd = new scala.util.Random
  var randomNo = rnd.nextInt(1000)
  val userName : String = "user" + randomNo;
  val pwd : String = "test";


  val SHORT_CODE : String = "2545"
  val SHORT_CODE1 : String = "1234 - Airtel"
  val SHORT_CODE2 : String = "1234 - Safaricom"

  val KEYWORD : String = "key"
  val SMS_UPDATE : String = "SMS"
  val ONE_VOTE : String = "ONE_VOTE"
  val RESPONSE : String = "RESPONSE"
  val SUB_REQ : String = "SUB"

  val DURATION : String = "DUR"
  val DATE : String = "DATE"
  val DAY : String = "DAY"
  val HOUR : String = "HOUR"
  val MIN : String = "MIN"
  val BUFFER : String = "BUFF"

  val SUB_CODE1: String = "SUB_CODE1"
  val SUB_DESC1: String = "SUB_DESC1"

  val SUB_CODE2: String = "SUB_CODE2"
  val SUB_DESC2: String = "SUB_DESC2"

  val SUB_CODE3: String = "SUB_CODE3"
  val SUB_DESC3: String = "SUB_DESC3"

  val SUB_CODE4: String = "SUB_CODE4"
  val SUB_DESC4: String = "SUB_DESC4"

  val SUB_CODE5: String = "SUB_CODE5"
  val SUB_DESC5: String = "SUB_DESC5"

  val SUB_CODE6: String = "SUB_CODE6"
  val SUB_DESC6: String = "SUB_DESC6"

  val SUB_CODE7: String = "SUB_CODE7"
  val SUB_DESC7: String = "SUB_DESC7"

  val SUB_CODE8: String = "SUB_CODE8"
  val SUB_DESC8: String = "SUB_DESC8"

  val SUB_CODE9: String = "SUB_CODE9"
  val SUB_DESC9: String = "SUB_DESC9"

  val SUB_CODE10: String = "SUB_CODE10"
  val SUB_DESC10: String = "SUB_DESC10"

  val SUB_CODE11: String = "SUB_CODE11"
  val SUB_DESC11: String = "SUB_DESC11"

  val SUB_CODE12: String = "SUB_CODE12"
  val SUB_DESC12: String = "SUB_DESC12"

  val CODE1: String = "CODE1"
  val OPERATOR1: String = "OPERATOR1"
  val KEY1: String = "key"
}