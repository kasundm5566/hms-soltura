package hms.soltura.automation

import hms.soltura.automation.CommonUiParams._
import collection.mutable.HashMap

/**
 * $LastChangedDate:$
 * $LastChangedBy:$
 * $LastChangedRevision:$
 */
object DataProvider {

  val ip : String = "http://127.0.0.1";
  val port : String = ":4287";
  val host : String = ip + port;
  val locale : String = "en";
  val openUrl : String = ip + port + "/soltura/login.html?locale="+locale
  val homepageUrl= ip + port +"/soltura/home.html"
  val browser : String = "*firefox"
  val speed : String = "1000"
  val timeout : String = "15000"

  def getApplicationDetails : HashMap[String, String] = {
    val map = new HashMap[String, String]
    map += APP_NAME_VOTE -> (APP_NAME_VOTE + randomNo)
    map += APP_NAME_ALERT -> (APP_NAME_ALERT + randomNo)
    map += APP_NAME_REQ -> (APP_NAME_REQ + randomNo)
    map += APP_NAME_CHA -> (APP_NAME_CHA + randomNo)
    map += SHORT_CODE -> "2545"
    map += SHORT_CODE1 -> "1234 - airtel"
    map += SHORT_CODE2 -> "4584 - safaricom"
    map += KEYWORD -> (KEYWORD + randomNo)

    map += SUB_CODE1 -> "c1"
    map += SUB_DESC1 -> "d1"
    map += SUB_CODE2 -> "c2"
    map += SUB_DESC2 -> "d2"
    map += SUB_CODE3 -> "c3"
    map += SUB_DESC3 -> "d3"
    //    map += SUB_CODE4 -> "c4"
    //    map += SUB_DESC4 -> "d4"
    //    map += SUB_CODE5 -> "c5"
    //    map += SUB_DESC5 -> "d5"
    //    map += SUB_CODE6 -> "c6"
    //    map += SUB_DESC6 -> "d6"
    //    map += SUB_CODE7 -> "c7"
    //    map += SUB_DESC7 -> "d7"
    //    map += SUB_CODE8 -> "c8"
    //    map += SUB_DESC8 -> "d8"
    //    map += SUB_CODE9 -> "c9"
    //    map += SUB_DESC9 -> "d9"
    //    map += SUB_CODE10 -> "c10"
    //    map += SUB_DESC10 -> "d10"
    //    map += SUB_CODE11 -> "c11"
    //    map += SUB_DESC11 -> "d11"
    //    map += SUB_CODE12 -> "c12"
    //    map += SUB_DESC12 -> "d12"

    map += SMS_UPDATE -> "yes"
    map += ONE_VOTE -> "yes"
    map += RESPONSE -> "no"

    randomNo += 1

    return map
  }

  def getKeywords : HashMap[String, String] = {
    val map = new HashMap[String, String]
    map += OPERATOR1 -> "Operator1"
    map += KEY1 -> (CODE1 + "-" + KEYWORD + randomNo)

    randomNo += 1

    return map
  }
}

