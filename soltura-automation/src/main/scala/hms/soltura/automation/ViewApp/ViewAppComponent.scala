package hms.soltura.automation.ViewApp

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */

object ViewAppComponent {

  val ChannelMyAppLink= "//div[@id='select_type']/div[2]/div[1]/div[1]/div/div[4]/div[1]/div/div/div[1]/div[2]/div[2]/a"
  val AlertMyAppLink  = "//div[@id='select_type']/div[2]/div[1]/div[1]/div/div/div[1]/div[2]/div[2]/a"
  val VoteMyAppLink   = "//div[@id='select_type']/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[2]"
  val RequestMyAppLink= "//div[@id='select_type']/div[2]/div[1]/div[1]/div/div/div[1]/div[2]/div[2]/a[2]"

  val searchbox = "searchInput"
  val searchbutton = "Search"

  val myApplicationLink = "//div[@id='success_link']/h3/a"

  val AlertAppViewLink  = "//div[@id='select_type']/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[2]"
  val VoteAppViewLink   = "//div[@id='select_type']/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]"
  val RequestAppViewLink= "//div[@id='select_type']/div[2]/div[1]/div[1]/div/div/div[1]/div[2]/div[2]/a[1]"
  val ChannelAppViewLink= AlertAppViewLink

  val chartsMenu = "chartLink"
  val settingsMenu = "settingLink"
  val usageMenu = "usageLink"
}


