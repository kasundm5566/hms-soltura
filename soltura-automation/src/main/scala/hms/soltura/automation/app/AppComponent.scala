package hms.soltura.automation.app

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
object AppComponent {

  val voteAppLink = "//div[@id=\"home_image\"]/div[1]/div[@id=\"menu_voting\"]/ul/li/a"
  val appNameText = "appName"
  val label = "label="
  val keywordLink1 = "//form[@id='quick-press']/div[1]/div[3]/ul/dt[1]/a"
  val keywordLink2 = "//form[@id='quick-press']/div[1]/div[3]/ul/dt[2]/a"

  val keywordText = "keyword"

  val checkBox1 = "//select[@name='selectedRoutingKeyValues[0]']"
  val checkBox2 = "//select[@name='selectedRoutingKeyValues[1]']"
  val checkBox3 = "//select[@name='selectedRoutingKeyValues[2]']"
  val ExpirableCheckBox = "expirableAppSelectionId"
  val CommonResponseCheckBox = "commonResponseSelectionId"

  val startDate = "startDate"
  val endDate = "endDate"

  val calander = "ui-datepicker-div"
  val calanderLeft = "//div[@id='ui-datepicker-div']/div[2]/div[1]/a"
  val calanderRight = "//div[@id='ui-datepicker-div']/div[2]/div[3]/a"
  val monthDrpdwn = "//div[@id='ui-datepicker-div']/div[3]/div/select[1]"
  val yearDrpdwn = "//div[@id='ui-datepicker-div']/div[3]/div/select[2]"
  val date = "//div[@id='ui-datepicker-div']/div[3]/table/tbody/tr[4]/td[3]/a"
  val eDate = "//div[@id='ui-datepicker-div']/div[3]/table/tbody/tr[4]/td[4]/a"
  val nextButton = "next"
  val agreeButton = "ok"
  val backButton = "back"
  val keyCodeCombo = "shortcode"
  val descText = "decs"
  val plusSign = "plus"

  val msgDuration = "//select[@name='selectedPeriodUnit']"
  val dispatchTime = "//select[@name='subscriptionDispatchMinute']"
  val bufferTime = "//select[@name='subscriptionDispatchBufferTime']"

  val deleteSign = "divDel_"
  val InputField = "input_"

  val oneVoteBox = "onevote1"
  val responseCombo = "responseSpecification"
  val responseYesLabel = "label=Respond With Current Results"
  val alertAppLink = "//div[@id=\"home_image\"]/div[2]/div[@id=\"menu_alert\"]/ul/li/a"
  val reqAppLink = "//div[@id=\"home_image\"]/div[2]/div[@id=\"menu_req\"]/ul/li/a"
  val chaAppLink = "//div[@id='menu_channel']/ul/li/a"
  val responseText = "responseMessage"
  val smsBox = "smsupdate"

  val advancedSettings = "//a[@id='open']"

  val chargingConfiguration = "//div[@class='demo']/h2[2]/a[@title='Expand/Collapse']"
  val chargingConfiguration_vote = "//div[@id='advance_content']/div/h2/a"

  val respondCofiguration = "//div[@class='demo']/h2[@id='advance_config']/a[@title='Expand/Collapse']"
  val ExpandAll = "//p[@class='switch']/a[1]"
  val CollapseAll = "//p[@class='switch']/a[2]"

  val unsubscriberSuccessfulMassage = "us"
  val label_unsubscriberSuccessfulMassage = "//div[@id='advance_content']/div/div[1]/h4[1]/label"

  val label_Charging = "//div[@id='advance_content'/div/div[2]/h4/label]"

  val charginForMassage_Chbox_div = "//div[@id='checkbox_1_Selected']"
  val charginForSubscription_Chbox_div = "//div[@id='checkbox_2_Selected']"

  val checkBox_charginForSubscription = "cps"
  val checkBox_charginForMassage = "cpm"

  val RB_FromSubscriber = "cfs_per"
  val RB_FromContentProvider = "cfc_rb"
  val RB_FromOperator = "cfc2"
  val RB_mobile = "rb_Mobile"

}
