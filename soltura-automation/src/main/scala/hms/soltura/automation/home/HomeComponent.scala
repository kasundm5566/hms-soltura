package hms.soltura.automation.home

/**
 * $LastChangedDate:$
 * $LastChangedBy:$
 * $LastChangedRevision:$
 */
object HomeComponent {

  val createAppLinkLocation : String = "//div[@id='menu1']/ul/li/a/div"
  val myAppLinkLocation : String = "//div[@id='menu3']/ul/li/a/div"
  val mySettingLinkLocation : String = "//ul/li/a[@title=\"Settings\"]/div"
  val reportLinkLocation : String = "//div[@id='menu4']/ul/li/a"
  val homeLinkLocation : String = "link=Home"
  val faqLinkLocation : String = "link=FAQ"

}