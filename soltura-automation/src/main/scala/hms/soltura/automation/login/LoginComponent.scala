package hms.soltura.automation.login

/**
 * $LastChangedDate:$
 * $LastChangedBy:$
 * $LastChangedRevision:$
 */
object LoginComponent  {

  val usernameText :String = "username"
  val pwdText :String = "password"
  val loginButton :String = "submit"

}