package hms.soltura.automation.settings

/**
 * $LastChangedDate: 2011-05-19 19:39:26 +0530 (Thu, 19 May 2011) $
 * $LastChangedBy: sanjeewas $
 * $LastChangedRevision: 73222 $
 */

object SettingsComponent {

  val appNameText = "appName"
  val label = "label="
  val keywordText = "keyword"

  val nextButton = "next"

  val keywordLink = ""

}