package hms.soltura.automation.common.ViewApp

import hms.soltura.automation.CommonUI
import hms.soltura.automation.CommonUiParams._
import org.scalatest.testng.TestNGSuite
import collection.mutable.HashMap
import org.testng.annotations._
import com.thoughtworks.selenium._
import org.testng.Assert._
import org.testng.annotations._
import org.scalatest.testng.TestNGSuite
import collection.mutable.HashMap
import java.util.Calendar
import java.text.SimpleDateFormat
import hms.soltura.automation.DataProvider._
import hms.soltura.automation.app.AppComponent._;
import hms.soltura.automation.ViewApp.ViewAppComponent._;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */

class ViewAlertAppTest extends CommonUI with TestNGSuite {
   override lazy val ss: Selenium = seleniumRef
    import ss._

    override def seleniumRef: Selenium = selenium

    var app: HashMap[String, String] = null
    var duplicateKeyword = "key002"
    var duplicateAppName = "APP_Alert48582"
    var duplicateAirtel = ""
    var duplicateSafaricom = ""
    var duplicateStartDate = ""
    var duplicateEndDate = ""
    var duplicateUnsubsSuccMass = "This is for check the Unsubscribed Successful"


@BeforeTest
  override def setUp = {
    super.setUp(host, browser)
    setSpeed(speed)
    open(openUrl)
    login
  }

  @AfterTest
  override def tearDown = {
    logout
    super.tearDown()
  }

  @BeforeMethod(groups = Array {"success"})
  def loadApp = {
    app = getApplicationDetails
  }

  @Test(groups = Array {"success"})
    def AlertAppCreating = {
    gotoHome
    gotoCreateApp

    //Step 01
    clickAndWait(alertAppLink)
    assertTrue(isTextPresent(rbundle.getString("alert.description")))

    //application Name
    `type`(appNameText, app(APP_NAME_ALERT))
    duplicateAppName = app(APP_NAME_ALERT)

    //--- Creating new Airtel key word----
        clickAndWait(keywordLink1)
        assertTrue(isTextPresent(rbundle.getString("create.new.keyword")))
        select(keyCodeCombo, label + app(SHORT_CODE1))
        `type`(keywordText, app(KEYWORD))
        duplicateKeyword = app(KEYWORD)
        clickAndWait(nextButton)
        clickAndWait(agreeButton)
        assertTrue(isTextPresent(rbundle.getString("application.details")))

    //Check for Expirable app check Box
    click(ExpirableCheckBox)
    waitForCondition("selenium.browserbot.getCurrentWindow().jQuery.active==0","1000")
    assertFalse(isVisible("endDateDiv"))

    click(ExpirableCheckBox)
    waitForCondition("selenium.browserbot.getCurrentWindow().jQuery.active==0","1000")
    assertTrue(isVisible("endDateDiv"))

    //type a description
    `type`(descText, "Creating successful basic voting app")

    //click Next
    clickAndWait(nextButton)
    assertTrue(isTextPresent(rbundle.getString("terms.and.condition")))
    clickAndWait(agreeButton)
    assertTrue(isTextPresent(rbundle.getString("is.this.information.correct")))
    clickAndWait(nextButton)
    clickAndWait(myApplicationLink)
    assertTrue(isTextPresent(rbundle.getString("my.applications")))

///////////////////////////////////////////////////////////////////
//////////////////////////// VIEW APP /////////////////////////////
///////////////////////////////////////////////////////////////////

       //Searching for App
      `type`(searchbox, app(APP_NAME_ALERT))
      clickAndWait(searchbutton)

      clickAndWait("link=View")
      assertTrue(isTextPresent(rbundle.getString("view.alert.application.title")))

      click(settingsMenu)
      waitForCondition("selenium.browserbot.getCurrentWindow().jQuery.active==0","1000")
       assertTrue(isTextPresent(duplicateAppName))
       assertTrue(isTextPresent(duplicateStartDate))

      click(usageMenu)
      waitForCondition("selenium.browserbot.getCurrentWindow().jQuery.active==0","1000")
      assertTrue(isTextPresent(rbundle.getString("view.registration.summary")))
    }
}