package hms.soltura.automation.common.ViewApp

import hms.soltura.automation.CommonUI
import hms.soltura.automation.CommonUiParams._
import org.scalatest.testng.TestNGSuite
import collection.mutable.HashMap
import org.testng.annotations._
import com.thoughtworks.selenium._
import org.testng.Assert._
import org.testng.annotations._
import org.scalatest.testng.TestNGSuite
import collection.mutable.HashMap
import java.util.Calendar
import java.text.SimpleDateFormat
import hms.soltura.automation.DataProvider._
import hms.soltura.automation.app.AppComponent._;
import hms.soltura.automation.ViewApp.ViewAppComponent._;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */

class ViewChannelAppTest extends CommonUI with TestNGSuite {
  override lazy val ss: Selenium = seleniumRef
  import ss._


  override def seleniumRef: Selenium = selenium

  var app: HashMap[String, String] = null
  var duplicateAppName = ""
  var duplicateKeyword = ""
  val formatString = "dd/MM/yyyy"
  val cal = Calendar.getInstance
  val UNITTEST_BUNDLE_NAME = "messages"
  var duplicateStartDate = ""
  var duplicateAirtel = ""
  var duplicateSafaricom = ""
  var duplicateUnsubsSuccMass = "This is for check the Unsubscribed Successful"

  @BeforeTest
  override def setUp = {
    super.setUp(host, browser)
    setSpeed(speed)
    open(openUrl)
    login
  }

  @AfterTest
  override def tearDown = {
    logout
    super.tearDown()
  }

  @BeforeMethod(groups = Array {"success"})
  def loadApp = {
    app = getApplicationDetails
  }


  @Test(groups = Array {"success"})
  def createBasicChannelAppWithoutSubCategory = {

    gotoHome
    gotoCreateApp

    //Step 01
    clickAndWait(chaAppLink)
    assertTrue(isTextPresent(rbundle.getString("subscription.description")))

    //application Name
    `type`(appNameText, app(APP_NAME_CHA))
    duplicateAppName = app(APP_NAME_CHA)

        //--- Creating new Airtel key word----
        clickAndWait(keywordLink1)
        assertTrue(isTextPresent(rbundle.getString("create.new.keyword")))
        select(keyCodeCombo, label + app(SHORT_CODE1))
        `type`(keywordText, app(KEYWORD))
        duplicateKeyword = app(KEYWORD)
        clickAndWait(nextButton)
        clickAndWait(agreeButton)
        assertTrue(isTextPresent(rbundle.getString("application.details")))

    //Check for Expirable app check Box
    click(ExpirableCheckBox)
    waitForCondition("selenium.browserbot.getCurrentWindow().jQuery.active==0","1000")
    assertFalse(isVisible("endDateDiv"))

    click(ExpirableCheckBox)
    waitForCondition("selenium.browserbot.getCurrentWindow().jQuery.active==0","1000")
    assertTrue(isVisible("endDateDiv"))

    //type a description
    `type`(descText, "Creating successful basic channel app")

    //Message Scheduled Duration
    select(msgDuration,label + "HOUR")

    //Preferred Dispatch Time
    select(dispatchTime,label + "35")

    //add text to Unsubscribed Successfully massage
    `type` (unsubscriberSuccessfulMassage,duplicateUnsubsSuccMass)

    //click Next
    clickAndWait(nextButton)
    assertTrue(isTextPresent(rbundle.getString("terms.and.condition")))
    clickAndWait(agreeButton)
    assertTrue(isTextPresent(rbundle.getString("is.this.information.correct")))
    clickAndWait(nextButton)
    clickAndWait(myApplicationLink)
    assertTrue(isTextPresent(rbundle.getString("my.applications")))

///////////////////////////////////////////////////////////////////
//////////////////////////// VIEW APP /////////////////////////////
///////////////////////////////////////////////////////////////////


    //Searching for App
      `type`(searchbox, app(APP_NAME_CHA))
       clickAndWait(searchbutton)

      clickAndWait("link=View")
      assertTrue(isTextPresent(rbundle.getString("view.subscription.application.title")))

      click(settingsMenu)
      waitForCondition("selenium.browserbot.getCurrentWindow().jQuery.active==0","1000")
      assertTrue(isTextPresent(duplicateAppName))
      assertTrue(isTextPresent(duplicateStartDate))

      click(usageMenu)
      waitForCondition("selenium.browserbot.getCurrentWindow().jQuery.active==0","1000")
      assertTrue(isTextPresent(rbundle.getString("view.registration.summary")))

   }
}



