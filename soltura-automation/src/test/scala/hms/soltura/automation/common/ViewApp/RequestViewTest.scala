package hms.soltura.automation.common.ViewApp

import hms.soltura.automation.CommonUI
import hms.soltura.automation.CommonUiParams._
import org.scalatest.testng.TestNGSuite
import collection.mutable.HashMap
import org.testng.annotations._
import com.thoughtworks.selenium._
import org.testng.Assert._
import org.testng.annotations._
import org.scalatest.testng.TestNGSuite
import collection.mutable.HashMap
import java.util.Calendar
import java.text.SimpleDateFormat
import hms.soltura.automation.DataProvider._
import hms.soltura.automation.app.AppComponent._;
import hms.soltura.automation.ViewApp.ViewAppComponent._;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */

class ViewRequestAppTest extends CommonUI with TestNGSuite {
  override lazy val ss: Selenium = seleniumRef
  import ss._

  override def seleniumRef: Selenium = selenium

  var app: HashMap[String, String] = null
  var keywords: HashMap[String, String] = null

  var duplicateKeyword = "key002"
  var duplicateAppName = "APP_Alert48582"

  var duplicateAirtel = ""
  var duplicateSafaricom = ""
  var duplicateYu = ""

  var duplicateStartDate = ""
  var duplicateEndDate = ""
  var duplicateResponseMessage = ""
  var duplicateUnsubsSuccMass = "This is for check the Unsubscribed Successful"

  val reservedKeywords: Array[String] = new Array[String](3)

  @BeforeTest
  override def setUp = {
    super.setUp(host, browser)
    setSpeed(speed)
    open(openUrl)
    login
  }

  @AfterTest
  override def tearDown = {
    logout
    super.tearDown()
  }

  @BeforeMethod(groups = Array {"success"})
  def loadApp = {
    app = getApplicationDetails
  }

  @Test(groups = Array {"success"})
  def viewBasicRequestApp = {
    gotoHome
    gotoCreateApp

    //Clicks the Request App Button
    clickAndWait(reqAppLink)
    assertTrue(isTextPresent(rbundle.getString("request.description")))


    // Enters the App Name text
    `type`(appNameText, app(APP_NAME_REQ))
    duplicateAppName = app(APP_NAME_REQ)

    // *** Selecting Keyword Details.. ***

/*--- Creating new key word----*/
        clickAndWait(keywordLink1)
        assertTrue(isTextPresent(rbundle.getString("create.new.keyword")))
        select(keyCodeCombo, label + app(SHORT_CODE1))
        `type`(keywordText, app(KEYWORD))
        duplicateKeyword = app(KEYWORD)
        clickAndWait(nextButton)
        clickAndWait(agreeButton)
        assertTrue(isTextPresent(rbundle.getString("application.details")))

    //Checks for Expirable App Check Box

    click(ExpirableCheckBox)
    waitForCondition("selenium.browserbot.getCurrentWindow().jQuery.active==0", "1000")
    assertFalse(isVisible("endDateDiv"))

    click(ExpirableCheckBox)
    waitForCondition("selenium.browserbot.getCurrentWindow().jQuery.active==0", "1000")
    assertTrue(isVisible("endDateDiv"))

    //Enter the Description Text
    `type`(descText, "Successfully Created Request App")

    //Enters the Response Message
    click(CommonResponseCheckBox)
    `type`(responseText, "Thank you for your request")
    duplicateResponseMessage = "Thank you for your request"

    //Click NEXT
    clickAndWait(nextButton)
    assertTrue(isTextPresent(rbundle.getString("terms.and.condition")))
    clickAndWait(agreeButton)
    assertTrue(isTextPresent(rbundle.getString("is.this.information.correct")))
    clickAndWait(nextButton)
    clickAndWait(myApplicationLink)
    assertTrue(isTextPresent(rbundle.getString("my.applications")))

    ///////////////////////////////////////////////////////////////////
    //////////////////////////// VIEW APP /////////////////////////////
    ///////////////////////////////////////////////////////////////////

    //Searching for App
    `type`(searchbox, app(APP_NAME_REQ))
    clickAndWait(searchbutton)

    clickAndWait("link=View")
    assertTrue(isTextPresent(rbundle.getString("view.request.application.title")))

    click(settingsMenu)
    waitForCondition("selenium.browserbot.getCurrentWindow().jQuery.active==0", "1000")
    assertTrue(isTextPresent(duplicateAppName))
    assertTrue(isTextPresent(duplicateStartDate))
    assertTrue(isTextPresent(duplicateEndDate))
    assertTrue(isTextPresent("Thank you for your request"))

    click(usageMenu)
    waitForCondition("selenium.browserbot.getCurrentWindow().jQuery.active==0", "1000")

  }
}



