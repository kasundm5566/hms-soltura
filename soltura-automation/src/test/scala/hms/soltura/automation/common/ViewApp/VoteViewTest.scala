package hms.soltura.automation.common.ViewApp;

import hms.soltura.automation.CommonUI
import hms.soltura.automation.CommonUiParams._
import org.scalatest.testng.TestNGSuite
import collection.mutable.HashMap
import org.testng.annotations._
import com.thoughtworks.selenium._
import org.testng.Assert._
import org.testng.annotations._
import org.scalatest.testng.TestNGSuite
import collection.mutable.HashMap
import java.util.Calendar
import java.text.SimpleDateFormat
import hms.soltura.automation.DataProvider._
import hms.soltura.automation.app.AppComponent._;
import hms.soltura.automation.ViewApp.ViewAppComponent._;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */

class ViewVoteAppTest extends CommonUI with TestNGSuite {

  override lazy val ss: Selenium = seleniumRef
  import ss._

  override def seleniumRef: Selenium = selenium

  var app: HashMap[String, String] = null
  var keywords: HashMap[String, String] = null

  var duplicateKeyword   = ""
  var duplicateAppName   = ""
  var duplicateAirtel    = ""
  var duplicateSafaricom = ""
  var duplicateStartDate = ""
  var duplicateEndDate = ""

  val reservedKeywords: Array[String] = new Array[String](3)

  @BeforeTest
  override def setUp = {
    super.setUp(host, browser)
    reservedKeywords.update(0, "reg")
    setSpeed(speed)
    open(openUrl)
    login
  }

  @AfterTest
  override def tearDown = {
    logout
    super.tearDown()
  }

  @BeforeMethod(groups = Array {"success"})
  def loadApp = {
    app = getApplicationDetails
    keywords = getKeywords
  }

  @Test(groups = Array {"success"})
  def viewBasicVotingApp = {
    gotoHome
    gotoCreateApp

    //Goto Create a Voting App link
    clickAndWait(voteAppLink)
    assertTrue(isTextPresent(rbundle.getString("voting.description")))

    //Type the app name
    `type`(appNameText, app(APP_NAME_VOTE))
    duplicateAppName = app(APP_NAME_VOTE)

    /*--- Creating new key word----*/
    clickAndWait(keywordLink1)
    assertTrue(isTextPresent(rbundle.getString("create.new.keyword")))
    select(keyCodeCombo, label + app(SHORT_CODE1))
    `type`(keywordText, app(KEYWORD))
    duplicateKeyword = app(KEYWORD)
    clickAndWait(nextButton)
    clickAndWait(agreeButton)
    assertTrue(isTextPresent(rbundle.getString("application.details")))

    //Check for Expirable app check Box
    click(ExpirableCheckBox)
    waitForCondition("selenium.browserbot.getCurrentWindow().jQuery.active==0", "1000")
    assertFalse(isVisible("endDateDiv"))

    click(ExpirableCheckBox)
    waitForCondition("selenium.browserbot.getCurrentWindow().jQuery.active==0", "1000")
    assertTrue(isVisible("endDateDiv"))

    //type a description
    `type`(descText, "Creating successful basic voting app")

    //Click NEXT
    clickAndWait(nextButton)
    assertTrue(isTextPresent(rbundle.getString("voting.options")))

    //Voting options
    `type`("input_0", app(SUB_CODE1))
    `type`("text_0", app(SUB_DESC1))

    if (app(ONE_VOTE).equalsIgnoreCase("yes")) {
      if (!isChecked(oneVoteBox)) {
        click(oneVoteBox)
      }
    }

    if (app(RESPONSE).equalsIgnoreCase("yes")) {
      select(responseCombo, responseYesLabel)
    }

    //click Next
    clickAndWait(nextButton)
    assertTrue(isTextPresent(rbundle.getString("terms.and.condition")))
    clickAndWait(agreeButton)
    assertTrue(isTextPresent(rbundle.getString("is.this.information.correct")))
    clickAndWait(nextButton)
    clickAndWait(myApplicationLink)
    assertTrue(isTextPresent(rbundle.getString("my.applications")))

///////////////////////////////////////////////////////////////////
//////////////////////////// VIEW APP /////////////////////////////
///////////////////////////////////////////////////////////////////

    //Searching for App
    `type`(searchbox, app(APP_NAME_VOTE))
    clickAndWait(searchbutton)
    clickAndWait("link=View")
    assertTrue(isTextPresent(rbundle.getString("view.vote.application.title")))

    click(chartsMenu)
    waitForCondition("selenium.browserbot.getCurrentWindow().jQuery.active==0","1000")

    click(settingsMenu)
    waitForCondition("selenium.browserbot.getCurrentWindow().jQuery.active==0","1000")
    assertTrue(isTextPresent(rbundle.getString("application.name")))

    assertTrue(isTextPresent(duplicateAppName))
    assertTrue(isTextPresent(duplicateStartDate))
    assertTrue(isTextPresent(duplicateEndDate))

    click(usageMenu)
    waitForCondition("selenium.browserbot.getCurrentWindow().jQuery.active==0","1000")
    assertTrue(isTextPresent(rbundle.getString("view.voting.summary")))

    }
}

