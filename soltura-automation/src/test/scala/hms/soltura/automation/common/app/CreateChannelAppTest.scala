/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.soltura.automation.common.app

import hms.soltura.automation.CommonUI
import hms.soltura.automation.CommonUiParams._
import org.scalatest.testng.TestNGSuite
import collection.mutable.HashMap
import org.testng.annotations._
import com.thoughtworks.selenium._
import org.testng.Assert._
import org.testng.annotations._
import org.scalatest.testng.TestNGSuite
import collection.mutable.HashMap
import java.util.Calendar
import java.text.SimpleDateFormat
import hms.soltura.automation.DataProvider._
import hms.soltura.automation.app.AppComponent._;

class CreateChannelAppTest extends CommonUI with TestNGSuite {
  override lazy val ss: Selenium = seleniumRef
  import ss._


  override def seleniumRef: Selenium = selenium

  var app: HashMap[String, String] = null
  var duplicateAppName = ""
  var duplicateKeyword = ""
  val formatString = "dd/MM/yyyy"
  val cal = Calendar.getInstance
  val UNITTEST_BUNDLE_NAME = "messages"
  var duplicateStartDate = ""
  var duplicateEndDate = ""
  var duplicateAirtel = ""
  var duplicateSafaricom = ""
  var duplicateUnsubsSuccMass = "Test Unsubscribed Successful"

  @BeforeTest
  override def setUp = {
    super.setUp(host, browser)
    setSpeed(speed)
    open(openUrl)
    login
  }

  @AfterTest
  override def tearDown = {
    logout
    super.tearDown()
  }

  @BeforeMethod(groups = Array {"success"})
  def loadApp = {
    app = getApplicationDetails
  }


  @Test(groups = Array {"success"})
  def createBasicChannelAppWithoutSubCategory = {

    gotoHome
    gotoCreateApp

    //Step 01
    clickAndWait(chaAppLink)
    assertTrue(isTextPresent(rbundle.getString("subscription.description")))

    //application Name
    `type`(appNameText, app(APP_NAME_CHA))
    duplicateAppName = app(APP_NAME_CHA)

    //--- Creating new Airtel key word----
        clickAndWait(keywordLink1)
        assertTrue(isTextPresent(rbundle.getString("create.new.keyword")))
        select(keyCodeCombo, label + app(SHORT_CODE1))
        `type`(keywordText, app(KEYWORD))
        duplicateKeyword = app(KEYWORD)
        clickAndWait(nextButton)
        clickAndWait(agreeButton)
        assertTrue(isTextPresent(rbundle.getString("application.details")))


    //--- Creating new Safaricom key word----
       clickAndWait(keywordLink2)
       assertTrue(isTextPresent(rbundle.getString("create.new.keyword")))
       select(keyCodeCombo, label + app(SHORT_CODE2))
       `type`(keywordText, app(KEYWORD))
       duplicateKeyword = app(KEYWORD)
       clickAndWait(nextButton)
       clickAndWait(agreeButton)
       assertTrue(isTextPresent(rbundle.getString("application.details")))

    //Check for Expirable app check Box
    click(ExpirableCheckBox)
    waitForCondition("selenium.browserbot.getCurrentWindow().jQuery.active==0","1000")
    assertFalse(isVisible("endDateDiv"))

    click(ExpirableCheckBox)
    waitForCondition("selenium.browserbot.getCurrentWindow().jQuery.active==0","1000")
    assertTrue(isVisible("endDateDiv"))

    click(startDate)
    assertTrue(isVisible(calander))
    click(calanderLeft)
    click(calanderRight)
    select(monthDrpdwn, label + "August")
    select(yearDrpdwn, label + "2013")
    click(date)
    duplicateStartDate = "20/08/2013"

    //end date calendar
    click(endDate)
    assertTrue(isVisible(calander))
    click(calanderLeft)
    click(calanderRight)
    select(monthDrpdwn, label + "November")
    select(yearDrpdwn, label + "2013")
    click(eDate)
    duplicateEndDate = "20/11/2013"

    //type a description
    `type`(descText, "Creating successful basic channel app")

    //Message Scheduled Duration
    select(msgDuration,label + "HOUR")

    //Preferred Dispatch Time
    select(dispatchTime,label + "35")

    //Advanced settings
    click(advancedSettings)
    waitForCondition("selenium.browserbot.getCurrentWindow().jQuery.active==0","1000")
    assertTrue(isElementPresent(chargingConfiguration))
    assertTrue(isElementPresent(respondCofiguration))
    assertTrue(isElementPresent(ExpandAll))
    assertTrue(isElementPresent(CollapseAll))

    //Expand all and Collapse all click
    click(ExpandAll)
    waitForCondition("selenium.browserbot.getCurrentWindow().jQuery.active==0","1000")
    assertTrue(isVisible(label_unsubscriberSuccessfulMassage))

    click(CollapseAll)
    waitForCondition("selenium.browserbot.getCurrentWindow().jQuery.active==0","1000")
    assertFalse(isVisible(label_unsubscriberSuccessfulMassage))

    //Response Configuration
    click(respondCofiguration)
    waitForCondition("selenium.browserbot.getCurrentWindow().jQuery.active==0","1000")
    assertTrue(isVisible(label_unsubscriberSuccessfulMassage))

    click(respondCofiguration)
    waitForCondition("selenium.browserbot.getCurrentWindow().jQuery.active==0","1000")
    assertFalse(isVisible(label_unsubscriberSuccessfulMassage))

    click(respondCofiguration)
    waitForCondition("selenium.browserbot.getCurrentWindow().jQuery.active==0","1000")

    //add text to Unsubscribed Successfully massage
    `type` (unsubscriberSuccessfulMassage,duplicateUnsubsSuccMass)

    //charging configuration
    click(chargingConfiguration)
    waitForCondition("selenium.browserbot.getCurrentWindow().jQuery.active==0","1000")
    assertFalse(isVisible("cpsDiv"))
    assertFalse(isVisible(charginForMassage_Chbox_div))
    assertTrue(isChecked(checkBox_charginForSubscription))

    click(chargingConfiguration)
    waitForCondition("selenium.browserbot.getCurrentWindow().jQuery.active==0","1000")
    assertFalse(isTextPresent(rbundle.getString("adv.setting.charging.from.cp")))

    click(chargingConfiguration)
    waitForCondition("selenium.browserbot.getCurrentWindow().jQuery.active==0","1000")

    click(RB_FromContentProvider)
    waitForCondition("selenium.browserbot.getCurrentWindow().jQuery.active==0","1000")
    assertFalse(isVisible("radiobut_1_Selected"))
    assertTrue(isVisible("cpsDiv"))

    click(RB_FromSubscriber)
    waitForCondition("selenium.browserbot.getCurrentWindow().jQuery.active==0","1000")
    assertTrue(isVisible("radiobut_1_Selected"))
    assertFalse(isVisible("cpsDiv"))

    //set check data
    click(checkBox_charginForSubscription)
    waitForCondition("selenium.browserbot.getCurrentWindow().jQuery.active==0","1000")
    assertFalse(isVisible(charginForSubscription_Chbox_div))

    click(checkBox_charginForMassage)
    waitForCondition("selenium.browserbot.getCurrentWindow().jQuery.active==0","1000")
    assertTrue(isVisible(charginForMassage_Chbox_div))
    click(RB_FromOperator)

    //click Next
    clickAndWait(nextButton)
    assertTrue(isTextPresent(rbundle.getString("terms.and.condition")))
    clickAndWait(agreeButton)
    assertTrue(isTextPresent(rbundle.getString("is.this.information.correct")))

    //confirm the basic values
    assertEquals(duplicateAppName , selenium.getValue(appNameText));
    assertEquals(duplicateStartDate , selenium.getValue(startDate));
    assertTrue(isChecked(ExpirableCheckBox))

     //confirm the response Configuration
     click(respondCofiguration)
     assertEquals(duplicateUnsubsSuccMass , selenium.getValue(unsubscriberSuccessfulMassage));

    //confirm the Advanced Configuration values
    click(chargingConfiguration)
    assertTrue(isChecked(RB_FromSubscriber))
    assertFalse(isChecked(RB_FromContentProvider))

    assertFalse(isVisible("cpsDiv"))
    assertTrue(isChecked(RB_FromOperator))

    //Step 04
    clickAndWait(nextButton)
    assertTrue(isTextPresent(rbundle.getString("application.created.succesfuly")))

  }

//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////  /* Additional Validation Tests *//////////////////////////
//////////////////////////////////////////////////////////////////////////////////////


    @Test(dependsOnMethods = Array {"createBasicChannelAppWithoutSubCategory"})
    def testChannelAppNameValidation = {
    gotoHome
    gotoCreateApp

    //Step 01
      clickAndWait(chaAppLink)
      assertTrue(isTextPresent(rbundle.getString("subscription.description")))

    //Application name validation
    `type`(appNameText, "")
    clickAndWait(nextButton)
    assertTrue(isTextPresent(rbundle.getString("app.name.required")))

    `type`(appNameText, duplicateAppName)
    clickAndWait(nextButton)
    assertTrue(isTextPresent(rbundle.getString("app.name.exists")))

    `type`(appNameText, "app$*&%#")
    clickAndWait(nextButton)
    assertTrue(isTextPresent(rbundle.getString("appname.invalid")))

    `type`(appNameText, "app")
    clickAndWait(nextButton)
    assertTrue(isTextPresent(rbundle.getString("appname.invalid.length")))

    `type`(appNameText, "abcdefghijklmnopqrstuvwxyz")
    clickAndWait(nextButton)
    assertTrue(isTextPresent(rbundle.getString("appname.invalid.length")))

  }

    @Test(dependsOnMethods = Array {"testChannelAppNameValidation"})
    def testChannelKeywordValidation = {
    gotoHome
    gotoCreateApp

    clickAndWait(voteAppLink)
    assertTrue(isTextPresent(rbundle.getString("voting.description")))

    `type`(appNameText, app(APP_NAME_CHA) + "v")

    clickAndWait(keywordLink2)
    assertTrue(isTextPresent(rbundle.getString("create.new.keyword")))

    // Keyword validation
    `type`(keywordText, "")
    clickAndWait(nextButton)
    assertTrue(isTextPresent(rbundle.getString("keyword.required")))

/*    `type`(keywordText, duplicateKeyword)
    clickAndWait(nextButton)
    assertTrue(isTextPresent(rbundle.getString("keyword.already.exists")))*/

    `type`(keywordText, "key 60877")
    clickAndWait(nextButton)
    assertTrue(isTextPresent(rbundle.getString("invalid.keyword")))

    `type`(keywordText, "k")
    clickAndWait(nextButton)
    assertTrue(isTextPresent(rbundle.getString("invalid.keyword")))

    `type`(keywordText, "abcdefghijklmnopqrst")
    clickAndWait(nextButton)
    assertTrue(isTextPresent(rbundle.getString("invalid.keyword")))

    `type`(keywordText, "key$*&%#")
    clickAndWait(nextButton)
    assertTrue(isTextPresent(rbundle.getString("invalid.keyword")))

    `type`(keywordText, app(KEYWORD) + "v")
    clickAndWait(nextButton)
    clickAndWait(agreeButton)
    assertTrue(isTextPresent(rbundle.getString("application.details")))
  }

  // Checking and Comparing Start and End Dates..
 @Test(dependsOnMethods = Array {"testChannelKeywordValidation"})
    def testChannelStartEndDateValidation = {

     gotoHome
     gotoCreateApp

     //Step 01
     clickAndWait(chaAppLink)
     assertTrue(isTextPresent(rbundle.getString("subscription.description")))

    //Check the error massage Start date must be greater than the current date
    click(startDate)
    select(monthDrpdwn,label+"June")
    select(yearDrpdwn,label+"2010")
    click(date)

    clickAndWait(nextButton)
    assertTrue(isTextPresent(rbundle.getString("start.date.invalid")))

    //Set start date of the calender
    click(startDate)
    select(monthDrpdwn,label+"June")
    select(yearDrpdwn,label+"2011")
    click(date)

    // Set End-date of the calender
    click(endDate)
    select(monthDrpdwn,label+"March")
    select(yearDrpdwn,label+"2011")
    click(date)

    clickAndWait(nextButton)
    assertTrue(isTextPresent(rbundle.getString("date.invalid")))
 }

}



