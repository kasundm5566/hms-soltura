package hms.soltura.automation.common.app

import hms.soltura.automation.CommonUI
import hms.soltura.automation.CommonUiParams._
import org.scalatest.testng.TestNGSuite
import collection.mutable.HashMap
import org.testng.annotations._
import com.thoughtworks.selenium._
import org.testng.Assert._
import org.testng.annotations._
import org.scalatest.testng.TestNGSuite
import collection.mutable.HashMap
import java.util.Calendar
import java.text.SimpleDateFormat
import hms.soltura.automation.DataProvider._
import hms.soltura.automation.app.AppComponent._;

/**
 * $LastChangedDate:$
 * $LastChangedBy:$
 * $LastChangedRevision:$
 */

class CreateRequestAppTest extends CommonUI with TestNGSuite {
  override lazy val ss: Selenium = seleniumRef
  import ss._

  override def seleniumRef: Selenium = selenium

  var app: HashMap[String, String] = null
  var keywords: HashMap[String, String] = null

  var duplicateKeyword = "key002"
  var duplicateAppName = "APP_Alert48582"

  var duplicateAirtel = ""
  var duplicateSafaricom = ""
  var duplicateYu = ""

  var duplicateStartDate = ""
  var duplicateEndDate = ""
  var duplicateResponseMessage = ""
  var duplicateUnsubsSuccMass = "This is for check the Unsubscribed Successful"

  val reservedKeywords: Array[String] = new Array[String](3)

  @BeforeTest
  override def setUp = {
    super.setUp(host, browser)
    setSpeed(speed)
    open(openUrl)
    login
  }

  @AfterTest
  override def tearDown = {
    logout
    super.tearDown()
  }

  @BeforeMethod(groups = Array {"success"})
  def loadApp = {
    app = getApplicationDetails
  }

  @Test(groups = Array {"success"})
  def createBasicRequestAppWithoutSubCategory = {
    gotoHome
    gotoCreateApp

    //Clicks the Request App Button
    clickAndWait(reqAppLink)
    assertTrue(isTextPresent(rbundle.getString("request.description")))


    // Enters the App Name text
    `type`(appNameText, app(APP_NAME_REQ))
    duplicateAppName = app(APP_NAME_REQ)

    // *** Selecting Keyword Details.. ***



    /*--- Creating new key word----*/
        clickAndWait(keywordLink1)
        assertTrue(isTextPresent(rbundle.getString("create.new.keyword")))
        select(keyCodeCombo, label + app(SHORT_CODE1))
        `type`(keywordText, app(KEYWORD))
        duplicateKeyword = app(KEYWORD)
        clickAndWait(nextButton)
        clickAndWait(agreeButton)
        assertTrue(isTextPresent(rbundle.getString("application.details")))


    /*--- Creating new key word----*/
       clickAndWait(keywordLink2)
       assertTrue(isTextPresent(rbundle.getString("create.new.keyword")))
       select(keyCodeCombo, label + app(SHORT_CODE2))
       `type`(keywordText, app(KEYWORD))
       duplicateKeyword = app(KEYWORD)
       clickAndWait(nextButton)
       clickAndWait(agreeButton)
       assertTrue(isTextPresent(rbundle.getString("application.details")))

    /* --- Application validity Duration --- */

    //Checks for Expirable App Check Box

    click(ExpirableCheckBox)
    waitForCondition("selenium.browserbot.getCurrentWindow().jQuery.active==0", "1000")
    assertFalse(isVisible("endDateDiv"))

    click(ExpirableCheckBox)
    waitForCondition("selenium.browserbot.getCurrentWindow().jQuery.active==0", "1000")
    assertTrue(isVisible("endDateDiv"))

    //Fills and Checks the Calender Value.

    //start date calander
    click(startDate)
    assertTrue(isVisible(calander))
    click(calanderLeft)
    click(calanderRight)
    select(monthDrpdwn, label + "August")
    select(yearDrpdwn, label + "2013")
    click(date)
    duplicateStartDate = "20/08/2013"

    //end date calendar
    click(endDate)
    assertTrue(isVisible(calander))
    click(calanderLeft)
    click(calanderRight)
    select(monthDrpdwn, label + "November")
    select(yearDrpdwn, label + "2013")
    click(eDate)
    duplicateEndDate = "20/11/2013"

    //Enter the Description Text
    `type`(descText, "Successfully Created Request App")

    //Enters the Response Message
    click(CommonResponseCheckBox)
    `type`(responseText, "Thank you for your request")
    duplicateResponseMessage = "Thank you for your request"

    //Advanced settings
    click(advancedSettings)
    waitForCondition("selenium.browserbot.getCurrentWindow().jQuery.active==0", "1000")
    assertTrue(isTextPresent(rbundle.getString("adv.setting.charging.from.cp")))

    click(chargingConfiguration_vote)
    waitForCondition("selenium.browserbot.getCurrentWindow().jQuery.active==0", "1000")
    assertFalse(isTextPresent(rbundle.getString("adv.setting.charging.from.cp")))
    assertFalse(isVisible("cpsDiv"))

    click(chargingConfiguration_vote)
    waitForCondition("selenium.browserbot.getCurrentWindow().jQuery.active==0", "1000")

    click(RB_FromContentProvider)
    waitForCondition("selenium.browserbot.getCurrentWindow().jQuery.active==0", "1000")
    assertTrue(isVisible("cpsDiv"))

    click(RB_FromSubscriber)
    click(RB_FromOperator)

    //Click NEXT
    clickAndWait(nextButton)
    assertTrue(isTextPresent(rbundle.getString("terms.and.condition")))
    clickAndWait(agreeButton)
    assertTrue(isTextPresent(rbundle.getString("is.this.information.correct")))

    //confirm the basic values
    assertEquals(duplicateAppName, selenium.getValue(appNameText));
    assertEquals(duplicateStartDate, selenium.getValue(startDate));
    assertEquals(duplicateEndDate, selenium.getValue(endDate));
    assertTrue(isChecked(ExpirableCheckBox));
    assertTrue(isChecked(CommonResponseCheckBox));
    assertEquals(duplicateResponseMessage, selenium.getValue(responseText));

    //Return to previous page
    clickAndWait(backButton)
    //Uncheck the Checkbox for Common Response Message
    click(CommonResponseCheckBox)

    //Click NEXT
    clickAndWait(nextButton)
    assertTrue(isTextPresent(rbundle.getString("terms.and.condition")))
    clickAndWait(agreeButton)
    assertTrue(isTextPresent(rbundle.getString("is.this.information.correct")))


    //confirm the basic values
    assertEquals(duplicateAppName, selenium.getValue(appNameText));
    assertEquals(duplicateStartDate, selenium.getValue(startDate));
    assertEquals(duplicateEndDate, selenium.getValue(endDate));
    assertTrue(isChecked(ExpirableCheckBox));
    assertFalse(isChecked(CommonResponseCheckBox));
    assertEquals("", selenium.getValue(responseText));

    //confirm the Advanced Configuration values
    click(chargingConfiguration_vote)
    assertTrue(isChecked(RB_FromSubscriber))
    assertFalse(isChecked(RB_FromContentProvider))

    assertFalse(isVisible("radiobut_1_Selected"))
    assertTrue(isChecked(RB_FromOperator))

    //Step 04
    clickAndWait(nextButton)
    assertTrue(isTextPresent(rbundle.getString("application.created.succesfuly")))
    clickAndWait("link=" + rbundle.getString("application.success.description"))
    assertTrue(isTextPresent(rbundle.getString("my.applications")))
  }
  //////////////////////////////////////////////////////////////////////////////////////
  //////////////////////////  /* Additional Validation Tests *//////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////

  @Test(dependsOnMethods = Array {"createBasicRequestAppWithoutSubCategory"})
  def testAppNameValidation = {
    gotoHome
    gotoCreateApp

    //Step 01
    clickAndWait(reqAppLink)
    assertTrue(isTextPresent(rbundle.getString("request.description")))

    //Application name validation
    `type`(appNameText, "")
    clickAndWait(nextButton)
    assertTrue(isTextPresent(rbundle.getString("app.name.required")))

    `type`(appNameText, duplicateAppName)
    clickAndWait(nextButton)
    assertTrue(isTextPresent(rbundle.getString("app.name.exists")))

    `type`(appNameText, "app$*&%#")
    clickAndWait(nextButton)
    assertTrue(isTextPresent(rbundle.getString("appname.invalid")))

    `type`(appNameText, "app")
    clickAndWait(nextButton)
    assertTrue(isTextPresent(rbundle.getString("appname.invalid.length")))

    `type`(appNameText, "abcdefghijklmnopqrstuvwxyz")
    clickAndWait(nextButton)
    assertTrue(isTextPresent(rbundle.getString("appname.invalid.length")))
  }

  @Test(dependsOnMethods = Array {"testAppNameValidation"})
  def testRequestKeywordValidation = {
    gotoHome
    gotoCreateApp

    clickAndWait(reqAppLink)
    assertTrue(isTextPresent(rbundle.getString("request.description")))

    // Enters the App Name text
    `type`(appNameText, app(APP_NAME_REQ) + "r")

    clickAndWait(keywordLink2)
    assertTrue(isTextPresent(rbundle.getString("create.new.keyword")))

    // Keyword validation
    `type`(keywordText, "")
    clickAndWait(nextButton)
    assertTrue(isTextPresent(rbundle.getString("keyword.required")))

/*    `type`(keywordText, duplicateKeyword)
    clickAndWait(nextButton)
    assertTrue(isTextPresent(rbundle.getString("keyword.already.exists")))*/

    `type`(keywordText, "key 60877")
    clickAndWait(nextButton)
    assertTrue(isTextPresent(rbundle.getString("invalid.keyword")))

    `type`(keywordText, "k")
    clickAndWait(nextButton)
    assertTrue(isTextPresent(rbundle.getString("invalid.keyword")))

    `type`(keywordText, "abcdefghijklmnopqrst")
    clickAndWait(nextButton)
    assertTrue(isTextPresent(rbundle.getString("invalid.keyword")))

    `type`(keywordText, "key$*&%#")
    clickAndWait(nextButton)
    assertTrue(isTextPresent(rbundle.getString("invalid.keyword")))

    `type`(keywordText, app(KEYWORD) + "v")
    clickAndWait(nextButton)
    clickAndWait(agreeButton)
    assertTrue(isTextPresent(rbundle.getString("application.details")))
  }

  // Checking and Comparing Start and End Dates..
  @Test(dependsOnMethods = Array {"createBasicRequestAppWithoutSubCategory"})
  def testChannelStartEndDateValidation = {

    gotoHome
    gotoCreateApp

    //Step 01
    clickAndWait(reqAppLink)
    assertTrue(isTextPresent(rbundle.getString("request.description")))

    //application Name
    `type`(appNameText, app(APP_NAME_ALERT))
    duplicateAppName = app(APP_NAME_ALERT)

    //Check the error massage Start date must be greater than the current date
    click(startDate)
    select(monthDrpdwn,label+"June")
    select(yearDrpdwn,label+"2010")
    click(date)

    clickAndWait(nextButton)
    assertTrue(isTextPresent(rbundle.getString("start.date.invalid")))

    //Set start date of the calender

    click(startDate)
    select(monthDrpdwn,label+"June")
    select(yearDrpdwn,label+"2011")
    click(date)

    // Set End-date of the calender

    click(endDate)
    select(monthDrpdwn,label+"March")
    select(yearDrpdwn,label+"2011")
    click(date)

    clickAndWait(nextButton)
    assertTrue(isTextPresent(rbundle.getString("date.invalid")))
  }

  // Checking common response available and not available states
  @Test(dependsOnMethods = Array {"createBasicRequestAppWithoutSubCategory"})
  def testChannelCommonResponseValidation = {
      gotoHome
    gotoCreateApp

    //Step 01
    clickAndWait(reqAppLink)
    assertTrue(isTextPresent(rbundle.getString("request.description")))

    // Enters the App Name text
    `type`(appNameText, app(APP_NAME_REQ) + "r")

    //Enter the Description Text
    `type`(descText, "Successfully Created Request App")

    //Enters the Response Message
    click(CommonResponseCheckBox)
    clickAndWait(nextButton)
    assertTrue(isTextPresent("Please provide a response message"))

  }

}



