package hms.soltura.automation.common.app

import hms.soltura.automation.CommonUI
import hms.soltura.automation.CommonUiParams._
import org.scalatest.testng.TestNGSuite
import collection.mutable.HashMap
import org.testng.annotations._
import com.thoughtworks.selenium._
import org.testng.Assert._
import org.testng.annotations._
import org.scalatest.testng.TestNGSuite
import collection.mutable.HashMap
import java.util.Calendar
import java.text.SimpleDateFormat
import hms.soltura.automation.DataProvider._
import hms.soltura.automation.app.AppComponent._;
import java.util.{Date, Locale}
import java.text.DateFormat
import java.text.DateFormat._


/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */

class CreateVoteAppTest extends CommonUI with TestNGSuite {
  override lazy val ss: Selenium = seleniumRef
  import ss._

  override def seleniumRef: Selenium = selenium

  var app: HashMap[String, String] = null
  var keywords: HashMap[String, String] = null

  val currentDate = getDateInstance();

  var duplicateKeyword = ""
  var duplicateAppName = ""
  var duplicateAirtel = ""
  var duplicateSafaricom = ""
  var duplicateStartDate = ""
  var duplicateEndDate = ""

  val reservedKeywords: Array[String] = new Array[String](3)

  @BeforeTest
  override def setUp = {
    super.setUp(host, browser)
    reservedKeywords.update(0, "reg")
    setSpeed(speed)
    open(openUrl)
    login
  }

  @AfterTest
  override def tearDown = {
    logout
    super.tearDown()
  }

  @BeforeMethod(groups = Array {"success"})
  def loadApp = {
    app = getApplicationDetails
    keywords = getKeywords
  }

  @Test(groups = Array {"success"})
  def createBasicVotingApp = {
    gotoHome
    gotoCreateApp

    //Goto Create a Voting App link
    clickAndWait(voteAppLink)
    assertTrue(isTextPresent(rbundle.getString("voting.description")))

    //Type the app name
    `type`(appNameText, app(APP_NAME_VOTE))
    duplicateAppName = app(APP_NAME_VOTE)

    /*--- Creating new key word----*/     
    clickAndWait(keywordLink1)
    assertTrue(isTextPresent(rbundle.getString("create.new.keyword")))
    select(keyCodeCombo, label + app(SHORT_CODE1))
    `type`(keywordText, app(KEYWORD))
    duplicateKeyword = app(KEYWORD)
    clickAndWait(nextButton)
    clickAndWait(agreeButton)
    assertTrue(isTextPresent(rbundle.getString("application.details")))


    /*--- Creating new key word----*/
     clickAndWait(keywordLink2)
     assertTrue(isTextPresent(rbundle.getString("create.new.keyword")))
     select(keyCodeCombo, label + app(SHORT_CODE2))
     `type`(keywordText, app(KEYWORD))
     duplicateKeyword = app(KEYWORD)
     clickAndWait(nextButton)
     clickAndWait(agreeButton)
     assertTrue(isTextPresent(rbundle.getString("application.details")))

    //Check for Expirable app check Box
    click(ExpirableCheckBox)
    waitForCondition("selenium.browserbot.getCurrentWindow().jQuery.active==0", "1000")
    assertFalse(isVisible("endDateDiv"))

    click(ExpirableCheckBox)
    waitForCondition("selenium.browserbot.getCurrentWindow().jQuery.active==0", "1000")
    assertTrue(isVisible("endDateDiv"))

    //start date calander
    click(startDate)
    assertTrue(isVisible(calander))
    click(calanderLeft)
    click(calanderRight)
    select(monthDrpdwn, label + "August")
    select(yearDrpdwn, label + "2013")
    click(date)
    duplicateStartDate = "20/08/2013"

    //end date calendar
    click(endDate)
    assertTrue(isVisible(calander))
    click(calanderLeft)
    click(calanderRight)
    select(monthDrpdwn, label + "November")
    select(yearDrpdwn, label + "2013")
    click(eDate)
    duplicateEndDate = "20/11/2013"

    //type a description
    `type`(descText, "Creating successful basic voting app")

    //Advanced settings
    click(advancedSettings)
    waitForCondition("selenium.browserbot.getCurrentWindow().jQuery.active==0", "1000")
    assertTrue(isTextPresent(rbundle.getString("adv.setting.charging.from.cp")))

    click(chargingConfiguration_vote)
    waitForCondition("selenium.browserbot.getCurrentWindow().jQuery.active==0", "1000")
    assertFalse(isTextPresent(rbundle.getString("adv.setting.charging.from.cp")))
    assertFalse(isVisible("cpsDiv"))

    click(chargingConfiguration_vote)
    waitForCondition("selenium.browserbot.getCurrentWindow().jQuery.active==0", "1000")

    click(RB_FromContentProvider)
    waitForCondition("selenium.browserbot.getCurrentWindow().jQuery.active==0", "1000")
    assertTrue(isVisible("cpsDiv"))

    click(RB_FromSubscriber)
    click(RB_FromOperator)

    //Click NEXT
    clickAndWait(nextButton)
    assertTrue(isTextPresent(rbundle.getString("voting.options")))

    //Voting options
    `type`("input_0", app(SUB_CODE1))
    `type`("text_0", app(SUB_DESC1))

    //create three voting options
    var i: Int = 1
    var did: Boolean = true

    var keyNameCode = "SUB_CODE" + (i + 1)
    var keyNameDesc = "SUB_DESC" + (i + 1)
    while (app.contains(keyNameCode)) {
      click(plusSign)
      `type`("input_" + i, app(keyNameCode))
      `type`("text_" + i, app(keyNameDesc))

      i += 1
      keyNameCode = "SUB_CODE" + (i + 1)
      keyNameDesc = "SUB_DESC" + (i + 1)

      if (i == 3 && did) {
        //delete the last voting option
        click(deleteSign + "2")
        assertFalse(isVisible(InputField + "2"))
        i -= 1
        did = false
        keyNameCode = "SUB_CODE" + (i + 1)
        keyNameDesc = "SUB_DESC" + (i + 1)
      }
    }

    if (app(ONE_VOTE).equalsIgnoreCase("yes")) {
      if (!isChecked(oneVoteBox)) {
        click(oneVoteBox)
      }
    }

    if (app(RESPONSE).equalsIgnoreCase("yes")) {
      select(responseCombo, responseYesLabel)
    }

    clickAndWait(nextButton)
        assertTrue(isTextPresent(rbundle.getString("terms.and.condition")))
    clickAndWait(agreeButton)
        assertTrue(isTextPresent(rbundle.getString("is.this.information.correct")))


    //Back Button
    clickAndWait(backButton)

    clickAndWait(backButton)

    //Advanced settings
    click(advancedSettings)
    waitForCondition("selenium.browserbot.getCurrentWindow().jQuery.active==0", "1000")
    assertTrue(isTextPresent(rbundle.getString("adv.setting.charging.from.cp")))

    click(chargingConfiguration_vote)
    waitForCondition("selenium.browserbot.getCurrentWindow().jQuery.active==0", "1000")
    assertFalse(isTextPresent(rbundle.getString("adv.setting.charging.from.cp")))
    assertFalse(isVisible("cpsDiv"))

    click(chargingConfiguration_vote)
    waitForCondition("selenium.browserbot.getCurrentWindow().jQuery.active==0", "1000")

    click(chargingConfiguration_vote)
    waitForCondition("selenium.browserbot.getCurrentWindow().jQuery.active==0", "1000")

    click(RB_FromSubscriber)
    waitForCondition("selenium.browserbot.getCurrentWindow().jQuery.active==0", "1000")
    assertFalse(isVisible("cpsDiv"))

    click(RB_FromOperator)

    clickAndWait(nextButton)
    
    clickAndWait(nextButton)
        assertTrue(isTextPresent(rbundle.getString("terms.and.condition")))
    clickAndWait(agreeButton)
        assertTrue(isTextPresent(rbundle.getString("is.this.information.correct")))


    //confirm the basic values
    assertEquals(duplicateAppName, selenium.getValue(appNameText));
    assertEquals(duplicateStartDate, selenium.getValue(startDate));
    assertEquals(duplicateEndDate, selenium.getValue(endDate));
    assertTrue(isChecked(ExpirableCheckBox))

    //confirm the Advanced Configuration values
    click(chargingConfiguration_vote)
    assertTrue(isChecked(RB_FromSubscriber))
    assertFalse(isChecked(RB_FromContentProvider))

    assertFalse(isVisible("radiobut_1_Selected"))
    assertTrue(isChecked(RB_FromOperator))

    //verify the candidate code values
    i = 0
    keyNameCode = "SUB_CODE" + (i + 1)
    keyNameDesc = "SUB_DESC" + (i + 1)
    while (app.contains(keyNameCode)) {
      assertEquals(getValue("input_" + i), app(keyNameCode))
      assertEquals(getValue("text_" + i), app(keyNameDesc))

      i += 1
      keyNameCode = "SUB_CODE" + (i + 1)
      keyNameDesc = "SUB_DESC" + (i + 1)
    }


    //Step 04
        clickAndWait(nextButton)
        assertTrue(isTextPresent(rbundle.getString("application.created.succesfuly")))
        clickAndWait("link="+rbundle.getString("application.success.description"))
        assertTrue(isTextPresent(rbundle.getString("my.applications")))

  }

  //////////////////////////////////////////////////////////////////////////////////////
  //////////////////////////  /* Additional Validation Tests *//////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////


  @Test(dependsOnMethods = Array {"createBasicVotingApp"})
  def testAppNameValidation = {
    gotoHome
    gotoCreateApp

    //Step 01
    clickAndWait(voteAppLink)
        assertTrue(isTextPresent(rbundle.getString("voting.description")))

    //Application name validation
    `type`(appNameText, "")
    clickAndWait(nextButton)
    assertTrue(isTextPresent(rbundle.getString("app.name.required")))

    `type`(appNameText, duplicateAppName)
    clickAndWait(nextButton)
    assertTrue(isTextPresent(rbundle.getString("app.name.exists")))

    `type`(appNameText, "app$*&%#")
    clickAndWait(nextButton)
    assertTrue(isTextPresent(rbundle.getString("appname.invalid")))

    `type`(appNameText, "app")
    clickAndWait(nextButton)
    assertTrue(isTextPresent(rbundle.getString("appname.invalid.length")))

    `type`(appNameText, "abcdefghijklmnopqrstuvwxyz")
    clickAndWait(nextButton)
    assertTrue(isTextPresent(rbundle.getString("appname.invalid.length")))
  }

  @Test(dependsOnMethods = Array {"testAppNameValidation"})
  def testVoteKeywordValidation = {
    gotoHome
    gotoCreateApp

    clickAndWait(voteAppLink)
        assertTrue(isTextPresent(rbundle.getString("voting.description")))

    `type`(appNameText, app(APP_NAME_VOTE) + "v")

    clickAndWait(keywordLink2)
        assertTrue(isTextPresent(rbundle.getString("create.new.keyword")))

    // Keyword validation
    `type`(keywordText, "")
    clickAndWait(nextButton)
    assertTrue(isTextPresent(rbundle.getString("keyword.required")))

/*    `type`(keywordText, duplicateKeyword)
    clickAndWait(nextButton)
    assertTrue(isTextPresent(rbundle.getString("keyword.already.exists")))*/

    `type`(keywordText, "key 60877")
    clickAndWait(nextButton)
    assertTrue(isTextPresent(rbundle.getString("invalid.keyword")))

    `type`(keywordText, "k")
    clickAndWait(nextButton)
    assertTrue(isTextPresent(rbundle.getString("invalid.keyword")))

    `type`(keywordText, "abcdefghijklmnopqrst")
    clickAndWait(nextButton)
    assertTrue(isTextPresent(rbundle.getString("invalid.keyword")))

    `type`(keywordText, "key$*&%#")
    clickAndWait(nextButton)
    assertTrue(isTextPresent(rbundle.getString("invalid.keyword")))

    `type`(keywordText, app(KEYWORD) + "v")
    clickAndWait(nextButton)
    clickAndWait(agreeButton)
    assertTrue(isTextPresent(rbundle.getString("application.details")))
  }


  // Checking and Comparing Start and End Dates..

  @Test(dependsOnMethods = Array {"testVoteKeywordValidation"})
  def testChannelStartEndDateValidation = {

    gotoHome
    gotoCreateApp

    //Step 01
    clickAndWait(voteAppLink)
          assertTrue(isTextPresent(rbundle.getString("voting.description")))

    //application Name
    `type`(appNameText, app(APP_NAME_VOTE))
    duplicateAppName = app(APP_NAME_VOTE)

    //Check the error massage Start date must be greater than the current date
    click(startDate)
    select(monthDrpdwn,label+"June")
    select(yearDrpdwn,label+"2010")
    click(date)

    clickAndWait(nextButton)
    assertTrue(isTextPresent(rbundle.getString("start.date.invalid")))

    //Set start date of the calender

    click(startDate)
    select(monthDrpdwn,label+"June")
    select(yearDrpdwn,label+"2011")
    click(date)

    // Set End-date of the calender

    click(endDate)
    select(monthDrpdwn,label+"March")
    select(yearDrpdwn,label+"2011")
    click(date)

    clickAndWait(nextButton)
    assertTrue(isTextPresent(rbundle.getString("date.invalid")))
  }
}