package hms.soltura.automation.common.home

import hms.soltura.automation.CommonUI
import hms.soltura.automation.DataProvider._
import java.util.{ResourceBundle, Locale}

import hms.soltura.automation.home.HomeComponent._;
import org.scalatest.testng.TestNGSuite
import org.testng.annotations.{Test, AfterTest, BeforeTest}
import com.thoughtworks.selenium.Selenium
import hms.soltura.automation.CommonUiParams._;
import org.testng.Assert._

/**
 * $LastChangedDate:$
 * $LastChangedBy:$
 * $LastChangedRevision:$
 */

class HomePageTest extends CommonUI  with TestNGSuite {

  override lazy val ss: Selenium = seleniumRef
  import ss._

  override def seleniumRef: Selenium = selenium


  @BeforeTest
  override def setUp = {
    super.setUp(host, browser)
    setSpeed(speed)
    open(openUrl)
  }
  @AfterTest
  override def tearDown = {
    super.tearDown()
  }

  @Test
  def testFAQ={
//    click(faqLinkLocation)
  }

  @Test
  def testLocale={
//    clickAndWait("link=Swahili")
//    if(locale.equals("en")){
//      val locBasha: Locale = new Locale("in")
//      val rbundleBasha: ResourceBundle = ResourceBundle.getBundle("messages",locBasha)
//      assertTrue(isTextPresent(rbundleBasha.getString("username")))
//    }else{
//
//      assertTrue(isTextPresent(rbundle.getString("username")))
//    }
//
//    clickAndWait("link=English")
//    if(locale.equals("in")){
//      val locEnglish: Locale = new Locale("en")
//      val rbundleEnglish: ResourceBundle = ResourceBundle.getBundle("messages",locEnglish)
//      //assertTrue(isTextPresent("New to Gapura SMS?"))
//      assertTrue(isTextPresent(rbundleEnglish.getString("username")))
//    }else{
//      assertTrue(isTextPresent(rbundle.getString("username")))
//    }
  }

  @Test(dependsOnMethods =Array ("testLocale","testFAQ"))
  def testCreateApp={
    login
    gotoCreateApp
    gotoHome
  }

  @Test(dependsOnMethods =Array {"testCreateApp"})
  def testMyApp={
    gotoMyApp
    gotoHome
  }

  @Test(dependsOnMethods =Array {"testMyApp"})
  def testMySettings={
    gotoSetting
    gotoHome
  }

  @Test(dependsOnMethods =Array {"testMySettings"})
  def testReports={
    gotoReport
    gotoHome
  }
}