package hms.soltura.automation.common.login

import hms.soltura.automation.CommonUI
import hms.soltura.automation.DataProvider._;
import hms.soltura.automation.CommonUiParams._;
import hms.soltura.automation.login.LoginComponent._;
import org.scalatest.testng.TestNGSuite
import com.thoughtworks.selenium.Selenium
import org.testng.annotations._
import org.testng.Assert._
/**
 * $LastChangedDate:$
 * $LastChangedBy:$
 * $LastChangedRevision:$
 */
class LoginTest  extends CommonUI with TestNGSuite {

  override lazy  val ss: Selenium = seleniumRef
  import ss._

  override def seleniumRef: Selenium = selenium


  @BeforeTest
  override def setUp = {
    super.setUp(host, browser)
    setSpeed(speed)
    open(openUrl)
  }
  @AfterTest
  override def tearDown = {
    logout
    super.tearDown()
  }

  @Test
  def testEmptyData= {
    `type`(usernameText, "")
    `type`(pwdText, "")
    clickAndWait(loginButton)
    assertTrue(isTextPresent("Username is a required field.\nPassword is a required field."))
  }

  @Test(dependsOnMethods =Array {"testEmptyData"})
  def testWithoutPassowrd= {
    `type`(usernameText, "user1234")
    `type`(pwdText, "")
    clickAndWait(loginButton)
    assertTrue(isTextPresent("Password is a required field"))
    assertTrue(isTextPresent(rbundle.getString("lost.your.password")))
  }

  @Test(dependsOnMethods =Array {"testWithoutPassowrd"})
  def testWithoutUserName= {
    `type`(usernameText, "")
    `type`(pwdText, pwd)
    clickAndWait(loginButton)
    assertTrue(isTextPresent("Username is a required field."))
  }

//  @Test(dependsOnMethods =Array {"testWithoutPassowrd"})
//  def testRefreshAfter2times= {
//    `type`(usernameText, "shamilacorp")
//    `type`(pwdText, "shamila12!@")
//    clickAndWait(loginButton)
//    assertTrue(isTextPresent("Login"))
//  }

  @Test(dependsOnMethods =Array {"testWithoutUserName"})
  def testCorrectPhonPass = {
    `type`(usernameText, "shamilacorp")
    `type`(pwdText, "shamila12!@")
    clickAndWait(loginButton)
    assertTrue(isTextPresent(rbundle.getString("logged.in.as")))
  }
}