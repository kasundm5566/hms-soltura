
ORCA : CP-PORTAL(Soltura Web) - README
========================================================

**Before reading this you should read the top level README file**

## Build Structure

Building soltura web for different clients is done using profiles.

### Folder Structure

* src/main/resource - Base product/common property files.
* src/main/profile - Profile specific property files.

When you put files inside profiles it should have the structure similar to the base product files.

*Eg:* If you want to have separate banner image, we have to create the new banner and place it inside **profile/new-profile/webapp/images/theme-vm/soltura-top.jpg**

**IMPORTANT**: If you add/remove property files in src/main/resource or in src/webapp which are customized in profiles, you have to make sure that those changes are correctly propegated to each of them.

## Build and Deployment

* To create war file use command 'mvn clean package' and copy that war file to CATALINA_HOME/web-apps/

* Start the tomcat and type the url http:<releventIP>:<port>/soltura (Ex: http://192.168.0.61:8080/soltura)


## How to Build Reporting module in Soltura?

Currently soltura is supporting two types of reports namely

  1. Message History Report
  2. Revenue Summary Report

Database tables relavent to Message History Report will reside in Orca-DB(`integration`) and all the other tables which are relevant to
revenue summary report will be in the separate Reporting DB. Since soltura is reusing the revenue report generation mechanism
in mChoice SDP, all the required modules needed to setup the SDP report view will also be needed for testing reports in soltura.

**Note:** Inorder to test the revenue summary report you have to have the running mChoice SDP reportviewer including CDR Agent,
Data Transfer and Data Loading modules. Please refer the READMEs available on those module in the mChoice SDP project
(MCHOICE_SDP_PROJECT_HOME/reporting/...) to get them installed and run.

Once the message flow for both SDP and Orca is available you can get the generated trans log to generate the soltura
revenue reports with the use of SDP Reporting module.

Message History report will show all the selected events occurred in the soltura side and reports can be genertaed for selected type
 of events (Please refer the PRD for the supported event types which will generate the message history reports.)


## Configuration changes
Following configurations need to be changed when you deploy soltura

 1. Allowed host list
    Update app.properties 'allowed.host.address', include all IP address required to be allowed here.

 2. Other web module urls
 	URLs for provisioning, reporting and app store can be found in messages_en.properties. Update them with correct URLs.
 	   url.provisioning
 	   url.appstore
 	   url.reporting
 	   url.home



## How to test Soltura with Governance module?

Please refer the README available in the *MCHOICE_SDP_PROJECT_HOME/modules/governance* to build the governance message flow and
to get the installation steps to deploy governanace UI refer README available in the MCHOICE_SDP_PROJECT_HOME/modules/governance-ui/

## Working With MsSql Server Database

1. Build Orm-Module with support to MsSqlServer. Refer /orm-mapping/readme.txt
2. In cp-portal.properties Comment MySql related settings and uncomment MsSqlServer related setting.
3. Build the project with -Pmssql flag
	e.g: mvn clean jetty:run -Pmssql
		 mvn clean package -Pmssql

