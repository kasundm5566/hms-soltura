/*
*   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
*   All Rights Reserved.
*
*   These materials are unpublished, proprietary, confidential source code of
*   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
*   of hSenid Software International (Pvt) Limited.
*
*   hSenid Software International (Pvt) Limited retains all title to and intellectual
*   property rights in these materials.
*
*   $LastChangedDate$
*   $LastChangedBy$
*   $LastChangedRevision$
*/
package hsenidmobile.orca.cpportal.controller;

import hms.common.registration.api.response.BasicUserResponseMessage;
import hsenidmobile.orca.core.applications.PeriodUnit;
import hsenidmobile.orca.cpportal.module.ApplicationInfo;
import hsenidmobile.orca.cpportal.module.KeywordDetails;
import hsenidmobile.orca.cpportal.util.DefaultProperties;
import hsenidmobile.orca.cpportal.util.SubscriptionChargingType;
import hsenidmobile.orca.cpportal.util.SubscriptionChargingTypeConfiguration;
import hsenidmobile.orca.orm.sdp.SdpApplicationRepositoryImpl;
import hsenidmobile.orca.rest.registration.transport.CommonRegistrationRequestSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/*
*   $LastChangedDate$
*   $LastChangedBy$
*   $LastChangedRevision$
*/
@Controller
public abstract class AbstractBaseController {

    private static final Logger logger = LoggerFactory.getLogger(AbstractBaseController.class);
    public final static SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    protected static final Locale langEng = Locale.ENGLISH;
    protected static final Locale langTamil = new Locale("ta");
    protected static final Locale langSin = new Locale("si");

    @Autowired
    protected CommonRegistrationRequestSender commonRegistrationRequestSender;
    @Autowired
    private SdpApplicationRepositoryImpl sdpApplicationRepository;
    @Autowired
    protected DefaultProperties defaultProperties;
    @Autowired
	private SubscriptionChargingTypeConfiguration subsChargingTypeConfig;

    /**
     * Converts the enum type to string so that it can be shown in UI
     *
     * @return
     */
    protected ArrayList<PeriodUnit> addPeriodUnits() {
        ArrayList<PeriodUnit> periodUnits = new ArrayList<PeriodUnit>();
        periodUnits.add(PeriodUnit.HOUR);
        periodUnits.add(PeriodUnit.DAY);
        periodUnits.add(PeriodUnit.WEEK);
        periodUnits.add(PeriodUnit.MONTH);
        return periodUnits;
    }

    /**
     * Initial keyword count for sub category
     *
     * @return
     */
    protected List<KeywordDetails> getInitialKeywords(ArrayList<KeywordDetails> availableList) {
        int availableSize = 0;
        if (availableList != null) {
            availableSize = availableList.size();
        }
        List<KeywordDetails> list = new ArrayList<KeywordDetails>();
        for (int i = 0; i < 12 - availableSize; i++) {
            list.add(new KeywordDetails());
        }
        return list;
    }

    protected void setAvailableSubscriptionTypes(ApplicationInfo applicationInfo) {
//        SubscriptionChargingType[] subscriptionChargingTypeArray = new SubscriptionChargingType[3];
//        subscriptionChargingTypeArray[0] = SubscriptionChargingType.DAILY_SUBS_CHARGING;
//        subscriptionChargingTypeArray[1] = SubscriptionChargingType.WEEKLY_SUBS_CHARGING;
//        subscriptionChargingTypeArray[2] = SubscriptionChargingType.MONTHLY_SUBS_CHARGING;
//        applicationInfo.setSubscriptionTypeArray(subscriptionChargingTypeArray);
    	applicationInfo.setSubscriptionTypeArray(subsChargingTypeConfig.getSupportedSubscriptionChargingTypes());
    }

    protected String handleUnAuthorizedUserAccess(String userName, String appId) {
        logger.warn("Unauthorized access found for the application ID [{}] by the user [{}]", appId, userName);
        return "redirect:/error.html?messageId=unauthorized.access.error.message";
    }

    protected String getAdditionalUserDetails(String username, String key) {
        logger.debug("getting additional user details for username [{}] and key [{}]", username, key);
        BasicUserResponseMessage basicUserResponseMessage = commonRegistrationRequestSender.getBasicUserResponseMessage(username);
        try {
          return basicUserResponseMessage.getAdditionalDataMap().get(key);
        } catch (NullPointerException ex) {
            logger.error("Error occurred while getting additional user details for username [{}], key [{}]", username, key);
            return "";
        }
    }
}
