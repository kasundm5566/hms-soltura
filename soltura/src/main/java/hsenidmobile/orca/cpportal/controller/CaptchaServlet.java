/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 */
package hsenidmobile.orca.cpportal.controller;

import nl.captcha.Captcha;
import nl.captcha.servlet.CaptchaServletUtil;
import nl.captcha.text.renderer.ColoredEdgesWordRenderer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Random;

/*
*   $LastChangedDate$
*   $LastChangedBy$
*   $LastChangedRevision$
*/
public class CaptchaServlet extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(CaptchaServlet.class);
    private int height = 40;
    private int width = 150;

    public static final String CAPTCHA_KEY = "captcha_key_name_value";

    protected void doGet(HttpServletRequest req, HttpServletResponse response) throws ServletException {

        try {
            logger.debug("=================== Creating Captcha image ====================");
            response.setHeader("Cache-Control", "no-cache");
            response.setDateHeader("Expires", 0);
            response.setHeader("Pragma", "no-cache");
            response.setDateHeader("Max-Age", 0);

            BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            Graphics2D graphics2D = image.createGraphics();
            Random r = new Random();
            logger.debug("Random number [{}]", r);
            String token = Long.toString(Math.abs(r.nextLong()), 36);
            String ch = token.substring(0, 5);
            Color c = new Color(1, 52, 107);
            GradientPaint gp = new GradientPaint(30, 30, c, 15, 25, Color.BLUE, true);
            graphics2D.setPaint(gp);
            Font font = new Font("Verdana", Font.CENTER_BASELINE, 26);
            logger.debug("Font type [{}]", font);
            graphics2D.setFont(font);
            graphics2D.drawString(ch, 2, 20);
            graphics2D.dispose();

            Captcha captcha = new Captcha.Builder(width, height).addText(new ColoredEdgesWordRenderer())
                    .addBackground().addNoise()
                    .gimp()
                    .build();
            req.getSession(true).setAttribute(CAPTCHA_KEY, captcha.getAnswer());
            CaptchaServletUtil.writeImage(response, captcha.getImage());
        } catch (Throwable e) {
            logger.error("Error occurred ", e);
            throw new ServletException("Error occurred", e);
        }
    }
}
