/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.      AppNotFoundException
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hsenidmobile.orca.cpportal.controller;

import hsenidmobile.orca.core.applications.NcsType;
import hsenidmobile.orca.core.model.AvailableShortCodeInfo;
import hsenidmobile.orca.core.model.ContentProvider;
import hsenidmobile.orca.core.model.Msisdn;
import hsenidmobile.orca.core.model.RoutingKey;
import hsenidmobile.orca.core.util.CorrelationIdGenerationService;
import hsenidmobile.orca.cpportal.module.CreateKeyword;
import hsenidmobile.orca.cpportal.service.CreateKeyword2ndryService;
import hsenidmobile.orca.cpportal.service.CurrentUser;
import hsenidmobile.orca.cpportal.service.impl.ContentProviderService;
import hsenidmobile.orca.cpportal.service.impl.application.ApplicationService;
import hsenidmobile.orca.cpportal.util.DefaultProperties;
import hsenidmobile.orca.cpportal.util.logging.AuditEntity;
import hsenidmobile.orca.cpportal.util.logging.AuditLogActionProperties;
import hsenidmobile.orca.cpportal.validator.application.CreateKeywordValidator;
import hsenidmobile.orca.orm.repository.ContentProviderDetailsRepositoryImpl;
import hsenidmobile.orca.rest.sdp.routing.transport.SdpRkManageRequestSender;
import org.apache.cxf.jaxrs.client.ClientWebApplicationException;
import org.apache.log4j.NDC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.access.expression.WebExpressionVoter;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.*;

import static hsenidmobile.orca.cpportal.util.logging.SolturaAuditLogger.audit;
import static hsenidmobile.orca.rest.common.util.RestApiKeys.ROUTING_KEYS_CATEGORIES;
import static hsenidmobile.orca.rest.common.util.RestApiKeys.SP_ID;

/**
 * $LastChangedDate$ $LastChangedBy$ $LastChangedRevision$
 */
@Controller
@SessionAttributes("routingInfo")
public class CreateKeywordController {

	private static final Logger logger = LoggerFactory.getLogger(CreateKeywordController.class);
	@Autowired
	private ContentProviderService contentProviderService;
	@Autowired
	private ApplicationService applicationService;
	@Autowired
	private ContentProviderDetailsRepositoryImpl cpDetailsRepository;
	@Autowired
	private CreateKeywordValidator createKeywordValidator;
	@Autowired
	protected DefaultProperties defaultProperties;
	@Autowired
	CreateKeyword2ndryService additionalProcessingService;
	@Autowired
	protected SdpRkManageRequestSender sdpRkManageRequestSender;
    @Autowired
    private ContentProviderDetailsRepositoryImpl cpDetailsRepositoryImpl;
    @Autowired
    private AuditLogActionProperties auditLogAction;

	private static SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

	@RequestMapping(value = "/createKeyword")
	public String form(@ModelAttribute("routingInfo") CreateKeyword routingInfo) {

		routingInfo.setAppLevel(true);
		return "/createKeyword/createKeyword";
	}

	/**
	 * Set the selected operator
	 *
	 * @param operator
	 *            - selected operator
	 * @return - createKeyword model
	 */
	@RequestMapping(value = "/operator")
	public String form(@ModelAttribute("routingInfo") CreateKeyword routingInfo,
			@RequestParam("operator") String operator) {
		try {
			List<AvailableShortCodeInfo> availableShortCodeInfoList = new ArrayList<AvailableShortCodeInfo>();
			for (String s : getAvailableKeywords(routingInfo, operator)) {
				AvailableShortCodeInfo availableShortCodeInfo = new AvailableShortCodeInfo(new Msisdn(s, operator),
						NcsType.SMS);
				availableShortCodeInfoList.add(availableShortCodeInfo);
			}
			routingInfo.setAvailableShortCodes(availableShortCodeInfoList);
			routingInfo.setAppLevel(true);
			return "/createKeyword/createKeyword";
		} catch (ClientWebApplicationException e) {
			logger.error("Error while processing the rerquest, Server error : ", e);
			return "redirect:/error.html?messageId=view.connect.error";
		} catch (Throwable e) {
			logger.error("Error in creating new keyword : ", e);
			return "redirect:/error.html?messageId=create.keyword.error";
		}
	}

	private List<String> getAvailableKeywords(CreateKeyword routingInfo, String operator) {
		Map<String, List<String>> rkMap = routingInfo.getAvailableOperatorSpecificShortCodeMap();
		logger.trace("Found Routing key map[{}]", rkMap);
		if (rkMap != null) {
			List<String> operatorRks = rkMap.get(operator);
			if (operatorRks != null) {
				return operatorRks;
			}
		}
		throw new RuntimeException("No RoutingKeys found for Operator[" + operator + "]");
	}

	@RequestMapping(value = "/createNewKeyword")
	public String formCreateKeyword(@ModelAttribute("routingInfo") CreateKeyword routingInfo) {

		routingInfo.setAppLevel(false);
		if (!routingInfo.isPageHasErrors()) {
			routingInfo.setPageHasErrors(true);
		}
		return "/createKeyword/createKeyword";

	}

	/**
	 * Submit the Edited app
	 */
	@RequestMapping(value = "/submit/{appLevel}", method = RequestMethod.POST)
	public String addRoutingInfo(@ModelAttribute("routingInfo") CreateKeyword routingInfo, BindingResult bindingResult,
			SessionStatus status, @PathVariable boolean appLevel, HttpServletRequest request) throws Throwable {
		final long correlationId = CorrelationIdGenerationService.getCorrelationId(defaultProperties.getServerId());
        ContentProvider contentProvider = null;
		try {
            NDC.push(String.valueOf(correlationId));

			try {
				logger.debug("addRoutingInfo with : [{}]", routingInfo);
				contentProviderService.addRoutingKey(routingInfo, correlationId);
                contentProvider = cpDetailsRepositoryImpl.getContentProvider();
				status.setComplete();
                //Audit Log
                createKeywordAuditLog(routingInfo.getRoutingKey(), contentProvider, request, AuditEntity.ActionStatus.SUCCESS);
				if (appLevel) {
					return "redirect:/create/return.html";
				}
				return "redirect:/return/keywordSettings.html";
			} catch (ClientWebApplicationException e) {
				logger.error("Error while processing the rerquest, Server error : ", e);
				return "redirect:/error.html?messageId=view.connect.error";
			} catch (Throwable e) {
				logger.error("Error in creating new keyword : ", e);
				return "redirect:/error.html?messageId=create.keyword.error";
			}
		} finally {
			NDC.remove();
		}
	}

	/**
	 * Cancel the edit process
	 */
	@RequestMapping(value = "/cancel/{appLevel}", method = RequestMethod.GET)
	public String cancel(SessionStatus status, @PathVariable boolean appLevel,
			@ModelAttribute("routingInfo") CreateKeyword createKeyword) throws Exception {
		status.setComplete();
		if (additionalProcessingService.isOn()) {
			additionalProcessingService.formBackingObject(createKeyword);
		}
		if (appLevel) {
			return "redirect:/create/return.html";
		}
		return "redirect:/settings/keywordSettings.html";
	}

	@RequestMapping(value = "/validate", method = RequestMethod.GET)
	public String validateGetRequest(@ModelAttribute("routingInfo") CreateKeyword routingInfo,
			BindingResult bindingResult) throws Throwable {
		routingInfo.setPageHasErrors(true);
		return "createKeyword/createKeyword";
	}

	@RequestMapping(value = "/validate", method = RequestMethod.POST)
	public String validate(@ModelAttribute("routingInfo") CreateKeyword routingInfo, BindingResult bindingResult)
			throws Throwable {
		try {
			createKeywordValidator.validate(routingInfo, bindingResult);

			if (bindingResult.hasErrors()) {
				routingInfo.setPageHasErrors(true);
				return "createKeyword/createKeyword";
			}
			routingInfo.setPageHasErrors(false);
			return "createKeyword/createKeyword";
		} catch (ClientWebApplicationException e) {
			logger.error("Error while processing the rerquest, Server error : ", e);
			return "redirect:/error.html?messageId=view.connect.error";
		} catch (Throwable e) {
			logger.error("Error in creating new keyword : ", e);
			return "redirect:/error.html?messageId=create.keyword.error";
		}
	}

	@ModelAttribute("routingInfo")
	protected Object formBackingObject() {
		final CreateKeyword createKeyword = new CreateKeyword();
		Calendar one_month = Calendar.getInstance();
		one_month.add(Calendar.MONTH, 1);
		createKeyword.setsDate(dateFormat.format(new Date()));
		createKeyword.seteDate(dateFormat.format(one_month.getTime()));
		ContentProvider contentProvider = cpDetailsRepository.findByUsername(CurrentUser.getUsername());
		Map<String, Object> message = new HashMap<String, Object>();
		message.put(SP_ID, contentProvider.getSpId());
		message.put(ROUTING_KEYS_CATEGORIES, Arrays.asList(defaultProperties.getRkCategories())); // A
																									// Module
																									// can
																									// have
																									// more
																									// than
																									// one
																									// categories.
		Map<String, Set<String>> response = sdpRkManageRequestSender.retrieveShortcodes(message);
		Map<String, List<String>> allAvailableShortCodes = new HashMap<String, List<String>>();
		if (response != null) {
			for (Map.Entry<String, Set<String>> entry : response.entrySet()) {
				allAvailableShortCodes.put(entry.getKey(), new ArrayList<String>(entry.getValue()));
			}
			createKeyword.setAvailableOperatorSpecificShortCodeMap(allAvailableShortCodes);
		}
		if (additionalProcessingService.isOn()) {
			additionalProcessingService.formBackingObject(createKeyword);
		}
		return createKeyword;
	}

    private void createKeywordAuditLog(RoutingKey routingInfo, ContentProvider contentProvider, HttpServletRequest request,
                                   AuditEntity.ActionStatus status) {
        String info = String.format("Shortcode: %s-%s - Keyword: %s",
                routingInfo.getShortCode().getOperator(), routingInfo.getShortCode().getAddress(), routingInfo.getKeyword());
        audit(new AuditEntity.AuditEntityBuilder(contentProvider)
                .populateFromHttpRequest(request)
                .action(auditLogAction.getCreateKeywordAction())
                .description(String.format(auditLogAction.getCreateKeywordActionDesc(), info))
                .status(status)
                .build());
    }
}
