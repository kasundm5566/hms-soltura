/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hsenidmobile.orca.cpportal.controller;

import hsenidmobile.orca.core.model.ContentProvider;
import hsenidmobile.orca.core.model.Msisdn;
import hsenidmobile.orca.core.repository.AppRepository;
import hsenidmobile.orca.cpportal.module.ApplicationInfo;
import hsenidmobile.orca.cpportal.module.Registration;
import hsenidmobile.orca.cpportal.module.UseApplication;
import hsenidmobile.orca.cpportal.module.ViewApp;
import hsenidmobile.orca.cpportal.service.CurrentUser;
import hsenidmobile.orca.cpportal.service.impl.MsisdnValidatorServiceImpl;
import hsenidmobile.orca.cpportal.util.logging.AuditEntity;
import hsenidmobile.orca.cpportal.util.logging.AuditLogActionProperties;
import hsenidmobile.orca.cpportal.validator.application.ApplicationValidator;
import hsenidmobile.orca.orm.repository.ContentProviderDetailsRepositoryImpl;
import hsenidmobile.orca.rest.sdp.sp.transport.SpManageRequestSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import hsenidmobile.orca.cpportal.util.logging.AuditEntity;
import javax.servlet.http.HttpServletRequest;

import static hsenidmobile.orca.cpportal.util.logging.AuditEntity.AuditEntityBuilder;
import static hsenidmobile.orca.cpportal.util.logging.AuditEntity.ActionStatus;
import static hsenidmobile.orca.cpportal.util.logging.SolturaAuditLogger.audit;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
@Controller
@SessionAttributes(value = {"useApp", "app", "viewApp"})
public class HomeController extends AbstractBaseController {

    private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
    @Autowired
    private ContentProviderDetailsRepositoryImpl cpDetailsRepositoryImpl;
    @Autowired
    private ApplicationValidator applicationValidator;
    @Autowired
    private MsisdnValidatorServiceImpl msisdnValidatorService;
    @Autowired
    protected SpManageRequestSender spManageRequestSender;
    @Autowired
    protected AppRepository appRepository;
    @Autowired
    private AuditLogActionProperties auditLogAction;

    private ContentProvider contentProvider;

    @RequestMapping(value = "/home")
    public void home(@ModelAttribute("useApp") UseApplication useApplication,
                     @ModelAttribute("app") ApplicationInfo applicationInfo,
                     @ModelAttribute("viewApp") ViewApp viewApp,
                     @ModelAttribute("registration") Registration registration, HttpServletRequest request,
                     SessionStatus status) throws Throwable {
        ContentProvider cp = null;
        try {
            logger.debug("Application creation Getting current user = [{}]", CurrentUser.getUsername());
            String userName = CurrentUser.getUsername();

            cp = cpDetailsRepositoryImpl.getContentProvider();

            logger.debug("CP Detailas = [{}]", cp);
            registration.setCp(cp);
            registration.setShortCode(msisdnValidatorService.getShortCode());
            status.setComplete();

            homeAccessAuditLog(cp, request, ActionStatus.SUCCESS);
        } catch (Throwable e) {
            logger.error("Error occurred ", e);
            homeAccessAuditLog(cp, request, ActionStatus.FAILED);
            throw e;
        }
    }

    @ModelAttribute("useApp")
    protected Object formBackingUseApp() throws Exception {
        return new UseApplication();
    }

    @ModelAttribute("viewApp")
    protected Object formBackingViewApp() throws Exception {
        return new ViewApp();
    }

    @ModelAttribute("app")
    protected Object formBackingApp() throws Exception {
        return new ApplicationInfo();
    }

    private void homeAccessAuditLog(ContentProvider contentProvider, HttpServletRequest request, ActionStatus status) {
        audit(new AuditEntityBuilder(contentProvider)
                .populateFromHttpRequest(request)
                .action(auditLogAction.getHomeAccessAction())
                .description(auditLogAction.getHomeAccessActionDesc())
                .status(status)
                .build());
    }
}
