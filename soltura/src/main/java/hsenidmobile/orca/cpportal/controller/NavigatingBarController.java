/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hsenidmobile.orca.cpportal.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */

@Controller
public class NavigatingBarController extends AbstractBaseController {    

    @RequestMapping(value = "/navigation/navLink5.html")
    public String navLink5() {
        return "navigation/navLink5";
    }

    @RequestMapping(value = "/navigation/navLink4.html")
    public String navLink4() {
        return "navigation/navLink4";
    }

    @RequestMapping(value = "/navigation/navLink3.html")
    public String navLink3() {
        return "navigation/navLink3";
    }

    @RequestMapping(value = "/navigation/navLink2.html")
    public String navLink2() {
        return "navigation/navLink2";
    }
}