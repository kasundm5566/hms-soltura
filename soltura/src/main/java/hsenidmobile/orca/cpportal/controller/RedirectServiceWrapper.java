/* 
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited. 
 *   All Rights Reserved. 
 * 
 *   These materials are unpublished, proprietary, confidential source code of 
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET 
 *   of hSenid Software International (Pvt) Limited. 
 * 
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual 
 *   property rights in these materials. 
 *
 */
package hsenidmobile.orca.cpportal.controller;

import hsenidmobile.orca.core.applications.alert.AlertService;
import hsenidmobile.orca.core.applications.request.RequestService;
import hsenidmobile.orca.core.applications.subscription.Subscription;
import hsenidmobile.orca.core.applications.voting.VotingService;
import hsenidmobile.orca.cpportal.module.ApplicationInfo;
import hsenidmobile.orca.cpportal.module.Registration;
import hsenidmobile.orca.cpportal.util.ServiceTypes;
import hsenidmobile.orca.cpportal.validator.application.ApplicationValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.support.SessionStatus;

import java.text.MessageFormat;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class RedirectServiceWrapper {

    private static final Logger logger = LoggerFactory.getLogger(RedirectServiceWrapper.class);

    public static String goToCreateAppPage(final ApplicationInfo applicationInfo, final ApplicationValidator applicationValidator,
                                           BindingResult bindingResult) {
        if (applicationInfo.getApp().getService() instanceof Subscription) {
            applicationValidator.applicationValidate(applicationInfo, bindingResult);
            if (bindingResult.hasErrors()) {
                applicationInfo.setPageHasErrors(true);
                return "createApplication/appDetails";
            } else if (!applicationInfo.isSubCategoryRequired()) {
                applicationInfo.setPageHasErrors(false);
                return "createApplication/appDetails";
            } else {
                applicationInfo.setPageHasErrors(true);
                return "createApplication/appType/createSubscriptionApp";
            }
        } else if (applicationInfo.getApp().getService() instanceof VotingService) {
            applicationValidator.applicationValidate(applicationInfo, bindingResult);
            applicationInfo.setPageHasErrors(true);
            if (bindingResult.hasErrors()) {
                return "createApplication/appDetails";
            } else {
                return "createApplication/appType/createVotingApp";
            }
        } else if (applicationInfo.getApp().getService() instanceof AlertService) {
            applicationValidator.applicationValidate(applicationInfo, bindingResult);
            if (bindingResult.hasErrors()) {
                applicationInfo.setPageHasErrors(true);
                return "createApplication/appDetails";
            } else if (!applicationInfo.isSubCategoryRequired()) {
                applicationInfo.setPageHasErrors(false);
                return "createApplication/appDetails";
            } else {
                applicationInfo.setPageHasErrors(true);
                return "createApplication/appType/createAlertApp";
            }
        } else if (applicationInfo.getApp().getService() instanceof RequestService) {
            applicationValidator.applicationValidate(applicationInfo, bindingResult);
            if (bindingResult.hasErrors()) {
                applicationInfo.setPageHasErrors(true);
                return "createApplication/appDetails";
            } else if (!applicationInfo.isSubCategoryRequired()) {
                applicationInfo.setPageHasErrors(false);
                return "createApplication/appDetails";
            } else {
                applicationInfo.setPageHasErrors(true);
                return "createApplication/appType/createRequestApp";
            }
        }
        return "redirect:/create/type/SMS.html";
    }

    public static String goToConfirmPage(final ApplicationInfo applicationInfo,
                                         final ApplicationValidator applicationValidator,
                                         final BindingResult bindingResult,
                                         final SessionStatus status) {

        logger.debug("CONFIRM PAGE :: APP INFO ********* [{}]", applicationInfo.toString());
        logger.debug("CONFIRM PAGE :: APP VALIDATOR ++++++++ [{}]", applicationValidator);
        logger.debug("CONFIRM PAGE :: BINDING RESULT ########### [{}]", bindingResult);
        logger.debug("CONFIRM PAGE :: SESSION STATE @@@@@@@@@@ [{}]", status);


        if (applicationInfo.getApp().getService() instanceof Subscription) {
            return confirmSubscriptionApplication(applicationInfo, bindingResult, applicationValidator);
        } else if (applicationInfo.getApp().getService() instanceof VotingService) {
            return confirmVotingApplication(applicationInfo, bindingResult, applicationValidator);
        } else if (applicationInfo.getApp().getService() instanceof AlertService) {
            return confirmAlertApplication(applicationInfo, bindingResult, applicationValidator);
        } else if (applicationInfo.getApp().getService() instanceof RequestService) {
            return confirmRequestApplication(applicationInfo, bindingResult, applicationValidator);
        }
        status.setComplete();
        return "redirect:/home.html";
    }

    private static String confirmAlertApplication(ApplicationInfo applicationInfo, BindingResult bindingResult,
                                                  ApplicationValidator applicationValidator) {
        appendServiceTypeToDescription(applicationInfo, ServiceTypes.ALERT);
        applicationInfo.setReadOnly(true);
        return "confirm/confirmCreateAlertApp";
    }


    private static String confirmVotingApplication(ApplicationInfo applicationInfo, BindingResult bindingResult,
                                                   ApplicationValidator applicationValidator) {
        appendServiceTypeToDescription(applicationInfo, ServiceTypes.VOTING);
        applicationInfo.setReadOnly(true);
        return "confirm/confirmCreateVotingApp";
    }

    private static String confirmSubscriptionApplication(ApplicationInfo applicationInfo, BindingResult bindingResult,
                                                         ApplicationValidator applicationValidator) {
        appendServiceTypeToDescription(applicationInfo, ServiceTypes.SUBSCRIPTION);
        applicationInfo.setReadOnly(true);
        return "confirm/confirmCreateSubscriptionApp";
    }

    private static String confirmRequestApplication(ApplicationInfo applicationInfo, BindingResult bindingResult,
                                                    ApplicationValidator applicationValidator) {
        appendServiceTypeToDescription(applicationInfo, ServiceTypes.REQUEST);
        applicationInfo.setReadOnly(true);
        return "confirm/confirmCreateRequestApp";
    }

    public static String handleRegistrationSsoApiError(String errorMessage, Registration registration) {
        registration.setPageHasErrors(true);
        registration.setNotifMessage(errorMessage);
        return "redirect:/error.html?messageId=" + errorMessage;
    }

    private static void appendServiceTypeToDescription(ApplicationInfo applicationInfo, ServiceTypes types) {

        String description = applicationInfo.getApp().getDescription();
        String suffix = MessageFormat.format(" - {0}", types.getValue());
        if (!description.endsWith(types.getValue())) {
            String newDescription = new StringBuilder()
                    .append(description)
                    .append(suffix).toString();
            applicationInfo.getApp().setDescription(newDescription);
        }
    }
}
