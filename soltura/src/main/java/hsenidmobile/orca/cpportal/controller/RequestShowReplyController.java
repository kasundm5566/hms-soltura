/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.      AppNotFoundException
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hsenidmobile.orca.cpportal.controller;

import hsenidmobile.orca.core.applications.request.RequestService;
import hsenidmobile.orca.core.applications.request.RequestServiceData;
import hsenidmobile.orca.core.model.Application;
import hsenidmobile.orca.core.model.message.RequestMessage;
import hsenidmobile.orca.core.model.message.RequestShowReplyRequest;
import hsenidmobile.orca.cpportal.util.client.HttpRequestSender;
import hsenidmobile.orca.orm.repository.AppRepositoryImpl;
import hsenidmobile.orca.orm.repository.RequestServiceDataRepositoryImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;

/**
 * $LastChangedDate: 2011-08-03 17:51:22 +0530 (Wed, 03 Aug 2011) $
 * $LastChangedBy: dulika $
 * $LastChangedRevision: 75535 $
 */
@Controller
@SessionAttributes("replyMessage")
public class RequestShowReplyController extends AbstractBaseController {

    private static final Logger logger = LoggerFactory.getLogger(RequestShowReplyController.class);
    @Autowired
    private AppRepositoryImpl appRepository;
    @Autowired
    @Qualifier("httpAppUpdateRequestSender")
    private HttpRequestSender httpAppUpdateRequestSender;
    @Autowired
    private RequestServiceDataRepositoryImpl requestServiceDataRepository;

    @RequestMapping(value = "/replyMessage")
    public String form(@ModelAttribute("replyMessage") RequestMessage replyMessage,
                       @RequestParam("appId") String appId,
                       @RequestParam("pageNumber") String pageNumber,
                       @RequestParam("messageId") String messageId) {

        replyMessage.setPageNumber(pageNumber);
        replyMessage.setAppId(appId);
        replyMessage.setMessageId(Long.parseLong(messageId));
        replyMessage.setMessageMaxLength(defaultProperties.getMaxMessageLength());
        return "/requestShow/sendResponse";
    }

    @RequestMapping(value = "/viewRequestReport", method = RequestMethod.POST)
    public String appDetails(@ModelAttribute("replyMessage")RequestMessage replyMessage,
                             SessionStatus status) throws Exception {
        RequestServiceData originalMsg = requestServiceDataRepository.findRequestServiceData(replyMessage.getMessageId());
        logger.debug("Sending Specific Response to [{}] received msg[{}]", replyMessage, originalMsg);

        ValidationResult result = isValidToReply(replyMessage, originalMsg);
        if (!result.isValid()) {
            return "redirect:/error.html?messageId=" + result.getErrorMsgKey();
        }

        RequestShowReplyRequest requestShowReplyRequest = new RequestShowReplyRequest();
        requestShowReplyRequest.setRecepient(originalMsg.getMsisdn().getAddress());
        requestShowReplyRequest.setAppId(replyMessage.getAppId());
        requestShowReplyRequest.setMessageContent(replyMessage.getMessage());
        requestShowReplyRequest.setMessageId(replyMessage.getMessageId());

        Application app = appRepository.findAppByAppId(replyMessage.getAppId());
        if(app.getService() instanceof RequestService) {
            sendRequestToCore(requestShowReplyRequest);
        } else {
            logger.warn("Unsupported service type found, please check the existency ");
            return "redirect:/error.html?messageId=view.send.request.reply.error";
        }
        status.setComplete();
        return "redirect:reports/viewReport/"+replyMessage.getAppId()+"/"+replyMessage.getPageNumber()+".html";
    }

    private ValidationResult isValidToReply(RequestMessage replyMessage, RequestServiceData originalMsg) {
        if(originalMsg.isReplied()){
            logger.error("Reply is already sent to message[{}]", originalMsg);
            return new ValidationResult("reply.already.sent.to.request.show.msg.error");
        }

        if(!originalMsg.getAppId().equals(replyMessage.getAppId())){
            logger.error("Message trying to reply for[{}] doesn't blog to requested application[{}]", originalMsg, replyMessage.getAppId());
            return new ValidationResult("cannot.send.reply.to.request.show.msg.error");
        }

        return new ValidationResult();
    }

    private void sendRequestToCore(RequestShowReplyRequest requestShowReplyRequest) {
        try {
            httpAppUpdateRequestSender.sendMessage(requestShowReplyRequest);
        } catch (Exception e) {
            logger.error("Error while sending the reply", e);
        }
    }

    @ModelAttribute("replyMessage")
    protected Object formBackingObject() {
        final RequestMessage replyMessage = new RequestMessage();

        return replyMessage;
    }

    private class ValidationResult{
        private final boolean isValid;
        private final String errorMsgKey;

        private ValidationResult(String errorMsgKey) {
            this.isValid = false;
            this.errorMsgKey = errorMsgKey;
        }

        private ValidationResult() {
            this.isValid = true;
            this.errorMsgKey = "";
        }

        public boolean isValid() {
            return isValid;
        }

        public String getErrorMsgKey() {
            return errorMsgKey;
        }
    }

}

