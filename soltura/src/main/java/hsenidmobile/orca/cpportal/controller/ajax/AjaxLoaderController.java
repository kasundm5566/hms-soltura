/*   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 **/
package hsenidmobile.orca.cpportal.controller.ajax;

import hsenidmobile.orca.core.model.ContentProvider;
import hsenidmobile.orca.core.model.Msisdn;
import hsenidmobile.orca.core.model.RoutingInfo;
import hsenidmobile.orca.core.model.RoutingKey;
import hsenidmobile.orca.core.repository.ContentProviderRepository;
import hsenidmobile.orca.core.services.impl.ParentKeyValidityPeriodException;
import hsenidmobile.orca.core.services.impl.RoutingKeyException;
import hsenidmobile.orca.core.services.impl.RoutingKeyManagerImpl;
import hsenidmobile.orca.cpportal.service.impl.AjaxService;
import hsenidmobile.orca.cpportal.service.impl.application.ApplicationService;
import hsenidmobile.orca.cpportal.validator.application.ApplicationValidator;
import hsenidmobile.orca.cpportal.validator.application.CreateKeywordValidator;
import org.apache.cxf.jaxrs.client.ClientWebApplicationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

/*
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 */

@Controller
public class AjaxLoaderController {

    private static final Logger logger = LoggerFactory.getLogger(AjaxLoaderController.class);
    @Autowired
    private AjaxService ajaxService;
    @Autowired
    private ApplicationValidator applicationValidator;
    @Autowired
    private RoutingKeyManagerImpl routingKeyServiceImpl;
    @Autowired
    private ApplicationService applicationService;
    @Autowired
    private ContentProviderRepository contentProviderRepository;
    @Autowired
    private CreateKeywordValidator createKeywordValidator;

    @RequestMapping(value = "/ajaxLoaderValidateAppName/{appName}")
    public void validateAppName(@PathVariable String appName,
                                HttpServletRequest request, HttpServletResponse response) throws Throwable {
        try {
            logger.debug("Ajax Loader called for validating application name : name [{}]" ,appName);
            ajaxService.respondForAppNameValidation(response, (applicationService.isAppNameExistinSDP(appName)
                    || applicationValidator.isAppNameExists(appName)));
        } catch (Throwable e) {
            logger.error("Error occurred ", e);
            throw e;
        }
    }

    @RequestMapping(value = "/ajaxLoaderValidateMsisdn/{msisdn}")
    public void validateMsisdn(@PathVariable String msisdn,
                               HttpServletRequest request, HttpServletResponse response) throws Throwable {
        try {
            logger.debug("Ajax Loader called for validating msisdn : [{}]", msisdn);
        } catch (Throwable e) {
            logger.error("Error occurred ", e);
            throw e;
        }
    }

    @RequestMapping(value = "/ajaxLoaderValidateRegistration/{name}")
    public void validateVerificationCode(@PathVariable String name,
                                         HttpServletRequest request, HttpServletResponse response) throws Throwable {
        logger.debug("Ajax Loader called for validating Verification Code for User [{}]", name);
        try {
            /*ContentProvider cp = contentProviderRepository.findByName(name);*/
            boolean verificationStatus = true/*cp.getMsisdns().get(0).isMsisdnVerified()*/;
            logger.debug("MSISDN Verified Status [{}]", verificationStatus);
            ajaxService.respondForSuccessfullVerification(response, verificationStatus);
        } catch (Throwable e) {
            logger.error("Error occurred ", e);
            throw e;
        }

    }

    @RequestMapping(value = "/ajaxLoaderValidateKeyword/{keyword}/{route}")
    public void validateKeyword(@PathVariable String keyword, @PathVariable String route,
                                HttpServletRequest request, HttpServletResponse response) throws Throwable {
        try {
            logger.debug("Ajax Loader called for validating : keyword [{}] route [{}]", new Object[]{keyword, route});
            final String[] routeArray = route.split("-");
            final RoutingInfo routingInfo = new RoutingInfo();
            routingInfo.setRoutingKey(new RoutingKey(new Msisdn(routeArray[0], routeArray[1]), keyword.trim().toLowerCase()));
            routingInfo.setStartDate(new Date().getTime());
            routingInfo.setEndDate(new Date().getTime());
            final ContentProvider contentProvider = applicationService.getContentProviderDetails(true);
            String isKeywordAvailable = "false";
            try {
                isKeywordAvailable = Boolean.toString(!routingKeyServiceImpl.isRoutingKeyAvailableToCreate(routingInfo,
                        contentProvider));
            } catch (ParentKeyValidityPeriodException e) {
                logger.warn("Exception in ajax validator for keyword : ", e);
                isKeywordAvailable = "dur";
            } catch (RoutingKeyException e) {
                logger.warn("Exception in ajax validator for keyword : ", e);
            }
            logger.debug("validating keyword : [{}] content provider [{}] validate keyword available [{}]",
                    new Object[] {keyword, contentProvider, isKeywordAvailable});
            ajaxService.respondForKeywordValidation(response, isKeywordAvailable);
        } catch (ClientWebApplicationException e) {
            logger.error("Error while processing the rerquest, Server error : ", e);
        } catch (Throwable e) {
            logger.error("Error occurred ", e);
            throw e;
        }
    }

    @RequestMapping(value = "/ajaxLoaderReadMessage/{messageId}")
    public void markRequestShowMessageAsRead(@PathVariable long messageId,
                                             HttpServletRequest request, HttpServletResponse response)
            throws Throwable {
        try {
            ajaxService.updateMessageReadStatus(messageId, response);
        } catch (Throwable e) {
            logger.error("Error occurred ", e);
            throw e;
        }
    }

    @RequestMapping(value = "/ajaxLoadMessagesForSubCategories/{appId}/{selectedSubCategoryIds}/{pageNumber}/" +
            "{messagesPerPage}")
    public void selectMessagesByCategory(@PathVariable String appId, @PathVariable String selectedSubCategoryIds,
                                         @PathVariable int pageNumber, @PathVariable int messagesPerPage,
                                         HttpServletRequest request, HttpServletResponse response) throws Throwable {
        try {

            ajaxService.getMessagesByCategory(appId, selectedSubCategoryIds, pageNumber,messagesPerPage, response);
        } catch (Throwable e) {
            logger.error("Error occurred ", e);
            throw e;
        }
    }

    @RequestMapping(value = "/ajaxLoaderRequestMessage/{appId}/{requestServiceId}/{start}/{end}")
    public void fetchMessagesForDataLoading(@PathVariable String appId, @PathVariable long requestServiceId,
                                            @PathVariable int start, @PathVariable int end,HttpServletRequest request,
                                            HttpServletResponse response) throws Throwable {
        try {
            logger.debug("AJAX CONTROLLER FETCH");
            
            ajaxService.fetchMessagesForPage(appId, requestServiceId, start, end, response);
        } catch (Throwable e) {
            logger.error("Error occurred ", e);
            throw e;
        }
    }
}
