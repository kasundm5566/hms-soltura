/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hsenidmobile.orca.cpportal.controller.application;

import hsenidmobile.orca.core.model.ContentProvider;
import hsenidmobile.orca.core.model.OrcaException;
import hsenidmobile.orca.cpportal.controller.AbstractBaseController;
import hsenidmobile.orca.cpportal.controller.RedirectServiceWrapper;
import hsenidmobile.orca.cpportal.module.ApplicationInfo;
import hsenidmobile.orca.cpportal.service.ApplicationInfoBuilder;
import hsenidmobile.orca.cpportal.service.CurrentUser;
import hsenidmobile.orca.cpportal.service.IntegratedDbCreateApplictionServiceImpl;
import hsenidmobile.orca.cpportal.service.impl.MsisdnValidatorServiceImpl;
import hsenidmobile.orca.cpportal.service.impl.application.ApplicationService;
import hsenidmobile.orca.cpportal.service.mock.SdpRoutingKeyRepository;
import hsenidmobile.orca.cpportal.util.logging.AuditEntity;
import hsenidmobile.orca.cpportal.util.logging.AuditLogActionProperties;
import hsenidmobile.orca.cpportal.validator.application.ApplicationValidator;
import hsenidmobile.orca.orm.repository.ContentProviderDetailsRepositoryImpl;
import org.apache.cxf.jaxrs.client.ClientWebApplicationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;

import static hsenidmobile.orca.cpportal.util.logging.SolturaAuditLogger.audit;
import static hsenidmobile.orca.rest.common.util.RestApiKeys.*;

import javax.servlet.http.HttpServletRequest;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
@Controller
@SessionAttributes("app")
public class  CreateApplicationController extends AbstractBaseController {

    private static final Logger logger = LoggerFactory.getLogger(CreateApplicationController.class);
    @Autowired
    private ApplicationValidator applicationValidator;
    @Autowired
    private ApplicationService applicationService;
    @Autowired
    private MsisdnValidatorServiceImpl msisdnValidatorService;
    @Autowired
    private ApplicationInfoBuilder applicationInfoBuilder;
    @Autowired
    private ContentProviderDetailsRepositoryImpl cpDetailsRepositoryImpl;
    @Autowired
    private AuditLogActionProperties auditLogAction;

    @RequestMapping(value = "/create/ncs")
    public String createApplicationNcs(@ModelAttribute("app") ApplicationInfo applicationInfo, SessionStatus status) {
        logger.debug("CREATE APP NCS REQ - ROUTING KEY [{}]", applicationInfo.getApp().getRoutingKeys());
        status.setComplete();
        return "redirect:type/SMS.html";
    }


    @RequestMapping(value = "/create/type/{ncsType}")
    public String createApplicationType(@PathVariable String ncsType,
                                        @ModelAttribute("app") ApplicationInfo applicationInfo,
                                        HttpServletRequest request) throws Throwable {
        ContentProvider contentProvider = null;
        try {
            contentProvider = cpDetailsRepositoryImpl.getContentProvider();

            logger.debug("CREATE APP TYPE REQ - ROUTING KEY [{}]", applicationInfo.getApp().getRoutingKeys());
            applicationInfoBuilder.setAppTypeInitialData(applicationInfo, ncsType);
            applicationInfo.setAdvanceConfigurationRequired(applicationService.isAdvanceSettingRequired());
            createAppAccessAuditLog("", contentProvider, request, AuditEntity.ActionStatus.SUCCESS);
        } catch (ClientWebApplicationException e) {
            logger.error("Error while processing the rerquest, Server error : ", e);
            createAppAccessAuditLog("", contentProvider, request, AuditEntity.ActionStatus.FAILED);
            return "redirect:/error.html?messageId=view.server.error";
        } catch (Throwable e) {
            logger.error("Error occurred while setting the initial data for application type selection page :", e);
            createAppAccessAuditLog("", contentProvider, request, AuditEntity.ActionStatus.FAILED);
            return "redirect:/error.html?messageId=create.application.loding.error";
        }
        return "/createApplication/applicationTypeSelection";
    }

    /**
     * Set the Initial application details for a channel application
     * @param applicationInfo - contain all the information regarding the application
     * @return  - Model contain the application creation methods for subscription
     */
    @RequestMapping(value = "/create/subscription")
    public String createSubscription(@ModelAttribute("app") ApplicationInfo applicationInfo,
                                     HttpServletRequest request) throws Throwable {
        applicationInfo.setTransactionPointService((IntegratedDbCreateApplictionServiceImpl) applicationService.getTransactionPointService());
        if (pageHasErrors(applicationInfo)) {
            applicationInfo = new ApplicationInfo();
            applicationInfo.setEditState(true);
            applicationInfo.setReadOnly(false);
            return "createApplication/appDetails";
        }

        ContentProvider contentProvider = null;
        try {
            applicationInfoBuilder.setInitialDataForSubscription(applicationInfo);
            contentProvider = cpDetailsRepositoryImpl.getContentProvider();

            String isSpMsisdnVerified = getAdditionalUserDetails(CurrentUser.getUsername(), MSISDN_VERIFIED);
            applicationInfo.setSpMsisdnVerified(isSpMsisdnVerified);
            applicationInfo.setServiceUrl(request.getContextPath() + request.getServletPath());
            applicationInfo.setMsisdnVerifyAllowedOnAppCreate(applicationInfoBuilder.isMsisdnVerifyAllowedOnAppCreate());
            //Audit Logging
            createAppAccessAuditLog("Services ", contentProvider, request, AuditEntity.ActionStatus.SUCCESS);
        } catch (Throwable e) {
            logger.error("Error occurred while generating initial data for subscription application : ", e);
            createAppAccessAuditLog("Services ", contentProvider, request, AuditEntity.ActionStatus.FAILED);
            return "redirect:/error.html?messageId=subscription.app.loding.error";
        }
        return "createApplication/appDetails";
    }

    /**
     * Set the Initial application details for a Voting type application
     * @return - Model contain the application creation methods for Voting
     */
    @RequestMapping(value = "/create/voting")
    public String createVoting(@ModelAttribute("app") ApplicationInfo applicationInfo,
                               HttpServletRequest request) throws Throwable {
        applicationInfo.setTransactionPointService((IntegratedDbCreateApplictionServiceImpl) applicationService.getTransactionPointService());
        if (pageHasErrors(applicationInfo)) {
            return "createApplication/appDetails";
        }

        ContentProvider contentProvider = null;
        try {
            contentProvider = cpDetailsRepositoryImpl.getContentProvider();
            if(!applicationInfo.isReadOnly())  {
                applicationInfoBuilder.setInitialDataForVoting(applicationInfo);
                //Audit Logging
                createAppAccessAuditLog("Voting ", contentProvider, request, AuditEntity.ActionStatus.SUCCESS);
            } else{
                applicationInfo.setReadOnly(false);
            }
        } catch (Throwable e) {
            logger.error("Error occurred while generating initial data for voting application : ", e);
            createAppAccessAuditLog("Voting ", contentProvider, request, AuditEntity.ActionStatus.FAILED);
            return "redirect:/error.html?messageId=voting.app.loding.error";
        }
        return "createApplication/appDetails";
    }

    /**
     * Set the Initial application details for a Alert type application
     * @param applicationInfo - contain all the information regarding the application
     * @return - Model contain the application creation methods for Alert
     */
    @RequestMapping(value = "/create/alert")
    public String createAlert(@ModelAttribute("app") ApplicationInfo applicationInfo, HttpServletRequest request) throws Throwable {
        applicationInfo.setTransactionPointService((IntegratedDbCreateApplictionServiceImpl) applicationService.getTransactionPointService());
        if (pageHasErrors(applicationInfo)) {
            applicationInfo.setReadOnly(false);
            return "createApplication/appDetails";
        }

        ContentProvider contentProvider = null;
        try {
            applicationInfoBuilder.setInitialDataForAlert(applicationInfo);
            contentProvider = cpDetailsRepositoryImpl.getContentProvider();

            String isSpMsisdnVerified = getAdditionalUserDetails(CurrentUser.getUsername(), MSISDN_VERIFIED);
            applicationInfo.setSpMsisdnVerified(isSpMsisdnVerified);
            applicationInfo.setServiceUrl(request.getContextPath() + request.getServletPath());
            applicationInfo.setMsisdnVerifyAllowedOnAppCreate(applicationInfoBuilder.isMsisdnVerifyAllowedOnAppCreate());
            //Audit Logging
            createAppAccessAuditLog("Alert ", contentProvider, request, AuditEntity.ActionStatus.SUCCESS);
        } catch (Throwable e) {
            logger.error("Error occurred while generating initial data for alert application : ", e);
            createAppAccessAuditLog("Alert ", contentProvider, request, AuditEntity.ActionStatus.FAILED);
            return "redirect:/error.html?messageId=alert.app.loding.error";
        }
        return "createApplication/appDetails";
    }

    /**
     * Set the Initial application details for a Request type application
     * @param applicationInfo - contain all the information regarding the application
     * @return - Model contain the application creation methods for Request
     */
    @RequestMapping(value = "/create/request")
    public String createRequest(@ModelAttribute("app") ApplicationInfo applicationInfo,
                                HttpServletRequest request) throws Throwable {
        applicationInfo.setTransactionPointService((IntegratedDbCreateApplictionServiceImpl) applicationService.getTransactionPointService());
        if (pageHasErrors(applicationInfo)) {
            applicationInfo.setReadOnly(false);
            return "createApplication/appDetails";
        }

        ContentProvider contentProvider = null;
        logger.debug("CREATE REQ - ROUTING KEY [{}]", applicationInfo.getApp().getRoutingKeys());
        try {
            applicationInfoBuilder.setInitialDataForRequest(applicationInfo);
            contentProvider = cpDetailsRepositoryImpl.getContentProvider();

            createAppAccessAuditLog("Request ", contentProvider, request, AuditEntity.ActionStatus.SUCCESS);
        } catch (Throwable e) {
            logger.error("Error occurred while generating initial data for request application : ", e);
            createAppAccessAuditLog("Request ", contentProvider, request, AuditEntity.ActionStatus.FAILED);
            return "redirect:/error.html?messageId=request.app.loding.error";
        }
        return "createApplication/appDetails";
    }

    private boolean pageHasErrors(ApplicationInfo applicationInfo) {
        if (! applicationInfo.isPageHasErrors()) {
            applicationInfo.setPageHasErrors(true);
            logger.debug("Back to application creation page");
            return true;
        }
        return false;
    }

    /**
     * Return to the Application again after creating the Keyword
     * @param applicationInfo - contain all the information regarding the application
     * @return - Model contain the application creation methods for the application
     */
    @RequestMapping(value = "/create/return")
    public String returnFromKeyword(@ModelAttribute("app") ApplicationInfo applicationInfo) throws Throwable {
        logger.debug("RETURN FROM KEYWORD - ROUTING KEY [{}]", applicationInfo.getApp().getRoutingKeys());

        try {
            applicationInfoBuilder.setInitialDataAfterKeywordCreation(applicationInfo);
            String returnUrl;
            returnUrl = "createApplication/appDetails";
            return returnUrl;

        } catch (ClientWebApplicationException e) {
            logger.error("Error while processing the rerquest, Server error : ", e);
            return "redirect:/error.html?messageId=view.server.error";
        }catch (Throwable e) {
            logger.error("Error occurred while loading the enetered application details : ", e);
            return "redirect:/error.html?messageId=application.data.loding.error";
        }
    }

    /**
     * Direct to the Create application page
     * @param applicationInfo - contain all the information regarding the application
     * @return createApplication/appDetails.jspx
     */
    @RequestMapping(value = "/create/appDetails")
    public String appDetails(@ModelAttribute("app") ApplicationInfo applicationInfo,BindingResult bindingResult)
            throws Throwable {
        logger.debug("APP DETAILS REQ - ROUTING KEY [{}]", applicationInfo.getApp().getRoutingKeys());
        try {
            return RedirectServiceWrapper.goToCreateAppPage(applicationInfo, applicationValidator, bindingResult);
        } catch (ClientWebApplicationException e) {
            logger.error("Error while processing the rerquest, Server error : ", e);
            return "redirect:/error.html?messageId=view.server.error";
        } catch (Throwable e) {
            logger.error("Error occurred while redirecting to the sub category detail page : ", e);
            return "redirect:/error.html?messageId=redirect.subcategory.page.error";
        }
    }

    /**
     * Validation of the Request application
     */
    @RequestMapping(value = "/create/validateReq")
    public String validateRequestAppSpecificInfo(@ModelAttribute("app") ApplicationInfo applicationInfo,
                                                 BindingResult bindingResult) throws Throwable {
        logger.debug("Validating Additional data for the request application [{}] ", applicationInfo.getApp().getAppName());
        try {

            applicationValidator.requestValidate(applicationInfo, bindingResult);
            if (bindingResult.hasErrors()) {
                applicationInfo.setPageHasErrors(true);
            } else {
                applicationInfo.setPageHasErrors(false);
            }
            return "createApplication/appType/createRequestApp";
        } catch (Throwable e) {
            logger.error("Error occurred while redirecting to the confirm page : ", e);
            return "redirect:/error.html?messageId=redirect.confirm.page.error";
        }
    }

    /**
     * Validation of the Alert application
     */
    @RequestMapping(value = "/create/validateAlert")
    public String validateAlertAppSpecificInfo(@ModelAttribute("app") ApplicationInfo applicationInfo,
                                               BindingResult bindingResult) throws Throwable {
        logger.debug("Validating Additional data for the alert application [{}] ", applicationInfo.getApp().getAppName());
        try {
            applicationValidator.alertValidate(applicationInfo, bindingResult);
            if(bindingResult.hasErrors()) {
                applicationInfo.setPageHasErrors(true);
            } else {
                applicationInfo.setPageHasErrors(false);
            }
            return  "createApplication/appType/createAlertApp";
        } catch (Throwable e) {
            logger.error("Error occurred while redirecting to the confirm page : ", e);
            return "redirect:/error.html?messageId=redirect.confirm.page.error";
        }
    }

    /**
     * Validation of the Channel application
     */
    @RequestMapping(value = "/create/validateChannel")
    public String validateChannelAppSpecificInfo(@ModelAttribute("app") ApplicationInfo applicationInfo,
                                                 BindingResult bindingResult) throws Throwable {
        logger.debug("Validating Additional data for the channel application [{}] ", applicationInfo.getApp().getAppName());
        try {
            applicationValidator.subscriptionValidate(applicationInfo, bindingResult);
            if(bindingResult.hasErrors()) {
                applicationInfo.setPageHasErrors(true);
            } else {
                applicationInfo.setPageHasErrors(false);
            }
            return  "createApplication/appType/createSubscriptionApp";
        } catch (Throwable e) {
            logger.error("Error occurred while redirecting to the confirm page : ", e);
            return "redirect:/error.html?messageId=redirect.confirm.page.error";
        }
    }

    /**
     * Validation of the Voting application
     */
    @RequestMapping(value = "/create/validateVoting")
    public String validateVotingAppSpecificInfo(@ModelAttribute("app") ApplicationInfo applicationInfo,
                                                BindingResult bindingResult) throws Throwable {
        logger.debug("Validating Additional data for the voting application [{}] ", applicationInfo.getApp().getAppName());
        try {
            applicationValidator.votingValidate(applicationInfo, bindingResult);

            if(bindingResult.hasErrors()) {
                applicationInfo.setPageHasErrors(true);
            } else {
                applicationInfo.setPageHasErrors(false);
            }
            return  "createApplication/appType/createVotingApp";
        } catch (Throwable e) {
            logger.error("Error occurred while redirecting to the confirm page : ", e);
            return "redirect:/error.html?messageId=redirect.confirm.page.error";
        }
    }

    /**
     * Display the confirm page according to the app type
     * @return  Confirm Page
     */
    @RequestMapping(value = "/create/confirm")
    public String confirm(@ModelAttribute("app") ApplicationInfo applicationInfo,
                          BindingResult bindingResult, SessionStatus status) throws Throwable {
        try {
            return RedirectServiceWrapper.goToConfirmPage(applicationInfo, applicationValidator, bindingResult, status);
        } catch (Throwable e) {
            logger.error("Error occurred while redirecting to the confirm page : ", e);
            return "redirect:/error.html?messageId=redirect.confirm.page.error";
        }
    }

    /**
     * Save the application (use a RoutingKeyRepository to store the application key detials)
     * @return - Application successfully created massage displaying page with links
     * @throws Throwable
     */

    @RequestMapping(value = "/create/save")
    public String save(@ModelAttribute("app") ApplicationInfo applicationInfo,
                       SessionStatus status, HttpServletRequest request) throws Throwable {
        logger.debug("[APPLICATION_CREATION_STARTED][user={}]", applicationInfo.getApp().getAppName());
        ContentProvider contentProvider = null;
        try {
            contentProvider = cpDetailsRepositoryImpl.getContentProvider();
            SdpRoutingKeyRepository.insertNewSelecteditem(applicationInfo.getApp().getAppName(),applicationInfo.getSelectedRoutingKeyValues());
            if (applicationInfo == null || applicationInfo.getApp() == null || applicationInfo.getApp().getAppName() == null) {
                return "/save/saveSuccess";
            }
            applicationInfo.getApp().setDescription(applicationInfo.getApp().getDescription().replaceAll("\r\n"," "));
            logger.debug("ROUTING KEYS [{}]", applicationInfo.getApp().getRoutingKeys());
            applicationService.createApplication(applicationInfo, CurrentUser.getUsername());
            status.setComplete();
            logger.debug("Sending email to [{}] upon successful application creation", CurrentUser.getUsername());
            applicationService.sendSuccessMail(applicationInfo, CurrentUser.getUsername());
            createAppCompleteAuditLog(applicationInfo, contentProvider, request, AuditEntity.ActionStatus.SUCCESS);
            return "/save/saveSuccess";
        } catch (ClientWebApplicationException e) {
            logger.error("Error while processing the rerquest, Server error : ", e);
            createAppCompleteAuditLog(applicationInfo, contentProvider, request, AuditEntity.ActionStatus.FAILED);
            return "redirect:/error.html?messageId=view.server.error";
        } catch (OrcaException e) {
            logger.error("Error in sdp  : ", e);
            createAppCompleteAuditLog(applicationInfo, contentProvider, request, AuditEntity.ActionStatus.FAILED);
            return "redirect:/error.html?messageId=view.sdp.app.creation.error";
        } catch (Throwable e) {
            logger.error("Error occurred while saving the application details : ", e);
            createAppCompleteAuditLog(applicationInfo, contentProvider, request, AuditEntity.ActionStatus.FAILED);
            return "redirect:/error.html?messageId=save.application.error";
        } finally {
            logger.debug("[APPLICATION_CREATION_COMPLETED][user={}]", applicationInfo.getApp().getAppName());

        }
    }

    /**
     * Handle the cancelation of creating a app and redirect to the home page
     * @param applicationInfo - contain all the information regarding the application
     * @return
     */
    @RequestMapping(value = "/create/cancel", method = RequestMethod.GET)
    public String cancel(@ModelAttribute("app") ApplicationInfo applicationInfo, SessionStatus status) {
        status.setComplete();
        return "redirect:/home.html";
    }

    /**
     * redirect to the createKeyword
     * @param applicationInfo - contain all the information regarding the application
     * @return - Model of creating a keyword
     */
    @RequestMapping(value = "/create/createKeyword")
    public String createKeyword(@ModelAttribute("app") ApplicationInfo applicationInfo,@RequestParam("operator") String operator) {
        return "redirect:../operator.html?operator="+operator;
    }

    @ModelAttribute("app")
    protected Object formBackingObject() throws Throwable {
        try {
            return applicationInfoBuilder.getInitialApplicationInfo();
        } catch (Throwable e) {
            logger.error("Error occurred while loading the initial data for the application creation page : ", e);
            return "redirect:/error.html?messageId=create.application.loding.error";
        }
    }

    private void createAppAccessAuditLog(String appType, ContentProvider contentProvider, HttpServletRequest request,
                                         AuditEntity.ActionStatus status) {
        audit(new AuditEntity.AuditEntityBuilder(contentProvider)
                .populateFromHttpRequest(request)
                .action(String.format(auditLogAction.getCreateAppAccessAction(), appType))
                .description(String.format(auditLogAction.getCreateAppAccessActionDesc(), appType))
                .status(status)
                .build());
    }

    private void createAppCompleteAuditLog(ApplicationInfo appInfo, ContentProvider contentProvider, HttpServletRequest request,
                                         AuditEntity.ActionStatus status) {
        String info = "";
        if (!(appInfo == null || appInfo.getApp() == null || appInfo.getApp().getAppName() == null)) {
            info = String.format("%s-%s-%s", appInfo.getApp().getAppId(), appInfo.getApp().getAppName(), appInfo.getServiceType());
        }
        audit(new AuditEntity.AuditEntityBuilder(contentProvider)
                .populateFromHttpRequest(request)
                .action(auditLogAction.getCreateAppCompleteAction())
                .description(String.format(auditLogAction.getCreateAppCompleteActionDesc(), info))
                .status(status)
                .build());
    }



}