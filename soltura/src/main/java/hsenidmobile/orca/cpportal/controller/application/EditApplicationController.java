/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hsenidmobile.orca.cpportal.controller.application;

import hsenidmobile.orca.core.applications.ApplicationImpl;
import hsenidmobile.orca.core.applications.PeriodUnit;
import hsenidmobile.orca.core.applications.alert.AlertService;
import hsenidmobile.orca.core.applications.exception.ApplicationException;
import hsenidmobile.orca.core.applications.request.RequestService;
import hsenidmobile.orca.core.applications.subscription.DefaultDispatchConfiguration;
import hsenidmobile.orca.core.applications.subscription.Periodicity;
import hsenidmobile.orca.core.applications.subscription.Subscription;
import hsenidmobile.orca.core.applications.voting.VotingResponseSpecification;
import hsenidmobile.orca.core.applications.voting.VotingService;
import hsenidmobile.orca.core.applications.voting.impl.NoVotingResponseSpecification;
import hsenidmobile.orca.core.model.RoutingKey;
import hsenidmobile.orca.core.model.SubCategory;
import hsenidmobile.orca.core.repository.SdpApplicationRepository;
import hsenidmobile.orca.cpportal.controller.AbstractBaseController;
import hsenidmobile.orca.cpportal.module.ApplicationInfo;
import hsenidmobile.orca.cpportal.module.KeywordDetails;
import hsenidmobile.orca.cpportal.module.ResponseSpecification;
import hsenidmobile.orca.cpportal.service.CurrentUser;
import hsenidmobile.orca.cpportal.service.impl.ContentProviderService;
import hsenidmobile.orca.cpportal.service.impl.application.ApplicationService;
import hsenidmobile.orca.cpportal.service.mock.SdpRoutingKeyRepository;
import hsenidmobile.orca.cpportal.util.Json2Java;
import hsenidmobile.orca.cpportal.util.ServiceTypes;
import hsenidmobile.orca.cpportal.validator.application.ApplicationValidator;
import hsenidmobile.orca.rest.sdp.routing.transport.SdpRkManageRequestSender;
import org.apache.cxf.jaxrs.client.ClientWebApplicationException;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;

import java.util.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
@SessionAttributes("app")
@Controller
public class EditApplicationController extends AbstractBaseController {

    private static final Logger logger = LoggerFactory.getLogger(EditApplicationController.class);
    @Autowired
    private ApplicationService applicationService;
    @Autowired
    private ApplicationValidator applicationValidator;
    @Autowired
    private SdpApplicationRepository sdpApplicationRepository;
    @Autowired
    private ContentProviderService contentProviderService;
    @Autowired
    protected SdpRkManageRequestSender sdpRkManageRequestSender;

    /**
     * Retrieve the application information and set the applicationInfo details according to those
     * (use sdpRoutingKeyRepository to retrieve the routing key informration)
     *
     * @param applicationInfo - contain all the information regarding the application
     * @param appId           - application id that need to be edited
     * @param status          - status of the application
     * @return - redirect to the relevent application edit handler
     */
    @RequestMapping(value = "/edit/editAppDetails/{appId}", method = RequestMethod.GET)
    public String appDetails(@ModelAttribute("app") ApplicationInfo applicationInfo, @PathVariable String appId,
                             SessionStatus status) throws Throwable {
        try {
            String userName = CurrentUser.getUsername();
            if (!contentProviderService.isAllowedToAccess(userName, appId)) {
                return handleUnAuthorizedUserAccess(userName, appId);
            }
            ApplicationImpl app = (ApplicationImpl) applicationService.findAppById(appId);
            applicationInfo.setApp(app);
            String stDate = dateFormat.format(app.getStartDate().toDate());
            String eDate = dateFormat.format(app.getEndDate().toDate());
            applicationInfo.setStartDate(stDate);
            applicationInfo.setEndDate(eDate);
            applicationInfo.setSubCategoryRequired(app.getService().isSubCategoriesSupported());
            logger.debug("Initial data binding for : applicationId [{}] startDate [{}] endDate [{}]",
                    new Object[]{appId, applicationInfo.getStartDate(), applicationInfo.getEndDate()});
            setRkInfo(app.getAppName(), applicationInfo);
            setAvailableSubscriptionTypes(applicationInfo);
            setAdvancesSettings(appId, applicationInfo);

        } catch (ClientWebApplicationException e) {
            logger.error("Error while processing the rerquest, Server error : ", e);
            return "redirect:/error.html?messageId=view.server.error";
        } catch (Throwable e) {
            logger.error("Error while setting up initial data for edit application : ", e);
            return "redirect:/error.html?messageId=view.application.error";
        }
        logger.debug("ApplicationInfo Details created : [{}]", applicationInfo);
        if (applicationInfo.getApp().getService() instanceof Subscription) {
            return "redirect:/edit/subscription.html";
        } else if (applicationInfo.getApp().getService() instanceof VotingService) {
            return "redirect:/edit/voting.html";
        } else if (applicationInfo.getApp().getService() instanceof AlertService) {
            return "redirect:/edit/alert.html";
        } else if (applicationInfo.getApp().getService() instanceof RequestService) {
            return "redirect:/edit/request.html";
        }
        status.setComplete();
        return "redirect:/home.html";
    }

    private void setRkInfo(String appName, ApplicationInfo applicationInfo) throws Throwable {
        List<String> assignedRkValues = SdpRoutingKeyRepository.getSelectedRoutingKeyListById(appName);
        applicationInfo.setAssignedRoutingKeyValues(assignedRkValues);
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("sp-id", contentProviderService.getContentProviderDetails(CurrentUser.getUsername()).getCpId());
        Map<String, Map<String, List<String>>> findRksResponse = sdpRkManageRequestSender.findAvailableRks(
                parameters);
        HashMap<String, List<RoutingKey>> availableRoutingKeyValues = (
                HashMap<String, List<RoutingKey>>) Json2Java.getAvailableRoutingKeys(findRksResponse).clone();

        for (int i = 0; i < applicationInfo.getAssignedRoutingKeyValues().size(); i++) {
            String[] split = applicationInfo.getAssignedRoutingKeyValues().get(i).split(" - ");
            String operator = split[0];
            List<RoutingKey> RoutngKeyList = availableRoutingKeyValues.remove(operator);
        }
        applicationInfo.setAvailableRoutingKeyValues(availableRoutingKeyValues);
        applicationInfo.setSelectedRoutingKeyValues(new ArrayList<String>(
                applicationInfo.getAssignedRoutingKeyValues().size()));
    }

    private void setAdvancesSettings(String appId, ApplicationInfo appInfo)
            throws ApplicationException {
        final ApplicationImpl app = appInfo.getApp();
        appInfo.setSubscriptionSuccessMsgEn(app.getSubscriptionResponse(Locale.ENGLISH));
        appInfo.setUnsubscribeSuccessMsgEn(app.getUnsubscribeResponse(Locale.ENGLISH));
//        Removed Invalid Request Message
//        appInfo.setRequestErrorMsgEn(app.getInvalidRequestErrorMessage(Locale.ENGLISH));

        //todo integrate charging setting with provisioning api

    }

    @RequestMapping(value = "/edit/type/{ncsType}")
    public String createApplicationType(@PathVariable String ncsType,
                                        @ModelAttribute("app") ApplicationInfo applicationInfo) {
        return "createApplication/appDetails";
    }

    @RequestMapping(value = "/edit/voting")
    public String editVoting(@ModelAttribute("app") ApplicationInfo applicationInfo, BindingResult bindingResults)
            throws Throwable {
        try {
            if (applicationInfo.getApp() == null) {
                return "redirect:/home.html";
            }
            VotingService votingService = (VotingService) applicationInfo.getApp().getService();
            List<SubCategory> subCategories = votingService.getSupportedSubCategories();
            logger.debug("Supported Keywords when Editing : [{}]", subCategories);
            applicationInfo.setServiceType(ServiceTypes.VOTING.name());
            setKeywordDetails(applicationInfo, subCategories);

            return "createApplication/appDetails";
        } catch (Throwable e) {
            logger.error("Error occurred while editing the voting application :", e);
            return "redirect:/error.html?messageId=edit.voting.app.error";
        }
    }

    @RequestMapping(value = "/edit/alert")
    public String editAlert(@ModelAttribute("app") ApplicationInfo applicationInfo) {
        try {
            AlertService alertService = (AlertService) applicationInfo.getApp().getService();
            List<SubCategory> subCategories = alertService.getSupportedSubCategories();
            setKeywordDetails(applicationInfo, subCategories);
            applicationInfo.setServiceType(ServiceTypes.ALERT.name());
            applicationInfo.setSmsUpdate(((AlertService) applicationInfo.getApp().getService()).isSmsUpdateRequired());

        } catch (RuntimeException e) {
            logger.error("Error occurred while editing the alert application :", e);
            return "redirect:/error.html?messageId=edit.alert.app.error";
        }
        return "createApplication/appDetails";
    }

    @RequestMapping(value = "/edit/subscription")
    public String editSubscription(@ModelAttribute("app") ApplicationInfo applicationInfo) throws Throwable {
        try {
            Subscription subscription = (Subscription) applicationInfo.getApp().getService();
            List<SubCategory> keywords = subscription.getSupportedSubCategories();
            setKeywordDetails(applicationInfo, keywords);
            applicationInfo.setServiceType(ServiceTypes.SUBSCRIPTION.name());
            applicationInfo.setPeriodUnitList(addPeriodUnits());
            Periodicity periodicity = subscription.getSupportedPeriodicities().get(0);

            final PeriodUnit periodicUnit = periodicity.getUnit();

            updateDispatchData(applicationInfo, periodicity, periodicUnit);
            applicationInfo.setSmsUpdate(((Subscription) applicationInfo.getApp().getService()).isSmsUpdateRequired());
        } catch (Throwable e) {
            logger.error("Error occurred while editing the subscription application :", e);
            return "redirect:/error.html?messageId=edit.subscription.app.error";
        }
        return "createApplication/appDetails";
    }

    private void updateDispatchData(ApplicationInfo applicationInfo, Periodicity periodicity, PeriodUnit periodicUnit) {
        applicationInfo.setSelectedPeriodUnit(periodicUnit);
        DefaultDispatchConfiguration dispatchConfiguration = periodicity.getDefaultDispatchConfiguration();
        applicationInfo.setSubscriptionDispatchDate(dispatchConfiguration.getDayOfMonth());
        applicationInfo.setSubscriptionDispatchDay(dispatchConfiguration.getDayOfWeek());
        applicationInfo.setSubscriptionDispatchHour(dispatchConfiguration.getHourOfDay());
        applicationInfo.setSubscriptionDispatchMinute(dispatchConfiguration.getMinuteOfHour());
//        Removed dispatch buffer time field from the form
//        applicationInfo.setSubscriptionDispatchBufferTime(periodicity.getAllowedBufferTime());
    }

    /**
     * return the application creation model enabling the related details
     *
     * @return - Model of application creation
     */
    @RequestMapping(value = "/edit/request")
    public String editRequest(@ModelAttribute("app") ApplicationInfo applicationInfo) {
        try {
            RequestService requestService = (RequestService) applicationInfo.getApp().getService();
            List<SubCategory> subCategories = requestService.getSupportedSubCategories();
            setKeywordDetails(applicationInfo, subCategories);
            applicationInfo.setServiceType(ServiceTypes.REQUEST.name());
            applicationInfo.setRequestAppResponseMsg(requestService.getResponseMessages().get(0).getMessage());

            return "createApplication/appDetails";
        } catch (RuntimeException e) {
            logger.error("Error occurred while editing the request application :", e);
            return "redirect:/error.html?messageId=edit.request.app.error";
        }
    }

    @RequestMapping(value = "/edit/ncs")
    public String createApplicationNcs(@ModelAttribute("app") ApplicationInfo applicationInfo, SessionStatus status) {
        status.setComplete();
        return "redirect:../manage/myApplications.html";
    }

    /**
     * Direct to the Edit Application page
     *
     * @param applicationInfo - contain all the information regarding the application
     */
    @RequestMapping(value = "/edit/appDetails")
    public String editApp(@ModelAttribute("app") ApplicationInfo applicationInfo, BindingResult bindingResult)
            throws Throwable {
        try {
            applicationValidator.applicationValidate(applicationInfo, bindingResult);
            if (bindingResult.hasErrors()) {
                return "createApplication/appDetails";
            }
            if (applicationInfo.getApp().getService() instanceof Subscription) {
                if (!applicationInfo.getApp().getService().isSubCategoriesSupported()) {
                    setChannelAppConfigurations(applicationInfo);
                    return "redirect:confirm.html";
                }
                return "createApplication/appType/createSubscriptionApp";
            } else if (applicationInfo.getApp().getService() instanceof VotingService) {
                final VotingService service = (VotingService) applicationInfo.getApp().getService();
                applicationInfo.setOnePerNumber(service.
                        isOneVotePerNumber());
                setVotingResponseSpecification(applicationInfo, service);
                return "createApplication/appType/createVotingApp";
            } else if (applicationInfo.getApp().getService() instanceof AlertService) {
                if (!applicationInfo.getApp().getService().isSubCategoriesSupported()) {
                    setAlertAppConfigurations(applicationInfo);
                    return "redirect:confirm.html";
                }
                return "createApplication/appType/createAlertApp";
            } else if (applicationInfo.getApp().getService() instanceof RequestService) {
                if (!applicationInfo.getApp().getService().isSubCategoriesSupported()) {
                    setRequestShowAppConfigurations(applicationInfo);
                    return "redirect:confirm.html";
                }
                return "createApplication/appType/createRequestApp";
            }
            return "redirect:/home.html";
        } catch (Throwable e) {
            logger.error("Error occurred while redirecting to the sub category detail page in application edit flow : ", e);
            return "redirect:/error.html?messageId=redirect.subcategory.page.error";
        }
    }

    private void setVotingResponseSpecification(ApplicationInfo applicationInfo, VotingService votelet) {
        final VotingResponseSpecification specification = votelet.getResponseSpecification();
        if (specification instanceof NoVotingResponseSpecification) {
            applicationInfo.setSelectedResponseSpecification(ResponseSpecification.NO_RESPOND.toString());
        } else {
            applicationInfo.setSelectedResponseSpecification(ResponseSpecification.SUMMERY_RESPOND.toString());
        }
    }

    /**
     * Validate Edited voting application
     */
    @RequestMapping(value = "/edit/validateVoting")
    public String validateVotingAppSpecificInfo(@ModelAttribute("app") ApplicationInfo applicationInfo,
                                                BindingResult bindingResults) throws Throwable {
        try {
            logger.debug("Edit Voting Application with : [{}]", applicationInfo);
            final VotingService service = (VotingService) applicationInfo.getApp().getService();
            service.setOneVotePerNumber(applicationInfo.isOnePerNumber());
            applicationValidator.votingValidate(applicationInfo, bindingResults);
            if (bindingResults.hasErrors()) {
                return "createApplication/appType/createVotingApp";
            }
            return "confirm/confirmCreateVotingApp";
        } catch (Throwable e) {
            logger.error("Error occurred while redirecting to the confirm page in application edit flow : ", e);
            return "redirect:/error.html?messageId=redirect.confirm.page.error";
        }
    }

    /**
     * Validate Edited channel application
     */
    @RequestMapping(value = "/edit/validateChannel")
    public String validateChannelAppSpecificInfo(@ModelAttribute("app") ApplicationInfo applicationInfo,
                                                 BindingResult bindingResults) throws Throwable {
        try {
            logger.debug("Edit Subscription Application with : [{}]", applicationInfo);
            setChannelAppConfigurations(applicationInfo);
            applicationValidator.subscriptionValidate(applicationInfo, bindingResults);
            if (bindingResults.hasErrors()) {
                return "createApplication/appType/createSubscriptionApp";
            }
            return "confirm/confirmCreateSubscriptionApp";
        } catch (Throwable e) {
            logger.error("Error occurred while redirecting to the confirm page in application edit flow : ", e);
            return "redirect:/error.html?messageId=redirect.confirm.page.error";
        }
    }

    private void setChannelAppConfigurations(ApplicationInfo applicationInfo) {
        final Subscription subscription = (Subscription) applicationInfo.getApp().getService();
        subscription.setSmsUpdateRequired(applicationInfo.isSmsUpdate());
        Periodicity periodicity = subscription.getSupportedPeriodicities().get(0);
//        Removed dispatch buffer time field from the form
//        periodicity.setAllowedBufferTime(applicationInfo.getSubscriptionDispatchBufferTime());
        periodicity.setAllowedBufferTime(0);
        periodicity.setDefaultDispatchConfiguration(applicationService.configureDispatchTime(applicationInfo));
        periodicity.setFirstScheduledDispatchTime(new DateTime(Periodicity.getFistDispatchableDate(
                periodicity.getUnit(), 1, periodicity.getDefaultDispatchConfiguration())));
    }

    /**
     * Validate Edited Validate application
     */
    @RequestMapping(value = "/edit/validateAlert")
    public String validateAlertAppSpecificInfo(@ModelAttribute("app") ApplicationInfo applicationInfo,
                                               BindingResult bindingResults) throws Throwable {
        try {
            logger.debug("Edit Alert Application with : [{}]", applicationInfo);
            setAlertAppConfigurations(applicationInfo);
            applicationValidator.alertValidate(applicationInfo, bindingResults);
            if (bindingResults.hasErrors()) {
                return "createApplication/appType/createAlertApp";
            }
            return "confirm/confirmCreateAlertApp";
        } catch (Throwable e) {
            logger.error("Error occurred while redirecting to the confirm page in application edit flow : ", e);
            return "redirect:/error.html?messageId=redirect.confirm.page.error";
        }
    }

    private void setAlertAppConfigurations(ApplicationInfo applicationInfo) {
        final AlertService alertService = (AlertService) applicationInfo.getApp().getService();
        alertService.setSmsUpdateRequired(applicationInfo.isSmsUpdate());
    }

    /**
     * Validate Edited Request application
     */
    @RequestMapping(value = "/edit/validateReq")
    public String validateRequestAppSpecificInfo(@ModelAttribute("app") ApplicationInfo applicationInfo,
                                                 BindingResult bindingResults) throws Throwable {
        setRequestShowAppConfigurations(applicationInfo);
        logger.debug("Edit Request Application with : [{}]", applicationInfo);
        applicationValidator.requestValidate(applicationInfo, bindingResults);
        if (bindingResults.hasErrors()) {
            return "createApplication/appType/createRequestApp";
        }
        return "confirm/confirmCreateRequestApp";
    }

    private void setRequestShowAppConfigurations(ApplicationInfo applicationInfo) {
        RequestService requestService = (RequestService) applicationInfo.getApp().getService();
        requestService.getResponseMessages().get(0).setMessage(applicationInfo.getRequestAppResponseMsg());
    }

    private List<KeywordDetails> getNewlyAddedSubCategory(List<KeywordDetails> selectedValues) {
        List<KeywordDetails> newSubCategory = new ArrayList<KeywordDetails>();
        logger.info("Newly added keyword : [{}]", selectedValues);
        if (selectedValues != null && !selectedValues.isEmpty()) {
            for (KeywordDetails subKeywordEntry : selectedValues) {
                if (!subKeywordEntry.getKeyword().trim().isEmpty()) {
                    logger.debug("Sub Keyword Entry [{}]", subKeywordEntry);
                    newSubCategory.add(subKeywordEntry);
                }
            }
        } else {
            logger.debug("This application does not contain any newly added subcategories");
        }
        return newSubCategory;
    }

    /**
     * Save the Edited application (use the routingRepository to store the routing key info)
     *
     * @return - save success
     */
    @RequestMapping(value = "/edit/save")
    public String save(@ModelAttribute("app") ApplicationInfo applicationInfo, SessionStatus status) throws Throwable {
        try {
            SdpRoutingKeyRepository.insertNewSelecteditem(
                    applicationInfo.getApp().getAppName(), applicationInfo.getSelectedRoutingKeyValues());
            if (applicationInfo == null || applicationInfo.getApp() == null
                    || applicationInfo.getApp().getAppName() == null) {
                return "/save/saveSuccess";
            }
            final ApplicationImpl app = applicationInfo.getApp();
            final Date startDate = dateFormat.parse(applicationInfo.getStartDate() + " 00:00:00");
            app.setStartDate(new DateTime(startDate.getTime()));
            app.setEndDate(new DateTime(dateFormat.parse(applicationInfo.getEndDate() + " 23:59:59").getTime()));
            logger.debug("Setting application new start time to [{}] endDate [{}]",
                    new Object[]{applicationInfo.getStartDate(), applicationInfo.getEndDate()});
            app.setDescription(app.getDescription().replaceAll("\r\n", " "));

            addNewSubCatageries(applicationInfo);

            applicationService.editApplication(applicationInfo);
            logger.info("Application Successfully updated!!!!");
            status.setComplete();
            return "/save/saveSuccess";
        } catch (ClientWebApplicationException e) {
            logger.error("Error while processing the rerquest, Server error : ", e);
            return "redirect:/error.html?messageId=view.server.error";
        } catch (Throwable e) {
            logger.error("Error occurred while saving the editted application details : ", e);
            return "redirect:/error.html?messageId=save.application.error";
        }
    }

    /**
     * Display the confirmation page according to the application type
     *
     * @return - confirmation page
     */
    @RequestMapping(value = "/edit/confirm")
    public String confirm(@ModelAttribute("app") ApplicationInfo applicationInfo, BindingResult bindingResults)
            throws Throwable {
        try {
            if (applicationInfo.getApp().getService() instanceof Subscription) {
                return "confirm/confirmCreateSubscriptionApp";
            } else if (applicationInfo.getApp().getService() instanceof VotingService) {
                return "confirm/confirmCreateVotingApp";
            } else if (applicationInfo.getApp().getService() instanceof AlertService) {
                return "confirm/confirmCreateAlertApp";
            } else if (applicationInfo.getApp().getService() instanceof RequestService) {
                return "confirm/confirmCreateRequestApp";
            }
            return "redirect:/home.html";
        } catch (Throwable e) {
            logger.error("Error occurred while redirecting to the confirm page in application edit flow : ", e);
            return "redirect:/error.html?messageId=redirect.confirm.page.error";
        }
    }

    private void addNewSubCatageries(ApplicationInfo applicationInfo) {
        if (applicationInfo.getApp().getService() instanceof Subscription) {
            final Subscription subscription = ((Subscription) applicationInfo.getApp().getService());
            subscription.getSupportedSubCategories().addAll(applicationService
                    .setKeywordDetails(getNewlyAddedSubCategory(applicationInfo.getSelectedValues())));
        } else if (applicationInfo.getApp().getService() instanceof AlertService) {
            final AlertService alertService = ((AlertService) applicationInfo.getApp().getService());
            alertService.getSupportedSubCategories().addAll(applicationService.setKeywordDetails(
                    getNewlyAddedSubCategory(applicationInfo.getSelectedValues())));
        } else if (applicationInfo.getApp().getService() instanceof VotingService) {
            final VotingService votingService = ((VotingService) applicationInfo.getApp().getService());
            votingService.getSupportedSubCategories().addAll(
                    applicationService.setKeywordDetails(getNewlyAddedSubCategory(applicationInfo.getSelectedValues())));
        } else if (applicationInfo.getApp().getService() instanceof RequestService) {
            final RequestService requestService = ((RequestService) applicationInfo.getApp().getService());
            requestService.getSupportedSubCategories().addAll(applicationService.setKeywordDetails(
                    getNewlyAddedSubCategory(applicationInfo.getSelectedValues())));
        }
    }

    /**
     * Cancelation Handler for Edit application
     *
     * @param applicationInfo - contain all the information regarding the application
     * @param status          - status of the application
     * @return - redirect to the home
     */
    @RequestMapping(value = "/edit/cancel", method = RequestMethod.GET)
    public String cancel(@ModelAttribute("registration") ApplicationInfo applicationInfo, SessionStatus status) {
        status.setComplete();
        return "redirect:/home.html";
    }

    @ModelAttribute("app")
    protected Object formBackingObject() throws Exception {
        ApplicationInfo appInfo = new ApplicationInfo();
        appInfo.setEditState(true);
        appInfo.setReadOnly(false);
        return appInfo;
    }

    private void setKeywordDetails(ApplicationInfo applicationInfo, List<SubCategory> categories) {
        ArrayList<KeywordDetails> keywordDetailArrayList = new ArrayList<KeywordDetails>();
        if (applicationInfo.getApp().getService().isSubCategoriesSupported()) {
            for (SubCategory entry : categories) {
                KeywordDetails detail = new KeywordDetails();
                detail.setKeyword(entry.getKeyword());
                detail.setDescriptionEn(entry.getDescription(Locale.ENGLISH).getMessage());
                keywordDetailArrayList.add(detail);
            }
            logger.debug("Keyword Details List when Editing: [{}]", keywordDetailArrayList);
            applicationInfo.setExistingSelectedValues(keywordDetailArrayList);
            applicationInfo.setSelectedValues(getInitialKeywords(keywordDetailArrayList));
        } else {
            //todo  handle this in the ui
        }
    }
}