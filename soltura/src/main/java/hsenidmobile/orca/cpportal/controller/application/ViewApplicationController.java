/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hsenidmobile.orca.cpportal.controller.application;

import hsenidmobile.nettyclient.MessageSendingFailedException;
import hsenidmobile.orca.core.applications.ApplicationImpl;
import hsenidmobile.orca.core.applications.ContentDispatchSummary;
import hsenidmobile.orca.core.applications.Service;
import hsenidmobile.orca.core.applications.alert.AlertService;
import hsenidmobile.orca.core.applications.exception.ApplicationException;
import hsenidmobile.orca.core.applications.request.RequestService;
import hsenidmobile.orca.core.applications.subscription.Subscription;
import hsenidmobile.orca.core.applications.subscription.SubscriptionServiceDataRepository;
import hsenidmobile.orca.core.applications.voting.VoteResultSummary;
import hsenidmobile.orca.core.applications.voting.VotingService;
import hsenidmobile.orca.core.flow.dispatch.aventuraapi.AventuraApiMtMessageDispatcher;
import hsenidmobile.orca.core.model.*;
import hsenidmobile.orca.core.model.message.AventuraSmsMessage;
import hsenidmobile.orca.core.model.message.RequestMessage;
import hsenidmobile.orca.core.model.message.RequestShowReplyRequest;
import hsenidmobile.orca.core.repository.AlertDispatchedContentRepository;
import hsenidmobile.orca.core.repository.SdpApplicationRepository;
import hsenidmobile.orca.core.services.impl.InvalidRequestServiceImpl;
import hsenidmobile.orca.core.util.CorrelationIdGenerationService;
import hsenidmobile.orca.core.util.PropertyHolder;
import hsenidmobile.orca.cpportal.controller.AbstractBaseController;
import hsenidmobile.orca.cpportal.module.KeywordDetails;
import hsenidmobile.orca.cpportal.module.MessageHistoryInfo;
import hsenidmobile.orca.cpportal.module.ViewApp;
import hsenidmobile.orca.cpportal.service.CurrentUser;
import hsenidmobile.orca.cpportal.service.impl.ContentProviderService;
import hsenidmobile.orca.cpportal.util.ServiceTypes;
import hsenidmobile.orca.cpportal.util.client.HttpRequestSender;
import hsenidmobile.orca.cpportal.util.logging.AuditEntity;
import hsenidmobile.orca.cpportal.util.logging.AuditLogActionProperties;
import hsenidmobile.orca.orm.repository.*;
import hsenidmobile.orca.rest.common.Response;
import hsenidmobile.orca.rest.sdp.app.transport.ProvAppManageRequestSender;
import hsenidmobile.orca.rest.sdp.subscriber.transport.SubscriptionDetailRequestSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeoutException;

import static hsenidmobile.orca.cpportal.util.logging.SolturaAuditLogger.audit;
import static hsenidmobile.orca.rest.common.util.RestApiKeys.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
@Controller
@SessionAttributes("viewApp")
public class ViewApplicationController extends AbstractBaseController {

    private static final Logger logger = LoggerFactory.getLogger(ViewApplicationController.class);
    @Autowired
    private InvalidRequestServiceImpl invalidRequestService;
    @Autowired
    private AppRepositoryImpl appRepository;
    @Autowired
    private SubscriptionServiceDataRepositoryImpl subscriptionDataRepository;
    @Autowired
    private VoteRepositoryImpl voteRepositoryImpl;
    @Autowired
    private RequestServiceDataRepositoryImpl requestServiceDataRepository;
    @Autowired
    private MessageDispatchStatRepositoryImpl messageDispatchStatRepository;
    @Autowired
    private SdpApplicationRepository sdpApplicationRepository;
    @Autowired
    AlertDispatchedContentRepository alertDispatchedContentRepository;
    @Autowired
    SubscriptionServiceDataRepository subscriptionServiceDataRepository;
    @Autowired
    private ContentProviderService contentProviderService;
    @Autowired
    protected SubscriptionDetailRequestSender subscriptionDetailRequestSender;
    @Autowired
    private ProvAppManageRequestSender provAppManageRequestSender;
    @Autowired
    private ContentProviderDetailsRepositoryImpl cpDetailsRepository;
    @Autowired
    private AuditLogActionProperties auditLogAction;

    private static final SimpleDateFormat dateFormater = new SimpleDateFormat("dd/MM/yyyy");
    private static final SimpleDateFormat dateTimeFormater = new SimpleDateFormat("dd-MM-yy HH:mm:ss");

    @RequestMapping(value = "/reports/viewReport/{appId}/{pageNumber}", method = RequestMethod.GET)
    public String appDetails(@ModelAttribute("viewApp") ViewApp viewApp, @PathVariable("appId") String appId,
                             @PathVariable("pageNumber") String pageNumber, HttpServletRequest request) throws Exception {
        Application app = appRepository.findAppByAppId(appId);
        ContentProvider cp = null;
        viewApp.setAppId(appId);
        try  {
            Map<String, String> parameters = new HashMap<String, String>();
            cp = cpDetailsRepository.getContentProvider();
            parameters.put(SP_ID, cp.getSpId());
            SdpAppLoginData sdpAppData = null;
            sdpAppData = sdpApplicationRepository.getSdpAppLoginDataForOrcaApp(app.getAppId());
            if (sdpAppData != null) {
                logger.debug("SDP app id [{}]", sdpAppData.getAppId());
                parameters.put(APP_ID, sdpAppData.getAppId());

                Map<String, Object> provisionedApp = provAppManageRequestSender.getApp(parameters);
                viewApp.setExpirable(Boolean.parseBoolean(String.valueOf(provisionedApp.get(EXPIRE))));
                Map<String, Object> ncsSlas = (Map<String, Object>) provisionedApp.get(NCS_SLAS);
                List<Map<String, String>> rkList = new ArrayList<Map<String, String>>();
                for (Map.Entry<String, Object> entry : ncsSlas.entrySet()) {
                    Map<String, Object> ncsSlaMap = (Map<String, Object>) entry.getValue();
                    String userName = CurrentUser.getUsername();
                    if (!contentProviderService.isAllowedToAccess(userName, appId)) {
                        return handleUnAuthorizedUserAccess(userName, appId);
                    }
                    Service service = app.getService();
                    if (service instanceof VotingService) {
                        logger.debug("Retriving data for voting application : appId [{}]", appId);
                        VotingService votingservice = (VotingService) service;
                        ReceivedMoSummary receivedMoSummary = appRepository.getMoSummary(appId);
                        //todo vote result summary by service id earlier it was votelet id
                        final Long serviceId = votingservice.getId();
                        List<VoteResultSummary> voteResults = voteRepositoryImpl.getVotingSummary(serviceId);
                        logger.debug("finding votlet summery for votletId [{}] voteResults size [{}]", new Object[]{
                                serviceId, voteResults.size()});
                        viewApp.setLastUpdated(dateFormater.format(new Date()));
                        viewApp.setReceivedMsgCount(receivedMoSummary.getMoCount());
                        setVotingUsage(viewApp, voteResults, app);
                        setVotingApplicationSettings(appId, viewApp, votingservice);
                        viewApp.setNoOfInavlidRequests(viewApp.getReceivedMsgCount() - viewApp.getTotalSuccssfulVoteCount());
                        viewApp.setServiceType(ServiceTypes.VOTING.toString());
                    } else if (service instanceof Subscription) {
                        Subscription subscription = (Subscription) service;
                        setSubscriptionApplicationSettings(appId, viewApp, subscription);
                        viewApp.setServiceType(ServiceTypes.SUBSCRIPTION.toString());
                        viewApp.setSmsUpdate(subscription.isSmsUpdateRequired());
                        viewApp.setMessageFrequency(subscription.getSupportedPeriodicities().get(0).getUnit().name());
                        getNumOfTotalRegRequests(provisionedApp.get(APP_ID).toString(), provisionedApp.get(PASSWORD)
                                .toString(), viewApp);
                    } else if (service instanceof AlertService) {
                        AlertService alertService = (AlertService) service;
                        List<SubCategory> keywordDetails = alertService.getSupportedSubCategories();
                        viewApp.setKeywordDetails(keywordDetails);
                        setAlertApplicationSettings(appId, viewApp, alertService);
                        viewApp.setNoOfInavlidRequests(viewApp.getReceivedMsgCount() - viewApp.getTotalSuccssfulVoteCount());
                        viewApp.setServiceType(ServiceTypes.ALERT.toString());
                        viewApp.setSmsUpdate(alertService.isSmsUpdateRequired());
                        getNumOfTotalRegRequests(provisionedApp.get(APP_ID).toString(), provisionedApp.get(PASSWORD)
                                .toString(), viewApp);
                    } else if (service instanceof RequestService) {
                        Map<String, Long> summaryMap = new HashMap<String, Long>();
                        RequestService requestService = (RequestService) service;
                        setRequestApplicationSettings(appId, viewApp, requestService, summaryMap);
                        viewApp.setRequestServiceId(requestService.getId());
                        viewApp.setTotalNumberOfRecords(6);
                        viewApp.setPageNumber(pageNumber);
                        viewApp.setServiceType(ServiceTypes.REQUEST.toString());
                        viewApp.setRequestServiceResponseMessage(requestService.getResponseMessages().iterator().
                                next().getMessage());
                        viewApp.setCommonResponseAvailable(requestService.isCommonResponseAvailable());
                    }
                    Object rkDetails = ncsSlaMap.get(ROUTING_KEYS);
                    if (rkDetails != null) {
                        List<Map<String, String>> rkMap = (List<Map<String, String>>) rkDetails;
                        logger.debug("Returned RK map {}", rkMap.toString() );
                        for (Map<String, String> maps : rkMap) {
                            rkList.add(maps);
                        }
                    } else {
                        logger.debug("No Routing keys found for the given ncs sla [{}]", ncsSlaMap.get(NCS_TYPE));
                    }
                }
                viewAppAuditLog(sdpAppData.getAppId(), cp, request, AuditEntity.ActionStatus.SUCCESS);
                viewApp.setSelectedKeywordList(rkList);
            } else {
                logger.warn("No SDP Application Details found for Orca Application ID [{}] ", appId);
                viewAppAuditLog(appId, cp, request, AuditEntity.ActionStatus.FAILED);
            }
        } catch (Throwable e) {
            logger.error("Error in Retrieving application Details : ", e);
            viewAppAuditLog(appId, cp, request, AuditEntity.ActionStatus.FAILED);
            return "redirect:/error.html?messageId=view.application.error";
        }
        return "/reports/viewReports";
    }

    @RequestMapping(value = "/reports/newReplyMessage")
    public String form(@ModelAttribute("viewApp") ViewApp viewApp,
                       @RequestParam("appId") String appId,
                       @RequestParam("pageNumber") String pageNumber,
                       @RequestParam("messageId") String messageId ) {

        return "redirect:/replyMessage.html?appId="+appId+"&pageNumber="+pageNumber+"&messageId="+messageId;
    }

    @RequestMapping(value = "/reports/viewCharts/{appId}", method = RequestMethod.GET)
    public String charts(@ModelAttribute("viewApp") ViewApp viewApp,
                         @PathVariable String appId) throws Throwable {
        try {
            long total = 0;
            for (VoteResultSummary voteResultSummary : viewApp.getVoteResults()) {
                if (total < voteResultSummary.getTotalVoteCount()) {
                    total = voteResultSummary.getTotalVoteCount();
                    viewApp.setWinnerVoteCount(total);
                    viewApp.setWinner(formatWinnerName(voteResultSummary.getCandidateName(), voteResultSummary.getCandidateCode()));
                } else if (total == voteResultSummary.getTotalVoteCount()) {
                    viewApp.setWinner(viewApp.getWinner() + ", " + formatWinnerName(voteResultSummary.getCandidateName(),
                            voteResultSummary.getCandidateCode()));
                }
            }
            if (total <= 0) {
                viewApp.setWinner(null);
            }
            return "../content/reports/charts";
        } catch (Throwable e) {
            logger.error("Error occurred while accessing the charts to check the winners :", e);
            return "redirect:/error.html?messageId=view.chart.error";
        }
    }

    private String formatWinnerName(String name, String code) {
        return name + "[" + code + "]";
    }

    /**
     * Getting the Total number of Registered Users.
     *
     * @param appId
     * @param password
     * @param viewApp
     */
    private void getNumOfTotalRegRequests(String appId, String password, ViewApp viewApp) {
        try {
            Response response = subscriptionDetailRequestSender.getSubscriberCount(appId, password);
            String key = response.getResponseParameters().get(BASE_SIZE);
            viewApp.setNumOfTotalRegRequest(key);
        }
        catch (Throwable e) {
            logger.error("Error occurred while Requesting the Total number of users Registered :", e);
        }
    }

    private void setVotingApplicationSettings(String appId, ViewApp viewApp, VotingService votingService)
            throws ApplicationException {

        setCommonAppSettings(appId, viewApp);
        viewApp.setNumberOfSubCategories(votingService.getSupportedSubCategories().size());
        viewApp.setSubCategoryCodeList(votingService.getSupportedSubCategories());
        viewApp.setOneVotePerNumber(votingService.isOneVotePerNumber());
    }

    /**
     * This will set the voting application usage details to the view voting application page
     *
     * @param viewApp
     * @param voteResults
     * @param app
     */
    private void setVotingUsage(ViewApp viewApp, List<VoteResultSummary> voteResults, Application app) {
        logger.debug("Setting voting application usage detaills for the application [{}]", app.getAppName());
        setCandidateData(voteResults, app, viewApp);
        long totalVoteCount = 0;
        if (voteResults.iterator().hasNext()) {
            for (VoteResultSummary voteResultSummary : voteResults) {
                totalVoteCount += voteResultSummary.getTotalVoteCount();
            }
        }
        viewApp.setTotalSuccssfulVoteCount(totalVoteCount);
    }

    /**
     * This will set the candidate names and descriptions to the view voting application page
     *
     * @param fetchedResultSummaries
     * @param app
     * @param viewApp
     */
    private void setCandidateData(final List<VoteResultSummary> fetchedResultSummaries, Application app, ViewApp viewApp) {
        logger.debug("Setting candidate name and Descriptions for application [{}] ", app.getAppName());
        List<VoteResultSummary> voteResultSummaries = new ArrayList<VoteResultSummary>();
        if (fetchedResultSummaries != null) {
            for (SubCategory subCategory : app.getService().getSupportedSubCategories()) {
                VoteResultSummary resultSummary = getVoteResultSummary(fetchedResultSummaries, subCategory.getKeyword());
                if (resultSummary == null) {
                    logger.debug("No result summary found for candidate[{}], created blank summary entry", subCategory.getKeyword());
                    voteResultSummaries.add(createBlankVoteResultSummay(subCategory));
                } else {
                    voteResultSummaries.add(resultSummary);
                }
            }
        } else {
            for (SubCategory subCategory : app.getService().getSupportedSubCategories()) {
                voteResultSummaries.add(createBlankVoteResultSummay(subCategory));
            }
        }
        viewApp.setVoteResults(voteResultSummaries);
    }

    private VoteResultSummary createBlankVoteResultSummay(SubCategory subCategory) {
        VoteResultSummary voteResultSummary = new VoteResultSummary();
        voteResultSummary.setCandidateCode(subCategory.getKeyword());
        voteResultSummary.setCandidateName(subCategory.getDescription(Locale.ENGLISH).getMessage());
        return voteResultSummary;
    }

    private VoteResultSummary getVoteResultSummary(List<VoteResultSummary> voteResultSummaries, String keyword) {
        for (VoteResultSummary resultSummay : voteResultSummaries) {
            if (resultSummay.getCandidateCode().equals(keyword)) {
                return resultSummay;
            }
        }

        return null;
    }

    private void setSubscriptionApplicationSettings(String appId, ViewApp viewApp, Subscription service)
            throws ApplicationException {

        setCommonAppSettings(appId, viewApp);
        viewApp.setNumberOfSubCategories(service.getSupportedSubCategories().size());
        viewApp.setSubCategoryCodeList(service.getSupportedSubCategories());
        List<KeywordDetails> subscriptionDetails = new ArrayList<KeywordDetails>();

        if (service.isSubCategoriesSupported()) {
            for (SubCategory subCategory : service.getSupportedSubCategories()) {
                KeywordDetails keywordDetails = createSubscriptionKeywordsForUI(subCategory, appId);

                //todo integrate this with the subscription API
                subscriptionDetails.add(keywordDetails);
            }
        } else {
            KeywordDetails keywordDetails = new KeywordDetails();
            ContentDispatchSummary summary = null;
            try {
                summary = subscriptionServiceDataRepository.getLastSentSubscriptionByApplicationId(appId);
                setContentDispatchSummaryDetails(keywordDetails, summary);
            } catch (ApplicationException e) {
                logger.error("Last sent alert message not found for the app id [{}]", appId);
            }
            subscriptionDetails.add(keywordDetails);
        }
        viewApp.setSubscriptionContentDetails(subscriptionDetails);
    }

    private void setAlertApplicationSettings(String appId, ViewApp viewApp, AlertService service)
            throws ApplicationException {

        setCommonAppSettings(appId, viewApp);
        viewApp.setNumberOfSubCategories(service.getSupportedSubCategories().size());
        viewApp.setSubCategoryCodeList(service.getSupportedSubCategories());
        List<KeywordDetails> alertContentDetails = new ArrayList<KeywordDetails>();

        if (service.isSubCategoriesSupported()) {
            for (SubCategory subCategory : service.getSupportedSubCategories()) {
                KeywordDetails keywordDetails = createKeywordsForUI(subCategory, appId);

                //todo integrate this with the subscription API
                alertContentDetails.add(keywordDetails);
            }
        } else {
            KeywordDetails keywordDetails = new KeywordDetails();
            ContentDispatchSummary summary = null;
            try {
                summary = alertDispatchedContentRepository.getLastSentAlertByApplicationId(appId);
                setContentDispatchSummaryDetails(keywordDetails, summary);
            } catch (ApplicationException e) {
                logger.error("Last sent alert message not found for the app id [{}]", appId);
            }
            alertContentDetails.add(keywordDetails);
        }
        viewApp.setAlertContentDetails(alertContentDetails);
    }

    private void setContentDispatchSummaryDetails(KeywordDetails keywordDetails, ContentDispatchSummary summary) {
        keywordDetails.setDescriptionEn(summary.getLastSentContent());
        keywordDetails.setLastSentOn(dateTimeFormater.format(summary.getLastSentOnTime().toDate()));
    }

    private void setRequestApplicationSettings(String appId, ViewApp viewApp, RequestService requestService,
                                               Map<String, Long> summaryMap) throws ApplicationException {
        setCommonAppSettings(appId, viewApp);
        viewApp.setNumberOfSubCategories(requestService.getSupportedSubCategories().size());
        viewApp.setSubCategoryCodeList(requestService.getSupportedSubCategories());

    }

    private void setCommonAppSettings(String appId, ViewApp viewApp) throws ApplicationException {
        ApplicationImpl application = (ApplicationImpl) appRepository.findAppByAppId(appId);
        viewApp.setAppName(application.getAppName());
        viewApp.setStartDate(dateFormater.format(application.getStartDate().toDate()));
        viewApp.setEndDate(dateFormater.format(application.getEndDate().toDate()));
        viewApp.setDescription(application.getDescription());
        setRkInfo(appId, viewApp);

        viewApp.setLastUpdatedTime(new Date());
        viewApp.setSubCategoryRequired(application.getService().isSubCategoriesSupported());
    }

    private void setRkInfo(String appId, ViewApp viewApp) throws ApplicationException {
        //todo load rk from the API
    }

    @ModelAttribute("viewApp")
    protected Object formBackingObject() throws Exception {
        return new ViewApp();
    }

    private KeywordDetails createKeywordsForUI(SubCategory subCategory, String appId) {
        KeywordDetails keywordDetails = new KeywordDetails();
        keywordDetails.setKeyword(subCategory.getKeyword());
        ContentDispatchSummary summary = null;
        try {
            summary = alertDispatchedContentRepository.getLastSentAlertBySubCategoryId(appId, subCategory.getId());
            setContentDispatchSummaryDetails(keywordDetails, summary);
        } catch (ApplicationException e) {
            logger.error("Last sent alert message not found for the app id [{}] and sub category id [{}]", appId,
                    subCategory.getId());
        }
        return keywordDetails;
    }

    private KeywordDetails createSubscriptionKeywordsForUI(SubCategory subCategory, String appId) {
        KeywordDetails keywordDetails = new KeywordDetails();
        keywordDetails.setKeyword(subCategory.getKeyword());
        ContentDispatchSummary summary = null;
        try {
            summary = subscriptionServiceDataRepository.getLastSentSubscriptionBySubCategoryId(appId, subCategory.getId());
            setContentDispatchSummaryDetails(keywordDetails, summary);
        } catch (ApplicationException e) {
            logger.error("Last sent alert message not found for the app id [{}] and sub category id [{}]", appId,
                    subCategory.getId());
        }
        return keywordDetails;
    }

    private void viewAppAuditLog(String appId, ContentProvider contentProvider, HttpServletRequest request, AuditEntity.ActionStatus status) {
        audit(new AuditEntity.AuditEntityBuilder(contentProvider)
                .populateFromHttpRequest(request)
                .action(auditLogAction.getViewAppAction())
                .description(String.format(auditLogAction.getViewAppActionDesc(), appId))
                .status(status)
                .build());
    }
}




