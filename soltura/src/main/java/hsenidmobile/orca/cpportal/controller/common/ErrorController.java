/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate: $
 *   $LastChangedBy: $
 *   $LastChangedRevision: $
 *
 */
package hsenidmobile.orca.cpportal.controller.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * $LastChangedDate: 2010-12-28 18:01:53 +0530 (Tue, 28 Dec 2010) $
 * $LastChangedBy: sampath $
 * $LastChangedRevision: 69349 $
 */
@Controller
public class ErrorController {
    private static final String LAST_ERROR_OCCURRED = "LAST_ERROR_OCCURRED";
    private static final String ERROR_MESSAGE = "errorMessage";

    private static final Logger logger = LoggerFactory.getLogger(ErrorController.class);

    @RequestMapping(value = "error")
    public String handleError(HttpServletRequest request, HttpServletResponse response)
            throws Throwable {
        try {
            String message = request.getParameter("messageId");
            if (message == null || message.equals("")) {
                message = (String) WebUtils.getSessionAttribute(request, LAST_ERROR_OCCURRED);
                logger.debug("[messageId] not available. Set the last error [{}] occurred", message);
            }
            request.setAttribute(ERROR_MESSAGE, message);
        } catch (Exception e) {
            request.setAttribute(ERROR_MESSAGE, "common.error.message");
        } finally {
            WebUtils.setSessionAttribute(request, LAST_ERROR_OCCURRED, request.getAttribute(ERROR_MESSAGE));
            logger.debug("Set last error [{}] as session attribute", request.getAttribute(ERROR_MESSAGE));
        }
        return "/common/error";
    }

    @RequestMapping(value = "loginFailError")
    public String handleLoginFailError(HttpServletRequest request, HttpServletResponse response)
            throws Throwable {
        try {
            String message = request.getParameter("messageId");
            if (message == null || message.equals("")) {
                message = (String) WebUtils.getSessionAttribute(request, LAST_ERROR_OCCURRED);
                logger.debug("[messageId] not available. Set the last error [{}] occurred", message);
            }
            request.setAttribute(ERROR_MESSAGE, message);
            request.getSession().invalidate();
        } catch (Exception e) {
            request.setAttribute(ERROR_MESSAGE, "common.error.message");
        } finally {
            WebUtils.setSessionAttribute(request, LAST_ERROR_OCCURRED, request.getAttribute(ERROR_MESSAGE));
            logger.debug("Set last error [{}] as session attribute", request.getAttribute(ERROR_MESSAGE));
        }
        return "/common/loginFailError";
    }

    @RequestMapping(value = "casfailedError")
    public String handleCasFailError(HttpServletRequest request, HttpServletResponse response) {
        return "casfailedError";
    }
}

