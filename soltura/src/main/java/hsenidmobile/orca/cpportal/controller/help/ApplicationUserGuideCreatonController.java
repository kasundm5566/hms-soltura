/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hsenidmobile.orca.cpportal.controller.help;

import hsenidmobile.orca.core.applications.Service;
import hsenidmobile.orca.core.applications.alert.AlertService;
import hsenidmobile.orca.core.applications.request.RequestService;
import hsenidmobile.orca.core.applications.subscription.Subscription;
import hsenidmobile.orca.core.applications.voting.VotingService;
import hsenidmobile.orca.core.model.Application;
import hsenidmobile.orca.core.model.SdpAppLoginData;
import hsenidmobile.orca.core.repository.SdpApplicationRepository;
import hsenidmobile.orca.cpportal.controller.AbstractBaseController;
import hsenidmobile.orca.cpportal.module.ApplicationInfo;
import hsenidmobile.orca.cpportal.service.CurrentUser;
import hsenidmobile.orca.cpportal.service.impl.ContentProviderService;
import hsenidmobile.orca.cpportal.service.impl.application.ApplicationService;
import hsenidmobile.orca.cpportal.util.ServiceTypes;
import hsenidmobile.orca.rest.sdp.app.transport.ProvAppManageRequestSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.NoSuchMessageException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.support.RequestContext;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hsenidmobile.orca.cpportal.util.ServiceTypes.*;
import static hsenidmobile.orca.rest.common.util.RestApiKeys.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
@Controller
public class ApplicationUserGuideCreatonController extends AbstractBaseController {

    private static final Logger logger = LoggerFactory.getLogger(ApplicationUserGuideCreatonController.class);
    @Autowired
    private ApplicationService applicationService;
    @Autowired
    private ContentProviderService contentProviderService;
    @Autowired
    private SdpApplicationRepository sdpApplicationRepository;
    @Autowired
    private ProvAppManageRequestSender provAppManageRequestSender;

    /**
     * This method will get the relavant help message template from the resource bundle and
     * customizes according to the application details and sends back to the UI
     *
     * @param applicationInfo
     * @param appId
     * @return
     * @throws hsenidmobile.orca.core.applications.exception.ApplicationException
     *
     */
    @RequestMapping(value = "/manage/appUserGuide/{appId}")
    public String showApplicationUserGuide(@ModelAttribute("app") ApplicationInfo applicationInfo,
                                           @PathVariable String appId,
                                           HttpServletRequest request,
                                           ModelMap model1) throws Throwable {
        try {
            String userName = CurrentUser.getUsername();
            if (!contentProviderService.isAllowedToAccess(userName, appId)) {
                return handleUnAuthorizedUserAccess(userName, appId);
            }
            RequestContext ctx = new RequestContext(request);
            Map<String, Object> model = new HashMap<String, Object>();

            Application app = applicationService.findAppById(appId);


            Map<String, String> parameters = new HashMap<String, String>();
            SdpAppLoginData sdpAppData = null;
            sdpAppData = sdpApplicationRepository.getSdpAppLoginDataForOrcaApp(app.getAppId());
            if (sdpAppData != null) {
                logger.debug("SDP app id [{}]", sdpAppData.getAppId());
                parameters.put(APP_ID, sdpAppData.getAppId());
            }

            List<Map<String, String>> routingkeyList = new ArrayList<Map<String, String>>();
            Map<String, Object> availableApps = provAppManageRequestSender.getApp(parameters);
            Map<String, Object> ncsSlas = (Map<String, Object>) availableApps.get(NCS_SLAS);

            for (Map.Entry<String, Object> entry : ncsSlas.entrySet()) {
                Map<String, Object> ncsSlaMap = (Map<String, Object>) entry.getValue();
                Object routingKeys = ncsSlaMap.get(ROUTING_KEYS);
                if (routingKeys != null) {
                    for (Map<String, String> maps : (List<Map<String, String>>) routingKeys) {
                        routingkeyList.add(maps);
                    }
                } else {
                    logger.debug("NO routing keys found for the given ncs sla [{}]", ncsSlaMap.get(NCS_TYPE));
                }
            }

            Map<String, Map<String,List<String>>> stringListMap = getHelpMessageTemplateByServiceId(app.getService(), ctx, appId, routingkeyList);
            fillMessageTemplateContent(applicationInfo, model, stringListMap);
            model1.addAttribute("model", model);
            return "/help/appUserGuide";
        } catch (Throwable e) {
            logger.error("Error occurred while viewing the application help details : ", e);
            return "redirect:/error.html?messageId=view.application.error";
        }
    }

    private void fillMessageTemplateContent(ApplicationInfo applicationInfo, Map<String, Object> model, Map<String, Map<String,List<String>>> stringListMap) {
        for (Map.Entry<String, Map<String,List<String>>> stringListEntry : stringListMap.entrySet()) {
            model.put("SERVICE_TYPE", stringListEntry.getKey());
            model.put("HELP_MESSAGES", stringListEntry.getValue());
            applicationInfo.setServiceType(stringListEntry.getKey());
        }
    }


    private Map<String, Map<String,List<String>>> getHelpMessageTemplateByServiceId(Service service, RequestContext ctx, String appId, List<Map<String, String>> routingkeyList) {
        Map<String, Map<String,List<String>>> helpMessageList = new HashMap<String, Map<String,List<String>>>();
        if (service instanceof VotingService) {
            helpMessageList.put(VOTING.name(), getVotingHelpMessageTemplate(ctx, appId, routingkeyList));
        } else if (service instanceof AlertService) {
            helpMessageList.put(ALERT.name(), getAlertHelpMessageTemplate(ctx, appId, routingkeyList));
        } else if (service instanceof Subscription) {
            helpMessageList.put(ServiceTypes.SUBSCRIPTION.name(), getSubscriptionHelpMessageTemplate(ctx, appId, routingkeyList));
        } else if (service instanceof RequestService) {
            helpMessageList.put(REQUEST.name(), getRequestHelpMessageTemplate(ctx, appId, routingkeyList));
        } else {
            throw new IllegalArgumentException("Unexpected service type found");
        }
        return helpMessageList;
    }

    //todo refactoring to be done once the finalized help message templates are received from Telkomsel
    //todo for the time being static template has been given

    private Map<String,List<String>> getVotingHelpMessageTemplate(RequestContext ctx, String appId, List<Map<String, String>> routingkeyList) {
        Map<String,List<String>> messageTemplates = new HashMap<String,List<String>>();
        generateHelpText("app.user.guide.how.to.use.voting.help.text", messageTemplates, ctx, routingkeyList);
        logger.debug("Received Template [{}]", messageTemplates);
        return messageTemplates;
    }

    private Map<String,List<String>> getAlertHelpMessageTemplate(RequestContext ctx, String appId, List<Map<String, String>> routingkeyList) {
        Map<String,List<String>> messageTemplates = new HashMap<String,List<String>>();
        generateHelpText("app.user.guide.how.to.use.alert.help.text", messageTemplates, ctx, routingkeyList);
        logger.debug("Received Template [{}]", messageTemplates);
        return messageTemplates;
    }

    private Map<String,List<String>> getSubscriptionHelpMessageTemplate(RequestContext ctx, String appId, List<Map<String, String>> routingkeyList) {
        Map<String,List<String>> messageTemplates = new HashMap<String,List<String>>();
        generateHelpText("app.user.guide.how.to.use.subscription.help.text", messageTemplates, ctx, routingkeyList);
        logger.debug("Received Template [{}]", messageTemplates);
        return messageTemplates;
    }

    private Map<String,List<String>> getRequestHelpMessageTemplate(RequestContext ctx, String appId, List<Map<String, String>> routingkeyList) {
        Map<String,List<String>> messageTemplates = new HashMap<String,List<String>>();
        generateHelpText("app.user.guide.how.to.use.request.help.text", messageTemplates, ctx, routingkeyList);
        logger.debug("Received Template [{}]", messageTemplates);
        return messageTemplates;
    }

    private void generateHelpText(String messageKey, Map<String,List<String>> messageTemplates, RequestContext ctx, List<Map<String, String>> routingkeyList) {
        String genertaedMessageKey;
        String helpMessage;
        String operator;
        boolean isMessagePresent;

        for (int i = 0; i < routingkeyList.size(); i++) {
            List<String> messageList = new ArrayList<String>();
            int messageId = 1;
            operator = routingkeyList.get(i).get(OPERATOR);
            logger.debug("MESSAGE TEMPLATE OPERATOR : [{}]", operator);
            do {
                genertaedMessageKey = messageKey + messageId;
                try {
                    helpMessage = ctx.getMessage(genertaedMessageKey);
                    logger.debug("ROUTING KEYS [{}]", routingkeyList.get(i));
                    String result = replace(helpMessage, "<SHORTCODE>", routingkeyList.get(i).get(SHORTCODE));
                    logger.debug("SHORTCODE : [{}]" , routingkeyList.get(i).get(SHORTCODE));
                    String result2 = replace(result, "<KEYWORD>", routingkeyList.get(i).get(KEYWORD));
                    isMessagePresent = true;
                    logger.debug("HELP MESSAGE : [{}]", result2);
                    messageList.add(result2);
                    ++messageId;
                } catch (NoSuchMessageException e) {
                    isMessagePresent = false;
                }
            }
            while (isMessagePresent);
            logger.debug("MESSAGE LIST SIZE : [{}]", messageList.size());
            messageTemplates.put(operator, messageList);
            logger.debug("INSERTED DATA TO MAP");
        }
    }

    static String replace(String str, String pattern, String replace) {
        int s = 0;
        int e = 0;
        StringBuffer result = new StringBuffer();

        logger.debug("STRING INPUT : [{}]", str);
        logger.debug("STRING PATTERN : [{}]", pattern);
        logger.debug("STRING REPLACE : [{}]", replace);

        while ((e = str.indexOf(pattern, s)) >= 0) {
            result.append(str.substring(s, e));
            result.append(replace);
            s = e + pattern.length();
            logger.debug("S : [{}]", s);
        }
        result.append(str.substring(s));
        logger.debug("RESULT AFTER REPLACE [{}]", result.toString());
        return result.toString();
    }
}