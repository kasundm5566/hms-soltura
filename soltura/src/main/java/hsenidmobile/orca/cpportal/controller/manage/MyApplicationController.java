/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hsenidmobile.orca.cpportal.controller.manage;

import hsenidmobile.orca.core.applications.ApplicationImpl;
import hsenidmobile.orca.core.applications.exception.ApplicationException;
import hsenidmobile.orca.core.model.Application;
import hsenidmobile.orca.core.model.ContentProvider;
import hsenidmobile.orca.core.model.SdpAppLoginData;
import hsenidmobile.orca.core.model.Status;
import hsenidmobile.orca.core.repository.SdpApplicationRepository;
import hsenidmobile.orca.cpportal.controller.AbstractBaseController;
import hsenidmobile.orca.cpportal.filter.ApplicationFilter;
import hsenidmobile.orca.cpportal.filter.CustomFilter;
import hsenidmobile.orca.cpportal.service.CurrentUser;
import hsenidmobile.orca.cpportal.service.impl.application.ApplicationService;
import hsenidmobile.orca.cpportal.util.DefaultProperties;
import hsenidmobile.orca.cpportal.util.logging.AuditEntity;
import hsenidmobile.orca.cpportal.util.logging.AuditLogActionProperties;
import hsenidmobile.orca.orm.repository.ContentProviderDetailsRepositoryImpl;
import hsenidmobile.orca.orm.repository.AppRepositoryImpl;
import hsenidmobile.orca.rest.sdp.app.transport.ProvAppManageRequestSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

import static hsenidmobile.orca.cpportal.util.logging.SolturaAuditLogger.audit;
import static hsenidmobile.orca.rest.common.util.RestApiKeys.APP_ID;
import static hsenidmobile.orca.rest.common.util.RestApiKeys.STATUS;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
@Controller
@SessionAttributes("model")
public class MyApplicationController extends AbstractBaseController {

    private static final Logger logger = LoggerFactory.getLogger(MyApplicationController.class);
    private static int MAX_CHARACTER_LENGTH = 15;
    @Autowired
    private ApplicationService applicationService;
    @Autowired
    private AppRepositoryImpl appRepository;
    @Autowired
    private ContentProviderDetailsRepositoryImpl cpDetailsRepository;
    @Autowired
    private ProvAppManageRequestSender provAppManageRequestSender;
    @Autowired
    private DefaultProperties defaultProperties;
    @Autowired
    private SdpApplicationRepository sdpApplicationRepository;
    @Autowired
    private AuditLogActionProperties auditLogAction;

    @Resource(name = "applicationStatus")
    Map<String, String> applicationStatus;
    private static final int MAX_NUM_OF_APPS_TO_BE_DISPLAYED = 6;

    @RequestMapping(value = "/manage/myApplications")
    public ModelAndView viewMyApplication(HttpServletRequest request) throws Throwable {
        ContentProvider cp = null;
        try {
            String currentUser = CurrentUser.getUsername();
            logger.debug("current user details : [{}]", currentUser);
            boolean isNextEnable = false;
            int numberOfAppsOnLastPage = 0;
            int numberOfPages = 0;
            Map<String, Object> model = new HashMap<String, Object>();
            cp = cpDetailsRepository.findByUsername(currentUser);
            List<ApplicationImpl> applicationList = appRepository.findCpAppBySpid(cp.getSpId());
            filter(applicationList, new ApplicationFilter());
            if (applicationList != null && applicationList.size() > MAX_NUM_OF_APPS_TO_BE_DISPLAYED) {
                isNextEnable = true;
                numberOfAppsOnLastPage = applicationList.size() % MAX_NUM_OF_APPS_TO_BE_DISPLAYED;
                numberOfAppsOnLastPage = (numberOfAppsOnLastPage == 0) ? MAX_NUM_OF_APPS_TO_BE_DISPLAYED : numberOfAppsOnLastPage;
                numberOfPages = (int) Math.ceil(applicationList.size() / MAX_NUM_OF_APPS_TO_BE_DISPLAYED);
            }
            if (applicationList.size() > 0) {
                logger.debug("user: [{}] having [{}] applications", new Object[]{currentUser, applicationList.size()});
            }
            setShortDescription(applicationList);
            Collections.sort(applicationList, new AppSortByName());
            model.put("application", applicationList);
            model.put("cpStatus", cp.getStatus().name());
            model.put("isNextEnable", isNextEnable);
            model.put("numberOfAppsOnLastPage", numberOfAppsOnLastPage);
            model.put("numberOfPages", numberOfPages);
            model.put("publishBaseUrl", defaultProperties.getBaseUrl());
            myAppsAccessAuditLog(cp, request, AuditEntity.ActionStatus.SUCCESS);
            return new ModelAndView("manage/myApplications", "model", model);
        } catch (Throwable e) {
            logger.error("Error occurred while viewing the my applications : ", e);
            myAppsAccessAuditLog(cp, request, AuditEntity.ActionStatus.FAILED);
            request.setAttribute("errorMessage", "view.application.error");
            return new ModelAndView("/common/error");
        }
    }

    @RequestMapping(value = "/manage/searchApplication", method = RequestMethod.GET)
    public ModelAndView findAppGetRequest(@ModelAttribute("model") ModelMap model) throws Throwable {
        logger.info("invoke find application [GET] model={}", model);
        return new ModelAndView("manage/myApplications", "model", model);
    }

    @RequestMapping(value = "/manage/searchApplication", method = RequestMethod.POST)
    public ModelAndView find(@RequestParam("searchName") String name, HttpServletRequest request,
                             HttpServletResponse response, ModelMap model) throws Throwable {
        logger.debug("Search Attribute [{}]", name);
        ContentProvider cp = null;
        try {
            String currentUser = CurrentUser.getUsername();
            logger.debug("current user details : [{}]", currentUser);
            boolean isNextEnable = false;
            int numberOfAppsOnLastPage = 0;
            int numberOfPages = 0;
            cp = cpDetailsRepository.findByUsername(currentUser);
            List<ApplicationImpl> applicationList = applicationService.findCpAppBySpid(currentUser, name, cp.getSpId());
            filter(applicationList, new ApplicationFilter());
            if (applicationList != null && applicationList.size() > MAX_NUM_OF_APPS_TO_BE_DISPLAYED) {
                isNextEnable = true;
                numberOfAppsOnLastPage = applicationList.size() % MAX_NUM_OF_APPS_TO_BE_DISPLAYED;
                numberOfAppsOnLastPage = (numberOfAppsOnLastPage == 0) ? MAX_NUM_OF_APPS_TO_BE_DISPLAYED : numberOfAppsOnLastPage;
                numberOfPages = (int) Math.ceil(applicationList.size() / MAX_NUM_OF_APPS_TO_BE_DISPLAYED);
            }
            if (applicationList.size() > 0) {
                logger.debug("user:[{}] having [{}] applications", new Object[]{currentUser, applicationList.size()});
            }
            setShortDescription(applicationList);
            Collections.sort(applicationList, new AppSortByName());
            model.put("application", applicationList);
            model.put("cpStatus", cp.getStatus().name());
            model.put("searchInput", name);
            model.put("isNextEnable", isNextEnable);
            model.put("numberOfAppsOnLastPage", numberOfAppsOnLastPage);
            model.put("numberOfPages", numberOfPages);
            model.put("isSearchResult", true);
            model.put("publishBaseUrl", defaultProperties.getBaseUrl());
            searchAppsAuditLog(name, cp, request, AuditEntity.ActionStatus.SUCCESS);
            return new ModelAndView("manage/myApplications", "model", model);
        } catch (Throwable e) {
            logger.error("Error occurred while displaying the search result : ", e);
            searchAppsAuditLog(name, cp, request, AuditEntity.ActionStatus.FAILED);
            request.setAttribute("errorMessage", "view.application.error");
            return new ModelAndView("/common/error");
        }
    }

    private void setShortDescription(List<ApplicationImpl> applicationList) {
        for (ApplicationImpl application : applicationList) {
            if (application.getDescription().length() > MAX_CHARACTER_LENGTH) {
                application.setShortDescription(application.getDescription().trim().substring(0, MAX_CHARACTER_LENGTH) + "...");
            } else {
                application.setShortDescription(application.getDescription().trim());
            }
            application.setDisplayStatus(applicationStatus.get(application.getStatus().name()));
        }
    }

    private <T> void filter(Collection<T> l, CustomFilter<T> filter) throws ApplicationException {
        Iterator<T> it = l.iterator();
        while (it.hasNext()) {
            ApplicationImpl application = (ApplicationImpl) it.next();
            // TODO Modify with the db changes
            updateStatus(application);
            //todo application status should be removed from the soltura
            /*Status updatedStatus = applicationService.updateOrcaApplicationState(application);
            application.setStatus(updatedStatus);*/
            if (filter.match(application)) {
                it.remove();
            }
        }
    }

    public class AppSortByName implements Comparator<ApplicationImpl> {
        @Override
        public int compare(ApplicationImpl o1, ApplicationImpl o2) {
            return o1.getAppName().toLowerCase().compareTo(o2.getAppName().toLowerCase());
        }
    }


    public void updateStatus(ApplicationImpl application) throws ApplicationException {
        logger.debug("Orca App ID [{}]  ", application.getAppId());
        Map<String, Object> statusRequest = new HashMap<String, Object>();
        SdpAppLoginData sdpAppData = null;
        sdpAppData = sdpApplicationRepository.getSdpAppLoginDataForOrcaApp(application.getAppId());
        if (sdpAppData != null) {
            String sdpAppId = sdpAppData.getAppId();
            application.setSdpAppId(sdpAppId);
            logger.debug("SDP app id [{}]", sdpAppId);
            statusRequest.put(APP_ID, sdpAppData.getAppId());
            try {
                Map<String, Object> statusMap = provAppManageRequestSender.getSdpAppStatus(statusRequest);
                logger.debug("App STATUS is set as : [{}]", Status.getEnum((String) statusMap.get(STATUS)).name());
                application.setStatus(Status.getEnum((String) statusMap.get("status")));
            } catch (Throwable e) {
                logger.warn("No Application found in SDP for orca App ID [{}] ", application.getAppId());
            }
        }
    }

    private void myAppsAccessAuditLog(ContentProvider contentProvider, HttpServletRequest request, AuditEntity.ActionStatus status) {
        audit(new AuditEntity.AuditEntityBuilder(contentProvider)
                .populateFromHttpRequest(request)
                .action(auditLogAction.getMyAppsAction())
                .description(auditLogAction.getMyAppsActionDesc())
                .status(status)
                .build());
    }

    private void searchAppsAuditLog(String searchText, ContentProvider contentProvider, HttpServletRequest request, AuditEntity.ActionStatus status) {
        audit(new AuditEntity.AuditEntityBuilder(contentProvider)
                .populateFromHttpRequest(request)
                .action(auditLogAction.getSearchAppsAction())
                .description(String.format(auditLogAction.getSearchAppsActionDesc(), searchText))
                .status(status)
                .build());
    }
}
