/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 */
package hsenidmobile.orca.cpportal.controller.manage;

import hsenidmobile.orca.core.applications.ApplicationImpl;
import hsenidmobile.orca.core.applications.Service;
import hsenidmobile.orca.core.applications.alert.AlertService;
import hsenidmobile.orca.core.applications.alert.traffic.AlertAppTraffic;
import hsenidmobile.orca.core.applications.subscription.Subscription;
import hsenidmobile.orca.core.flow.exception.MessageThroughputExceededException;
import hsenidmobile.orca.core.model.*;
import hsenidmobile.orca.core.model.message.AppDataUpdateRequest;
import hsenidmobile.orca.core.repository.AppTrafficRepository;
import hsenidmobile.orca.core.repository.MessageHistoryRepository;
import hsenidmobile.orca.core.util.CorrelationIdGenerationService;
import hsenidmobile.orca.cpportal.controller.AbstractBaseController;
import hsenidmobile.orca.cpportal.module.ContentKeywordDetail;
import hsenidmobile.orca.cpportal.module.KeywordDetails;
import hsenidmobile.orca.cpportal.module.Registration;
import hsenidmobile.orca.cpportal.module.UseApplication;
import hsenidmobile.orca.cpportal.service.CurrentUser;
import hsenidmobile.orca.cpportal.service.impl.ContentProviderService;
import hsenidmobile.orca.cpportal.service.impl.application.ApplicationService;
import hsenidmobile.orca.cpportal.service.impl.application.SubscriptionApplicationService;
import hsenidmobile.orca.cpportal.util.ServiceTypes;
import hsenidmobile.orca.cpportal.util.client.HttpRequestSender;
import hsenidmobile.orca.cpportal.util.logging.AuditEntity;
import hsenidmobile.orca.cpportal.util.logging.AuditLogActionProperties;
import hsenidmobile.orca.orm.repository.ContentProviderDetailsRepositoryImpl;
import hsenidmobile.orca.orm.repository.SubscriptionServiceDataRepositoryImpl;
import hsenidmobile.orca.orm.sdp.SdpApplicationRepositoryImpl;
import org.apache.cxf.jaxrs.client.ClientWebApplicationException;
import org.apache.log4j.NDC;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static hsenidmobile.orca.core.flow.MessageFlowErrorReasons.DAYS_EXCEEDED;
import static hsenidmobile.orca.core.util.eventlog.EventLogger.createEventLog;
import static hsenidmobile.orca.cpportal.util.logging.SolturaAuditLogger.audit;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */

@Controller
@SessionAttributes("useApp")
public class UseApplicationController extends AbstractBaseController {

    private static final Logger logger = LoggerFactory.getLogger(UseApplicationController.class);
    @Autowired
    private ApplicationService applicationService;
    @Autowired
    private SubscriptionApplicationService subscriptionApplicationService;
    @Autowired
    private SubscriptionServiceDataRepositoryImpl subscriptionDataRepository;
    @Autowired
    private SdpApplicationRepositoryImpl sdpApplicationRepository;
    @Autowired
    private MessageHistoryRepository messageHistoryRepository;
    @Autowired
    private ContentProviderService contentProviderService;
    @Autowired
    private AppTrafficRepository appTrafficRepository;
    @Autowired
    @Qualifier("httpAppUpdateRequestSender")
    private HttpRequestSender httpAppUpdateRequestSender;
    @Autowired
    private ContentProviderDetailsRepositoryImpl cpDetailsRepositoryImpl;
    @Autowired
    private AuditLogActionProperties auditLogAction;

    private static final String TYPE = "type";
    private static final String CONCURRENT_UPDATE = "concurrentupdate";
    private static final String EMPTY_MESSAGE = "emptyMessage";

    @RequestMapping(value = "/use/add/{appId}", method = RequestMethod.GET)
    public String useApplication(@ModelAttribute("useApp") UseApplication useApplication, @PathVariable String appId)
            throws Throwable {
        try {
            logger.debug("Retrieving application data for appId [{}]", appId);
            String userName = CurrentUser.getUsername();
            if(!contentProviderService.isAllowedToAccess(userName, appId)) {
                return handleUnAuthorizedUserAccess(userName, appId);
            }
            List<ApplicationImpl> appList = applicationService.getContentProviderDetails(true).getApplications();
            ApplicationImpl app = null;
            for (ApplicationImpl application : appList) {
                if (application.getAppId().equals(appId)) {
                    app = application;
                }
            }

            if (app == null) {
                throw new RuntimeException("This user is not Authorized to access this application.");
            }
            logger.debug("Application found : application [{}]", app);
            useApplication.setApplication(app);
            useApplication.setSubCategoryRequired(app.getService().isSubCategoriesSupported());
            if (app.getService() instanceof Subscription) {
                createSubscriptionContentUpdateDisplay(useApplication, app);
            } else if (app.getService() instanceof AlertService) {
                addAlertInitialData(useApplication);
                useApplication.setServiceType(ServiceTypes.ALERT.toString());
            } else {
                logger.error("Unsupported service type found for the use application functionality");
            }
            useApplication.setMessageMaxLength(defaultProperties.getMaxMessageLength());
            return "/useApplication/useApplication";
        } catch (ClientWebApplicationException e) {
            logger.error("Error while processing the rerquest, Server error : ", e);
            return "redirect:/error.html?messageId=view.server.error";
        } catch (Throwable e) {
            logger.error("Error occurred while loading the content : ", e);
            return "redirect:/error.html?messageId=view.application.error";
        }
    }

    @RequestMapping(value = "/use/update")
    public String useUpdate(@ModelAttribute("useApp") UseApplication useApplication,
                            SessionStatus status, HttpServletRequest request)
            throws Throwable {
        final long correlationId = CorrelationIdGenerationService.getCorrelationId(defaultProperties.getServerId());
        ContentProvider cp = null;
        List<KeywordDetails> updateMsgs = new ArrayList<KeywordDetails>();
        try {
            NDC.push(String.valueOf(correlationId));
            cp = cpDetailsRepositoryImpl.getContentProvider();
            boolean isMessageEmpty = false;
            try {

                if (useApplication.getApplication().getService() instanceof Subscription) {
                    updateSubscriptionApplicationData(useApplication);

                    List<KeywordDetails> serviceMsgs = new ArrayList<KeywordDetails>();
                    for(ContentKeywordDetail coInfo : useApplication.getContentKeywordDetails()) {
                        for(KeywordDetails info : coInfo.getDetails()) { serviceMsgs.add(info); }
                    }
                    updateMsgs = serviceMsgs;
                } else if (useApplication.getApplication().getService() instanceof AlertService) {
                    isMessageEmpty = updateAlertApplicationData(useApplication, correlationId);
                    updateMsgs = useApplication.getAlertKeywordDetails();
                }

                //Audit Log
                updateAppContentAuditLog(useApplication.getServiceType(), updateMsgs, cp, request, AuditEntity.ActionStatus.SUCCESS);

                generateEventLog(useApplication);
                if (!isMessageEmpty) {
                    return "redirect:/use/success.html";
                } else {
//                    return handleUpdateContentError(useApplication, new Throwable(), isMessageEmpty);
//                    return "redirect:/use/add/" + useApplication.getApplication().getAppId() + ".html";
                    return "redirect:/error.html?messageId=empty.content.update";
                }
            } catch (Throwable e) {
                logger.error("Error while updating content", e);
                updateAppContentAuditLog(useApplication.getServiceType(), updateMsgs, cp, request, AuditEntity.ActionStatus.FAILED);
                return handleUpdateContentError(useApplication, e, isMessageEmpty);
            }
        } finally {
            NDC.remove();
        }
    }

    private String handleUpdateContentError(UseApplication useApplication, Throwable e, boolean isMessageEmpty) {
        logger.error("Error occurred while updating the content for the application [ " +
                useApplication.getApplication().getAppName() + " ]", e);
        if (e instanceof OrcaException) {
            if (useApplication.getApplication().getService() instanceof AlertService) {
                if (e instanceof MessageThroughputExceededException) {
                    if(((MessageThroughputExceededException) e).getReason() == DAYS_EXCEEDED) {
                        return "redirect:/error.html?messageId=alert.tpd.exceeded.message";
                    } else {
                        return "redirect:/error.html?messageId=alert.tpm.exceeded.message";
                    }
                } else {
                    return "redirect:/error.html?messageId=alert.sending.error";
                }
            }  else {
                return "redirect:/error.html?messageId=content.update.error";
            }
        } else if (useApplication.getApplication() != null) {
            String redirectPath = "redirect:/use/add/" + useApplication.getApplication().getAppId() + ".html";
            logger.info("Redirecting user back to [{}] due to error [{}]",
                    new Object[] { redirectPath, e.getMessage() });
            if (isMessageEmpty) {
                useApplication.setMessage(EMPTY_MESSAGE);
            } else {
                useApplication.setMessage(CONCURRENT_UPDATE);
            }
            return redirectPath;
        } else {
            return "redirect:/error.html?messageId=common.error.message";
        }
    }

    private boolean updateAlertApplicationData(UseApplication useApplication, final long correlationId) throws Throwable {
        AppDataUpdateRequest appDataUpdateRequest = new AppDataUpdateRequest();
        appDataUpdateRequest.setCorrelationId(String.valueOf(correlationId));
        String appId = useApplication.getApplication().getAppId();
        appDataUpdateRequest.setAppId(appId);
        final List<ContentRelease> contentRelease = applicationService.getAlertMessagesToBeSent(
                useApplication.getAlertKeywordDetails(), useApplication.getApplication().getService());
        appDataUpdateRequest.setUpdateContent(contentRelease);
        String appName = useApplication.getApplication().getAppName();
        AlertAppTraffic alertAppTraffic = verifyDailyBasedThroughput(useApplication, appName);
        httpAppUpdateRequestSender.sendMessage(appDataUpdateRequest);
        logger.debug("Content updated for [{}]", appName);
        updateAlertAppTrafficData(appName, alertAppTraffic);

        return contentRelease.get(0).getContents().isEmpty();
    }

    private AlertAppTraffic verifyDailyBasedThroughput(UseApplication useApplication, String appName) throws
            MessageThroughputExceededException {
        Service service = useApplication.getApplication().getService();
        AlertAppTraffic alertAppTraffic = appTrafficRepository.findAlertAppTraffic(useApplication.getApplication().getAppId());
        logger.debug("Found Alert App traffic data for the application [{}] - [{}] from the UI ", new Object[] {appName,
                alertAppTraffic.toString()});
        AlertService alertService = (AlertService) service;
        alertAppTraffic.verifyThroughput(alertService.getMapd(), alertService.getMapm());
        return alertAppTraffic;
    }

    private void updateAlertAppTrafficData(String appName, AlertAppTraffic alertAppTraffic) {
        int currentDailyCount = alertAppTraffic.getCurrentAlertMsgsPerDay().getCurrentNoOfAlertMessages();
        int currentMonthlyCount = alertAppTraffic.getCurrentAlertMsgsPerMonth().getCurrentNoOfAlertMessages();
        alertAppTraffic.getCurrentAlertMsgsPerDay().setCurrentNoOfAlertMessages(++currentDailyCount);
        alertAppTraffic.getCurrentAlertMsgsPerMonth().setCurrentNoOfAlertMessages(++currentMonthlyCount);
        appTrafficRepository.updateAlertAppThroughput(alertAppTraffic);
        logger.debug("Updated Alert App traffic data for the application [{}] - [{}] from the UI ", new Object[] {appName,
                alertAppTraffic.toString()});
    }

    @RequestMapping(value = "/use/success")
    public String success(HttpServletRequest request, @ModelAttribute("useApp") UseApplication useApplication ,SessionStatus status)
            throws OrcaException {
        String type = null;
        if (useApplication.getApplication().getService() instanceof AlertService) {
            type = "alert";
        } else {
            type = "channel";
        }
        request.setAttribute(TYPE, type);
        status.setComplete();
        return "/useApplication/success";
    }

    @RequestMapping(value = "/use/back", method = RequestMethod.GET)
    public String cancel(@ModelAttribute("useApp") Registration registration, SessionStatus status) {
        status.setComplete();
        return "redirect:../manage/myApplications.html";
    }

    private void addAlertInitialData(UseApplication useApplication) {
        logger.debug("Creating alert initial data");
        ApplicationImpl app = useApplication.getApplication();
        final AlertService alertService = (AlertService) app.getService();
        List<KeywordDetails> alertKeywordDetails = new ArrayList<KeywordDetails>();
        if (!alertService.isSubCategoriesSupported()) {
            KeywordDetails keywordDetails = new KeywordDetails();
            try {
                setKeywordAsSubKeyword(app, keywordDetails);
                alertKeywordDetails.add(keywordDetails);
            } catch (Exception e) {
                logger.error("Error in setting keyword for application which not support sub category [{}]", e);
            }
        } else {
            for (SubCategory subCategory : alertService.getSupportedSubCategories()) {
                logger.debug("Found existing keyword detail[{}]", subCategory);
                KeywordDetails keywordDetails = new KeywordDetails();
                keywordDetails.setKeyword(subCategory.getKeyword());
                keywordDetails.setDescriptionEn("");
                keywordDetails.setDescriptionTa("");
                keywordDetails.setDescriptionSi("");
                alertKeywordDetails.add(keywordDetails);
            }
        }
        useApplication.setAlertKeywordDetails(alertKeywordDetails);
    }

    private void setKeywordAsSubKeyword(ApplicationImpl app, KeywordDetails keywordDetails) {
        final String keyword = getServiceKeyword(app);
        keywordDetails.setKeyword(keyword);
        keywordDetails.setDescriptionEn("");
        keywordDetails.setDescriptionTa("");
        keywordDetails.setDescriptionSi("");
    }

    @ModelAttribute("useApp")
    protected Object formBackingObject() throws Exception {
        return new UseApplication();
    }

    private void createSubscriptionContentUpdateDisplay(UseApplication useApplication, ApplicationImpl app) {
        logger.info("Creating content for Subscription Update");
        Subscription subscription = (Subscription) app.getService();
        useApplication.setPeriodicity(subscription.getSupportedPeriodicities().get(0));

        String serviceKeyword = getServiceKeyword(app);

        // todo set initial editable content

        List<ContentKeywordDetail> initialEditableContent = subscriptionApplicationService.getEditableContent(
                subscriptionDataRepository.getUpdatableContents(app.getAppId(), new DateTime()), serviceKeyword,
                subscription);

        // todo correct event log printing

        logger.debug("Editable content size from initial data[{}]", initialEditableContent.size());
        initialEditableContent = subscriptionApplicationService.fillMissingTimeSlots(useApplication.getPeriodicity(),
                initialEditableContent);
        logger.debug("Editable content size after filling missing slots [{}]", initialEditableContent.size());
        List<ContentKeywordDetail> newContent = createNewBlankContentForSubscription(subscription,
                initialEditableContent, createSubscriptionSubkeywordsList(app, serviceKeyword));
        logger.debug("Newly created blank content size [{}]", newContent.size());
        initialEditableContent.addAll(newContent);
        useApplication.setContentKeywordDetails(subscriptionApplicationService
                .sortContentsByScheduleTime(initialEditableContent));
        logger.trace("Final content list[{}]", useApplication.getContentKeywordDetails());
        useApplication.setServiceType(ServiceTypes.SUBSCRIPTION.toString());
    }

    private List<ContentKeywordDetail> createNewBlankContentForSubscription(Subscription subscription,
                                                                            List<ContentKeywordDetail> initialEditableContent, List<String> availableSubKeywords) {
        ContentKeywordDetail lastScheduledContent = null;
        if (initialEditableContent.size() > 0) {
            lastScheduledContent = initialEditableContent.get(initialEditableContent.size() - 1);
        }
        int numberOfNewContentsRequired = defaultProperties.getMaxDispatchableContentCount()
                - initialEditableContent.size();

        // todo create new blank content

        List<ContentKeywordDetail> newContent = subscriptionApplicationService.createNewContentKeywordDetailList(
                subscription.getSupportedPeriodicities().get(0), availableSubKeywords, lastScheduledContent,
                numberOfNewContentsRequired);
        return newContent;
    }

    private List<String> createSubscriptionSubkeywordsList(ApplicationImpl app, String serviceKeyword) {
        List<String> availableSubKeywords;
        if (app.getService().isSubCategoriesSupported()) {
            availableSubKeywords = new ArrayList<String>();
            List<SubCategory> subCategoryList = app.getService().getSupportedSubCategories();
            for (SubCategory subCategory : subCategoryList) {
                availableSubKeywords.add(subCategory.getKeyword());
            }
        } else {
            availableSubKeywords = Arrays.asList(serviceKeyword);
        }
        return availableSubKeywords;
    }

    private String getServiceKeyword(ApplicationImpl app) {
        //todo get the assigned keywords for the application

        return "";
    }

    private void generateEventLog(UseApplication useApplication) {
        MessageHistory messageHistory = new MessageHistory();
        messageHistory.setApplicationId(useApplication.getApplication().getAppId());
        messageHistory.setApplicationName(useApplication.getApplication().getAppName());
        if (useApplication.getApplication().getService() instanceof Subscription) {
            messageHistory.setEvent(Event.CONTENT_UPDATED);
            messageHistory.setMessage("message.history.report.channel.content.sent.message");
            createEventLog(useApplication.getApplication(), null, null, Event.CONTENT_UPDATED,
                    "message.history.report.channel.content.sent.message");
        } else if (useApplication.getApplication().getService() instanceof AlertService) {
            messageHistory.setEvent(Event.CONTENT_SENT);
            messageHistory.setMessage("message.history.report.alert.content.sent.message");
            createEventLog(useApplication.getApplication(), null, null, Event.CONTENT_SENT,
                    "message.history.report.alert.content.sent.message");
        }
        messageHistoryRepository.saveEvent(messageHistory);
    }

    private void updateSubscriptionApplicationData(UseApplication useApplication) throws OrcaException {
        Subscription subscription = (Subscription) useApplication.getApplication().getService();
        subscription.setSubscriptionDataRepository(subscriptionDataRepository);
        subscription.setApplication(useApplication.getApplication());
        List<ContentKeywordDetail> updatedKeywordDetailsList = useApplication.getContentKeywordDetails();
        updatedKeywordDetailsList = subscriptionApplicationService.filterBlankContent(updatedKeywordDetailsList);
        if (!useApplication.getApplication().getService().isSubCategoriesSupported()) {
            resetSubkeywordToBlank(updatedKeywordDetailsList);
        }
        List<ContentRelease> updatedContentReleaseList = subscriptionApplicationService
                .createContentReleaseFromContentKeywordDetailsList(updatedKeywordDetailsList, subscription,
                        useApplication.getApplication().getAppId(), useApplication.getApplication().getAppName());
        subscription.updateServiceData(updatedContentReleaseList);

    }

    private void resetSubkeywordToBlank(List<ContentKeywordDetail> upatedKeywordDetailsList) {
        logger.debug("Not Supporting Subkeywords, so removing service-keyword added for display purposes");
        for (ContentKeywordDetail contentKeywordDetail : upatedKeywordDetailsList) {
            if (contentKeywordDetail.getDetails().size() != 1) {
                throw new IllegalStateException(
                        "Sub category not supported app has content with more than one category["
                                + contentKeywordDetail + "], has more than one item.");
            }
            contentKeywordDetail.getDetails().get(0).setKeyword("");
        }
    }

    private void updateAppContentAuditLog(String appType, List<KeywordDetails> msgList, ContentProvider contentProvider, HttpServletRequest request,
                                         AuditEntity.ActionStatus status) {
        List<String> msgs = new ArrayList<String>();
        for(KeywordDetails info : msgList) {
            if(!info.getDescriptionEn().isEmpty()) { msgs.add(info.getDescriptionEn()); };
        }

        audit(new AuditEntity.AuditEntityBuilder(contentProvider)
                .populateFromHttpRequest(request)
                .action(String.format(auditLogAction.getUpdateAppContentAction(), appType))
                .description(String.format(auditLogAction.getUpdateAppContentActionDesc(), appType, msgs.toString()))
                .status(status)
                .build());
    }
}
