/*
 * (C) Copyright 2010-2011 hSenid International (pvt) Limited. 
 * All Rights Reserved. 
 *
 * These materials are unpublished, proprietary, confidential source code of 
 * hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid 
 * International (pvt) Limited. 
 *
 * hSenid International (pvt) Limited retains all title to and intellectual 
 * property rights in these materials.
 *
 */

package hsenidmobile.orca.cpportal.controller.settings;


import hsenidmobile.orca.core.model.ContentProvider;
import hsenidmobile.orca.cpportal.controller.AbstractBaseController;
import hsenidmobile.orca.cpportal.module.Registration;
import hsenidmobile.orca.cpportal.service.CurrentUser;
import hsenidmobile.orca.cpportal.service.impl.ContentProviderService;
import org.apache.cxf.jaxrs.client.ClientWebApplicationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
*   $LastChangedDate$
*   $LastChangedBy$
*   $LastChangedRevision$
*/
@Controller
@SessionAttributes("registration")
public class MyProfileController extends AbstractBaseController {

    private static final Logger logger = LoggerFactory.getLogger(MyProfileController.class);
    @Autowired
    private ContentProviderService contentProviderService;

    @RequestMapping("/settings/personalDataSettings")
    public String viewProfile(@ModelAttribute("registration") Registration registration,BindingResult bindResult) throws Throwable {
        try {
            String currentUser = CurrentUser.getUsername();
            ContentProvider contentProvider = contentProviderService.getContentProviderDetails(true);
            registration.setCp(contentProvider);
            registration.setUserName(currentUser);
            return "settings/personalDataSettings";
        } catch (ClientWebApplicationException e) {
            logger.error("Error while processing the rerquest, Server error : ", e);
            return "redirect:/error.html?messageId=view.server.error";
        } catch (Throwable e) {
            logger.error("Error while loading profile information : ", e);
            return "redirect:/error.html?messageId=user.data.loding.error";
        }
    }

    @RequestMapping("/personalData/update")
    public String updateProfile(@ModelAttribute("registration") Registration registration, BindingResult bindingResult,
                                SessionStatus sessionStatus) throws Throwable {
        try {
            ContentProvider contentProvider = registration.getCp();
            if(bindingResult.hasErrors()) {
                return "settings/personalDataSettings";
            }
            if(registration.getMailType().equals("Express")) {
                contentProvider.getBillingInfo().setExpressDelivery(true);
            }
            else {
                contentProvider.getBillingInfo().setExpressDelivery(false);
            }
            contentProviderService.updateContentProvider(contentProvider);
            sessionStatus.setComplete();
            logger.debug("Content Provider Successfully Updated with [{}]", registration.toString());
            return "redirect:/settings/personalDataSettings.html";
        } catch (Throwable e) {
            logger.error("Error while updating profile information : ", e);
            return "redirect:/error.html?messageId=user.data.update.error";
        }
    }

    @ModelAttribute("registration")
    protected Object formBackingObject() throws Exception {
        Registration registration = new Registration();
        return registration;
    }
}
