/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.      AppNotFoundException
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hsenidmobile.orca.cpportal.controller.settings;

import hsenidmobile.orca.core.model.ContentProvider;
import hsenidmobile.orca.core.model.Msisdn;
import hsenidmobile.orca.core.repository.ContentProviderRepository;
import hsenidmobile.orca.core.services.impl.RoutingKeyManagerImpl;
import hsenidmobile.orca.cpportal.controller.AbstractBaseController;
import hsenidmobile.orca.cpportal.module.Registration;
import hsenidmobile.orca.cpportal.service.CurrentUser;
import hsenidmobile.orca.cpportal.service.impl.ContentProviderService;
import hsenidmobile.orca.cpportal.service.impl.MsisdnValidatorServiceImpl;
import hsenidmobile.orca.cpportal.service.impl.application.ApplicationService;
import hsenidmobile.orca.cpportal.util.logging.AuditEntity;
import hsenidmobile.orca.cpportal.util.logging.AuditLogActionProperties;
import hsenidmobile.orca.cpportal.validator.application.CreateKeywordValidator;
import hsenidmobile.orca.orm.repository.ContentProviderDetailsRepositoryImpl;
import org.apache.cxf.jaxrs.client.ClientWebApplicationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

import static hsenidmobile.orca.cpportal.util.logging.SolturaAuditLogger.audit;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
@Controller
public class MySettingController extends AbstractBaseController {

    private static final Logger logger = LoggerFactory.getLogger(MySettingController.class);
    @Autowired
    private ContentProviderDetailsRepositoryImpl cpDetailsRepository;
    @Autowired
    private ApplicationService applicationService;
    @Autowired
    private ContentProviderService contentProviderService;
    @Autowired
    private RoutingKeyManagerImpl RoutingKeyManager;
    @Autowired
    private CreateKeywordValidator createKeywordValidator;
    @Autowired
    private MsisdnValidatorServiceImpl msisdnValidatorService;
    @Autowired
    private AuditLogActionProperties auditLogAction;

    @RequestMapping(value = "/settings/mySettings")
    public ModelAndView viewMySettings (@ModelAttribute("registration") Registration registration, HttpServletRequest request,
                                        SessionStatus status) throws Throwable {
        ContentProvider cp = null;
        try {
            logger.debug("Application creation Getting current user in My Settings Home Page = [{}]", CurrentUser.getUsername());
            String userName = CurrentUser.getUsername();
            cp = cpDetailsRepository.findByUsername(userName);
            logger.trace("CP Details = [{}]", cp);
//            String verificationCode = cp.getMsisdns().iterator().next().getVerificationCode();
            registration.setCp(cp);
            registration.setShortCode(msisdnValidatorService.getShortCode());
//            logger.debug("Verification Code of  [{}] is [{}]", new Object[]{userName, verificationCode});
            settingsAcessAuditLog(cp, request, AuditEntity.ActionStatus.SUCCESS);
            return new ModelAndView("settings/mySettings");
        } catch (Throwable e) {
            logger.error("Error occurred ", e);
            settingsAcessAuditLog(cp, request, AuditEntity.ActionStatus.FAILED);
            throw e;
        }
    }

    @RequestMapping(value = "/settings/personalData")
    public String createApplicationNcs(@ModelAttribute("registration")Registration registration) {
        return "redirect:../settings/personalDataSettings.html";
    }

    @RequestMapping(value = "/settings/activate")
    public String activateConfirmationCode(@ModelAttribute("registration")Registration registration) {
        return "/settings/confirmationCodePopup";
    }

    @RequestMapping(value = "/settings/addNewNumber")
    public String addNewNumber(@ModelAttribute("registration")Registration registration) {
        return "/settings/addNewNumberPopup";
    }

    @RequestMapping(value = "/settings/keywordSettings")
    public String configureKeywordData(@ModelAttribute("registration")Registration registration, HttpServletRequest request) throws Throwable {
        ContentProvider contentProvider = null;
        try {
            contentProvider = contentProviderService.getContentProviderForSettings();

            //todo display the routing keys assigned to cp

            registration.setCp(contentProvider);
            keywordsAcessAuditLog(contentProvider, request, AuditEntity.ActionStatus.SUCCESS);
            return "/settings/keywordSettings";
        } catch (ClientWebApplicationException e) {
            logger.error("Error while processing the request, Server error : ", e);
            keywordsAcessAuditLog(contentProvider, request, AuditEntity.ActionStatus.FAILED);
            return "redirect:/error.html?messageId=view.server.error";
        } catch (Throwable e) {
            logger.error("Error while setting keyword details : ", e);
            keywordsAcessAuditLog(contentProvider, request, AuditEntity.ActionStatus.FAILED);
            return "redirect:/error.html?messageId=keyword.data.loding.error";
        }
    }

    @RequestMapping(value = "/return/keywordSettings")
    public String returnAfetrConfigureKeywordSetting(@ModelAttribute("registration")Registration registration) throws Throwable {
        try {
            ContentProvider contentProvider = contentProviderService.getContentProviderForSettings();
            registration.setCp(contentProvider);
            return "/settings/keywordSettings";
        } catch (ClientWebApplicationException e) {
            logger.error("Error while processing the rerquest, Server error : ", e);
            return "redirect:/error.html?messageId=view.server.error";
        } catch (Throwable e) {
            logger.error("Error while setting keyword details : ", e);
            return "redirect:/error.html?messageId=keyword.data.loding.error";
        }
    }

    @RequestMapping(value = "/settings/addNewKeyword")
    public String addNewKeyword(@ModelAttribute("registration")Registration registration) {
        return "/settings/addAnotherKeywordPopup";
    }

    @RequestMapping(value = "/settings/userAccountSettings")
    public String configureUserAccountData(@ModelAttribute("registration")Registration registration) {
        return "/settings/userAccountSettings";
    }

    @RequestMapping(value = "/settings/addNewAccount")
    public String addNewAccount(@ModelAttribute("registration")Registration registration) {
        return "/settings/addNewAccountPopup";
    }

    @ModelAttribute (value = "registration")
    public Registration formbackingObject() throws Exception{
        Registration registration = new Registration();
        return registration;
    }

    private void settingsAcessAuditLog(ContentProvider contentProvider, HttpServletRequest request, AuditEntity.ActionStatus status) {
        audit(new AuditEntity.AuditEntityBuilder(contentProvider)
                .populateFromHttpRequest(request)
                .action(auditLogAction.getSettingsAccessAction())
                .description(auditLogAction.getSettingsAccessActionDesc())
                .status(status)
                .build());
    }

    private void keywordsAcessAuditLog(ContentProvider contentProvider, HttpServletRequest request, AuditEntity.ActionStatus status) {
        audit(new AuditEntity.AuditEntityBuilder(contentProvider)
                .populateFromHttpRequest(request)
                .action(auditLogAction.getMyKeywordsAccessAction())
                .description(auditLogAction.getMyKeywordsAccessActionDesc())
                .status(status)
                .build());
    }
}
