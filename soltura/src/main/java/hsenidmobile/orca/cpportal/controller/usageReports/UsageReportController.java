/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hsenidmobile.orca.cpportal.controller.usageReports;

import hsenidmobile.orca.core.applications.ApplicationImpl;
import hsenidmobile.orca.core.model.ContentProvider;
import hsenidmobile.orca.core.model.MessageHistory;
import hsenidmobile.orca.core.model.RevenueSummary;
import hsenidmobile.orca.cpportal.controller.AbstractBaseController;
import hsenidmobile.orca.cpportal.module.MessageHistoryInfo;
import hsenidmobile.orca.cpportal.module.RevenueReportInfo;
import hsenidmobile.orca.cpportal.service.impl.MessageHistoryReportService;
import hsenidmobile.orca.cpportal.service.impl.RevenueReportService;
import hsenidmobile.orca.cpportal.util.logging.AuditEntity;
import hsenidmobile.orca.cpportal.util.logging.AuditLogActionProperties;
import hsenidmobile.orca.cpportal.validator.usageReports.MessageHistoryReportValidator;
import hsenidmobile.orca.cpportal.validator.usageReports.RevenueReportValidator;
import hsenidmobile.orca.orm.repository.ContentProviderDetailsRepositoryImpl;
import org.apache.cxf.jaxrs.client.ClientWebApplicationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContext;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hsenidmobile.orca.cpportal.util.SolturaUtils.reportTypes.*;
import static hsenidmobile.orca.cpportal.util.logging.SolturaAuditLogger.audit;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
@Controller
@SessionAttributes("messageHistoryInfo")
public class UsageReportController extends AbstractBaseController {

    private final static Logger logger = LoggerFactory.getLogger(UsageReportController.class);
    @Autowired
    private MessageHistoryReportValidator messageHistoryReportValidator;
    @Autowired
    private MessageHistoryReportService messageHistoryReportService;
    @Autowired
    private RevenueReportValidator revenueReportValidator;
    @Autowired
    private RevenueReportService revenueReportService;
    @Autowired
    private ContentProviderDetailsRepositoryImpl cpDetailsRepositoryImpl;
    @Autowired
    private AuditLogActionProperties auditLogAction;

    // Report type selection page
    @RequestMapping("/usageReports/selectReportType")
    public String selectReportType() {
        return "usageReports/viewReports";
    }

    // Initial load of Message History Report page
    @RequestMapping("/usageReports/messageHistoryReport")
    public String showMessageHistoryReport(@ModelAttribute("messageHistoryInfo") MessageHistoryInfo messageHistoryInfo)
            throws Throwable {
        logger.debug("Request message history for generate report [{}]", messageHistoryInfo);
        try {
            clearCache(messageHistoryInfo);
            setInitialData(messageHistoryInfo);
            return "usageReports/messageHistoryReport";
        } catch (Throwable e) {
            logger.error("Error occurred while loading Message History Report : ", e);
            return "redirect:/error.html?messageId=message.history.report.error";
        }
    }

    private void clearCache(MessageHistoryInfo messageHistoryInfo) {
        messageHistoryInfo.setSelectedApplicationIdsForCp(null);
        messageHistoryInfo.setShowAll(false);
        messageHistoryInfo.setMessageHistory(null);
    }

    private void setInitialData(MessageHistoryInfo messageHistoryInfo) {
        messageHistoryReportService.setCurrentDate(messageHistoryInfo);
        messageHistoryReportService.loadAllApplicationsForCp(messageHistoryInfo);
    }


    @ModelAttribute("messageHistoryInfo")
    public MessageHistoryInfo createCommand() {
        MessageHistoryInfo historyInfo = new MessageHistoryInfo();
        setInitialData(historyInfo);
        logger.debug("Command created for generate report [{}]", historyInfo);
        return historyInfo;
    }


    @RequestMapping(value = "/usageReports/messageHistoryReportForm", method = RequestMethod.GET)
    public String showMessageHistoryReportGetRequest(@ModelAttribute("messageHistoryInfo") MessageHistoryInfo messageHistoryInfo,
                                                     HttpServletRequest request) {
        RequestContext ctx = new RequestContext(request);
        messageHistoryReportService.formatMessageContent(messageHistoryInfo.getMessageHistory(), ctx);
        logger.debug("Get request for message history form [{}]", messageHistoryInfo);
        return "usageReports/messageHistoryReport";
    }
    
    // Form is submitted in Message History Report page
    @RequestMapping(value = "/usageReports/messageHistoryReportForm", method = RequestMethod.POST)
    public String showMessageHistoryReport(@ModelAttribute("messageHistoryInfo") MessageHistoryInfo messageHistoryInfo,
                                           BindingResult bindingResult,
                                           HttpServletRequest request) throws Throwable {
        ContentProvider contentProvider = null;
        try {
            RequestContext ctx = new RequestContext(request);
            contentProvider = cpDetailsRepositoryImpl.getContentProvider();
            messageHistoryReportService.loadAllApplicationsForCp(messageHistoryInfo);
            messageHistoryReportValidator.validate(messageHistoryInfo, bindingResult);
            if (bindingResult.hasErrors()) {
                return "usageReports/messageHistoryReport";
            }
            messageHistoryReportService.getMessageHistoryForApps(messageHistoryInfo, ctx);
            msgHistoryAuditLog(messageHistoryInfo, contentProvider, request, AuditEntity.ActionStatus.SUCCESS);
            return "usageReports/messageHistoryReport";
        } catch (Throwable e) {
            logger.error("Error occurred while displaying search results for Message History Report : ", e);
            msgHistoryAuditLog(messageHistoryInfo, contentProvider, request, AuditEntity.ActionStatus.FAILED);
            return "redirect:/error.html?messageId=message.history.report.error";
        }
    }

    @RequestMapping("/usageReports/messageHistoryReportForm/{format}")
    public ModelAndView exportReport(@PathVariable String format,
                                     @ModelAttribute("messageHistoryInfo") MessageHistoryInfo messageHistoryInfo,
                                     BindingResult bindingResult,
                                     HttpServletRequest request) throws Throwable {
        logger.info("Resolving report for type [{}]", format);
        try {
            RequestContext ctx = new RequestContext(request);
            messageHistoryReportService.loadAllApplicationsForCp(messageHistoryInfo);
            messageHistoryReportValidator.validate(messageHistoryInfo, bindingResult);
            if (bindingResult.hasErrors()) {
                return new ModelAndView("messageHistoryReport", "messageHistoryInfo", messageHistoryInfo);
            }
            messageHistoryReportService.getMessageHistoryForApps(messageHistoryInfo, ctx);

            Map<String, List<MessageHistory>> historyData = new HashMap<String, List<MessageHistory>>();
            historyData.put("historyData", messageHistoryInfo.getMessageHistory());
            if (format.equals(PDF.name())) {
                return new ModelAndView("pdfMessageHistorySummary", "historyReportData", historyData);
            } else if (format.equals(EXCEL.name())) {
                return new ModelAndView("excelMessageHistorySummary", "historyReportData", historyData);
            } else if (format.equals(CSV.name())) {
                return new ModelAndView("csvMessageHistorySummary", "historyReportData", historyData);
            } else {
                logger.warn("Unsupported Exporting Option found");
                throw new RuntimeException("Unsupported Report Format Request Found");
            }
        } catch (ClientWebApplicationException e) {
            logger.error("Error while processing the rerquest, Server error : ", e);
            return null;
        } catch (Throwable e) {
            logger.error("Error occurred ", e);
            throw e;
        }
    }

    // Initial load of Revenue Report page
    @RequestMapping("/usageReports/revenueReport")
    public String showRevenueReport(@ModelAttribute("revenueReportInfo") RevenueReportInfo revenueReportInfo)
            throws Throwable {
        try {
            revenueReportService.setCurrentDate(revenueReportInfo);
            revenueReportService.loadAllApplicationsForCp(revenueReportInfo);
            return "usageReports/applicationRevenueReport";
        } catch (Throwable e) {
            logger.error("Error occurred while loading Revenue Report : ", e);
            return "redirect:/error.html?messageId=revenue.summary.report.error";
        }
    }

    // Form is submitted in Revenue Report page
    @RequestMapping("/usageReports/revenueReportForm")
    public String showRevenueReport(@ModelAttribute("revenueReportInfo") RevenueReportInfo revenueReportInfo,
                                    BindingResult bindingResult) throws Throwable {
        logger.info("revenueReportForm submitted with following details : [{}]", revenueReportInfo);
        try {
            logger.info("Finding revenue report : [{}]", revenueReportInfo);
            revenueReportService.loadAllApplicationsForCp(revenueReportInfo);
            revenueReportValidator.validate(revenueReportInfo, bindingResult);
            if (bindingResult.hasErrors()) {
                return "usageReports/applicationRevenueReport";
            } else {
                logger.info("Validation successful generating revenue report : [{}]", revenueReportInfo);
            }
            revenueReportService.getRevenueReportForApps(revenueReportInfo);
            return "usageReports/applicationRevenueReport";
        } catch (Throwable e) {
            logger.error("Error occurred while displaying search results for Revenue Report : ", e);
            return "redirect:/error.html?messageId=revenue.summary.report.error";
        }
    }

    @RequestMapping("/usageReports/revenueReportForm/{format}")
    public ModelAndView exportRevenueReport(@PathVariable String format,
                                            @ModelAttribute("revenueReportInfo") RevenueReportInfo revenueReportInfo,
                                            BindingResult bindingResult) throws Throwable {
        try {
            logger.info("Finding revenue report : [{}]", revenueReportInfo);
            revenueReportService.loadAllApplicationsForCp(revenueReportInfo);
            if (bindingResult.hasErrors()) {
                return new ModelAndView("usageReports/applicationRevenueReport", "revenueReportInfo", revenueReportInfo);
            }
            revenueReportService.getRevenueReportForApps(revenueReportInfo);
            Map<String, List<RevenueSummary>> revenueData = new HashMap<String, List<RevenueSummary>>();
            revenueData.put("revenueData", revenueReportInfo.getRevenueSummaries());
            if (format.equals(PDF.name())) {
                return new ModelAndView("pdfRevenueSummarySummary", "revenueReportData", revenueData);
            } else if (format.equals(EXCEL.name())) {
                return new ModelAndView("excelRevenueSummarySummary", "revenueReportData", revenueData);
            } else if (format.equals(CSV.name())) {
                return new ModelAndView("csvRevenueSummarySummary", "revenueReportData", revenueData);
            } else {
                logger.warn("Unsupported Exporting Option found");
                throw new RuntimeException("Unsupported Report Format Request Found");
            }
        } catch (Throwable e) {
            logger.error("Error occurred ", e);
            throw e;
        }
    }

    private void msgHistoryAuditLog(MessageHistoryInfo msgHistoryInfo, ContentProvider contentProvider, HttpServletRequest request, AuditEntity.ActionStatus status) {
        String info = "";
        if(msgHistoryInfo != null) {
            info = String.format("Apps - [%s], From Date - %s, To Date - %s, Show All - %s",
                    msgHistoryInfo.getSelectedApplicationIdsForCp(), msgHistoryInfo.getReportFromDate(), msgHistoryInfo.getReportToDate(),
                    msgHistoryInfo.isShowAll());
        }
        audit(new AuditEntity.AuditEntityBuilder(contentProvider)
                .populateFromHttpRequest(request)
                .action(auditLogAction.getViewMsgHistoryAction())
                .description(String.format(auditLogAction.getViewMsgHistoryActionDesc(), info))
                .status(status)
                .build());
    }
}
