/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hsenidmobile.orca.cpportal.filter;

import hsenidmobile.orca.core.applications.ApplicationImpl;
import hsenidmobile.orca.core.model.Status;


public class ApplicationFilter implements CustomFilter {

	@Override
	public boolean match(Object o) {
		ApplicationImpl application = (ApplicationImpl) o;
		Status status = application.getStatus();
		return status == Status.RETIRED || status == Status.EXPIRED || status == Status.TERMINATED
				|| status == Status.PROV_APP_NOT_FOUND;
	}
}
