/*
 *   (C) Copyright 200-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hsenidmobile.orca.cpportal.filter;

import hms.common.registration.api.response.BasicUserResponseMessage;
import hsenidmobile.orca.core.model.ContentProvider;
import hsenidmobile.orca.core.model.Msisdn;
import hsenidmobile.orca.core.model.Status;
import hsenidmobile.orca.core.repository.AppRepository;
import hsenidmobile.orca.orm.repository.ContentProviderDetailsRepositoryImpl;
import hsenidmobile.orca.rest.registration.transport.CommonRegistrationRequestSender;
import hsenidmobile.orca.rest.sdp.sp.transport.SpManageRequestSender;
import org.apache.cxf.jaxrs.client.ClientWebApplicationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static hms.common.registration.api.util.RestApiKeys.EMAIL;
import static hms.common.registration.api.util.RestApiKeys.MSISDN;
import static hms.common.registration.api.util.RestApiKeys.USERNAME;
import static hsenidmobile.orca.rest.common.util.RestApiKeys.*;

/**
 * $LastChangedDate: 2011-05-11 15:44:27 +0530 (Wed, 11 May 2011) $
 * $LastChangedBy: supunh $
 * $LastChangedRevision: 73079 $
 */
public class AuthenticationSuccessHandlerImpl extends SimpleUrlAuthenticationSuccessHandler {

    @Autowired
    protected CommonRegistrationRequestSender commonRegistrationRequestSender;
    @Autowired
    protected ContentProviderDetailsRepositoryImpl cpDetailsRepositoryImpl;
    @Autowired
    protected AppRepository appRepository;
    @Autowired
    protected SpManageRequestSender spManageRequestSender;

    private ContentProvider contentProvider;
    final String destination  ="loginFailError.html?messageId=";

    private static final Logger logger = LoggerFactory.getLogger(AuthenticationSuccessHandlerImpl.class);

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                                        Authentication authentication) throws IOException, ServletException {
        if (authentication != null) {
            String userName = authentication.getName();
            setDefaultTargetUrl("/index.jsp");
            contentProvider = cpDetailsRepositoryImpl.getContentProvider();
            contentProvider.setName(userName);
            try {
                BasicUserResponseMessage basicUserResponseMessage = commonRegistrationRequestSender.getBasicUserResponseMessage(userName);
                addUserDetailsToSession(request, basicUserResponseMessage);
                if (basicUserResponseMessage != null) {
                    setCpDetails(basicUserResponseMessage);
                    Map<String, Object> sdpResponse = sdpAuthentication();
                    if (sdpResponse.get(STATUS).equals(ALLOWED)) {
                        contentProvider.setSpId((String) sdpResponse.get(SP_ID));
                        logger.debug("setting the application by the sp-id [" + contentProvider.getSpId() + "]");
                        contentProvider.setApplications(appRepository.findCpAppBySpid(contentProvider.getSpId()));
                        super.onAuthenticationSuccess(request, response, authentication);
                    } else {
                        Map<String, String> errorCode = getErrorCodes();
                        String url = destination + errorCode.get(sdpResponse.get(STATUS_CODE));
                        response.sendRedirect(response.encodeRedirectURL(url));
                    }
                }
            } catch (ClientWebApplicationException e) {
                logger.error("Error while processing the request for getting User ID", e);
                Map<String, String> errorCode = getErrorCodes();
                String url = destination + errorCode.get(SP_LOGIN_ERROR_CODE_E1601);
                response.sendRedirect(response.encodeRedirectURL(url));
            }
        }
    }

    private void addUserDetailsToSession(HttpServletRequest request, BasicUserResponseMessage basicUserResponseMessage) {
        logger.error(basicUserResponseMessage.toString());
        String lastLoginTime;

        String username = basicUserResponseMessage.getAdditionalDataMap().get(USERNAME);

        if (basicUserResponseMessage.getAdditionalDataMap().get(LAST_LOGIN_TIME) == null) {
            lastLoginTime = "";
        } else {
            /*Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(Long.valueOf(basicUserResponseMessage.getAdditionalDataMap().get(LAST_LOGIN_TIME)));*/
            try {
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd h:mm:ss");
                Date date = dateFormat.parse(basicUserResponseMessage.getAdditionalDataMap().get(LAST_LOGIN_TIME));
                lastLoginTime = date.toString();
            } catch (ParseException e) {
                logger.error("Parse exception occurred when parsing a string to date: [{}]", e);
                lastLoginTime = "";
            }
        }
        request.getSession().setAttribute(LAST_LOGIN_TIME, lastLoginTime);
        request.getSession().setAttribute(CONTACT_PERSON_NAME, basicUserResponseMessage.getAdditionalDataMap().get(CONTACT_PERSON_NAME));
        request.getSession().setAttribute(FIRST_NAME, basicUserResponseMessage.getAdditionalDataMap().get(FIRST_NAME));
        request.getSession().setAttribute(LAST_NAME, basicUserResponseMessage.getAdditionalDataMap().get(LAST_NAME));
        request.getSession().setAttribute(DISPLAY_NAME, basicUserResponseMessage.getAdditionalDataMap().get(DISPLAY_NAME) == null
                ? username : basicUserResponseMessage.getAdditionalDataMap().get(DISPLAY_NAME));
    }

    private void setCpDetails(BasicUserResponseMessage response){
        contentProvider.setCpId(response.getCorporateUserId());
        /*setUserStatus(response);*/
        contentProvider.setStatus(Status.ACTIVE);
        if(response.getAdditionalDataMap() != null){
            contentProvider.setEmail(response.getAdditionalDataMap().get(EMAIL));
            contentProvider.setUserType(response.getType().name());

            Msisdn cpMsisdn = new Msisdn();
            cpMsisdn.setAddress(response.getAdditionalData(MSISDN));
            contentProvider.setMsisdn(cpMsisdn);
        } else {
            logger.error("Additional data cannot be resolved please check your registration details.");
        }
    }

    private Map<String, Object> sdpAuthentication(){
        try {
            Map<String, Object> params = new HashMap<String, Object >();
            params.put(CP_ID, contentProvider.getCpId());
            Map<String, Object> response = spManageRequestSender.authenticateSpbyCpid(params);
            return response;
        } catch (ClientWebApplicationException e) {
            logger.error("Error while processing the request for getuserDI (commonRegistrationRequestSender):", e);
            throw e;
        }
    }

    private Map<String, String> getErrorCodes(){
        Map<String, String> errorCode = new HashMap<String, String>();
        errorCode.put(SP_LOGIN_ERROR_CODE_E1812, SP_LOGIN_ERROR_CODE_PREFIX + "E1812");
        errorCode.put(SP_LOGIN_ERROR_CODE_E1811, SP_LOGIN_ERROR_CODE_PREFIX + "E1811");
        errorCode.put(SP_LOGIN_ERROR_CODE_E1302, SP_LOGIN_ERROR_CODE_PREFIX + "E1302");
        errorCode.put(SP_LOGIN_ERROR_CODE_E1601, SP_LOGIN_ERROR_CODE_PREFIX + "E1601");
        errorCode.put(SP_LOGIN_ERROR_CODE_E1819, SP_LOGIN_ERROR_CODE_PREFIX + "E1819");
        return errorCode;
    }
}