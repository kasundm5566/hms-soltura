/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hsenidmobile.orca.cpportal.filter;

import hsenidmobile.orca.core.model.RoutingInfo;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class AvailableRoutingKeyFilter implements CustomFilter {

    @Override
    public boolean match(Object o) {
        RoutingInfo routingInfo = (RoutingInfo) o;
        return routingInfo.getEndDate() < System.currentTimeMillis();
    }
}
