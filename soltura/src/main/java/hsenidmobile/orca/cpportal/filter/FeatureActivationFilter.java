/*
 *   (C) Copyright 2008-2012 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate: $
 *   $LastChangedBy: $
 *   $LastChangedRevision: $
 *
 */
package hsenidmobile.orca.cpportal.filter;

import hsenidmobile.orca.cpportal.util.FeatureListLoader;

import java.io.IOException;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import hsenidmobile.orca.cpportal.util.SolturaUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * $LastChangedDate: $ $LastChangedBy: $ $LastChangedRevision: $
 * <c:if test="${fl['New Dialog Feature']}">
 */
public class FeatureActivationFilter implements Filter {

	private static final Logger logger = LoggerFactory.getLogger(FeatureActivationFilter.class);
	private Map<String, Boolean> featureList;

	public FeatureActivationFilter(FeatureListLoader flLoader) {
		super();
		this.featureList = flLoader.getFeatureList();
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		logger.info("Initialized FeatureActivationFilter");

	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException,
			ServletException {
		HttpServletRequest request = (HttpServletRequest) req;
		HttpSession session = request.getSession(false);
        if (session != null) {
			Object fl = session.getAttribute(SolturaUtils.FEATURE_LIST_SESSION_KEY);
			if (fl == null) {
				logger.info("Set the System Feature list to the session");
				session.setAttribute(SolturaUtils.FEATURE_LIST_SESSION_KEY, featureList);
			}
		}
		chain.doFilter(req, resp);

	}

	@Override
	public void destroy() {
	}

}
