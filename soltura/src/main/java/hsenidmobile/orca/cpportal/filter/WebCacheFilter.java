/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 */
package hsenidmobile.orca.cpportal.filter;

/**
 * $LastChangedDate:$
 * $LastChangedBy:$
 * $LastChangedRevision:$
 */

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WebCacheFilter implements Filter {

    private Logger log = LoggerFactory.getLogger(WebCacheFilter.class);

    public void init(FilterConfig filterConfig) throws ServletException {
        log.debug("Initialling web caching filter.....");
    }

    public void doFilter(ServletRequest servletRequest, ServletResponse
            servletResponse, FilterChain filterChain) throws IOException, ServletException {
        filterChain.doFilter(servletRequest, servletResponse);
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        response.setHeader("Pragma", "no-cache"); //HTTP 1.0
        response.setHeader("Cache-Control", "no-cache"); //HTTP 1.1
        response.setDateHeader("Expires", 0); //prevents caching at the proxy server
        response.setHeader("Cache-Control", "private"); // HTTP 1.1
        response.setHeader("Cache-Control", "no-store"); // HTTP 1.1
        response.setHeader("Cache-Control", "max-stale=0"); // HTTP 1.1
    }

    public void destroy() {
    }
}
