/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 */
package hsenidmobile.orca.cpportal.module;

import hsenidmobile.orca.core.applications.ApplicationImpl;
import hsenidmobile.orca.core.applications.PeriodUnit;
import hsenidmobile.orca.core.model.AvailableShortCodeInfo;
import hsenidmobile.orca.core.model.CpFinancialInstrument;
import hsenidmobile.orca.core.model.RoutingKey;
import hsenidmobile.orca.cpportal.service.IntegratedDbCreateApplictionServiceImpl;
import hsenidmobile.orca.cpportal.util.SubscriptionChargingType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class ApplicationInfo {

    public ApplicationInfo() {
        responseSpecification = new ArrayList<ResponseSpecification>();
        responseSpecification.add(ResponseSpecification.SUMMERY_RESPOND);
        responseSpecification.add(ResponseSpecification.NO_RESPOND);
    }

    private ApplicationImpl app;

    private List<AvailableShortCodeInfo> availableShortCodeInfos;
    private List<ResponseSpecification> responseSpecification;
    private String serviceType;
    private List<KeywordDetails> selectedValues;
    private List<KeywordDetails> existingSelectedValues;
    private List<CpFinancialInstrument> availableFinancialInstrumentList;
    private List<String> selectedRoutingKeyValues;
    private List<String> assignedRoutingKeyValues;
    private List<PeriodUnit> periodUnitList;

    private String startDate;
    private String endDate;
    private String requestAppResponseMsg;
    private String OperatorforCreateNewKeyword;
    private String unsubscribeSuccessMsgEn;
    private String subscriptionSuccessMsgEn;
    private String selectedResponseSpecification = ResponseSpecification.SUMMERY_RESPOND.name();
    private PeriodUnit selectedPeriodUnit;
    private HashMap<String,List<RoutingKey>> availableRoutingKeyValues;
    private int subscriptionDispatchDay;
    private int subscriptionDispatchMinute;
    private int subscriptionDispatchDate;
    private int subscriptionDispatchHour;

    protected String successVotingRespondHeader;
    protected String successVotingRespondFooter;
    private boolean smsUpdate;
    private boolean onePerNumber;
    private boolean editState;
    private boolean readOnly;
    private boolean subCategoryRequired;
    private boolean advanceConfigurationRequired;
    private boolean pageHasErrors = true;
    private SubscriptionChargingType subscriptionChargingType;
    private SubscriptionChargingType[] subscriptionChargingTypeArray;
    private SubscriptionScheduleType subscriptionScheduleType;
    private int maxNumOfVotesPerSubscriber;
    private String isMsisdnVerifyAllowedOnAppCreate;
    private String spMsisdnVerified;
    private String serviceUrl;

    public String getServiceUrl() {
        return serviceUrl;
    }

    public void setServiceUrl(String serviceUrl) {
        this.serviceUrl = serviceUrl;
    }

    public String getMsisdnVerifyAllowedOnAppCreate() {
        return isMsisdnVerifyAllowedOnAppCreate;
    }

    public void setMsisdnVerifyAllowedOnAppCreate(String msisdnVerifyAllowedOnAppCreate) {
        isMsisdnVerifyAllowedOnAppCreate = msisdnVerifyAllowedOnAppCreate;
    }

    public String getSpMsisdnVerified() {
        return spMsisdnVerified;
    }

    public void setSpMsisdnVerified(String spMsisdnVerified) {
        this.spMsisdnVerified = spMsisdnVerified;
    }

    private IntegratedDbCreateApplictionServiceImpl transactionPointService;

    public boolean isReadOnly() {
        return readOnly;
    }

    public void setReadOnly(boolean readOnly) {
        this.readOnly = readOnly;
    }

    public SubscriptionChargingType[] getSubscriptionTypeArray() {
        return subscriptionChargingTypeArray;
    }

    public void setSubscriptionTypeArray(SubscriptionChargingType[] subscriptionChargingTypeArray) {
        this.subscriptionChargingTypeArray = subscriptionChargingTypeArray;
    }

    public int getSubscriptionDispatchDay() {
        return subscriptionDispatchDay;
    }

    public void setSubscriptionDispatchDay(int subscriptionDispatchDay) {
        this.subscriptionDispatchDay = subscriptionDispatchDay;
    }

    public int getSubscriptionDispatchHour() {
        return subscriptionDispatchHour;
    }

    public void setSubscriptionDispatchHour(int subscriptionDispatchHour) {
        this.subscriptionDispatchHour = subscriptionDispatchHour;
    }

    public int getSubscriptionDispatchMinute() {
        return subscriptionDispatchMinute;
    }

    public void setSubscriptionDispatchMinute(int subscriptionDispatchMinute) {
        this.subscriptionDispatchMinute = subscriptionDispatchMinute;
    }

    public int getSubscriptionDispatchDate() {
        return subscriptionDispatchDate;
    }

    public void setSubscriptionDispatchDate(int subscriptionDispatchDate) {
        this.subscriptionDispatchDate = subscriptionDispatchDate;
    }


//    public int getSubscriptionDispatchBufferTime() {
//        return subscriptionDispatchBufferTime;
//    }
//
//    public void setSubscriptionDispatchBufferTime(int subscriptionDispatchBufferTime) {
//        this.subscriptionDispatchBufferTime = subscriptionDispatchBufferTime;
//    }

    public PeriodUnit getSelectedPeriodUnit() {
        return selectedPeriodUnit;
    }

    public void setSelectedPeriodUnit(PeriodUnit selectedPeriodUnit) {
        this.selectedPeriodUnit = selectedPeriodUnit;
    }

    public List<PeriodUnit> getPeriodUnitList() {
        return periodUnitList;
    }

    public void setPeriodUnitList(List<PeriodUnit> periodUnitList) {
        this.periodUnitList = periodUnitList;
    }

    public List<KeywordDetails> getSelectedValues() {
        return selectedValues;
    }

    public void setSelectedValues(List<KeywordDetails> selectedValues) {
        this.selectedValues = selectedValues;
    }

    public List<KeywordDetails> getExistingSelectedValues() {
        return existingSelectedValues;
    }

    public void setExistingSelectedValues(List<KeywordDetails> existingSelectedValues) {
        this.existingSelectedValues = existingSelectedValues;
    }

    public ApplicationImpl getApp() {
        return app;
    }

    public void setApp(ApplicationImpl app) {
        this.app = app;
    }

    public boolean isSmsUpdate() {
        return smsUpdate;
    }

    public void setSmsUpdate(boolean smsUpdate) {
        this.smsUpdate = smsUpdate;
    }

    public boolean isOnePerNumber() {
        return onePerNumber;
    }

    public void setOnePerNumber(boolean onePerNumber) {
        this.onePerNumber = onePerNumber;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public boolean isEditState() {
        return editState;
    }

    public void setEditState(boolean editState) {
        this.editState = editState;
    }

    public List<AvailableShortCodeInfo> getAvailableShortCodes() {
        return availableShortCodeInfos;
    }

    public void setAvailableShortCodes(List<AvailableShortCodeInfo> availableShortCodeInfos) {
        this.availableShortCodeInfos = availableShortCodeInfos;
    }

    public List<ResponseSpecification> getResponseSpecification() {
        return responseSpecification;
    }

    public void setResponseSpecification(List<ResponseSpecification> responseSpecification) {
        this.responseSpecification = responseSpecification;
    }

    public String getSelectedResponseSpecification() {
        return selectedResponseSpecification;
    }

    public void setSelectedResponseSpecification(String selectedResponseSpecification) {
        this.selectedResponseSpecification = selectedResponseSpecification;
    }

    public String getSubscriptionSuccessMsgEn() {
        return subscriptionSuccessMsgEn;
    }

    public void setSubscriptionSuccessMsgEn(String subscriptionSuccessMsgEn) {
        this.subscriptionSuccessMsgEn = subscriptionSuccessMsgEn;
    }

    public String getUnsubscribeSuccessMsgEn() {
        return unsubscribeSuccessMsgEn;
    }

    public void setUnsubscribeSuccessMsgEn(String unsubscribeSuccessMsgEn) {
        this.unsubscribeSuccessMsgEn = unsubscribeSuccessMsgEn;
    }

//    public String getRequestErrorMsgEn() {
//        return requestErrorMsgEn;
//    }
//
//    public void setRequestErrorMsgEn(String requestErrorMsgEn) {
//        this.requestErrorMsgEn = requestErrorMsgEn;
//    }

    public String getSuccessVotingRespondHeader() {
        return successVotingRespondHeader;
    }

    public void setSuccessVotingRespondHeader(String successVotingRespondHeader) {
        this.successVotingRespondHeader = successVotingRespondHeader;
    }

    public String getSuccessVotingRespondFooter() {
        return successVotingRespondFooter;
    }

    public void setSuccessVotingRespondFooter(String successVotingRespondFooter) {
        this.successVotingRespondFooter = successVotingRespondFooter;
    }

    public SubscriptionChargingType getSubscriptionChargingType() {
        return subscriptionChargingType;
    }

    public void setSubscriptionChargingType(SubscriptionChargingType subscriptionChargingType) {
        this.subscriptionChargingType = subscriptionChargingType;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public int getMaxNumOfVotesPerSubscriber() {
        return maxNumOfVotesPerSubscriber;
    }

    public void setMaxNumOfVotesPerSubscriber(int maxNumOfVotesPerSubscriber) {
        this.maxNumOfVotesPerSubscriber = maxNumOfVotesPerSubscriber;
    }

    public SubscriptionScheduleType getSubscriptionScheduleType() {
        return subscriptionScheduleType;
    }

    public void setSubscriptionScheduleType(SubscriptionScheduleType subscriptionScheduleType) {
        this.subscriptionScheduleType = subscriptionScheduleType;
    }

    public boolean isSubCategoryRequired() {
        return subCategoryRequired;
    }

    public void setSubCategoryRequired(boolean subCategoryRequired) {
        this.subCategoryRequired = subCategoryRequired;
    }

    public String getRequestAppResponseMsg() {
        return requestAppResponseMsg;
    }

    public void setRequestAppResponseMsg(String requestAppResponseMsg) {
        this.requestAppResponseMsg = requestAppResponseMsg;
    }

    public boolean isAdvanceConfigurationRequired() {
        return advanceConfigurationRequired;
    }

    public void setAdvanceConfigurationRequired(boolean advanceConfigurationRequired) {
        this.advanceConfigurationRequired = advanceConfigurationRequired;
    }

    public boolean isPageHasErrors() {
        return pageHasErrors;
    }

    public void setPageHasErrors(boolean pageHasErrors) {
        this.pageHasErrors = pageHasErrors;
    }

    public List<CpFinancialInstrument> getAvailableFinancialInstrumentList() {
        return availableFinancialInstrumentList;
    }

    public void setAvailableFinancialInstrumentList(List<CpFinancialInstrument> availableFinancialInstrumentList) {
        this.availableFinancialInstrumentList = availableFinancialInstrumentList;
    }

    public enum SubscriptionScheduleType {
        SEND_IMMEDIATELY, SEND_EVERY_HOURS, SEND_EVERY_DAYS, SEND_EVERY_WEEK;
    }

    public HashMap<String,List<RoutingKey>> getAvailableRoutingKeyValues() {
        return availableRoutingKeyValues;
    }

    public void setAvailableRoutingKeyValues(HashMap<String, List<RoutingKey>> availableRoutingKeyValues) {
        this.availableRoutingKeyValues = availableRoutingKeyValues;
    }

    public List<String> getSelectedRoutingKeyValues() {
        return selectedRoutingKeyValues;
    }

    public void setSelectedRoutingKeyValues(List<String> selectedRoutingKeyValues) {
        this.selectedRoutingKeyValues = selectedRoutingKeyValues;
    }

    public List<String> getAssignedRoutingKeyValues() {
        return assignedRoutingKeyValues;
    }

    public void setAssignedRoutingKeyValues(List<String> assignedRoutingKeyValues) {
        this.assignedRoutingKeyValues = assignedRoutingKeyValues;
    }

    @Override
    public String toString() {
        StringBuilder sb= null;
        if (selectedRoutingKeyValues !=null && selectedRoutingKeyValues.size()>0) {
            sb = new StringBuilder(selectedRoutingKeyValues.size()*2);

            for (String s : selectedRoutingKeyValues) {
                sb.append(s);
                sb.append(" || ");
            }
        }


        return "ApplicationInfo{" +
                "app=" + app +
                ", availableShortCodeInfos=" + availableShortCodeInfos +
                ", selectedValues=" + selectedValues +
                ", periodUnitList=" + periodUnitList +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", selectedPeriodUnit=" + selectedPeriodUnit +
                ", smsUpdate=" + smsUpdate +
                ", onePerNumber=" + onePerNumber +
                ", editState=" + editState +
                ", pageHasErrors=" + pageHasErrors +
                ", selectedRoutingKeys=" + selectedRks(sb) +
                '}';
    }

    public String selectedRks(StringBuilder sb) {
        if (sb != null) {
            return sb.toString();
        }
        return "";
    }

    public IntegratedDbCreateApplictionServiceImpl getTransactionPointService() {
        return transactionPointService;
    }

    public void setTransactionPointService(IntegratedDbCreateApplictionServiceImpl transactionPointService) {
        this.transactionPointService = transactionPointService;
    }
}
