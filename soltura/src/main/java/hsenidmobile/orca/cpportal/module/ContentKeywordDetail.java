/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 */
package hsenidmobile.orca.cpportal.module;

import hsenidmobile.orca.core.applications.subscription.Periodicity;
import hsenidmobile.orca.core.applications.subscription.SubscriptionContentRelease;
import static hsenidmobile.orca.core.applications.subscription.SubscriptionContentRelease.ContentReleaseStatus.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class ContentKeywordDetail {

    public enum ContentStatus {
        /**
         * Content is currently in DB and scheduled to be dispatched.
         * */
        SCHEDULED,
        /**
         * Content is currently in DB and NOT-scheduled to be dispatched.
         * */
        DISCARDED,
        /**
         * New content.
         * */
        NEW
    }

    private Long contentGroupId = null;
    private String showPeriod;
    private Periodicity periodicity;
    private List<KeywordDetails> details;
    private String lastSentOn;
    private Date scheduledDate;
    private ContentStatus status = ContentStatus.NEW;

    public Long getContentGroupId() {
        return contentGroupId;
    }

    public void setContentGroupId(Long contentGroupId) {
        this.contentGroupId = contentGroupId;
    }

    public String getShowPeriod() {
        return showPeriod;
    }

    public void setShowPeriod(String showPeriod) {
        this.showPeriod = showPeriod;
    }

    public Periodicity getPeriodicity() {
        return periodicity;
    }

    public void setPeriodicity(Periodicity periodicity) {
        this.periodicity = periodicity;
    }

    public List<KeywordDetails> getDetails() {
        return details;
    }

    public void setDetails(List<KeywordDetails> details) {
        this.details = details;
    }

    public void addKeyword(KeywordDetails keywordDetails) {
        if (details == null) {
            details = new ArrayList<KeywordDetails>();
        }
        details.add(keywordDetails);
    }

    public String getLastSentOn() {
        return lastSentOn;
    }

    public void setLastSentOn(String lastSentOn) {
        this.lastSentOn = lastSentOn;
    }

    public Date getScheduledDate() {
        return scheduledDate;
    }

    public void setScheduledDate(Date scheduledDate) {
        this.scheduledDate = scheduledDate;
    }

    public ContentStatus getStatus() {
        return status;
    }

    public void setStatus(ContentStatus status) {
        this.status = status;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(250);
        builder.append("ContentKeywordDetail [contentGroupId=");
        builder.append(contentGroupId);
        builder.append(", showPeriod=");
        builder.append(showPeriod);
        builder.append(", periodicity=");
        builder.append(periodicity);
        builder.append(", details=");
        builder.append(details);
        builder.append(", lastSentOn=");
        builder.append(lastSentOn);
        builder.append(", scheduledDate=");
        builder.append(scheduledDate);
        builder.append(", status=");
        builder.append(status);
        builder.append("]");
        return builder.toString();
    }

    /**
     * Check whether the content is empty.
     *
     * @return - Return true if English description for all categories are
     *         blank.
     */
    public boolean isEmpty() {
        if (details == null || details.isEmpty()) {
            return true;
        } else {
            for (KeywordDetails kd : details) {
                if (kd.getDescriptionEn() != null && kd.getDescriptionEn().trim().length() > 0) {
                    return false;
                }
            }
        }
        return true;
    }

    public void setStatus(SubscriptionContentRelease.ContentReleaseStatus contentGroupStatus) {
        if (contentGroupStatus == FUTURE) {
            this.status = ContentStatus.SCHEDULED;
        } else if (contentGroupStatus == DISCARDED) {
            this.status = ContentStatus.DISCARDED;
        } else {
            this.status = ContentStatus.NEW;
        }
    }
}
