/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.      AppNotFoundException
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hsenidmobile.orca.cpportal.module;

import hsenidmobile.orca.core.model.AvailableShortCodeInfo;
import hsenidmobile.orca.core.model.RoutingInfo;

import java.util.List;
import java.util.Map;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class CreateKeyword extends RoutingInfo {

    private List<AvailableShortCodeInfo> availableShortCodeInfos;

    private String  sDate;
    private String eDate;
    private String chargingCycleSize;
    private boolean pageHasErrors=true;
    private boolean appLevel;
    private Map<String, List<String>> availableOperatorSpecificShortCodeMap;

    public List<AvailableShortCodeInfo> getAvailableShortCodes() {
        return availableShortCodeInfos;
    }

    public void setAvailableShortCodes(List<AvailableShortCodeInfo> availableShortCodeInfos) {
        this.availableShortCodeInfos = availableShortCodeInfos;
    }

    public Map<String, List<String>> getAvailableOperatorSpecificShortCodeMap() {
        return availableOperatorSpecificShortCodeMap;
    }

    public void setAvailableOperatorSpecificShortCodeMap(Map<String, List<String>> availableOperatorSpecificShortCodeMap) {
        this.availableOperatorSpecificShortCodeMap = availableOperatorSpecificShortCodeMap;
    }

    public String getsDate() {
        return sDate;
    }

    public void setsDate(String sDate) {
        this.sDate = sDate;
    }

    public String geteDate() {
        return eDate;
    }

    public void seteDate(String eDate) {
        this.eDate = eDate;
    }

    public boolean isPageHasErrors() {
        return pageHasErrors;
    }

    public void setPageHasErrors(boolean pageHasErrors) {
        this.pageHasErrors = pageHasErrors;
    }

    public boolean isAppLevel() {
        return appLevel;
    }

    public void setAppLevel(boolean appLevel) {
        this.appLevel = appLevel;
    }

	public String getChargingCycleSize() {
		return chargingCycleSize;
	}

	public void setChargingCycleSize(String chargingCycleSize) {
		this.chargingCycleSize = chargingCycleSize;
	}
}
