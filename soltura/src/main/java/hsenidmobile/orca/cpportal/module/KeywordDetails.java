/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 */
package hsenidmobile.orca.cpportal.module;

import java.util.Date;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class KeywordDetails {

	private String keyword;
	private String descriptionEn;
	private String descriptionTa;
	private String descriptionSi;
	private String lastSentOn;
	private long numOfusersRegisterd;

	public KeywordDetails(){

	}

	public KeywordDetails(String keyword, String descriptionEn) {
		this.keyword = keyword;
		this.descriptionEn = descriptionEn;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getDescriptionEn() {
		return descriptionEn;
	}

	public void setDescriptionEn(String descriptionEn) {
		this.descriptionEn = descriptionEn;
	}

	public String getDescriptionTa() {
		return descriptionTa;
	}

	public void setDescriptionTa(String descriptionTa) {
		this.descriptionTa = descriptionTa;
	}

	public String getDescriptionSi() {
		return descriptionSi;
	}

	public void setDescriptionSi(String descriptionSi) {
		this.descriptionSi = descriptionSi;
	}

    public String getLastSentOn() {
        return lastSentOn;
    }

    public void setLastSentOn(String lastSentOn) {
        this.lastSentOn = lastSentOn;
    }

    public long getNumOfusersRegisterd() {
		return numOfusersRegisterd;
	}

	public void setNumOfusersRegisterd(long numOfusersRegisterd) {
		this.numOfusersRegisterd = numOfusersRegisterd;
	}

	public void clearDescriptions() {
		descriptionEn = "";
		descriptionTa = "";
		descriptionSi = "";
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(150);
		builder.append("KeywordDetails [keyword=");
		builder.append(keyword);
		builder.append(", descriptionEn=");
		builder.append(descriptionEn);
		builder.append(", descriptionTa=");
		builder.append(descriptionTa);
		builder.append(", descriptionSi=");
		builder.append(descriptionSi);
		builder.append(", lastSentOn=");
		builder.append(lastSentOn);
		builder.append(", numOfusersRegisterd=");
		builder.append(numOfusersRegisterd);
		builder.append("]");
		return builder.toString();
	}

}
