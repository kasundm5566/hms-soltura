/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.cpportal.module;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class Login {

    private String username;
    private String password;
    private boolean rememberMe;

    private String email;
    private String subject;
    private String company;
    private String country;
    private String contact;
    private String message;
    private String demoVersion;
    private String isEmailSentSuccessfully;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isRememberMe() {
        return rememberMe;
    }

    public void setRememberMe(boolean rememberMe) {
        this.rememberMe = rememberMe;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDemoVersion() {
        return demoVersion;
    }

    public void setDemoVersion(String demoVersion) {
        this.demoVersion = demoVersion;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getIsEmailSentSuccessfully() {
        return isEmailSentSuccessfully;
    }

    public void setEmailSentSuccessfully(String emailSentSuccessfully) {
        isEmailSentSuccessfully = emailSentSuccessfully;
    }

    @Override
    public String toString() {
        return "Login{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", rememberMe=" + rememberMe +
                ", email='" + email + '\'' +
                ", subject='" + subject + '\'' +
                ", company='" + company + '\'' +
                ", country='" + country + '\'' +
                ", contact='" + contact + '\'' +
                ", message='" + message + '\'' +
                ", demoVersion='" + demoVersion + '\'' +
                '}';
    }
}
