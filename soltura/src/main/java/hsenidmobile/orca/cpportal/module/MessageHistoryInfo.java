/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hsenidmobile.orca.cpportal.module;

import hsenidmobile.orca.core.model.MessageHistory;
import hsenidmobile.orca.core.applications.ApplicationImpl;

import java.util.List;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class MessageHistoryInfo {

    private List<ApplicationImpl> availableApplicationsForCp;
    private String reportFromDate;
    private String reportToDate;
    private boolean showAll;

    private List<String> selectedApplicationIdsForCp;

    private List<MessageHistory> messageHistory;

    public List<MessageHistory> getMessageHistory() {
        return messageHistory;
    }

    public void setMessageHistory(List<MessageHistory> messageHistory) {
        this.messageHistory = messageHistory;
    }

    public List<ApplicationImpl> getAvailableApplicationsForCp() {
        return availableApplicationsForCp;
    }

    public void setAvailableApplicationsForCp(List<ApplicationImpl> availableApplicationsForCp) {
        this.availableApplicationsForCp = availableApplicationsForCp;
    }

    public String getReportFromDate() {
        return reportFromDate;
    }

    public void setReportFromDate(String reportFromDate) {
        this.reportFromDate = reportFromDate;
    }

    public String getReportToDate() {
        return reportToDate;
    }

    public void setReportToDate(String reportToDate) {
        this.reportToDate = reportToDate;
    }

    public boolean isShowAll() {
        return showAll;
    }

    public void setShowAll(boolean showAll) {
        this.showAll = showAll;
    }

    public List<String> getSelectedApplicationIdsForCp() {
        return selectedApplicationIdsForCp;
    }

    public void setSelectedApplicationIdsForCp(List<String> selectedApplicationIdsForCp) {
        this.selectedApplicationIdsForCp = selectedApplicationIdsForCp;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("MessageHistoryInfo");
        sb.append("{availableApplicationsForCp=").append(availableApplicationsForCp);
        sb.append(", reportFromDate='").append(reportFromDate).append('\'');
        sb.append(", reportToDate='").append(reportToDate).append('\'');
        sb.append(", showAll=").append(showAll);
        sb.append(", selectedApplicationIdsForCp=").append(selectedApplicationIdsForCp);
        sb.append(", messageHistory=").append(messageHistory);
        sb.append('}');
        return sb.toString();
    }
}
