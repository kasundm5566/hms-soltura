/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
package hsenidmobile.orca.cpportal.module;

import hsenidmobile.orca.core.model.ContentProvider;

import java.util.List;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class Registration {

    private List<String> availableOperators;
    private ContentProvider cp;
    private String userName;
    private String password;
    private String newPassword;
    private String confirmPassword;
    private String captchaValue;
    private String name;
    private String email;
    private String nic;
    private String telephone;
    private String contactPersonName;
    private String fax;
    private String registerCode;
    private String operatorType;
    private String chargingNumber;
    private String billingName;
    private String billingAddress;
    private String mailType;
    private String registrationType;
    private String verificationCode;
    private String cpSentVerificationCode;
    private String shortCode;
    private boolean pageHasErrors =true;
    private boolean editState = false;
    private String firstName;
    private String lastName;
    private String birthday;
    private String gender;
    private String country;
    private String loc;
    private String businessNumber;
    private List<String> locations;
    private String notifMessage;

    public ContentProvider getCp() {
        return cp;
    }

    public void setCp(ContentProvider cp) {
        this.cp = cp;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNic() {
        return nic;
    }

    public void setNic(String nic) {
        this.nic = nic;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getContactPersonName() {
        return contactPersonName;
    }

    public void setContactPersonName(String contactPersonName) {
        this.contactPersonName = contactPersonName;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getRegisterCode() {
        return registerCode;
    }

    public void setRegisterCode(String registerCode) {
        this.registerCode = registerCode;
    }

    public String getOperatorType() {
        return operatorType;
    }

    public void setOperatorType(String operatorType) {
        this.operatorType = operatorType;
    }

    public String getChargingNumber() {
        return chargingNumber;
    }

    public void setChargingNumber(String chargingNumber) {
        this.chargingNumber = chargingNumber;
    }

    public String getBillingName() {
        return billingName;
    }

    public void setBillingName(String billingName) {
        this.billingName = billingName;
    }

    public String getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(String billingAddress) {
        this.billingAddress = billingAddress;
    }


    public String getMailType() {
        return mailType;
    }

    public void setMailType(String mailType) {
        this.mailType = mailType;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public List<String> getAvailableOperators() {
        return availableOperators;
    }

    public void setAvailableOperators(List<String> availableOperators) {
        this.availableOperators = availableOperators;
    }

    public String getCaptchaValue() {
        return captchaValue;
    }

    public void setCaptchaValue(String captchaValue) {
        this.captchaValue = captchaValue;
    }

    public String getRegistrationType() {
        return registrationType;
    }

    public void setRegistrationType(String registrationType) {
        this.registrationType = registrationType;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public boolean isPageHasErrors() {
        return pageHasErrors;
    }

    public void setPageHasErrors(boolean pageHasErrors) {
        this.pageHasErrors = pageHasErrors;
    }

    public boolean isEditState() {
        return editState;
    }

    public void setEditState(boolean editState) {
        this.editState = editState;
    }

    public String getVerificationCode() {
        return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
    }

    public String getCpSentVerificationCode() {
        return cpSentVerificationCode;
    }

    public void setCpSentVerificationCode(String cpSentVerificationCode) {
        this.cpSentVerificationCode = cpSentVerificationCode;
    }

    public String getShortCode() {
        return shortCode;
    }

    public void setShortCode(String shortCode) {
        this.shortCode = shortCode;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getLoc() {
        return loc;
    }

    public void setLoc(String loc) {
        this.loc = loc;
    }

    public String getBusinessNumber() {
        return businessNumber;
    }

    public void setBusinessNumber(String businessNumber) {
        this.businessNumber = businessNumber;
    }

    public String getNotifMessage() {
        return notifMessage;
    }

    public void setNotifMessage(String notifMessage) {
        this.notifMessage = notifMessage;
    }

    public List<String> getLocations() {
        return locations;
    }

    public void setLocations(List<String> locations) {
        this.locations = locations;
    }

    @Override
    public String toString() {
        return "Registration{" +
                "userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", confirmPassword='" + confirmPassword + '\'' +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", nic='" + nic + '\'' +
                ", telephone=" + telephone +
                ", contactPersonName='" + contactPersonName + '\'' +
                ", fax=" + fax +
                ", registerCode='" + registerCode + '\'' +
                ", operatorType='" + operatorType + '\'' +
                ", chargingNumber=" + chargingNumber +
                ", billingName='" + billingName + '\'' +
                ", billingAddress='" + billingAddress + '\'' +
                ", mailType='" + mailType + '\'' +
                ", registrationType='" + registrationType + '\'' +
                '}';
    }


    public void clear() {
        this.pageHasErrors = true;
        this.email = null;
        this.userName = null;
        this.password = null;
        this.confirmPassword = null;
        this.firstName = null;
    }
}
