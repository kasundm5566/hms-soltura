/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hsenidmobile.orca.cpportal.module;

import hsenidmobile.orca.core.applications.ApplicationImpl;
import hsenidmobile.orca.core.model.RevenueSummary;

import java.util.List;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class RevenueReportInfo {

    private List<ApplicationImpl> availableApplicationsForCp;
    private List<String> availableSubCategories;
    private String reportFromDate;
    private String reportToDate;
    private boolean showAll;
    private List<String> selectedApplicationIdsForCp;
    private List<RevenueSummary> revenueSummaries;

    public List<ApplicationImpl> getAvailableApplicationsForCp() {
        return availableApplicationsForCp;
    }

    public void setAvailableApplicationsForCp(List<ApplicationImpl> availableApplicationsForCp) {
        this.availableApplicationsForCp = availableApplicationsForCp;
    }

    public List<String> getAvailableSubCategories() {
        return availableSubCategories;
    }

    public void setAvailableSubCategories(List<String> availableSubCategories) {
        this.availableSubCategories = availableSubCategories;
    }

    public String getReportFromDate() {
        return reportFromDate;
    }

    public void setReportFromDate(String reportFromDate) {
        this.reportFromDate = reportFromDate;
    }

    public String getReportToDate() {
        return reportToDate;
    }

    public void setReportToDate(String reportToDate) {
        this.reportToDate = reportToDate;
    }

    public boolean isShowAll() {
        return showAll;
    }

    public void setShowAll(boolean showAll) {
        this.showAll = showAll;
    }

    public List<String> getSelectedApplicationIdsForCp() {
        return selectedApplicationIdsForCp;
    }

    public void setSelectedApplicationIdsForCp(List<String> selectedApplicationIdsForCp) {
        this.selectedApplicationIdsForCp = selectedApplicationIdsForCp;
    }

    public List<RevenueSummary> getRevenueSummaries() {
        return revenueSummaries;
    }

    public void setRevenueSummaries(List<RevenueSummary> revenueSummaries) {
        this.revenueSummaries = revenueSummaries;
    }

    @Override
    public String toString() {
        return "RevenueReportInfo{" +
                "availableApplicationsForCp=" + availableApplicationsForCp +
                ", availableSubCategories=" + availableSubCategories +
                ", reportFromDate='" + reportFromDate +
                ", reportToDate='" + reportToDate +
                ", showAll=" + showAll +
                ", selectedApplicationIdsForCp=" + selectedApplicationIdsForCp +
                ", revenueSummaries=" + revenueSummaries +
                '}';
    }
}
