/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 */
package hsenidmobile.orca.cpportal.module;

import hsenidmobile.orca.core.applications.ApplicationImpl;
import hsenidmobile.orca.core.applications.subscription.Periodicity;

import java.util.List;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class UseApplication {

    private ApplicationImpl application;
    private Periodicity periodicity;
    private KeywordDetails keywordDetails;
    private List<ContentKeywordDetail> contentKeywordDetails;
    private List<KeywordDetails> alertKeywordDetails;
    private String text;
    private String serviceType;
    private String message;
    private int messageMaxLength;
    private boolean subCategoryRequired;

    public ApplicationImpl getApplication() {
        return application;
    }

    public void setApplication(ApplicationImpl application) {
        this.application = application;
    }

    public Periodicity getPeriodicity() {
        return periodicity;
    }

    public void setPeriodicity(Periodicity periodicity) {
        this.periodicity = periodicity;
    }

    public KeywordDetails getKeywordDetails() {
        return keywordDetails;
    }

    public void setKeywordDetails(KeywordDetails keywordDetails) {
        this.keywordDetails = keywordDetails;
    }

    public List<ContentKeywordDetail> getContentKeywordDetails() {
        return contentKeywordDetails;
    }

    public void setContentKeywordDetails(List<ContentKeywordDetail> contentKeywordDetails) {
        this.contentKeywordDetails = contentKeywordDetails;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public List<KeywordDetails> getAlertKeywordDetails() {
        return alertKeywordDetails;
    }

    public void setAlertKeywordDetails(List<KeywordDetails> alertKeywordDetails) {
        this.alertKeywordDetails = alertKeywordDetails;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getMessageMaxLength() {
        return messageMaxLength;
    }

    public void setMessageMaxLength(int messageMaxLength) {
        this.messageMaxLength = messageMaxLength;
    }

    public boolean isSubCategoryRequired() {
        return subCategoryRequired;
    }

    public void setSubCategoryRequired(boolean subCategoryRequired) {
        this.subCategoryRequired = subCategoryRequired;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
