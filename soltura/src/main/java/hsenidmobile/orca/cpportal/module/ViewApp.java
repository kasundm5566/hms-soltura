/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 */
package hsenidmobile.orca.cpportal.module;

import hsenidmobile.orca.core.applications.voting.VoteResultSummary;
import hsenidmobile.orca.core.model.DispatchSummary;
import hsenidmobile.orca.core.model.SubCategory;

import java.util.Date;
import java.util.List;
import java.util.Map;
/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class ViewApp {

    private String appId;
    private String LastUpdated;
    private String serviceType;
    private String winner;
    private String requestServiceResponseMessage;
    private String appName;
    private String startDate;
    private String routingKeyExpiredDate;
    private String endDate;
    private String pageNumber;
    private String operator;
    private String keyword;
    private String numOfTotalRegRequest;
    private String shortCode;
    private String chargingMsisdn;
    private String description;
    private String messageFrequency;
    private long receivedMsgCount;
    private long noOfInavlidRequests;
    private long numOfTotalMsgsSent;
    private long numOfSuccessMsgsSent;
    private long numOfFailedMsgsSent;
    private long numOfSuccessRegRequest;
    private long numOfFailedRegRequest;
    private long winnerVoteCount;
    private long requestServiceId;
    private long totalSuccssfulVoteCount;
    private boolean smsUpdate;
    private boolean commonResponseAvailable;
    private boolean subCategoryRequired;
    private boolean oneVotePerNumber;
    private List<SubCategory> subCategoryCodeList;
    private List<ContentKeywordDetail> contentKeywordDetails;
    private List<KeywordDetails> alertContentDetails;
    private List<Map<String, String>> selectedKeywordList;
    private List<ViewRequest> viewRequests;
    private List<KeywordDetails> subscriptionContentDetails;
    private List<SubCategory> keywordDetails;
    private List<VoteResultSummary> voteResults;
    private DispatchSummary dispatchSummary;
    private Map<String, Long> candidatesVote;
    private Map<String, Long> subscriptionSummaryMap;
    private double revenue;
    private int numberOfSubCategories;
    private int totalNumberOfRecords;
    private Date lastUpdatedTime;

    private boolean isExpirable;

    public List<Map<String, String>> getSelectedKeywordList() {
        return selectedKeywordList;
    }

    public void setSelectedKeywordList(List<Map<String, String>> selectedKeywordList) {
        this.selectedKeywordList = selectedKeywordList;
    }

    public long getWinnerVoteCount() {
        return winnerVoteCount;
    }

    public void setWinnerVoteCount(long winnerVoteCount) {
        this.winnerVoteCount = winnerVoteCount;
    }

    public int getTotalNumberOfRecords() {
        return totalNumberOfRecords;
    }

    public void setTotalNumberOfRecords(int totalNumberOfRecords) {
        this.totalNumberOfRecords = totalNumberOfRecords;
    }

    public long getRequestServiceId() {
        return requestServiceId;
    }

    public void setRequestServiceId(long requestServiceId) {
        this.requestServiceId = requestServiceId;
    }

    public List<ViewRequest> getViewRequests() {
        return viewRequests;
    }

    public void setViewRequests(List<ViewRequest> viewRequest) {
        this.viewRequests = viewRequest;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public long getNoOfInavlidRequests() {
        return noOfInavlidRequests;
    }

    public void setNoOfInavlidRequests(long noOfInavlidRequests) {
        this.noOfInavlidRequests = noOfInavlidRequests;
    }

    public List<VoteResultSummary> getVoteResults() {
        return voteResults;
    }

    public void setVoteResults(List<VoteResultSummary> voteResults) {
        this.voteResults = voteResults;
    }

    public String getLastUpdated() {
        return LastUpdated;
    }

    public void setLastUpdated(String lastUpdated) {
        LastUpdated = lastUpdated;
    }

    public long getReceivedMsgCount() {
        return receivedMsgCount;
    }

    public void setReceivedMsgCount(long receivedMsgCount) {
        this.receivedMsgCount = receivedMsgCount;
    }

    public Map<String, Long> getCandidatesVote() {
        return candidatesVote;
    }

    public void setCandidatesVote(Map<String, Long> candidatesVote) {
        this.candidatesVote = candidatesVote;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public DispatchSummary getDispatchSummary() {
        return dispatchSummary;
    }

    public void setDispatchSummary(DispatchSummary dispatchSummary) {
        this.dispatchSummary = dispatchSummary;
    }

    public Map getSubscriptionSummaryMap() {
        return subscriptionSummaryMap;
    }

    public void setSubscriptionSummaryMap(Map subscriptionSummaryMap) {
        this.subscriptionSummaryMap = subscriptionSummaryMap;
    }

    public List<SubCategory> getKeywordDetails() {
        return keywordDetails;
    }

    public void setKeywordDetails(List<SubCategory> keywordDetails) {
        this.keywordDetails = keywordDetails;
    }

    public double getRevenue() {
        return revenue;
    }

    public void setRevenue(double revenue) {
        this.revenue = revenue;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getShortCode() {
        return shortCode;
    }

    public void setShortCode(String shortCode) {
        this.shortCode = shortCode;
    }

    public String getChargingMsisdn() {
        return chargingMsisdn;
    }

    public void setChargingMsisdn(String chargingMsisdn) {
        this.chargingMsisdn = chargingMsisdn;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getNumberOfSubCategories() {
        return numberOfSubCategories;
    }

    public void setNumberOfSubCategories(int numberOfSubCategories) {
        this.numberOfSubCategories = numberOfSubCategories;
    }

    public List<SubCategory> getSubCategoryCodeList() {
        return subCategoryCodeList;
    }

    public void setSubCategoryCodeList(List<SubCategory> subCategoryCodeList) {
        this.subCategoryCodeList = subCategoryCodeList;
    }

    public String getMessageFrequency() {
        return messageFrequency;
    }

    public void setMessageFrequency(String messageFrequency) {
        this.messageFrequency = messageFrequency;
    }

    public boolean isSmsUpdate() {
        return smsUpdate;
    }

    public void setSmsUpdate(boolean smsUpdate) {
        this.smsUpdate = smsUpdate;
    }

    public List<ContentKeywordDetail> getContentKeywordDetails() {
        return contentKeywordDetails;
    }

    public void setContentKeywordDetails(List<ContentKeywordDetail> contentKeywordDetails) {
        this.contentKeywordDetails = contentKeywordDetails;
    }

    public long getNumOfTotalMsgsSent() {
        return numOfTotalMsgsSent;
    }

    public void setNumOfTotalMsgsSent(long numOfTotalMsgsSent) {
        this.numOfTotalMsgsSent = numOfTotalMsgsSent;
    }

    public long getNumOfSuccessMsgsSent() {
        return numOfSuccessMsgsSent;
    }

    public void setNumOfSuccessMsgsSent(long numOfSuccessMsgsSent) {
        this.numOfSuccessMsgsSent = numOfSuccessMsgsSent;
    }

    public long getNumOfFailedMsgsSent() {
        return numOfFailedMsgsSent;
    }

    public void setNumOfFailedMsgsSent(long numOfFailedMsgsSent) {
        this.numOfFailedMsgsSent = numOfFailedMsgsSent;
    }

    public String getNumOfTotalRegRequest() {
        return numOfTotalRegRequest;
    }

    public void setNumOfTotalRegRequest(String numOfTotalRegRequest) {
        this.numOfTotalRegRequest = numOfTotalRegRequest;
    }

    public long getNumOfSuccessRegRequest() {
        return numOfSuccessRegRequest;
    }

    public void setNumOfSuccessRegRequest(long numOfSuccessRegRequest) {
        this.numOfSuccessRegRequest = numOfSuccessRegRequest;
    }

    public long getNumOfFailedRegRequest() {
        return numOfFailedRegRequest;
    }

    public void setNumOfFailedRegRequest(long numOfFailedRegRequest) {
        this.numOfFailedRegRequest = numOfFailedRegRequest;
    }

    public List<KeywordDetails> getAlertContentDetails() {
        return alertContentDetails;
    }

    public void setAlertContentDetails(List<KeywordDetails> alertContentDetails) {
        this.alertContentDetails = alertContentDetails;
    }

    public List<KeywordDetails> getSubscriptionContentDetails() {
        return subscriptionContentDetails;
    }

    public void setSubscriptionContentDetails(List<KeywordDetails> subscriptionContentDetails) {
        this.subscriptionContentDetails = subscriptionContentDetails;
    }

    public long getTotalSuccssfulVoteCount() {
        return totalSuccssfulVoteCount;
    }

    public void setTotalSuccssfulVoteCount(long totalSuccssfulVoteCount) {
        this.totalSuccssfulVoteCount = totalSuccssfulVoteCount;
    }

    public Date getLastUpdatedTime() {
        return lastUpdatedTime;
    }

    public void setLastUpdatedTime(Date lastUpdatedTime) {
        this.lastUpdatedTime = lastUpdatedTime;
    }

    public String getWinner() {
        return winner;
    }

    public void setWinner(String winner) {
        this.winner = winner;
    }

    public boolean isOneVotePerNumber() {
        return oneVotePerNumber;
    }

    public void setOneVotePerNumber(boolean oneVotePerNumber) {
        this.oneVotePerNumber = oneVotePerNumber;
    }

    public boolean isSubCategoryRequired() {
        return subCategoryRequired;
    }

    public void setSubCategoryRequired(boolean subCategoryRequired) {
        this.subCategoryRequired = subCategoryRequired;
    }

    public String getRoutingKeyExpiredDate() {
        return routingKeyExpiredDate;
    }

    public void setRoutingKeyExpiredDate(String routingKeyExpiredDate) {
        this.routingKeyExpiredDate = routingKeyExpiredDate;
    }

    public String getRequestServiceResponseMessage() {
        return requestServiceResponseMessage;
    }

    public void setRequestServiceResponseMessage(String requestServiceResponseMessage) {
        this.requestServiceResponseMessage = requestServiceResponseMessage;
    }

    public boolean isCommonResponseAvailable() {
        return commonResponseAvailable;
    }

    public void setCommonResponseAvailable(boolean commonResponseAvailable) {
        this.commonResponseAvailable = commonResponseAvailable;
    }

    public String getPageNumber() {

        return pageNumber;
    }

    public void setPageNumber(String pageNumber) {
        this.pageNumber = pageNumber;
    }

    public boolean isExpirable() {
        return isExpirable;
    }

    public void setExpirable(boolean expirable) {
        isExpirable = expirable;
    }
}
