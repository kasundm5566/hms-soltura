/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hsenidmobile.orca.cpportal.service;

import hsenidmobile.orca.core.applications.PeriodUnit;
import hsenidmobile.orca.core.model.ContentProvider;
import hsenidmobile.orca.core.model.CpFinancialInstrument;
import hsenidmobile.orca.cpportal.module.ApplicationInfo;
import hsenidmobile.orca.cpportal.module.KeywordDetails;
import hsenidmobile.orca.cpportal.service.impl.application.ApplicationService;
import hsenidmobile.orca.cpportal.util.Json2Java;
import hsenidmobile.orca.cpportal.util.SubscriptionChargingType;
import hsenidmobile.orca.cpportal.util.SubscriptionChargingTypeConfiguration;
import hsenidmobile.orca.rest.sdp.routing.transport.SdpRkManageRequestSender;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hsenidmobile.orca.rest.common.util.RestApiKeys.SP_ID;

//import static hsenidmobile.orca.cpportal.service.mock.SdpRoutingKeyRepository.getAvailableRks;

/**
 * $LastChangedDate$ $LastChangedBy$ $LastChangedRevision$
 */
public class AbstractApplicationInfoBuilder {

	@Autowired
	protected ApplicationService applicationService;
	@Autowired
	protected SdpRkManageRequestSender sdpRkManageRequestSender;
	@Autowired
	protected SubscriptionChargingTypeConfiguration subsChargingTypeConfig;

	/**
	 * Initial keyword count for sub category
	 *
	 * @return
	 */
	protected List<KeywordDetails> getInitialKeywords(ArrayList<KeywordDetails> availableList) {
		int availableSize = 0;
		if (availableList != null) {
			availableSize = availableList.size();
		}
		List<KeywordDetails> list = new ArrayList<KeywordDetails>();
		for (int i = 0; i < 12 - availableSize; i++) {
			list.add(new KeywordDetails());
		}
		return list;
	}

	/**
	 * Converts the enum type to string so that it can be shown in UI
	 *
	 * @return
	 */
	protected ArrayList<PeriodUnit> addPeriodUnits() {
		ArrayList<PeriodUnit> periodUnits = new ArrayList<PeriodUnit>();
		periodUnits.add(PeriodUnit.HOUR);
		periodUnits.add(PeriodUnit.DAY);
		periodUnits.add(PeriodUnit.WEEK);
		periodUnits.add(PeriodUnit.MONTH);
		return periodUnits;
	}

	protected void setFinancialInstrumentsForCp(ApplicationInfo applicationInfo) {
		List<CpFinancialInstrument> cpFinancialInstruments = applicationService.getContentProviderDetails(true)
				.getAvailableFinancialInstruments();
		applicationInfo.setAvailableFinancialInstrumentList(cpFinancialInstruments);
	}

	protected void setInitialKeyword(ApplicationInfo applicationInfo) {
		ContentProvider providerDetails = applicationService.getContentProviderDetails(true);
		applicationInfo.setAvailableRoutingKeyValues(providerDetails.getUnassignedRoutingKeys());
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put(SP_ID, providerDetails.getSpId());
		Map<String, Map<String, List<String>>> findRksResponse = sdpRkManageRequestSender.findAvailableRks(parameters);
		applicationInfo.setSelectedRoutingKeyValues(new ArrayList<String>(Json2Java.getAvailableRoutingKeys(
				findRksResponse).size()));
	}

	protected void setAvailableSubscriptionTypes(ApplicationInfo applicationInfo) {
//		SubscriptionChargingType[] subscriptionChargingTypeArray = new SubscriptionChargingType[3];
//		subscriptionChargingTypeArray[0] = SubscriptionChargingType.DAILY_SUBS_CHARGING;
//		subscriptionChargingTypeArray[1] = SubscriptionChargingType.WEEKLY_SUBS_CHARGING;
//		subscriptionChargingTypeArray[2] = SubscriptionChargingType.MONTHLY_SUBS_CHARGING;
//		applicationInfo.setSubscriptionTypeArray(subscriptionChargingTypeArray);
		applicationInfo.setSubscriptionTypeArray(subsChargingTypeConfig.getSupportedSubscriptionChargingTypes());
	}
}
