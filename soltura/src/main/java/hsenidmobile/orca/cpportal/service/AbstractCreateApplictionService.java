/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hsenidmobile.orca.cpportal.service;

import hsenidmobile.orca.core.applications.ApplicationImpl;
import hsenidmobile.orca.core.applications.exception.ApplicationException;
import hsenidmobile.orca.core.model.Application;
import hsenidmobile.orca.core.model.ContentProvider;
import hsenidmobile.orca.core.repository.AppRepository;
import hsenidmobile.orca.core.services.RoutingKeyManager;
import hsenidmobile.orca.cpportal.module.ApplicationInfo;
import hsenidmobile.orca.cpportal.module.KeywordDetails;
import hsenidmobile.orca.cpportal.util.MailSender;
import hsenidmobile.orca.orm.repository.ContentProviderDetailsRepositoryImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static hsenidmobile.orca.core.model.Status.REJECTED;
import static hsenidmobile.orca.core.model.Status.REQUEST_FOR_APPROVAL;

public abstract class AbstractCreateApplictionService extends BaseService {
    @Autowired
    protected ContentProviderDetailsRepositoryImpl cpDetailsRepository;
    @Autowired
    protected AppRepository appRepository;
    @Autowired
    protected RoutingKeyManager routingKeyManager;
    @Autowired
    protected MailSender mailSender;
    private static final Logger logger = LoggerFactory.getLogger(AbstractCreateApplictionService.class);

    public abstract void createApplication(ContentProvider providerDetails, ApplicationInfo appInfo, ApplicationImpl app, String userName) throws Exception;

    public abstract void editApplication(ApplicationImpl application, List<KeywordDetails> keywords) throws Exception;

    protected void doEditApplication(ApplicationImpl application, List<KeywordDetails> keywords, ContentProvider providerDetails) throws ApplicationException {
        if (application.getStatus() == REJECTED) {
            application.setStatus(REQUEST_FOR_APPROVAL);
            mailSender.sendRequestForApprovalEmail(providerDetails.getName(), application.getAppName(),
                    providerDetails.getEmail());
        }
        appRepository.updateApplication(application);
        logger.info("orca application created creating SDP application [{}]", application);
        editSdpApplication(application, keywords);
        logger.info("sending email");
        if (application.getStatus() == REJECTED) {
            mailSender.sendRequestForApprovalEmail(providerDetails.getName(), application.getAppName(),
                    providerDetails.getEmail());
        }
        logger.info("SDP application created successfully committing transactions");
    }

    protected void editSdpApplication(Application updatedApplication, List<KeywordDetails> keywords) throws ApplicationException {

        logger.debug("========================= Editing SDP application ==============================");
        //todo need to update with the newly added routing keys and subcategories if needed 
//        profileService.updateProductionApp(updatedApplication,
//                getNewlyAddedSubCategory(keywords));
        logger.debug("=======SDP application edited successfully for App Id [{}] ======", updatedApplication.getAppId());
    }
//
//    private List<SubKeyword> getNewlyAddedSubCategory(List<KeywordDetails> keywords) throws
//            RoutingKeyNotFoundException, DuplicateShortcodeException, KwForShortcodeExceedException, ProvisioningException {
//        List<SubKeyword> subKeywordList = new ArrayList<SubKeyword>();
//        if (keywords != null && !(keywords.isEmpty())) {
//            logger.debug("Newly added keywords size [{}]", keywords.size());
//            for (KeywordDetails subKeywordEntry : keywords) {
//                logger.debug("subKeywordEntry [{}]", subKeywordEntry.getKeyword());
//                if (!(subKeywordEntry.getKeyword().trim().isEmpty())) {
//                    subKeywordList.add(new SubKeyword(subKeywordEntry.getKeyword()));
//                }
//            }
//        } else {
//            logger.debug("No newly added sub categories found");
//        }
//        return subKeywordList;
//    }
}
