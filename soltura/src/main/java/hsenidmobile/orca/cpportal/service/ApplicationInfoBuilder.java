/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hsenidmobile.orca.cpportal.service;

import hsenidmobile.orca.core.applications.NcsType;
import hsenidmobile.orca.core.applications.alert.AlertService;
import hsenidmobile.orca.core.applications.request.RequestService;
import hsenidmobile.orca.core.applications.subscription.Subscription;
import hsenidmobile.orca.core.applications.voting.VotingService;
import hsenidmobile.orca.core.model.ApplicationChargingInfo;
import hsenidmobile.orca.core.model.OrcaException;
import hsenidmobile.orca.cpportal.module.ApplicationInfo;
import hsenidmobile.orca.cpportal.module.ResponseSpecification;
import hsenidmobile.orca.cpportal.util.ServiceTypes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static hsenidmobile.orca.cpportal.util.SubscriptionChargingType.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class ApplicationInfoBuilder extends AbstractApplicationInfoBuilder {

    private static final Logger logger = LoggerFactory.getLogger(ApplicationInfoBuilder.class);

    public final static SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

    private String frmSubscriberPerMsgDefaultChgValue="PI";
    private String allowMsisdnVerifyOnAppCreate = "";


    public void setAllowMsisdnVerifyOnAppCreate(String allowMsisdnVerifyOnAppCreate) {
        this.allowMsisdnVerifyOnAppCreate = allowMsisdnVerifyOnAppCreate;
    }

    public void setFrmSubscriberPerMsgDefaultChgValue(String frmSubscriberPerMsgDefaultChgValue) {
		this.frmSubscriberPerMsgDefaultChgValue = frmSubscriberPerMsgDefaultChgValue;
	}

	public ApplicationInfo getInitialApplicationInfo() throws Exception {
        ApplicationInfo applicationInfo = applicationService.initialDataGenerator();
        final Date date = new Date();
        Calendar calendarAddMonth = Calendar.getInstance();
        calendarAddMonth.add(Calendar.MONTH, 1);
        applicationInfo.setStartDate(dateFormat.format(date));
        applicationInfo.setEndDate(dateFormat.format(calendarAddMonth.getTime()));
        applicationInfo.setEditState(false);
        applicationInfo.setSelectedValues(getInitialKeywords(null));
        applicationInfo.setPeriodUnitList(addPeriodUnits());
        logger.debug("Initial date for Cp: [{}] application Data : [{}]", new Object[]{CurrentUser.getUsername(),applicationInfo});
        return applicationInfo;
    }

    public void setAppTypeInitialData(ApplicationInfo applicationInfo, String ncsType) throws OrcaException {
        applicationInfo.getApp().setNcsType(NcsType.valueOf(ncsType));
        setFinancialInstrumentsForCp(applicationInfo);
        setInitialKeyword(applicationInfo);
        setAvailableSubscriptionTypes(applicationInfo);
        ApplicationChargingInfo chargingInfo = new ApplicationChargingInfo();
        chargingInfo.setFromSubscriberChargingModel(subsChargingTypeConfig.getFromSubscriberChargingModel());
        chargingInfo.setFrmSubscriberChargingValue(subsChargingTypeConfig.getFrmSubsctiberDefaultChargingType());
		applicationInfo.getApp().setApplicationChargingInfo(chargingInfo);
        if (ServiceTypes.REQUEST.name().equals(applicationInfo.getServiceType()) ||
                ServiceTypes.VOTING.name().equals(applicationInfo.getServiceType())) {
            applicationInfo.getApp().getApplicationChargingInfo().setChargingForMessage(true);
        }
        applicationInfo.getApp().getApplicationChargingInfo().setChargingFromSubscriber(true);
        applicationInfo.getApp().getApplicationChargingInfo().setFrmSubscriberPerMsgChargingValue(frmSubscriberPerMsgDefaultChgValue);
        applicationInfo.getApp().setExpirableApp(false);
        applicationInfo.getApp().setRequireCommonResponse(false);
    }

    public void setInitialDataForSubscription(ApplicationInfo applicationInfo) {
        applicationInfo.getApp().setService(new Subscription());
        applicationInfo.setServiceType(ServiceTypes.SUBSCRIPTION.name());
        //made default subscription charging type as free
        applicationInfo.setSubscriptionChargingType(MONTHLY_SUBS_CHARGING);
        //made default charging to per message charging
        applicationInfo.getApp().getApplicationChargingInfo().setChargingForSubscription(true);
        applicationInfo.getApp().setRegistrationRequired(true);
        applicationInfo.setReadOnly(false);
        applicationInfo.setSubCategoryRequired(false);
    }

    public void setInitialDataForVoting(ApplicationInfo applicationInfo) {
        applicationInfo.getApp().setService(new VotingService());
        applicationInfo.setServiceType(ServiceTypes.VOTING.name());
        applicationInfo.setSubscriptionChargingType(MONTHLY_SUBS_CHARGING);
        applicationInfo.getApp().getApplicationChargingInfo().setFrmSubscriberPerMsgChargingValue(frmSubscriberPerMsgDefaultChgValue);
        applicationInfo.getApp().getApplicationChargingInfo().setChargingForMessage(true);
        applicationInfo.setReadOnly(false);
        applicationInfo.setSubCategoryRequired(true);
    }

    public void setInitialDataForAlert(ApplicationInfo applicationInfo) {
        applicationInfo.getApp().setService(new AlertService());
        applicationInfo.setServiceType(ServiceTypes.ALERT.name());
        applicationInfo.setSubscriptionChargingType(MONTHLY_SUBS_CHARGING);
        applicationInfo.getApp().getApplicationChargingInfo().setChargingForSubscription(true);
        applicationInfo.getApp().setRegistrationRequired(true);
        applicationInfo.setReadOnly(false);
        applicationInfo.setSubCategoryRequired(false);
        applicationInfo.setSelectedResponseSpecification(ResponseSpecification.SUMMERY_RESPOND.toString());
    }

    public void setInitialDataForRequest(ApplicationInfo applicationInfo) {
        applicationInfo.getApp().setService(new RequestService());
        applicationInfo.setServiceType(ServiceTypes.REQUEST.name());
        applicationInfo.setSubscriptionChargingType(MONTHLY_SUBS_CHARGING);
        applicationInfo.getApp().getApplicationChargingInfo().setFrmSubscriberPerMsgChargingValue(frmSubscriberPerMsgDefaultChgValue);
        applicationInfo.getApp().getApplicationChargingInfo().setChargingForMessage(true);
        applicationInfo.setReadOnly(false);
        applicationInfo.setSubCategoryRequired(false);
    }

    public void setInitialDataAfterKeywordCreation(ApplicationInfo applicationInfo)
            throws OrcaException {
        setFinancialInstrumentsForCp(applicationInfo);
        setInitialKeyword(applicationInfo);
    }

    public String isMsisdnVerifyAllowedOnAppCreate() {
        return allowMsisdnVerifyOnAppCreate;
    }
}
