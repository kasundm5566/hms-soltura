package hsenidmobile.orca.cpportal.service;

import hms.commons.SnmpLogUtil;
import org.apache.commons.beanutils.ContextClassLoaderLocal;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.*;
import org.springframework.core.Ordered;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class ApplicationStatusEventListener implements ServletContextListener {

    private static String startTrap;
    private static String stopTrap;

    public void setStartTrap(String startTrap) {
        this.startTrap = startTrap;
    }

    public void setStopTrap(String stopTrap) {
        this.stopTrap = stopTrap;
    }

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        SnmpLogUtil.log(startTrap != null ? startTrap : "2021.400.1.2|1|soltura started");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        SnmpLogUtil.log(stopTrap != null ? stopTrap : "2021.400.1.1|1|soltura is down");
    }
}
