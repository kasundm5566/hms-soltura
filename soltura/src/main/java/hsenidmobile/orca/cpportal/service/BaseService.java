/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hsenidmobile.orca.cpportal.service;

import hms.pgw.api.common.PayInstrumentInfo;
import hms.pgw.api.response.PayInstrumentResponse;
import hsenidmobile.nettyclient.MessageSender;
import hsenidmobile.orca.core.applications.Service;
import hsenidmobile.orca.core.applications.alert.AlertContentRelease;
import hsenidmobile.orca.core.applications.exception.ApplicationException;
import hsenidmobile.orca.core.applications.request.RequestServiceData;
import hsenidmobile.orca.core.applications.subscription.DefaultDispatchConfiguration;
import hsenidmobile.orca.core.applications.subscription.Periodicity;
import hsenidmobile.orca.core.applications.subscription.Subscription;
import hsenidmobile.orca.core.model.*;
import hsenidmobile.orca.cpportal.module.ApplicationInfo;
import hsenidmobile.orca.cpportal.module.KeywordDetails;
import hsenidmobile.orca.cpportal.util.Json2Java;
import hsenidmobile.orca.orm.repository.ContentProviderDetailsRepositoryImpl;
import hsenidmobile.orca.rest.pg.transport.PaymentGatewayRequestSender;
import hsenidmobile.orca.rest.sdp.app.transport.ProvAppManageRequestSender;
import hsenidmobile.orca.rest.sdp.routing.transport.SdpRkManageRequestSender;
import org.apache.cxf.jaxrs.client.ClientWebApplicationException;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

import static hsenidmobile.orca.core.model.Content.ContentSource.WEB;
import static hsenidmobile.orca.rest.common.util.RestApiKeys.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */

public abstract class BaseService {

    private final static Logger logger = LoggerFactory.getLogger(BaseService.class);

    @Autowired
    protected ContentProviderDetailsRepositoryImpl cpDetailsRepository;
    @Autowired
    protected PaymentGatewayRequestSender paymentGatewayRequestSender;
    @Autowired
    protected SdpRkManageRequestSender sdpRkManageRequestSender;
    @Autowired
    protected ProvAppManageRequestSender provAppManageRequestSender;

    private static Properties properties = new Properties();

    // Used to hold the time buffer values for subscription applications
    private Map<String, Integer> subscriptionBufferTimeValues;
    protected static final Locale langEng = Locale.ENGLISH;
    protected static final Locale langTamil = new Locale("ta");
    protected static final Locale langSin = new Locale("si");
    private final int LAST_DAY_OF_THE_MONTH = 32;
    private static Map<String, ContentProvider> contentProviderMap = new HashMap<String, ContentProvider>();

    public DefaultDispatchConfiguration configureDispatchTime(ApplicationInfo appInfo) {

        DefaultDispatchConfiguration configuration;
        int hour = appInfo.getSubscriptionDispatchHour();
        int minute = appInfo.getSubscriptionDispatchMinute();

        switch (appInfo.getSelectedPeriodUnit()) {
            case DAY:
                configuration = DefaultDispatchConfiguration.configureForDay(hour, minute);
                break;
            case HOUR:
                configuration = DefaultDispatchConfiguration.configureForHour(minute);
                break;
            case MONTH:
                int date = appInfo.getSubscriptionDispatchDate();
                if (date == LAST_DAY_OF_THE_MONTH) {
                    configuration = DefaultDispatchConfiguration.configureForLastDayOfMonth(date, hour, minute);
                } else {
                    configuration = DefaultDispatchConfiguration.configureForDayOfMonth(date, hour, minute);
                }
                break;
            case WEEK:
                int day = appInfo.getSubscriptionDispatchDay();
                configuration = DefaultDispatchConfiguration.configureForWeek(day, hour, minute);
                break;
            default:
                throw new IllegalStateException("Given period unit [" + appInfo.getSelectedPeriodUnit()
                        + "] is not supported");
        }
        return configuration;
    }

    private void setBufferTime(ApplicationInfo appInfo, Periodicity periodicity) {
        if (appInfo.getSelectedPeriodUnit() == null) {
            throw new RuntimeException("Periodic Unit should not be null for subscription applications");
        }
        int allowedBufferTime = 0;
        InputStream inputStream = MessageSender.class.getResourceAsStream("/properties/cp-portal.properties");
        try {
            properties.load(inputStream);
            switch (appInfo.getSelectedPeriodUnit()) {
                case DAY:
                    allowedBufferTime = 60*Integer.parseInt(properties.getProperty("buffer.delay.day"));
                    break;
                case HOUR:
                    allowedBufferTime = Integer.parseInt(properties.getProperty("buffer.delay.hour"));
                    break;
                case MONTH:
                    allowedBufferTime = 60*24*Integer.parseInt(properties.getProperty("buffer.delay.month"));
                    break;
                case WEEK:
                    allowedBufferTime = 60*24*Integer.parseInt(properties.getProperty("buffer.delay.week"));
                    break;
                default:
                    throw new IllegalStateException("Given period unit [" + appInfo.getSelectedPeriodUnit()
                            + "] is not supported");
            }
            periodicity.setAllowedBufferTime(allowedBufferTime);
        } catch (IOException e) {
            logger.error("Error occured while reading property file");
        }
    }

    protected List<RequestServiceData> createRequestServiceData() {
        ArrayList<RequestServiceData> datas = new ArrayList<RequestServiceData>();
        RequestServiceData requestServiceData = new RequestServiceData();
        datas.add(requestServiceData);
        return datas;
    }

    /**
     * Returns the Current logged in Content Provider Details
     *
     * @param retriveDataFromDBIgnoreCash If this value is set as true cash memory will be ignored and
     *                                    new database query will be done
     * @return
     */
    public ContentProvider getContentProviderDetails(boolean retriveDataFromDBIgnoreCash) throws ClientWebApplicationException{
        final String username = CurrentUser.getUsername();
        if (!contentProviderMap.containsKey(username) || retriveDataFromDBIgnoreCash) {
            ContentProvider contentProvider;
            try {
                contentProvider = getContentProviderDetails(username);
                contentProviderMap.put(username, contentProvider);
                return contentProvider;
            } catch (ClientWebApplicationException e){
                throw e;
            } catch (Exception e) {
                logger.error("Error in retrieving CP details : " + e, e);
                throw new RuntimeException("CP could not be retrieved", e);
            }
        }
        return contentProviderMap.get(username);
    }

    /**
     * Get ContentProvider loaded with it's RoutingKey details from database.
     *
     * @param username
     * @return
     */
    public ContentProvider getContentProviderDetails(final String username) {
        logger.debug("Getting CP for username[{}]", username);
        ContentProvider contentProvider = cpDetailsRepository.findByUsername(username);
        logger.debug("Content Provider [{}]", contentProvider);
        PayInstrumentResponse response = paymentGatewayRequestSender.getAvailableFinancialInstruments(contentProvider.getCpId());
        logger.debug("Received Payment Gateway Response[{}]", response);
        if (response.getPayInstrumentInfos() == null) {
            logger.debug("PG Request sending failed for user [{}]", username);
            contentProvider.setAvailableFinancialInstruments(null);
        } else {
            PayInstrumentInfo[] payInstrumentInfos = response.getPayInstrumentInfos();
            List<CpFinancialInstrument> cpFinancialInstruments = new ArrayList<CpFinancialInstrument>();
            if (payInstrumentInfos != null) {
                for (PayInstrumentInfo entry : payInstrumentInfos) {
                    CpFinancialInstrument cpFinancialInstrument = new CpFinancialInstrument(
                            entry.getPayInstrumentName(), entry.getPayInstrumentId(), entry.getPayInsAccountId());
                    cpFinancialInstruments.add(cpFinancialInstrument);
                }
            }
            contentProvider.setAvailableFinancialInstruments(cpFinancialInstruments);
        }
        logger.debug("Getting CP for name[{}]", contentProvider.getName());
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put(SP_ID, contentProvider.getSpId());
        Map<String, Map<String, List<String>>> findRksResponse = sdpRkManageRequestSender.findAvailableRks(parameters);
        logger.debug("FIND RKS RESPONSE :  " + findRksResponse);
        HashMap<String, List<RoutingKey>> rkMap = Json2Java.getAvailableRoutingKeys(findRksResponse);
        //todo devide to owned and unassigned according to the correct logic
        contentProvider.setOwnedRoutingKeys(rkMap);
        contentProvider.setUnassignedRoutingKeys(rkMap);
        return contentProvider;
    }

    public ContentProvider getContentProviderForSettings(){
        final String username = CurrentUser.getUsername();

        try {
            logger.debug("Getting CP for username[{}]", username);
            ContentProvider contentProvider = cpDetailsRepository.findByUsername(username);
            logger.debug("Content Provider [{}]", contentProvider);
            Map<String, String> parameters = new HashMap<String, String>();
            parameters.put(SP_ID, contentProvider.getSpId());
            List<Map<String, Object>> findRksResponse = sdpRkManageRequestSender.findAll(parameters);
            logger.debug("FIND All RKS RESPONSE :  " + findRksResponse);
            List<RoutingKey> rkMap =Json2Java.getAllRks(findRksResponse);
            contentProvider.setOwnedRoutingInfo(rkMap);
            return contentProvider;
        } catch (ClientWebApplicationException e){
            throw e;
        } catch (Exception e) {
            logger.error("Error in retriving CP details : " + e, e);
            throw new RuntimeException("CP could not be retrieved", e);
        }
    }

    public ArrayList<SubCategory> setKeywordDetails(List<KeywordDetails> keywordDetailses) {
        ArrayList<SubCategory> subCategoryCodeList = new ArrayList<SubCategory>();
        if (keywordDetailses != null) {
            removeUnNecessaryKeywords(keywordDetailses);
            for (KeywordDetails entry : keywordDetailses) {
                final String keyword = entry.getKeyword().trim();
                if (!keyword.equals("")) {
                    SubCategory subCategory = new SubCategory();
                    subCategory.setKeyword(keyword.trim().toLowerCase());
                    subCategory.setDescription(Arrays.asList(addKeywordDetailsLocale(entry)));
                    logger.debug("user selected keyword details : [{}]", subCategory);
                    subCategoryCodeList.add(subCategory);
                } else {
                    logger.debug("Removing unused keyword details before adding to database [{}]", entry);
                }
            }
        } else {
            logger.debug("Application does not support for sub categories hence removing unnecessary keywords is " +
                    "not needed");
        }
        logger.debug("Number of keywords selected by user is [{}]", subCategoryCodeList.size());
        logger.debug("No keyword were selected adding one element with blank data");

        return subCategoryCodeList;
    }

    public List<Periodicity> setSupportedPeriodicities(ApplicationInfo appInfo, Subscription subscription) {
        List<Periodicity> supportedPeriodicities = new ArrayList<Periodicity>();
        Periodicity periodicity = new Periodicity();
        periodicity.setUnit(appInfo.getSelectedPeriodUnit());
        periodicity.setPeriodValue(1);
        DefaultDispatchConfiguration defaultDispatchConfiguration = configureDispatchTime(appInfo);
        periodicity.setFirstScheduledDispatchTime(new DateTime(Periodicity.getFistDispatchableDate(
                periodicity.getUnit(), 1, defaultDispatchConfiguration)));
        periodicity.setDefaultDispatchConfiguration(defaultDispatchConfiguration);
        setBufferTime(appInfo, periodicity);
        supportedPeriodicities.add(periodicity);
        subscription.setDefaultPeriodicity(periodicity);
        return supportedPeriodicities;
    }

    public List<ContentRelease> getAlertMessagesToBeSent(List<KeywordDetails> keywordDetailses, Service service)
            throws ApplicationException {
        List<ContentRelease> contentReleaseArrayList = new ArrayList<ContentRelease>();
        List<Content> contents = new ArrayList<Content>();
        for (KeywordDetails entry : keywordDetailses) {
            final String keyword = entry.getKeyword().trim();
            logger.debug("Keywords details : keyword [{}] EnDescription [{}]",
                    new Object[]{keyword, entry.getDescriptionEn()});
            if (!entry.getDescriptionEn().trim().equals("")) {
                Content content = new Content();
                content.setSubCategory(service.getSubcategory(entry.getKeyword()));
                content.setContent(addKeywordDetailsLocale(entry));
                content.setSource(WEB);
                contents.add(content);
                logger.debug("user selected keyword details : " + entry);
            } else {
                logger.debug("Removing unused keyword details before adding to database [{}]", entry);
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug("Number of keywords selected by user is [" + contents.size() + "]");
        }
        final AlertContentRelease alertContentRelease = new AlertContentRelease();
        alertContentRelease.setContents(contents);
        contentReleaseArrayList.add(alertContentRelease);
        return contentReleaseArrayList;
    }

    private LocalizedMessage addKeywordDetailsLocale(KeywordDetails entry) {
        if (entry.getDescriptionEn() == null) {
            return new LocalizedMessage("", Locale.ENGLISH);
        } else {
            return new LocalizedMessage(entry.getDescriptionEn().trim(), Locale.ENGLISH);
        }
    }

    public boolean isAppNameExistinSDP(String appName) throws ClientWebApplicationException{
        try{
            logger.debug("Check availability in sdp for appName [{}]", appName);
            Map<String, Object> parameters = new HashMap<String, Object>();
            parameters.put(NAME, appName);
            Map<String,Object> appNameExistResponse = provAppManageRequestSender.validateAppName(parameters);

            if (appNameExistResponse.get(AVAILABLE).equals("true")){
                logger.debug("App Name available in sdp, Response object is [{}] ",appNameExistResponse );
                return false;
            } else {
                logger.debug("App Name not available in sdp, Response object is [{}] ",appNameExistResponse );
                return true;
            }
        } catch (ClientWebApplicationException e) {
            throw e;
        } catch (Exception e){
            logger.error("processing request to check sdp app existance: ",e);
            return false;
        }
    }

    public void removeUnNecessaryKeywords(List<KeywordDetails> keywordDetailses) {
        logger.debug("Keyword list given to remove unnecessary keywords[{}]", keywordDetailses);

        if (keywordDetailses != null && !keywordDetailses.isEmpty()) {
            for (int i = 0; i < keywordDetailses.size(); i++) {
                String keyword = keywordDetailses.get(i).getKeyword();
                logger.debug("Checking keyword [{}]", keyword);
                if (keyword.trim().equals("")) {
                    keywordDetailses.remove(i);
                }
            }
        } else {
            logger.debug("Application does not support for sub categories hence removing unnecessary keywords is "
                    + "not needed");
        }

        logger.debug("Keyword list after to removing unnecessary keywords[{}]", keywordDetailses);
    }

    public void setSubscriptionBufferTimeValues(Map<String, Integer> subscriptionBufferTimeValues) {
        this.subscriptionBufferTimeValues = subscriptionBufferTimeValues;
    }

    public interface Checker<T> {
        public boolean check(T obj, String keyword);
    }

    public class CatChecker implements Checker<SubCategory> {
        public boolean check(SubCategory cat, String keyword) {
            return (cat.getKeyword().equals(keyword)); // or whatever, implement
            // your comparison here
        }
    }

    // put this in some class

    public static <T> Collection<T> findAll(Collection<T> coll, Checker<T> chk, String keyword) {
        LinkedList<T> l = new LinkedList<T>();
        for (T obj : coll) {
            if (chk.check(obj, keyword))
                l.add(obj);
        }
        return l;
    }
}



