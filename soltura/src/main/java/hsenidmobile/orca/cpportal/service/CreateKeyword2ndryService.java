/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hsenidmobile.orca.cpportal.service;

import hsenidmobile.orca.cpportal.module.CreateKeyword;/*
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */

public interface CreateKeyword2ndryService {
    boolean isOn ();

    Object formBackingObject(CreateKeyword domain);
}
