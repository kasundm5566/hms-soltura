package hsenidmobile.orca.cpportal.service;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Collection;
/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *   
 *   $LastChangedDate$
 *   $LastChangedBy$ 
 *   $LastChangedRevision$
 */

public class CurrentUser {

//    public static String username;
//    public static String firstRole;
//    public static Collection<GrantedAuthority> availableRoles;

//    public static void init() {
//        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//        username = authentication.getName();
//        availableRoles = authentication.getAuthorities();
//        firstRole = authentication.getAuthorities().iterator().next().getAuthority();
//    }

    public static String getUsername(){
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication.getName();
    }

    public static boolean isAuthenticated(String role) {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        for (GrantedAuthority grantedAuthority : authentication.getAuthorities()) {
            if (grantedAuthority.getAuthority().equals(role)) {
                return true;
            }
        }
        return false;
    }
}
