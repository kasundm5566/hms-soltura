/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hsenidmobile.orca.cpportal.service;

import hsenidmobile.orca.core.applications.ApplicationImpl;
import hsenidmobile.orca.core.applications.Service;
import hsenidmobile.orca.core.applications.alert.AlertService;
import hsenidmobile.orca.core.applications.subscription.Subscription;
import hsenidmobile.orca.core.model.ApplicationChargingInfo;
import hsenidmobile.orca.core.model.ApplicationChargingInfo.FromSubscriberChargingModel;
import hsenidmobile.orca.core.model.ContentProvider;
import hsenidmobile.orca.core.model.OrcaException;
import hsenidmobile.orca.core.model.OrcaSdpAppIntegration;
import hsenidmobile.orca.core.repository.AppTrafficRepository;
import hsenidmobile.orca.core.repository.SdpApplicationRepository;
import hsenidmobile.orca.cpportal.module.ApplicationInfo;
import hsenidmobile.orca.cpportal.module.KeywordDetails;
import hsenidmobile.orca.cpportal.util.DefaultProperties;
import hsenidmobile.orca.rest.common.Response;
import hsenidmobile.orca.rest.sdp.app.transport.ProvAppManageRequestSender;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static hsenidmobile.orca.core.model.Status.REJECTED;
import static hsenidmobile.orca.core.model.Status.REQUEST_FOR_APPROVAL;
import static hsenidmobile.orca.rest.common.ResponseStatus.SUCCESS;
import static hsenidmobile.orca.rest.common.util.RestApiKeys.*;
import static hsenidmobile.orca.core.model.ApplicationChargingInfo.*;

/*
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */

public class IntegratedDbCreateApplictionServiceImpl extends AbstractCreateApplictionService {

	private static final Logger LOGGER = LoggerFactory.getLogger(IntegratedDbCreateApplictionServiceImpl.class);
	private static final SimpleDateFormat FORMATTER = new SimpleDateFormat("dd/MM/yyyy");
	public static final String REGEX_SEPERATOR = ",";
	public static final String INSTRUMENT_REGEX = "_____";
	protected SessionFactory sessionFactory;
	@Autowired
	private ProvAppManageRequestSender provAppManageRequestSender;
	@Autowired
	private AppTrafficRepository appTrafficRepository;
	@Autowired
	private SdpApplicationRepository sdpApplicationRepository;
    @Autowired
    protected DefaultProperties defaultProperties;

	private String subscriberChargingPiName;
    private boolean subscriptionConfirmationRequired;
	private Map<ApplicationChargingInfo.AppChargingType, String> subscriptionAmount;
	private String moConnectionUrl;
	private String moChargingAmount;
	private String mtChargingAmount;

	private String moTps;
	private String moTpd;
	private String mtTps;
	private String mtTpd;
	private String maxNoOfBcMsgPerDay;
	private String password;
	private String allowedHostAddress;
	private boolean govern;
	private boolean advertise;
	private boolean applyTax;
	private String revenueShare;
	private static final String SPLITTER = "-";
	private String defaultSenderAddressFormatter;
	private String aliasFormatter;

	private String currencyCode;
	private String taxSuffix;

	private String perMessageChargingSuffix;
	private String dailySubscriptionChargingSuffix;
	private String monthlySubscriptionChargingSuffix;
	private String weeklySubscriptionChargingSuffix;

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void createApplication(ContentProvider cp, ApplicationInfo appInfo, ApplicationImpl app, String userName)
			throws Exception {

		String spId = cp.getSpId();
		if (app.getService() instanceof AlertService) {
			try {
				appTrafficRepository.createAlertAppTraffic(app.getAppId());
				LOGGER.debug("Alert Application Traffic information added for application [{}]", app.getAppName());
			} catch (Exception e) {
				LOGGER.error("Error occured while adding alert application traffic data", e);
			}
		}
		Response response = provAppManageRequestSender.createSdpProvisionedApp(
				getProvisionedAppRequestMessage(app, appInfo, spId, cp.getUserType(), userName), app.getAppId());
		if (response.getResponseStatus() == SUCCESS) {
			Map<String, String> responseParameters = response.getResponseParameters();
			sdpApplicationRepository.addOrcaSdpAppIntegrationData(new OrcaSdpAppIntegration(app.getAppId(),
					responseParameters.get(APP_ID), responseParameters.get(PASSWORD), spId));
			appRepository.createApplication(app);
			addAppToCpSession(cp, app);
		} else {
			LOGGER.error("SDP app creation failed, please check the sdp.");
			throw new OrcaException("application creation failed");
		}
	}

	private void addAppToCpSession(ContentProvider cp, ApplicationImpl app) {
		if (cp.getApplications() == null) {
			cp.setApplications(new ArrayList<ApplicationImpl>());
		} else {
			cp.getApplications().add(app);
		}
	}

	/**
	 * Creates Provisioning App Request
	 *
	 *
	 * @param app
	 * @param appInfo
	 * @param spId
	 * @param userType
	 * @param userName
	 * @return
	 * @throws OrcaException
	 */
	private Map<String, Object> getProvisionedAppRequestMessage(ApplicationImpl app, ApplicationInfo appInfo,
			String spId, String userType, String userName) throws OrcaException {
		Map<String, Object> parameters = new HashMap<String, Object>();
		Map<String, Object> ncsSlas = new HashMap<String, Object>();
		generateAppBasicReqParams(app, parameters, spId, userType, userName);
		LOGGER.debug("Selected Routing Keys  [{}]", appInfo.getSelectedRoutingKeyValues().toString());

		Map<String, Map<String, Object>> chargingSlaDetails = getChargingSlaDetails(app);
		LOGGER.debug("Created basic chaging configurations for applicaiton[{}]", chargingSlaDetails);

		for (String rkEntry : appInfo.getSelectedRoutingKeyValues()) {
			generateAppNcsSlaParams(app, ncsSlas, rkEntry, chargingSlaDetails.get("MT_CHARGING"));
		}
		Service service = app.getService();
		if (service instanceof AlertService || app.getService() instanceof Subscription) {
			setSubscriptionBasicReqParams(app, ncsSlas, chargingSlaDetails.get("SUBSCRIPTION"));
		}
		parameters.put(NCS_SLAS, ncsSlas);
		LOGGER.debug("PROV-APP: Prov App Creation Request  [{}]", parameters.toString());
		return parameters;
	}

	private void setSubscriptionBasicReqParams(final ApplicationImpl app, final Map<String, Object> ncsSlas,
			Map<String, Object> subscriptionSlaMap) {
		String subscriptionSuccessRegResponse = app.getSubscriptionResponse(Locale.ENGLISH);
		String unsubscriptionSuccessResponse = app.getUnsubscribeResponse(Locale.ENGLISH);
		subscriptionSlaMap.put(SUBSCRIPTION_SUCCESS_REG_RESPONSE, subscriptionSuccessRegResponse);
		subscriptionSlaMap.put(SUBSCRIPTION_SUCCESS_UNREG_RESPONSE, unsubscriptionSuccessResponse);
		subscriptionSlaMap.put(SUBSCRIPTION_CONFIRMATION_REQUIRED, subscriptionConfirmationRequired);
		subscriptionSlaMap.put(NCS_TYPE, SUBSCRIPTION_NCS_TYPE);
		subscriptionSlaMap.put(MAX_NO_OF_BC_MSG_PER_DAY, maxNoOfBcMsgPerDay);
		subscriptionSlaMap.put(ALLOW_HTTP_REQUESTS, true);
		ncsSlas.put(SUBSCRIPTION_NCS_TYPE, subscriptionSlaMap);
	}

	/**
	 * Creates basic Application Request parameters
	 *
	 * @param app
	 * @param parameters
	 * @param spId
	 * @param userType
	 * @param userName
	 */
	private void generateAppBasicReqParams(ApplicationImpl app, Map<String, Object> parameters, String spId,
			String userType, String userName) {
		parameters.put(SP_ID, spId);
		String appName = app.getAppName();
		parameters.put(EXPIRE, String.valueOf(app.isExpirableApp()));
		parameters.put(NAME, appName);
		parameters.put(DESCRIPTION, app.getDescription());
		parameters.put(START_DATE, FORMATTER.format(app.getStartDate().toDate()));
		if (app.isExpirableApp()) {
			parameters.put(END_DATE, FORMATTER.format(app.getEndDate().toDate()));
		}
		parameters.put(PASSWORD, password);
		setAllowedHostAddressList(parameters);
		parameters.put(BLACK_LIST, new ArrayList());
		parameters.put(WHITE_LIST, new ArrayList());
		parameters.put(NUMBER_MASKING, false);
		parameters.put(GOVERN_ENABLED, govern);
		parameters.put(ADVERTISE_ENABLED, advertise);
		parameters.put(TAX_ENABLED, applyTax);
		parameters.put(REVENUE_SHARE, revenueShare);
		parameters.put(CHARGING_TYPE, FLAT_CHARGING_TYPE);
		parameters.put(CREATED_BY, userName);
		parameters.put(CREATED_USER_TYPE, userType);
	}

	private void setAllowedHostAddressList(Map<String, Object> parameters) {
		ArrayList allowedHostAddressList = new ArrayList();
		String[] hostAddresses = allowedHostAddress.split(REGEX_SEPERATOR);
		for (String hostAddress : hostAddresses) {
			allowedHostAddressList.add(hostAddress.trim());
		}
		parameters.put(ALLOWED_HOST_ADDRESS, allowedHostAddressList);
	}

	/**
	 * Creates NCS SLA request parameters
	 *
	 * @param app
	 * @param ncsSlas
	 * @param rkEntry
	 */
	private void generateAppNcsSlaParams(ApplicationImpl app, Map<String, Object> ncsSlas, String rkEntry,
			Map<String, Object> baseMtChargingSla) {

		if (baseMtChargingSla == null) {
			LOGGER.error("Base Mt SLA config not found.");
			throw new RuntimeException("Base Mt SLA config not found.");
		}
		Map<String, Object> ncsSlaMap = new HashMap<String, Object>();
		Map<String, Object> moSlaMap = new HashMap<String, Object>();
		Map<String, String> rkMap = new HashMap<String, String>();
		List<Map<String, String>> rkList = new ArrayList<Map<String, String>>();
		String[] keywordData = rkEntry.split(SPLITTER);
		String operator = keywordData[0].trim();
		String ncsType = app.getNcsType().name().trim();
		rkMap.put(SHORTCODE, keywordData[1].trim());
		rkMap.put(KEYWORD, keywordData[2].trim());
		rkList.add(rkMap);
		ncsSlaMap.put(OPERATOR, operator);
		ncsSlaMap.put(NCS_TYPE, ncsType.toLowerCase());
		ncsSlaMap.put(SUBSCRIPTION_REQUIRED, app.getService().isSubscriptionRequired());
		moSlaMap.put(MO_CONNECTION_URL, moConnectionUrl);
		moSlaMap.put(TPS, moTps);
		moSlaMap.put(TPD, moTpd);
		setMoChargingSla(moSlaMap);
		ncsSlaMap.put(MO, moSlaMap);
		ncsSlaMap.put(MT, getMtSla(app, baseMtChargingSla, rkMap));
		ncsSlaMap.put(ROUTING_KEYS, rkList);
		String key = operator + SPLITTER + ncsType;
		ncsSlas.put(key, ncsSlaMap);
	}

	private Map<String, Object> getMtSla(ApplicationImpl app, Map<String, Object> baseMtChargingSla, Map<String, String> rkMap) {
		Map<String, Object> mtSlaMap = new HashMap<String, Object>();
		mtSlaMap.put(TPS, mtTps);
		mtSlaMap.put(TPD, mtTpd);

		String appName = app.getAppName();
        String shortCode = rkMap.get(SHORTCODE);
        ArrayList<String> aliases = new ArrayList<String>();

        LOGGER.debug("DEFAULT SENDER ADDRESS is empty? [{}]", ifEmptyDefaultSenderAddress());
        if(shortCode != null) {
            mtSlaMap.put(DEFAULT_SENDER_ADDRESS, MessageFormat.format(getDefaultSenderAddressFormatter(), appName, shortCode));
            aliases.add(MessageFormat.format(getAliasFormatter(), appName, shortCode));
        } else {
            mtSlaMap.put(DEFAULT_SENDER_ADDRESS, MessageFormat.format(getDefaultSenderAddressFormatter(), appName));
            aliases.add(MessageFormat.format(getAliasFormatter(), appName));
        }

		mtSlaMap.put(ALISING, aliases);
		mtSlaMap.put(CHARGING_SLA, baseMtChargingSla);

		return mtSlaMap;
	}

	private Map<String, Map<String, Object>> getChargingSlaDetails(ApplicationImpl app) throws OrcaException {
		Map<String, Object> mtChargingSlaMap = new HashMap<String, Object>();
		Map<String, Object> subscriptionSlaMap = new HashMap<String, Object>();
		ApplicationChargingInfo applicationChargingInfo = app.getApplicationChargingInfo();
		LOGGER.debug("Application Charging configuration[{}]", applicationChargingInfo);

		mtChargingSlaMap.put(CHARGING_TYPE, FLAT_CHARGING_TYPE);

		if (applicationChargingInfo.isChargingFromSubscriber()) {
			if (!app.isRegistrationRequired()) {
				createSubscriberPerMsgFlatMtChargingDetails(mtChargingSlaMap, applicationChargingInfo);
			} else {
				if (applicationChargingInfo.getFromSubscriberChargingModel() == FromSubscriberChargingModel.ONLY_ONE_CHG_TYPE_ALLOWED) {
					// Per message charging
					if ("CHG_PER_MSG".equals(applicationChargingInfo.getFrmSubscriberChargingValue())) {
						subscriptionSlaMap.put(CHARGING_SLA, createSubscriptionFreeCharging());
						createSubscriberPerMsgFlatMtChargingDetails(mtChargingSlaMap, applicationChargingInfo);
					} else {// Subscription charging
						subscriptionSlaMap.put(CHARGING_SLA, createFlatSubscriptionChargingDetails(app));
						mtChargingSlaMap.put(CHARGING_TYPE, FREE_CHARGING_TYPE);
					}
				} else {
					// Can select both per message charging and subscription
					// charging
					if (applicationChargingInfo.isChargingForMessage()) {
						createSubscriberPerMsgFlatMtChargingDetails(mtChargingSlaMap, applicationChargingInfo);
					} else {
						mtChargingSlaMap.put(CHARGING_TYPE, FREE_CHARGING_TYPE);
					}

					if (app.getApplicationChargingInfo().isChargingForSubscription()) {
						subscriptionSlaMap.put(CHARGING_SLA, createFlatSubscriptionChargingDetails(app));
					} else {
						subscriptionSlaMap.put(CHARGING_SLA, createSubscriptionFreeCharging());
					}
				}
			}
		} else {
			createSpPerMsgFlatMtChargingDetails(mtChargingSlaMap, applicationChargingInfo);
		}
		LOGGER.debug("Created charging configuration. MtCharging[{}], SubscriptionCharging[{}]", mtChargingSlaMap, subscriptionSlaMap);
		Map<String, Map<String, Object>> returnData = new HashMap<String, Map<String, Object>>();
		returnData.put("MT_CHARGING", mtChargingSlaMap);
		returnData.put("SUBSCRIPTION", subscriptionSlaMap);
		return returnData;

	}

	private void createSpPerMsgFlatMtChargingDetails(Map<String, Object> mtChargingSlaMap,
			ApplicationChargingInfo applicationChargingInfo) {
		mtChargingSlaMap.put(CHARGE_PARTY, SP_CHARGE_PARTY);
		String instruments = applicationChargingInfo.getSelectedFinancialInstrumentType();
		String[] instrumentArray = instruments.split(INSTRUMENT_REGEX);
		mtChargingSlaMap.put(PAYMENT_INSTRUMENT_NAME, instrumentArray[1]);
		mtChargingSlaMap.put(PAYMENT_INSTRUMENT_ID, instrumentArray[2]);
		mtChargingSlaMap.put(PAYMENT_ACCOUNT, instrumentArray[0]);
	}

	private void createSubscriberPerMsgFlatMtChargingDetails(Map<String, Object> mtChargingSlaMap,
			ApplicationChargingInfo applicationChargingInfo) throws OrcaException {
		mtChargingSlaMap.put(CHARGE_PARTY, SUBSCRIBER_CHARGE_PARTY);
		mtChargingSlaMap.put(CHARGING_AMOUNT, mtChargingAmount);
		mtChargingSlaMap.put(ALLOWED_PI, new String[] { subscriberChargingPiName });
//		if (PER_MSG_PI_CHG.equals(applicationChargingInfo.getFrmSubscriberPerMsgChargingValue())) {
//			mtChargingSlaMap.put(CHARGING_METHOD, DEFAULT_PAYMENT_INSTRUMENT);
//		} else if (PER_MSG_OPR_CHG
//				.equals(applicationChargingInfo.getFrmSubscriberPerMsgChargingValue())) {
//			mtChargingSlaMap.put(CHARGING_METHOD, OPERATOR_CHARGING);
//		} else {
//			throw new OrcaException(
//					"Invalide Per Message charing type selected for From Subscriber charing");
//		}
	}

	private Map<String, Object> createFlatSubscriptionChargingDetails(ApplicationImpl app) {
		/*
			"charging" : {
				"amount" : "20",
				"frequency" : "monthly",
				"service-code" : "1789",
				"party" : "subscriber",
				"type" : "flat",
				"allowed-payment-instruments" : [
					"Mobile Account"
				]
			},
		 */
		Map<String, Object> subsChargingDetails = new HashMap<String, Object>();
		subsChargingDetails.put(CHARGING_TYPE, FLAT_CHARGING_TYPE);
		subsChargingDetails.put(ALLOWED_PI, new String[] { subscriberChargingPiName });
		subsChargingDetails.put(CHARGING_AMOUNT,
				subscriptionAmount.get(app.getApplicationChargingInfo().getChargingType()));
		subsChargingDetails.put(SUBSCRIPTION_FREQUENCY, app.getApplicationChargingInfo().getChargingType().name());
		return subsChargingDetails;
	}

	private Map<String, String> createSubscriptionFreeCharging() {
		Map<String, String> chargingDetails = new HashMap<String, String>();
		chargingDetails.put(CHARGING_TYPE, FREE_CHARGING_TYPE);
		return chargingDetails;
	}

	private void setMoChargingSla(Map<String, Object> moSlaMap) {
		Map<String, Object> moChargingSlaMap = new HashMap<String, Object>();
		moChargingSlaMap.put(CHARGING_TYPE, FREE_CHARGING_TYPE);
		moSlaMap.put(CHARGING_SLA, moChargingSlaMap);
	}

	private boolean ifEmptyDefaultSenderAddress() {
		return defaultSenderAddressFormatter == null || defaultSenderAddressFormatter.trim().length() == 0;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void editApplication(ApplicationImpl application, List<KeywordDetails> keywords) throws Exception {
		ContentProvider providerDetails = getContentProviderDetails(false);
		if (application.getStatus() == REJECTED) {
			application.setStatus(REQUEST_FOR_APPROVAL);
			mailSender.sendRequestForApprovalEmail(providerDetails.getName(), application.getAppName(),
					providerDetails.getEmail());
		}
		sessionFactory.getCurrentSession().update(application);
		LOGGER.info("orca application created creating SDP application [{}]", application);
		editSdpApplication(application, keywords);
		LOGGER.info("sending email");
		if (application.getStatus() == REJECTED) {
			mailSender.sendRequestForApprovalEmail(providerDetails.getName(), application.getAppName(),
					providerDetails.getEmail());
		}
		LOGGER.info("SDP application created successfully committing transactions");
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public void setMoConnectionUrl(String moConnectionUrl) {
		this.moConnectionUrl = moConnectionUrl;
	}

	public void setMoChargingAmount(String moChargingAmount) {
		this.moChargingAmount = moChargingAmount;
	}

	public String getMoChargingAmount() {
		return moChargingAmount;
	}

	public void setMoTps(String moTps) {
		this.moTps = moTps;
	}

	public void setMoTpd(String moTpd) {
		this.moTpd = moTpd;
	}

	public void setMtTps(String mtTps) {
		this.mtTps = mtTps;
	}

	public void setMtTpd(String mtTpd) {
		this.mtTpd = mtTpd;
	}

	public void setMaxNoOfBcMsgPerDay(String maxNoOfBcMsgPerDay) {
		this.maxNoOfBcMsgPerDay = maxNoOfBcMsgPerDay;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setAllowedHostAddress(String allowedHostAddress) {
		this.allowedHostAddress = allowedHostAddress;
	}

	public void setSdpApplicationRepository(SdpApplicationRepository sdpApplicationRepository) {
		this.sdpApplicationRepository = sdpApplicationRepository;
	}

	public void setSubscriptionAmount(Map<ApplicationChargingInfo.AppChargingType, String> subscriptionAmount) {
		this.subscriptionAmount = subscriptionAmount;
	}

	public void setGovern(boolean govern) {
		this.govern = govern;
	}

	public void setAdvertise(boolean advertise) {
		this.advertise = advertise;
	}

	public void setApplyTax(boolean applyTax) {
		this.applyTax = applyTax;
	}

	public void setRevenueShare(String revenueShare) {
		this.revenueShare = revenueShare;
	}

	public void setDefaultSenderAddressFormatter(String defaultSenderAddressFormatter) {
		this.defaultSenderAddressFormatter = defaultSenderAddressFormatter;
	}

	public String getDefaultSenderAddressFormatter() {
		return defaultSenderAddressFormatter;
	}

    public String getAliasFormatter() { return aliasFormatter; }

    public void setAliasFormatter(String aliasFormatter) {
        this.aliasFormatter = aliasFormatter;
    }

    public void setMtChargingAmount(String mtChargingAmount) {
		this.mtChargingAmount = mtChargingAmount;
	}

	public String getMtChargingAmount() {
		return mtChargingAmount;
	}

	public Map<ApplicationChargingInfo.AppChargingType, String> getSubscriptionAmount() {
		return subscriptionAmount;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getTaxSuffix() {
		return taxSuffix;
	}

	public void setTaxSuffix(String taxSuffix) {
		this.taxSuffix = taxSuffix;
	}

	public void init() {
        if(currencyCode.trim().equals("FJD")){
            perMessageChargingSuffix = MessageFormat.format(" ({0} {1})", currencyCode, mtChargingAmount);
            dailySubscriptionChargingSuffix = MessageFormat.format(" ({0} {1})", currencyCode,
                    subscriptionAmount.get(ApplicationChargingInfo.AppChargingType.daily));
            monthlySubscriptionChargingSuffix = MessageFormat.format(" ({0} {1})", currencyCode,
                    subscriptionAmount.get(ApplicationChargingInfo.AppChargingType.monthly));
            weeklySubscriptionChargingSuffix = MessageFormat.format(" ({0} {1})", currencyCode,
                    subscriptionAmount.get(ApplicationChargingInfo.AppChargingType.weekly));
        }else{
            perMessageChargingSuffix = MessageFormat.format(" ({0} {1} + {2})", currencyCode, mtChargingAmount, taxSuffix);
            dailySubscriptionChargingSuffix = MessageFormat.format(" ({0} {1} + {2})", currencyCode,
                    subscriptionAmount.get(ApplicationChargingInfo.AppChargingType.daily), taxSuffix);
            monthlySubscriptionChargingSuffix = MessageFormat.format(" ({0} {1} + {2})", currencyCode,
                    subscriptionAmount.get(ApplicationChargingInfo.AppChargingType.monthly), taxSuffix);
            weeklySubscriptionChargingSuffix = MessageFormat.format(" ({0} {1} + {2})", currencyCode,
                    subscriptionAmount.get(ApplicationChargingInfo.AppChargingType.weekly), taxSuffix);
        }


	}

	public String getPerMessageChargingSuffix() {
		return perMessageChargingSuffix;
	}

	public String getDailySubscriptionChargingSuffix() {
		return dailySubscriptionChargingSuffix;
	}

	public String getMonthlySubscriptionChargingSuffix() {
		return monthlySubscriptionChargingSuffix;
	}

	public String getWeeklySubscriptionChargingSuffix() {
		return weeklySubscriptionChargingSuffix;
	}

	public void setSubscriberChargingPiName(String subscriberChargingPiName) {
		this.subscriberChargingPiName = subscriberChargingPiName;
	}

    public boolean isSubscriptionConfirmationRequired() { return subscriptionConfirmationRequired; }

    public void setSubscriptionConfirmationRequired(boolean subscriptionConfirmationRequired) {
        this.subscriptionConfirmationRequired = subscriptionConfirmationRequired;
    }
}