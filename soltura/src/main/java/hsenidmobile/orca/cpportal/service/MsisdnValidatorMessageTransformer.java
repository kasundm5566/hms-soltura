/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hsenidmobile.orca.cpportal.service;
/**
 * Listener class for Subscriber Charging MSISDN validation
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */

public class MsisdnValidatorMessageTransformer {
      private static final String VERSION_KEY = "version";
      private static final String CORRELATOR_KEY = "correlator";
      private static final String ADDRESS_KEY = "address";
      private static final String MESSAGE_KEY = "message";

//    public MchoiceAventuraSmsMessage transformRequest(HttpServletRequest request){
//        return processRequest(request);
//    }
//
//        private MchoiceAventuraSmsMessage processRequest(HttpServletRequest request) {
//        return new MchoiceAventuraSmsMessage(
//                request.getParameter(VERSION_KEY),
//                request.getParameter(CORRELATOR_KEY),
//                request.getParameter(ADDRESS_KEY),
//                request.getParameter(MESSAGE_KEY)
//        );
//    }
}
