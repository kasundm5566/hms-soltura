/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hsenidmobile.orca.cpportal.service;

import javax.servlet.http.HttpServletRequest;

/**
 * Listener class for Subscriber Charging MSISDN validation
 * <p/>
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */


public interface MsisdnValidatorService {

    void verifyChargingMsisdn(HttpServletRequest request);
}
