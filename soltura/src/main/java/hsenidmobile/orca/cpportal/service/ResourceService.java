/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hsenidmobile.orca.cpportal.service;

import java.util.List;

/**
 * $LastChangedDate:$
 * $LastChangedBy:$
 * $LastChangedRevision:$
 */
public class ResourceService {

    private List<String> locations;
    private List<String> countryDetails;

    public List<String> getLocations() {
        return locations;
    }

    public void setLocations(List<String> locations) {
        this.locations = locations;
    }

    public List<String> getCountryDetails() {
        return countryDetails;
    }

    public void setCountryDetails(List<String> countryDetails) {
        this.countryDetails = countryDetails;
    }
}