/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hsenidmobile.orca.cpportal.service;

import hsenidmobile.orca.core.applications.ApplicationImpl;
import hsenidmobile.orca.core.model.ContentProvider;
import hsenidmobile.orca.cpportal.module.ApplicationInfo;
import hsenidmobile.orca.cpportal.module.KeywordDetails;
import org.hibernate.FlushMode;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.classic.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
/*   
 *   $LastChangedDate$
 *   $LastChangedBy$ 
 *   $LastChangedRevision$
 *
 */

public class SingleTransactionPointService extends AbstractCreateApplictionService {

    private static final Logger logger = LoggerFactory.getLogger(SingleTransactionPointService.class);

    protected SessionFactory sessionFactory;
    protected SessionFactory sdpSessionFactory;

    @Override
    public void createApplication(ContentProvider providerDetails, ApplicationInfo appInfo, ApplicationImpl app, String userName) throws Exception {

        logger.info("Starting Single Transaction point to create Orca app [{}]", appInfo.getApp().getAppId());

        final Session session = sessionFactory.openSession();
        final Session sdpSession = sdpSessionFactory.openSession();
        session.setFlushMode(FlushMode.MANUAL);
        sdpSession.setFlushMode(FlushMode.MANUAL);
        final Transaction transaction = session.beginTransaction();
        final Transaction sdpTransaction = sdpSession.beginTransaction();

        logger.info("Transaction started for orca session factory and SDP session factory");
        try {
            providerDetails.getApplications().add(app);
            cpDetailsRepository.update(providerDetails);
            commitAndFlushSession(session, sdpSession, transaction, sdpTransaction);
        } catch (Throwable e) {
            handleCreateAppError(transaction, sdpTransaction, e);
        } finally {
            closeSession(session, sdpSession);
        }
        logger.info("======= Application created successfully for App Id [{}]======", appInfo.getApp().getAppId());
    }

    @Override
    public void editApplication(ApplicationImpl application, List<KeywordDetails> keywords) throws Exception {
        ContentProvider providerDetails = getContentProviderDetails(false);
        logger.info("Starting Single Transaction point to edit Orca app [{}]", application.getAppId());

        final Session session = sessionFactory.openSession();
        final Session sdpSession = sdpSessionFactory.openSession();
        session.setFlushMode(FlushMode.MANUAL);
        sdpSession.setFlushMode(FlushMode.MANUAL);
        final Transaction transaction = session.beginTransaction();
        final Transaction sdpTransaction = sdpSession.beginTransaction();

        logger.info("Transaction started for orca session factory and SDP session factory");
        try {
            doEditApplication(application, keywords, providerDetails);
            commitAndFlushSession(session, sdpSession, transaction, sdpTransaction);
        } catch (Throwable e) {
            handleEditAppError(transaction, sdpTransaction, e);
        } finally {
            closeSession(session, sdpSession);
        }
        logger.info("======= Application edit successful for App Id [{}]======", application.getAppId());
    }

    private void handleEditAppError(Transaction transaction, Transaction sdpTransaction, Throwable e) throws Exception {
        logger.error("Error editing application : ", e);
        logger.info("Starting to rollback Orca and SDP applications");
        transaction.rollback();
        sdpTransaction.rollback();
        logger.info("Rollback successful..");
        throw new Exception(e);
    }

    private void commitAndFlushSession(Session session, Session sdpSession, Transaction transaction, Transaction sdpTransaction) {
        transaction.commit();
        sdpTransaction.commit();
        session.flush();
        sdpSession.flush();
    }

    private void closeSession(Session session, Session sdpSession) {
        session.clear();
        session.close();
        sdpSession.clear();
        sdpSession.close();
    }

    private void handleCreateAppError(Transaction transaction, Transaction sdpTransaction, Throwable e) throws Exception {
        logger.error("Error creating application : ", e);
        logger.info("Starting to rollback Orca and SDP applications");
        transaction.rollback();
        sdpTransaction.rollback();
        logger.info("Rollback successful..");
        throw new Exception(e);
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void setSdpSessionFactory(SessionFactory sdpSessionFactory) {
        this.sdpSessionFactory = sdpSessionFactory;
    }
}
