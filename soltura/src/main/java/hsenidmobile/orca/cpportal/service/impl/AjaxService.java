package hsenidmobile.orca.cpportal.service.impl;

import hsenidmobile.orca.core.applications.exception.RequestServiceException;
import hsenidmobile.orca.core.applications.request.RequestServiceData;
import hsenidmobile.orca.core.repository.SdpApplicationRepository;
import hsenidmobile.orca.cpportal.service.impl.application.ApplicationService;
import hsenidmobile.orca.orm.repository.RequestServiceDataRepositoryImpl;

import org.apache.commons.lang3.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */

public class AjaxService {

    private static final Logger logger = LoggerFactory.getLogger(AjaxService.class);

    @Autowired
    private ApplicationService applicationService;
    @Autowired
    private RequestServiceDataService requestServiceDataService;

    @Autowired
    private RequestServiceDataRepositoryImpl requestServiceDataRepository;

    @Autowired
    private SdpApplicationRepository sdpApplicationRepository;

    @Resource(name = "requestShowDatePattern")
    private String requestShowDatePattern = "dd-MM-yyyy HH:mm:ss";
    SimpleDateFormat dateTimeFormater = new SimpleDateFormat(requestShowDatePattern);


    public void respondForAppNameValidation(HttpServletResponse response, boolean appNameExists) throws IOException {

        createResponseParameters(response);
        StringBuilder xml = new StringBuilder().append("<?xml version=\"1.0\"?>");
        xml.append("<ajax-loader>");
        xml.append("<values>");
        xml.append("<app-exsists>");
        xml.append(appNameExists);
        xml.append("</app-exsists>");
        xml.append("</values>");
        xml.append("</ajax-loader>");
        logger.debug("Returning respond : [{}] for ajax request", xml);
        response.getWriter().println(xml);
    }



    public void respondForSuccessfullVerification(HttpServletResponse response, boolean verificationStatus) throws IOException {

        createResponseParameters(response);
        StringBuilder xml = new StringBuilder().append("<?xml version=\"1.0\"?>");
        xml.append("<ajax-loader>");
        xml.append("<values>");
        xml.append("<verification-status>");
        xml.append(verificationStatus);
        xml.append("</verification-status>");
        xml.append("</values>");
        xml.append("</ajax-loader>");
        logger.debug("Returning respond : [{}] for ajax request", xml);
        response.getWriter().println(xml);
    }

    public void respondForKeywordValidation(HttpServletResponse response, String keywordExists) throws IOException {

        createResponseParameters(response);
        StringBuilder xml = new StringBuilder().append("<?xml version=\"1.0\"?>");
        xml.append("<ajax-loader>");
        xml.append("<values>");
        xml.append("<username-exsists>");
        xml.append(keywordExists);
        xml.append("</username-exsists>");
        xml.append("</values>");
        xml.append("</ajax-loader>");
        logger.debug("Returning respond : [{}] for ajax request", xml);
        response.getWriter().println(xml);
    }


    private void createResponseParameters(HttpServletResponse response) {
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate, private, max-stale=0 ");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.addHeader("Pragma", "no-cache");
        response.addDateHeader("Expires", 0); //prevents caching at the proxy server
        response.setContentType("text/xml");
    }

    public void updateMessageReadStatus(long messageId, HttpServletResponse response) throws RequestServiceException, IOException {
        createResponseParameters(response);
        boolean sucees = false;
        try {
            requestServiceDataService.updateMessageReadStatus(messageId);
            sucees = true;
        } catch (RequestServiceException e) {
            logger.error("Error occured while updating the message status as READ", e);
        }
        StringBuilder xml = new StringBuilder().append("<?xml version=\"1.0\"?>");
        xml.append("<ajax-loader>");
        xml.append("<values>");
        xml.append("<success>");
        xml.append(sucees);
        xml.append("</success>");
        xml.append("</values>");
        xml.append("</ajax-loader>");
        response.getWriter().println(xml);
    }
    //TODO: USE A SEPARATE METHOD FOR COMMON XML PROPERTIES

    public void getMessagesByCategory(String appId, String subCategoryIds, int pageNumber, int messagesPerPage,
                                      HttpServletResponse response) throws RequestServiceException, IOException {
        List<RequestServiceData> requestServiceData = requestServiceDataService.getMessagesByCategory(appId, subCategoryIds);
        int totalNumberOfMessages = requestServiceData.size();
        int start = (pageNumber - 1) * messagesPerPage;
        int end = start + messagesPerPage;
        if (end > totalNumberOfMessages) {
            end = totalNumberOfMessages;
        }

        createResponseParameters(response);
        StringBuilder xml = new StringBuilder(500).append("<?xml version=\"1.0\"?>");
        xml.append("<ajax-loader>");
        xml.append("<values>");
        xml.append("<messages>");
        for (int i = start; i < end; i++) {
            xml.append("<message>");
            xml.append("<messageID>");
            xml.append(requestServiceData.get(i).getMessageId());
            xml.append("</messageID>");
            xml.append("<dateTime>");
            xml.append(dateTimeFormater.format(requestServiceData.get(i).getReceivedTime()));
            xml.append("</dateTime>");
            xml.append("<messageContent>");
            xml.append(requestServiceData.get(i).getReceivedMessage());
            xml.append("</messageContent>");
            xml.append("<senderAddress>");
            xml.append(requestServiceData.get(i).getMsisdn().getAddress());
            xml.append("</senderAddress>");
            xml.append("<isRead>");
            xml.append(requestServiceData.get(i).isRead());
            xml.append("</isRead>");
            xml.append("<isCommonResponseAvailable>");
            xml.append(requestServiceData.get(i).isCommonResponseAvailable());
            xml.append("</isCommonResponseAvailable>");
            xml.append("<appId>");
            xml.append(appId);
            xml.append("</appId>");
            xml.append("<subcategory>");
            xml.append(requestServiceData.get(i).getSubKeyword());
            xml.append("</subcategory>");
            xml.append("</message>");
        }
        xml.append("</messages>");
        xml.append("<records>");
        xml.append("<totalNumberOfRecords>");
        xml.append(totalNumberOfMessages);
        xml.append("</totalNumberOfRecords>");
        xml.append("</records>");
        xml.append("</values>");
        xml.append("</ajax-loader>");
        logger.debug("Response [{}]", xml.toString());
        response.getWriter().println(xml);
    }

    public void fetchMessagesForPage(String appId, long requestService, int start, int end,
                                     HttpServletResponse response) throws IOException, RequestServiceException {

        logger.debug("FETCH MESSAGE FOR PAGE");
        Map m = requestServiceDataService.fetchMessagesForPage(appId, requestService, start, end);
        List<RequestServiceData> requestServiceData = (List<RequestServiceData>) (m.get("requestServiceData"));

        createResponseParameters(response);
        StringBuilder xml = new StringBuilder(500).append("<?xml version=\"1.0\"?>");
        xml.append("<ajax-loader>");
        xml.append("<values>");
        xml.append("<messages>");
        for (RequestServiceData rq : requestServiceData) {
            xml.append("<message>");
            xml.append("<messageId>");
            xml.append(rq.getMessageId());
            xml.append("</messageId>");
            xml.append("<dateTime>");
            xml.append(dateTimeFormater.format(rq.getReceivedTime()));
            xml.append("</dateTime>");
            xml.append("<messageContent>");
            xml.append(StringEscapeUtils.escapeHtml4(rq.getReceivedMessage()));
            xml.append("</messageContent>");
            xml.append("<senderAddress>");
            xml.append(rq.getMsisdn().getAddress());
            xml.append("</senderAddress>");
            xml.append("<isRead>");
            xml.append(rq.isRead());
            xml.append("</isRead>");
            xml.append("<isReplied>");
            xml.append(rq.isReplied());
            xml.append("</isReplied>");
            xml.append("<isCommonResponseAvailable>");
            xml.append(rq.isCommonResponseAvailable());
            xml.append("</isCommonResponseAvailable>");
            xml.append("<appId>");
            xml.append(appId);
            xml.append("</appId>");
            xml.append("<subcategory>");
            xml.append(rq.getSubKeyword());
            xml.append("</subcategory>");
            xml.append("</message>");
        }
        xml.append("</messages>");
        xml.append("<records>");
        xml.append("<totalNumberOfRecords>");
        xml.append(m.get("totalNumberOfRecords"));
        xml.append("</totalNumberOfRecords>");
        xml.append("</records>");
        xml.append("</values>");
        xml.append("</ajax-loader>");

        response.getWriter().println(xml);
    }

    public void respondForAppStateChange(HttpServletResponse response, String isSuccess) throws IOException {

        createResponseParameters(response);
        StringBuilder xml = new StringBuilder().append("<?xml version=\"1.0\"?>");
        xml.append("<ajax-loader>");
        xml.append("<values>");
        xml.append("<app-state-change-success>");
        xml.append(isSuccess);
        xml.append("</app-state-change-success>");
        xml.append("</values>");
        xml.append("</ajax-loader>");
        logger.debug("Returning respond : [{}] for ajax request", xml);
        response.getWriter().println(xml);
    }
}
