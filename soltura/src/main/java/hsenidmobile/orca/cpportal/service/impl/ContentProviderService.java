/*
 *   (C) Copyright 200-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hsenidmobile.orca.cpportal.service.impl;

import hsenidmobile.orca.core.model.*;
import hsenidmobile.orca.core.model.message.RoutingKeyRegisterRequest;
import hsenidmobile.orca.core.services.RoutingKeyManager;
import hsenidmobile.orca.cpportal.module.CreateKeyword;
import hsenidmobile.orca.cpportal.module.Registration;
import hsenidmobile.orca.cpportal.service.BaseService;
import hsenidmobile.orca.cpportal.service.CurrentUser;
import hsenidmobile.orca.cpportal.util.MailSender;
import hsenidmobile.orca.cpportal.util.client.HttpRequestSender;
import hsenidmobile.orca.orm.repository.ContentProviderDetailsRepositoryImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static hsenidmobile.orca.rest.common.util.RestApiKeys.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class ContentProviderService extends BaseService {

    private static final Logger logger = LoggerFactory.getLogger(ContentProviderService.class);
    private static SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
    @Autowired
    protected ContentProviderDetailsRepositoryImpl cpDetailsRepository;
    @Autowired
    private RoutingKeyManager routingKeyManager;
    @Autowired
    private MailSender mailSender;
    @Autowired
    @Qualifier("httpRoutingKeyRegisterRequestSender")
    private HttpRequestSender httpRoutingKeyRegisterRequestSender;
    private boolean sendHttpReuqestForRoutingKey;

    public void saveContentProvider(Registration registration) throws Throwable {
        logger.debug("Started saveContentProvider with : [{}]", registration);
        ContentProvider contentProvider = createContentProvider(registration);
        // to cooperate settings were removed from ui and later need to be
        // supported for both individual and coopertae
        contentProvider.setPersonal(true);
        if (contentProvider.getBillingInfo() != null && registration.getMailType() != null) {
            contentProvider.getBillingInfo().setExpressDelivery(
                    registration.getMailType().equals("Express") ? true : false);
        }
        cpDetailsRepository.save(contentProvider);

        logger.info("---------------------Contents Saved Successfully----------------------------");
    }

    public void updateContentProvider(ContentProvider contentProvider) {
        logger.debug("Started updateContentProvider with : [{}]", contentProvider);
        cpDetailsRepository.update(contentProvider);

        logger.info("---------------------Contents updated Successfully----------------------------");
    }

    public ContentProvider findCpbyCpName(String cpName) {
        return cpDetailsRepository.findByName(cpName);
    }

    public ContentProvider findVerificationCode(String cpChargingNumber) {
        return cpDetailsRepository.findByName(cpChargingNumber);
    }

    protected ContentProvider createContentProvider(Registration registration) throws ContentProviderException {
        ContentProvider contentProvider = new ContentProvider();
        contentProvider.setName(registration.getName());
        contentProvider.setContactPersonName(registration.getContactPersonName());
        contentProvider.setCreateDate(new Date());
        contentProvider.setEmail(registration.getEmail());
        contentProvider.setNic(registration.getNic());
        contentProvider.setTelephone(registration.getTelephone());
        contentProvider.createContentProviderId(registration.getName());
        contentProvider.setDescription(registration.getBillingAddress());
        contentProvider.setStatus(Status.ACTIVE);
        contentProvider.setBillingInfo(new BillingInfo());
        setBillingInfo(contentProvider, registration);
        return contentProvider;
    }

    private void setBillingInfo(ContentProvider contentProvider, Registration registration) {
        if (registration.getBillingAddress() != null) {
            contentProvider.getBillingInfo().setBillingAddress(registration.getBillingAddress());
            contentProvider.getBillingInfo().setBillingName(registration.getBillingName());
        }
        // todo need to set delivery type
    }

    private String generateVerificationCode() {
        NumberFormat numberFormat = new DecimalFormat("000");
        final String currentTime = Double.toString(System.currentTimeMillis());
        return currentTime.substring(currentTime.length() - 3, currentTime.length())
                + numberFormat.format(new Random().nextInt(100));
    }

    public void addRoutingKey(CreateKeyword createKeyword, long correlationId) throws Exception {
        logger.debug("Adding RoutingKey : [{}]", createKeyword);
        createKeyword.setStartDate(dateFormat.parse(createKeyword.getsDate() + " 00:00:00").getTime());
        createKeyword.setEndDate(dateFormat.parse(createKeyword.geteDate() + " 23:59:59").getTime());
        final String[] addressArray = createKeyword.getRoutingKey().getShortCode().getAddress().split("-");
        createKeyword.getRoutingKey().getShortCode().setAddress(addressArray[0]);
        createKeyword.getRoutingKey().getShortCode().setOperator(addressArray[1]);
        try {
            createKeyword.getRoutingKey().setKeyword(createKeyword.getRoutingKey().getKeyword().trim().toLowerCase());
            ContentProvider contentProvider = getContentProviderDetails(CurrentUser.getUsername());
            //check this for rk recursive charging when integrate with mandate module
            final HashMap<String, List<RoutingKey>> ownedRoutingInfo = contentProvider.getOwnedRoutingKeys();
            logger.debug("CP[{}]", contentProvider);
            logger.debug("Create KEYWORD [{}]", createKeyword);
            boolean performRkCreationCharging = true;
            if (ownedRoutingInfo == null || ownedRoutingInfo.size() == 0) {
                logger.info("Creating Keyword for the first time");
                performRkCreationCharging = false;
            }
            if (hasChargingMsisdnVerified(contentProvider)) {
                saveRoutingKey(createKeyword, performRkCreationCharging, correlationId, contentProvider.getSpId());
            } else {
                logger.warn("The CP [ " + contentProvider.getName()
                        + " ] trying to request new keywords without verifying" + "the charging msisdn details");
                throw new ContentProviderException(
                        ContentProviderException.CONTENT_PROVIDER_CHARGING_MSISDN_NOT_VERIFIED);
            }
            logger.info("Contents Saved Successfully");
        } catch (Exception e) {
            logger.error("Error while updating routing info", e);
            throw e;
        }
    }

    public boolean hasChargingMsisdnVerified(ContentProvider contentProvider) {
        return /*contentProvider.getMsisdns().get(0).isMsisdnVerified();*/  true;
    }

    private void saveRoutingKey(CreateKeyword createKeyword, boolean performRkCreationCharging, long correlationId, String spId)
            throws Exception {
        Map<String, String> messageMap = new HashMap<String, String>();
        messageMap.put(SP_ID, spId);
        messageMap.put(SHORTCODE, createKeyword.getRoutingKey().getShortCode().getAddress());
        messageMap.put(OPERATOR, createKeyword.getRoutingKey().getShortCode().getOperator());
        messageMap.put(KEYWORD, createKeyword.getRoutingKey().getKeyword());
        logger.debug("SAVE ROUTING KEY - [{}]", messageMap);
        Map<String, Object> message = sdpRkManageRequestSender.registerRks(messageMap);
    }

    public void sendRkRegisterHttpRequest(CreateKeyword createKeyword, String ncsType, String spId, long endDate,
                                          boolean performRkCreationCharging, long correlationId) throws Exception {
        RoutingKeyRegisterRequest routingKeyRegisterRequest = new RoutingKeyRegisterRequest();
        routingKeyRegisterRequest.setCorrelationId(correlationId);
        routingKeyRegisterRequest.setShortCode(createKeyword.getRoutingKey().getShortCode().getAddress());
        routingKeyRegisterRequest.setKeyword(createKeyword.getRoutingKey().getKeyword());
        routingKeyRegisterRequest.setNcsType(ncsType);
        routingKeyRegisterRequest.setSpId(spId);
        routingKeyRegisterRequest.setExpireDate(String.valueOf(endDate));
        routingKeyRegisterRequest.setPerformRkCreationCharging(performRkCreationCharging);
        routingKeyRegisterRequest.setChargingCycleSize(createKeyword.getChargingCycleSize());
        httpRoutingKeyRegisterRequestSender.sendMessage(routingKeyRegisterRequest);
    }

    public void resetPassword(String userName, String emailAddress) {
        String newPassword = generateNewPassword(userName);
        try {
            mailSender.sendForgotPasswordEmail(emailAddress, userName, newPassword);
            ContentProvider contentProvider = cpDetailsRepository.findByUsername(userName);
            cpDetailsRepository.update(contentProvider);
            logger.debug("content provider update successfully with new password");
        } catch (Exception ex) {
            logger.debug("Password not reset correctly : [{}]", ex);
            throw new RuntimeException("Error has occurred with reseting password");
        }

    }

    public boolean isAllowedToAccess(String userName, String appId) {
        return cpDetailsRepository.isCpUserAllowedToManageRequestedApp(userName, appId);
    }

    private String generateNewPassword(String userName) {
        NumberFormat numberFormat = new DecimalFormat("000000");
        return userName.substring(0, 4) + numberFormat.format(new Random().nextInt(1000000));
    }

    public void setSendHttpReuqestForRoutingKey(boolean sendHttpReuqestForRoutingKey) {
        this.sendHttpReuqestForRoutingKey = sendHttpReuqestForRoutingKey;
    }
}
