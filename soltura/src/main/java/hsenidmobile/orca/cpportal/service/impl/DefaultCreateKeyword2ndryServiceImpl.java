/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hsenidmobile.orca.cpportal.service.impl;

import hsenidmobile.orca.cpportal.module.CreateKeyword;
import hsenidmobile.orca.cpportal.service.CreateKeyword2ndryService;/*
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class DefaultCreateKeyword2ndryServiceImpl implements CreateKeyword2ndryService {

    public boolean isOn() {
        return false;
    }

    public Object formBackingObject(CreateKeyword domain) {
        throw new UnsupportedOperationException("default implementation provided by CreateKeywordController");
    }
}
