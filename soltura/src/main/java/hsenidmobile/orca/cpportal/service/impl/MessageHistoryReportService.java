/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hsenidmobile.orca.cpportal.service.impl;

import hsenidmobile.orca.core.applications.exception.ApplicationException;
import hsenidmobile.orca.core.model.MessageHistory;
import hsenidmobile.orca.core.model.ServiceTypes;
import hsenidmobile.orca.cpportal.module.MessageHistoryInfo;
import hsenidmobile.orca.cpportal.service.impl.application.ApplicationService;
import hsenidmobile.orca.orm.repository.MessageHistoryRepositoryImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.support.RequestContext;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static hsenidmobile.orca.core.model.Event.CONTENT_PUBLISHED;
import static hsenidmobile.orca.core.model.ServiceTypes.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class MessageHistoryReportService {

    private static final Logger logger = LoggerFactory.getLogger(MessageHistoryReportService.class);

    private ApplicationService applicationService;
    private MessageHistoryRepositoryImpl messageHistoryRepository;
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    @Resource(name = "reportMessageTemplates")
    private Map<String, String> messageTemplates;

    public void setCurrentDate(MessageHistoryInfo messageHistoryInfo) {
        messageHistoryInfo.setReportFromDate(dateFormat.format(new Date()));
        messageHistoryInfo.setReportToDate(dateFormat.format(new Date()));
    }

    public void loadAllApplicationsForCp(MessageHistoryInfo messageHistoryInfo) {
        messageHistoryInfo.setAvailableApplicationsForCp(applicationService.getContentProviderDetails(true).
                getApplications());
    }

    public void getMessageHistoryForApps(MessageHistoryInfo messageHistoryInfo, RequestContext ctx) throws ApplicationException {
        List<String> selectedAppIds = messageHistoryInfo.getSelectedApplicationIdsForCp();
        boolean showAll = messageHistoryInfo.isShowAll();
        long fromDate = 0, toDate = 0;

        List<MessageHistory> messageHistoryList;

        try {
            fromDate = dateFormat.parse(messageHistoryInfo.getReportFromDate()).getTime();
            toDate = dateFormat.parse(messageHistoryInfo.getReportToDate()).getTime();
        } catch (ParseException e) {
            logger.info("Validation failed with Message History Report : Error in parsing Dates");
            return;
        }

        messageHistoryList = messageHistoryRepository.getMatchingMessageHistoryForCp(selectedAppIds,
                fromDate, toDate, !showAll);
        formatMessageContent(messageHistoryList, ctx);
        messageHistoryInfo.setMessageHistory(messageHistoryList);
    }

    public void formatMessageContent(List<MessageHistory> messageHistoryList, RequestContext ctx) {
        for (MessageHistory messageHistory : messageHistoryList) {
            getFormattedReportContent(ctx, messageHistory);
        }
    }

    private void getFormattedReportContent(RequestContext ctx, MessageHistory messageHistory) {
        try {
            switch (ServiceTypes.valueOf(messageHistory.getServiceType())) {
                case VOTING:
                    messageHistory.setMessage(ctx.getMessage(messageTemplates.get(VOTING.name()),  new Object[] {
                            messageHistory.getMessageHistoryRecord().getTotalMsgCount()}));
                    break;
                case REQUEST:
                    messageHistory.setMessage(ctx.getMessage(messageTemplates.get(REQUEST.name()),  new Object[] {
                            messageHistory.getMessageHistoryRecord().getTotalMsgCount()}));
                    break;
                case ALERT:
                    messageHistory.setMessage(ctx.getMessage(messageTemplates.get(ALERT.name())));
                    break;
                case SUBSCRIPTION:
                    if(messageHistory.getEvent() == CONTENT_PUBLISHED)  {
                        messageHistory.setMessage(ctx.getMessage(messageTemplates.get(messageHistory.getEvent().name())));
                    } else {
                        messageHistory.setMessage(ctx.getMessage(messageTemplates.get(SUBSCRIPTION.name())));
                    }

                    break;
                case BROADCAST:
                    messageHistory.setMessage(ctx.getMessage(messageTemplates.get(BROADCAST.name()),  new Object[] {
                            messageHistory.getMessageHistoryRecord().getMessageContent(),
                            messageHistory.getMessageHistoryRecord().getMessageSendingStatus(),
                            messageHistory.getMessageHistoryRecord().getTotalSubscriberCount(),
                            messageHistory.getMessageHistoryRecord().getSuccessMsgCount(),
                            messageHistory.getMessageHistoryRecord().getFailedMsgCount()
                    }));
                    break;
                default:
                    logger.warn("Undefined Service Type Found");
                    throw new IllegalStateException("Service Type Not Defined.");
            }
        } catch (IllegalArgumentException e) {
            logger.warn("Undefined Service Type Found");
        }
    }

    public void setMessageHistoryRepository(MessageHistoryRepositoryImpl messageHistoryRepository) {
        this.messageHistoryRepository = messageHistoryRepository;
    }

    public void setApplicationService(ApplicationService applicationService) {
        this.applicationService = applicationService;
    }
}
