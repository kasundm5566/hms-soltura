/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
*/

package hsenidmobile.orca.cpportal.service.impl;

import hsenidmobile.orca.core.repository.ContentProviderRepository;
import hsenidmobile.orca.cpportal.service.MsisdnValidatorMessageTransformer;
import hsenidmobile.orca.cpportal.service.MsisdnValidatorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.net.URL;

/**
 * <p/>
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */

public class MsisdnValidatorServiceImpl implements MsisdnValidatorService {
    private static final Logger logger = LoggerFactory.getLogger(MsisdnValidatorServiceImpl.class);
    private String shortCode;
    private int verificationCodePosition;
    private String defaultAppId;
    private String defaultPassword;
    private URL coreEndPointUrl;

    private MsisdnValidatorMessageTransformer msisdnValidatorMessageTransformer;

    private static final String MESSAGE_SPLITTER = " ";
    private static final String TRIM_PATTERN = "\\s+";

    /*Verify the charging Msisdn with the verification code*/
    @Override
    public void verifyChargingMsisdn(HttpServletRequest request) {

    }

    public void setVerificationCodePosition(int verificationCodePosition) {
        this.verificationCodePosition = verificationCodePosition;
    }


    public void setShortCode(String shortCode) {
        this.shortCode = shortCode;
    }

    public void setSubscriberMsisdnValidatorMessageTransformer(MsisdnValidatorMessageTransformer
            msisdnValidatorMessageTransformer) {
        this.msisdnValidatorMessageTransformer = msisdnValidatorMessageTransformer;
    }

    public String getShortCode() {
        return shortCode;
    }

    public String getDefaultAppId() {
        return defaultAppId;
    }

    public void setDefaultAppId(String defaultAppId) {
        this.defaultAppId = defaultAppId;
    }

    public String getDefaultPassword() {
        return defaultPassword;
    }

    public void setDefaultPassword(String defaultPassword) {
        this.defaultPassword = defaultPassword;
    }

    public void setCoreEndPointUrl(URL coreEndPointUrl) {
        this.coreEndPointUrl = coreEndPointUrl;
    }
}
    
