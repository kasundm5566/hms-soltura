/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hsenidmobile.orca.cpportal.service.impl;

import hsenidmobile.orca.core.applications.exception.RequestServiceException;
import hsenidmobile.orca.core.applications.request.RequestServiceData;
import hsenidmobile.orca.orm.repository.RequestServiceDataRepositoryImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class RequestServiceDataService {

    private static final Logger logger = LoggerFactory.getLogger(RequestServiceDataService.class);
    @Autowired
    private RequestServiceDataRepositoryImpl requestServiceDataRepository;

    public void updateMessageReadStatus(long messageId) throws RequestServiceException {
        RequestServiceData requestServiceData = requestServiceDataRepository.findRequestServiceData(messageId);
        requestServiceData.setRead(true);
        requestServiceDataRepository.updateRequestServiceData(requestServiceData);
    }

    public List<RequestServiceData> getMessagesByCategory(String appId, String subCategoryIds) throws RequestServiceException {
        List<RequestServiceData> requestServiceDataList = new ArrayList<RequestServiceData>();
        String[] selectedValueArray = subCategoryIds.split(",");

//        for (String s : selectedValueArray) {
//            long l = Long.parseLong(s.trim());
        requestServiceDataList.addAll(requestServiceDataRepository.getAllRequestMessagesBySubCategory(appId,
                selectedValueArray));
//        }
        sortByReceivedTime(requestServiceDataList);
        return requestServiceDataList;
    }

    private void sortByReceivedTime(List<RequestServiceData> requestServiceDataList) {
        SortByReceivedTime byReceivedTime = new SortByReceivedTime();
        Collections.sort(requestServiceDataList, byReceivedTime);
    }

    public Map fetchMessagesForInitialLoading(String appId, long requestServiceId) throws RequestServiceException {
        Map dataToXml = new HashMap();
        List<RequestServiceData> requestServiceData = requestServiceDataRepository.getAllRequestMessagesForService(appId, requestServiceId);
        int totalNumberOfRecords = requestServiceDataRepository.getTotalMessageCount(appId);
        dataToXml.put("requestServiceData", requestServiceData);
        dataToXml.put("totalNumberOfRecords", totalNumberOfRecords);
        return dataToXml;
    }

    public Map fetchMessagesForPage(String appId, long requestServiceId, int start, int end) throws RequestServiceException {
        Map dataToXml = new HashMap();
        logger.info("Request service ajax called appId[{}] start[{}] end[{}]", new Object[]{appId, start, end});
        List<RequestServiceData> requestServiceData = requestServiceDataRepository.getRequestMessages(appId, requestServiceId, start, end);
        logger.debug("data fetched size[{}]", requestServiceData.size());
        int totalNumberOfRecords = requestServiceDataRepository.getTotalMessageCount(appId);
        dataToXml.put("requestServiceData", requestServiceData);
        dataToXml.put("totalNumberOfRecords", totalNumberOfRecords);
        return dataToXml;
    }

    public class SortByReceivedTime implements Comparator<RequestServiceData> {
        @Override
        public int compare(RequestServiceData o1, RequestServiceData o2) {
            return ((Long)o2.getReceivedTime()).compareTo((Long)o1.getReceivedTime());
        }
    }
}
