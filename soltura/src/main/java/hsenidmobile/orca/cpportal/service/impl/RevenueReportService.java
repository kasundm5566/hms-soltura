/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hsenidmobile.orca.cpportal.service.impl;

import hsenidmobile.orca.core.applications.exception.ApplicationException;
import hsenidmobile.orca.core.model.RevenueSummary;
import hsenidmobile.orca.cpportal.module.RevenueReportInfo;
import hsenidmobile.orca.cpportal.service.impl.application.ApplicationService;
import hsenidmobile.orca.orm.repository.RevenueReportRepositoryImpl;
import hsenidmobile.orca.orm.sdp.SdpApplicationRepositoryImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class RevenueReportService {

    private static final Logger logger = LoggerFactory.getLogger(RevenueReportService.class);

    @Autowired
    private ApplicationService applicationService;

    @Autowired
    private RevenueReportRepositoryImpl revenueReportRepository;

    @Autowired
    private SdpApplicationRepositoryImpl sdpApplicationRepository;

    private final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    private static final String MOBILE_ORIGINATED = "mobileOriginated";
    private static final String MOBILE_TERMINATED = "mobileTerminated";

    public void getRevenueReportForApps(RevenueReportInfo revenueReportInfo) {
        List<String> selectedAppIds = revenueReportInfo.getSelectedApplicationIdsForCp();
        boolean showAll = revenueReportInfo.isShowAll();
        List<String> sdpAppIdList = new ArrayList<String>();
        Date fromDate;
        Date toDate;

        logger.info("Finding SDP application ID for orcaID");

//        HashMap<String, ApplicationImpl> orcaAppIdSdpApp = new HashMap<String, ApplicationImpl>(selectedAppIds.size());

        String sdpAppId;
        for (String selectedAppId : selectedAppIds) {
            try {
                sdpAppId = sdpApplicationRepository.getSdpAppLoginDataForOrcaApp(selectedAppId).getAppId();
//                orcaAppIdSdpApp.put(sdpAppId, selectedApp);
                sdpAppIdList.add(sdpAppId);
            } catch (ApplicationException e) {
                logger.warn("SDP APP not found for orcaAppId [" + selectedAppId + "]");
            }
        }

        try {
            fromDate = dateFormat.parse(revenueReportInfo.getReportFromDate());
            toDate = dateFormat.parse(revenueReportInfo.getReportToDate());
        } catch (ParseException e) {
            logger.info("Validation failed with Revenue Report : Error in parsing Dates");
            return;
        }

        List<RevenueSummary> revenueSummaries = revenueReportRepository.
                getMatchingRevenueSummaries(sdpAppIdList, fromDate, toDate, !showAll);

        logger.info("found revenue summary count [{}]", revenueSummaries.size());
        setRevenueValuesForAllRevenueSummaries(revenueSummaries);

//        for (RevenueSummary revenueSummary : revenueSummaries) {
//            revenueSummary.getRevenueSummaryPrimaryKey().setApplicationId(orcaAppIdSdpApp.get(revenueSummary.getRevenueSummaryPrimaryKey().getApplicationId()).getAppId());
//            revenueSummary.setApplicationName(orcaAppIdSdpApp.get(revenueSummary.getRevenueSummaryPrimaryKey().getApplicationId()).getAppName());
//        }

        revenueReportInfo.setRevenueSummaries(revenueSummaries);
    }

    private void setRevenueValuesForAllRevenueSummaries(List<RevenueSummary> revenueSummaries) {
        //todo implement sdp reporting integration here
//        logger.info("generating applicationn revenue");
//        App app;
//        String direction;
//
//        for (RevenueSummary revenueSummary : revenueSummaries) {
//            BigDecimal revenueShareValue = new BigDecimal("0.0");
//            try {
//                logger.debug("revenue summary [{}]", revenueSummary);
//                app = appRepostory.findAppById(revenueSummary.getRevenueSummaryPrimaryKey().getApplicationId());
//                direction = revenueSummary.getRevenueSummaryPrimaryKey().getDirection();
//                formattedName(revenueSummary);
//                try {
//                    if (direction.equals(MOBILE_ORIGINATED)) {
//                        Map<String, ChargingSla> chargingSlas = app.getNcsSla(NcsType.SMS).iterator().next().
//                                getMoSla().getChargingSlas();
//                        logger.info("finding charging sla for MO : [{}]", chargingSlas);
//                        final ChargingSla chargingSla = chargingSlas.get("FLAT");
//                        if (chargingSla == null) {
//                            logger.debug("Charging sla is not FLAT : setting revenue share as zero");
//                            // revenue share value set to zero at the beginning of the loop
//                        } else {
//                            revenueShareValue = chargingSla.
//                                    getLocalServiceType().getRevenueShareValue();
//                        }
//                    } else if (direction.equals(MOBILE_TERMINATED)) {
//                        revenueShareValue = app.getNcsSla(NcsType.SMS).iterator().next().getMtSla().getChargingSla().
//                                getLocalServiceType().getRevenueShareValue();
//                    }
//                } catch (Exception e) {
//                    logger.error("Error in finding revenue share for appId [{}]", app.getAppId());
//                }
//                revenueSummary.setTotalRevenue(revenueSummary.getTotalRevenue() * revenueShareValue.longValue() / 100);
//            } catch (AppNotFoundException e) {
//                logger.warn("SDP APP not found for sdpaAppId [" +
//                        revenueSummary.getRevenueSummaryPrimaryKey().getApplicationId() + "]");
//            }
//        }
    }

    private void formattedName(RevenueSummary revenueSummary) {
        String appName = revenueSummary.getApplicationName();
        String alteredAppName= appName.substring(0, 2);
        revenueSummary.setApplicationName(alteredAppName);
    }

    public void setCurrentDate(RevenueReportInfo revenueReportInfo) {
        Calendar calendar = Calendar.getInstance();
        revenueReportInfo.setReportToDate(dateFormat.format(calendar.getTime()));
        calendar.add(Calendar.MONDAY, -1);
        revenueReportInfo.setReportFromDate(dateFormat.format(calendar.getTime()));
    }

    public void loadAllApplicationsForCp(RevenueReportInfo revenueReportInfo) {
        revenueReportInfo.setAvailableApplicationsForCp(applicationService.
                getContentProviderDetails(true).getApplications());
    }
}
