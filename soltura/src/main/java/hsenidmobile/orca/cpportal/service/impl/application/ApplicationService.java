/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hsenidmobile.orca.cpportal.service.impl.application;

import hsenidmobile.orca.core.applications.ApplicationImpl;
import hsenidmobile.orca.core.applications.alert.AlertService;
import hsenidmobile.orca.core.applications.exception.ApplicationException;
import hsenidmobile.orca.core.applications.policy.RegistrationPolicy;
import hsenidmobile.orca.core.applications.request.RequestService;
import hsenidmobile.orca.core.applications.subscription.Subscription;
import hsenidmobile.orca.core.applications.voting.VotingService;
import hsenidmobile.orca.core.applications.voting.impl.NoVotingResponseSpecification;
import hsenidmobile.orca.core.applications.voting.impl.ResultSummaryVotingResponseSpecification;
import hsenidmobile.orca.core.model.*;
import hsenidmobile.orca.core.repository.*;
import hsenidmobile.orca.core.services.RoutingKeyManager;
import hsenidmobile.orca.cpportal.module.ApplicationInfo;
import hsenidmobile.orca.cpportal.module.KeywordDetails;
import hsenidmobile.orca.cpportal.module.ResponseSpecification;
import hsenidmobile.orca.cpportal.service.AbstractCreateApplictionService;
import hsenidmobile.orca.cpportal.service.BaseService;
import hsenidmobile.orca.cpportal.util.MailSender;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.*;

import static hsenidmobile.orca.core.model.ApplicationChargingInfo.AppChargingType.*;
import static hsenidmobile.orca.core.model.Status.*;
import static hsenidmobile.orca.cpportal.util.SubscriptionChargingType.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class ApplicationService extends BaseService {

    private final static Logger logger = LoggerFactory.getLogger(ApplicationService.class);

    private String subscriptionResponseInitialValueEn;
    private String subscriptionResponseInitialValueTa;
    private String subscriptionResponseInitialValueSi;

    private String unSubscribeResponseInitialValueEn;
    private String unSubscribeResponseInitialValueTa;
    private String unSubscribeResponseInitialValueSi;

    private String requestErrorInitialValueEn;
    private String requestErrorInitialValueTa;
    private String requestErrorInitialValueSi;
    private boolean advanceSettingRequired;

    protected String successVotingRespondHeader;
    protected String successVotingRespondWithoutResultHeader;
    protected String successVotingRespondFooter;

    private int maxAlertMessagesPerDay;
    private int maxAlertMessagesPerMonth;

    @Autowired
    private AppRepository appRepository;
    @Autowired
    private RoutingKeyManager routingKeyManager;
    @Autowired
    private SdpApplicationRepository sdpApplicationRepository;
    @Autowired
    private MessageHistoryRepository messageHistoryRepository;
    @Autowired
    private MailSender mailSender;
    @Resource (name = "transactionPointService")
    private AbstractCreateApplictionService transactionPointService;

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
    private boolean sendToActivePd;

    public void createApplication(ApplicationInfo appInfo, String userName) throws Exception {
        ApplicationImpl app = appInfo.getApp();

        app.createAppId(app.getAppName());
        logger.debug("Create app method called with appId:[{}] applicationName:[{}]", new Object[] { app.getAppId(),
                app.getAppName() });
        // todo current time has to be set if start date is today
        final Date startDate = dateFormat.parse(appInfo.getStartDate() + " 00:00:00");
        final Date endDate = dateFormat.parse(appInfo.getEndDate() + " 23:59:59");
        app.setEndDate(new DateTime(endDate.getTime()));
        app.setStartDate(new DateTime(startDate.getTime()));

        logger.debug("SELECTED ROUTING KEYS  [{}]",appInfo.getSelectedRoutingKeyValues());

        if (sendToActivePd) {
            app.setStatus(ACTIVE);
        } else {
            app.setStatus(PENDING_FOR_APPROVAL);
        }
        app.setRegistrationPolicy(RegistrationPolicy.OPEN);
        setApplicationAuthorDetails(appInfo);
        setSubscriptionChargingTypes(appInfo);
        addAdvancesSettings(appInfo);
        addServiceDependentData(appInfo);
        try {
            logger.debug("Creating Orca application : [{}]", app);
            final ContentProvider providerDetails = getContentProviderDetails(true);
            transactionPointService.createApplication(providerDetails, appInfo, app, userName);
        } catch (Exception e) {
            throw e;
        }
    }

    private void addAdvancesSettings(ApplicationInfo appInfo) {
        final ApplicationImpl app = appInfo.getApp();
        app.setSubscriptionResponse(createLocalizedResponse(appInfo.getSubscriptionSuccessMsgEn(), "", ""));
        app.setUnsubscribeResponse(createLocalizedResponse(appInfo.getUnsubscribeSuccessMsgEn(), "", ""));
    }

    private List<LocalizedMessage> createLocalizedResponse(String enMgs, String siMsg, String taMsg) {
        return Arrays.asList(new LocalizedMessage(enMgs, Locale.ENGLISH),
                new LocalizedMessage(siMsg, new Locale("si")), new LocalizedMessage(taMsg, new Locale("ta")));
    }

    public void editApplication(ApplicationInfo applicationInfo) throws Exception {
        addAdvancesSettings(applicationInfo);
        try {
            final ApplicationImpl app = applicationInfo.getApp();
            logger.debug("edit Application with appId:[{}] appName:[{}]",
                    new Object[] { app.getAppId(), app.getAppName() });
            List<KeywordDetails> keywords = applicationInfo.getSelectedValues();
            transactionPointService.editApplication(app, keywords);
        } catch (Exception e) {
            logger.error("Error updating application ", e);
            throw e;
        }
    }

    public List<ApplicationImpl> findCpAppBySpid(String cpUserName, String appSearchInput, String spId)
            throws ApplicationException {
        logger.debug("search application with Content provider Username  :[{}] and search criteria [{}]", new Object[] {
                cpUserName, appSearchInput });
        final List<ApplicationImpl> appList = appRepository.findCpMangedAppByAppName(appSearchInput, spId);
        setApplicationStateInfo(appList);
        return appList;
    }

    private void setApplicationStateInfo(List<ApplicationImpl> applications) throws ApplicationException {
        for (ApplicationImpl application : applications) {
            if (isAllowedTochangeState(application)) {
                application.setSateChageAllowed(true);
            } else {
                application.setSateChageAllowed(false);
            }
        }
    }

    private boolean isAllowedTochangeState(ApplicationImpl application) {
        return application.getService() instanceof VotingService || application.getService() instanceof RequestService;
    }

    public Application findAppById(String appId) throws ApplicationException {
        Application app;
        try {
            app = appRepository.findAppByAppId(appId);
        } catch (ApplicationException e) {
            logger.error("error while findAppByAppId [{}]", e);
            throw e;
        }
        return app;
    }

    private void addServiceDependentData(ApplicationInfo appInfo) throws Exception {
        ApplicationImpl app = appInfo.getApp();
        if (app.getService() instanceof Subscription) {
            addSubscriptionServiceData(appInfo);
        } else if (app.getService() instanceof VotingService) {
            addVotingServiceData(appInfo);
        } else if (app.getService() instanceof AlertService) {
            addAlertServiceData(appInfo);
        } else if (app.getService() instanceof RequestService) {
            addRequestServiceData(appInfo);
        }
    }

    private void setApplicationAuthorDetails(ApplicationInfo appInfo) {
        //todo integrate with common regisrtration to get the msisdn by the username and update the author table with teh received phone number
//        final ArrayList<Author> authors = new ArrayList<Author>();
//        final Author author = new Author();
//        Msisdn selectedMsisdn = null;
//        for (CpChargingMsisdn cpChargingMsisdn : getContentProviderDetails(true).getMsisdns()) {
//            if (cpChargingMsisdn.getMsisdn().getAddress().equals(appInfo.getSelectedChargingMsisdn())) {
//                selectedMsisdn = cpChargingMsisdn.getMsisdn();
//                break;
//            }
//        }
//        if (selectedMsisdn == null) {
//            throw new RuntimeException("Selected Charging MSISDN is not recognized : address ["
//                    + appInfo.getSelectedChargingMsisdn() + "]");
//        }
//        author.setMsisdn(new Msisdn(selectedMsisdn.getAddress(), selectedMsisdn.getOperator()));
//        authors.add(author);
//        appInfo.getApp().setAuthors(authors);
    }


    private void setSubscriptionChargingTypes(ApplicationInfo appInfo) throws OrcaException {
        ApplicationChargingInfo applicationChargingInfo = appInfo.getApp().getApplicationChargingInfo();
        if (appInfo.getSubscriptionChargingType() == ONE_TIME_SUBS_CHARGING) {
            applicationChargingInfo.setChargingType(onetime);
        } else if (appInfo.getSubscriptionChargingType() == DAILY_SUBS_CHARGING) {
            applicationChargingInfo.setChargingType(daily);
        } else if (appInfo.getSubscriptionChargingType() == WEEKLY_SUBS_CHARGING) {
            applicationChargingInfo.setChargingType(weekly);
        } else if (appInfo.getSubscriptionChargingType() == MONTHLY_SUBS_CHARGING) {
            applicationChargingInfo.setChargingType(monthly);
        } else if (appInfo.getSubscriptionChargingType() == FREE_SUBS_CHARGING) {
            applicationChargingInfo.setChargingType(free);
        } else {
            throw new OrcaException("Unsupported charging type found for soltura application when setting subscription charging types");
        }
    }

    private void addSubscriptionServiceData(ApplicationInfo appInfo) {

        ApplicationImpl app = appInfo.getApp();
        Subscription subscription = (Subscription) app.getService();
        subscription.setApplication(app);
        if (appInfo.isSubCategoryRequired()) {
            subscription.setSupportedSubCategories(setKeywordDetails(appInfo.getSelectedValues()));
        }
        subscription.setSupportedPeriodicities(setSupportedPeriodicities(appInfo, subscription));
        subscription.setDefaultLocale(Locale.ENGLISH);
        subscription.setSmsUpdateRequired(appInfo.isSmsUpdate());
    }

    public void addVotingServiceData(ApplicationInfo appInfo) throws Exception {
        logger.debug("Adding Voting Service Data..");
        ApplicationImpl app = appInfo.getApp();
        VotingService votingService = (VotingService) app.getService();
        votingService.setApplication(app);
        votingService.setDefaultLocale(Locale.ENGLISH);
        votingService.setSupportedSubCategories(setKeywordDetails(appInfo.getSelectedValues()));
        votingService.setOneVotePerNumber(appInfo.isOnePerNumber());
        setRespondSpecification(appInfo, votingService);
        app.setRegistrationPolicy(RegistrationPolicy.NONE);
    }

    public void setRespondSpecification(ApplicationInfo appInfo, VotingService votingService) {
        logger.debug("Setting voting response specification[{}]", appInfo.getSelectedResponseSpecification());
        if (ResponseSpecification.valueOf(appInfo.getSelectedResponseSpecification()) == ResponseSpecification.NO_RESPOND) {
            votingService.setResponseSpecification(new NoVotingResponseSpecification());
        } else if (ResponseSpecification.valueOf(appInfo.getSelectedResponseSpecification()) == ResponseSpecification.SUMMERY_RESPOND) {
            setResultSummaryVotingResponse(appInfo, votingService);
        } else {
            logger.debug("Setting default voting response specification[{}]", ResponseSpecification.SUMMERY_RESPOND);
            setResultSummaryVotingResponse(appInfo, votingService);
        }
    }

    /**
     * Create a response for voting application with the current results
     *
     * @param appInfo
     * @param votingService
     */
    private void setResultSummaryVotingResponse(ApplicationInfo appInfo, VotingService votingService) {
        final ResultSummaryVotingResponseSpecification summaryVotingResponseSpecification = new ResultSummaryVotingResponseSpecification();
        List<LocalizedMessage> localizedMessageList = new ArrayList<LocalizedMessage>();
        LocalizedMessage message;
        if (votingService.isOneVotePerNumber()) {
            message = new LocalizedMessage(successVotingRespondHeader, Locale.ENGLISH);
        } else {
            message = new LocalizedMessage(successVotingRespondWithoutResultHeader, Locale.ENGLISH);
        }
        localizedMessageList.add(message);
        summaryVotingResponseSpecification.setResponseTemplate(localizedMessageList);
        votingService.setResponseSpecification(summaryVotingResponseSpecification);
    }

    private void addAlertServiceData(ApplicationInfo appInfo) {
        ApplicationImpl app = appInfo.getApp();
        AlertService alertService = (AlertService) app.getService();
        alertService.setDefaultLocale(Locale.ENGLISH);
        alertService.setApplication(app);
        alertService.setMapd(maxAlertMessagesPerDay);
        alertService.setMapm(maxAlertMessagesPerMonth);
        if (appInfo.isSubCategoryRequired()) {
            alertService.setSupportedSubCategories(setKeywordDetails(appInfo.getSelectedValues()));
        }
        alertService.setSmsUpdateRequired(appInfo.isSmsUpdate());
    }

    /**
     * Sets the request service data
     *
     * @param appInfo
     */
    private void addRequestServiceData(ApplicationInfo appInfo) {
        ApplicationImpl app = appInfo.getApp();
        RequestService requestService = (RequestService) app.getService();
        requestService.setDefaultLocale(Locale.ENGLISH);
        requestService.setApplication(app);
        requestService.setResponseMessages(Arrays.asList(new LocalizedMessage(appInfo.getRequestAppResponseMsg(),
                Locale.ENGLISH)));
        requestService.setCommonResponseAvailable(app.isRequireCommonResponse());
        if (appInfo.isSubCategoryRequired()) {
            requestService.setSupportedSubCategories(setKeywordDetails(appInfo.getSelectedValues()));
        }
    }

    public ApplicationInfo initialDataGenerator() throws Exception {
        logger.debug("adding initial Data");
        final ApplicationInfo initialApplication = new ApplicationInfo();
        try {
            initialApplication.setApp(new ApplicationImpl());
            initialApplication.setSubscriptionSuccessMsgEn(subscriptionResponseInitialValueEn);
            initialApplication.setUnsubscribeSuccessMsgEn(unSubscribeResponseInitialValueEn);
//            Removed Invalid Request Message
//            initialApplication.setRequestErrorMsgEn(requestErrorInitialValueEn);
            initialApplication.setSuccessVotingRespondHeader(successVotingRespondHeader);
            initialApplication.setSuccessVotingRespondFooter(successVotingRespondFooter);
            logger.info("Added initial Data SuccessFully!");
        } catch (Exception e) {
            logger.error("Error Occurred while generating initial data ", e);
            throw e;
        }
        return initialApplication;
    }

    public void suspendApp(String appId) throws ApplicationException {
        ApplicationImpl application = (ApplicationImpl) appRepository.findAppByAppId(appId);
        application.setStatus(BLOCKED);
        appRepository.updateApplication(application);
    }

    public void activateApp(String appId) throws ApplicationException {
        ApplicationImpl application = (ApplicationImpl) appRepository.findAppByAppId(appId);
        application.setStatus(ACTIVE);
        appRepository.updateApplication(application);
    }

    public void sendSuccessMail(ApplicationInfo applicationInfo, String username){
        ContentProvider cp = cpDetailsRepository.findByUsername(username);
        mailSender.sendApplicationCreationSuccessEmail(applicationInfo, cp.getEmail(), cp.getName());
    }


    public Status updateOrcaApplicationState(final Application application) {
        Status updatedStatus =  application.getStatus();
        try {
            String sdpAppState = appRepository.findSdpAppState(application.getAppId());
            if((sdpAppState.equals(Status.RETIRED.name()) && updatedStatus != Status.RETIRED)) {
                application.setStatus(Status.RETIRED);
                appRepository.updateApplication(application);
                updatedStatus = application.getStatus();
                logger.info("Moved Soltura application [ " + application.getAppName() + " ] to Retired state hence the " +
                        "SDP application got retired");
            }
        } catch (Exception e) {
            logger.error("Error while retiring the soltura application when the SDP Application got retired");
        }
        return updatedStatus;
    }

    public void setSubscriptionResponseInitialValueEn(String subscriptionResponseInitialValueEn) {
        this.subscriptionResponseInitialValueEn = subscriptionResponseInitialValueEn;
    }

    public void setSubscriptionResponseInitialValueTa(String subscriptionResponseInitialValueTa) {
        this.subscriptionResponseInitialValueTa = subscriptionResponseInitialValueTa;
    }

    public void setSubscriptionResponseInitialValueSi(String subscriptionResponseInitialValueSi) {
        this.subscriptionResponseInitialValueSi = subscriptionResponseInitialValueSi;
    }

    public void setUnSubscribeResponseInitialValueEn(String unSubscribeResponseInitialValueEn) {
        this.unSubscribeResponseInitialValueEn = unSubscribeResponseInitialValueEn;
    }

    public void setUnSubscribeResponseInitialValueTa(String unSubscribeResponseInitialValueTa) {
        this.unSubscribeResponseInitialValueTa = unSubscribeResponseInitialValueTa;
    }

    public void setUnSubscribeResponseInitialValueSi(String unSubscribeResponseInitialValueSi) {
        this.unSubscribeResponseInitialValueSi = unSubscribeResponseInitialValueSi;
    }

    public void setRequestErrorInitialValueEn(String requestErrorInitialValueEn) {
        this.requestErrorInitialValueEn = requestErrorInitialValueEn;
    }

    public void setRequestErrorInitialValueTa(String requestErrorInitialValueTa) {
        this.requestErrorInitialValueTa = requestErrorInitialValueTa;
    }

    public void setRequestErrorInitialValueSi(String requestErrorInitialValueSi) {
        this.requestErrorInitialValueSi = requestErrorInitialValueSi;
    }

    public void setSuccessVotingRespondHeader(String successVotingRespondHeader) {
        this.successVotingRespondHeader = successVotingRespondHeader;
    }

    public void setSuccessVotingRespondWithoutResultHeader(String successVotingRespondWithoutResultHeader) {
        this.successVotingRespondWithoutResultHeader = successVotingRespondWithoutResultHeader;
    }

    public void setSuccessVotingRespondFooter(String successVotingRespondFooter) {
        this.successVotingRespondFooter = successVotingRespondFooter;
    }

    public void setSendToActivePd(boolean sendToActivePd) {
        this.sendToActivePd = sendToActivePd;
    }

    public boolean isAdvanceSettingRequired() {
        return advanceSettingRequired;
    }

    public void setAdvanceSettingRequired(boolean advanceSettingRequired) {
        this.advanceSettingRequired = advanceSettingRequired;
    }

    public int getMaxAlertMessagesPerDay() {
        return maxAlertMessagesPerDay;
    }

    public void setMaxAlertMessagesPerDay(int maxAlertMessagesPerDay) {
        this.maxAlertMessagesPerDay = maxAlertMessagesPerDay;
    }

    public int getMaxAlertMessagesPerMonth() {
        return maxAlertMessagesPerMonth;
    }

    public void setMaxAlertMessagesPerMonth(int maxAlertMessagesPerMonth) {
        this.maxAlertMessagesPerMonth = maxAlertMessagesPerMonth;
    }

    public AbstractCreateApplictionService getTransactionPointService() {
        return transactionPointService;
    }
}