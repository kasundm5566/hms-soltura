/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hsenidmobile.orca.cpportal.service.impl.application;

import hsenidmobile.orca.core.applications.exception.ApplicationException;
import hsenidmobile.orca.core.applications.subscription.Periodicity;
import hsenidmobile.orca.core.applications.subscription.Subscription;
import hsenidmobile.orca.core.applications.subscription.SubscriptionContentRelease;
import hsenidmobile.orca.core.model.Content;
import hsenidmobile.orca.core.model.ContentRelease;
import hsenidmobile.orca.core.model.LocalizedMessage;
import hsenidmobile.orca.cpportal.module.ContentKeywordDetail;
import hsenidmobile.orca.cpportal.module.ContentKeywordDetail.ContentStatus;
import hsenidmobile.orca.cpportal.module.KeywordDetails;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import static ch.lambdaj.Lambda.on;
import static ch.lambdaj.Lambda.sort;
import static hsenidmobile.orca.core.applications.subscription.SubscriptionContentRelease.ContentReleaseStatus.DISCARDED;
import static hsenidmobile.orca.core.applications.subscription.SubscriptionContentRelease.ContentReleaseStatus.FUTURE;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class SubscriptionApplicationService {

    private final static Logger logger = LoggerFactory.getLogger(SubscriptionApplicationService.class);

    public ContentKeywordDetail createNewContentKeywordDetail(Periodicity subscriptionPeriodicity,
                                                              List<String> availableSubKeywords, ContentKeywordDetail lastScheduledContent) {
        ContentKeywordDetail contentKeywordDetail = new ContentKeywordDetail();
        contentKeywordDetail.setStatus(ContentStatus.NEW);
        setSubKeywords(availableSubKeywords, contentKeywordDetail);
        if (lastScheduledContent == null) {
            logger.debug("Last scheduled content not available, calculating schedule time based on current time");

            DateTime currentTime = new DateTime();
            DateTime scheduleTimeBasedOnCurrentTime = subscriptionPeriodicity.getNextScheduledDispatchTime(currentTime);

            if (isLastContentSentTimeWithinScheduleTimeBuffer(subscriptionPeriodicity, scheduleTimeBasedOnCurrentTime)) {
                DateTime lastSentTime = subscriptionPeriodicity.getLastSentTime();
                logger.debug("Last scheduled content not available; but last content sent time[{}]"
                        + " is within schedule time buffer[{}/{}], so calculating schedule time based on taking"
                        + "[{}] as last scheduled time.", new Object[] { lastSentTime, scheduleTimeBasedOnCurrentTime,
                        subscriptionPeriodicity.getAllowedBufferTime(), scheduleTimeBasedOnCurrentTime });
                contentKeywordDetail.setScheduledDate(subscriptionPeriodicity
                        .getNextScheduleTimeBasedOnLastScheduleTime(scheduleTimeBasedOnCurrentTime.toDate()));
            } else {
                contentKeywordDetail.setScheduledDate(scheduleTimeBasedOnCurrentTime.toDate());
            }
        } else {
            logger.debug("Calculating schedule time based on last content's[{}] schedule time",
                    lastScheduledContent.getScheduledDate());
            if (isScheduleTimeAfterNow(subscriptionPeriodicity, new DateTime(lastScheduledContent.getScheduledDate()))) {
                contentKeywordDetail.setScheduledDate(subscriptionPeriodicity
                        .getNextScheduleTimeBasedOnLastScheduleTime(lastScheduledContent.getScheduledDate()));
            } else {
                logger.debug("Last content's schedule time already passed, calculating schedule time based on current time");
                contentKeywordDetail.setScheduledDate(subscriptionPeriodicity.getNextScheduledDispatchTime(
                        new DateTime()).toDate());
            }

        }
        contentKeywordDetail.setShowPeriod(DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM).format(
                contentKeywordDetail.getScheduledDate()));

        logger.debug("Created new content[{}]", contentKeywordDetail);
        return contentKeywordDetail;
    }

    private boolean isLastContentSentTimeWithinScheduleTimeBuffer(Periodicity subscriptionPeriodicity,
                                                                  DateTime scheduleTimeBasedOnCurrentTime) {
        // todo check this
        if (subscriptionPeriodicity.getLastSentTime() != null
                && subscriptionPeriodicity.getLastSentTime().getMillis() > 0) {
            DateTime lastSentTime = new DateTime(subscriptionPeriodicity.getLastSentTime());
            logger.debug("Last content sent time[{}] scheduleTimeBasedOnCurrentTime [{}]", new Object[] { lastSentTime,
                    scheduleTimeBasedOnCurrentTime });
            return lastSentTime.isAfter(scheduleTimeBasedOnCurrentTime);

        } else {
            return false;
        }
    }

    private void setSubKeywords(List<String> availableSubKeywords, ContentKeywordDetail contentKeywordDetail) {
        if (availableSubKeywords == null || availableSubKeywords.isEmpty()) {
            logger.debug("Subkeywords not supported.");
            contentKeywordDetail.addKeyword(new KeywordDetails("", ""));
        } else {
            logger.debug("Subkeywords[{}] supported.", availableSubKeywords);
            for (String subkey : availableSubKeywords) {
                contentKeywordDetail.addKeyword(new KeywordDetails(subkey, ""));
            }
        }
    }

    // todo commented due to the build failure need to check
    public List<ContentKeywordDetail> getEditableContent(List<SubscriptionContentRelease> contentReleaseList,
                                                         String serviceKeyword, Subscription subscription) {
        if (logger.isDebugEnabled()) {
            logger.debug("Getting editable contents.");
        }
        List<ContentKeywordDetail> editableContentKeywordDetailsList = new ArrayList<ContentKeywordDetail>();

        for (SubscriptionContentRelease contentRelease : contentReleaseList) {
            if ((contentRelease.getStatus() == FUTURE || contentRelease.getStatus() == DISCARDED)
                    && isScheduleTimeAfterNow(contentRelease.getPeriodicity(),
                    contentRelease.getScheduledDispatchTimeFrom())) {
                ContentKeywordDetail contentKeywordDetail = createContentKeywordDetailFromContentGroup(contentRelease,
                        serviceKeyword, subscription);
                contentKeywordDetail.setPeriodicity(contentRelease.getPeriodicity());
                if (logger.isDebugEnabled()) {
                    logger.debug("Found editable content[" + contentKeywordDetail + "]");
                }
                editableContentKeywordDetailsList.add(contentKeywordDetail);
            } else {
                if (logger.isDebugEnabled()) {
                    logger.debug("Content not editable[" + contentRelease + "]");
                }
            }
        }
        return editableContentKeywordDetailsList;
    }

    private boolean isScheduleTimeAfterNow(Periodicity periodicity, DateTime scheduleDispatchTime) {
        DateTime scheduleTime = new DateTime(scheduleDispatchTime);
        // scheduleTime =
        // scheduleTime.plusMinutes(periodicity.getAllowedBufferTime());
        return scheduleTime.isAfterNow();
    }

    private KeywordDetails createKeywordsForUI(Content content) {
        KeywordDetails keywordDetails = new KeywordDetails();
        keywordDetails.setKeyword(content.getSubCategory().getKeyword());
        keywordDetails.setDescriptionEn(content.getContent().getMessage());

        // TODO do the localization refactoring
        // if (content.getDescription(new Locale("ta")) != null) {
        // keywordDetails.setDescriptionTa(content.getDescription(Locale.));
        // } else {
        // keywordDetails.setDescriptionTa("");
        // }
        // if (content.getDescription(new Locale("si")) != null) {
        // keywordDetails.setDescriptionSi(content.getDescription(new
        // Locale("si")));
        // } else {
        // keywordDetails.setDescriptionSi("");
        // }
        return keywordDetails;
    }

    // todo commented due to the build failure need to check
    private ContentKeywordDetail createContentKeywordDetailFromContentGroup(SubscriptionContentRelease contentRelease,
                                                                            String serviceKeyword, Subscription subscription) {
        ContentKeywordDetail contentKeywordDetail = new ContentKeywordDetail();
        contentKeywordDetail.setShowPeriod(DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM).format(
                contentRelease.getScheduledDispatchTimeFrom().toDate()));
        contentKeywordDetail.setScheduledDate(contentRelease.getScheduledDispatchTimeFrom().toDate());
        // todo set this as needed
        contentKeywordDetail.setStatus(contentRelease.getStatus());
        contentKeywordDetail.setContentGroupId(contentRelease.getId());
        if (subscription.isSubCategoriesSupported()) {
            logger.debug("Application supports subkeywords");
            for (Content content : contentRelease.getContents()) {
                contentKeywordDetail.addKeyword(createKeywordsForUI(content));
            }
        } else {
            logger.debug("Application DO NOT supports subkeywords");
            KeywordDetails keywordDetails = new KeywordDetails();
            keywordDetails.setKeyword(serviceKeyword);
            keywordDetails.setDescriptionEn(contentRelease.getContents().get(0).getContent().getMessage());
            contentKeywordDetail.addKeyword(keywordDetails);
        }
        return contentKeywordDetail;
    }

    public List<ContentKeywordDetail> createNewContentKeywordDetailList(Periodicity subscriptionPeriodicity,
                                                                        List<String> availableSubKeywords, final ContentKeywordDetail lastScheduledContent,
                                                                        int numberOfNewContentsRequired) {

        List<ContentKeywordDetail> newContentList = new ArrayList<ContentKeywordDetail>();
        ContentKeywordDetail lastContent = lastScheduledContent;
        for (int i = 0; i < numberOfNewContentsRequired; i++) {
            ContentKeywordDetail newContent = createNewContentKeywordDetail(subscriptionPeriodicity,
                    availableSubKeywords, lastContent);
            lastContent = newContent;

            newContentList.add(newContent);
        }

        return newContentList;
    }

    private List<Content> createKeywordDetailFromUiKeywordDetails(List<KeywordDetails> details,
                                                                  Subscription subscription) throws ApplicationException {
        List<Content> contents = new ArrayList<Content>();
        for (KeywordDetails keywordDetails : details) {
            Content content = new Content();
            content.setSubCategory(subscription.getSubcategory(keywordDetails.getKeyword().trim()));
            content.setContent(new LocalizedMessage(keywordDetails.getDescriptionEn(), Locale.ENGLISH));
            content.setSource(Content.ContentSource.WEB);
            contents.add(content);
        }
        return contents;
    }

    public List<ContentRelease> createContentReleaseFromContentKeywordDetailsList(
            List<ContentKeywordDetail> contentKeywordDetailsList, Subscription subscription, String appId, String appName)
            throws ApplicationException {
        List<ContentRelease> contentReleases = new ArrayList<ContentRelease>();
        if (contentKeywordDetailsList != null) {
            for (ContentKeywordDetail contentKeywordDetail : contentKeywordDetailsList) {
                SubscriptionContentRelease subscriptionContentRelease = new SubscriptionContentRelease();
                subscriptionContentRelease.setAppId(appId);
                subscriptionContentRelease.setAppName(appName);
                subscriptionContentRelease.setPeriodicity(subscription.getSupportedPeriodicities().get(0));
                subscriptionContentRelease.setScheduledDispatchTimeFrom(new DateTime(contentKeywordDetail
                        .getScheduledDate()));
                subscriptionContentRelease.setScheduledDispatchTimeTo(subscriptionContentRelease
                        .getScheduledDispatchTimeFrom().plusMinutes(
                        subscriptionContentRelease.getPeriodicity().getAllowedBufferTime()));
                subscriptionContentRelease.setContents(createKeywordDetailFromUiKeywordDetails(
                        contentKeywordDetail.getDetails(), subscription));
                subscriptionContentRelease.setId(contentKeywordDetail.getContentGroupId());
                contentReleases.add(subscriptionContentRelease);
            }
        }
        return contentReleases;
    }

    public List<ContentKeywordDetail> filterBlankContent(List<ContentKeywordDetail> upatedKeywordDetailsList) {
        logger.debug("Filtering blank contents");

        List<ContentKeywordDetail> filteredList = new ArrayList<ContentKeywordDetail>();

        if (upatedKeywordDetailsList != null) {
            for (ContentKeywordDetail content : upatedKeywordDetailsList) {
                if (!content.isEmpty() || (content.getContentGroupId() != null && content.getContentGroupId() > 0)) {
                    filteredList.add(content);
                } else {
                    logger.debug("Removed blank content[{}]", content);
                }
            }
        }
        return filteredList;
    }

    public List<ContentKeywordDetail> sortContentsByScheduleTime(List<ContentKeywordDetail> contentKeywordDetailsList) {
        List<ContentKeywordDetail> sortedList = new ArrayList<ContentKeywordDetail>();
        if (contentKeywordDetailsList != null) {
            sortedList = sort(contentKeywordDetailsList, on(ContentKeywordDetail.class).getScheduledDate());
        }

        return sortedList;
    }

    public List<ContentKeywordDetail> fillMissingTimeSlots(Periodicity periodicity,
                                                           List<ContentKeywordDetail> contentList) {
        logger.debug("Start for filling missing time slots.");
        List<ContentKeywordDetail> newList = new ArrayList<ContentKeywordDetail>();

        contentList = sortContentsByScheduleTime(contentList);
        if (contentList != null && contentList.size() > 0) {
            DateTime scheduleStartDate = new DateTime(contentList.get(0).getScheduledDate());
            DateTime scheduleEndDate = new DateTime(contentList.get(contentList.size() - 1).getScheduledDate());
            List<DateTime> allAvailableTimeSlots = calculateAllAvailableTimeSlots(periodicity, scheduleStartDate,
                    scheduleEndDate);

            List<String> availableSubKeywords = new ArrayList<String>();
            for (KeywordDetails detail : contentList.get(0).getDetails()) {
                availableSubKeywords.add(detail.getKeyword());
            }

            for (DateTime dateTime : allAvailableTimeSlots) {
                ContentKeywordDetail kd = findContentKeywordDetailScheduledOn(dateTime, contentList);
                if (kd != null) {
                    newList.add(kd);
                } else {
                    newList.add(createContentKeywordDetailWithScheduleTime(availableSubKeywords, dateTime));
                }
            }
        }
        return newList;
    }

    private ContentKeywordDetail createContentKeywordDetailWithScheduleTime(List<String> availableSubKeywords,
                                                                            DateTime dateTime) {
        ContentKeywordDetail contentKeywordDetail = new ContentKeywordDetail();
        contentKeywordDetail.setStatus(ContentStatus.NEW);
        setSubKeywords(availableSubKeywords, contentKeywordDetail);
        contentKeywordDetail.setScheduledDate(dateTime.toDate());
        contentKeywordDetail.setShowPeriod(DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM).format(
                contentKeywordDetail.getScheduledDate()));

        logger.debug("Created new content[{}]", contentKeywordDetail);
        return contentKeywordDetail;
    }

    private ContentKeywordDetail findContentKeywordDetailScheduledOn(DateTime dateTime,
                                                                     List<ContentKeywordDetail> contentList) {
        for (ContentKeywordDetail contentKeywordDetail : contentList) {
            if (dateTime.isEqual(contentKeywordDetail.getScheduledDate().getTime())) {
                logger.debug("Found already scheduled content[{}]", contentKeywordDetail);
                return contentKeywordDetail;
            }
        }

        return null;
    }

    private List<DateTime> calculateAllAvailableTimeSlots(Periodicity periodicity, DateTime scheduleStartDate,
                                                          DateTime scheduleEndDate) {
        logger.debug("Generating all available time slots between[{}/{}]", new Object[] { scheduleStartDate,
                scheduleEndDate });
        if (scheduleEndDate.isBefore(scheduleStartDate)) {
            throw new IllegalArgumentException("End date[" + scheduleEndDate + "] is before start date["
                    + scheduleStartDate + "]");
        }

        if (scheduleStartDate.isEqual(scheduleEndDate)) {
            return Arrays.asList(scheduleStartDate);
        } else {
            List<DateTime> timeSlots = new ArrayList<DateTime>();
            timeSlots.add(scheduleStartDate);
            DateTime nextSchedule = scheduleStartDate;
            while (nextSchedule.isBefore(scheduleEndDate)) {
                nextSchedule = new DateTime(periodicity.getNextScheduleTimeBasedOnLastScheduleTime(nextSchedule
                        .toDate()));
                timeSlots.add(nextSchedule);
            }

            logger.debug("All available time slots[{}]", timeSlots);
            return timeSlots;

        }
    }
}
