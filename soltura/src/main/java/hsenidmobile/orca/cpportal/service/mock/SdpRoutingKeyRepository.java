/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hsenidmobile.orca.cpportal.service.mock;

import hsenidmobile.orca.core.model.Msisdn;
import hsenidmobile.orca.core.model.RoutingKey;
import hsenidmobile.orca.cpportal.module.CreateKeyword;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.String;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */

public class SdpRoutingKeyRepository {

    private static final Logger logger = LoggerFactory.getLogger(SdpRoutingKeyRepository.class);
    private static HashMap<String, List<RoutingKey>> availableRoutingKeyMap = new HashMap<String, List<RoutingKey>>();
    private static HashMap<String, List<String>> selectedRoutingKeyMap = new HashMap<String, List<String>>();
    private static List<String> availableKeywords = new ArrayList<String>();

 //   List<MyType> myList = new ArrayList<MyType>();

    static {
        //todo integrate this with the API - added hardcoded value to create the ui flows
        RoutingKey rk1 = new RoutingKey(new Msisdn("2545", "Safaricom"), "sk1");
        RoutingKey rk4 = new RoutingKey(new Msisdn("2545", "Safaricom"), "sk2");
        RoutingKey rk2 = new RoutingKey(new Msisdn("9696", "Yu"), "yk3");
        RoutingKey rk5 = new RoutingKey(new Msisdn("9696", "Yu"), "yk4");
        RoutingKey rk3 = new RoutingKey(new Msisdn("1234", "Airtel"), "ak5");
        RoutingKey rk6 = new RoutingKey(new Msisdn("1234", "Airtel"), "ak6");

        List<RoutingKey> rkKeyList1 = new ArrayList<RoutingKey>();
        List<RoutingKey> rkKeyList2 = new ArrayList<RoutingKey>();
        List<RoutingKey> rkKeyList3 = new ArrayList<RoutingKey>();

        rkKeyList1.add(rk1);
        rkKeyList1.add(rk4);
        rkKeyList2.add(rk2);
        rkKeyList2.add(rk5);
        rkKeyList3.add(rk3);
        rkKeyList3.add(rk6);

        //Available keyword list for Validating process.
        availableKeywords.add("Keyword1");
        availableKeywords.add("Keyword2");
        availableKeywords.add("Keyword3");

        // change --> for each
        availableRoutingKeyMap.put(rk1.getShortCode().getOperator(),rkKeyList1);
        availableRoutingKeyMap.put(rk2.getShortCode().getOperator(),rkKeyList2);
        availableRoutingKeyMap.put(rk3.getShortCode().getOperator(),rkKeyList3);
    }

    public static void insertNewRk(CreateKeyword createKeyword) {
        RoutingKey key = createKeyword.getRoutingKey();
        availableRoutingKeyMap.get(key.getShortCode().getOperator()).add(key);
    }

    public static HashMap<String, List<RoutingKey>> getAvailableRks() {
        return availableRoutingKeyMap;
    }

    public static void insertNewSelecteditem(String appId, List<String> RoutKeys) {

        logger.debug("AppId [{}] inserted into map", appId);
        int size = RoutKeys.size();
        for (int i=0;i<size;) {
            if(RoutKeys.get(i).equals("N/A")) {
                RoutKeys.remove(i);
                size--;
                i--;
            }
            i++;
        }

        if (RoutKeys.size() == 0) {
            RoutKeys.add("operator1");
        }
        if(selectedRoutingKeyMap.get(appId) == null){
            selectedRoutingKeyMap.put(appId,RoutKeys);
        }else {
            List<String> ExistingList = selectedRoutingKeyMap.get(appId);
            ExistingList.addAll(RoutKeys);
        }
        logger.debug("Successfully inserted into the Map [{}] ", selectedRoutingKeyMap.get(appId).toString());
    }

    public static List<String> getSelectedRoutingKeyListById(String appName) {
        logger.debug("Successfully returned [{}] ", selectedRoutingKeyMap.get(appName).toString());
        return selectedRoutingKeyMap.get(appName);
    }

    public static List<String> getAvailableKeywords() {
        return availableKeywords;
    }

}

