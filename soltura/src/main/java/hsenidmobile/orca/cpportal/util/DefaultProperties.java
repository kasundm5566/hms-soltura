/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hsenidmobile.orca.cpportal.util;
/*
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */

public class DefaultProperties {

    private int maxDispatchableContentCount = 12;
    private int maxMessageLength = 160;
    private boolean isTestbedVersion;
    private boolean isSingleOperatorDeployment;
    private int serverId;
    private String baseUrl;
    private String [] rkCategories;

    public int getMaxDispatchableContentCount() {
        return maxDispatchableContentCount;
    }

    public void setMaxDispatchableContentCount(int maxDispatchableContentCount) {
        this.maxDispatchableContentCount = maxDispatchableContentCount;
    }

    public int getMaxMessageLength() {
        return maxMessageLength;
    }

    public void setMaxMessageLength(int maxMessageLength) {
        this.maxMessageLength = maxMessageLength;
    }

    public boolean isTestbedVersion() {
        return isTestbedVersion;
    }

    public void setTestbedVersion(boolean testbedVersion) {
        isTestbedVersion = testbedVersion;
    }

    public boolean isSingleOperatorDeployment() { return isSingleOperatorDeployment; }

    public void setSingleOperatorDeployment(boolean isSingleOperatorDeployment) {
        this.isSingleOperatorDeployment = isSingleOperatorDeployment;
    }

    public int getServerId() {
		return serverId;
	}

	public void setServerId(int serverId) {
		this.serverId = serverId;
	}

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

	public String[] getRkCategories() {
		return rkCategories;
	}

	public void setRkCategories(String[] rkCategories) {
		this.rkCategories = rkCategories;
	}


}
