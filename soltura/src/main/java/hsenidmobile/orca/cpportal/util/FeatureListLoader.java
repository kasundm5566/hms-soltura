/*
 *   (C) Copyright 2008-2012 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate: $
 *   $LastChangedBy: $
 *   $LastChangedRevision: $
 *
 */
package hsenidmobile.orca.cpportal.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * $LastChangedDate: $ $LastChangedBy: $ $LastChangedRevision: $
 *
 */
public class FeatureListLoader {
	private static final Logger logger = LoggerFactory.getLogger(FeatureListLoader.class);
	private static final String FEATURE_IDENTIFIER = "feature.";
	private final Map<String, Boolean> featureList;

	public FeatureListLoader(Properties fp) {
		Map<String, Boolean> flm = new HashMap<String, Boolean>();
		logger.info("Started loading feature list");
		Set features = fp.keySet();
		for (Object key : features) {
			String fk = (String) key;
			if (fk.startsWith(FEATURE_IDENTIFIER)) {
				if (Boolean.parseBoolean(fp.getProperty(fk, "false"))) {
					logger.info("Found enabled feature: [{}]", fk);
					flm.put(fk.replace(FEATURE_IDENTIFIER, ""), true);
				} else {
					logger.debug("Feature [{}] is disabled", fk);
				}
			} else {
				logger.debug("Ignored key[{}]", fk);
			}
		}
		this.featureList = flm;
	}

	public Map<String, Boolean> getFeatureList() {
		return featureList;
	}

}
