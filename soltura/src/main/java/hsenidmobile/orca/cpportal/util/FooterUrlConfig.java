/*
 *   (C) Copyright 2008-2012 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate: $
 *   $LastChangedBy: $
 *   $LastChangedRevision: $
 *
 */
package hsenidmobile.orca.cpportal.util;

import java.util.Properties;

/**
 * $LastChangedDate: $ $LastChangedBy: $ $LastChangedRevision: $
 *
 */
public class FooterUrlConfig {

	public static Properties cpPortalProps;

	public static String getLink(String key) {
		return cpPortalProps.getProperty(key, "");
	}

	public static void setCpPortalProps(Properties cpPortalProps) {
		FooterUrlConfig.cpPortalProps = cpPortalProps;
	}



}
