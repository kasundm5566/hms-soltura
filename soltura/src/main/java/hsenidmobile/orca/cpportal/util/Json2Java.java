/**
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hsenidmobile.orca.cpportal.util;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import hms.common.rest.util.Message;
import hsenidmobile.orca.core.model.Msisdn;
import hsenidmobile.orca.core.model.RoutingKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import static hsenidmobile.orca.rest.common.util.RestApiKeys.*;

/*
* $LastChangedDate$
* $LastChangedBy$
* $LastChangedRevision$
*/

public class Json2Java {

    private final static Logger logger = LoggerFactory.getLogger(Json2Java.class);

//    @Resource(name = "availableOperators")
    public static List<String> availableOperators;

    /**
     * getMap provides a Map representation of the JSON Object
     *
     * @param jsonResponse The JSON object string
     * @return Map of JSONObject.
     */
    public static Map<String, String> getMap(String jsonResponse) {
        Gson gson = new Gson();
        Map<String, String> mapResponse =
                gson.fromJson(jsonResponse, new TypeToken<Map<String, String>>() {
                }.getType());
        return mapResponse;
    }


    public static List<String> getList(String jsonResponse) {
        Gson gson = new Gson();
        List<String> mapResponse =
                gson.fromJson(jsonResponse, new TypeToken<List<String>>() {
                }.getType());
        return mapResponse;
    }


    public static HashMap<String, List<RoutingKey>> getAvailableRoutingKeys(Map<String, Map<String, List<String>>> response) {
        HashMap<String, List<RoutingKey>> availableRoutingKey = new HashMap<String, List<RoutingKey>>();
        for (String availableOperator : availableOperators) {
            List<RoutingKey> rkKeyList = new ArrayList<RoutingKey>();
            for (Map.Entry<String, Map<String, List<String>>> entry : response.entrySet()) {
                if (availableOperator.equals(entry.getKey())) {
                    rkKeyList = getRoutingKeyList(availableOperator, entry.getValue());
                }
            }
            availableRoutingKey.put(availableOperator, rkKeyList);
        }
        return availableRoutingKey;
    }

    public static List<RoutingKey> getAllRks(List<Map<String, Object>> response) {
        List<RoutingKey> availableRoutingKey = new ArrayList<RoutingKey>();
        for (Map<String, Object> entry : response) {
            Msisdn tempMsisdn = new Msisdn((String) entry.get(OPERATOR), (String) entry.get(SHORTCODE));
            RoutingKey tempRk = new RoutingKey(tempMsisdn, (String) entry.get(KEYWORD));
            if ((String) entry.get(APP_ID) != null) {
                if (((String) entry.get(APP_ID)).equals(""))  {
                    tempRk.setApp_id("-");
                    tempRk.setStatus("AVAILABLE");
                } else {
                    tempRk.setApp_id((String) entry.get(APP_ID));
                    tempRk.setStatus("ASSIGNED");
                }
            } else {
                tempRk.setApp_id("-");
                tempRk.setStatus("AVAILABLE");
            }
            availableRoutingKey.add(tempRk);
        }
        return availableRoutingKey;
    }

    private static List<RoutingKey> getRoutingKeyList(String operator, Map<String, List<String>> rkData) {
        List<RoutingKey> rkKeyList = new ArrayList<RoutingKey>();
        if (rkData != null && !rkData.isEmpty()) {
            for (Map.Entry<String, List<String>> entry : rkData.entrySet()) {
                for (String keyword : entry.getValue()) {
                    rkKeyList.add(new RoutingKey(new Msisdn(entry.getKey(), operator),
                            keyword));
                }
            }
        }
        return rkKeyList;
    }

    public static Map<String, List<String>> getAvailableShortCodes(Message response) {
        Map<String, List<String>> shortCodeMap = new HashMap<String, List<String>>();
        Map<String, String> map = response.getParameters();
        Set entries = map.entrySet();
        for (Object entry1 : entries) {
            Map.Entry entry = (Map.Entry) entry1;
            List<String> resultMap = getList(entry.getValue().toString());

            shortCodeMap.put(entry.getKey().toString(), resultMap);

        }
        return shortCodeMap;
    }

    public void setAvailableOperators(String [] availableOperators) {
    	List<String> opList = new ArrayList<String>();
    	for (String operator : availableOperators) {
    		opList.add(operator);
		}
        Json2Java.availableOperators = opList;
    }
}