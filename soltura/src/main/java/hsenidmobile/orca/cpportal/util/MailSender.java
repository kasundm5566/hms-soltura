package hsenidmobile.orca.cpportal.util;
/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *   
 *   $LastChangedDate$
 *   $LastChangedBy$ 
 *   $LastChangedRevision$
 */

import hsenidmobile.orca.cpportal.module.ApplicationInfo;
import hsenidmobile.orca.cpportal.module.Login;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.*;
import javax.mail.internet.*;
import java.text.MessageFormat;
import java.util.*;

public class MailSender {

    private static final Logger logger = LoggerFactory.getLogger(MailSender.class);

    private String email = "soltura@9696.lk";
    private String password = "soltura@hms";
    private String host = "smtp.gmail.com";
    private int port = 465;
    private String tls = "true";
    private String authentication = "true";
    private String from;
    private String toAddresses;
    private String ccAddresses;
    private String bccAddresses;
    private String isDemoVersion;
    private String demoTemplate;
    private String forgotPasswordSubject;
    private String forgotPasswordTemplate;
    private String applicationCreationSuccessSubject;
    private String applicationCreationSuccessTemplate;
    private String requestForApprovalSubject;
    private String requestForApprovalTemplate;
    private String requestForApprovalPdUserAddress;
    private Boolean smtpDebug = Boolean.TRUE;
    private boolean socketFactoryPort = true;
    private String socketFactoryClass = "javax.net.ssl.SSLSocketFactory";
    private String socketFactoryFallback = "false";

    public void sendDemoEmail(final Login login) {

        final String mail = MessageFormat.format(demoTemplate, login.getUsername(), login.getEmail(), login.getCompany(),
                login.getCountry(), login.getContact(), login.getMessage());

        logger.debug("Mail sender class called message [{}]", mail);
        logger.debug("Mail server details : [{}]", toString());
        new Thread() {
            public void run() {
                sendMail(from, toAddresses, ccAddresses, bccAddresses, login.getSubject(), mail);
            }
        }.start();
    }

    public void sendApplicationCreationSuccessEmail(ApplicationInfo applicationInfo, final String email, String name) {

        final String mail = MessageFormat.format(applicationCreationSuccessTemplate, name, applicationInfo.getApp().getAppName());
        final String subject = MessageFormat.format(applicationCreationSuccessSubject, applicationInfo.getApp().getAppName());
        logger.debug("Mail sender class called message [{}]", mail);
        logger.debug("Mail server details : [{}]", toString());
        new Thread() {
            public void run() {
                sendMail(from, email, ccAddresses, bccAddresses, subject, mail);


            }
        }.start();

    }

    public void sendForgotPasswordEmail(final String to, String username, String newPassword) {

        final String mail = MessageFormat.format(forgotPasswordTemplate, username, newPassword);

        logger.debug("Forgot Mail sender method called message [{}]", mail);
        logger.debug("Mail server details : from: [{}] to: [{}]", new Object[]{from, to});
        new Thread() {
            public void run() {
                if (sendMail(from, to, "", "", forgotPasswordSubject, mail)) {
                    logger.debug("Message is sent successfully.");
                } else {
                    throw new RuntimeException("Error in Mail sending...");
                }


            }
        }.start();
    }

    public void sendRequestForApprovalEmail(final String cpName, final String appName, final String email) {
        final String mail = MessageFormat.format(requestForApprovalTemplate, cpName, appName);
        logger.debug("Request for approval Mail sender method called for message [{}]", mail);
        logger.debug("Mail server details : from: [{}] to: [{}]", new Object[]{email, requestForApprovalPdUserAddress});
        new Thread() {
            public void run() {
                if (sendMail(email, requestForApprovalPdUserAddress, "", "", requestForApprovalSubject, mail)) {
                    logger.debug("Message is sent successfully.");
                } else {
                    throw new RuntimeException("Error in Mail sending...");
                }


            }
        }.start();
    }

    private synchronized boolean sendMail(String frm, String to, String cc, String bcc, String subject, String text) {
        Properties props = new Properties();
        props.put("mail.smtp.user", email);
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.port", port);
        props.put("mail.smtp.starttls.enable", tls);
        props.put("mail.smtp.auth", authentication);
        props.put("mail.smtp.debug", smtpDebug);
//        props.put("mail.smtp.socketFactory.port", socketFactoryPort);
//        props.put("mail.smtp.socketFactory.class", socketFactoryClass);
//        props.put("mail.smtp.socketFactory.fallback", socketFactoryFallback);

        try {
            Session session = Session.getDefaultInstance(props, null);
            session.setDebug(smtpDebug);
            MimeMessage msg = new MimeMessage(session);
            msg.setText(text);
            msg.setSubject(subject);
            msg.setFrom(new InternetAddress(frm));

            for (String aTo : to.split(",")) {
                msg.addRecipient(Message.RecipientType.TO, new InternetAddress(aTo));
            }
            if (!cc.equals("")) {
                for (String aCc : cc.split(",")) {
                    msg.addRecipient(Message.RecipientType.CC, new InternetAddress(aCc));
                }
            }
            if (!bcc.equals("")) {
                for (String aBcc : bcc.split(",")) {
                    msg.addRecipient(Message.RecipientType.BCC, new InternetAddress(aBcc));
                }
            }
            msg.saveChanges();
            Transport transport = session.getTransport("smtp");
            transport.connect(host, port, email, password);
            transport.sendMessage(msg, msg.getAllRecipients());
            transport.close();
            return true;
        }
        catch (Exception e) {
            //TODO add snmp traps for email sending
            logger.error("Error in sending mail : ", e);
            return false;
        }
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public void setToAddresses(String toAddresses) {
        this.toAddresses = toAddresses;
    }

    public void setCcAddresses(String ccAddresses) {
        this.ccAddresses = ccAddresses;
    }

    public void setBccAddresses(String bccAddresses) {
        this.bccAddresses = bccAddresses;
    }

    public void setTls(String tls) {
        this.tls = tls;
    }

    public void setAuthentication(String authentication) {
        this.authentication = authentication;
    }

    public String getDemoVersion() {
        return isDemoVersion;
    }

    public void setDemoVersion(String demoVersion) {
        isDemoVersion = demoVersion;
    }

    public void setDemoTemplate(String demoTemplate) {
        this.demoTemplate = demoTemplate;
    }

    public void setForgotPasswordTemplate(String forgotPasswordTemplate) {
        this.forgotPasswordTemplate = forgotPasswordTemplate;
    }

    public void setForgotPasswordSubject(String forgotPasswordSubject) {
        this.forgotPasswordSubject = forgotPasswordSubject;
    }

    public void setRequestForApprovalSubject(String requestForApprovalSubject) {
        this.requestForApprovalSubject = requestForApprovalSubject;
    }

    public void setRequestForApprovalTemplate(String requestForApprovalTemplate) {
        this.requestForApprovalTemplate = requestForApprovalTemplate;
    }

    public void setRequestForApprovalPdUserAddress(String requestForApprovalPdUserAddress) {
        this.requestForApprovalPdUserAddress = requestForApprovalPdUserAddress;
    }

    public void setApplicationCreationSuccessTemplate(String applicationCreationSuccessTemplate) {
        this.applicationCreationSuccessTemplate = applicationCreationSuccessTemplate;
    }

    public void setApplicationCreationSuccessSubject(String applicationCreationSuccessSubject) {
        this.applicationCreationSuccessSubject = applicationCreationSuccessSubject;
    }

    public void setSmtpDebug(Boolean smtpDebug) {
        this.smtpDebug = smtpDebug;
    }

    public void setSocketFactoryPort(boolean socketFactoryPort) {
        this.socketFactoryPort = socketFactoryPort;
    }

    public void setSocketFactoryClass(String socketFactoryClass) {
        this.socketFactoryClass = socketFactoryClass;
    }

    public void setSocketFactoryFallback(String socketFactoryFallback) {
        this.socketFactoryFallback = socketFactoryFallback;
    }

    @Override
    public String toString() {
        return "MailSender{" +
                "email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", host='" + host + '\'' +
                ", port='" + port + '\'' +
                ", from='" + from + '\'' +
                ", toAddresses='" + toAddresses + '\'' +
                ", ccAddresses='" + ccAddresses + '\'' +
                ", bccAddresses='" + bccAddresses + '\'' +
                '}';
    }
}
