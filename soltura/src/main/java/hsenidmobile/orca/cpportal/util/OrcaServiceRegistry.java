/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hsenidmobile.orca.cpportal.util;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class OrcaServiceRegistry {

    private static String reportUrl;
    private static String provisioningUrl;
    private static String pgwAddPayment;
    
    private static String customVotingAppValue;
    private static String customSubscriptionAppValue;
    private static String customAlertAppValue;
    private static String customRequestAppValue;

    static {
        setReportUrl(null);
        setProvisioningUrl(null);
        setPgwAddPayment(null);
    }

    public static String getReportUrl() {
        return reportUrl;
    }

    private static void setReportUrl(String reportUrl) {
        OrcaServiceRegistry.reportUrl = reportUrl;
    }

    public static String getProvisioningUrl() {
        return provisioningUrl;
    }

    private static void setProvisioningUrl(String provisioningUrl) {
        OrcaServiceRegistry.provisioningUrl = provisioningUrl;
    }

    public static String getPgwAddPayment() {
        return pgwAddPayment;
    }

    private static void setPgwAddPayment(String pgwAddPayment) {
        OrcaServiceRegistry.pgwAddPayment = pgwAddPayment;
    }

    public static String getCustomVotingAppValue() {
        return customVotingAppValue;
    }

    private static void setCustomVotingAppValue(String customVotingAppValue) {
        OrcaServiceRegistry.customVotingAppValue = customVotingAppValue;
    }

    public static String getCustomSubscriptionAppValue() {
        return customSubscriptionAppValue;
    }

    private static void setCustomSubscriptionAppValue(String customSubscriptionAppValue) {
        OrcaServiceRegistry.customSubscriptionAppValue = customSubscriptionAppValue;
    }

    public static String getCustomAlertAppValue() {
        return customAlertAppValue;
    }

    private static void setCustomAlertAppValue(String customAlertAppValue) {
        OrcaServiceRegistry.customAlertAppValue = customAlertAppValue;
    }

    public static String getCustomRequestAppValue() {
        return customRequestAppValue;
    }

    private static void setCustomRequestAppValue(String customRequestAppValue) {
        OrcaServiceRegistry.customRequestAppValue = customRequestAppValue;
    }
}
