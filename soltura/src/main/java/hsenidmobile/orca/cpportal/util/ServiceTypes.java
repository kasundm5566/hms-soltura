/*
 *   (C) Copyright 2005-2007 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hsenidmobile.orca.cpportal.util;

import static hsenidmobile.orca.cpportal.util.OrcaServiceRegistry.*;

/**
 * Date: Feb 4, 2010
 * Time: 12:57:19 PM
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */

public enum ServiceTypes {

    VOTING(getCustomVotingAppValue()),
    SUBSCRIPTION(getCustomSubscriptionAppValue()),
    ALERT(getCustomAlertAppValue()),
    REQUEST(getCustomRequestAppValue());

    private String value;
    
    ServiceTypes(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
