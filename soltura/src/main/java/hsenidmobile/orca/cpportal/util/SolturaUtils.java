/* 
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited. 
 *   All Rights Reserved. 
 * 
 *   These materials are unpublished, proprietary, confidential source code of 
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET 
 *   of hSenid Software International (Pvt) Limited. 
 * 
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual 
 *   property rights in these materials. 
 *
 */
package hsenidmobile.orca.cpportal.util;

import hsenidmobile.orca.cpportal.filter.CustomFilter;

import java.util.Collection;
import java.util.Iterator;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */

public class SolturaUtils {
    public static final String SMSC_PD_NCS_ID = "smsc-pd";
    public static final String OPERATOR_NAME = "Telkomsel";

    //Defines key values used when setting the  additional information for the content provider
    public static final String FIRST_NAME = "FIRST_NAME";
    public static final String LAST_NAME = "LAST_NAME";
    public static final String BIRTH_DAY = "BIRTH_DAY";
    public static final String GENDER = "GENDER";
    public static final String COUNTRY = "COUNTRY";
    public static final String LOCATION = "LOCATION";
    public static final String BUSINESS_NUMBER = "BUSINESS_NUMBER";
    public static final String EMAIL = "EMAIL";

    public static final String FEATURE_LIST_SESSION_KEY = "fl";
    public static final String FEATURE_KEY_ENABLE_AUDIT_LOGS = "ENABLE_AUDIT_LOGS";

    public static enum reportTypes {
        CSV,
        EXCEL,
        PDF
    }

    public static <T> void filter(Collection<T> l, CustomFilter<T> filter) {
        Iterator<T> it= l.iterator();
        while(it.hasNext()) {
            if(filter.match(it.next())) {
                it.remove();
            }
        }
    }
}
