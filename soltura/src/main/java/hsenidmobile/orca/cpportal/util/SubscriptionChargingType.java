/* 
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited. 
 *   All Rights Reserved. 
 * 
 *   These materials are unpublished, proprietary, confidential source code of 
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET 
 *   of hSenid Software International (Pvt) Limited. 
 * 
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual 
 *   property rights in these materials. 
 *
 */
package hsenidmobile.orca.cpportal.util;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public enum SubscriptionChargingType {
    
    ONE_TIME_SUBS_CHARGING("adv.setting.subscription.one.time"),
    MONTHLY_SUBS_CHARGING("adv.setting.subscription.monthly"),
    WEEKLY_SUBS_CHARGING("adv.setting.subscription.weekly"),
    DAILY_SUBS_CHARGING("adv.setting.subscription.daily"),
    FREE_SUBS_CHARGING;
    private String description;

    public String getDescription() { return description;}
    SubscriptionChargingType(String messageKey) {
        this.description = messageKey;
    }

    SubscriptionChargingType() {
    }
}
