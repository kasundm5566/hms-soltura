/*
 *   (C) Copyright 2008-2012 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate: $
 *   $LastChangedBy: $
 *   $LastChangedRevision: $
 *
 */
package hsenidmobile.orca.cpportal.util;

import hsenidmobile.orca.core.model.ApplicationChargingInfo.FromSubscriberChargingModel;

import java.util.ArrayList;
import java.util.List;

/**
 * $LastChangedDate: $ $LastChangedBy: $ $LastChangedRevision: $
 *
 */
public class SubscriptionChargingTypeConfiguration {

	private SubscriptionChargingType[] supportedSubscriptionChargingTypes;
	private FromSubscriberChargingModel fromSubscriberChargingModel;
	private String frmSubsctiberDefaultChargingType;

	public FromSubscriberChargingModel getFromSubscriberChargingModel() {
		return fromSubscriberChargingModel;
	}

	public void setFromSubscriberChargingModelBool(boolean onlyOneChargingTypeAllowedFromSubscriber) {
		if(onlyOneChargingTypeAllowedFromSubscriber){
			this.fromSubscriberChargingModel = FromSubscriberChargingModel.ONLY_ONE_CHG_TYPE_ALLOWED;
		} else {
			this.fromSubscriberChargingModel = FromSubscriberChargingModel.MULTIPLE_CHG_TYPE_ALLOWED;
		}
	}

	public void setSupportedSubsChargingTypesStr(String[] supportedSubscriptionChargingTypes) {
		SubscriptionChargingType[] enums = SubscriptionChargingType.values();
		List<SubscriptionChargingType> chargingTypeList = new ArrayList<SubscriptionChargingType>();
		for (String ct : supportedSubscriptionChargingTypes) {
			chargingTypeList.add(findMatching(enums, ct));
		}
		this.supportedSubscriptionChargingTypes = chargingTypeList
				.toArray(new SubscriptionChargingType[chargingTypeList.size()]);
	}

	private SubscriptionChargingType findMatching(SubscriptionChargingType[] enums, String str) {
		for (SubscriptionChargingType ev : enums) {
			if (ev.name().equals(str)) {
				return ev;
			}
		}

		throw new RuntimeException("No matching SubscriptionChargingType found for String[" + str + "]");
	}

	public SubscriptionChargingType[] getSupportedSubscriptionChargingTypes() {
		return supportedSubscriptionChargingTypes;
	}

	public String getFrmSubsctiberDefaultChargingType() {
		return frmSubsctiberDefaultChargingType;
	}

	public void setFrmSubsctiberDefaultChargingType(String frmSubsctiberDefaultChargingType) {
		this.frmSubsctiberDefaultChargingType = frmSubsctiberDefaultChargingType;
	}

}
