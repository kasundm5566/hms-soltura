/*   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 **/
package hsenidmobile.orca.cpportal.util.client;

import hsenidmobile.nettyclient.channelpool.BootstrapFactory;
import hsenidmobile.nettyclient.channelpool.ChannelEventListener;
import org.jboss.netty.bootstrap.ClientBootstrap;
import org.jboss.netty.channel.socket.nio.NioClientSocketChannelFactory;

import java.net.URI;
import java.util.concurrent.Executors;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class BootstrapFactoryImpl implements BootstrapFactory {

	@Override
	public ClientBootstrap createNewBootstrap(ChannelEventListener channelEventListener, URI attachedHost) {
		ClientBootstrap clientBootstrap = new ClientBootstrap(new NioClientSocketChannelFactory(
                Executors.newCachedThreadPool(), Executors.newCachedThreadPool()));
		clientBootstrap.setPipelineFactory(new HttpClientPipelineFactory(channelEventListener, attachedHost));
		return clientBootstrap;
	}
}
