/*   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 **/
package hsenidmobile.orca.cpportal.util.client;

import hms.commons.SnmpLogUtil;
import hsenidmobile.nettyclient.MessageResponseFuture;
import hsenidmobile.nettyclient.MessageSender;
import hsenidmobile.nettyclient.MessageSendingFailedException;
import hsenidmobile.orca.core.model.message.AppDataUpdateRequest;
import hsenidmobile.orca.core.model.message.RequestShowReplyRequest;
import hsenidmobile.orca.core.model.message.RoutingKeyRegisterRequest;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.handler.codec.http.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.TimeoutException;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class HttpRequestSender {

    private static final Logger logger = LoggerFactory.getLogger(HttpRequestSender.class);

    private MessageSender messageSender;
    private URI destination;
    private URI replyDestination;
    private static final String ncsKey = "ncs";
    private static final String shortcodeKey = "shortcode";
    private static final String keywordKey = "keyword";
    private static final String spIdkey = "spId";
    private static final String CORRELATION_ID_HEADER = "correlation_id";
    private final String BREAKER = "&";
    private final String EQUAL = "=";
    private String expireDateKey = "expireDate";
    private final String performRkCreationChargingKey = "performCreationCharging";
    private final String chargingCycleSizeKey = "chargingCycleSize";
    private String xml = "xml";
    private String apiUserName;
    private String apiPassword;
    private int responseTimeout = 5000;
    private String defaultChargingCycleSize = "30";
    private String orcaConnectionDownTrap;
    private String orcaConnectionUpTrap;


    public void setMessageSender(MessageSender messageSender) {
        this.messageSender = messageSender;
    }

    public void setDestination(String destination) {
        try {
            this.destination = new URI(destination);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public void setApiUserName(String apiUserName) {
        this.apiUserName = apiUserName;
    }

    public void setApiPassword(String apiPassword) {
        this.apiPassword = apiPassword;
    }

    public boolean sendMessage(AppDataUpdateRequest appDataUpdateRequest) throws IOException, MessageSendingFailedException,
            InterruptedException, TimeoutException {

        try {
            logger.info("Sending AppDataUpdateRequest[{}] to core[{}]", new Object[]{appDataUpdateRequest, destination});

            HttpRequest request = new DefaultHttpRequest(HttpVersion.HTTP_1_1, HttpMethod.POST, destination.toASCIIString());
            request.setHeader(HttpHeaders.Names.HOST, destination.getHost());
            request.setHeader(HttpHeaders.Names.CONNECTION, HttpHeaders.Values.KEEP_ALIVE);

            request.setHeader(HttpHeaders.Names.CONTENT_TYPE, "application/octet-stream");
            request.setContent(setSerializeData(appDataUpdateRequest));
            request.setHeader(HttpHeaders.Names.CONTENT_LENGTH, String.valueOf(request.getContent().capacity()));

            MessageResponseFuture responseFuture = messageSender.sendMessage(destination, request);
            responseFuture.waitTillFinish(responseTimeout);
            if (responseFuture.isSuccess()) {
                SnmpLogUtil.clearTrap("orca-app-data-update-connection", orcaConnectionUpTrap);
                return true;
            } else {
                return false;
            }
        } catch (IOException e) {
            SnmpLogUtil.trap("orca-app-data-update-connection", orcaConnectionDownTrap);
            throw e;
        } catch (MessageSendingFailedException e) {
            SnmpLogUtil.trap("orca-app-data-update-connection", orcaConnectionDownTrap);
            throw e;
        } catch (InterruptedException e) {
            SnmpLogUtil.trap("orca-app-data-update-connection", orcaConnectionDownTrap);
            throw e;
        } catch (TimeoutException e) {
            SnmpLogUtil.trap("orca-app-data-update-connection", orcaConnectionDownTrap);
            throw e;
        }
    }

    public void sendMessage(RequestShowReplyRequest requestShowReplyRequest) throws IOException, MessageSendingFailedException,
            InterruptedException, TimeoutException {

        try {
            logger.info("Sending requestShowReplyRequest[{}] to core[{}]", new Object[]{requestShowReplyRequest, replyDestination});

            HttpRequest request = new DefaultHttpRequest(HttpVersion.HTTP_1_1, HttpMethod.POST, replyDestination.toASCIIString());
            request.setHeader(HttpHeaders.Names.HOST, destination.getHost());
            request.setHeader(HttpHeaders.Names.CONNECTION, HttpHeaders.Values.KEEP_ALIVE);
            request.setHeader(HttpHeaders.Names.CONTENT_TYPE, "application/octet-stream");
            request.setContent(serializeReplyData(requestShowReplyRequest));
            request.setHeader(HttpHeaders.Names.CONTENT_LENGTH, String.valueOf(request.getContent().capacity()));

            MessageResponseFuture responseFuture = messageSender.sendMessage(replyDestination, request);
            responseFuture.waitTillFinish(responseTimeout);
            if (responseFuture.isSuccess()) {
                logger.debug("Reply [{}] sent for the recipient [{}]",requestShowReplyRequest.getMessageContent(),
                                                                  requestShowReplyRequest.getRecepient());
                SnmpLogUtil.clearTrap("soltura-orca-request-show-reply-connection", orcaConnectionUpTrap);
            } else {
                logger.error("Message [{}] sending fail to the recipient [{}]",requestShowReplyRequest.getMessageContent(),
                                                                  requestShowReplyRequest.getRecepient());
                SnmpLogUtil.trap("soltura-orca-request-show-reply-connection", orcaConnectionDownTrap);
            }
        } catch (IOException e) {
            SnmpLogUtil.trap("soltura-orca-request-show-reply-connection", orcaConnectionDownTrap);
            throw e;
        } catch (MessageSendingFailedException e) {
            SnmpLogUtil.trap("soltura-orca-request-show-reply-connection", orcaConnectionDownTrap);
            throw e;
        } catch (InterruptedException e) {
            SnmpLogUtil.trap("soltura-orca-request-show-reply-connection", orcaConnectionDownTrap);
            throw e;
        } catch (TimeoutException e) {
            SnmpLogUtil.trap("soltura-orca-request-show-reply-connection", orcaConnectionDownTrap);
            throw e;
        }
    }

    public boolean sendMessage(RoutingKeyRegisterRequest routingKeyRegisterRequest) throws Exception {

		logger.info("Sending RoutingKeyRegisterRequest[{}] to SDP[{}]", new Object[] { routingKeyRegisterRequest,
				destination });

        final Object response;
        try {
            HttpRequest request = new DefaultHttpRequest(HttpVersion.HTTP_1_1, HttpMethod.POST, destination.toASCIIString());
            request.setHeader(HttpHeaders.Names.HOST, destination.getHost());
            request.setHeader(HttpHeaders.Names.CONNECTION, HttpHeaders.Values.KEEP_ALIVE);
            request.setHeader(HttpHeaders.Names.CONTENT_TYPE, "application/octet-stream");
            request.setHeader(CORRELATION_ID_HEADER, String.valueOf(routingKeyRegisterRequest.getCorrelationId()));
            final byte[] bytes = setData(routingKeyRegisterRequest, request);
            request.addHeader(HttpHeaders.Names.CONTENT_LENGTH, String.valueOf(bytes.length));
            MessageResponseFuture responseFuture = messageSender.sendMessage(destination, request);
            responseFuture.waitTillFinish(responseTimeout);
            response = responseFuture.getResponse();
        } catch (IOException e) {
            SnmpLogUtil.trap("soltura-orca-routing-key-connection", orcaConnectionDownTrap);
            throw e;
        } catch (MessageSendingFailedException e) {
            SnmpLogUtil.trap("soltura-orca-routing-key-connection", orcaConnectionDownTrap);
            throw e;
        } catch (InterruptedException e) {
            SnmpLogUtil.trap("soltura-orca-routing-key-connection", orcaConnectionDownTrap);
            throw e;
        } catch (TimeoutException e) {
            SnmpLogUtil.trap("soltura-orca-routing-key-connection", orcaConnectionDownTrap);
            throw e;
        }
        if (response == null) {
            SnmpLogUtil.trap("soltura-orca-routing-key-connection", orcaConnectionDownTrap);
			throw new Exception("No Response from SDP.");
		} else {
			HttpServerResponse serverResponse = (HttpServerResponse) response;
			if (serverResponse.isSuccess()) {
				logger.info("RoutingKey Successfully created by the SDP.");
                SnmpLogUtil.clearTrap("soltura-orca-routing-key-connection", orcaConnectionDownTrap);
				return true;
			} else {
                SnmpLogUtil.trap("soltura-orca-routing-key-connection", orcaConnectionDownTrap);
				throw new Exception("Error response from SDP[" + serverResponse.getRespMsg() + "].");
			}
		}
	}

    private ChannelBuffer setSerializeData(AppDataUpdateRequest appDataUpdateRequest) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(appDataUpdateRequest);

        ChannelBuffer buf = ChannelBuffers.copiedBuffer(byteArrayOutputStream.toByteArray());

        objectOutputStream.close();
        byteArrayOutputStream.close();

        return buf;
    }

    private ChannelBuffer serializeReplyData(RequestShowReplyRequest requestShowReplyRequest) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(requestShowReplyRequest);

        ChannelBuffer buf = ChannelBuffers.copiedBuffer(byteArrayOutputStream.toByteArray());

        objectOutputStream.close();
        byteArrayOutputStream.close();

        return buf;
    }

    private byte[] setData(RoutingKeyRegisterRequest routingKeyRegisterRequest, HttpRequest httpRequest) throws IOException {
        logger.debug("setting up data to be sent in order to register the routing key. [{}]", routingKeyRegisterRequest);
        StringBuilder stringBuilder = new StringBuilder("?");
        stringBuilder.append(ncsKey + EQUAL).append(routingKeyRegisterRequest.getNcsType())
                .append(BREAKER + shortcodeKey + EQUAL).append(routingKeyRegisterRequest.getShortCode())
                .append(BREAKER + keywordKey + EQUAL).append(routingKeyRegisterRequest.getKeyword())
                .append(BREAKER + spIdkey + EQUAL).append(routingKeyRegisterRequest.getSpId())
                .append(BREAKER + expireDateKey + EQUAL).append(routingKeyRegisterRequest.getExpireDate())
        		.append(BREAKER + performRkCreationChargingKey + EQUAL).append(routingKeyRegisterRequest.isPerformRkCreationCharging())
        		.append(BREAKER + chargingCycleSizeKey + EQUAL).append(getChargingCycleSize(routingKeyRegisterRequest));
        logger.debug("Routing Key Register Request [{}]", stringBuilder.toString());
        final byte[] bytes = stringBuilder.toString().getBytes();
        httpRequest.setContent(ChannelBuffers.wrappedBuffer(bytes));
        return bytes;
    }

    private String getChargingCycleSize(RoutingKeyRegisterRequest routingKeyRegisterRequest){
    	String cycleSize = routingKeyRegisterRequest.getChargingCycleSize();
    	if(cycleSize == null || cycleSize.isEmpty()){
    		cycleSize = defaultChargingCycleSize;
    		logger.info("Using default charging cycle size [{}]days since it is not provided.", defaultChargingCycleSize);
    	}

    	return cycleSize;
    }

	public void setResponseTimeout(int responseTimeout) {
		this.responseTimeout = responseTimeout;
	}

	public void setDefaultChargingCycleSize(String defaultChargingCycleSize) {
		this.defaultChargingCycleSize = defaultChargingCycleSize;
	}

    public void setReplyDestination(URI replyDestination) {
        this.replyDestination = replyDestination;
    }

    public void setOrcaConnectionDownTrap(String orcaConnectionDownTrap) {
        this.orcaConnectionDownTrap = orcaConnectionDownTrap;
    }

    public void setOrcaConnectionUpTrap(String orcaConnectionUpTrap) {
        this.orcaConnectionUpTrap = orcaConnectionUpTrap;
    }
}
