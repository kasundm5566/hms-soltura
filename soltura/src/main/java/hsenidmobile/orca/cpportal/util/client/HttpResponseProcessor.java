/*   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 **/
package hsenidmobile.orca.cpportal.util.client;

import hsenidmobile.nettyclient.channelpool.ResponseProcessor;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.handler.codec.http.HttpResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class HttpResponseProcessor implements ResponseProcessor {

	private static final Logger logger = LoggerFactory.getLogger(HttpResponseProcessor.class);
	private static final int HTTP_OK = 200;

	@Override
	public Object processReceivedMessage(ChannelHandlerContext ctx, MessageEvent e) {
		HttpResponse response = (HttpResponse) e.getMessage();
		if (response.getStatus().getCode() == HTTP_OK) {
			return new HttpServerResponse(getResponseContent(response), true);
		} else {
			logger.error("Http Error code[{}] received from Server.", response.getStatus().getCode());
			return new HttpServerResponse(getResponseContent(response), false);
		}

	}

	private String getResponseContent(HttpResponse response) {
		ChannelBuffer content = response.getContent();
		String xmlResponse = "";
		if (content.readable()) {
			xmlResponse = content.toString("UTF-8");
			logger.debug("Received Response [{}]", xmlResponse);
		} else {
			logger.warn("Received response is not readable.");
		}
		return xmlResponse;
	}

}
