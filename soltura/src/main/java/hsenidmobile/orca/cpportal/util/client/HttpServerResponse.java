/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hsenidmobile.orca.cpportal.util.client;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
public class HttpServerResponse {

	private String respMsg;
	private boolean isSuccess;

	public HttpServerResponse(String respMsg, boolean isSuccess) {
		super();
		this.respMsg = respMsg;
		this.isSuccess = isSuccess;
	}

	public String getRespMsg() {
		return respMsg;
	}

	public boolean isSuccess() {
		return isSuccess;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(100);
		builder.append("HttpResponse [respMsg=");
		builder.append(respMsg);
		builder.append(", isSuccess=");
		builder.append(isSuccess);
		builder.append("]");
		return builder.toString();
	}

}
