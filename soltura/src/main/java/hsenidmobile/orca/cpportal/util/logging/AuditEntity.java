package hsenidmobile.orca.cpportal.util.logging;

import eu.bitwalker.useragentutils.UserAgent;
import hsenidmobile.orca.core.model.ContentProvider;
import hsenidmobile.orca.core.model.Msisdn;
import hsenidmobile.orca.cpportal.util.SolturaUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.HttpHeaders;
import java.util.Map;

public class AuditEntity {

    /*
*
    Username	        User who performed the activity
    MSISDN
    IP
    Browser type
    Browser version
    Session ID
    Module
    Action	            Activity which was performed by the mentioned user
    Action description
    TimeStamp	        Timestamp value
    Status	Success or Failed (Whether the activity performed successfully or not)


    john123|8801800000002|127.0.0.1|Firefox 32|32|3D08D1BA8F09F645B0E4BDDDA6B9B790|SOLTURA|Access Soltura Home|Accessing Soltura Home Dashboard|2014-10-26 14:25:58,602|Success
*
* */

    private static final DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss,SSS");

    private final String username;
    private final String msisdn;
    private final String ip;
    private final String browserType;
    private final String browserVersion;
    private final String sessionId;
    private final String module = "SOLTURA";
    private final String action;
    private final String actionDescription;
    private final String timeStamp;
    private final String status;
    private final boolean isAuditable;

    private AuditEntity(String username,
                        String msisdn,
                        String ip,
                        String browserType,
                        String browserVersion,
                        String sessionId,
                        String action,
                        String actionDescription,
                        String timeStamp,
                        String status,
                        boolean isAuditable) {
        this.username = username;
        this.msisdn = msisdn;
        this.ip = ip;
        this.browserType = browserType;
        this.browserVersion = browserVersion;
        this.sessionId = sessionId;
        this.action = action;
        this.actionDescription = actionDescription;
        this.timeStamp = timeStamp;
        this.status = status;
        this.isAuditable = isAuditable;
    }

    public String getUsername() {
        return username;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public String getIp() {
        return ip;
    }

    public String getBrowserType() {
        return browserType;
    }

    public String getBrowserVersion() {
        return browserVersion;
    }

    public String getSessionId() {
        return sessionId;
    }

    public String getModule() {
        return module;
    }

    public String getAction() {
        return action;
    }

    public String getActionDescription() {
        return actionDescription;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public String getStatus() {
        return status;
    }

    public boolean isAuditable() { return isAuditable; }

    public static class AuditEntityBuilder {
        private String username;
        private String msisdn;
        //
        private String ip;
        private String browserType;
        private String browserVersion;
        private String sessionId;
        //
        private String action;
        private String actionDescription;
        private String status;
        private boolean isAuditable;


        public AuditEntityBuilder(ContentProvider cp) {
            if(cp != null) {
                this.username = cp.getName();
                if(cp.getMsisdn() != null)
                    this.msisdn = cp.getMsisdn().getAddress();
            }
        }

        public AuditEntityBuilder populateFromHttpRequest(HttpServletRequest request) {
            try {
                this.ip = request.getRemoteAddr();
                UserAgent userAgent = UserAgent.parseUserAgentString(request.getHeader(HttpHeaders.USER_AGENT));
                browserType = userAgent.getBrowser().getName();
                browserVersion = userAgent.getBrowserVersion().getMajorVersion();
                sessionId = request.getSession().getId();

                Object featureList = request.getSession().getAttribute(SolturaUtils.FEATURE_LIST_SESSION_KEY);
                if(featureList != null) {
                    Map<String, Boolean> featureMap = (Map<String, Boolean>) featureList;
                    if(featureMap.containsKey(SolturaUtils.FEATURE_KEY_ENABLE_AUDIT_LOGS)) {
                        isAuditable = featureMap.get(SolturaUtils.FEATURE_KEY_ENABLE_AUDIT_LOGS);
                    }
                }

            } catch (Exception e) {
            }

            finally {
                return this;
            }
        }

        public AuditEntityBuilder status(ActionStatus actionStatus) {
            this.status = actionStatus.getStatus();
            return this;
        }

        public AuditEntityBuilder action(String action) {
            this.action = action;
            return this;
        }

        public AuditEntityBuilder description(String actionDescription) {
            this.actionDescription = actionDescription;
            return this;
        }

        public AuditEntityBuilder username(String username) {
            this.username = username;
            return this;
        }

        public AuditEntityBuilder msisdn(String msisdn) {
            this.msisdn = msisdn;
            return this;
        }

        public AuditEntity build() {
            return new AuditEntity(
                    username,
                    msisdn,
                    ip,
                    browserType,
                    browserVersion,
                    sessionId,
                    action,
                    actionDescription,
                    new DateTime().toString(fmt),
                    status,
                    isAuditable);
        }

    }

    public static enum ActionStatus {
        SUCCESS("Success"),
        FAILED("Failed");

        private final String status;

        ActionStatus(String status) {
            this.status = status;
        }

        public String getStatus() {
            return status;
        }
    }
}
