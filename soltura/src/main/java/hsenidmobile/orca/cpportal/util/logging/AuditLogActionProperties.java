package hsenidmobile.orca.cpportal.util.logging;

public class AuditLogActionProperties {

    private String homeAccessAction;
    private String homeAccessActionDesc;
    private String createKeywordAction;
    private String createKeywordActionDesc;
    private String createAppAccessAction;
    private String createAppAccessActionDesc;
    private String createAppCompleteAction;
    private String createAppCompleteActionDesc;
    private String viewAppAction;
    private String viewAppActionDesc;
    private String myAppsAction;
    private String myAppsActionDesc;
    private String searchAppsAction;
    private String searchAppsActionDesc;
    private String settingsAccessAction;
    private String settingsAccessActionDesc;
    private String myKeywordsAccessAction;
    private String myKeywordsAccessActionDesc;
    private String viewMsgHistoryAction;
    private String viewMsgHistoryActionDesc;
    private String updateAppContentAction;
    private String updateAppContentActionDesc;

    public String getHomeAccessAction() { return homeAccessAction; }
    public void setHomeAccessAction(String homeAccessAction) { this.homeAccessAction = homeAccessAction; }
    public String getHomeAccessActionDesc() { return homeAccessActionDesc; }
    public void setHomeAccessActionDesc(String homeAccessActionDesc) { this.homeAccessActionDesc = homeAccessActionDesc; }

    public String getCreateKeywordAction() { return createKeywordAction; }
    public void setCreateKeywordAction(String createKeywordAction) { this.createKeywordAction = createKeywordAction; }
    public String getCreateKeywordActionDesc() { return createKeywordActionDesc; }
    public void setCreateKeywordActionDesc(String createKeywordActionDesc) { this.createKeywordActionDesc = createKeywordActionDesc; }

    public String getCreateAppAccessAction() { return createAppAccessAction; }
    public void setCreateAppAccessAction(String createAppAccessAction) { this.createAppAccessAction = createAppAccessAction; }
    public String getCreateAppAccessActionDesc() { return createAppAccessActionDesc; }
    public void setCreateAppAccessActionDesc(String createAppAccessActionDesc) { this.createAppAccessActionDesc = createAppAccessActionDesc; }

    public String getCreateAppCompleteAction() { return createAppCompleteAction; }
    public void setCreateAppCompleteAction(String createAppCompleteAction) { this.createAppCompleteAction = createAppCompleteAction; }
    public String getCreateAppCompleteActionDesc() { return createAppCompleteActionDesc; }
    public void setCreateAppCompleteActionDesc(String createAppCompleteActionDesc) { this.createAppCompleteActionDesc = createAppCompleteActionDesc; }

    public String getViewAppAction() { return viewAppAction; }
    public void setViewAppAction(String viewAppAction) { this.viewAppAction = viewAppAction; }
    public String getViewAppActionDesc() { return viewAppActionDesc; }
    public void setViewAppActionDesc(String viewAppActionDesc) { this.viewAppActionDesc = viewAppActionDesc; }

    public String getMyAppsAction() { return myAppsAction; }
    public void setMyAppsAction(String myAppsAction) { this.myAppsAction = myAppsAction; }
    public String getMyAppsActionDesc() { return myAppsActionDesc; }
    public void setMyAppsActionDesc(String myAppsActionDesc) { this.myAppsActionDesc = myAppsActionDesc; }

    public String getSearchAppsAction() { return searchAppsAction; }
    public void setSearchAppsAction(String searchAppsAction) { this.searchAppsAction = searchAppsAction; }
    public String getSearchAppsActionDesc() { return searchAppsActionDesc; }
    public void setSearchAppsActionDesc(String searchAppsActionDesc) { this.searchAppsActionDesc = searchAppsActionDesc; }

    public String getSettingsAccessAction() { return settingsAccessAction; }
    public void setSettingsAccessAction(String settingsAccessAction) { this.settingsAccessAction = settingsAccessAction; }
    public String getSettingsAccessActionDesc() { return settingsAccessActionDesc; }
    public void setSettingsAccessActionDesc(String settingsAccessActionDesc) { this.settingsAccessActionDesc = settingsAccessActionDesc; }

    public String getMyKeywordsAccessAction() { return myKeywordsAccessAction; }
    public void setMyKeywordsAccessAction(String myKeywordsAccessAction) { this.myKeywordsAccessAction = myKeywordsAccessAction; }
    public String getMyKeywordsAccessActionDesc() { return myKeywordsAccessActionDesc; }
    public void setMyKeywordsAccessActionDesc(String myKeywordsAccessActionDesc) { this.myKeywordsAccessActionDesc = myKeywordsAccessActionDesc; }

    public String getViewMsgHistoryAction() { return viewMsgHistoryAction; }
    public void setViewMsgHistoryAction(String viewMsgHistoryAction) { this.viewMsgHistoryAction = viewMsgHistoryAction; }
    public String getViewMsgHistoryActionDesc() { return viewMsgHistoryActionDesc; }
    public void setViewMsgHistoryActionDesc(String viewMsgHistoryActionDesc) { this.viewMsgHistoryActionDesc = viewMsgHistoryActionDesc; }

    public String getUpdateAppContentAction() { return updateAppContentAction; }
    public void setUpdateAppContentAction(String updateAppContentAction) { this.updateAppContentAction = updateAppContentAction; }
    public String getUpdateAppContentActionDesc() { return updateAppContentActionDesc; }
    public void setUpdateAppContentActionDesc(String updateAppContentActionDesc) { this.updateAppContentActionDesc = updateAppContentActionDesc; }
}
