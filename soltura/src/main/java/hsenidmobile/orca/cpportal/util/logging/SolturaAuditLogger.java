package hsenidmobile.orca.cpportal.util.logging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class SolturaAuditLogger {

    public static final String logTemplate = "{}|{}|{}|{}|{}|{}|{}|{}|{}|{}|{}";

    private static final Logger logger = LoggerFactory.getLogger("audit.log");

    public static final void audit(AuditEntity auditEntity) {
        if(auditEntity.isAuditable()) {
            logger.info(logTemplate, valueOrEmpty(auditEntity.getUsername()),
                    valueOrEmpty(auditEntity.getMsisdn()),
                    valueOrEmpty(auditEntity.getIp()),
                    valueOrEmpty(auditEntity.getBrowserType()),
                    valueOrEmpty(auditEntity.getBrowserVersion()),
                    valueOrEmpty(auditEntity.getSessionId()),
                    valueOrEmpty(auditEntity.getModule()),
                    valueOrEmpty(auditEntity.getAction()),
                    valueOrEmpty(auditEntity.getActionDescription()),
                    valueOrEmpty(auditEntity.getTimeStamp()),
                    valueOrEmpty(auditEntity.getStatus()));
        }
    }

    // Return value if not null, otherwise return empty string.
    private static String valueOrEmpty(String value) {
        return value != null ? value : "";
    }

}