package hsenidmobile.orca.cpportal.validator;

import hsenidmobile.orca.core.applications.ApplicationImpl;
import hsenidmobile.orca.core.services.impl.RoutingKeyManagerImpl;
import hsenidmobile.orca.cpportal.module.ApplicationInfo;
import hsenidmobile.orca.cpportal.module.KeywordDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import hsenidmobile.orca.cpportal.service.mock.SdpRoutingKeyRepository;


/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 */


public abstract class AbstractValidator {

    private static final Logger Logger = LoggerFactory.getLogger(AbstractValidator.class);
    public final static SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

    protected long APP_NAME_MIN_LENGTH;
    protected long APP_NAME_MAX_LENGTH;
    protected long SUBCATEGORY_MIN_LENGTH;
    protected long SUBCATEGORY_DESC_MIN_LENGTH;
    protected long SUBCATEGORY_MAX_LENGTH;
    protected long SUBCATEGORY_DESC_MAX_LENGTH;
    protected String MESSAGE_LENGTH;
    protected String DESCRIPTION_LENGTH;
    protected int TELEPHONE_LENGTH;
    protected boolean keywordExpirationValidationRequired;

    /*
                [-+]?: Can have an optional - or + sign at the beginning.
                [0-9]*: Can have any numbers of digits between 0 and 9
                \\.? : the digits may have an optional decimal point.
     */
    protected String TEL_EXPRESSION;
    protected String KEYWORD_EXPRESSION;
    protected String SUB_CATEGORY_EXPRESSION;
    protected String PASSWORD_EXPRESSION;
    protected String USERANME_LENGTH;
    protected String PASSWORD_LENGTH;
    protected String PASSWORD_MIN_LENGTH;
    protected String CP_NAME_LENGTH;
    protected String BUSINESS_NUMBER_LENGTH;
    protected String CP_TEL_LENGTH;
    protected String CP_EMAIL_LENGTH;
    protected String CHARGING_MSISDN;
    protected String REG_CODE_LENGTH;
    protected String BILLING_NAME_MAX_LENGTH;
    protected String BILLING_ADDRESS_MAX_LENGTH;
    protected String APP_NAME_EXPRESSION;
    protected String MSISDN_EXPRESSION;
    protected String MAP_VALUE_MAX_LENGTH;
    protected String BIRTH_DATE_PATTERN;
    protected String REQUEST_APP_RESPONSE_MSG_LENGTH;
    protected String MSISDN_PREFIX;
    protected String MSISDN_PREFIX_REPLACEMENT;


    @Autowired
    private RoutingKeyManagerImpl routingKeyServiceImpl;
    protected boolean disblePrefixReplacement = true;

    protected void validateApplicationGeneralData(ApplicationImpl application, Errors errors, BindingResult bindingResult) {
        if (parameterBlank(application.getAppName())) {
            errors.reject("appName", "app.name.required");
        }

    }

    protected boolean parameterInvalid(Object object) {
        return (object == null);
    }

    protected boolean parameterBlank(String object) {
        return parameterInvalid(object) || object.trim().equals("");
    }

    protected boolean isSubKeywordHasSpaces(ApplicationInfo appInfo) {
        boolean hasSpaces = false;
        List<KeywordDetails> keywordList = appInfo.getSelectedValues();
        for (KeywordDetails keywordDetails : keywordList) {
            if (keywordDetails.getKeyword().contains(" ")) {
                keywordList.remove(keywordDetails.getKeyword());
                hasSpaces = true;
            }
        }
        return hasSpaces;
    }

//    protected Map<String, Boolean> isSubKeywordHasReservedKeyword(List<KeywordDetails> keywordList) {
//        Map<String, Boolean> map = new HashMap<String, Boolean>();
//        for (KeywordDetails keywordDetails : keywordList) {
//            if (routingKeyServiceImpl.isReservedKeyword(keywordDetails.getKeyword(), OPERATOR_NAME)) {
//                keywordList.remove(keywordDetails.getKeyword());
//                map.put(keywordDetails.getKeyword(), true);
//            }
//        }
//        return map;
//    }

    public void validateForReservedKeyword(boolean subCatRequired, List<KeywordDetails> keywordDetailsList,
                                           BindingResult bindingResults) {
        //todo integrate this with sdp rk validate api 
//        if (subCatRequired) {
//            Map<String, Boolean> map = isSubKeywordHasReservedKeyword(keywordDetailsList);
//            for (Map.Entry<String, Boolean> me : map.entrySet()) {
//                if (me.getValue()) {
//                    bindingResults.rejectValue("selectedValues", "subkeyword.is.not.owned.by.cp",
//                            new Object[]{me.getKey()}, "Reserved Keyword Found [ " + me.getKey() + " ]");
//                }
//            }
//        }
    }


    public void validateForInvalidSubCategory(boolean subCategoryRequired, List<KeywordDetails> keywordDetailsList,
                                              BindingResult bindingResults) {
        if(subCategoryRequired) {
            isInvalidSubCategory(keywordDetailsList, bindingResults);
        }
    }

    protected boolean isInvalidSubCategory(List<KeywordDetails> keywordList, BindingResult bindingResults) {
        Logger.debug("Validating subcategories for special characters");
        boolean invalidCharSet = false;
        for (KeywordDetails keywordDetails : keywordList) {
            if(!parameterBlank(keywordDetails.getKeyword())) {
                Pattern pattern = Pattern.compile(SUB_CATEGORY_EXPRESSION);
                Matcher matcher = pattern.matcher(keywordDetails.getKeyword().trim());
                if (!matcher.matches()) {
                    invalidCharSet = true;
                    Logger.debug("Rejecting sub category because it does not contain valid character set: keyword [{}]",
                            keywordDetails.getKeyword());
                    bindingResults.rejectValue("selectedValues", "invalid.sub.category", "Invalid Sub Category Found [ "
                            + keywordDetails.getKeyword() + " ]");
                    break;
                }
            }
        }
        return invalidCharSet;
    }

    protected boolean isKeywordDuplicates(boolean isEditState, List<KeywordDetails> keywordExistingList,
                                          List<KeywordDetails> keywordList) {
        if(keywordList != null) {
            if (!isEditState) {
                return isKeywordElementsDuplicatesInSameList(keywordList);
            } else {
                return isKeywordElementsDuplicatesInSameList(keywordList) ||
                        isKeywordElementsDuplicatesInTwoList(keywordList, keywordExistingList);
            }
        }
        return false;
    }

    protected boolean isKeywordElementsDuplicatesInSameList(List<KeywordDetails> keywordList) {
        for (int i = 0; i < keywordList.size() - 1; i++) {
            for (int j = i +1 ; j < keywordList.size(); j++) {
                if (keywordList.get(j).getKeyword() != null && !keywordList.get(j).getKeyword().equals(""))
                    if (keywordList.get(i).getKeyword() != null &&
                            (keywordList.get(i).getKeyword().toLowerCase()).equals(keywordList.get(j).getKeyword().toLowerCase())) {
                        return true;
                    }
            }
        }
        return false;
    }


    private boolean isKeywordElementsDuplicatesInTwoList( List<KeywordDetails> firstList,
                                                          List<KeywordDetails> secondList){
        for (KeywordDetails aFirstList : firstList) {
            for (KeywordDetails aSecondList : secondList) {
                if (!aFirstList.getKeyword().equals("")) {
                    if ((aFirstList.getKeyword().toLowerCase()).equals(aSecondList.getKeyword().toLowerCase())) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    protected boolean hasEmptyDescriptions(ApplicationInfo appInfo) {
        for (KeywordDetails aKeywordList : appInfo.getSelectedValues()) {
            if (!parameterBlank(aKeywordList.getKeyword()) && parameterBlank(aKeywordList.getDescriptionEn())) {
                return true;
            }
            if (parameterBlank(aKeywordList.getKeyword()) && !parameterBlank(aKeywordList.getDescriptionEn())) {
                return true;
            }
        }
        return false;
    }


    protected boolean isDatesInvalid(ApplicationInfo appInfo) {

        try {
            final String startDate = appInfo.getStartDate();
            Date sDate = dateFormat.parse(startDate);
            final String endDate = appInfo.getEndDate();
            Date eDate = dateFormat.parse(endDate);
            return validateDateRange(sDate, eDate);
        } catch (ParseException e) {
            Logger.warn("error parsing Dates", e);
            return true;
        }
    }

    protected boolean validateDateRange(Date sDate, Date eDate) {
//        if ((sDate.after(eDate)) || (sDate.equals(eDate))){
        if ((sDate.after(eDate))){
            return true;
        }
        return false;
    }

    protected boolean isStartDateInvalid(ApplicationInfo appInfo, boolean isVotletCheck) {
        final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
        try {
            Date startDate;
            startDate = dateFormat.parse(appInfo.getStartDate() + " 23:59:59");
            final Date currentTime = new Date();
            boolean result = false;
            if (!appInfo.isEditState()) {
                result = currentTime.after(startDate);
            }
            Logger.debug("Rejecting start date value : [{}] current date [{}] start date [{}]",
                    new Object[]{result, currentTime, startDate});
            return result;
        } catch (ParseException e) {
            Logger.warn("error parsing Dates", e);
            return true;
        }
    }

    public static boolean isValidAppNameLength(String value, Long minValue, Long maxValue){
        boolean valid = true;
        if(value != null){
            if ((value.length() <= maxValue) && (value.length() >= minValue)) {
                valid = true;
            } else {
                valid = false;
            }
        }
        return valid;
    }

    public static boolean isNotValidSubcategoryLength(ApplicationInfo appInfo, Long minValue, Long maxValue){
        boolean valid = true;
        List<KeywordDetails> keywordList = appInfo.getSelectedValues();
        for (KeywordDetails keywordDetail : keywordList) {
            if (!keywordDetail.getKeyword().isEmpty()){
                if (!((keywordDetail.getKeyword().length() <= maxValue) &&
                        (keywordDetail.getKeyword().length() >= minValue))) {
                    valid = false;
                }
            }
        }
        return valid;
    }

    public static boolean isNotValidSubcategoryDescLength(ApplicationInfo appInfo, Long minValue, Long maxValue){
        boolean valid = true;
        List<KeywordDetails> keywordList = appInfo.getSelectedValues();
        for (KeywordDetails keywordDetails : keywordList) {
            if (!keywordDetails.getDescriptionEn().isEmpty()){
                if (!((keywordDetails.getDescriptionEn().length() <= maxValue) &&
                        (keywordDetails.getDescriptionEn().length() >= minValue))) {
                    valid = false;
                }
            }
        }
        return valid;
    }


    public static boolean isValidLength(String value, String maxValue){
        int maxLength  = Integer.parseInt(maxValue);
        boolean valid = true;
        if(value != null){
            if (value.length() <= maxLength) {
                valid = true;
            } else {
                valid = false;
            }
        }
        return valid;
    }


    public static boolean isValidDescriptionLength(String description, String maxValue) {
        int maxLength  = Integer.parseInt(maxValue);
        boolean valid = true;
        if(description != null){
            if (description.length() <= maxLength) {
                valid = true;
            } else {
                valid = false;
            }
        }
        return valid;
    }

    public static boolean isValidRegex(String value, String regex) {
        Pattern pattern = Pattern.compile(regex);
        return pattern.matcher(value).matches();
    }

    protected boolean isNotSpace(String userName) {
        if(userName != null){
            if(userName.split(" ").length > 1){
                return false;
            }else {
                return true;
            }
        }else {
            return false;
        }
    }

    public void setMESSAGE_LENGTH(String MESSAGE_LENGTH) {
        this.MESSAGE_LENGTH = MESSAGE_LENGTH;
    }

    public void setAPP_NAME_MIN_LENGTH(long APP_NAME_MIN_LENGTH) {
        this.APP_NAME_MIN_LENGTH = APP_NAME_MIN_LENGTH;
    }

    public void setAPP_NAME_MAX_LENGTH(long APP_NAME_MAX_LENGTH) {
        this.APP_NAME_MAX_LENGTH = APP_NAME_MAX_LENGTH;
    }

    public void setSUBCATEGORY_MIN_LENGTH(long SUBCATEGORY_MIN_LENGTH) {
        this.SUBCATEGORY_MIN_LENGTH = SUBCATEGORY_MIN_LENGTH;
    }

    public void setSUBCATEGORY_MAX_LENGTH(long SUBCATEGORY_MAX_LENGTH) {
        this.SUBCATEGORY_MAX_LENGTH = SUBCATEGORY_MAX_LENGTH;
    }

    public void setSUBCATEGORY_DESC_MIN_LENGTH(long SUBCATEGORY_DESC_MIN_LENGTH) {
        this.SUBCATEGORY_DESC_MIN_LENGTH = SUBCATEGORY_DESC_MIN_LENGTH;
    }

    public void setSUBCATEGORY_DESC_MAX_LENGTH(long SUBCATEGORY_DESC_MAX_LENGTH) {
        this.SUBCATEGORY_DESC_MAX_LENGTH = SUBCATEGORY_DESC_MAX_LENGTH;
    }

    public void setDESCRIPTION_LENGTH(String DESCRIPTION_LENGTH) {
        this.DESCRIPTION_LENGTH = DESCRIPTION_LENGTH;
    }

    public void setTEL_EXPRESSION(String TEL_EXPRESSION) {
        this.TEL_EXPRESSION = TEL_EXPRESSION;
    }

    public void setKEYWORD_EXPRESSION(String KEYWORD_EXPRESSION) {
        this.KEYWORD_EXPRESSION = KEYWORD_EXPRESSION;
    }

    public void setTELEPHONE_LENGTH(int TELEPHONE_LENGTH) {
        this.TELEPHONE_LENGTH = TELEPHONE_LENGTH;
    }

    public void setUSERANME_LENGTH(String USERANME_LENGTH) {
        this.USERANME_LENGTH = USERANME_LENGTH;
    }

    public void setPASSWORD_LENGTH(String PASSWORD_LENGTH) {
        this.PASSWORD_LENGTH = PASSWORD_LENGTH;
    }

    public void setCP_NAME_LENGTH(String CP_NAME_LENGTH) {
        this.CP_NAME_LENGTH = CP_NAME_LENGTH;
    }

    public void setBUSINESS_NUMBER_LENGTH(String BUSINESS_NUMBER_LENGTH) {
        this.BUSINESS_NUMBER_LENGTH = BUSINESS_NUMBER_LENGTH;
    }

    public void setCP_TEL_LENGTH(String CP_TEL_LENGTH) {
        this.CP_TEL_LENGTH = CP_TEL_LENGTH;
    }

    public void setCP_EMAIL_LENGTH(String CP_EMAIL_LENGTH) {
        this.CP_EMAIL_LENGTH = CP_EMAIL_LENGTH;
    }

    public void setCHARGING_MSISDN(String CHARGING_MSISDN) {
        this.CHARGING_MSISDN = CHARGING_MSISDN;
    }

    public void setREG_CODE_LENGTH(String REG_CODE_LENGTH) {
        this.REG_CODE_LENGTH = REG_CODE_LENGTH;
    }

    public void setPASSWORD_EXPRESSION(String PASSWORD_EXPRESSION) {
        this.PASSWORD_EXPRESSION = PASSWORD_EXPRESSION;
    }

    public void setPASSWORD_MIN_LENGTH(String PASSWORD_MIN_LENGTH) {
        this.PASSWORD_MIN_LENGTH = PASSWORD_MIN_LENGTH;
    }

    public String getBILLING_NAME_MAX_LENGTH() {
        return BILLING_NAME_MAX_LENGTH;
    }

    public void setBILLING_NAME_MAX_LENGTH(String BILLING_NAME_MAX_LENGTH) {
        this.BILLING_NAME_MAX_LENGTH = BILLING_NAME_MAX_LENGTH;
    }

    public String getBILLING_ADDRESS_MAX_LENGTH() {
        return BILLING_ADDRESS_MAX_LENGTH;
    }

    public void setBILLING_ADDRESS_MAX_LENGTH(String BILLING_ADDRESS_MAX_LENGTH) {
        this.BILLING_ADDRESS_MAX_LENGTH = BILLING_ADDRESS_MAX_LENGTH;
    }

    public void setAPP_NAME_EXPRESSION(String APP_NAME_EXPRESSION) {
        this.APP_NAME_EXPRESSION = APP_NAME_EXPRESSION;
    }

    public void setMSISDN_EXPRESSION(String MSISDN_EXPRESSION) {
        this.MSISDN_EXPRESSION = MSISDN_EXPRESSION;
    }

    public void setMAP_VALUE_MAX_LENGTH(String MAP_VALUE_MAX_LENGTH) {
        this.MAP_VALUE_MAX_LENGTH = MAP_VALUE_MAX_LENGTH;
    }

    public void setBIRTH_DATE_PATTERN(String BIRTH_DATE_PATTERN) {
        this.BIRTH_DATE_PATTERN = BIRTH_DATE_PATTERN;
    }

    public void setREQUEST_APP_RESPONSE_MSG_LENGTH(String REQUEST_APP_RESPONSE_MSG_LENGTH) {
        this.REQUEST_APP_RESPONSE_MSG_LENGTH = REQUEST_APP_RESPONSE_MSG_LENGTH;
    }

    public void setMSISDN_PREFIX(String MSISDN_PREFIX) {
        this.MSISDN_PREFIX = MSISDN_PREFIX;
    }

    public void setMSISDN_PREFIX_REPLACEMENT(String MSISDN_PREFIX_REPLACEMENT) {
        this.MSISDN_PREFIX_REPLACEMENT = MSISDN_PREFIX_REPLACEMENT;
    }

    public void setDisblePrefixReplacement(boolean disblePrefixReplacement) {
        this.disblePrefixReplacement = disblePrefixReplacement;
    }

    public void setKeywordExpirationValidationRequired(boolean keywordExpirationValidationRequired) {
        this.keywordExpirationValidationRequired = keywordExpirationValidationRequired;
    }

    public void setSUB_CATEGORY_EXPRESSION(String SUB_CATEGORY_EXPRESSION) {
        this.SUB_CATEGORY_EXPRESSION = SUB_CATEGORY_EXPRESSION;
    }
}



