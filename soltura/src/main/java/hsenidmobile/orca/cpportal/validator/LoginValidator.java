/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hsenidmobile.orca.cpportal.validator;

import hsenidmobile.orca.core.model.ContentProvider;
import hsenidmobile.orca.cpportal.module.Login;
import hsenidmobile.orca.orm.repository.ContentProviderDetailsRepositoryImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */


public class LoginValidator extends AbstractValidator {

    @Autowired
    private ContentProviderDetailsRepositoryImpl cpDetailsRepository;

    private static final Logger logger = LoggerFactory.getLogger(LoginValidator.class);

    public void validate(Login login, BindingResult bindingResult) {

        if (parameterInvalid(login.getUsername())) {
            bindingResult.rejectValue("username", "username.required");
        } else if (parameterInvalid(login.getPassword())) {
            bindingResult.rejectValue("password", "password.required");
        }
    }

    public void forgetPasswordValidator(Login login, BindingResult bindingResult) {

        if (parameterBlank(login.getUsername())) {
            bindingResult.rejectValue("username", "username.required");
        } else if (parameterBlank(login.getEmail())) {
            bindingResult.rejectValue("email", "email.required");
        } else {
            try {
                ContentProvider contentProvider = cpDetailsRepository.findByUsername(login.getUsername());
                if (isEmailNotMatch(login.getEmail(),contentProvider.getEmail())) {
                    bindingResult.rejectValue("username", "username.email.invalid");
                }
            } catch (Exception e) {
                bindingResult.rejectValue("username", "username.email.invalid");
            }

        }
    }

    public void ssoForgetPasswordValidator(Login login, BindingResult bindingResult) {
        replacePrefix(login);
        if(parameterBlank(login.getUsername())) {
            bindingResult.rejectValue("username", "username.required");
        } else if (!isValidRegex(login.getUsername(), MSISDN_EXPRESSION)) {
            bindingResult.rejectValue("username", "name.invalid.length"
                    , "Invalid Phone Number Found");
        } else if (!isNotSpace(login.getUsername())) {
            bindingResult.rejectValue("username", "username.null");
        }
    }

    private boolean isEmailNotMatch(String email, String cpEmail) {
        return !(email.equals(cpEmail));
    }

    private void replacePrefix(Login login) {
        String msisdn = login.getUsername();
        if (! disblePrefixReplacement) {
            if ((msisdn != null && ! msisdn.equals("")) && msisdn.length() > 1) {
                if (msisdn.startsWith(MSISDN_PREFIX)) {
                    login.setUsername(MSISDN_PREFIX_REPLACEMENT + msisdn.substring(1));
                }
            }
        }
    }


}
