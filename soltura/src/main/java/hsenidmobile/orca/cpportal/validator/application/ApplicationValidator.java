/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hsenidmobile.orca.cpportal.validator.application;

import hsenidmobile.orca.core.applications.ApplicationImpl;
import hsenidmobile.orca.core.applications.alert.AlertService;
import hsenidmobile.orca.core.applications.request.RequestService;
import hsenidmobile.orca.core.applications.subscription.Subscription;
import hsenidmobile.orca.core.applications.voting.VotingService;
import hsenidmobile.orca.core.model.ApplicationChargingInfo;
import hsenidmobile.orca.cpportal.module.ApplicationInfo;
import hsenidmobile.orca.cpportal.module.KeywordDetails;
import hsenidmobile.orca.cpportal.service.impl.application.ApplicationService;
import hsenidmobile.orca.cpportal.util.ServiceTypes;
import hsenidmobile.orca.cpportal.validator.AbstractValidator;
import hsenidmobile.orca.orm.repository.AppRepositoryImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static hsenidmobile.orca.cpportal.util.SubscriptionChargingType.MONTHLY_SUBS_CHARGING;


public class ApplicationValidator extends AbstractValidator {

    private static final Logger logger = LoggerFactory.getLogger(ApplicationValidator.class);

    @Autowired
    private AppRepositoryImpl appRepImpl;

    @Autowired
    private ApplicationService applicationService;

    public void validate(Object o, Errors errors, BindingResult bindingResult) {
        ApplicationInfo applicationInfo = (ApplicationInfo) o;
        ApplicationImpl applicationImpl = applicationInfo.getApp();
        validateApplicationGeneralData(applicationImpl, errors, bindingResult);
    }

    public void votingValidate(ApplicationInfo applicationInfo, BindingResult bindingResults) {
        logger.debug("Validating Create voting Application with : [{}]", applicationInfo);
        if (parameterInvalid(applicationInfo.getSelectedValues())) {
            bindingResults.rejectValue("selectedValues", "voting.vote.code.required");
        }
        if (parameterBlank(applicationInfo.getStartDate())) {
            bindingResults.rejectValue("startDate", "startdate.required");
        }
        if (parameterBlank(applicationInfo.getEndDate())) {
            bindingResults.rejectValue("endDate", "enddate.required");
        }
        if (hasEmptyDescriptions(applicationInfo)) {
            bindingResults.rejectValue("selectedValues", "description.invalid");
        }
        if (isSubCategoryRequired(applicationInfo) && isSubKeywordHasSpaces(applicationInfo)) {
            bindingResults.rejectValue("selectedValues", "keyword.have.space");
        }
        if (isSubCategoryRequired(applicationInfo) && !isNotValidSubcategoryLength(applicationInfo, SUBCATEGORY_MIN_LENGTH, SUBCATEGORY_MAX_LENGTH)){
            bindingResults.rejectValue("selectedValues", "subcategory.invalid.length", new Object[]{SUBCATEGORY_MIN_LENGTH, SUBCATEGORY_MAX_LENGTH}
                    , "Sub category length should be in the rangee");
        }
        if (isSubCategoryRequired(applicationInfo) && !isNotValidSubcategoryDescLength(applicationInfo, SUBCATEGORY_DESC_MIN_LENGTH, SUBCATEGORY_DESC_MAX_LENGTH)){
            bindingResults.rejectValue("selectedValues", "subcategory.desc.invalid.length", new Object[]{SUBCATEGORY_DESC_MIN_LENGTH, SUBCATEGORY_DESC_MAX_LENGTH}
                    , "Sub category description length should be in the rangee");
        }
        validateForReservedKeyword(applicationInfo.isSubCategoryRequired(),
                applicationInfo.getSelectedValues(), bindingResults);
        validateForInvalidSubCategory(applicationInfo.isSubCategoryRequired(),
                applicationInfo.getSelectedValues(), bindingResults);
//        if (isDatesInvalid(applicationInfo)) {
//            bindingResults.rejectValue("votletStartDate", "date.invalid");
//        } else if (isStartDateInvalid(applicationInfo, true)) {
//            bindingResults.rejectValue("votletStartDate", "start.date.invalid");
//        } else {
//            try {
//                Date startDate = dateFormat.parse(applicationInfo.getStartDate());
//                Date endDate = dateFormat.parse(applicationInfo.getEndDate());
//
//                Date startVotletDate = dateFormat.parse(applicationInfo.getVotletStartDate());
//                Date endVotletDate = dateFormat.parse(applicationInfo.getVotletEndDate());
//                if (startDate.after(startVotletDate)) {
//                    bindingResults.rejectValue("votletStartDate", "votlet.starts.after.app");
//                } else if (endVotletDate.after(endDate)) {
//                    bindingResults.rejectValue("votletStartDate", "votlet.end.after.app");
//                } else if (startDate.after(endVotletDate)) {
//                    bindingResults.rejectValue("votletStartDate", "votlet.end.before.start.app");
//                }
//                if (startVotletDate.after(endDate)) {
//                    bindingResults.rejectValue("votletStartDate", "votlet.starts.after.app.end");
//                }
//            } catch (ParseException e) {
//                logger.warn("error parsing Dates", e);
//                bindingResults.rejectValue("votletStartDate", "date.invalid");
//            }
//        }
        if (isKeywordDuplicates(applicationInfo.isEditState(),
                applicationInfo.getExistingSelectedValues(), applicationInfo.getSelectedValues())) {
            bindingResults.rejectValue("selectedValues", "candidate.keyword.duplicates");
        }
        logger.info("validation completed found errors [{}]", bindingResults.getErrorCount());
        StringBuilder stringBuilder = new StringBuilder("Following errors found on validation [");
        for (ObjectError objectError : bindingResults.getAllErrors()) {
            stringBuilder.append(objectError.getObjectName() + " : " + objectError.getDefaultMessage() + "] [");
        }
        logger.debug("[{}]", stringBuilder.append("]"));
    }

    public void alertValidate(ApplicationInfo applicationInfo, BindingResult bindingResults) {
        logger.debug("Validation Started on Alert application creation with :[{}]", applicationInfo);
        if (isSubCategoryRequired(applicationInfo) && parameterInvalid(applicationInfo.getSelectedValues())) {
            bindingResults.rejectValue("alertMessage", "alert.msg.required");
        }
        if (isSubCategoryRequired(applicationInfo) && isSubKeywordHasSpaces(applicationInfo)) {
            bindingResults.rejectValue("selectedValues", "keyword.have.space");
        }
        if (isSubCategoryRequired(applicationInfo) && !isNotValidSubcategoryLength(applicationInfo, SUBCATEGORY_MIN_LENGTH, SUBCATEGORY_MAX_LENGTH)){
            bindingResults.rejectValue("selectedValues", "subcategory.invalid.length", new Object[]{SUBCATEGORY_MIN_LENGTH, SUBCATEGORY_MAX_LENGTH}
                    , "Sub category length should be in the rangee");
        }
        validateSubCategories(applicationInfo, bindingResults);

//        if (applicationInfo.getApp().isRegistrationRequired() && applicationInfo.isChargingFromSubscriber()) {
//            if(!applicationInfo.isChargingPerMessage() && !applicationInfo.isChargingPerSubscription()) {
//                bindingResults.rejectValue("chargingFromSubscriber", "alert.msg.required");
//            }
//        }

        logger.info("Validation Success in Alert Application creation..");
    }

    private boolean isSubCategoryRequired(ApplicationInfo applicationInfo) {
        return applicationInfo.isSubCategoryRequired();
    }

    public void requestValidate(ApplicationInfo applicationInfo, BindingResult bindingResults) {
        logger.debug("Validation Started on Request application creation with :[{}]",applicationInfo);
        if (isSubCategoryRequired(applicationInfo) && isSubKeywordHasSpaces(applicationInfo)) {
            bindingResults.rejectValue("selectedValues", "keyword.have.space");
        }
        if (isSubCategoryRequired(applicationInfo) && !isNotValidSubcategoryLength(applicationInfo, SUBCATEGORY_MIN_LENGTH, SUBCATEGORY_MAX_LENGTH)){
            bindingResults.rejectValue("selectedValues", "subcategory.invalid.length", new Object[]{SUBCATEGORY_MIN_LENGTH, SUBCATEGORY_MAX_LENGTH}
                    , "Sub category length should be in the rangee");
        }
        validateSubCategories(applicationInfo, bindingResults);
        logger.info("Validation Success in Request Application creation..");
    }


    public void subscriptionValidate(ApplicationInfo applicationInfo, BindingResult bindingResults) {
        logger.debug("Validation Started on subscription application creation with :[{}]", applicationInfo);
        if (isSubCategoryRequired(applicationInfo) && isSubKeywordHasSpaces(applicationInfo)) {
            bindingResults.rejectValue("selectedValues", "keyword.have.space");
        }
        if (isSubCategoryRequired(applicationInfo) && !isNotValidSubcategoryLength(applicationInfo, SUBCATEGORY_MIN_LENGTH, SUBCATEGORY_MAX_LENGTH)){
            bindingResults.rejectValue("selectedValues", "subcategory.invalid.length", new Object[]{SUBCATEGORY_MIN_LENGTH, SUBCATEGORY_MAX_LENGTH}
                    , "Sub category length should be in the rangee");
        }
        validateSubCategories(applicationInfo, bindingResults);
        logger.info("Validation Success in subscription Application creation..");
    }

    public void validateSubCategories(ApplicationInfo applicationInfo, BindingResult bindingResults) {

        final boolean isSubCategoryRequired = applicationInfo.isSubCategoryRequired();
        final List<KeywordDetails> keywordDetailsList = applicationInfo.getSelectedValues();

        validateForReservedKeyword(isSubCategoryRequired, keywordDetailsList, bindingResults);
        validateForInvalidSubCategory(isSubCategoryRequired, keywordDetailsList, bindingResults);
        final boolean isError = isKeywordDuplicates(applicationInfo.isEditState(),
                applicationInfo.getExistingSelectedValues(), keywordDetailsList);

        if (isSubCategoryRequired(applicationInfo) && isError) {
            bindingResults.rejectValue("selectedValues", "keyword.duplicates");
        }
    }

    public void applicationValidate(ApplicationInfo applicationInfo, BindingResult bindingResults) {
        logger.debug("Validation Started application creation with :[{}]", applicationInfo);

        if (!applicationInfo.isEditState()) {
            validateSelectedRoutingKeys(applicationInfo, bindingResults);
            if(parameterBlank(applicationInfo.getApp().getAppName())) {
                bindingResults.rejectValue("app.appName", "app.name.required");
            } else if (parameterBlank(applicationInfo.getApp().getDescription())) {
                bindingResults.rejectValue("app.description", "app.description.required");
            } else if (!isValidAppNameLength(applicationInfo.getApp().getAppName(), APP_NAME_MIN_LENGTH, APP_NAME_MAX_LENGTH)) {
                bindingResults.rejectValue("app.appName", "appname.invalid.length", new Object[]{APP_NAME_MIN_LENGTH, APP_NAME_MAX_LENGTH}
                        , "Application Name Length should be within the range");
            } else if (!isValidRegex(applicationInfo.getApp().getAppName(), APP_NAME_EXPRESSION)) {
                bindingResults.rejectValue("app.appName", "appname.invalid");
            } else if (applicationService.isAppNameExistinSDP(applicationInfo.getApp().getAppName())
                    || isAppNameExists(applicationInfo.getApp().getAppName())) {
                bindingResults.rejectValue("app.appName", "app.name.exists");
            }
            logger.debug("VALIDATE ROUTING KEYS ####################### [{}] ", applicationInfo.getSelectedRoutingKeyValues().size());

            if (operatorNotSelected(applicationInfo.getSelectedRoutingKeyValues())) {
                logger.debug("OPERATOR NOT SELECTED ===============================");
                bindingResults.rejectValue("selectedRoutingKeyValues", "keyword.required");
            }

        }

        if (!isValidLength(applicationInfo.getSubscriptionSuccessMsgEn(), MESSAGE_LENGTH)) {
            bindingResults.rejectValue("subscriptionSuccessMsgEn", "message.invalid.length", new Object[]{MESSAGE_LENGTH}
                    , "Subscription Response is too long");
        }

        if (!isValidDescriptionLength(applicationInfo.getApp().getDescription(), DESCRIPTION_LENGTH)) {
            bindingResults.rejectValue("app.description", "description.invalid.length", new Object[]{DESCRIPTION_LENGTH}
                    , "Description length is too long");
        }

        if (!isValidLength(applicationInfo.getUnsubscribeSuccessMsgEn(), MESSAGE_LENGTH)) {
            bindingResults.rejectValue("unsubscribeSuccessMsgEn", "message.invalid.length", new Object[]{MESSAGE_LENGTH}
                    , "Unsubscription Response is too long");
        }

        //Invalid Request Message Removed
//        if (!isValidLength(applicationInfo.getRequestErrorMsgEn(), MESSAGE_LENGTH)) {
//            bindingResults.rejectValue("requestErrorMsgEn", "message.invalid.length", new Object[]{MESSAGE_LENGTH}
//                    , "Invalid Request is too long");
//        }
        if (applicationInfo.getApp().isExpirableApp() && isDatesInvalid(applicationInfo)) {
            bindingResults.rejectValue("startDate", "date.invalid");
        } else if (isStartDateInvalid(applicationInfo, false)) {
            bindingResults.rejectValue("startDate", "start.date.invalid");
        } else {
            if (!applicationInfo.isEditState()) {
//                validateIfKeywordWithinDateRange(applicationInfo, bindingResults);
            }
        }

        if (applicationInfo.isEditState() && applicationInfo.getApp().isRegistrationRequired()) {
            ApplicationChargingInfo applicationChargingInfo = applicationInfo.getApp().getApplicationChargingInfo();
            if (!applicationChargingInfo.isChargingForMessage() && !applicationChargingInfo.isChargingForSubscription()) {
                bindingResults.rejectValue("chargingPerMessage", "charing.method.not.selected");
            }
        }

        logger.info("Validation Success in Application creation..");

        if (!applicationInfo.isEditState()) {
            validateSubscriptionCharging(applicationInfo, bindingResults);
        }
        if(applicationInfo.getServiceType().equals(ServiceTypes.REQUEST.name())) {
            validateRequestAppResponseMessage(applicationInfo, bindingResults);
        }
    }

    private void validateSelectedRoutingKeys(ApplicationInfo applicationInfo, BindingResult bindingResults) {
        Set keywordSet = new HashSet(applicationInfo.getSelectedRoutingKeyValues());
        if (keywordSet.size() == 1 && parameterBlank((String) keywordSet.iterator().next())) {
            bindingResults.rejectValue("selectedRoutingKeyValues", "keyword.required");
        }
    }

    private void validateRequestAppResponseMessage(ApplicationInfo applicationInfo, BindingResult bindingResults) {
        if (applicationInfo.getApp().isRequireCommonResponse()) {
            if(parameterBlank(applicationInfo.getRequestAppResponseMsg())) {
                bindingResults.rejectValue("requestAppResponseMsg", "response.message.required");
            } else {
                if(!isValidLength(applicationInfo.getRequestAppResponseMsg(), REQUEST_APP_RESPONSE_MSG_LENGTH)) {
                    bindingResults.rejectValue("requestAppResponseMsg", "response.message.too.long");
                }
            }
        }else {
            applicationInfo.setRequestAppResponseMsg(null);
        }
    }

    private void validateSubscriptionCharging(ApplicationInfo applicationInfo, BindingResult bindingResults) {
        ApplicationChargingInfo applicationChargingInfo = applicationInfo.getApp().getApplicationChargingInfo();
        if (applicationInfo.getApp().isRegistrationRequired() && applicationChargingInfo.isChargingFromSubscriber()) {
            if(!applicationChargingInfo.isChargingForMessage() && !applicationChargingInfo.isChargingForSubscription()) {
                bindingResults.rejectValue("app.appName", "subscription.charging.required");
            }
        }

        if(applicationChargingInfo.getSelectedFinancialInstrumentType() == null && !applicationChargingInfo.isChargingFromSubscriber()){
            bindingResults.rejectValue("app.appName", "subscription.financial.instrument.required");
            if (applicationInfo.getApp().getService() instanceof AlertService
                    || applicationInfo.getApp().getService() instanceof Subscription) {

                applicationInfo.getApp().getApplicationChargingInfo().setChargingForSubscription(true);
                applicationInfo.setSubscriptionChargingType(MONTHLY_SUBS_CHARGING);
                applicationInfo.getApp().getApplicationChargingInfo().setChargingForMessage(false);
                applicationInfo.getApp().getApplicationChargingInfo().setChargingFromSubscriber(true);
                applicationInfo.getApp().getApplicationChargingInfo().setFrmSubscriberPerMsgChargingValue("PI");
            } else if (applicationInfo.getApp().getService() instanceof VotingService
                    || applicationInfo.getApp().getService() instanceof RequestService) {

                applicationInfo.getApp().getApplicationChargingInfo().setChargingFromSubscriber(true);
                applicationInfo.getApp().getApplicationChargingInfo().setFrmSubscriberPerMsgChargingValue("PI");
            }
        }
    }
    //todo modify this keyword validation after finalizing with the new sdp
//    private void validateIfKeywordWithinDateRange(ApplicationInfo applicationInfo, BindingResult bindingResults) {
//
//        final RoutingInfo routingInfo = selectedKeywordDetails(applicationInfo);
//        if (routingInfo == null) {
//            bindingResults.rejectValue("availableKeyword", "keyword.required");
//            return;
//        }
//        Date applicationEndDate = null;
//        try {
//            applicationEndDate = dateFormat.parse(applicationInfo.getEndDate());
//        } catch (ParseException e) {
//            logger.error("Error while converting date range to check keyword end date and application end date :", e);
//            bindingResults.rejectValue("startDate", "date.invalid");
//            return;
//        }
//        if(keywordExpirationValidationRequired) {
//            final Date keywordEndDate = new Date(routingInfo.getEndDate());
//            if (applicationEndDate.after(keywordEndDate)) {
//                final String[] stringErrorList = new String[2];
//                stringErrorList[0] = "'" + routingInfo.getRoutingKey().getKeyword() + "'";
//                stringErrorList[1] = "'" + dateFormat.format(keywordEndDate) + "'";
//                bindingResults.rejectValue("startDate", "invalid.keyword.selected.for.application", stringErrorList, null);
//            }
//        }
//    }

//    public RoutingInfo selectedKeywordDetails(ApplicationInfo applicationInfo) {
//        //todo handle the selected routing key list
//        List<String> rkList = applicationInfo.getSelectedRoutingKeyValues();
//        final String[] keywordArray = applicationInfo.getSelectedKeyword().split("-");
//        if (keywordArray.length != 2) {
//            return null;
//        }
//        String keyword = keywordArray[0];
//        String selectedShortCode = applicationInfo.getSelectedShortCode();
//        String operator = keywordArray[1];
//
//        final ContentProvider providerDetails = applicationService.getContentProviderDetails(false);
//        logger.debug("Available keywords for Cp : cpId[{}] size[{}]", new Object[]{providerDetails.getCpId(),
//                providerDetails.getOwnedRoutingInfo().size()});
//        Msisdn shortCode;
//        for (RoutingInfo routingInfo : providerDetails.getUnassignedUnExpiredRoutingKeys()) {
//            shortCode = routingInfo.getRoutingKey().getShortCode();
//            if (routingInfo.getRoutingKey().getKeyword().equals(keyword) &&
//                    shortCode.getAddress().equals(selectedShortCode) &&
//                    shortCode.getOperator().equals(operator)) {
//                return routingInfo;
//            }
//        }
//        return null;
//    }

    private boolean isKeywordsBlank(ApplicationInfo appInfo) {
        boolean isBlank = true;
        for (KeywordDetails aKeywordList : appInfo.getSelectedValues()) {
            if (!parameterBlank(aKeywordList.getKeyword())) {
                isBlank = false;
            }
        }
        return isBlank;
    }

    public boolean isAppNameExists(String appName) {
        return appRepImpl.isAppNameExists(appName);
    }

    public boolean operatorNotSelected(List<String> routingKeys) {
//        operator1 - 3456 - yek3
        int count=0;
        for (String s : routingKeys) {
            if ("N/A".equals(s)) {
                count++;
            }
        }

        logger.debug("COUNTER &&&&&&&&&&&&&&& [{}]", count);
        return count == routingKeys.size();
    }
}




