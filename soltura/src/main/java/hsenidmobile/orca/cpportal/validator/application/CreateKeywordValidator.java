package hsenidmobile.orca.cpportal.validator.application;


import hsenidmobile.orca.core.applications.exception.ApplicationException;
import hsenidmobile.orca.core.model.*;
import hsenidmobile.orca.core.services.impl.RoutingKeyManagerImpl;
import hsenidmobile.orca.cpportal.module.CreateKeyword;
import hsenidmobile.orca.cpportal.service.CurrentUser;
import hsenidmobile.orca.cpportal.service.impl.application.ApplicationService;
import hsenidmobile.orca.cpportal.validator.AbstractValidator;
import hsenidmobile.orca.orm.repository.ContentProviderDetailsRepositoryImpl;
import hsenidmobile.orca.rest.common.ResponseCodeMapper;
import hsenidmobile.orca.rest.sdp.routing.transport.SdpRkManageRequestSender;
import org.apache.cxf.jaxrs.client.ClientWebApplicationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static hsenidmobile.orca.rest.common.util.RestApiKeys.*;

public class CreateKeywordValidator extends AbstractValidator {

    private static final Logger logger = LoggerFactory.getLogger(CreateKeywordValidator.class);

    @Autowired
    private ResponseCodeMapper responseCodeMapper;
    @Autowired
    protected ContentProviderDetailsRepositoryImpl cpDetailsRepository;
    @Autowired
    protected RoutingKeyManagerImpl routingKeyManagerImpl;
    @Autowired
    protected ApplicationService applicationService;
    @Autowired
    protected SdpRkManageRequestSender sdpRkManageRequestSender;

    public void validate(CreateKeyword routingInfo, BindingResult bindingResult) throws Exception {
        logger.debug("Started Validating with : [{}]", routingInfo);
        try {
            ContentProvider cp = cpDetailsRepository.findByUsername(CurrentUser.getUsername());
            if (parameterBlank(routingInfo.getRoutingKey().getShortCode().getAddress())) {
                bindingResult.rejectValue("routingKey.shortCode.address", "shortcode.address.required");
            }
            if (parameterBlank(routingInfo.getsDate())) {
                bindingResult.rejectValue("sDate", "startdate.required");
            }
            if (parameterBlank(routingInfo.geteDate())) {
                bindingResult.rejectValue("eDate", "enddate.required");
            }
            if (isDatesInvalid(routingInfo)) {
                bindingResult.rejectValue("sDate", "end.date.is.before.start.date");
            }
            validateKeyword(routingInfo, bindingResult, cp);
            logger.info("Validation on routingInfo success!!");
        } catch (Exception e) {
            logger.error("Error while Validating Create keyword data", e);
            throw e;
        }
    }

    protected boolean isDatesInvalid(CreateKeyword routingInfo) {

        try {
            Date startDate = dateFormat.parse(routingInfo.getsDate());
            Date endDate = dateFormat.parse(routingInfo.geteDate());
            return startDate.after(endDate);
        } catch (ParseException e) {
            logger.warn("error parsing Dates", e);
            return true;
        }
    }

    /*
        TODO: Made this method a synchronized due to the bug id: 18755. Need to test whether this solves the issue
     */
    protected synchronized void validateKeyword(CreateKeyword createKeyword, BindingResult bindingResult, ContentProvider cp) throws Exception {
        boolean keywordValid = parameterBlank(createKeyword.getRoutingKey().getKeyword());
        logger.debug("KEYWORD [ {} ] ", createKeyword.getRoutingKey().getKeyword());
        boolean validCharSet = false;
        if (!keywordValid) {
            Pattern pattern = Pattern.compile(KEYWORD_EXPRESSION);
            Matcher matcher = pattern.matcher(createKeyword.getRoutingKey().getKeyword().trim());
            if (!matcher.matches()) {
                validCharSet = true;
                logger.debug("Rejecting keyword because it does not contain valid character set: keyword [{}]",
                        createKeyword.getRoutingKey().getKeyword());
            }
        }
        if (keywordValid) {
            bindingResult.rejectValue("routingKey.keyword", "keyword.required");
        } else if (validCharSet) {
            bindingResult.rejectValue("routingKey.keyword", "invalid.keyword");
        }  else if  (!KeywordAvailable(createKeyword, cp.getCpId())) {
            bindingResult.rejectValue("routingKey.keyword", "keyword.already.exists");
            logger.debug("Requested keyword is not Available.");
        } else {
            final String[] addressArray = createKeyword.getRoutingKey().getShortCode().getAddress().split("-");
            final RoutingInfo routingInfo = new RoutingInfo();
            routingInfo.setRoutingKey(new RoutingKey(new Msisdn(addressArray[0], addressArray[1]),
                    createKeyword.getRoutingKey().getKeyword().trim()));
            routingInfo.setStartDate(dateFormat.parse(createKeyword.getsDate() + " 00:00:00").getTime());
            routingInfo.setEndDate(dateFormat.parse(createKeyword.geteDate() + " 23:59:59").getTime());
            logger.debug("---------------------- Validating keyword [{}] ------------------------", routingInfo);
        }
    }

    /**
     * Check the Availability of the user input keyword comparing with reserved keywords in Repo.
     * @param createKeyword
     * @param cpId
     * @return
     */
    protected boolean KeywordAvailable(CreateKeyword createKeyword, String cpId) throws Exception {
        boolean isRkAvailable = true;
        Map<String, String> messageMap = new HashMap<String, String>();
        try {
            String code = createKeyword.getRoutingKey().getShortCode().getAddress();
            String parts[] = code.split("-");
            String shortcode = parts[0];
            String operator = parts[1];
            String keyword = createKeyword.getRoutingKey().getKeyword().toLowerCase();
            logger.debug("======== " + cpId + " , " + shortcode + " , " + keyword + " , " + operator);
            messageMap.put(SHORTCODE, shortcode);
            messageMap.put(KEYWORD, keyword);
            messageMap.put(OPERATOR, operator);
            Map<String, Object> response =  sdpRkManageRequestSender.validateAvailableRks(messageMap);
            String rkAvailabilityStatus = (String) response.get(STATUS);
            if (rkAvailabilityStatus.trim().equals(KEYWORD_NOT_AVAILABLE_CODE)) {
                isRkAvailable = false;
            }
        }
        catch (Exception  e) {
            logger.debug("Error occurred while validating the Keyword  " + e);
            if (e.getClass().equals(ClientWebApplicationException.class)){
                throw e;
            }
        }
        return isRkAvailable;
    }

    public boolean isAllowedToDelete(RoutingInfo routingInfo, BindingResult bindingResult, ContentProvider cp) throws ApplicationException {
        boolean isAllowedToDelete = false;
        if (routingInfo.getStatus() == RoutingInfoStatus.AVAILABLE) {
            isAllowedToDelete = true;
        } else if (routingInfo.getStatus() == RoutingInfoStatus.ASSIGNED &&
                applicationService.findAppById(routingInfo.getOwningAppId()).getStatus() == Status.RETIRED) {
            isAllowedToDelete = true;
        } else {
            //todo need to remove this logs
            logger.info("Not allowed to delete the routing key [{}]", routingInfo.getRoutingKey().getKeyword());
            bindingResult.rejectValue("cp.ownedRoutingInfo", "error.delete.keyword.info");
        }
        return isAllowedToDelete;
    }
}









