/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hsenidmobile.orca.cpportal.validator.application;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class SearchApplicationValidator implements Validator {
     @Override
    public boolean supports(Class aClass) {
        return true;
    }

    @Override
    public void validate(Object o, Errors errors) {

    }
}
