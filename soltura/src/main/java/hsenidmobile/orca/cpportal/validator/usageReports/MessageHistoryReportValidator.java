/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hsenidmobile.orca.cpportal.validator.usageReports;

import hsenidmobile.orca.cpportal.module.MessageHistoryInfo;
import hsenidmobile.orca.cpportal.validator.AbstractValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.BindingResult;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class MessageHistoryReportValidator extends AbstractValidator {

    private static final Logger logger = LoggerFactory.getLogger(MessageHistoryReportValidator.class);

    public void validate(MessageHistoryInfo messageHistoryInfo, BindingResult bindingResult) {
        final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date reportFromDate;
        Date reportToDate;

        logger.debug("Validation Started Message History Report with :[{}]", messageHistoryInfo);

        if (parameterInvalid(messageHistoryInfo.getSelectedApplicationIdsForCp())) {
            logger.debug("Validation failed with Message History Report : " +
                    "At lease one application name must be selected");
            bindingResult.rejectValue("selectedApplicationIdsForCp", "application.name.required");
        }
        if (!messageHistoryInfo.isShowAll()) {
            if (!parameterBlank(messageHistoryInfo.getReportFromDate()) &&
                    !parameterBlank(messageHistoryInfo.getReportToDate())) {
                try {
                    reportFromDate = dateFormat.parse(messageHistoryInfo.getReportFromDate());
                    reportToDate = dateFormat.parse(messageHistoryInfo.getReportToDate());

                    if (validateDateRange(reportFromDate, reportToDate)) {
                        logger.debug("Validation failed with Message History Report : " +
                                "From data should be earlier than To date");
                        bindingResult.rejectValue("reportFromDate", "date.invalid");
                    }
                } catch (ParseException e) {
                    logger.debug("Validation failed with Message History Report : Error in parsing Dates");
                }
            }
        }
    }

}
