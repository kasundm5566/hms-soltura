/* 
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited. 
 *   All Rights Reserved. 
 * 
 *   These materials are unpublished, proprietary, confidential source code of 
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET 
 *   of hSenid Software International (Pvt) Limited. 
 * 
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual 
 *   property rights in these materials. 
 *
 */
package hsenidmobile.orca.cpportal.view;

import org.springframework.web.servlet.view.AbstractView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletOutputStream;
import java.util.Map;
import java.io.FileWriter;
import java.io.OutputStream;
import java.io.Writer;
import java.io.PrintWriter;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public abstract class AbstractCsvView extends AbstractView {

    /** The content type for an Excel response */
    private static final String CONTENT_TYPE = "application/csv";
    /** The extension to look for existing templates */
    private static final String EXTENSION = ".csv";

    public AbstractCsvView() {
        setContentType(CONTENT_TYPE);
    }

    @Override
    protected void renderMergedOutputModel(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        response.setContentType(getContentType());
        OutputStream out = response.getOutputStream();
        PrintWriter printWriter = new PrintWriter(out);
        buildCsvDocument(model, printWriter, request, response);
        response.setContentType(getContentType());
        printWriter.flush();
//        ServletOutputStream outputStream = response.getOutputStream();
//        printWriter.write(outputStream);
//        outputStream.flush();

    }

    protected abstract void buildCsvDocument(Map model,
                                             PrintWriter printWriter,
                                             HttpServletRequest request,
                                             HttpServletResponse response)
            throws Exception;
}
