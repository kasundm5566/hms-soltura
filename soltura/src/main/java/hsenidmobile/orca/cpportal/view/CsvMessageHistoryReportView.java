/* 
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited. 
 *   All Rights Reserved. 
 * 
 *   These materials are unpublished, proprietary, confidential source code of 
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET 
 *   of hSenid Software International (Pvt) Limited. 
 * 
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual 
 *   property rights in these materials. 
 *
 */
package hsenidmobile.orca.cpportal.view;

import hsenidmobile.orca.core.model.MessageHistory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.support.RequestContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class CsvMessageHistoryReportView extends AbstractCsvView {

    private static final Logger logger = LoggerFactory.getLogger(CsvMessageHistoryReportView.class);
    private ReportConfigurationLoader reportConfigurationLoader;
    private String csvContentType;
    private String csvContentDisposition;
    private SimpleDateFormat dateFormater;

    @Override
    protected void buildCsvDocument(Map model, PrintWriter printWriter, HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            logger.debug("Creating CSV report for Message History Started");
            Map<String, List<MessageHistory>> historyData =
                    (Map<String, List<MessageHistory>>) model.get("historyReportData");
            final List<MessageHistory> recordList = historyData.get("historyData");
            RequestContext ctx = new RequestContext(request);

            for (MessageHistory messageHistory : recordList) {
                printWriter.append(createRecord(messageHistory, ctx).toString());
                printWriter.append("\n");
            }
            response.setContentType(csvContentType) ;
            response.addHeader("Content-Disposition", MessageFormat.format(csvContentDisposition, getArguments()));
            printWriter.flush();
            logger.debug("Creating CSV report for Message History Finished");
        } catch (Exception e) {
            logger.error("Error while generating CSV report for Message History", e);
        }
    }

    private StringBuffer createRecord(MessageHistory messageHistory, RequestContext ctx) {
        StringBuffer record = new StringBuffer();
        if (messageHistory.getMessageTimeStamp() != 0) {
            record.append(dateFormater.format(new Date(messageHistory.getMessageTimeStamp()))).append(",");
        } else {
            record.append(",");
        }
        record.append(messageHistory.getApplicationName()).append(",");
        record.append(ctx.getMessage("message.history.event." + messageHistory.getEvent().toString())).append(",");
        record.append(messageHistory.getMessage());
        return record;
    }

    private String getArguments() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
        return  dateFormat.format(new Date());
    }

    public void setReportConfigurationLoader(ReportConfigurationLoader reportConfigurationLoader) {
        this.reportConfigurationLoader = reportConfigurationLoader;
    }

    public void setCsvContentType(String csvContentType) {
        this.csvContentType = csvContentType;
    }

    public void setCsvContentDisposition(String csvContentDisposition) {
        this.csvContentDisposition = csvContentDisposition;
    }

    public void setDatePattern(String pattern) {
        this.dateFormater = new SimpleDateFormat(pattern); 
    }
}
