/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hsenidmobile.orca.cpportal.view;

import hsenidmobile.orca.core.model.RevenueSummary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;
import java.util.Date;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class CsvRevenueSummaryReportView extends AbstractCsvView {

    private ReportConfigurationLoader reportConfigurationLoader;
    private static final Logger logger = LoggerFactory.getLogger(CsvRevenueSummaryReportView.class);
    private String csvContentType;
    private String csvContentDisposition;

    @Override
    protected void buildCsvDocument(Map model, PrintWriter printWriter, HttpServletRequest request,
                                    HttpServletResponse response) throws Exception {
        try {
            logger.debug("Creating CSV report for Revenue Summary Started");
            Map<String, List<RevenueSummary>> revenueData =
                    (Map<String, List<RevenueSummary>>) model.get("revenueReportData");
            final List<RevenueSummary> recordList = revenueData.get("revenueData");

            for (RevenueSummary revenueSummary : recordList) {
                printWriter.append(revenueSummary.printReport());
                printWriter.append("\n");
            }
            response.setContentType(csvContentType) ;
            response.addHeader("Content-Disposition", MessageFormat.format(csvContentDisposition, getArguments()));
            printWriter.flush();
            logger.debug("Creating CSV report for Revenue Summary Finished");
        } catch (Exception e) {
            logger.error("Error while generating CSV report for Revenue Summary", e);
        }
    }

    private String getArguments() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
        return  dateFormat.format(new Date());
    }

    public void setReportConfigurationLoader(ReportConfigurationLoader reportConfigurationLoader) {
        this.reportConfigurationLoader = reportConfigurationLoader;
    }

    public void setCsvContentType(String csvContentType) {
        this.csvContentType = csvContentType;
    }

    public void setCsvContentDisposition(String csvContentDisposition) {
        this.csvContentDisposition = csvContentDisposition;
    }
}
