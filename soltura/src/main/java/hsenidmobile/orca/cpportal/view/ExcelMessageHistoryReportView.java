/* 
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited. 
 *   All Rights Reserved. 
 * 
 *   These materials are unpublished, proprietary, confidential source code of 
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET 
 *   of hSenid Software International (Pvt) Limited. 
 * 
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual 
 *   property rights in these materials. 
 *
 */
package hsenidmobile.orca.cpportal.view;

import java.util.Map;
import java.util.List;
import java.util.Date;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.view.document.AbstractExcelView;
import hsenidmobile.orca.core.model.MessageHistory;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class ExcelMessageHistoryReportView extends AbstractExcelView {

    private ReportConfigurationLoader reportConfigurationLoader;
    private static final Logger logger = LoggerFactory.getLogger(ExcelMessageHistoryReportView.class);
    private String excelContentType ;
    private String excelContentDisposition;

    @Override
    protected void buildExcelDocument(Map model, HSSFWorkbook workbook,
                                      HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        try {
            logger.debug("Creating Excel report for Message History Started");
            Map<String, List<MessageHistory>> historyData =
                    (Map<String, List<MessageHistory>>) model.get("historyReportData");
            //create a wordsheet
            HSSFSheet sheet = workbook.createSheet("Message History Report");
            HSSFRow header = sheet.createRow(0);
            header.createCell(0).setCellValue(reportConfigurationLoader.getRecordId());
            header.createCell(1).setCellValue(reportConfigurationLoader.getTimeStamp());
            header.createCell(2).setCellValue(reportConfigurationLoader.getAppId());
            header.createCell(3).setCellValue(reportConfigurationLoader.getAppName());
            header.createCell(4).setCellValue(reportConfigurationLoader.getEventName());
            header.createCell(5).setCellValue(reportConfigurationLoader.getMessage());
            int rowNum = 1;
            for (MessageHistory messageHistory : historyData.get("historyData")) {
                HSSFRow row = sheet.createRow(rowNum++);
                row.createCell(0).setCellValue(messageHistory.getId());
                row.createCell(1).setCellValue(messageHistory.getMessageTimeStamp());
                row.createCell(2).setCellValue(messageHistory.getApplicationId());
                row.createCell(3).setCellValue(messageHistory.getApplicationName());
                row.createCell(4).setCellValue(messageHistory.getEvent().name());
                row.createCell(5).setCellValue(messageHistory.getMessage());
            }
            response.setContentType(excelContentType);
            response.addHeader("Content-Disposition", MessageFormat.format(excelContentDisposition, getArguments()));
            logger.debug("Creating Excel report for Message History Finished");
        } catch (Exception e) {
            logger.error("Error while generating Excel report for Message History", e);
        }
    }

    private String getArguments() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
        return  dateFormat.format(new Date());
    }

    public void setReportConfigurationLoader(ReportConfigurationLoader reportConfigurationLoader) {
        this.reportConfigurationLoader = reportConfigurationLoader;
    }

    public void setExcelContentType(String excelContentType) {
        this.excelContentType = excelContentType;
    }

    public void setExcelContentDisposition(String excelContentDisposition) {
        this.excelContentDisposition = excelContentDisposition;
    }
}
