/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hsenidmobile.orca.cpportal.view;

import hsenidmobile.orca.core.model.RevenueSummary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.web.servlet.view.document.AbstractExcelView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;
import java.util.Date;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class ExcelRevenueSummaryReportView extends AbstractExcelView {

    private ReportConfigurationLoader reportConfigurationLoader;
    private static final Logger logger = LoggerFactory.getLogger(ExcelRevenueSummaryReportView.class);
    private String excelContentType;
    private String excelContentDisposition;

    @Override
    protected void buildExcelDocument(Map model, HSSFWorkbook workbook, HttpServletRequest httpServletRequest,
                                      HttpServletResponse httpServletResponse) throws Exception {
        try {
            logger.debug("Creating Excel report for Revenue Summary Started");
            Map<String, List<RevenueSummary>> revenueData =
                    (Map<String, List<RevenueSummary>>) model.get("revenueReportData");
            //create a wordsheet
            HSSFSheet sheet = workbook.createSheet("Revenue Summary Report");
            HSSFRow header = sheet.createRow(0);
            header.createCell(0).setCellValue(reportConfigurationLoader.getAppId());
            header.createCell(1).setCellValue(reportConfigurationLoader.getAppName());
            header.createCell(2).setCellValue(reportConfigurationLoader.getTransactionCount());
            header.createCell(3).setCellValue(reportConfigurationLoader.getDirection());
            header.createCell(4).setCellValue(reportConfigurationLoader.getTotalRevenue());
            int rowNum = 1;
            for (RevenueSummary revenueSummary : revenueData.get("revenueData")) {
                HSSFRow row = sheet.createRow(rowNum++);
                row.createCell(0).setCellValue(revenueSummary.getRevenueSummaryPrimaryKey().getApplicationId());
                row.createCell(1).setCellValue(revenueSummary.getApplicationName());
                row.createCell(2).setCellValue(revenueSummary.getTotalTransCount());
                row.createCell(3).setCellValue(revenueSummary.getRevenueSummaryPrimaryKey().getDirection());
                row.createCell(4).setCellValue(revenueSummary.getTotalRevenue());
            }
            httpServletResponse.setContentType(excelContentType) ;
            httpServletResponse.addHeader("Content-Disposition", MessageFormat.format(excelContentDisposition, getArguments()));
            logger.debug("Creating Excel report for Revenue Summary Finished");
        } catch (Exception e) {
            logger.error("Error while generating Excel report for Revenue Summary", e);
        }
    }

    private String getArguments() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
        return  dateFormat.format(new Date());
    }

    public void setReportConfigurationLoader(ReportConfigurationLoader reportConfigurationLoader) {
        this.reportConfigurationLoader = reportConfigurationLoader;
    }

    public void setExcelContentType(String excelContentType) {
        this.excelContentType = excelContentType;
    }

    public void setExcelContentDisposition(String excelContentDisposition) {
        this.excelContentDisposition = excelContentDisposition;
    }
}
