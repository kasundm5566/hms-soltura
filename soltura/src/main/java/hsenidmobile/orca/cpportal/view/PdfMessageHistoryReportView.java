/* 
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited. 
 *   All Rights Reserved. 
 * 
 *   These materials are unpublished, proprietary, confidential source code of 
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET 
 *   of hSenid Software International (Pvt) Limited. 
 * 
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual 
 *   property rights in these materials. 
 *
 */
package hsenidmobile.orca.cpportal.view;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import hsenidmobile.orca.core.model.MessageHistory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.view.document.AbstractPdfView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class PdfMessageHistoryReportView extends AbstractPdfView {

    private ReportConfigurationLoader reportConfigurationLoader;
    private static final Logger logger = LoggerFactory.getLogger(PdfMessageHistoryReportView.class);
    private static final int NUMBER_OF_FIELDS = 6;
    private String pdfContentType;
    private String pdfContentDisposition;

    @Override
    protected void buildPdfDocument(Map model, Document document,
                                    PdfWriter writer, HttpServletRequest request,
                                    HttpServletResponse response) throws Exception {
        try {
            logger.debug("Creating PDF report for Message History Started");
            Map<String, List<MessageHistory>> historyData =
                    (Map<String, List<MessageHistory>>) model.get("historyReportData");
            PdfPTable pdfPTable = new PdfPTable(NUMBER_OF_FIELDS);

            pdfPTable.addCell(reportConfigurationLoader.getRecordId());
            pdfPTable.addCell(reportConfigurationLoader.getTimeStamp());
            pdfPTable.addCell(reportConfigurationLoader.getAppId());
            pdfPTable.addCell(reportConfigurationLoader.getAppName());
            pdfPTable.addCell(reportConfigurationLoader.getEventName());
            pdfPTable.addCell(reportConfigurationLoader.getMessage());
            for (MessageHistory messageHistory : historyData.get("historyData")) {
                pdfPTable.addCell(String.valueOf(messageHistory.getId()));
                pdfPTable.addCell(messageHistory.getMessageTimeStampInDateFormat());
                pdfPTable.addCell(messageHistory.getApplicationId());
                pdfPTable.addCell(messageHistory.getApplicationName());
                pdfPTable.addCell(messageHistory.getEvent().name());
                pdfPTable.addCell(messageHistory.getMessage());
            }
            pdfPTable.getDefaultCell().setPadding(20f);

            response.setContentType(pdfContentType) ;
            response.addHeader("Content-Disposition", MessageFormat.format(pdfContentDisposition, getArguments()));
            document.add(pdfPTable);
            logger.debug("Creating PDF report for Message History Finished");
        } catch (DocumentException e) {
            logger.error("Error while generating PDF report for Message History", e);
        }
    }

    private String getArguments() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
        return  dateFormat.format(new Date());
    }

    public void setReportConfigurationLoader(ReportConfigurationLoader reportConfigurationLoader) {
        this.reportConfigurationLoader = reportConfigurationLoader;
    }

    public void setPdfContentType(String pdfContentType) {
        this.pdfContentType = pdfContentType;
    }

    public void setPdfContentDisposition(String pdfContentDisposition) {
        this.pdfContentDisposition = pdfContentDisposition;
    }
}
