/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hsenidmobile.orca.cpportal.view;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.PdfWriter;
import hsenidmobile.orca.core.model.RevenueSummary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.view.document.AbstractPdfView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;
import java.util.Date;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class PdfRevenueSummRyReportView extends AbstractPdfView {

    private ReportConfigurationLoader reportConfigurationLoader;
    private static final Logger logger = LoggerFactory.getLogger(PdfRevenueSummRyReportView.class);
    private static final int NUMBER_OF_FIELDS = 5;
    private String pdfContentType;
    private String pdfContentDisposition;

    @Override
    protected void buildPdfDocument(Map model, Document document, PdfWriter pdfWriter,
                                    HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse)
            throws Exception {
        try {
            logger.debug("Creating PDF report for Revenue Summary Started");
            Map<String, List<RevenueSummary>> revenueData =
                    (Map<String, List<RevenueSummary>>) model.get("revenueReportData");
            Table table = new Table(NUMBER_OF_FIELDS);
            table.addCell(reportConfigurationLoader.getAppId());
            table.addCell(reportConfigurationLoader.getAppName());
            table.addCell(reportConfigurationLoader.getTransactionCount());
            table.addCell(reportConfigurationLoader.getDirection());
            table.addCell(reportConfigurationLoader.getTotalRevenue());
            for (RevenueSummary revenueSummary : revenueData.get("revenueData")) {
                table.addCell(String.valueOf(revenueSummary.getRevenueSummaryPrimaryKey().getApplicationId()));
                table.addCell(revenueSummary.getApplicationName());
                table.addCell(String.valueOf(revenueSummary.getTotalTransCount()));
                table.addCell(revenueSummary.getRevenueSummaryPrimaryKey().getDirection());
                table.addCell(String.valueOf(revenueSummary.getTotalRevenue()));
            }
            httpServletResponse.setContentType(pdfContentType) ;
            httpServletResponse.addHeader("Content-Disposition", MessageFormat.format(pdfContentDisposition, getArguments()));
            document.add(table);
            logger.debug("Creating PDF report for Revenue Summary Finished");
        } catch (DocumentException e) {
            logger.error("Error while generating PDF report for Revenue Summary", e);
        }
    }

    private String getArguments() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
        return  dateFormat.format(new Date());
    }

    public void setReportConfigurationLoader(ReportConfigurationLoader reportConfigurationLoader) {
        this.reportConfigurationLoader = reportConfigurationLoader;
    }

    public void setPdfContentType(String pdfContentType) {
        this.pdfContentType = pdfContentType;
    }

    public void setPdfContentDisposition(String pdfContentDisposition) {
        this.pdfContentDisposition = pdfContentDisposition;
    }
}
