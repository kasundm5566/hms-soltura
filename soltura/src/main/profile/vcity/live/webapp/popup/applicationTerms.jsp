<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!--
(C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
All Rights Reserved.

These materials are unpublished, proprietary, confidential source code of
hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
of hSenid Software International (Pvt) Limited.

hSenid Software International (Pvt) Limited retains all title to and intellectual
property rights in these materials.
-->

<script type="text/javascript">
    function submitForm() {
        document.forms[1].submit();
    }

    function agreeStatement() {
        var checkBoxTick = document.getElementById("checkBoxTick");
        if (checkBoxTick.checked) {
            document.getElementById("ok").disabled = false
        } else {
            document.getElementById("ok").disabled = true
        }
    }

</script>
<div id="popup-terms" style="display:none;">
<div id="body" style="width:auto;height:auto;">
<div class="wrap">
<div id="dashboard-widgets-wrap">
<div id='dashboard-widgets' class='metabox-holder' style="width:550px">
<div id="dashboard_quick_press" class="postbox ">
<h3 class='hndle'><span><fmt:message key="terms.and.condition"/></span></h3>

<div class="inside" style="margin:20px; max-height:400px;overflow:auto;">
<div>

    <p><font size="2" face="Verdana"><b>Terms of Service</b></font> <br><br></p>

    <p><font size="2" face="Verdana">
        This Developer Program License Agreement ("Agreement"") is made and
        entered into in [Date] ("Effective Date"") by and between Virtual Mobile Ltd,
        registered in Nairobi, Kenya having its place of business at Virtual
        Building, Ring Road, Kilimani Kenya and [company] ("XXX"), having
        its place of business at [address].</font> <br><br></p>
    <p><font size="2" face="Verdana"><b>PLEASE READ THE FOLLOWING LICENSE AGREEMENT
        TERMS AND CONDITIONS CAREFULLY BEFORE DOWNLOADING OR USING HEWANI PORTAL.
        THESE TERMS AND CONDITIONS CONSTITUTE A LEGAL AGREEMENT BETWEEN YOU
        AND VIRTUAL MOBILE LTD.</b></font> <br><br></p>

    <p><font size="4" face="Verdana"><b>Virtual Mobile Ltd App Store Developer Program License Agreement</b></font> <br><br></p>

    <p><font size="4" face="Verdana"><b>Purpose</b></font> <br><br></p>
    <p><font size="2" face="Verdana">You would like to use Hewani Portal (as defined below) to develop one or more Applications (as defined below) for Virtual Mobile Ltd App Store. Virtual Mobile Ltd is willing to grant You a limited license to use Hewani Portal to develop and test Your Applications on the terms and conditions set forth in this Agreement.</font> <br><br></p>
    <p><font size="2" face="Verdana">Applications developed under this Agreement and selected by Virtual Mobile Ltd can be distributed in two ways: (1) through the App Store (2) on a limited basis for use on Registered Devices (as defined below).</font> <br><br></p>
    <p><font size="2" face="Verdana">Applications that meet Documentation and Program Requirements may be submitted for consideration for distribution via the App Store. If submitted by You and selected by Virtual Mobile Ltd your Applications will be digitally signed and distributed through the App Store.</font> <br><br></p>

    <p><font size="4" face="Verdana"><b>1. Accepting this Agreement; Definitions</b></font> <br><br></p>
    <p><font size="2" face="Verdana"><b>1.1 Acceptance</b></font> <br><br></p>
    <p><font size="2" face="Verdana">In order to use Hewani Portal and related services, You must first agree to this License Agreement. If You do not or cannot agree to this License Agreement, You are not permitted to use Hewani Portal or related services. Do not download or use Hewani Portal or any related services in that case.</font> <br><br></p>
    <p><font size="2" face="Verdana">You accept and agree to the terms of this License Agreement on Your own behalf and/or on behalf of Your company, organization or educational institution as its authorized legal representative, by doing either of the following:</font> <br><br> </p>
    <p><font size="2" face="Verdana">a)Checking the box displayed at the end of this Agreement, if You are reading this on  the App Zone website </font> <br><br></p>
    <p><font size="2" face="Verdana">b)Clicking an "Agree" or similar button, where this option is provided.</font> <br><br></p>
    <p><font size="2" face="Verdana">c)Digital signing if You receive this Agreement via E-mail/Fax etc.</font> <br><br></p>
    <p><font size="2" face="Verdana"><b>1.2 Updates; No Support or Maintenance</b></font> <br><br></p>  <br><br>
    <p><font size="2" face="Verdana">Hewani Portal or services provided hereunder may be extended, enhanced or otherwise modified at any time without notice, but Virtual Mobile Ltd shall not be obligated to provide You with any Updates to Hewani Portal. If Updates are made available, the terms of this Agreement will govern such Updates, unless the Update is accompanied by a separate license in which case the terms of that license will govern. Virtual Mobile Ltd is not obligated to provide any maintenance, technical or other support for Hewani Portal or services. </font> <br><br></p>
    <p><font size="2" face="Verdana">You acknowledge that Virtual Mobile Ltd has no express or implied obligation to announce or make available any Updates to Hewani Portal or to any services to anyone in the future. Should an Update be made available, it may have APIs, features, services or functionality that are different from those found in Hewani Portal licensed or the services provided hereunder.</font> <br><br></p>

    <p><font size="4" face="Verdana"><b>2. Your Obligations</b></font> <br><br></p>
    <p><font size="2" face="Verdana"><b>2.1 General</b></font> <br><br></p>
    <p><font size="2" face="Verdana"><i>You certify and agree that:</i></font> <br><br></p>
    <p><font size="2" face="Verdana">(a) All information provided by You, including without limitation Licensed Application Information, will be current, true, accurate and complete and You will promptly notify Virtual Mobile Ltd of any changes to such information;</font> <br><br></p>
    <p><font size="2" face="Verdana">(b) You will comply with the terms of and fulfill Your obligations under this Agreement and You agree to monitor and be responsible for Your Authorized Developers' use of Hewani Portal and services and Authorized Test Devices and their compliance with the terms of this Agreement;</font> <br><br></p>
    <p><font size="2" face="Verdana">(c) You will be solely responsible for all costs, expenses, losses and liabilities incurred, and activities undertaken by You and Authorized Developers in connection with Hewani Portal and services, the Registered Devices, Your Applications and Your related development and distribution efforts, including, but not limited to any related development efforts, network and server equipment, Internet service(s), or any other hardware, software or services used by You in connection with Your use of any services;</font> <br><br></p>
    <p><font size="2" face="Verdana">(d) You represent and warrant that You own or control the necessary rights in order to appoint Virtual Mobile Ltd and Subsidiaries as Your worldwide agent for the delivery of Your Licensed Applications, and that the fulfillment of such appointment by Virtual Mobile Ltd and Subsidiaries shall not violate or infringe the rights of any third party; and</font> <br><br></p>
    <p><font size="2" face="Verdana">(e) You will not act in any manner which conflicts or interferes with any existing commitment or obligation You may have and no agreement previously entered into by You will interfere with Your performance of Your obligations under this Agreement.</font> <br><br></p>

    <p><font size="2" face="Verdana"><b>2.2 Use of Hewani Portal.</b></font> <br><br></p>
    <p><font size="2" face="Verdana">As a condition to use Hewani Portal and any services, You agree that:</font> <br><br></p>
    <p><font size="2" face="Verdana">(a) You will only use Hewani Portal and any services for the purposes and in the manner expressly permitted by this Agreement and in accordance with all applicable laws and regulations;</font> <br><br></p>
    <p><font size="2" face="Verdana">(b) You will not use Hewani Portal  or any services for any unlawful or illegal activity, nor to develop any Application which would commit or facilitate the commission of a crime, or other tortuous, unlawful or illegal act;</font> <br><br></p>
    <p><font size="2" face="Verdana">(c) Your Application will be developed in compliance with the Documentation and the Program</font> <br><br></p>
    <p><font size="2" face="Verdana">&#09;         a) Requirements, the current set of which is set forth in Section 3.3 below;</font> <br><br></p>
    <p><font size="2" face="Verdana">&#09;         b) Program Agreement Page 6</font> <br><br></p>
    <p><font size="2" face="Verdana">(d) To the best of Your knowledge and belief, Your Application and Licensed Application Information do not and will not violate, misappropriate, or infringe any Virtual Mobile Ltd or third party copyrights, trademarks, rights of privacy and publicity, trade secrets, patents, or other proprietary or legal rights (e.g. musical composition or performance rights, video rights, photography or image rights, logo rights, third party data rights, etc. for content and materials that may be included in Your Application);</font> <br><br></p>
    <p><font size="2" face="Verdana">(e) Through use of Hewani Portal, services or otherwise, You will not create any Application or other program that would disable, hack or otherwise interfere with the Security Solution, or any security, digital signing, digital rights management, verification or authentication mechanisms implemented in or by the Virtual Mobile Ltd App Store operating system, any services or other Hewani Portal or technology, or enable others to do so; and </font> <br><br></p>
    <p><font size="2" face="Verdana">(f) Applications developed using Hewani Portal may only be distributed if selected for distribution via the App Store as contemplated in this Agreement.</font> <br><br></p>

    <p><font size="2" face="Verdana"><b>2.3 Program Requirements for Applications</b></font> <br><br></p>
    <p><font size="2" face="Verdana">Any Application developed using this Hewani Portal must meet all of the following criteria and requirements, as they may be modified from time to time:</font> <br><br></p>
    <p><font size="2" face="Verdana"><b>Functionality:</b></font> <br><br></p>
    <p><font size="2" face="Verdana">2.3.1 Applications may only be used in the manner prescribed by Virtual Mobile.</font> <br><br></p>
    <p><font size="2" face="Verdana">2.3.2 An Application may not itself install or launch other executable code by any means, including without limitation through the use of a plug-in architecture, calling other frameworks, other APIs or otherwise. No interpreted code may be downloaded or used in an Application except for code that is interpreted and run by Virtual Mobile.</font> <br><br></p>
    <p><font size="2" face="Verdana">2.3.3 An Application may only read data from or write data to an Application's designated container area on the device, except as otherwise specified by Virtual Mobile.</font> <br><br></p>
    <p><font size="2" face="Verdana"><b>User Interface and Data:</b></font> <br><br></p>
    <p><font size="2" face="Verdana">2.3.4 Applications must comply with the Human Interface Guidelines and other Documentation provided by Virtual Mobile Ltd.</font> <br><br></p>
    <p><font size="2" face="Verdana">2.3.5 Any form of user or device data collection, or image, picture or voice capture or recording performed by the Application (collectively "Recordings"), and any form of user data, content or information processing, maintenance, uploading, syncing, or transmission performed by the Application (collectively "Transmissions") must comply with all applicable privacy laws and regulations as well as any Virtual Mobile Ltd program requirements related to such aspects, including but not limited to any notice or consent requirements. In particular, a reasonably conspicuous audio, visual or other indicator must be displayed to the user as part of the Application to indicate that a Recording is taking place.</font> <br><br></p>
    <p><font size="2" face="Verdana"><b>Local Laws, User Privacy, Location Services and Mapping:</b></font> <br><br></p>
    <p><font size="2" face="Verdana">2.3.6 Applications must comply with all applicable criminal, civil and statutory laws and regulations, including those in any jurisdictions in which Your Applications may be delivered. In addition, for Applications that use location-based APIs or that collect, transmit, maintain, process, share, disclose or otherwise use a user's personal information or data:</font> <br><br></p>
    <p><font size="2" face="Verdana">&#09;         - You and the Application must comply with all applicable privacy and data collection laws and regulations with respect to any collection, transmission, maintenance, processing, use, etc. of the user's location data or personal information by the Application.</font> <br><br></p>
    <p><font size="2" face="Verdana">&#09;         - Applications may not be designed or marketed for the purpose of harassing, abusing, stalking, threatening or otherwise violating the legal rights (such as the rights of privacy and publicity) of others.</font> <br><br></p>
    <p><font size="2" face="Verdana">&#09;         - Applications may not perform any functions or link to any content or use any robot, spider, site search or other retrieval application or device to scrape, retrieve or index services provided by Virtual Mobile Ltd or its licensors, or to collect, disseminate or use information about users for any unauthorized purpose.</font> <br><br></p>
    <p><font size="2" face="Verdana">&#09;         - Applications that offer location-based services or functionality must notify and obtain consent from an individual before his or her location data is being collected, transmitted or otherwise used by the Application.</font> <br><br></p>
    <p><font size="2" face="Verdana">2.3.7 For Applications that use location-based APIs for real-time route guidance (including, but not limited to, turn-by-turn route guidance and other routing that is enabled through the use of a sensor), You must place the following notice in Your end user license agreement: YOUR USE OF THIS REAL TIME ROUTE GUIDANCE APPLICATION IS AT YOUR SOLE RISK. LOCATION DATA MAY NOT BE ACCURATE.</font> <br><br></p>
    <p><font size="2" face="Verdana">2.3.8 Applications must not disable, override or otherwise interfere with any Virtual Mobile Ltd implemented system alerts, warnings, display panels, consent panels and the like, including, but not limited to, those that are intended to notify the user that the user's location data is being collected, transmitted, maintained, processed or used, or intended to obtain consent for such use. If consent is denied or withdrawn, Applications may not collect, transmit, maintain, process or utilize the user's location data or perform any other actions for which the user's consent has been denied or withdrawn.</font> <br><br></p>
    <p><font size="2" face="Verdana"><b>Content and Materials:</b></font> <br><br></p>
    <p><font size="2" face="Verdana">2.3.9 Any master recordings and musical compositions embodied in Your Application must be wholly-owned by You or licensed to You on a fully paid-up basis and in a manner that will not require the payment of any fees, royalties and/or sums by Virtual Mobile Ltd to You or any third party. In addition, if Your Application will be distributed outside of Kenya, any master recordings and musical compositions embodied in Your Application </font> <br><br></p>
    <p><font size="2" face="Verdana">&#09;&#09;         (a) Must not fall within the repertoire of any mechanical or performing/communication rights collecting or licensing organization now or in the future and </font> <br><br></p>
    <p><font size="2" face="Verdana">&#09;&#09;         (b) If licensed, must be exclusively licensed to You for Your Application by each applicable copyright owner.</font> <br><br></p>
    <p><font size="2" face="Verdana">2.3.10 If Your Application includes or will include any other content, You must either own all such content or have permission from the content owner to use it in Your Application.</font> <br><br></p>
    <p><font size="2" face="Verdana">2.3.11 Applications must not contain any obscene, pornographic, offensive or defamatory content or materials of any kind (text, graphics, images, photographs, etc.), or other content or materials that in Virtual Mobile's reasonable judgment may be found objectionable by Virtual Mobile Ltd App Store.</font> <br><br></p>
    <p><font size="2" face="Verdana">2.3.12 Applications must not contain any malware, malicious or harmful code, program, or other internal component (e.g. computer viruses, trojan horses, "backdoors") which could damage, destroy, or adversely affect other software, firmware, hardware, data, systems, services, or networks.</font> <br><br></p>
    <p><font size="2" face="Verdana">2.3.13 If Your Application includes any FOSS, You agree to comply with all applicable FOSS licensing terms. You also agree not to use any FOSS in the development of Your Application in such a way that would cause the non-FOSS portions of Hewani Portal to be subject to any FOSS licensing terms or obligations.</font> <br><br></p>
    <p><font size="2" face="Verdana"><b>In App Purchase API:</b></font> <br><br></p>
    <p><font size="2" face="Verdana">2.3.14 Only Paid Applications (which require you to enter into a separate agreement with Virtual Mobile Ltd (Schedule 2) may use the In App Purchase API. In addition:</font> <br><br></p>
    <p><font size="2" face="Verdana">&#09;         - Your Application may only use the In App Purchase API for adding functionality that has been reviewed and approved by Virtual Mobile Ltd and/or content that has been approved by Virtual Mobile Ltd in accordance with the processes set forth in Section 6.</font> <br><br></p>
    <p><font size="2" face="Verdana">&#09;         - You may not enable end users to purchase Currency of any kind through the In App Purchase API, including but not limited to any Currency for exchange, gifting, redemption, transfer, trading or use in purchasing or obtaining anything within or outside of Your Application. For the avoidance of doubt, nothing herein is intended to prohibit You from offering for sale goods or services (other than Currency) to be delivered outside of Your Application.</font> <br><br></p>
    <p><font size="2" face="Verdana">&#09;         - You may not enable the end user to set up a pre-paid account to be used for subsequent purchases of content or functionality, or otherwise create balances or credits that end users can redeem or use to make purchases at a later time.</font> <br><br></p>
    <p><font size="2" face="Verdana"><b>Cellular Network:</b></font> <br><br></p>
    <p><font size="2" face="Verdana">2.3.15 If an Application requires or will have access to the cellular network, then additionally such Applications:</font> <br><br></p>
    <p><font size="2" face="Verdana">&#09;         - Must comply with Virtual Mobile Ltd’s best practices and other guidelines on how Applications should access and use the cellular network;</font> <br><br></p>
    <p><font size="2" face="Verdana">&#09;         - Must not in Virtual Mobile Ltd's reasonable judgment excessively uses or unduly burden network capacity or bandwidth;</font> <br><br></p>
    <p><font size="2" face="Verdana">&#09;         - May not have Voice over Internet Protocol (VoIP) functionality using the cellular network.</font> <br><br></p>

    <p><font size="4" face="Verdana"><b>5. Digital Signing of Applications; Restrictions on Certificates</b></font> <br><br></p>
    <p><font size="2" face="Verdana">All Applications must be signed with a Virtual Mobile-issued certificate in order to be installed on Registered Devices. During the Term of this Agreement, You may obtain development-related digital certificates from Virtual Mobile Ltd, subject to a maximum number as reasonably determined by Virtual Mobile Ltd that will allow Your Application to be installed and tested on Authorized Test Devices. You may also obtain, during the Term, one or more production digital certificates from Virtual Mobile, subject to a maximum number as reasonably determined by Virtual Mobile Ltd, to be used for the sole purpose of signing Your Application(s) prior to submission of Your Application to Virtual Mobile Ltd or limited distribution of Your Application for use on Registered Devices.</font> <br><br></p>
    <p><font size="2" face="Verdana">In relation to this, You represent and warrant that: </font> <br><br></p>
    <p><font size="2" face="Verdana">(a) You will not take any action to interfere with the normal operation of any Virtual Mobile-issued digital certificates </font> <br><br></p>
    <p><font size="2" face="Verdana">(b) You are solely responsible for preventing any unauthorized person from having access to Your digital certificates and corresponding private keys and You will use best efforts to safeguard Your digital certificates and corresponding private keys from compromise; </font> <br><br></p>
    <p><font size="2" face="Verdana">(c) You agree to immediately notify Virtual Mobile Ltd in writing if You have any reason to believe there has been a compromise of any of Your digital certificates or corresponding private keys; </font> <br><br></p>
    <p><font size="2" face="Verdana">(d) You will not provide or transfer Virtual Mobile-issued digital certificates provided under this Program to any third party, nor use Your digital certificate to sign a third party's application; and </font> <br><br></p>
    <p><font size="2" face="Verdana">(e) You will use Virtual Mobile-issued certificates provided under this Program exclusively for the purpose of signing Your Applications for testing, submission to Virtual Mobile Ltd and/or limited distribution for use on Registered Devices as contemplated under this Program, and only in accordance with this Agreement. You further represent and warrant that the licensing terms governing Your Application, or governing any third party code or FOSS included in Your Application, will be consistent with and not conflict with the digital signing or content protection aspects of the Program or any of the terms, conditions or requirements of the Program or this Agreement. In particular, such licensing terms will not purport to require Virtual Mobile Ltd (or its agents) to disclose or make available any of the keys, authorization codes, methods, procedures, data or other information related to the Security Solution, digital signing or digital rights management mechanisms utilized as part of the Program.</font> <br><br></p>
    <p><font size="2" face="Verdana">If You discover any such inconsistency or conflict, You agree to immediately notify Virtual Mobile Ltd of it and will cooperate with Virtual Mobile Ltd to resolve such matter. Virtual Mobile Ltd may immediately cease distribution of any affected Licensed Applications and refuse to accept any subsequent Application submissions from You until such matter is resolved to Virtual Mobile Ltd’s reasonable satisfaction.</font> <br><br></p>

    <p><font size="4" face="Verdana"><b>6. Revenue Sharing</b></font> <br><br></p>
    <p><font size="2" face="Verdana">There are three revenue sharing models.</font> <br><br></p>
    <p><font size="2" face="Verdana"><b>6.1 Subscription Model</b></font> <br><br></p>
    <p><font size="2" face="Verdana">For subscription based applications, the Developer will receive 28% of the revenue. Or for e.g. in the case of a Ksh 1 Subscription App – Ksh 0,28 (whichever is lower) will be shared with the Application Developer.</font> <br><br></p>
    <p><font size="2" face="Verdana"><b>6.2 Event Based Model</b></font> <br><br></p>
    <p><font size="2" face="Verdana">For an event based application, the Developer will receive 28% of the revenue. Or for e.g. in the case of an Event based App where every event is to be charged Ksh 0.10 the developer will receive Ksh 0.028 (whichever is lower) from each event.</font> <br><br></p>
    <p><font size="2" face="Verdana"><b>6.3 Downloadable App Model</b></font> <br><br></p>
    <p><font size="2" face="Verdana">For a downloadable app, the Developer will receive 28% of the revenue. Or for e.g. in the case of a Ksh 2 Downloadable App –Ksh 0.56 (whichever is lower) will be shared with the Application Developer.</font> <br><br></p>
    <p><font size="2" face="Verdana">If the telecom operator breaches the contract or delays payment for whatever reasons, Virtual Mobile Ltd is not obliged to pay the developers.</font> <br><br></p>

    <p><font size="4" face="Verdana"><b>7. Application Submission and Selection</b></font> <br><br></p>
    <p><font size="2" face="Verdana"><b>7.1 Submission to Virtual Mobile</b></font> <br><br></p>
    <p><font size="2" face="Verdana">You may submit Your Application for consideration by Virtual Mobile Ltd for distribution via the App Store once You decide that Your Application has been adequately tested and is complete. By submitting Your Application, You represent and warrant that Your Application complies with the Documentation and Program Requirements then in effect. You further agree that You will not attempt to hide or obscure any features, content or functionality in Your submitted Applications from Virtual Mobile's review or otherwise hinder Virtual Mobile Ltd from being able to fully review such Applications. In addition, You agree to inform Virtual Mobile Ltd in writing through App Developer forum if Your Application connects to a physical device and, if so, to disclose the means of such connection and identify at least one physical device with which Your Application is designed to communicate. You agree to cooperate with Virtual Mobile Ltd in this submission process and to answer questions and provide information and materials in connection with Your submitted Application, as reasonably requested by Virtual Mobile, including, without limitation, to provide Virtual Mobile Ltd with access to or with samples of physical devices that connect to Your Application.</font> <br><br></p>
    <p><font size="2" face="Verdana">If You are submitting functionality that You would like to make available through the use of the In App Purchase API, then the functionality must be submitted to and approved by Virtual Mobile Ltd in the same manner as the Application in accordance with this Section 6. For both functionality and content submissions, You must provide the name, text description, price, unique identifier number, and other information that Virtual Mobile Ltd reasonably requests (collectively, the "Submission Description") to Virtual Mobile. For content submissions, the actual content will not have to be submitted to Virtual Mobile, unless requested by Virtual Mobile. Virtual Mobile Ltd reserves the right to review the actual content that has been described in the Submission Descriptions at any time, including, but not limited to, in the submission process and after approval of the Submission Description by Virtual Mobile. If You would like to provide additional content through the In App Purchase API that is not described in Your Submission Description, then You must first submit a new or updated Submission Description for review and approval by Virtual Mobile Ltd prior to using the In App Purchase API for the delivery of any such content. Virtual Mobile Ltd reserves the right to withdraw its approval of content previously approved, and You agree to stop making such content available for use within Your Application. For avoidance of doubt, all content and functionality delivered through the In App Purchase API is subject to the Program Requirements for Applications.</font> <br><br></p>
    <p><font size="2" face="Verdana">If You make any changes to an Application (including to any functionality made available through use of the In App Purchase API) after submission to Virtual Mobile, You must resubmit the Application to Virtual Mobile. Similarly all bug fixes, updates, upgrades, modifications, enhancements, supplements to, revisions, new releases and new versions of Your Application must be submitted to Virtual Mobile Ltd for review in order for them to be considered for distribution via the App Store. Further, if Your Application is accepted for distribution via the App Store, You agree that Virtual Mobile Ltd may use Your Application for the limited purpose of compatibility testing of Your Application with the Virtual Mobile Ltd OS, for finding and fixing bugs in the Virtual Mobile Ltd OS and for purposes of providing other information to You (e.g. crash logs).</font> <br><br></p>
    <p><font size="2" face="Verdana"><b>6.2 Selection by Virtual Mobile Ltd for Distribution</b></font> <br><br></p>
    <p><font size="2" face="Verdana">You understand and agree that Virtual Mobile Ltd may, in its sole discretion:</font> <br><br></p>
    <p><font size="2" face="Verdana">(a) Determine that Your Application does not meet all or any part of the Documentation or Program Requirements then in effect;</font> <br><br></p>
    <p><font size="2" face="Verdana">(b) Reject Your Application for distribution for any reason, even if Your Application meets the Documentation and Program Requirements; or</font> <br><br></p>
    <p><font size="2" face="Verdana">(c) Select and digitally sign Your Application for distribution via the App Store. Virtual Mobile Ltd shall not be responsible for any costs, expenses, damages, losses (including without limitation lost business opportunities or lost profits) or other liabilities You may incur as a result of Your Application development, use of this Virtual Mobile Ltd Hewani Portal, use of any services, or participation in the Program, including without limitation the fact that Your Application may not be selected for distribution via the App Store. You will be solely responsible for developing Applications that are safe, free of defects in design and operation, and comply with applicable laws and regulations. You will also be solely responsible for any documentation and end user customer support and warranty of Your Applications. The fact that Virtual Mobile Ltd may have reviewed, tested, approved or selected an Application will not relieve You of any of these responsibilities.</font> <br><br></p>

    <p><font size="4" face="Verdana"><b>7. Distribution</b></font> <br><br></p>
    <p><font size="2" face="Verdana">Applications developed under this Agreement may be distributed in two ways: (1) Through the App Store, if selected, and (2) Distribution for use on a limited number of Registered Devices.</font> <br><br></p>

    <p><font size="4" face="Verdana"><b>8.  Revocation</b></font> <br><br></p>
    <p><font size="2" face="Verdana">You understand and agree that Virtual Mobile Ltd may cease distribution of Your Licensed Application(s) and/or Licensed Application Information or revoke the digital certificate of any of Your Applications at any time. By way of example only, Virtual Mobile Ltd might choose to do this if at any time:</font> <br><br></p>
    <p><font size="2" face="Verdana">(a) Any of Your Provisioning Profiles, digital certificates or corresponding private keys has been compromised or Virtual Mobile Ltd has reason to believe that either has been compromised;</font> <br><br></p>
    <p><font size="2" face="Verdana">(b) Virtual Mobile Ltd has been notified or otherwise has reason to believe that Your Application violates, misappropriates, or infringes the rights of a third party or of Virtual Mobile;</font> <br><br></p>
    <p><font size="2" face="Verdana">(c) Virtual Mobile Ltd has reason to believe that Your Application contains malicious or harmful code, malware, programs or other internal components (e.g. software virus);</font> <br><br></p>
    <p><font size="2" face="Verdana">(d) Virtual Mobile Ltd has reason to believe that Your Application damages, corrupts, degrades, destroys or otherwise adversely affects the devices it operates on, or any other software, firmware, hardware, data, systems, or networks accessed or used by the Application;</font> <br><br></p>
    <p><font size="2" face="Verdana">(e) You breach any term or condition of this Agreement or the Registered Virtual Mobile Ltd App Store Developer terms and conditions;</font> <br><br></p>
    <p><font size="2" face="Verdana">(f) Any information or documents provided by You to Virtual Mobile Ltd for the purpose of verifying Your identity or obtaining Provisioning Profiles or Virtual Mobile-issued digital certificates is false or inaccurate;</font> <br><br></p>
    <p><font size="2" face="Verdana">(g) Any representation, warranty or certification provided by You to Virtual Mobile Ltd in this Agreement is untrue or inaccurate;</font> <br><br></p>
    <p><font size="2" face="Verdana">(h) Virtual Mobile Ltd is required by law, regulation or other governmental or court order to take such action;</font> <br><br></p>
    <p><font size="2" face="Verdana">(i) You request that Virtual Mobile Ltd take such action in accordance with Schedule 1;</font> <br><br></p>
    <p><font size="2" face="Verdana">(j) You misuse or overburden any services provided hereunder;</font> <br><br></p>
    <p><font size="2" face="Verdana">(k) You fail to renew this Agreement and pay the applicable renewal fee; or</font> <br><br></p>
    <p><font size="2" face="Verdana">(I) Virtual Mobile Ltd has reason to believe that such action is prudent or necessary.</font> <br><br></p>

    <p><font size="4" face="Verdana"><b>10. Confidentiality</b></font> <br><br></p>
    <p><font size="2" face="Verdana"><b>10.1 Information Deemed Virtual Mobile Ltd Confidential</b></font> <br><br></p>
    <p><font size="2" face="Verdana">You agree that all pre-release versions of Hewani Portal (including pre-release Documentation) and services, any terms and conditions contained herein that disclose prerelease features of Hewani Portal or services, and the terms and conditions of Schedule 2 (available separately to cover distribution of paid-for Licensed Applications via the App Store) will be deemed "Virtual Mobile Ltd Confidential Information"; provided however that upon the commercial release of Hewani Portal the terms and conditions that disclose pre-release features of the Virtual Mobile Ltd Software or services will no longer be confidential. Notwithstanding the foregoing, Virtual Mobile Ltd Confidential Information will not include: (i) information that is generally and legitimately available to the public through no fault or breach of Yours, (ii) information that is generally made available to the public by Virtual Mobile, (iii) information that is independently developed by You without the use of any Virtual Mobile Ltd Confidential Information, (iv) information that was rightfully obtained from a third party who had the right to transfer or disclose it to You without limitation, or (v) any FOSS included in Hewani Portal  and accompanied by licensing terms that do not impose confidentiality obligations on the use or disclosure of such FOSS.</font> <br><br></p>
    <p><font size="2" face="Verdana"><b>10.2 Obligations Regarding Virtual Mobile Ltd Confidential Information</b></font> <br><br></p>
    <p><font size="2" face="Verdana">You agree to protect Virtual Mobile Ltd Confidential Information using at least the same degree of care that You use to protect Your own confidential information of similar importance, but no less than a reasonable degree of care. You agree to use Virtual Mobile Ltd Confidential Information solely for the purpose of exercising Your rights and performing Your obligations under this Agreement and agree not to use Virtual Mobile Ltd Confidential Information for any other purpose, for Your own or any third party's benefit, without Virtual Mobile's prior written consent. You further agree not to disclose or disseminate Virtual Mobile Ltd Confidential Information to anyone other than: (i) those of Your employees and contractors, or those of Your faculty and staff if You are an educational institution, who have a need to know and who are bound by a written agreement that prohibits unauthorized use or disclosure of the Virtual Mobile Ltd Confidential Information; or (ii) except as otherwise agreed or permitted in writing by Virtual Mobile. You may disclose Virtual Mobile Ltd Confidential Information to the extent required by law, provided that You take reasonable steps to notify Virtual Mobile Ltd of such requirement before disclosing the Virtual Mobile Ltd Confidential Information and to obtain protective treatment of the Virtual Mobile Ltd Confidential Information. You acknowledge that damages for improper disclosure of Virtual Mobile Ltd Confidential Information may be irreparable; therefore, Virtual Mobile Ltd is entitled to seek equitable relief, including injunction and preliminary injunction, in addition to all other remedies.</font> <br><br></p>
    <p><font size="2" face="Verdana"><b>10.3 Information Submitted to Virtual Mobile Ltd Not Deemed Confidential</b></font> <br><br></p>
    <p><font size="2" face="Verdana">Virtual Mobile Ltd works with many application and software developers and some of their products may be similar to or compete with Your Applications. Virtual Mobile Ltd may also be developing its own similar or competing applications and products or may decide to do so in the future. To avoid potential misunderstandings, Virtual Mobile Ltd cannot agree, and expressly disclaims, any confidentiality obligations or use restrictions, express or implied, with respect to any information that You may provide in connection with this Agreement or the Program, including information about Your Application, Licensed Application Information and metadata (such disclosures will be referred to as "Licensee Disclosures"). You agree that any such Licensee Disclosures will be non-confidential. Virtual Mobile Ltd will be free to use and disclose any Licensee Disclosures on an unrestricted basis without notifying or compensating You. You release Virtual Mobile Ltd from all liability and obligations that may arise from the receipt, review, use, or disclosure of any portion of any Licensee Disclosures. Any physical materials You submit to Virtual Mobile Ltd will become Virtual Mobile Ltd property and Virtual Mobile Ltd will have no obligation to return those materials to You or to certify their destruction.</font> <br><br></p>
    <p><font size="2" face="Verdana"><b>10.4 Press Releases and Other Publicity</b></font> <br><br></p>
    <p><font size="2" face="Verdana">You may not issue any press releases or make any other public statements regarding this Agreement, its terms and conditions, or the relationship of the parties without Virtual Mobile's express prior written approval, which may be withheld at Virtual Mobile's discretion.</font> <br><br></p>

    <p><font size="4" face="Verdana"><b>11. Indemnification</b></font> <br><br></p>
    <p><font size="2" face="Verdana">To the extent permitted by law, You agree to indemnify, defend and hold harmless Virtual Mobile, its directors, officers, employees, independent contractors and agents (each an " Virtual Mobile Ltd Indemnified Party") from any and all claims, losses, liabilities, damages, expenses and costs (including without limitation attorney’s fees and court costs) (collectively "Losses") incurred by an Virtual Mobile Ltd Indemnified Party as a result of: Your breach of this Agreement; a breach of any certification, covenant, representation or warranty made by You in this Agreement; any claims that Your Applications or the distribution, sale, offer for sale, use or importation of Your Applications (whether alone or as an essential part of a combination), Licensed Application Information or metadata violate or infringe any third party intellectual property or proprietary rights; any claims arising out of Virtual Mobile's permitted use, promotion or distribution of Your Licensed Application(s), Licensed Application Information, related trademarks and logos, or images and other materials that You provide to Virtual Mobile Ltd at Virtual Mobile's request; and/or otherwise related to or arising from Your use of the Virtual Mobile Ltd Hewani Portal or services, Your Application(s), Licensed Application Information, metadata, Registered Devices, or Your development and distribution of Applications.</font> <br><br></p>
    <p><font size="2" face="Verdana">You acknowledge that neither Hewani Portal nor any services are intended for use in the development of Applications in which errors or inaccuracies in the content, data or information provided by the Application or the failure of the Application, could lead to death, personal injury, or severe physical or environmental damage, and, to the extent permitted by law, You hereby agree to indemnify, defend and hold harmless each Virtual Mobile Ltd Indemnified Party from any Losses incurred by such Virtual Mobile Ltd Indemnified Party by reason of any such use. In no event may You enter into any settlement or like agreement with a third party that affects Virtual Mobile Ltd’s rights or binds Virtual Mobile Ltd in any way, without the prior written consent of Virtual Mobile.</font> <br><br></p>

    <p><font size="4" face="Verdana"><b>12. Termination</b></font> <br><br></p>
    <p><font size="2" face="Verdana"><b>12.1 Termination</b></font> <br><br></p>
    <p><font size="2" face="Verdana">This Agreement and all rights and licenses granted by Virtual Mobile Ltd hereunder and any services provided hereunder will terminate effective immediately upon notice from Virtual Mobile Ltd:</font> <br><br></p>
    <p><font size="2" face="Verdana">(a) If You or any of Your Authorized Developers fail to comply with any term of this Agreement other than those contained in Section 10 (Confidentiality) and fail to cure such breach within 30 days after becoming aware of or receiving notice of such breach;</font> <br><br></p>
    <p><font size="2" face="Verdana">(b) If You or any of Your Authorized Developers fail to comply with the terms of Section 10;</font> <br><br></p>
    <p><font size="2" face="Verdana">(d) If You, at any time during the Term, commence an action for patent infringement against Virtual Mobile;</font> <br><br></p>
    <p><font size="2" face="Verdana">(e) If You engage, or encourage others to engage, in any fraudulent, improper, unlawful or dishonest act relating to this Agreement, including, but not limited to, embezzlement, alteration or falsification of documents, theft, inappropriate use of computer systems, bribery, or other misrepresentation of facts. Virtual Mobile Ltd may also terminate this Agreement, or suspend Your rights to use the Virtual Mobile Ltd Hewani Portal if You fail to accept any new Program Requirements or Agreement terms as described in Section 4.</font> <br><br></p>
    <p><font size="2" face="Verdana">Either party may terminate this Agreement for its convenience, for any reason or no reason, effective 30 days after providing the other party with written notice of its intent to terminate.</font> <br><br></p>

    <p><font size="4" face="Verdana"><b>13. NO WARRANTY</b></font> <br><br></p>
    <p><font size="2" face="Verdana">Hewani Portal may contain inaccuracies or errors that could cause failures or loss of data and it may be incomplete. Virtual Mobile Ltd or its licensors may provide or make available through the Virtual Mobile Ltd Software or as part of the Program, certain web-based applications, certificate-issuance services, App Store services or other services for Your use (collectively the "Services" for purposes of this Section 13 and 14). Virtual Mobile Ltd and its licensors reserve the right to change, suspend, remove, or disable access to any Services at any time without notice. In no event will Virtual Mobile Ltd or its licensors be liable for the removal of or disabling of access to any such Services. Virtual Mobile Ltd or its licensors may also impose limits on the use of or access to certain Services, in any case and without notice or liability. </font> <br><br></p>
    <p><font size="2" face="Verdana">YOU EXPRESSLY ACKNOWLEDGE AND AGREE THAT USE OF THE VIRTUAL MOBILE LTD SOFTWARE, SECURITY SOLUTION, SERVICE-RELATED SOFTWARE AND ANY SERVICES IS AT YOUR SOLE RISK AND THAT THE ENTIRE RISK AS TO SATISFACTORY QUALITY, PERFORMANCE, ACCURACY AND EFFORT IS WITH YOU. HEWANI PORTAL SECURITY SOLUTION, SERVICE-RELATED SOFTWARE AND ANY SERVICES ARE PROVIDED "AS IS" AND "AS AVAILABLE", WITH ALL FAULTS AND WITHOUT WARRANTY OF ANY KIND, AND VIRTUAL MOBILE, VIRTUAL MOBILE'S AGENTS AND VIRTUAL MOBILE LTD 'S LICENSORS (COLLECTIVELY REFERRED TO AS "VIRTUAL MOBILE" FOR THE PURPOSES OF SECTIONS 13 AND 14) HEREBY DISCLAIM ALL WARRANTIES AND CONDITIONS WITH RESPECT TO THE VIRTUAL MOBILE LTD SOFTWARE, SECURITY SOLUTION, SERVICE-RELATED SOFTWARE AND SERVICES, EITHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING WITHOUT LIMITATION THE IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, SATISFACTORY QUALITY, FITNESS FOR A PARTICULAR PURPOSE, ACCURACY, TIMELINESS, AND NON-INFRINGEMENT OF THIRD PARTY RIGHTS. VIRTUAL MOBILE LTD DOES NOT WARRANT AGAINST INTERFERENCE WITH YOUR ENJOYMENT OF HEWANI PORTAL, SERVICE RELATED SOFTWARE OR SERVICES, THAT HEWANI PORTAL., SECURITY SOLUTION, SERVICE-RELATED SOFTWARE OR SERVICES WILL MEET YOUR REQUIREMENTS, THAT THE OPERATION OF HEWANI PORTAL., SECURITY SOLUTION, SERVICE-RELATED SOFTWARE OR THE PROVISION OF SERVICES WILL BE UNINTERRUPTED, TIMELY, SECURE OR ERROR-FREE, THAT DEFECTS OR ERRORS IN HEWANI PORTAL. , SECURITY SOLUTION, SERVICE-RELATED SOFTWARE OR SERVICES WILL BE CORRECTED, OR THAT HEWANI PORTAL, SECURITY SOLUTION, SERVICE-RELATED SOFTWARE OR SERVICES WILL BE COMPATIBLE WITH FUTURE VIRTUAL MOBILE LTD PRODUCTS, SERVICES OR SOFTWARE, OR THAT ANY INFORMATION STORED OR TRANSMITTED THROUGH ANY HEWANI PORTAL, SERVICE-RELATED SOFTWARE OR SERVICES WILL NOT BE LOST, CORRUPTED OR DAMAGED. NO ORAL OR WRITTEN INFORMATION OR ADVICE GIVEN BY VIRTUAL MOBILE LTD OR AN VIRTUAL MOBILE LTD AUTHORIZED REPRESENTATIVE WILL CREATE A WARRANTY NOT EXPRESSLY STATED IN THIS AGREEMENT. SHOULD HEWANI PORTAL, SECURITY SOLUTION, SERVICE RELATED SOFTWARE OR SERVICES PROVE DEFECTIVE, YOU ASSUME THE ENTIRE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION. </font> <br><br></p>
    <p><font size="2" face="Verdana">Location data provided by any Services is for basic navigational purposes only and is not intended to be relied upon in situations where precise location information is needed or where erroneous, inaccurate or incomplete location data may lead to death, personal injury, property or environmental damage. Neither Virtual Mobile Ltd nor any of its licensors guarantees the availability, accuracy, completeness, reliability, or timeliness of location data or any other data or information displayed by any Services.</font> <br><br></p>
    <p><font size="4" face="Verdana"><b>14. LIMITATION OF LIABILITY</b></font> <br><br></p>
    <p><font size="2" face="Verdana">TO THE EXTENT NOT PROHIBITED BY LAW, IN NO EVENT WILL VIRTUAL MOBILE LTD BE LIABLE FOR PERSONAL INJURY, OR ANY INCIDENTAL, SPECIAL, INDIRECT, CONSEQUENTIAL OR PUNITIVE DAMAGES WHATSOEVER, INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF PROFITS, LOSS OF DATA, BUSINESS INTERRUPTION OR ANY OTHER COMMERCIAL DAMAGES OR LOSSES, ARISING OUT OF OR RELATED TO THIS AGREEMENT, YOUR USE OR INABILITY TO USE HEWANI PORTAL., SECURITY SOLUTION OR SERVICES, OR YOUR DEVELOPMENT EFFORTS OR PARTICIPATION IN THE PROGRAM, HOWEVER CAUSED, WHETHER UNDER A THEORY OF CONTRACT, WARRANTY, TORT (INCLUDING NEGLIGENCE), PRODUCTS LIABILITY, OR OTHERWISE, EVEN IF VIRTUAL MOBILE LTD HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, AND NOTWITHSTANDING THE FAILURE OF ESSENTIAL PURPOSE OF ANY REMEDY. </font> <br><br></p>
    <p><font size="2" face="Verdana">THIS AGREEMENT SHALL BE INTERPRETED, CONSTRUED AND ENFORCED ACCORDING TO THE LAWS OF KENYA.</font> <br><br></p>



<%--<p><font size="2" face="Verdana">Soltura Definition</font> <br><br>--%>
<%--</p>--%>
<%--<p><font size="2" face="Verdana">mChoice<sup>TM</sup> Soltura is a value--%>
<%--added services platform which enables subscribers and content providers--%>
<%--to create divers VAS facilitating the formation of focused interest--%>
<%--groups, resulting in a massive influx of messages. With mChoice<sup>TM</sup>--%>
<%--Soltura the operator enables a wide range of VAS offering subscribers�--%>
<%--an unlimited choice in diverse applications. Through a seamless set--%>
<%--of tools and controls, the operator allows technical and non technical--%>
<%--subscribers to create their own VAS applications enabling thousands--%>
<%--of such communities in their mobile network. </font> <br><br></p>--%>
<%--<p><font size="2" face="Verdana">Tag line� </font> <br><br></p>--%>
<%--<p><font size="2" face="Verdana">mChoice<sup>TM</sup> Soltura � Freedom--%>
<%--to Explore </font></p>--%>
</div>
</div>
</div>
</div>
</div>
<div style="text-align:center;">

    <form action="confirm.html" name="agreementForm" method="post">
        <%--<input type="checkbox" name="checkBoxTick" id="checkBoxTick"--%>
        <%--tabindex="1" style="border:0"/>--%>
        <%--<label for="checkBoxTick" style="margin-left:10px">I Agree</label><fmt:message--%>
        <%--key='blank'/>--%>
                                 <span>
                                     <jsp:element name="input">
                                        <jsp:attribute name="type">button</jsp:attribute>
                                        <jsp:attribute name="name">ok</jsp:attribute>
                                        <jsp:attribute name="id">ok</jsp:attribute>
                                        <jsp:attribute name="value"><fmt:message key='i.agree'/></jsp:attribute>
                                        <jsp:attribute name="class">button</jsp:attribute>
                                        <jsp:attribute name="style">width:50px;</jsp:attribute>
                                        <jsp:attribute name="onclick">submitForm()</jsp:attribute>
                                    </jsp:element>
                                     <%--<input type="button" name="ok" id="ok" value="I Agree" class="button" style="width:50px;"--%>
                                           <%--onclick="submitForm()"/>--%>
                                </span>
        <%--<span>--%>
        <%--<input type="button" name="cancel" id="cancel" value="Cancel" class="button"--%>
        <%--style="width:70px;"/>--%>
        <%--</span>--%>
    </form>
</div>
</div>
<!-- wrap -->
<div class="clear"></div>
</div>
</div>