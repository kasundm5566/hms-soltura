<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!--
(C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
All Rights Reserved.

These materials are unpublished, proprietary, confidential source code of
hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
of hSenid Software International (Pvt) Limited.

hSenid Software International (Pvt) Limited retains all title to and intellectual
property rights in these materials.
-->

<script type="text/javascript">
    function submitForm() {
        document.forms[1].submit();
    }

    function agreeStatement() {
        var checkBoxTick = document.getElementById("checkBoxTick");
        if (checkBoxTick.checked) {
            document.getElementById("ok").disabled = false
        } else {
            document.getElementById("ok").disabled = true
        }
    }

</script>
<div id="popup-terms" style="display:none;">
    <div id="body" style="width:auto;height:auto;">
        <div class="wrap">
            <div id="dashboard-widgets-wrap">
                <div id='dashboard-widgets' class='metabox-holder' style="width:550px">
                    <div id="dashboard_quick_press" class="postbox ">
                        <h3 class='hndle'><span><fmt:message key="terms.and.condition"/></span></h3>


                        <div class="inside" style="margin:20px; max-height:400px;overflow:auto;">
                            <div>
                                <p><strong>Terms of Service</strong><br /></p>
                                <p><strong>1.&nbsp; </strong><strong>Introduction</strong> </p>
                                <p><br />
                                    Idea Mart is an online platform (the "Service") provided by Dialog  Axiata PLC (the "Company"), that acts as a portal through which users (the  "Developers"/ "You") create applications (the "App(s)") for use on mobiles by other users  (the "End Users").  <br />
                                    By using the Service, you agree to be bound by the following terms  and conditions (the &quot;Terms of Service&quot;).  The Company shall not be responsible or  liable for any breaches of third party agreements by any Apps developer or  published by you.<br />
                                    The Company reserves the right to change these Terms of Service in  any way and at any time at its sole discretion. &nbsp;It is your responsibility  to review the Terms of Service from time to time so you are aware of any changes  or updates. Any new features that augment or enhance the current Service shall  also be subject to the Terms of Service. Continued use of the Service after any  such changes shall constitute your consent to such changes. Please read the  Terms of Service very carefully.<br />
                                    <br />
                                    <strong>2. Account  Terms</strong> </p>
                                <p><br />
                                    Violation of any of the Terms of Service may result in the  termination of your account without notice. The Company prohibits  inappropriate, obscene, offensive and illegal conduct and content while using  its Service—this includes both conduct and content while using its Service and  conduct and content in any product resulting from use of the Service. In using  the Service, you understand and agree that the Company is not responsible for  the content posted, shared or produced using the Service. You agree to use the  Service at your own risk. </p>
                                <p>
                                    A. You must be a natural person or a body corporate. </p>
                                <p><br />
                                    B. If you are a natural person, you must be 18 years or  older to use this Service. </p>
                                <p><br />
                                    C. You must provide valid information for the registration process in  order to access the Service. </p>
                                <p><br />
                                    D. You are responsible for maintaining the security of your account  and password. You are solely responsible for any and all use of your account  and actions taken under your account. You agree to notify the Company  immediately if you believe your account has been accessed or used without your  permission. You will also be responsible for the maintenance of any devices  used in conjunction with your account. You agree to notify the Company  immediately if you believe your device(s) may have been stolen or is otherwise  being used by a third party without your permission. The Company cannot and  will not be liable for any loss or damage from your failure to comply with this  security obligation. </p>
                                <p><br />
                                    E. You are responsible for all material you produce using the Service  and activity that occurs under your account (even when content is posted by  others who have access to your account). </p>
                                <p><br />
                                    F. You shall not use the Service for any purposes illegal or  otherwise in conflict with these Terms of Service. You must not, in the use of  the Service, violate any laws in the legal jurisdiction to which you are  subject to (including but not limited to any state, local, provincial,  regional, federal or international laws). Take special heed of copyright laws  when using the Service. </p>
                                <p><br />
                                    G.You shall not use  Content with malicious intent or falsify, manipulate, or tamper with data  related to but not limited to App usage, downloads, sessions, impressions,  click-throughs or other end-user generated data.</p>
                                <p><br />
                                    H. App name used by you must be relevant and  unique to the App being created. Branding/Trademarks need license or permission to use.<strong> </strong></p>
                                <p><br />
                                    I. Keywords used by you must be short,  relevant, attractive, memorable and unique. You must  not use other app names or company names as keywords. Keywords  have to be single words and the text field is limited to  15 characters but must be more than 2 characters.&nbsp;Once  created, keywords cannot be changed.&nbsp;Choose keywords carefully.&nbsp;For  e.g. a good keyword for an App called "Cinema updates" can be "cineup". If you  enter a keyword that is trademarked or references another app’s name or company  name, your app may be removed.</p>
                                <p><br />
                                    J. You understand and agree that the  Company holds the right to review and approve any content or App and any  Content or App will be made available in the Service only upon prior approval by  the Company, provided nothing herein shall limit your responsibility with  respect to the content or the App. You understand and agree that the Company  may take minimum 2 working days for the approval of your registration or App  from the submission date. </p>
                                <p><br />
                                    K. You  agree that the Company shall not generate any short message or distribute any  content among its subscribers or your end users between 22:00 hours to 06:00  hours or any other time decided by the Company.</p>
                                <p>L. You understand and agree that the  maintenance of the App shall be the Developer’s responsibility and the Company  may withhold or refuse to make any payment hereunder, if the Company in its  sole discretion determines that the Developer has not maintained the App to the  Company’s expected standards.</p>
                                <p><br /><strong>3. Ownership and Licenses</strong> </p>
                                <p><br />
                                    A. With the exception of Developer Content (as  defined below), all content and intellectual property available through the  Service, including, without limitation, all software code and builds relating  to Apps, all data compiled by the Company from the Service and the Apps, and  all information derived by the Company from such compiled data is the  proprietary content and property of, and is solely owned provided by, the  Company and/or is used by the Company under license. Such content is protected  by laws relating to copyright, patent, trade secret and/or other forms of  intellectual property and by other applicable laws, and the Company reserves  and retains all rights with respect thereto. The look and feel of the  Service is copyright of the Company.  The  Company hereby grants to you a non-exclusive, non-transferable, revocable,  limited license to use the Service and the content conditioned upon your  adherences to the terms and restrictions set forth herein. </p>
                                <p> <br />
                                    B. You shall not duplicate,  copy, or reuse any portion of the HTML/CSS or visual design elements related to  the content without express written permission from the Company or for any  other purpose not protected by fair use. You are authorized to  use the content as long as you abide by these Terms of Service. You agree not  to copy, alter, modify, reverse engineer, or create derivative works of the  content, including, without limitation, the Apps, in any way that violates the  use restrictions contained in these Terms of Service. Any unauthorized use of  the content may violate copyright law, trade mark law, or other applicable laws  and regulations and will result in the termination of the license granted  hereunder. Except as expressly set forth in these Terms of Service, these Terms  of Service do not, and will not be interpreted or construed to, grant to you  any license to any intellectual property rights or other proprietary rights.</p>
                                <p><br />
                                    C. Subject to the rights of  the Company in the Service itself and the content, you shall retain ownership  of: (i) all materials and content that you provide to the Company in connection  with the Apps; and (ii) any modules that you build on your own in connection  with Apps (collectively, the "Developer Content").  By using the Service, you grant to the  Company a perpetual, non-exclusive, transferable, sub-licensable, royalty-free,  worldwide license to use any Developer Content in connection with the  operation, maintenance, and optimization of the Service. </p>
                                <p><br />
                                    D. By using the Service,  you represent and warrant that you have full right and ownership of or are  otherwise legally allowed to use or license or sublicense any and all Developer  Content and other material you upload to or distribute through the Service and  that such Developer Content does not infringe any third party rights,  including, without limitation, intellectual property rights. </p>
                                <p><br />
                                    E. If the materials you use  in connection with the Service requires licensing or licensing fees in exchange  for its use, you shall be solely responsible for securing and paying for all  digital delivery licenses, mechanical licenses, any public performance  licenses, synchronization licenses and any other licenses from all copyright  owners (or their agents).</p>
                                <p> <br />
                                    <strong>4. Trademarks</strong> </p>
                                <p><br />
                                    The name and mark IDEA  MART or DIALOG or DIALOG AXIATA  and any other logos, graphics, designs,  web/page designs, and icons of the Company used in connection with the Service  are registered or unregistered trademarks, service marks or trade dress of the  Company (the &quot;Marks&quot;). You shall not use the Marks in any form or in  any media without the prior written consent of the Company. <br />
                                    You shall retain all right, title and interest in and to all of  your logos, promotional graphics and related marketing designs (collectively,  the "Developer Art"); provided, however, that Developer hereby grants to the  Company a worldwide, perpetual, royalty-free, fully sublicenseable,  non-exclusive license to use the Developer Art, as well as Developer’s  corporate and/or trade name for purposes of marketing the Company’s products  and services to third parties. </p>
                                <p><br />
                                    <strong>5. Developer’s End Users</strong> </p>
                                <p><br />
                                    Developer shall be  responsible for the End Users’ access to the Apps and the Service including the  terms of use and privacy.<br />
                                    The Developer shall communicate the price charged for the  use of the Apps and the nature and method of using the App to the End Users.</p>
                                <p><br /><strong>6</strong><strong>. Payments and Refund Terms</strong> </p>
                                <p><br />
                                    A. The Company does not  charge a fee for the registration or use of the Service to create Apps by the  Developer.</p>
                                <p><br />
                                    B. The fee chargeable from the End Users for the use of the App  shall be indicated by the Company to the Developer during the creation of an  App by the Developer. The Developer shall be responsible for communicating and  obtaining the consent of the End User for the fees charged by the Company from  the End Users for using the App.</p>
                                <p>C. The Company shall pay  seventy percent of the fees charged from the End Users for the use of an App to  the Developer. The Developer shall be  solely responsible for account details entered. Once payment is completed by  the Company, there will be no reconciliation, in case of account details are  incorrectly entered. </p>
                                <p><br />
                                    D. All  payments and charges to be made to the Licensor shall be in Sri Lanka Rupees (SLR) <br />
                                    E. Where  appropriate and legally required, all taxes applicable under this Agreement  on the&nbsp;payments made  hereunder shall be      borne or paid by  the Party charged with the tax. </p>
                                <p>F. The payments under this Agreement do not    include Value Added Taxes imposed under  the Value Added Tax Act, No 14 of  2002 and amendment thereto.  Therefore, such tax shall  be charged by  such Developer and shall be paid by the  Company only upon submission of  proof of  registration for VAT and a valid tax invoice  in terms of the Value Added Tax  Act, No 14 of 2002. </p>
                                <p>6.5 Where any payment made under this Agreement is subject to withholding  tax ("WHT"), the Company shall  make the necessary payments  under this Agreement after  deducting WHT thereon (subject to  any Direction made available by  the Company), and furnish certificate of deduction to the Developer.</p>
                                <p>6.6 In case of amendment to current taxes  and or applicability of any new /  additional taxes to this Agreement  by way of statute or by-law, the    Parties  hereto shall  follow such amending or   new legislation  or by-law from the effective    dates  indicated in such legislation. <br />
                                    <br />
                                    <strong>7. Modifications to the Service and Prices</strong> </p>
                                <p><br />
                                    A.The  Company reserves the right at any time and from time to time to modify or  discontinue—temporarily or permanently—the Service (or any part thereof) with  or without notice at any time. </p>
                                <p><br />
                                    B.Prices  of all Services, including but not limited to any monthly subscription-plan  fees, are subject to change at any time, with or without notice. Notice may be  provided at any time by posting the changes to <a href="https://ideamart.dialog.lk">https://ideamart.dialog.lk</a> </p>
                                <p><br />
                                    C.The  Company shall not be liable to You or to any third party for any modification,  price change, suspension or discontinuance of the Service or any other related  service. </p>
                                <p><br />
                                    D.From  time to time, the Company may issue an update to the Service that may add,  modify, and/or remove features. These updates may be pushed out automatically  with little or no notice, although the Company may notify you in advance of an  upcoming update, including details on what the update includes.   <br />
                                    <br />
                                    <strong>8.Deactivation and Termination</strong> </p>
                                <p><br />
                                    A. The Company may deactivate an account at your request and reserves the  right, in its sole discretion, to terminate your account or access to the  Service at any time, with or without notice or explanation, for any or no  reason, and without liability. </p>
                                <p><br />
                                    B. You  are solely responsible for properly deactivating your account. Account  deactivation requests must be submitted in writing to <strong>admin@ideamart.dialog.lk.</strong> Deactivations by phone or sent to any other email address will not be  considered valid. </p>
                                <p><br />
                                    C. The  Company has the right to suspend or terminate your account and refuse any and  all current or future use of the Service or that of any other service provided  by the Company for any reason, at any time, with or without notice, and without  any refund of monies paid. Such termination of service will result in the  deactivation or termination of your account. You will no longer be able to  access your account and any Content stored with the Service in relation to the  account. </p>
                                <p><br />
                                    D. The  Company reserves the right to refuse service to anyone for any reason at any  time. Such refusal may include, but is not limited to, Apps involving materials  that the Company determines to be violent, obscene, or offensive; to advocate  violent or illegal activity; to contain (or have the potential to contain) any  malware; or to contravene any law, statute, or ordinance; or to violate these Terms of Service; or to violate any third parties’ terms of service.<br />
                                    <br />
                                    <strong>9. Privacy</strong> </p>
                                <p><br />
                                    The privacy of users  is important to the Company and you shall keep all and any information of the  users of the app and the content in strict confidence and shall not disclose  unless required by a court of law or government authority with competent  jurisdiction.</p>
                                <p><br />
                                    <strong>10. Quality of Service</strong> </p>
                                <p><br />
                                    While reasonable efforts  are made to keep the Service accurate and current, the Company assumes no  liability for any inaccuracies in the Service or any of its services or for any  damages that may result from the use of information posted to the Service.</p>
                                <p> <br />
                                    A.You  understand and agree that your use of the Service is at your own risk and on an  as-is basis. </p>
                                <p><br />
                                    B.The  Company does <u>not</u> warrant that: <br />
                                    - The Service will meet  your specific requirements; <br />
                                    - The Service will be  uninterrupted, timely, secure or error-free; <br />
                                    - The results that may be  obtained from the use of the Service will be accurate or reliable; <br />
                                    - The quality of any  products, services, information, or other material purchased or obtained by you  through the Service will meet your expectations and any errors in the Service  will be corrected. </p>
                                <p><br />
                                    C. At  no time is the Company obligated to issue a monetary refund, nor is it liable  for damages in connection with any use of the Service or any related defect,  perceived or real, in the services rendered. </p>
                                <p><br />
                                    D. You understand that the words and opinions of other Developers using the Service  are not those of the Company and the Company does not and cannot accept  responsibility of such words or opinions.<br />
                                    <strong> </strong><br />
                                    <strong>11. Indemnification</strong> </p>
                                <p><br />
                                    You agree to indemnify and hold the Company and its employees,  suppliers, licensors, agents and service providers (and its and their  successors, officers, directors, and employees) harmless from and against any  and all claims, demands, costs, liabilities, judgment, losses, expenses and  damages (including attorneys’ fees) arising out of, in connection with, or  related to: <br />
                                    - Your  use of the Service, including without limitation any problems arising from  technical difficulties (including but not limited to, the transmission of  computer viruses and the interruption of services), any fraudulent use of a  credit card or other payment method used to purchases services, or any  violation of these Terms of Service; <br />
                                    - Your  Developer Content; or<br />
                                    - Any  data, software, services or other materials that you use in connection with  your access or use of the Service, including without limitation any claim that  such data, software, services, or other materials, or any part thereof,  infringes, misappropriates, or otherwise violates any copyright, patent, trade  secret, trademark or other legal right of any third party.</p>
                                <p><br /><strong>12. Disclaimers and  Warranties</strong> </p>
                                <p><br />
                                    You expressly agree that the use of the Service is at your sole  risk. The Service, including any content, applications, or materials provided  thereunder, are provided on an &quot;as is&quot; basis and the Company hereby  expressly disclaims all representations and warranties of any kind, whether  express or implied, including, but not limited to, warranties of title,  merchantability, fitness for a particular purpose, or non infringement. Without  limiting the foregoing: (a) the Company cannot and does not guarantee any  specific results from the use of the Service and the Company specifically does  not make any claim or warranty that the Services will be uninterrupted or  error-free and assumes no responsibility for any error, omission, interruption,  deletion, defect, delay in operation or transmission, communications line  failure, theft or destruction or unauthorized access to, or alteration of, any  content or any user communication or message; (b) the Company does not  represent or warrant that applications, content, data or materials on the Service  are accurate, complete, reliable, current or error-free or that the Services  are free of viruses or other harmful components and, accordingly, you should  always exercise caution in the use and downloading or use of any such  applications, content, data or materials and use industry-recognized software  to detect and disable or block viruses, malware and other malicious code; (c) You  understand and agree that You download, access or otherwise obtain  applications, content, data and materials from the Service at your own  discretion and risk and that you are solely responsible for Your use thereof  and any damages to your mobile phone or mobile device or computer system, any  loss of data, and any other damage or harm of any kind that may result  therefrom; (d) the Company is not responsible for any problems or technical  malfunction of any mobile phone or mobile device, telephone network or lines,  computer online systems, servers or providers, computer equipment, software,  failure of any email or players due to technical problems or traffic congestion  on the internet or on any of the Service or combination thereof, including any  injury or damage to users or to any person's mobile phone or mobile device or  computer related to or resulting from participation or downloading materials in  connection with the Service; (e) under no circumstances will the Company be  responsible for any loss or damage, including personal injury or death,  resulting from use of the Service, from any user content posted on or through  the Service; and (f) the Company is not responsible for the conduct, whether  online or offline, of any user of the Service.<br />
                                    <br />
                                    To  the extent that the applicable law does not allow the exclusions and  disclaimers of warranties as set forth above, some or all of the above  exclusions and disclaimers may not apply to You, in which case all warranties  will be limited to the fullest extent permitted by applicable law.You  acknowledge that the disclaimers, limitations and waivers of liability  contained herein will survive any termination of your account(s) or any  services. The exclusions and disclaimers set forth in this section will survive  any termination or expiration of your registered user account or your use of  the Service. </p>
                                <p><br />
                                    <strong>13. </strong><strong>Limitation of liability</strong> </p>
                                <p><br />
                                    In no event will the Company, its affiliates, or its and their  respective directors, officers, employees, agents, successors and assigns be  liable for any indirect, consequential, exemplary, incidental, special or  punitive damages, including damages for lost profits or loss of data, arising  out of or resulting from, your use of the Service, even if the Company is aware  of or has been advised of the possibility of such damages. Without prejudice to  the above, the Company's aggregate liability to You for any cause whatsoever,  regardless of the form of the action, will at all times be limited to the  revenue earned by the Company through  the relevant App of the Developer during the year (or part thereof) immediately  before the claim. To the extent applicable law does not allow the exclusions  and limitations of damages as set forth above, some or all of the above  exclusions and limitations may not apply to you, in which case the Company's  liability to you will be limited to the fullest extent permitted by applicable  law. The limitations and exclusions set forth in this section will survive any  termination or expiration of your registered user account or Your use of the Service. </p>
                                <p><br />
                                    <strong>14. </strong><strong>Exclusions and  Limitations</strong> </p>
                                <p><br />
                                    Some jurisdictions do not allow the exclusion of certain  warranties or the limitation or exclusion of liability for incidental or  consequential damages. Accordingly, some of the above limitations may not apply  to You. The exclusions and limitations of liability in these Terms of Service  will apply notwithstanding any failure of essential purpose of any limited  remedy.   <br />
                                    <strong></strong><br />
                                    <strong>15. Entire Agreement and Severability</strong> </p>
                                <p><br />
                                    These  Terms of Service are the entire agreement between you and the Company with  respect to the Service, and supersede all prior or contemporaneous  communications and proposals (whether oral, written or electronic) between you  and Company with respect to the Service (including but not  limited to any prior versions of the Terms of Service.  If, any of the terms and conditions of these Terms of Service shall be or become unenforceable for any cause or  reason whatsoever, the ensuing lack of enforceability shall not affect the  other provisions hereof, and in such event the Parties hereto shall endeavour  to substitute forthwith such other enforceable provisions as will most closely  correspond to the legal and economic contents of the said terms and conditions.</p>
                                <p><br /><strong>16. General Conditions</strong> </p>
                                <p><br />
                                    A. These Terms of Service  shall be governed by the laws of Sri Lanka and any dispute or interpretation  arising out of these Terms of Service shall be referred to a court of law with  competent jurisdiction in Colombo, Sri Lanka </p>
                                <p><br />
                                    B.You understand that the Company uses third party vendors and hosting  partners to provide the necessary hardware, software, networking, storage, and  related technologies required to maintain the Service.</p>
                                <p><br />
                                    C. Technical support is only available through community  forums. The Company reserves the right to change the way it offers technical  support at any time with or without notice. </p>
                                <p><br />
                                    D. The Company may use  third party services to augment or enhance its Service. The Company is not  responsible for services offered by other companies and cannot be held liable  for their actions, including to any resulting damages, defects or failures. </p>
                                <p><br />
                                    E. You  agree not to reproduce, duplicate, copy, sell, resell or exploit any portion of  the Service, use of the Service, access to the Service, or its resulting Apps  or any other resulting product without permission.</p>
                                <p><br />
                                    F. You must not modify, reproduce, mimic, adapt or hack the Service or  modify another website so as to falsely imply or mislead that it is associated  with the Company, including any other services or brands under the Company’s  name. </p>
                                <p><br />
                                    G. You  understand that the technical processing and transmission of the Service,  including your content, may be transferred unencrypted and involve (a) transmissions  over various networks; and (b) changes to conform and adapt to technical  requirements of connecting networks or devices. </p>
                                <p><br />
                                    H. Any  verbal, physical, written or other abuse (including threats of abuse or  retribution) of any of the Company’s customers, employees, members, or officers  may result in immediate account termination. This includes abuse in any form of  communication, both online or offline. </p>
                                <p><br />
                                    I. You  may not impersonate any Company employee or suggest in any way that you are  employed by the Company. You may not represent your services or product as part  of the Service or mislead other Developers to believe you are an official  extension of the Service or any Company brand. </p>
                                <p><br />
                                    J. You are solely responsible for  compliance with local laws, if and to the extent local laws are applicable.</p>
                                <p>K. By  using the Service, you expressly consent to the Company building your App on  multiple platforms and multiple mobile operating systems with multiple data  carriers, even if these aforementioned entities are not available or known  today. </p>
                                <p><br />
                                    L. User  understands that any Apps created with the Service is through their own  volition and therefore indemnify and hold harmless the Company and all its  subsidiaries, employees, and any agent acting on their behalf from any and all  liabilities, claims, demands, or personal injury, including death that may be  sustained, due to or relating in any way but not limited to copyright  infringement, fraudulence, or trademark violation by way of creating and/or  using of any Apps created by the Company. </p>
                                <p><br />
                                    M.The  failure of the Company to exercise or enforce any right or provision of the  Terms of Service shall not constitute a waiver of such right or provision. </p>
                                <p><br />
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div style="text-align:center;">

                <form action="confirm.html" name="agreementForm" method="post">
                                 <span>
                                     <jsp:element name="input">
                                         <jsp:attribute name="type">button</jsp:attribute>
                                         <jsp:attribute name="name">ok</jsp:attribute>
                                         <jsp:attribute name="id">ok</jsp:attribute>
                                         <jsp:attribute name="value"><fmt:message key='i.agree'/></jsp:attribute>
                                         <jsp:attribute name="class">button</jsp:attribute>
                                         <jsp:attribute name="style">width:50px;</jsp:attribute>
                                         <jsp:attribute name="onclick">submitForm()</jsp:attribute>
                                     </jsp:element>
                                </span>
                </form>
            </div>
        </div>
        <!-- wrap -->
        <div class="clear"></div>
    </div>
</div>