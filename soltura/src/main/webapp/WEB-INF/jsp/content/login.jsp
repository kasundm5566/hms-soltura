<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="/popup/forgetPassword.jsp"></jsp:include>

<script type="text/javascript">
    //var count=0;
    <spring:hasBindErrors name="signin">
    setTimeout("showPopup()", 500);
    //        showPopup();
    //count=1;
    </spring:hasBindErrors>
    <%--<spring:bind path="signin">
        <c:if test="${signin.isEmailSentSuccessfully eq 'true'}">
            <c:out value="fsdffsf"></c:out>
            showPopup();
        </c:if>



        <c:choose>
                    <c:when test="${signin.isEmailSentSuccessfully == 'true'}">
                         alert('true');
                    </c:when>
                     <c:when test="${signin.isEmailSentSuccessfully == 'false'}">
                             alert('false');
                     </c:when>
                     <c:otherwise>
                            alert('no');
                     </c:otherwise>
         </c:choose>
    </spring:bind>--%>


    function formreset()
    {
        document.getElementById("UserName").value="";
        document.getElementById("Email").value="";
        document.getElementById("errorUsername").style.display="none";
        document.getElementById("errorEmail").style.display="none";
        showPopup();
    }


    function showPopup() {
        document.getElementById("popup-terms").style.display="block";
        jQuery.facebox(jQuery("#popup-terms"));
    }
</script>



<div id="header_login">
    <p id="tagline"><fmt:message key='tag.line'/></p>
    <div class="clear">&nbsp;</div>
    <div class="header_login_content">
        <div class="clear">&nbsp;</div>
        <div class="grid_1 alpha">&nbsp;</div>
        <div class="grid_8" id="welcome"></div>
        <div class="grid_8">&nbsp;</div>
        <div class="grid_1 omega">&nbsp;</div>
        <div class="clear">&nbsp;</div>
        <div class="grid_1 alpha">&nbsp;</div>
        <script type="text/javascript">
            function hideErrorMsg() {
                $("#login_error").hide("slow");
            }
        </script>

        <div class="grid_12">
            <div id="right_pane">
                <div id="login">
                    <c:if test="${SPRING_SECURITY_LAST_EXCEPTION != null}">
                        <div id="login_error" style="margin-top:-70px; margin-bottom:10px">
                            <c:set value="${sessionScope['SPRING_SECURITY_LAST_EXCEPTION'].message}" var="errorMessage"/>
                            <fmt:message key="${errorMessage}"/><br/>
                            <jsp:element name="a">
                                <jsp:attribute name="href">#</jsp:attribute>
                                <jsp:attribute name="onclick">formreset()</jsp:attribute>
                                <jsp:attribute name="title"><fmt:message key='request.new.password.tooltip'/></jsp:attribute>
                                <jsp:body><fmt:message key='lost.your.password'/></jsp:body>
                            </jsp:element>
                        </div>
                    </c:if>

                    <form action="j_spring_security_check" method="post" id="loginform" name="login" >

                        <p>
                            <label><fmt:message key='username'/><br/>
                                <jsp:element name="input">
                                    <jsp:attribute name="type">text</jsp:attribute>
                                    <jsp:attribute name="name">j_username</jsp:attribute>
                                    <jsp:attribute name="id">j_username</jsp:attribute>
                                    <jsp:attribute name="class">input</jsp:attribute>
                                    <jsp:attribute name="tabindex">1</jsp:attribute>
                                    <jsp:attribute name="onclick">hideErrorMsg()</jsp:attribute>
                                    <jsp:attribute name="value"><c:out value="${SPRING_SECURITY_LAST_USERNAME}"/></jsp:attribute>
                                </jsp:element>
                            </label>
                        </p>

                        <p>
                            <label><fmt:message key='password'/><br/>
                                <input type="password" onfocus="hideErrorMsg()" name="j_password" id="j_password"
                                       class="input" size="20"
                                       tabindex="2"/>
                            </label>
                        </p>

                        <!-- <p class="forgetmenot">
                           <input id="rememberMe" type='checkbox' name='_spring_security_remember_me' tabindex="3"
                                  class="checkbox"/>
                           <label for="rememberMe"><fmt:message key='remember.me'/></label>
                       </p> -->

                        <p class="submit">
                            <input class="submit_button" type="submit" name="login" id="submit" tabindex="4"
                                   value="<fmt:message key='login'/>"/>
                        </p>
                    </form>
                </div>
            </div>
        </div>
        <div class="grid_1 omega">&nbsp;</div>
        <div class="clear">&nbsp;</div>
    </div>
    <div class="clear">&nbsp;</div>
</div>
<script type="text/javascript">

    try {
        document.getElementById('j_username').focus();
    } catch(e) {
    }


</script>