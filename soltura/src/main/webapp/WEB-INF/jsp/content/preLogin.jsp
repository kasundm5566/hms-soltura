<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>


<c:set var="isNewUser" value="false"/>

<security:authorize ifAnyGranted="ROLE_ORCA_NEW_USER">
    <c:set var="isNewUser" value="true"/>
</security:authorize>

<c:choose>
    <c:when test="${not isNewUser}">
        <jsp:forward page="/home.html"/>
    </c:when>
    <c:otherwise>
        <%--todo : Add the email capture contex URL here --%>
         <%--<jsp:forward page="/home.html"/>--%>
        <jsp:forward page="/additionalInfo/userAdditionalDetails.html"/>
    </c:otherwise>
</c:choose>



