<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page pageEncoding="UTF-8" %>
<div id="footer_container" class="container_18">
    <div class="grid_18" id="trends">
        <div class="current">
            <ul>
            	<c:if test="${fl['SHOW_FT_LEARN_MORE']}">
					<li><a target="_blank" href="<%= hsenidmobile.orca.cpportal.util.FooterUrlConfig.getLink("soltura.footer.learn.more.link") %>">
	                    <fmt:message key='learn.more'/></a></li>
				</c:if>

				<%--<li><a href="http://virtualcity.co.ke"><fmt:message key='learn.more'/></a></li>--%>
				<%--<li><a target="_blank" href="http://virtualcity.co.ke"><fmt:message key='about.us'/></a></li>--%>
				<%--<li><a target="_blank" href="<c:url value='http://virtualcity.co.ke'/>"><fmt:message key='faq'/></a></li>--%>
				<%--<li><a target="_blank" href="http://virtualcity.co.ke"><fmt:message key='blog'/></a></li>--%>
				<%--<li><a target="_blank" href="http://virtualcity.co.ke"><fmt:message key='forum'/></a></li>--%>
				<%--<li class="footerRightCorner"><a target="_blank" href="http://www.youtube.com/user/hewanilife">--%>
				<%--<img src="<c:url value="/images/youtube-icon.gif"/>"--%>
				<%--title="youtube" alt="youTube"></a></li>--%>
				<c:if test="${fl['SHOW_FT_FB']}">
					<li class="footerRightCorner"><a target="_blank"
						href="<%= hsenidmobile.orca.cpportal.util.FooterUrlConfig.getLink("soltura.footer.facebook.link") %>"> <img
							src="<c:url value="/images/facebook-icon.gif"/>" title="facebook"
							alt="facebook"></a></li>
				</c:if>
				<c:if test="${fl['SHOW_FT_TW']}">
					<li class="footerRightCorner"><a target="_blank"
						href="<%= hsenidmobile.orca.cpportal.util.FooterUrlConfig.getLink("soltura.footer.twitter.link") %>"> <img
							src="<c:url value="/images/twitter-icon.gif"/>" title="twitter"
							alt="twitter"></a></li>
				</c:if>
			</ul>
        </div>
    </div>
    <div class="grid_18">
        <div class="wrapper-footer-ie left-corner">
            <div class="right-corner">
            </div>
        </div>
        <div id="footer" class="round">
            <ul>
                <li class="first">Copyright &copy; <script> document.write(new Date().getFullYear()); </script> <a href='http://www.hsenidmobile.com'>hSenid Mobile</a> (Pvt) Ltd. All rights reserved |
                <a href='<fmt:message key="soltura.tc.tag.url"/>'><fmt:message key='soltura.tc.tag'/></a> </li>
            </ul>
            <div id="clock" class="alignright"><fmt:message key='last.login.time'/><fmt:message key='blank'/>
                <%= request.getSession().getAttribute("last-login-time") == null ? "-" : request.getSession().getAttribute("last-login-time") %></div>
            <div class="clear"><fmt:message key='blank'/> </div>
        </div>
    </div>
</div>