<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<script type="text/javascript">
    function logoutConfirm(url) {
        var logOutTxt = '<fmt:message key="header.logout.confirm.message" />';
        jConfirm(logOutTxt, '<fmt:message key="confirmation.dialog" />', function(r) {
            if(r == true) {
                window.location = url;
            }
        });
    }
</script>
<c:set var="logged" value="${true}"/>
<sec:authorize ifAnyGranted="ROLE_SOL_LOGIN">
    <c:set var="logged" value="${false}"/>

    <div id="header_container">
        <div class="grid_18">
            <div id="topnav">
                <div class="current">
                    <div class="home">
                        <div id="mod-navigation">
                        <ul>
                            <sec:authorize ifAnyGranted="ROLE_SOL_HEADER_MENU_REPORTING"><li><a href ='<fmt:message key="url.reporting"/>'><fmt:message key="navigation.reporting"/></a></li></sec:authorize>
                            <sec:authorize ifAnyGranted="ROLE_SOL_HEADER_MENU_APPSTORE"><li><a href='<fmt:message key="url.appstore"/>'><fmt:message key="navigation.appstore"/></a></li></sec:authorize>
                            <sec:authorize ifAnyGranted="ROLE_SOL_HEADER_MENU_PROVISIONING"><li><a href='<fmt:message key="url.provisioning"/>'><fmt:message key="navigation.provisioning"/></a></li></sec:authorize>
                        </ul>
                        </div>
                    </div>

                    <ul>
                        <li><a href="#" onclick="logoutConfirm('/<fmt:message key='home.path'/>/j_spring_security_logout')"><fmt:message key='logout'/></a></li>
                        <li><a href='<fmt:message key="url.home"/>'><fmt:message key='home'/></a></li>
                        <li><fmt:message key='logged.in.as'/>
                            <label style="padding-right:10px; font-size:10pt"><%= request.getSession().getAttribute("display-name") %></label></li>
                            <%--<li><a href="#"><fmt:message key='help'/></a></li>--%>
                        <c:if test="${fl['SHOW_LANG_SELEC_SW']}">
                        	<li><a href="<c:url value='?locale=sw'/>"><fmt:message key='locale.kenya'/></a></li>
                        </c:if>
                        <c:if test="${fl['SHOW_LANG_SELEC_EN']}">
                        	<li><a href="<c:url value='?locale=en'/>"><fmt:message key='locale.english'/></a></li>
                        </c:if>
                        <c:if test="${fl['SHOW_LANG_SELEC_BN']}">
                            <li><a href="<c:url value='?locale=bn'/>"><fmt:message key='locale.bengali'/></a></li>
                        </c:if>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</sec:authorize>
<c:if test="${logged}">
    <div id="topnav">
        <div class="current">
            <ul>
            <c:if test="${fl['SHOW_LANG_SELEC_SW']}">
                <li><a href="<c:url value='?locale=sw'/>"><fmt:message key='locale.kenya'/></a></li>
            </c:if>
			<c:if test="${fl['SHOW_LANG_SELEC_EN']}">
                <li><a href="<c:url value='?locale=en'/>"><fmt:message key='locale.english'/></a></li>
			</c:if>
            <c:if test="${fl['SHOW_LANG_SELEC_BN']}">
                    <li><a href="<c:url value='?locale=bn'/>"><fmt:message key='locale.bengali'/></a></li>
            </c:if>
            </ul>
        </div>
    </div>
</c:if>
<%--<c:if test="${logged}">--%>
<%--<div id="topnav" style="padding-bottom:6px">--%>
<%--<div class="current">--%>
<%--<ul>--%>
<%--<li>&nbsp;</li>--%>
<%--</ul>--%>
<%--</div>--%>
<%--</div>--%>
<%--</c:if>--%>
