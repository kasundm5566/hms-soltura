<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script type="text/javascript">
    function logoutConfirm(url) {
        var logOutTxt = '<fmt:message key="header.logout.confirm.message" />';
        jConfirm(logOutTxt, '<fmt:message key="confirmation.dialog" />', function(r) {
            if(r == true) {
                window.location = url;
            }
        });
    }
</script>
<c:set var="logged" value="${true}"/>
<sec:authorize ifAnyGranted="ROLE_SOL_LOGIN">
    <c:set var="logged" value="${false}"/>

    <div id="header_container">
        <div class="grid_18">
            <div id="topnav">
                <div class="current">
                    <ul>
                    <c:if test="${fl['SHOW_LANG_SELEC_SW']}">
                        <li><a href="<c:url value='?locale=sw'/>"><fmt:message key='locale.kenya'/></a></li>
					</c:if>
					<c:if test="${fl['SHOW_LANG_SELEC_EN']}">
                        <li><a href="<c:url value='?locale=en'/>"><fmt:message key='locale.english'/></a></li>
					</c:if>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</sec:authorize>
<c:if test="${logged}">
    <div id="topnav">
        <div class="current">
            <ul>
            	<c:if test="${fl['SHOW_LANG_SELEC_SW']}">
                	<li><a href="<c:url value='?locale=sw'/>"><fmt:message key='locale.kenya'/></a></li>
                </c:if>
				<c:if test="${fl['SHOW_LANG_SELEC_EN']}">
                	<li><a href="<c:url value='?locale=en'/>"><fmt:message key='locale.english'/></a></li>
                </c:if>
            </ul>
        </div>
    </div>
</c:if>