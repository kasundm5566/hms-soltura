<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<html xmlns="http://www.w3.org/1999/xhtml" lang="en-US">
<meta http-equiv="X-UA-Compatible" content="IE=7" />

<fmt:bundle basename="messages">
    <head>
        <title><fmt:message key="soltura.page.title" /></title>
        <meta http-equiv="X-UA-Compatible" content="IE=9" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name='robots' content='noindex,nofollow'/>
        <link rel='stylesheet' href='<c:url value="/css/body.css"/>' type='text/css' media='all'/>
        <link rel='stylesheet' href='<c:url value="/css/login.css"/>' type='text/css' media='all'/>
        <link rel='stylesheet' href='<c:url value="/css/tables.css"/>' type='text/css' media='all'/>
        <link rel='stylesheet' href='<c:url value="/css/grid.css"/>' type='text/css' media='all'/>
        <link rel='stylesheet' href='<c:url value="/css/manage.css"/>' type='text/css' media='all'/>
        <link rel="stylesheet" href="<c:url value="/css/facebox/facebox.css"/>" media="screen" type="text/css"/>
        <link rel="stylesheet" href="<c:url value="/css/image-stripping.css"/>" media="screen" type="text/css"/>
        <link rel="stylesheet" href="<c:url value="/css/ui/custom.css"/>" media="screen" type="text/css"/>
        <link rel="stylesheet" href="<c:url value="/css/dialog/jquery.alerts.css"/>" media="screen" type="text/css"/>
        <link rel="stylesheet" href="<c:url value="/css/ui-hint.css"/>" media="screen" type="text/css"/>

        <link rel="shortcut icon" href="<c:url value="/images/fav.ico"/>"/>
        <c:set var="css"><spring:theme code="css"/></c:set>
        <c:if test="${not empty css}">
            <link rel="stylesheet" href="<c:url value="${css}"/>" type="text/css" media='all'/>
        </c:if>

        <script type="text/javascript" src="<c:url value="/javascripts/facebox/jquery.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/javascripts/facebox/facebox.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/javascripts/application.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/javascripts/addHTMLControls.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/javascripts/orca_js.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/javascripts/validate.js"/>"></script>

        <script type="text/javascript">
            jQuery(document).ready(function($) {
                $('a[rel*=facebox]').facebox();
            });
        </script>

        <script language="JavaScript" type="text/javascript" src="<c:url value="/javascripts/datetime.js"/>"></script>
        <script language="JavaScript" type="text/javascript" src="<c:url value="/javascripts/jquery.bgiframe.js"/>"></script>
        <script language="JavaScript" type="text/javascript" src="<c:url value="/javascripts/dialog/jquery.alerts.js"/>"></script>

        <!--[if lte IE 7]>
        <link rel='stylesheet' id='ie-css' href='<c:url value="/css/ie.css"/>' type='text/css'
              media='all'/>
        <![endif]-->
    </head>

    <body>
    <div id="brand-logo"></div>
    <div id="container" class="container_18">
        <tiles:insert attribute="header" flush="false"/>
        <![if !IE]>
        <div id="header" class="grid_18">
            <![endif]>
            <!--[if IE]>
            <div id="header_ie" class="grid_18"/>
            <![endif]-->
            <tiles:insert attribute="subHeader" flush="false"/>
            <tiles:insert attribute="body" flush="false"/>
        </div>
        <tiles:insert attribute="footer" flush="false"/>
    </div>
    </body>
</fmt:bundle>
</html>
