<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-US">
<fmt:bundle basename="messages">
    <head>
        <title><fmt:message key="soltura.error.page.title" /></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name='robots' content='noindex,nofollow'/>
        <link rel="shortcut icon" href="/images/fav.ico"/>
        <c:set var="css"><spring:theme code="css"/></c:set>
        <c:if test="${not empty css}">
            <link rel="stylesheet" href="<c:url value="${css}"/>" type="text/css" />
        </c:if>
        <link rel='stylesheet' href='<c:url value="/css/body.css"/>' type='text/css' media='all'/>
        <link rel='stylesheet' href='<c:url value="/css/login.css"/>' type='text/css' media='all'/>
        <link rel='stylesheet' href='<c:url value="/css/tables.css"/>' type='text/css' media='all'/>
        <link rel='stylesheet' href='<c:url value="/css/grid.css"/>' type='text/css' media='all'/>
        <link rel='stylesheet' href='<c:url value="/css/manage.css"/>' type='text/css' media='all'/>
        <link rel="stylesheet" href="<c:url value="/css/facebox/facebox.css"/>" media="screen" type="text/css"/>
        <link rel="stylesheet" href="<c:url value="/css/image-stripping.css"/>" media="screen" type="text/css"/>
        <link rel="stylesheet" href="<c:url value="/css/ui/custom.css"/>" media="screen" type="text/css"/>
        <link rel="stylesheet" href="<c:url value="/css/dialog/jquery.alerts.css"/>" media="screen" type="text/css"/>
        <link rel="stylesheet" href="<c:url value="/css/ui-hint.css"/>" media="screen" type="text/css"/>

        <%--<link rel='stylesheet' href='/css/body.css' type='text/css' media='all'/>--%>
        <%--<link rel='stylesheet' href='/css/login.css' type='text/css' media='all'/>--%>
        <%--<link rel='stylesheet' href='/css/tables.css' type='text/css' media='all'/>--%>
        <%--<link rel='stylesheet' href='/css/grid.css' type='text/css' media='all'/>--%>
        <%--<link rel='stylesheet' href='/css/manage.css' type='text/css' media='all'/>--%>
        <%--<link rel="stylesheet" href="/css/ui/custom.css" media="screen" type="text/css"/>--%>
    </head>

    <body>

    <div id="container" class="container_18">
        <div id="topnav">
            <div class="current">
                <ul>
                    <li></li>
                </ul>
            </div>

        </div>
        <![if !IE]>
        <div id="header" class="grid_18"><![endif]>
            <div id="popup-terms" style="display:none;">
                <div id="body" style="width:auto;height:auto;">
                    <div class="wrap">

                        <div id="dashboard-widgets-wrap">
                            <div id='dashboard-widgets' class='metabox-holder' style="width:420px">
                                <div id="dashboard_quick_press" class="postbox ">
                                    <h3 class='hndle'></h3>

                                    <div class="inside" style="margin:5px; max-height:430px;overflow:auto;">


                                        <div class="clear">&nbsp;</div>
                                        <div style="text-align:center;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clear">&nbsp;</div>
                        </div>
                    </div>
                    <!-- wrap -->

                    <div class="clear"></div>
                </div>
            </div>

            <div id="header_login">
                <p id="tagline"><br/><br/></p>
                <div class="clear">&nbsp;</div>
                <div class="header_login_content" id="error-image">
                    <div class="clear">&nbsp;</div>
                    <div id="error-message">
                        <fmt:message key="error.page.main.heading"/>
                    </div>
                    <div style="padding-bottom:10px; padding-top:10px; text-align:center;"><label><fmt:message key="error.page.sub.heading"/> </label></div>
                    <div style="text-align:center;"><a href="#" onClick="location.href='../home.html';"><fmt:message key="error.page.return.to.home"/> </a></div>
                    <div class="clear">&nbsp;</div>

                </div>
                <div class="clear">&nbsp;</div>
            </div>
        </div>

        <div id="footer_container" class="container_18">
            <div class="grid_18" id="trends">
                <div class="current">
                    <ul>

                        <li><a href="http://www.hsenidmobile.com/Soltura.php">Learn More</a></li>
                        <li><a target="_blank" href="http://www.hsenidmobile.com/company_profile.html">About Us</a></li>


                        <li><a target="_blank" href="http://soltura.hsenidmobile.com/">Blog</a></li>
                        <li><a target="_blank" href="http://soltura.hsenidmobile.com/">Forum</a></li>
                    </ul>
                </div>
                <div class="clear"></div>

            </div>
            <div class="grid_18">
                <div id="footer" class="round">
                    <ul>
                        <li class="first">Copyright &copy; 2011 <a href='"http://www.hsenidmobile.com"'>hSenid Mobile</a> (Pvt) Ltd. All rights reserved.</li>
                        <li><a target="_blank" href="http://www.hsenidmobile.com/contact_us.html">Contact Us</a></li>

                        <li><a href="#"><fmt:message key="terms.and.condition" /></a></li>
                        <li><a href="#"><fmt:message key="privacy.policy" /></a></li>
                    </ul>
                    <div id="clock" class="alignright">&nbsp;</div>
                    <div class="clear">&nbsp; </div>
                </div>
                <div class="clear"></div>

            </div>
        </div>

    </div>
    </body>
</fmt:bundle>
</html>
