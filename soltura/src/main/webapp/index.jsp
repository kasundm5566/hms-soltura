
<%--(C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
All Rights Reserved.

These materials are unpublished, proprietary, confidential source code of
hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
of hSenid Software International (Pvt) Limited.

hSenid Software International (Pvt) Limited retains all title to and intellectual
property rights in these materials.--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>


<sec:authorize ifAllGranted="ROLE_SOL_LOGIN">
    <c:redirect url="/home.html"/>
</sec:authorize>
                                                      
<%--<sec:authorize ifNotGranted="ROLE_LOGIN_REP">--%>
    <%--<jsp:forward page="accessDenied.jsp"/>--%>
<%--</sec:authorize>--%>