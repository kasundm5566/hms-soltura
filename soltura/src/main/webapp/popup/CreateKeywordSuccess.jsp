<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!--
(C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
All Rights Reserved.

These materials are unpublished, proprietary, confidential source code of
hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
of hSenid Software International (Pvt) Limited.

hSenid Software International (Pvt) Limited retains all title to and intellectual
property rights in these materials.
-->

<script type="text/javascript">
    function submitForm() {
        document.forms[0].submit();
    }

    function agreeStatement() {
        var checkBoxTick = document.getElementById("checkBoxTick");
        if (checkBoxTick.checked) {
            document.getElementById("ok").disabled = false
        } else {
            document.getElementById("ok").disabled = true
        }
    }
</script>

<div id="popup-terms" style="display:none;">
    <div id="body" style="width:auto;height:auto;">
        <div class="wrap">
            <div id="dashboard-widgets-wrap">
                <div id='dashboard-widgets' class='metabox-holder' style="width:550px">
                    <div id="dashboard_quick_press" class="postbox ">
                        <h3 class='hndle'><span><fmt:message key="keyward.create.success"/></span></h3>
                        <div class="inside" style="margin:20px; max-height:400px;overflow:auto;">
                            <div>
                                <br>
                                <p><font size="2" face="Verdana"><b><fmt:message key="keyward.create.success.message"/></b></font> <br><br></p>
                            </div>

                            <div style="text-align:center;">
                                <form action="submit/${routingInfo.appLevel}.html" name="agreementForm" method="post">
                                     <span>
                                         <jsp:element name="input">
                                            <jsp:attribute name="type">button</jsp:attribute>
                                            <jsp:attribute name="name">ok</jsp:attribute>
                                            <jsp:attribute name="id">ok</jsp:attribute>
                                            <jsp:attribute name="value"><fmt:message key='ok'/></jsp:attribute>
                                            <jsp:attribute name="class">button</jsp:attribute>
                                            <jsp:attribute name="style">width:50px;</jsp:attribute>
                                            <jsp:attribute name="onclick">submitForm()</jsp:attribute>
                                        </jsp:element>
                                    </span>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- wrap -->
        <div class="clear"></div>
    </div>
</div>