<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!--
(C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
All Rights Reserved.

These materials are unpublished, proprietary, confidential source code of
hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
of hSenid Software International (Pvt) Limited.

hSenid Software International (Pvt) Limited retains all title to and intellectual
property rights in these materials.
-->

<script type="text/javascript">
    function sendMessage() {
        var parameters = $(document.forms[0]).serialize();
        var val = document.getElementById("messageInput").value;
        alert(val);
        $.ajax({
            type: "POST",
            context: document.body,
            async: false,
            cache: false,
            url: "../viewRequestReport.html",
            data : parameters
        });
        $(document).trigger('close.facebox');
        return false;
    }
</script>

<div id="popup-terms" style="display:none;">
    <div id="body" style="width:auto;height:auto;">
        <div class="wrap">
            <form id="massageForm" action="/" method="post">
                <div id="dashboard-widgets-wrap">
                    <div id='dashboard-widgets' class='metabox-holder' style="width:550px">
                        <div id="dashboard_quick_press" class="postbox ">
                            <h3 class='hndle'><span><fmt:message key="reply.msg"/> </span></h3>
                            <div class="inside" style="margin:20px; max-height:200px;overflow:auto;">
                                <div>
                                    <div style="width:380px;">
                                        <div style="width:270px; float:right; display:none;">
                                            <input type="text" name="receiver" id="receiverField" value="" />
                                            <input type="text" name="appId" id="appIdField" value="" />
                                        </div>
                                        <div style="width:270px; float:right;">
                                            <textarea cols="3" rows="3" name="messageInput" id="messageInput" tabindex="1"
                                                      style="width:345px;border-color:#C2C7CD;">type</textarea>

                                        </div>
                                    </div>
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="text-align:center;">
                    <span>
                        <jsp:element name="input">
                            <jsp:attribute name="type">button</jsp:attribute>
                            <jsp:attribute name="name">send</jsp:attribute>
                            <jsp:attribute name="id">sendReply</jsp:attribute>
                            <jsp:attribute name="value"><fmt:message key='send'/></jsp:attribute>
                            <jsp:attribute name="class">submit_button</jsp:attribute>
                            <jsp:attribute name="onclick">sendMessage()</jsp:attribute>
                            <jsp:attribute name="style">width:50px</jsp:attribute>
                        </jsp:element>
                    </span>
                </div>
            </form>
        </div>
        <!-- wrap -->
        <div class="clear"></div>
    </div>
</div>