/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hsenidmobile.orca.cpportal.filter;

import hsenidmobile.orca.core.model.RoutingInfo;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class AvailableRoutingKeyFilterTest {

    private AvailableRoutingKeyFilter availableRoutingKeyFilter;
    RoutingInfo routingInfo;

    @Before
    public void setup() throws Exception {
        routingInfo = new RoutingInfo();
        availableRoutingKeyFilter = new AvailableRoutingKeyFilter();
        Calendar clCalendar = Calendar.getInstance();
        clCalendar.add(Calendar.DAY_OF_MONTH, -1);
        routingInfo.setEndDate(clCalendar.getTimeInMillis());

    }

    @Test
    public void testAvailableRoutingKeyFilter() {
        assertTrue(availableRoutingKeyFilter.match(routingInfo));
        Calendar clCalendar = Calendar.getInstance();
        clCalendar.add(Calendar.DAY_OF_MONTH, +1);
        routingInfo.setEndDate(clCalendar.getTimeInMillis());
        assertFalse(availableRoutingKeyFilter.match(routingInfo));
    }
}
