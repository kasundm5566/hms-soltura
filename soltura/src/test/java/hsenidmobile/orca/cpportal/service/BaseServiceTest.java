/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 */

package hsenidmobile.orca.cpportal.service;

import hsenidmobile.orca.core.applications.PeriodUnit;
import hsenidmobile.orca.core.applications.subscription.DefaultDispatchConfiguration;
import hsenidmobile.orca.core.model.ContentProvider;
import hsenidmobile.orca.core.model.CpFinancialInstrument;
import hsenidmobile.orca.core.model.RoutingKey;
import hsenidmobile.orca.cpportal.module.ApplicationInfo;
import hsenidmobile.orca.orm.repository.ContentProviderDetailsRepositoryImpl;
import hsenidmobile.orca.rest.common.Response;
import hsenidmobile.orca.rest.common.ResponseStatus;
import hsenidmobile.orca.rest.pg.transport.PaymentGatewayRequestSender;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Tester class for [BaseService].
 */
public class BaseServiceTest extends BaseService {

    ApplicationInfo applicationInfo = new ApplicationInfo();
    private static final PeriodUnit PERIOD_MONTH = PeriodUnit.MONTH;
    private Mockery context;
    private ContentProvider contentProvider;
    private Map<String, DefaultDispatchConfiguration> defaultDispathTimeValues;
    private Map<String, Integer> subscriptionBufferTimeValues;
    private PaymentGatewayRequestSender paymentGatewayRequestSender;
    private static Map<String, String> availableFinanacialInstruments = new HashMap();

    static {
        availableFinanacialInstruments.put("BankA", "BankID");
        availableFinanacialInstruments.put("Mobile-WalletA", "WalletID");
    }

    @Before
    public void setUp() throws Exception {
        context = new Mockery();
        applicationInfo.setSelectedPeriodUnit(PERIOD_MONTH);
        applicationInfo.setSubscriptionDispatchDate(1);
        cpDetailsRepository = context.mock(ContentProviderDetailsRepositoryImpl.class);
        paymentGatewayRequestSender = context.mock(PaymentGatewayRequestSender.class);

        DefaultDispatchConfiguration monthConfig = DefaultDispatchConfiguration.configureForDayOfMonth(1, 8, 30);

        defaultDispathTimeValues = new HashMap<String, DefaultDispatchConfiguration>();
        defaultDispathTimeValues.put("MONTH", monthConfig);

        subscriptionBufferTimeValues = new HashMap<String, Integer>();
        subscriptionBufferTimeValues.put("MONTH", 60 * 6);
        setSubscriptionBufferTimeValues(subscriptionBufferTimeValues);

        contentProvider = new ContentProvider();
        contentProvider.setName("test");
        contentProvider.setOwnedRoutingKeys(new HashMap<String, List<RoutingKey>>());
        contentProvider.setUnassignedRoutingKeys(new HashMap<String, List<RoutingKey>>());
        contentProvider.setAvailableFinancialInstruments(new ArrayList<CpFinancialInstrument>());
    }

    @After
    public void tearDown() throws Exception {

    }

    /**
     * Test method createSubscriptionServiceData(ApplicationInfo appInfo)
     *
     * @throws Exception
     */
    //todo fix this testcase failure
//	@Test
//	public void testCreateSubscriptionServiceData() throws Exception {
//		final List<SubscriptionServiceData> dataList = createSubscriptionServiceData(applicationInfo);
//		for (SubscriptionServiceData subscriptionServiceData : dataList) {
//			assertEquals("Testing periodicity", subscriptionServiceData.getPeriodicity().getUnit(), PERIOD_MONTH);
//		}
//	}

    /**
     * Test method getContentProviderDetails(boolean
     * retriveDataFromDBIgnoreCash)
     *
     * @throws Exception
     */
//    @Test
    public void testGetContentProviderDetails() throws Exception {
        final SecurityContextImpl context1 = new SecurityContextImpl();
        final Response response = new Response();
        response.setResponseStatus(ResponseStatus.SUCCESS);
        response.setResponseParameters(availableFinanacialInstruments);
        context1.setAuthentication(new UsernamePasswordAuthenticationToken("test", "test"));
        SecurityContextHolder.setContext(context1);
        context.checking(new Expectations() {
            {
                allowing(cpDetailsRepository).findByUsername(with("test"));
                will(returnValue(contentProvider));
                allowing(paymentGatewayRequestSender).getAvailableFinancialInstruments(with(any(String.class)));
                will(returnValue(response));
            }
        });
    }

    /**
     * Test method addCpUser(Registration registration, ContentProvider
     * contentProvider)
     *
     * @throws Exception
     */
    @Test
    public void testAddCpUser() throws Exception {
        // TODO: Test goes here...
    }

    /**
     * Test method availableShortCodes(NcsType ncsType, String operator)
     */
    @Test
    public void testAvailableShortCodesForNcsTypeOperator() throws Exception {
        // TODO: Test goes here...
    }

    /**
     * Test method availableShortCodes(NcsType ncsType)
     */
    @Test
    public void testAvailableShortCodesNcsType() throws Exception {
        // TODO: Test goes here...
    }

    /**
     * Test method setKeywordDetails(List<KeywordDetails> keywordDetailses)
     */
    @Test
    public void testSetKeywordDetails() throws Exception {
        // TODO: Test goes here...
    }

    /**
     * Test method addSubscriptionKeywordDetails(List<KeywordDetails>
     * keywordDetailses)
     */
    @Test
    public void testAddSubscriptionKeywordDetails() throws Exception {
        // TODO: Test goes here...
    }

}
