
/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *   
 *   $LastChangedDate$
 *   $LastChangedBy$ 
 *   $LastChangedRevision$
 */
 
package hsenidmobile.orca.cpportal.service;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;

import static junit.framework.Assert.assertEquals;

/**
 * CurrentUser Tester.
 *
 */
public class CurrentUserTest {

    private String USER_NAME = "test";
    private String PASSWORD = "pass";

    @Before
    public void setUp() throws Exception {
        final SecurityContextImpl context = new SecurityContextImpl();
        context.setAuthentication(new UsernamePasswordAuthenticationToken(USER_NAME, PASSWORD));
        SecurityContextHolder.setContext(context);        
    }

    @After
    public void tearDown() throws Exception {

    }

    /**
     *
     * Method: getUsername()
     * @throws Exception
     */
    @Test
    public void testGetUsername() throws Exception {
        assertEquals("Testing current username", CurrentUser.getUsername(), USER_NAME);
    }

    /**
     *
     * Method: isAuthenticated(String role)
     * @throws Exception
     */
    @Test
    public void testIsAuthenticated() throws Exception {
        assertEquals("Testing is user authenticated", CurrentUser.isAuthenticated(""), false);
    }
}
