/*
 *   (C) Copyright 2008-2009 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hsenidmobile.orca.cpportal.service.impl;

import hsenidmobile.orca.core.applications.ApplicationImpl;
import hsenidmobile.orca.cpportal.service.impl.application.ApplicationService;
import junit.framework.TestCase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Locale;
import java.util.Map;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class CreateApplicationServiceTest extends TestCase {

    private static final Logger logger = LoggerFactory.getLogger(CreateApplicationServiceTest.class);


    public void testCreateApplication() {
        assertTrue(true);
    }


    public void testInitialDataGenarator() {
        logger.info("Started test on TestInitialDataGenarator");
        ApplicationService appService = new ApplicationService();
        try {
//            ApplicationImpl applicationImpl = appService.initialDataGenerator();
//            assertNotNull("applicationImpl object cannot be null", applicationImpl);
//            Map initailSubscriptionResponse = applicationImpl.getSubscriptionResponse();
//            assertNotNull("InitailSubscriptionResponse object cannot be null", initailSubscriptionResponse);
//            assertTrue("size initailSubscriptionResponse should be > 0", initailSubscriptionResponse.size() > 0);
//            assertEquals("Expected value should be blank ", initailSubscriptionResponse.get(Locale.ENGLISH), "");
//            Map unSubscribeResponse = applicationImpl.getUnsubscribeResponse();
//            assertNotNull("InitailSubscriptionResponse object cannot be null", unSubscribeResponse);
//            assertTrue("size initailSubscriptionResponse should be > 0", unSubscribeResponse.size() > 0);
//            assertEquals("Expected value should be blank ", unSubscribeResponse.get(Locale.ENGLISH), "");
//            Map initialInvalidRequestErrorMessage = applicationImpl.getInvalidRequestErrorMessage();
//            assertNotNull("InitailSubscriptionResponse object cannot be null", initialInvalidRequestErrorMessage);
//            assertTrue("size initailSubscriptionResponse should be > 0", initialInvalidRequestErrorMessage.size() > 0);
//            assertEquals("Expected value should be blank ", initialInvalidRequestErrorMessage.get(Locale.ENGLISH), "");

            logger.info("test on TestInitialDataGenarator success!!");
        } catch (Exception e) {
            logger.error("Error while testing TestInitialDataGenarator", e);
        }

    }
}