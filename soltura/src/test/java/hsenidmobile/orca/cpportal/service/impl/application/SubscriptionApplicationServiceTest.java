/*
 *   (C) Copyright 2008-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hsenidmobile.orca.cpportal.service.impl.application;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import hsenidmobile.orca.core.applications.PeriodUnit;
import hsenidmobile.orca.core.applications.subscription.DefaultDispatchConfiguration;
import hsenidmobile.orca.core.applications.subscription.Periodicity;
import hsenidmobile.orca.cpportal.module.ContentKeywordDetail;
import hsenidmobile.orca.cpportal.module.ContentKeywordDetail.ContentStatus;
import hsenidmobile.orca.cpportal.module.KeywordDetails;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.DateTimeUtils;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class SubscriptionApplicationServiceTest {

	private SubscriptionApplicationService service;

	@Before
	public void setUp() {
		service = new SubscriptionApplicationService();
	}

	@Test
	@Ignore //TODO: Fix the test case
	public void testCreateNewContentKeywordDetailWhenCurrentlyNoContentAvailable() throws Exception {
		// Change system time
		DateTimeUtils.setCurrentMillisFixed(new DateTime(2010, 9, 8, 7, 50, 0, 0).getMillis());

		Periodicity subscriptionPeriodicity = new Periodicity(PeriodUnit.DAY, 1, 30);
		subscriptionPeriodicity.setDefaultDispatchConfiguration(DefaultDispatchConfiguration.configureForDay(8, 30));
		subscriptionPeriodicity.setFirstScheduledDispatchTime(new DateTime(Periodicity.getFistDispatchableDate(PeriodUnit.DAY, 1,
				subscriptionPeriodicity.getDefaultDispatchConfiguration())));

		List<String> availableSubKeywords = Arrays.asList("subkey0");
		ContentKeywordDetail newContent = service.createNewContentKeywordDetail(subscriptionPeriodicity,
				availableSubKeywords, null);
		assertNotNull("new content cannot be null", newContent);
		assertEquals("number of sub keys", 1, newContent.getDetails().size());
		assertEquals("sub key", "subkey0", newContent.getDetails().get(0).getKeyword());
		assertEquals("status", ContentStatus.NEW, newContent.getStatus());
		assertEquals("schedule time", new DateTime(2010, 9, 8, 8, 30, 0, 0).toDate(), newContent.getScheduledDate());
		assertEquals("formated time string", "Sep 8, 2010 8:30:00 AM", newContent.getShowPeriod());

		newContent = service.createNewContentKeywordDetail(subscriptionPeriodicity, Collections.EMPTY_LIST, null);
		assertNotNull("new content cannot be null", newContent);
		assertEquals("number of sub keys", 1, newContent.getDetails().size());
		assertEquals("no sub key", "", newContent.getDetails().get(0).getKeyword());
		assertEquals("schedule time", new DateTime(2010, 9, 8, 8, 30, 0, 0).toDate(), newContent.getScheduledDate());
		assertEquals("formated time string", "Sep 8, 2010 8:30:00 AM", newContent.getShowPeriod());

		DateTimeUtils.setCurrentMillisFixed(new DateTime(2010, 10, 8, 7, 50, 0, 0).getMillis());
		newContent = service.createNewContentKeywordDetail(subscriptionPeriodicity, availableSubKeywords, null);
		assertNotNull("new content cannot be null", newContent);
		assertEquals("number of sub keys", 1, newContent.getDetails().size());
		assertEquals("sub key", "subkey0", newContent.getDetails().get(0).getKeyword());
		assertEquals("schedule time", new DateTime(2010, 10, 8, 8, 30, 0, 0).toDate(), newContent.getScheduledDate());
		assertEquals("formated time string", "Oct 8, 2010 8:30:00 AM", newContent.getShowPeriod());
	}

	/**
	 * Pre-conditions for this test scenario.
	 *
	 * Assume current time is 9.14 and app is Hourly app scheduled to sent in
	 * 10th minute and buffer is 10mins.
	 *
	 * Last scheduled content by the user if for 9.10 and now it is sent.
	 *
	 * @throws Exception
	 */
	@Test
	public void testCreateNewContentWithinBufferTimeOfLastSentContentAndLastSentIsTheLastOfScheduledContent()
			throws Exception {
		// Change system time
		DateTimeUtils.setCurrentMillisFixed(new DateTime(2010, 9, 8, 9, 14, 0, 0).getMillis());

		Periodicity subscriptionPeriodicity = new Periodicity(PeriodUnit.HOUR, 1, 10);
		subscriptionPeriodicity.setDefaultDispatchConfiguration(DefaultDispatchConfiguration.configureForHour(10));
		subscriptionPeriodicity.setFirstScheduledDispatchTime(new DateTime(2010, 9, 7, 7, 10, 0, 0));
		subscriptionPeriodicity.setLastSentTime(new DateTime(2010, 9, 8, 9, 14, 0, 0));

		List<String> availableSubKeywords = Arrays.asList("subkey0");
		ContentKeywordDetail newContent = service.createNewContentKeywordDetail(subscriptionPeriodicity,
				availableSubKeywords, null);
		assertNotNull("new content cannot be null", newContent);
		assertEquals("number of sub keys", 1, newContent.getDetails().size());
		assertEquals("sub key", "subkey0", newContent.getDetails().get(0).getKeyword());
		assertEquals("status", ContentStatus.NEW, newContent.getStatus());
		// Even though current time 9.14 is with in buffer and no last scheduled
		// content is available(all have being sent) schedule time of new
		// content should be 10.10 not 9.10 since content for 9.10 is already
		// sent
		assertEquals("schedule time", new DateTime(2010, 9, 8, 10, 10, 0, 0).toDate(), newContent.getScheduledDate());
		assertEquals("formated time string", "Sep 8, 2010 10:10:00 AM", newContent.getShowPeriod());

	}

	@Test
	public void testCreateNewContentKeywordDetailWhenContentAvailable() throws Exception {
		// Change system time
		DateTimeUtils.setCurrentMillisFixed(new DateTime(2010, 9, 8, 7, 50, 0, 0).getMillis());

		Periodicity subscriptionPeriodicity = new Periodicity(PeriodUnit.DAY, 1, 30);
		subscriptionPeriodicity.setDefaultDispatchConfiguration(DefaultDispatchConfiguration.configureForDay(8, 30));
		subscriptionPeriodicity.setFirstScheduledDispatchTime(new DateTime(Periodicity.getFistDispatchableDate(PeriodUnit.DAY, 1,
				subscriptionPeriodicity.getDefaultDispatchConfiguration())));

		ContentKeywordDetail contentKeywordDetail = new ContentKeywordDetail();
		contentKeywordDetail.setScheduledDate(new DateTime(2010, 9, 9, 8, 30, 0, 0).toDate());
		List<String> availableSubKeywords = Arrays.asList("subkey0");

		ContentKeywordDetail newContent = service.createNewContentKeywordDetail(subscriptionPeriodicity,
				availableSubKeywords, contentKeywordDetail);
		assertNotNull("new content cannot be null", newContent);
		assertEquals("number of sub keys", 1, newContent.getDetails().size());
		assertEquals("sub key", "subkey0", newContent.getDetails().get(0).getKeyword());
		assertEquals("schedule time", new DateTime(2010, 9, 10, 8, 30, 0, 0).toDate(), newContent.getScheduledDate());

		newContent = service.createNewContentKeywordDetail(subscriptionPeriodicity, Collections.EMPTY_LIST,
				contentKeywordDetail);
		assertNotNull("new content cannot be null", newContent);
		assertEquals("number of sub keys", 1, newContent.getDetails().size());
		assertEquals("no sub key", "", newContent.getDetails().get(0).getKeyword());
		assertEquals("schedule time", new DateTime(2010, 9, 10, 8, 30, 0, 0).toDate(), newContent.getScheduledDate());
	}

	@Test
	public void testCreateNewContentKeywordDetailWhenContentAvailableAndOutdated() throws Exception {
		// Change system time
		DateTimeUtils.setCurrentMillisFixed(new DateTime(2010, 9, 10, 7, 50, 0, 0).getMillis());

		Periodicity subscriptionPeriodicity = new Periodicity(PeriodUnit.DAY, 1, 30);
		subscriptionPeriodicity.setDefaultDispatchConfiguration(DefaultDispatchConfiguration.configureForDay(8, 30));
		subscriptionPeriodicity.setFirstScheduledDispatchTime(new DateTime(Periodicity.getFistDispatchableDate(PeriodUnit.DAY, 1,
				subscriptionPeriodicity.getDefaultDispatchConfiguration())));

		ContentKeywordDetail contentKeywordDetail = new ContentKeywordDetail();
		contentKeywordDetail.setScheduledDate(new DateTime(2010, 9, 9, 8, 30, 0, 0).toDate());
		List<String> availableSubKeywords = Arrays.asList("subkey0");

		ContentKeywordDetail newContent = service.createNewContentKeywordDetail(subscriptionPeriodicity,
				availableSubKeywords, contentKeywordDetail);
		assertNotNull("new content cannot be null", newContent);
		assertEquals("number of sub keys", 1, newContent.getDetails().size());
		assertEquals("sub key", "subkey0", newContent.getDetails().get(0).getKeyword());
		assertEquals("schedule time", new DateTime(2010, 9, 10, 8, 30, 0, 0).toDate(), newContent.getScheduledDate());

		newContent = service.createNewContentKeywordDetail(subscriptionPeriodicity, availableSubKeywords, newContent);
		assertNotNull("new content cannot be null", newContent);
		assertEquals("number of sub keys", 1, newContent.getDetails().size());
		assertEquals("sub key", "subkey0", newContent.getDetails().get(0).getKeyword());
		assertEquals("schedule time", new DateTime(2010, 9, 11, 8, 30, 0, 0).toDate(), newContent.getScheduledDate());

		newContent = service.createNewContentKeywordDetail(subscriptionPeriodicity, Collections.EMPTY_LIST,
				contentKeywordDetail);
		assertNotNull("new content cannot be null", newContent);
		assertEquals("number of sub keys", 1, newContent.getDetails().size());
		assertEquals("no sub key", "", newContent.getDetails().get(0).getKeyword());
		assertEquals("schedule time", new DateTime(2010, 9, 10, 8, 30, 0, 0).toDate(), newContent.getScheduledDate());
	}

	@Test
	@Ignore //TODO: Fix the test case
	public void testCreateNewContentKeywordDetailListWhenNoCurrentContentAvailable() throws Exception {
		// Change system time
		DateTimeUtils.setCurrentMillisFixed(new DateTime(2010, 9, 8, 7, 50, 0, 0).getMillis());

		Periodicity subscriptionPeriodicity = new Periodicity(PeriodUnit.DAY, 1, 30);
		subscriptionPeriodicity.setDefaultDispatchConfiguration(DefaultDispatchConfiguration.configureForDay(8, 30));
		subscriptionPeriodicity.setFirstScheduledDispatchTime(new DateTime(Periodicity.getFistDispatchableDate(PeriodUnit.DAY, 1,
				subscriptionPeriodicity.getDefaultDispatchConfiguration())));

		int numberOfNewContentsRequired = 4;
		List<String> availableSubKeywords = Arrays.asList("subkey0");
		List<ContentKeywordDetail> newContentList = service.createNewContentKeywordDetailList(subscriptionPeriodicity,
				availableSubKeywords, null, numberOfNewContentsRequired);
		assertNotNull("new contents list cannot be null", newContentList);
		assertEquals("new contents list size", 4, newContentList.size());
		assertEquals("number of sub keys in content 1", 1, newContentList.get(0).getDetails().size());
		assertEquals("sub key in content 1", "subkey0", newContentList.get(0).getDetails().get(0).getKeyword());
		assertEquals("schedule time in content 1", new DateTime(2010, 9, 8, 8, 30, 0, 0).toDate(), newContentList
				.get(0).getScheduledDate());

		assertEquals("number of sub keys in content 4", 1, newContentList.get(3).getDetails().size());
		assertEquals("sub key in content 4", "subkey0", newContentList.get(3).getDetails().get(0).getKeyword());
		assertEquals("schedule time in content 4", new DateTime(2010, 9, 11, 8, 30, 0, 0).toDate(),
				newContentList.get(3).getScheduledDate());
	}

	@Test
	public void testCreateNewContentKeywordDetailListWhenCurrentContentAvailable() throws Exception {
		// Change system time
		DateTimeUtils.setCurrentMillisFixed(new DateTime(2010, 9, 8, 7, 50, 0, 0).getMillis());

		Periodicity subscriptionPeriodicity = new Periodicity(PeriodUnit.DAY, 1, 30);
		subscriptionPeriodicity.setDefaultDispatchConfiguration(DefaultDispatchConfiguration.configureForDay(8, 30));
		subscriptionPeriodicity.setFirstScheduledDispatchTime(new DateTime(Periodicity.getFistDispatchableDate(PeriodUnit.DAY, 1,
				subscriptionPeriodicity.getDefaultDispatchConfiguration())));

		ContentKeywordDetail contentKeywordDetail = new ContentKeywordDetail();
		contentKeywordDetail.setScheduledDate(new DateTime(2010, 9, 9, 8, 30, 0, 0).toDate());
		int numberOfNewContentsRequired = 4;
		List<String> availableSubKeywords = Arrays.asList("subkey0");
		List<ContentKeywordDetail> newContentList = service.createNewContentKeywordDetailList(subscriptionPeriodicity,
				availableSubKeywords, contentKeywordDetail, numberOfNewContentsRequired);
		assertNotNull("new contents list cannot be null", newContentList);
		assertEquals("new contents list size", 4, newContentList.size());

		assertEquals("number of sub keys in content 1", 1, newContentList.get(0).getDetails().size());
		assertEquals("sub key in content 1", "subkey0", newContentList.get(0).getDetails().get(0).getKeyword());
		assertEquals("schedule time in content 1", new DateTime(2010, 9, 10, 8, 30, 0, 0).toDate(),
				newContentList.get(0).getScheduledDate());
		assertEquals("formated time string", "Sep 10, 2010 8:30:00 AM", newContentList.get(0).getShowPeriod());

		assertEquals("number of sub keys in content 4", 1, newContentList.get(3).getDetails().size());
		assertEquals("sub key in content 4", "subkey0", newContentList.get(3).getDetails().get(0).getKeyword());
		assertEquals("schedule time in content 4", new DateTime(2010, 9, 13, 8, 30, 0, 0).toDate(),
				newContentList.get(3).getScheduledDate());
		assertEquals("formated time string", "Sep 13, 2010 8:30:00 AM", newContentList.get(3).getShowPeriod());
	}
 //to do fix this test case
//	@Test
//	public void testGetEditableContentForAppWithSubkeys() throws Exception {
//		DateTimeUtils.setCurrentMillisFixed(new DateTime(2010, 9, 8, 7, 50, 0, 0).getMillis());
//
//		SubscriptionServiceData serviceData = new SubscriptionServiceData();
//		Periodicity subscriptionPeriodicity = new Periodicity(PeriodUnit.DAY, 1, 30);
//		subscriptionPeriodicity.setDefaultDispatchConfiguration(DefaultDispatchConfiguration.configureForDay(8, 30));
//		subscriptionPeriodicity.setFirstScheduledDispatchTime(new DateTime(Periodicity.getFistDispatchableDate(PeriodUnit.DAY, 1,
//				subscriptionPeriodicity.getDefaultDispatchConfiguration())));
//		serviceData.setPeriodicity(subscriptionPeriodicity);
//		ContentGroup contentGroup = new ContentGroup();
//		contentGroup.addContent(new KeywordDetail("subkey0", "Sample content"));
//		contentGroup.setScheduledDispathTime(new DateTime(2010, 9, 10, 8, 30, 0, 0).toDate());
//
//		serviceData.setData(Arrays.asList(contentGroup));
//		List<ContentKeywordDetail> contentKeywordDetails = service.getEditableContent(serviceData, true, "");
//
//		assertNotNull("editable list cannot be null", contentKeywordDetails);
//		assertEquals("all the content should be editable", 1, contentKeywordDetails.size());
//		assertEquals("content size", 1, contentKeywordDetails.get(0).getDetails().size());
//		assertEquals("Sample content", contentKeywordDetails.get(0).getDetails().get(0).getDescriptionEn());
//		assertEquals("subkey0", contentKeywordDetails.get(0).getDetails().get(0).getKeyword());
//	}

    //todo fix this testcase
//	@Test
//	public void testGetEditableContentForAppWithOutSubkeys() throws Exception {
//		DateTimeUtils.setCurrentMillisFixed(new DateTime(2010, 9, 8, 7, 50, 0, 0).getMillis());
//
//		SubscriptionServiceData serviceData = new SubscriptionServiceData();
//		Periodicity subscriptionPeriodicity = new Periodicity(PeriodUnit.DAY, 1, 30);
//		subscriptionPeriodicity.setDefaultDispatchConfiguration(DefaultDispatchConfiguration.configureForDay(8, 30));
//		subscriptionPeriodicity.setFirstScheduledDispatchTime(new DateTime(Periodicity.getFistDispatchableDate(PeriodUnit.DAY, 1,
//				subscriptionPeriodicity.getDefaultDispatchConfiguration())));
//		serviceData.setPeriodicity(subscriptionPeriodicity);
//		ContentGroup contentGroup = new ContentGroup();
//		contentGroup.addContent(new KeywordDetail("", "Sample content"));
//		contentGroup.setScheduledDispathTime(new DateTime(2010, 9, 10, 8, 30, 0, 0).toDate());
//
//		serviceData.setData(Arrays.asList(contentGroup));
//		List<ContentKeywordDetail> contentKeywordDetails = service.getEditableContent(serviceData, false, "system-key");
//
//		assertNotNull("editable list cannot be null", contentKeywordDetails);
//		assertEquals("all the content should be editable", 1, contentKeywordDetails.size());
//		assertEquals("content size", 1, contentKeywordDetails.get(0).getDetails().size());
//		assertEquals("Sample content", contentKeywordDetails.get(0).getDetails().get(0).getDescriptionEn());
//		assertEquals("system-key", contentKeywordDetails.get(0).getDetails().get(0).getKeyword());
//	}

    //todo fix this test acse failure
//	@Test
//	public void testGetEditableContentWithOutdatedContent() throws Exception {
//		// Set current time
//		DateTimeUtils.setCurrentMillisFixed(new DateTime(2010, 9, 11, 7, 50, 0, 0).getMillis());
//
//		SubscriptionServiceData serviceData = new SubscriptionServiceData();
//		Periodicity subscriptionPeriodicity = new Periodicity(PeriodUnit.DAY, 1, 30);
//		subscriptionPeriodicity.setDefaultDispatchConfiguration(DefaultDispatchConfiguration.configureForDay(8, 30));
//		subscriptionPeriodicity.setFirstScheduledDispatchTime(new DateTime(Periodicity.getFistDispatchableDate(PeriodUnit.DAY, 1,
//				subscriptionPeriodicity.getDefaultDispatchConfiguration())));
//		serviceData.setPeriodicity(subscriptionPeriodicity);
//
//		ContentGroup cg1 = new ContentGroup();
//		cg1.addContent(new KeywordDetail("subkey0", "Sample content1"));
//		cg1.setScheduledDispathTime(new DateTime(2010, 9, 10, 8, 30, 0, 0).toDate());// This
//																						// is
//																						// outdated
//		ContentGroup cg2 = new ContentGroup();
//		cg2.addContent(new KeywordDetail("subkey0", "Sample content2"));
//		cg2.setScheduledDispathTime(new DateTime(2010, 9, 11, 8, 30, 0, 0).toDate());
//		ContentGroup cg3 = new ContentGroup();
//		cg3.addContent(new KeywordDetail("subkey0", "Sample content3"));
//		cg3.setScheduledDispathTime(new DateTime(2010, 9, 12, 8, 30, 0, 0).toDate());
//		ContentGroup cg4 = new ContentGroup();
//		cg4.addContent(new KeywordDetail("subkey0", "Sample content4"));
//		cg4.setScheduledDispathTime(new DateTime(2010, 9, 13, 8, 30, 0, 0).toDate());
//
//		serviceData.setData(Arrays.asList(cg1, cg2, cg3, cg4));
//		List<ContentKeywordDetail> contentKeywordDetails = service.getEditableContent(serviceData, true, "system-key");
//
//		assertNotNull("editable list cannot be null", contentKeywordDetails);
//		assertEquals("all the content should be editable", 3, contentKeywordDetails.size());
//
//		assertEquals("size of single content group", 1, contentKeywordDetails.get(0).getDetails().size());
//		assertEquals("subkey0", contentKeywordDetails.get(0).getDetails().get(0).getKeyword());
//		assertEquals("Sample content2", contentKeywordDetails.get(0).getDetails().get(0).getDescriptionEn());
//		assertEquals("Sample content3", contentKeywordDetails.get(1).getDetails().get(0).getDescriptionEn());
//		assertEquals("Sample content4", contentKeywordDetails.get(2).getDetails().get(0).getDescriptionEn());
//
//	}
 //todo fix this test case failure
//	@Test
//	public void testGetEditableContentWithDiscardedContent() throws Exception {
//		// Set current time
//		DateTimeUtils.setCurrentMillisFixed(new DateTime(2010, 9, 11, 8, 40, 0, 0).getMillis());
//
//		SubscriptionServiceData serviceData = new SubscriptionServiceData();
//		Periodicity subscriptionPeriodicity = new Periodicity(PeriodUnit.DAY, 1, 30);
//		subscriptionPeriodicity.setDefaultDispatchConfiguration(DefaultDispatchConfiguration.configureForDay(8, 30));
//		subscriptionPeriodicity.setFirstScheduledDispatchTime(new DateTime(Periodicity.getFistDispatchableDate(PeriodUnit.DAY, 1,
//				subscriptionPeriodicity.getDefaultDispatchConfiguration())));
//		serviceData.setPeriodicity(subscriptionPeriodicity);
//
//		ContentGroup cg1 = new ContentGroup();
//		cg1.addContent(new KeywordDetail("subkey0", "Sample content1"));
//		cg1.setScheduledDispathTime(new DateTime(2010, 9, 10, 8, 30, 0, 0).toDate());// This
//																						// is
//																						// outdated
//		ContentGroup cg2 = new ContentGroup();
//		cg2.addContent(new KeywordDetail("subkey0", "Sample content2"));
//		cg2.setScheduledDispathTime(new DateTime(2010, 9, 11, 8, 30, 0, 0).toDate());
//		ContentGroup cg3 = new ContentGroup();
//		cg3.setStatus(ContentGroupStatus.DISCARDED); // This content should be
//														// included in editable
//		cg3.addContent(new KeywordDetail("subkey0", "Sample content3"));
//		cg3.setScheduledDispathTime(new DateTime(2010, 9, 12, 8, 30, 0, 0).toDate());
//		ContentGroup cg4 = new ContentGroup();
//		cg4.addContent(new KeywordDetail("subkey0", "Sample content4"));
//		cg4.setScheduledDispathTime(new DateTime(2010, 9, 13, 8, 30, 0, 0).toDate());
//
//		serviceData.setData(Arrays.asList(cg1, cg2, cg3, cg4));
//		List<ContentKeywordDetail> contentKeywordDetails = service.getEditableContent(serviceData, true, "system-key");
//
//		assertNotNull("editable list cannot be null", contentKeywordDetails);
//		assertEquals("all the content should be editable", 3, contentKeywordDetails.size());
//
//		assertEquals("size of single content group", 1, contentKeywordDetails.get(0).getDetails().size());
//		assertEquals("subkey0", contentKeywordDetails.get(0).getDetails().get(0).getKeyword());
//		assertEquals("Sample content2", contentKeywordDetails.get(0).getDetails().get(0).getDescriptionEn());
//		assertEquals("status of content 2", ContentStatus.SCHEDULED, contentKeywordDetails.get(0).getStatus());
//		assertEquals("Sample content3", contentKeywordDetails.get(1).getDetails().get(0).getDescriptionEn());
//		assertEquals("status of content 3", ContentStatus.DISCARDED, contentKeywordDetails.get(1).getStatus());
//		assertEquals("Sample content4", contentKeywordDetails.get(2).getDetails().get(0).getDescriptionEn());
//
//	}
//todo fox the testcase failure
//	@Test
//	public void testGetEditableContentWithOutdatedContentAndCurrentTimeWithinBufferAndContentAlreadySent()
//			throws Exception {
//		// Set current time
//		DateTimeUtils.setCurrentMillisFixed(new DateTime(2010, 9, 11, 8, 40, 0, 0).getMillis());
//
//		SubscriptionServiceData serviceData = new SubscriptionServiceData();
//		Periodicity subscriptionPeriodicity = new Periodicity(PeriodUnit.DAY, 1, 30);
//		subscriptionPeriodicity.setDefaultDispatchConfiguration(DefaultDispatchConfiguration.configureForDay(8, 30));
//		subscriptionPeriodicity.setFirstScheduledDispatchTime(new DateTime(Periodicity.getFistDispatchableDate(PeriodUnit.DAY, 1,
//				subscriptionPeriodicity.getDefaultDispatchConfiguration())));
//		serviceData.setPeriodicity(subscriptionPeriodicity);
//
//		ContentGroup cg1 = new ContentGroup();
//		cg1.addContent(new KeywordDetail("subkey0", "Sample content1"));
//		cg1.setScheduledDispathTime(new DateTime(2010, 9, 10, 8, 30, 0, 0).toDate());// This
//																						// is
//																						// outdated
//		ContentGroup cg2 = new ContentGroup();
//		cg2.addContent(new KeywordDetail("subkey0", "Sample content2"));
//		cg2.setScheduledDispathTime(new DateTime(2010, 9, 11, 8, 30, 0, 0).toDate());
//		cg2.setStatus(ContentGroupStatus.SENT); // This content is sent
//		ContentGroup cg3 = new ContentGroup();
//		cg3.addContent(new KeywordDetail("subkey0", "Sample content3"));
//		cg3.setScheduledDispathTime(new DateTime(2010, 9, 12, 8, 30, 0, 0).toDate());
//		ContentGroup cg4 = new ContentGroup();
//		cg4.addContent(new KeywordDetail("subkey0", "Sample content4"));
//		cg4.setScheduledDispathTime(new DateTime(2010, 9, 13, 8, 30, 0, 0).toDate());
//
//		serviceData.setData(Arrays.asList(cg1, cg2, cg3, cg4));
//		List<ContentKeywordDetail> contentKeywordDetails = service.getEditableContent(serviceData, true, "system-key");
//
//		assertNotNull("editable list cannot be null", contentKeywordDetails);
//		assertEquals("all the content should be editable", 2, contentKeywordDetails.size());
//
//		assertEquals("size of single content group", 1, contentKeywordDetails.get(0).getDetails().size());
//		assertEquals("subkey0", contentKeywordDetails.get(0).getDetails().get(0).getKeyword());
//		assertEquals("Sample content3", contentKeywordDetails.get(0).getDetails().get(0).getDescriptionEn());
//		assertEquals("Sample content4", contentKeywordDetails.get(1).getDetails().get(0).getDescriptionEn());
//
//	}

    //todo fix this test acse failure
//	@Test
//	public void testCreateContentGroupFromContentKeywordDetailsList() throws Exception {
//
//		ContentKeywordDetail ckd1 = new ContentKeywordDetail();
//		ckd1.addKeyword(new KeywordDetails("subkey0", "description1-0"));
//		ckd1.addKeyword(new KeywordDetails("subkey1", "description1-1"));
//
//		ContentKeywordDetail ckd2 = new ContentKeywordDetail();
//		ckd2.addKeyword(new KeywordDetails("subkey0", "description2-0"));
//		ckd2.addKeyword(new KeywordDetails("subkey1", "description2-1"));
//
//		ContentKeywordDetail ckd3 = new ContentKeywordDetail();
//		ckd3.addKeyword(new KeywordDetails("subkey0", "description3-0"));
//		ckd3.addKeyword(new KeywordDetails("subkey1", "description3-1"));
//
//		List<ContentKeywordDetail> contentKeywordDetailsList = Arrays.asList(ckd1, ckd2, ckd3);
//
//		List<ContentGroup> contentGroupsList = service
//				.createContentReleaseFromContentKeywordDetailsList(contentKeywordDetailsList);
//		assertNotNull("resultant list cannot be null", contentGroupsList);
//		assertEquals("created list's size", contentKeywordDetailsList.size(), contentGroupsList.size());
//
//		assertEquals("keyword should match", contentKeywordDetailsList.get(0).getDetails().get(0).getKeyword(),
//				contentGroupsList.get(0).getAllKeywords().get(0).getKeyword());
//		assertEquals("description should match", contentKeywordDetailsList.get(0).getDetails().get(0)
//				.getDescriptionEn(), contentGroupsList.get(0).getAllKeywords().get(0).getDescription(Locale.ENGLISH));
//		assertEquals("keyword should match", contentKeywordDetailsList.get(0).getDetails().get(1).getKeyword(),
//				contentGroupsList.get(0).getAllKeywords().get(1).getKeyword());
//		assertEquals("description should match", contentKeywordDetailsList.get(0).getDetails().get(1)
//				.getDescriptionEn(), contentGroupsList.get(0).getAllKeywords().get(1).getDescription(Locale.ENGLISH));
//	}

	@Test
	public void testFilterBlankContent() throws Exception {
		List<ContentKeywordDetail> filteredList = service.filterBlankContent(null);
		assertNotNull("filtered content cannot be null", filteredList);
		filteredList = service.filterBlankContent(Collections.EMPTY_LIST);
		assertNotNull("filtered content cannot be null", filteredList);

		ContentKeywordDetail ckd1 = new ContentKeywordDetail();
		ckd1.addKeyword(new KeywordDetails("subkey0", "description1-0"));
		ckd1.addKeyword(new KeywordDetails("subkey1", "description1-1"));

		ContentKeywordDetail ckd2 = new ContentKeywordDetail();
		ckd2.addKeyword(new KeywordDetails("subkey0", "    "));
		ckd2.addKeyword(new KeywordDetails("subkey1", ""));

		ContentKeywordDetail ckd3 = new ContentKeywordDetail();
		ckd3.addKeyword(new KeywordDetails("subkey0", "description3-0"));
		ckd3.addKeyword(new KeywordDetails("subkey1", ""));

		ContentKeywordDetail ckd4 = new ContentKeywordDetail();
		ckd4.addKeyword(new KeywordDetails("subkey0", ""));
		ckd4.addKeyword(new KeywordDetails("subkey1", "description4-1"));

		ContentKeywordDetail ckd5 = new ContentKeywordDetail();
		ckd5.setContentGroupId(Long.valueOf(45)); // This content should not be filtered since
									// content groupId is there
		ckd5.addKeyword(new KeywordDetails("subkey0", ""));
		ckd5.addKeyword(new KeywordDetails("subkey1", ""));

		List<ContentKeywordDetail> contentKeywordDetailsList = Arrays.asList(ckd1, ckd2, ckd3, ckd4, ckd5);

		filteredList = service.filterBlankContent(contentKeywordDetailsList);
		assertNotNull("filtered content cannot be null", filteredList);
		assertEquals("size after filtering", 4, filteredList.size());
		assertEquals("content of filtered", "description1-0", filteredList.get(0).getDetails().get(0)
				.getDescriptionEn());
		assertEquals("content of filtered", "description3-0", filteredList.get(1).getDetails().get(0)
				.getDescriptionEn());
		assertEquals("content of filtered", "", filteredList.get(2).getDetails().get(0).getDescriptionEn());
		assertEquals("content of filtered", "description4-1", filteredList.get(2).getDetails().get(1)
				.getDescriptionEn());

		assertEquals("content of filtered", "", filteredList.get(3).getDetails().get(0).getDescriptionEn());
		assertEquals("content of filtered", "", filteredList.get(3).getDetails().get(1).getDescriptionEn());
	}

	@Test
	public void testSortContentsByScheduleTime() throws Exception {
		List<ContentKeywordDetail> sortedList = service.sortContentsByScheduleTime(null);
		assertNotNull("sorted list cannot be null", sortedList);

		ContentKeywordDetail ckd1 = new ContentKeywordDetail();
		ckd1.setContentGroupId(Long.valueOf(1));
		ckd1.setScheduledDate(new DateTime(2010, 9, 9, 8, 30, 0, 0).toDate());

		ContentKeywordDetail ckd2 = new ContentKeywordDetail();
		ckd2.setContentGroupId(Long.valueOf(2));
		ckd2.setScheduledDate(new DateTime(2010, 9, 9, 10, 30, 0, 0).toDate());

		ContentKeywordDetail ckd3 = new ContentKeywordDetail();
		ckd3.setContentGroupId(Long.valueOf(3));
		ckd3.setScheduledDate(new DateTime(2010, 9, 9, 12, 30, 0, 0).toDate());

		ContentKeywordDetail ckd4 = new ContentKeywordDetail();
		ckd4.setContentGroupId(Long.valueOf(4));
		ckd4.setScheduledDate(new DateTime(2010, 9, 9, 9, 30, 0, 0).toDate());

		ContentKeywordDetail ckd5 = new ContentKeywordDetail();
		ckd5.setContentGroupId(Long.valueOf(5));
		ckd5.setScheduledDate(new DateTime(2010, 9, 9, 11, 30, 0, 0).toDate());

		List<ContentKeywordDetail> contentKeywordDetailsList = Arrays.asList(ckd1, ckd2, ckd3, ckd4, ckd5);

		sortedList = service.sortContentsByScheduleTime(contentKeywordDetailsList);
		assertNotNull("sorted list cannot be null", sortedList);
		assertEquals(new DateTime(2010, 9, 9, 8, 30, 0, 0).toDate(), sortedList.get(0).getScheduledDate());
		assertEquals(new DateTime(2010, 9, 9, 9, 30, 0, 0).toDate(), sortedList.get(1).getScheduledDate());
		assertEquals(new DateTime(2010, 9, 9, 10, 30, 0, 0).toDate(), sortedList.get(2).getScheduledDate());
		assertEquals(new DateTime(2010, 9, 9, 11, 30, 0, 0).toDate(), sortedList.get(3).getScheduledDate());
		assertEquals(new DateTime(2010, 9, 9, 12, 30, 0, 0).toDate(), sortedList.get(4).getScheduledDate());
	}

	@Test
	public void testFillMissingTimeSlotsForHour() throws Exception {
		Periodicity periodicity = new Periodicity(PeriodUnit.HOUR, 1, 10);
		periodicity.setDefaultDispatchConfiguration(DefaultDispatchConfiguration.configureForHour(30));
		periodicity.setFirstScheduledDispatchTime(new DateTime(Periodicity.getFistDispatchableDate(PeriodUnit.HOUR, 1,
				periodicity.getDefaultDispatchConfiguration())));

		List<ContentKeywordDetail> contentList = null;
		List<ContentKeywordDetail> newList = service.fillMissingTimeSlots(periodicity, contentList);
		assertNotNull("result list cannot be null", newList);

		ContentKeywordDetail ckd1 = new ContentKeywordDetail();
		ckd1.setContentGroupId(Long.valueOf(1));
		ckd1.addKeyword(new KeywordDetails("subkey0", "description1-0"));
		ckd1.setScheduledDate(new DateTime(2010, 9, 9, 8, 30, 0, 0).toDate());

		ContentKeywordDetail ckd2 = new ContentKeywordDetail();
		ckd2.setContentGroupId(Long.valueOf(2));
		ckd2.addKeyword(new KeywordDetails("subkey0", "description1-0"));
		ckd2.setScheduledDate(new DateTime(2010, 9, 9, 10, 30, 0, 0).toDate());

		ContentKeywordDetail ckd3 = new ContentKeywordDetail();
		ckd3.setContentGroupId(Long.valueOf(3));
		ckd3.addKeyword(new KeywordDetails("subkey0", "description1-0"));
		ckd3.setScheduledDate(new DateTime(2010, 9, 9, 12, 30, 0, 0).toDate());

		ContentKeywordDetail ckd4 = new ContentKeywordDetail();
		ckd4.setContentGroupId(Long.valueOf(4));
		ckd4.addKeyword(new KeywordDetails("subkey0", "description1-0"));
		ckd4.setScheduledDate(new DateTime(2010, 9, 9, 14, 30, 0, 0).toDate());

		contentList = Arrays.asList(ckd1, ckd2, ckd3, ckd4);

		newList = service.fillMissingTimeSlots(periodicity, contentList);
		assertNotNull("result list cannot be null", newList);
		assertEquals("list size after missing times slots were filled.", 7, newList.size());
		assertEquals("scheduled time", new DateTime(2010, 9, 9, 8, 30, 0, 0).toDate(), newList.get(0)
				.getScheduledDate());
		assertEquals("scheduled time", new DateTime(2010, 9, 9, 9, 30, 0, 0).toDate(), newList.get(1)
				.getScheduledDate());
		assertEquals("scheduled time", new DateTime(2010, 9, 9, 10, 30, 0, 0).toDate(), newList.get(2)
				.getScheduledDate());
		assertEquals("scheduled time", new DateTime(2010, 9, 9, 11, 30, 0, 0).toDate(), newList.get(3)
				.getScheduledDate());
		assertEquals("scheduled time", new DateTime(2010, 9, 9, 12, 30, 0, 0).toDate(), newList.get(4)
				.getScheduledDate());
		assertEquals("scheduled time", new DateTime(2010, 9, 9, 13, 30, 0, 0).toDate(), newList.get(5)
				.getScheduledDate());
		assertEquals("scheduled time", new DateTime(2010, 9, 9, 14, 30, 0, 0).toDate(), newList.get(6)
				.getScheduledDate());

	}

	@Test
	public void testFillMissingTimeSlotsWhenOnlyOneContentAvailable() throws Exception {
		Periodicity periodicity = new Periodicity(PeriodUnit.HOUR, 1, 10);
		periodicity.setDefaultDispatchConfiguration(DefaultDispatchConfiguration.configureForHour(30));
		periodicity.setFirstScheduledDispatchTime(new DateTime(Periodicity.getFistDispatchableDate(PeriodUnit.HOUR, 1,
				periodicity.getDefaultDispatchConfiguration())));

		List<ContentKeywordDetail> contentList = null;
		ContentKeywordDetail ckd1 = new ContentKeywordDetail();
		ckd1.setContentGroupId(Long.valueOf(1));
		ckd1.addKeyword(new KeywordDetails("subkey0", "description1-0"));
		ckd1.setScheduledDate(new DateTime(2010, 9, 9, 8, 30, 0, 0).toDate());

		contentList = Arrays.asList(ckd1);

		List<ContentKeywordDetail> newList = service.fillMissingTimeSlots(periodicity, contentList);
		assertNotNull("result list cannot be null", newList);
		assertEquals("list size after missing times slots were filled.", 1, newList.size());
		assertEquals("scheduled time", new DateTime(2010, 9, 9, 8, 30, 0, 0).toDate(), newList.get(0)
				.getScheduledDate());

	}

	@Test
	public void testFillMissingTimeSlotsWhenNoMissingSlotsAvailable() throws Exception {
		Periodicity periodicity = new Periodicity(PeriodUnit.HOUR, 1, 10);
		periodicity.setDefaultDispatchConfiguration(DefaultDispatchConfiguration.configureForHour(30));
		periodicity.setFirstScheduledDispatchTime(new DateTime(Periodicity.getFistDispatchableDate(PeriodUnit.HOUR, 1,
				periodicity.getDefaultDispatchConfiguration())));

		List<ContentKeywordDetail> contentList = null;
		List<ContentKeywordDetail> newList = null;

		ContentKeywordDetail ckd1 = new ContentKeywordDetail();
		ckd1.setContentGroupId(Long.valueOf(1));
		ckd1.addKeyword(new KeywordDetails("subkey0", "description1-0"));
		ckd1.setScheduledDate(new DateTime(2010, 9, 9, 8, 30, 0, 0).toDate());

		ContentKeywordDetail ckd2 = new ContentKeywordDetail();
		ckd2.setContentGroupId(Long.valueOf(2));
		ckd2.addKeyword(new KeywordDetails("subkey0", "description1-0"));
		ckd2.setScheduledDate(new DateTime(2010, 9, 9, 9, 30, 0, 0).toDate());

		ContentKeywordDetail ckd3 = new ContentKeywordDetail();
		ckd3.setContentGroupId(Long.valueOf(3));
		ckd3.addKeyword(new KeywordDetails("subkey0", "description1-0"));
		ckd3.setScheduledDate(new DateTime(2010, 9, 9, 10, 30, 0, 0).toDate());

		contentList = Arrays.asList(ckd1, ckd2, ckd3);

		newList = service.fillMissingTimeSlots(periodicity, contentList);
		assertNotNull("result list cannot be null", newList);
		assertEquals("list size after missing times slots were filled.", 3, newList.size());
		assertEquals("scheduled time", new DateTime(2010, 9, 9, 8, 30, 0, 0).toDate(), newList.get(0)
				.getScheduledDate());
		assertEquals("scheduled time", new DateTime(2010, 9, 9, 9, 30, 0, 0).toDate(), newList.get(1)
				.getScheduledDate());
		assertEquals("scheduled time", new DateTime(2010, 9, 9, 10, 30, 0, 0).toDate(), newList.get(2)
				.getScheduledDate());

	}

	@Test
	public void testFillMissingTimeSlotsForDay() throws Exception {
		Periodicity periodicity = new Periodicity(PeriodUnit.DAY, 1, 30);
		periodicity.setDefaultDispatchConfiguration(DefaultDispatchConfiguration.configureForDay(8, 30));
		periodicity.setFirstScheduledDispatchTime(new DateTime(Periodicity.getFistDispatchableDate(PeriodUnit.DAY, 1,
				periodicity.getDefaultDispatchConfiguration())));

		List<ContentKeywordDetail> contentList = null;
		List<ContentKeywordDetail> newList = null;

		ContentKeywordDetail ckd1 = new ContentKeywordDetail();
		ckd1.setContentGroupId(Long.valueOf(1));
		ckd1.addKeyword(new KeywordDetails("subkey0", "description1-0"));
		ckd1.setScheduledDate(new DateTime(2010, 9, 9, 8, 30, 0, 0).toDate());

		ContentKeywordDetail ckd2 = new ContentKeywordDetail();
		ckd2.setContentGroupId(Long.valueOf(2));
		ckd2.addKeyword(new KeywordDetails("subkey0", "description1-0"));
		ckd2.setScheduledDate(new DateTime(2010, 9, 15, 8, 30, 0, 0).toDate());

		ContentKeywordDetail ckd3 = new ContentKeywordDetail();
		ckd3.setContentGroupId(Long.valueOf(3));
		ckd3.addKeyword(new KeywordDetails("subkey0", "description1-0"));
		ckd3.setScheduledDate(new DateTime(2010, 9, 13, 8, 30, 0, 0).toDate());

		ContentKeywordDetail ckd4 = new ContentKeywordDetail();
		ckd4.setContentGroupId(Long.valueOf(4));
		ckd4.addKeyword(new KeywordDetails("subkey0", "description1-0"));
		ckd4.setScheduledDate(new DateTime(2010, 9, 16, 8, 30, 0, 0).toDate());

		contentList = Arrays.asList(ckd1, ckd2, ckd3, ckd4);

		newList = service.fillMissingTimeSlots(periodicity, contentList);
		assertNotNull("result list cannot be null", newList);
		assertEquals("list size after missing times slots were filled.", 8, newList.size());
		assertEquals("scheduled time", new DateTime(2010, 9, 9, 8, 30, 0, 0).toDate(), newList.get(0)
				.getScheduledDate());
		assertEquals("scheduled time", new DateTime(2010, 9, 10, 8, 30, 0, 0).toDate(), newList.get(1)
				.getScheduledDate());
		assertEquals("scheduled time", new DateTime(2010, 9, 11, 8, 30, 0, 0).toDate(), newList.get(2)
				.getScheduledDate());
		assertEquals("scheduled time", new DateTime(2010, 9, 12, 8, 30, 0, 0).toDate(), newList.get(3)
				.getScheduledDate());
		assertEquals("scheduled time", new DateTime(2010, 9, 13, 8, 30, 0, 0).toDate(), newList.get(4)
				.getScheduledDate());
		assertEquals("scheduled time", new DateTime(2010, 9, 14, 8, 30, 0, 0).toDate(), newList.get(5)
				.getScheduledDate());
		assertEquals("scheduled time", new DateTime(2010, 9, 15, 8, 30, 0, 0).toDate(), newList.get(6)
				.getScheduledDate());
		assertEquals("scheduled time", new DateTime(2010, 9, 16, 8, 30, 0, 0).toDate(), newList.get(7)
				.getScheduledDate());

	}

	@Test
	public void testFillMissingTimeSlotsForMonth() throws Exception {
		Periodicity periodicity = new Periodicity(PeriodUnit.MONTH, 1, 30);
		periodicity.setDefaultDispatchConfiguration(DefaultDispatchConfiguration.configureForDayOfMonth(31, 8, 30));
		periodicity.setFirstScheduledDispatchTime(new DateTime(Periodicity.getFistDispatchableDate(PeriodUnit.MONTH, 1,
				periodicity.getDefaultDispatchConfiguration())));

		List<ContentKeywordDetail> contentList = null;
		List<ContentKeywordDetail> newList = null;

		ContentKeywordDetail ckd1 = new ContentKeywordDetail();
		ckd1.setContentGroupId(Long.valueOf(1));
		ckd1.addKeyword(new KeywordDetails("subkey0", "description1-0"));
		ckd1.setScheduledDate(new DateTime(2010, 9, 30, 8, 30, 0, 0).toDate());

		ContentKeywordDetail ckd2 = new ContentKeywordDetail();
		ckd2.setContentGroupId(Long.valueOf(2));
		ckd2.addKeyword(new KeywordDetails("subkey0", "description1-0"));
		ckd2.setScheduledDate(new DateTime(2010, 8, 31, 8, 30, 0, 0).toDate());

		ContentKeywordDetail ckd3 = new ContentKeywordDetail();
		ckd3.setContentGroupId(Long.valueOf(3));
		ckd3.addKeyword(new KeywordDetails("subkey0", "description1-0"));
		ckd3.setScheduledDate(new DateTime(2010, 11, 30, 8, 30, 0, 0).toDate());

		ContentKeywordDetail ckd4 = new ContentKeywordDetail();
		ckd4.setContentGroupId(Long.valueOf(4));
		ckd4.addKeyword(new KeywordDetails("subkey0", "description1-0"));
		ckd4.setScheduledDate(new DateTime(2011, 2, 28, 8, 30, 0, 0).toDate());

		contentList = Arrays.asList(ckd1, ckd2, ckd3, ckd4);

		newList = service.fillMissingTimeSlots(periodicity, contentList);
		assertNotNull("result list cannot be null", newList);
		assertEquals("list size after missing times slots were filled.", 7, newList.size());
		assertEquals("scheduled time", new DateTime(2010, 8, 31, 8, 30, 0, 0).toDate(), newList.get(0)
				.getScheduledDate());
		assertEquals("scheduled time", new DateTime(2010, 9, 30, 8, 30, 0, 0).toDate(), newList.get(1)
				.getScheduledDate());
		assertEquals("scheduled time", new DateTime(2010, 10, 31, 8, 30, 0, 0).toDate(), newList.get(2)
				.getScheduledDate());
		assertEquals("scheduled time", new DateTime(2010, 11, 30, 8, 30, 0, 0).toDate(), newList.get(3)
				.getScheduledDate());
		assertEquals("scheduled time", new DateTime(2010, 12, 31, 8, 30, 0, 0).toDate(), newList.get(4)
				.getScheduledDate());
		assertEquals("scheduled time", new DateTime(2011, 1, 31, 8, 30, 0, 0).toDate(), newList.get(5)
				.getScheduledDate());
		assertEquals("scheduled time", new DateTime(2011, 2, 28, 8, 30, 0, 0).toDate(), newList.get(6)
				.getScheduledDate());

	}
}
